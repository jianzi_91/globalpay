<?php

namespace App\Repositories;

use App\Models\Country;
use Illuminate\Support\Facades\Schema;

class CountryRepo
{
    public static function all()
    {
        return Country::all();
    }

    public function getTable()
    {
        return with(new Country)->getTable();
    }

    public function getKeyName()
    {
        return with(new Country)->getKeyName();
    }

    public static function all_member_selectable_country()
    {
		$country_id = with(new Country)->getKeyName();
		$country_table = with(new Country)->getTable();
        
        $country_locale = 'country_'.str_replace('-', '_', app()->getLocale());
        $country_default = 'country_en';
        if(Schema::hasColumn($country_table, $country_locale) &&  $country_locale !=  $country_default){
            $country = Country::selectRaw("CASE WHEN ".$country_locale." <> '' THEN ".$country_locale." ELSE ".$country_default." END as country_name, ".$country_id);
        }
        else{
            $country_locale = $country_default;
            $country = Country::select($country_locale." as country_name", $country_id);
        }
        //dd($country->where('user', 1)->toSql());
        $country = $country->where('user', 1)->orderBy('country_en');
        
        return $country->pluck('country_name', $country_id);
    }

    public static function all_member_selectable_phone_code()
    {
        $country_table = with(new Country)->getTable();
        
        $country_locale = 'country_'.str_replace('-', '_', app()->getLocale());
        $country_default = 'country_en';
        if(Schema::hasColumn($country_table, $country_locale) &&  $country_locale !=  $country_default){
            $country_name = "CASE WHEN ".$country_locale." <> '' THEN ".$country_locale." ELSE ".$country_default." END";
        }
        else{
            $country_name = $country_locale = $country_default;
        }
        $country = Country::where('user', 1)->selectRaw("CONCAT(".$country_name.", \" (+\",phone_code, \")\") as phone_code_concat, phone_code, country_code_2")->orderBy('country_en');
        
        return $country->get()->keyBy('country_code_2');
    }

    public static function getCountryByCode($code)
    {
        $country_locale = 'country_'.str_replace('-', '_', app()->getLocale()?app()->getLocale(): "en");
        $country_default = 'country_en';

        return Country::where(with(new Country)->getKeyName(), $code)
            ->selectRaw("CASE WHEN ".$country_locale." <> '' THEN ".$country_locale." ELSE ".$country_default." END as country_name")
            ->value("country_name");
    }

    public static function getWithdrawalCurrencyId($code)
    {
        return Country::where(with(new Country)->getKeyName(), $code)
            ->value("withdraw_currency_id");
    }
}
