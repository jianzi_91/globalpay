<?php

namespace App\Repositories;

use Carbon\Carbon;
use DB;
use Exception;

use App\Models\Member;
use App\Models\MemberTrade;
use App\Models\MemberTradeLog;
use App\Models\MemberTradeConvert;
use App\Models\MemberNonTrade;
use App\Models\MemberNonTradeLog;
use App\Models\TradeStats;
use App\Models\TradeSell;
use App\Models\TradeBuy;
use App\Models\TradeMatched;
use App\Models\TradeNumber;
use App\Models\TradeQueue;
use App\Models\Activation;
use App\Models\TradeSetting;
use App\Utils\DateTimeTool;

class TradeRepo
{
    public function createTradeBuy($uid, $data)
    {
        return TradeBuy::create([
            'uid' => $uid,
            'initial_qty' => $data['max_qty'],
            'min_qty' => $data['min_qty'],
            'open_qty' => $data['max_qty'],
            'currency' => 'CNY', // temporary hardcode
            'price' => $data['price'],
            'payment_method' => isset($data['payment'])?$data['payment']:TradeSell::PAYMENT['BANK_TRANSFER'],
            'type' => isset($data['type']) ? $data['type'] : TradeBuy::TYPE['CUSTOM'],
            'status' => TradeSell::STATUS['PENDING'],
        ]);
    }

    public function createTradeSell($uid, $data)
    {
        return TradeSell::create([
            'uid' => $uid,
            'initial_qty' => $data['max_qty'],
            'min_qty' => $data['min_qty'],
            'open_qty' => $data['max_qty'],
            'locked_qty' => isset($data['locked_qty'])?$data['locked_qty']:0,
            'currency' => 'CNY', // temporary hardcode
            'price' => $data['price'],
            'payment_method' => isset($data['payment'])?$data['payment']:TradeSell::PAYMENT['BANK_TRANSFER'],
            'payment_id' => isset($data['payment_id'])?$data['payment_id']:null,
            'type' => isset($data['type']) ? $data['type'] : TradeSell::TYPE['CUSTOM'],
            'status' => TradeSell::STATUS['PENDING'],
        ]);
    }

    public function countQueueingSells($buyId)
    {
        return TradeQueue::where('buy_id', $buyId)
            ->active()
            ->count();
    }

    public function countQueueingBuys($sellId)
    {
        return TradeQueue::where('sell_id', $sellId)
            ->active()
            ->count();
    }

    public function getPendingTradeBuy() 
    {
        return TradeBuy::where('status', 'P')
            ->where('open_qty', '>', 0)
            ->where('type', TradeBuy::TYPE['CUSTOM'])
            ->orderBy('price', 'desc');
    }

    public function getPendingTradeSell() 
    {
        return TradeSell::where('status', 'P')
            ->where('open_qty', '>', 0)
            ->where('type', TradeSell::TYPE['CUSTOM'])
            ->orderBy('price', 'asc');
    }

    public function getTradeSellById($sellId)
    {
        return TradeSell::find($sellId);
    }

    public function getTradeBuyById($buyId)
    {
        return TradeBuy::find($buyId);
    }

    public function getTradeSetting()
    {
        return TradeSetting::first();
    }

    public function requestBuying($tradesell_id, $buyer_id, $qty)
    {
        $setting = $this->getTradeSetting();
        $tradeSell = TradeSell::find($tradesell_id);
        $buyer = Member::find($buyer_id);
        $seller = Member::find($tradeSell->uid);

        // Create new tradebuy by system
        $data = [
            'min_qty' => $qty,
            'max_qty' => $qty,
            'price' => $tradeSell->price,
            'type' => TradeBuy::TYPE['SYSTEM'],
        ];

        $tradeBuy = $this->createTradeBuy($buyer_id, $data);

        $tradeBuy->update([
            'open_qty' => 0,
            'payment_method' => $tradeSell->payment_method,
        ]);
        // End of createion
        $qty_after_fees = $qty + ($qty * $setting->platform_charge / 100) + ($qty * $setting->charity_percent / 100);

        $tradeSell->open_qty -= $qty_after_fees;
        $tradeSell->locked_qty  += $qty_after_fees;
        $tradeSell->save();

        // queue the buying request
        return TradeQueue::create([
            'buy_id' => $tradeBuy->id,
            'buy_uid' => $tradeBuy->uid,
            'sell_id' => $tradeSell->id,
            'sell_uid' => $tradeSell->uid,
            'status' => TradeQueue::STATUS['INITIATED'],
            'quantity' => $qty,
            'price' => $tradeSell->price,
            // 'expired_at' => Carbon::now()->addDay(),
            'expired_at' => Carbon::now()->addDay(),
        ]);
    }

    public function requestSelling($tradebuy_id, $seller_id, $qty)
    {
        $setting = $this->getTradeSetting();
        $tradeBuy = TradeBuy::find($tradebuy_id);
        $seller = Member::find($seller_id);
        $buyer = Member::find($tradeBuy->uid);

        $qty_after_fees = $qty + ($qty * $setting->platform_charge / 100) + ($qty * $setting->charity_percent / 100);

        // Create new tradesell by system
        $data = [
            'min_qty' => $qty_after_fees,
            'max_qty' => $qty_after_fees,
            'locked_qty' => $qty_after_fees,
            'price' => $tradeBuy->price,
            'type' => TradeBuy::TYPE['SYSTEM'],
        ];

        $tradeSell = $this->createTradeSell($seller_id, $data);
        // End of createion

        $tradeSell->update([
            'open_qty' => 0,
            'payment_method' => $tradeBuy->payment_method,
        ]);

        $tradeBuy->open_qty -= $qty;
        $tradeBuy->save();

        // queue the buying request
        return TradeQueue::create([
            'buy_id' => $tradeBuy->id,
            'buy_uid' => $tradeBuy->uid,
            'sell_id' => $tradeSell->id,
            'sell_uid' => $tradeSell->uid,
            'status' => TradeQueue::STATUS['INITIATED'],
            'quantity' => $qty,
            'price' => $tradeBuy->price,
            // 'expired_at' => Carbon::now()->addDay(),
            'expired_at' => Carbon::now()->addDay(),
        ]);
    }

    public function getMemberPendingBuyingRequests($member_id)
    {
        return TradeQueue::where('buy_uid', $member_id)
                ->where('status', TradeQueue::STATUS['PENDING'])
                ->get();
    }

    public function getMemberPendingAndPaidBuyingRequests($member_id)
    {
        return TradeQueue::where('buy_uid', $member_id)
                ->whereIn('status', [TradeQueue::STATUS['PENDING'],TradeQueue::STATUS['PAID']])
                ->get();
    }

    public function getMemberBuyingRequestById($queue_id)
    {
        return TradeBuy::where('id', $queue_id)
                ->first();
    }

    public function getMemberTradeSells($member_id)
    {
        return TradeSell::where('uid', $member_id)
            ->where('status', TradeSell::STATUS['PENDING'])
            ->get();
    }

    public function getMemberTradeBuys($member_id)
    {
        return TradeBuy::where('uid', $member_id)->get();
    }

    public function getTradeQueueById($queue_id)
    {
        return TradeQueue::find($queue_id);
    }

    public function getTradeQueueByTradeSellId($tradesell_id)
    {
        return TradeQueue::where('sell_id', $tradesell_id)->first();
    }

    public function getTradeQueueByTradeBuyId($tradebuy_id)
    {
        return TradeQueue::where('buy_id', $tradebuy_id)->first();
    }

    public function getQueueingTradeByTradeSellId($tradesell_id)
    {
        return TradeQueue::where('sell_id', $tradesell_id)
            ->where('status', TradeQueue::STATUS['PENDING'])
            ->orWhere('status', TradeQueue::STATUS['PAID'])
            ->first();
    }

    public function getQueueingTradeByTradeBuyId($tradebuy_id)
    {
        return TradeQueue::where('buy_id', $tradebuy_id)
            ->where('status', TradeQueue::STATUS['PENDING'])
            ->orWhere('status', TradeQueue::STATUS['PAID'])
            ->first();
    }

    public function getLockedCoin($member_id)
    {
        $queues = TradeQueue::where('sell_id', $member_id)
                            ->where('status', TradeQueue::STATUS['PENDING'])
                            ->get();

        $total_locked = 0;

        foreach($queues as $queue) {
            $total_locked += $queue->price * $queue->quantity;
        }

        $charity_locked = $total_locked * config('trade.charity_rate');
        $processing_locked = $total_locked * config('trade.processing_rate');

        $coin_locked = $total_locked - $charity_locked - $processing_locked;

        return [
            'charity' => $charity_locked,
            'processing' => $processing_locked,
            'coin' => $coin_locked,
            'total' => $total_locked,
        ];
    }

    public function getMemberQueueingBuys($member_id)
    {
        return TradeQueue::where('buy_uid', $member_id)
                ->whereIn('status', [TradeQueue::STATUS['PENDING'],TradeQueue::STATUS['PAID']])
                ->get();
    }

    public function getMemberCustomBuys($member_id)
    {
        return TradeBuy::where('uid', $member_id)
            ->custom()
            ->where('status',TradeBuy::STATUS['PENDING'])
            ->get();
    }

    public function getMemberQueueingSells($member_id)
    {
        return TradeQueue::where('sell_uid', $member_id)
                ->whereIn('status', [TradeQueue::STATUS['PENDING'],TradeQueue::STATUS['PAID']])
                ->get();
    }

    public function getMemberCustomSells($member_id)
    {
        return TradeSell::where('uid', $member_id)
            ->pending()
            ->custom()
            ->get();
    }

    public function fulfillTradeBuy($tradebuy_id)
    {
        return TradeBuy::find($tradebuy_id)
            ->update([
                'fulfilled_at' => DateTimeTool::systemDateTime(),
                'status' => TradeBuy::STATUS['FULFILLED'],
            ]);
    }

    public function fulfillTradeSell($tradesell_id)
    {
        return TradeSell::find($tradesell_id)
            ->update([
                'fulfilled_at' => DateTimeTool::systemDateTime(),
                'status' => TradeSell::STATUS['FULFILLED'],
            ]);
    }

    public function raiseTradePrice()
    {
        $tradeSetting = TradeSetting::first();
        $tradeSetting->qty_sold = 0;
        $tradeSetting->price += $tradeSetting->price_change;
        return $tradeSetting->save();
    }

    public function updateTradeQuantityMax($amt_max)
    {
        $tradeSetting = TradeSetting::first();
        $tradeSetting->qty_per_price = $amt_max;
        return $tradeSetting->save();
    }

    public function completeTrade($queue_id)
    {
        return TradeQueue::find($queue_id)
            ->update([
                'status' => TradeQueue::STATUS['COMPLETE'],
            ]);
    }

    public function cancelTradeQueue($queue_id)
    {
        return TradeQueue::find($queue_id)
            ->update([
                'status' => TradeQueue::STATUS['CANCELLED'],
            ]);
    }

    public function cancelTradeSell($tradesell_id)
    {
        return TradeSell::find($tradesell_id)
            ->update([
                'status' => TradeSell::STATUS['CANCELLED'],
            ]);
    }

    public function cancelTradeBuy($tradebuy_id)
    {
        return TradeBuy::find($tradebuy_id)
            ->update([
                'status' => TradeBuy::STATUS['CANCELLED'],
            ]);
    }

    public function getTradeBuyActiveQueues($tradebuy_id)
    {
        return TradeQueue::where('buy_id', $tradebuy_id)
            ->active()
            ->get();
    }

    public function getTradeBuyInactiveQueues($tradebuy_id)
    {
        return TradeQueue::where('buy_id', $tradebuy_id)
            ->inactive()
            ->get();
    }

    public function getTradeSellActiveQueues($tradesell_id)
    {
        return TradeQueue::where('sell_id', $tradesell_id)
            ->active()
            ->get();
    }

    public function getTradeSellInactiveQueues($tradesell_id)
    {
        return TradeQueue::where('sell_id', $tradesell_id)
            ->inactive()
            ->get();
    }

    public function paginatedMemberInactiveQueues($member_id, $chunk)
    {
        return TradeQueue::where('buy_uid', $member_id)
            ->orWhere('sell_uid', $member_id)
            ->inactive()
            ->paginate($chunk);
    }

    public function getMemberPendingTradeSell($member_id)
    {
        return TradeSell::where('uid', $member_id)
            ->where('status', TradeSell::STATUS['PENDING'])
            ->get();
    }

    public function getMemberPendingTradeBuy($member_id)
    {
        return TradeBuy::where('uid', $member_id)
            ->where('status', TradeBuy::STATUS['PENDING'])
            ->get();
    }

    public function getAllPendingQueues()
    {
        return TradeQueue::where('status', TradeQueue::STATUS['PENDING'])
            ->get();
    }

    public function getTodayCompletedTrades()
    {
        return TradeQueue::where('status', TradeQueue::STATUS['COMPLETE'])
            ->whereBetween('updated_at', [Carbon::today(),Carbon::tomorrow()])
            ->get();           
    }

    public function getPaymentMethodArray()
    {
        return array(TradeSell::PAYMENT['BANK_TRANSFER'],TradeSell::PAYMENT['WECHAT_PAY'],TradeSell::PAYMENT['ALIPAY']);
    }
}
