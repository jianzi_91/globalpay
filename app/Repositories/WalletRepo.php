<?php

namespace App\Repositories;

use App\Models\MemberWalletRecord;
use App\Models\MemberStatus;
use App\Models\Wallet;
use App\Models\TransInfo;
use App\Models\TradeSetting;

class WalletRepo
{
    public function getWalletsByUserId($user_id)
    {
        return Wallet::where('uid', $user_id)
            ->select('rwallet', "awallet", "awallet2", 'wallet1', 'wallet2', 'cwallet', 'swallet', 'fwallet', 'iwallet', 'dwallet', 'dowallet')
            ->first();
    }

    public function createWalletsByUserId($user_id)
    {
        return Wallet::create(['uid' => $user_id]);
    }

    public function transferByTrade($buyer, $seller, $system, $trade_info)
    {
        $buyer->wallet->update([
            'cwallet' => $buyer->wallet->cwallet + $trade_info['quantity'],
        ]);

        $seller->wallet->update([
            'cwallet' => $seller->wallet->cwallet - $trade_info['quantity'] - $trade_info['maintenance'] - $trade_info['charity'],
            'dowallet' => $seller->wallet->dowallet + $trade_info['charity'],
        ]);

        $system->wallet->update([
            'cwallet' => $system->wallet->cwallet + $trade_info['maintenance'],
        ]);

        $trade_info['buy_trans_code'] = TransInfo::$trans_type_to_code['trade-buy'];
        $trade_info['sell_trans_code'] = TransInfo::$trans_type_to_code['trade-sell'];

        // Record member transaction
        $buyer_trans = new MemberWalletRecord;
        $buyer_trans->itype = MemberWalletRecord::ITYPE['USER'];
        $buyer_trans->iid = $buyer->id;
        $buyer_trans->uid = $buyer->id;
        $buyer_trans->wallet = 'cwallet';
        $buyer_trans->type = MemberWalletRecord::TYPE['DEBIT'];
        $buyer_trans->tuid = $seller->id;
        $buyer_trans->trans_code = TransInfo::$trans_type_to_code['trade-sell'];
        $buyer_trans->amount = $transaction_info['amount'];
        $buyer_trans->balance = $buyer->wallet->cwallet;
        $buyer_trans->descr = TransInfo::getDescFromCode($trade_info->toJson());
        $buyer_trans->json_descr = $trade_info->toJson();

        // Record receiver transaction
        $receiver_trans = new MemberWalletRecord;
        $receiver_trans->member_id = $receiver->id;
        $receiver_trans->transaction_type = MemberWalletRecord::TYPE['MEMBER_WALLET_TRANSFER_IN'];
        $receiver_trans->related_id = $member->id;
        $receiver_trans->{$transfer_to_wallet} = $transfer_info['amount'];
        $receiver_trans->{$transfer_to_wallet . '_balance'} = $receiver_wallet->{$transfer_to_wallet};
        $receiver_trans->remarks = $transfer_info['remarks'];
        $receiver_trans->creator_uid = $transfer_info['initiator_id'];
        $receiver_trans->creator_user_type = $transfer_info['initiator_user_type'];
        $receiver_trans->save();
    }

    public function buyCoin($queue)
    {
        $wallet = Wallet::where('uid', $queue->buy_uid)->first();

        if ($wallet->member->memberStatus->alliance_status == MemberStatus::ALLIANCE['YES']) {
            $wallet->fwallet += $queue->quantity;
        }
        else {
            $wallet->awallet2 += $queue->quantity;
        }
        
        $wallet->save();

        $buy_arr = [
            'id' => $queue->buy_id,
            'uid' => $queue->buy_uid,
            'qty' => $queue->quantity,
            'trans_code' => TransInfo::$trans_type_to_code['trade-buy'],
        ];

        // Record transaction
        $buyer_trans = new MemberWalletRecord;
        $buyer_trans->itype = MemberWalletRecord::ITYPE['USER'];
        $buyer_trans->iid = $queue->sell_uid;
        $buyer_trans->uid = $queue->buy_uid;
        if ($wallet->member->memberStatus->alliance_status == MemberStatus::ALLIANCE['YES']) {
            $buyer_trans->wallet = 'fwallet';
        }
        else {
            $buyer_trans->wallet = 'awallet2';
        }
        $buyer_trans->wallet = 'awallet2';
        $buyer_trans->type = MemberWalletRecord::TYPE['CREDIT'];
        $buyer_trans->tuid = $queue->sell_uid;
        $buyer_trans->trans_code = TransInfo::$trans_type_to_code['trade-buy'];
        $buyer_trans->amount = $queue->quantity;
        $buyer_trans->balance = $wallet->awallet2;
        $buyer_trans->descr = 'Buy ' . $queue->quantity . ' coins from User ID #' .$queue->sell_uid;
        $buyer_trans->descr_json = json_encode($buy_arr);
        $buyer_trans->status = 10;
        $buyer_trans->save();
    }

    public function sellCoin($queue)
    {
        $wallet = Wallet::where('uid', $queue->sell_uid)->first();
        $setting = TradeSetting::first();

        $maintenance = $queue->quantity * $setting->platform_charge / 100;
        $charity = $queue->quantity * $setting->charity_percent / 100;

        $wallet->cwallet -= ($queue->quantity + $maintenance + $charity);
        $wallet->dowallet += $charity;
        $wallet->save();

        // Return the maintenance fee to system wallet
        $system = Wallet::where('uid', config('trade.trade_acc_id'))->first();
        $system->cwallet += $maintenance;
        $system->save();

        $cwallet_arr = [
            'id' => $queue->sell_id,
            'uid' => $queue->sell_uid,
            'qty' => $queue->quantity + $maintenance + $charity,
            'trans_code' => TransInfo::$trans_type_to_code['trade-sell'],
        ];

        $dowallet_arr = [
            'id' => $queue->sell_id,
            'uid' => $queue->sell_uid,
            'qty' => $charity,
            'trans_code' => TransInfo::$trans_type_to_code['trade-charity'],
        ];

        $system_arr = [
            'uid' => config('trade.trade_acc_id'),
            'qty' => $maintenance,
            'trans_code' => TransInfo::$trans_type_to_code['trade-maintenance'],
        ];

        // Record transaction
        $cwallet_trans = new MemberWalletRecord;
        $cwallet_trans->itype = MemberWalletRecord::ITYPE['USER'];
        $cwallet_trans->iid = $queue->sell_uid;
        $cwallet_trans->uid = $queue->sell_uid;
        $cwallet_trans->wallet = 'cwallet';
        $cwallet_trans->type = MemberWalletRecord::TYPE['DEBIT'];
        $cwallet_trans->tuid = $queue->buy_uid;
        $cwallet_trans->trans_code = TransInfo::$trans_type_to_code['trade-sell'];
        $cwallet_trans->amount = $cwallet_arr['qty'];
        $cwallet_trans->balance = $wallet->cwallet;
        $cwallet_trans->descr = 'Sell ' . $cwallet_arr['qty'] . ' coins to User ID #' .$queue->buy_uid;
        $cwallet_trans->descr_json = json_encode($cwallet_arr);
        $cwallet_trans->status = 10;
        $cwallet_trans->save();

        // Record transaction
        $dowallet_trans = new MemberWalletRecord;
        $dowallet_trans->itype = MemberWalletRecord::ITYPE['USER'];
        $dowallet_trans->iid = $queue->sell_uid;
        $dowallet_trans->uid = $queue->sell_uid;
        $dowallet_trans->wallet = 'dowallet';
        $dowallet_trans->type = MemberWalletRecord::TYPE['CREDIT'];
        $dowallet_trans->trans_code = TransInfo::$trans_type_to_code['trade-charity'];
        $dowallet_trans->amount = $charity;
        $dowallet_trans->balance = $wallet->dowallet;
        $dowallet_trans->descr = 'Gain ' . $charity . ' coins to dowallet from Queue ID #' .$queue->id;
        $dowallet_trans->descr_json = json_encode($dowallet_arr);
        $dowallet_trans->status = 10;
        $dowallet_trans->save();

        $system_trans = new MemberWalletRecord;
        $system_trans->itype = MemberWalletRecord::ITYPE['USER'];
        $system_trans->iid = $queue->sell_uid;
        $system_trans->uid = config('trade.trade_acc_id');
        $system_trans->wallet = 'cwallet';
        $system_trans->type = MemberWalletRecord::TYPE['CREDIT'];
        $system_trans->trans_code = TransInfo::$trans_type_to_code['trade-maintenance'];
        $system_trans->amount = $maintenance;
        $system_trans->balance = $system->cwallet;
        $system_trans->descr = 'Gain ' . $maintenance . ' coins from Queue ID #' .$queue->id;
        $system_trans->descr_json = json_encode($system_arr);
        $system_trans->status = 10;
        $system_trans->save();
    }
}