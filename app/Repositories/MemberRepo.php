<?php

namespace App\Repositories;

use App\Models\Member;
use App\Models\MemberTrade;
use App\Models\MemberRating;
use App\Models\Activation;
use App\Models\Wallet;

class MemberRepo
{
    public function getMemberById($userId) 
    {
        return Member::find($userId);
    }

    public function getUidByUsername($user_name)
    {
        return Member::where('username', $user_name)
            ->value('id');
    }

    public function getOfferRankValueByUserId($userId)
    {
        return Member::where('id', $userId)
            ->value('crank');
    }

    public function getAmtSoldByUserId($userId)
    {
        return MemberTrade::where('uid', $userId)
            ->value('amt_sold');
    }

    public function getWalletsByUserId($userId)
    {
        return Wallet::where('uid', $userId)->first();
    }

    public function getDebtStatusByUserId($userId)
    {
        return Member::where('id', $userId)
            ->value('dstatus');
    }

    public function getTotalInvestmentByUserId($user_id)
    {
        return Member::getUserTotalInvestment($user_id);
    }

    public function getSystemAccount()
    {
        return Member::find(config('trade.trade_acc_id'));
    }

    public function rateMember($trade_id, $rater_uid, $rated_uid, $rate, $comment = null)
    {
        return MemberRating::create([
            'trade_id' => $trade_id,
            'rater_uid' => $rater_uid,
            'rated_uid' => $rated_uid,
            'rate' => $rate,
            'comment' => $comment,
        ]);
    }

    public function getMemberRatedTrade($trade_id, $member_id)
    {
        return MemberRating::where('trade_id', $trade_id)
            ->where('rater_uid', $member_id)
            ->first();
    }

    public function getMemberRating($member_id)
    {
        return MemberRating::where('rated_uid', $member_id)->avg('rate');
    }

    public function getTradableWallet()
    {
        $query = new Member;
        $member_table = $query->getTable();
        $wallet_table = with(new Wallet)->getTable();

        $query->setTable($member_table . " AS m");
        return $query->join($wallet_table . " AS w", "m.id", "=", "w.uid")
            ->where('m.company_status', Member::$company_status_to_code["no"])
            ->where('m.status', Member::$status_to_code['active'])
            ->sum(\DB::raw("w.awallet + w.cwallet"));
    }
}