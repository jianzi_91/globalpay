<?php

namespace App\Interfaces;

interface ManualWithdrawalCode
{
    // Status
    const WITHDRAWAL_PENDING = 0;
    const WITHDRAWAL_COMPLETE = 1;
    const WITHDRAWAL_CANCELLED = 2;
}