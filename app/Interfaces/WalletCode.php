<?php

namespace App\Interfaces;

interface WalletCode
{
    // Wallet Type
    const WALLET_R = 'rwallet'; //注册钱包
    const WALLET_A = 'awallet'; //支付通 [一级商城购物钱包]
    const WALLET_A2 = 'awallet2'; //二级支付通 [二级商城购物钱包]
    const WALLET_1 = 'wallet1'; //金徽章
    const WALLET_2 = 'wallet2'; //红徽章
    const WALLET_C = 'cwallet'; //元宝
    const WALLET_S = 'swallet'; //储蓄通
    const WALLET_DO = 'dowallet'; //交易通
    const WALLET_F = 'fwallet'; //理财通
    const WALLET_D = 'dwallet'; //债务钱包
    const WALLET_I = 'iwallet'; //投资钱包
    const WALLET_SC = 'sc_wallet'; //消费卷

    // Transaction code
    const PURCHASE_WALLET_A_API = 100;
    const PURCHASE_WALLET_A2_API = 101;
    const PURCHASE_WALLET_A_A2_API = 102;
    const PURCHASE_SCWALLET= 103;
    const PURCHASE_CHECKOUT_SCWALLET_API = 104;

    // const PURCHASE_REWARD_X3_FWALLET = 110;
    const PURCHASE_SCWALLET_REWARD_X3_FWALLET = 110;
    const QUICKEN_RELEASE_PURCHASE_REWARD = 111;
    const QUICKEN_RELEASE_REPEAT_REWARD = 112;
    const DAILY_RELEASE_REWARD = 113;
    const QUICKEN_RELEASE_FWALLET_REWARD = 114;

    const WALLET_EDIT_ADMIN_ADD = 200;
    const WALLET_EDIT_ADMIN_SUBTRACT = 201;

    const CASHOUT_MEMBER = 202;
    const CASHOUT_ADMIN_APPROVED = 203;
    const CASHOUT_ADMIN_REJECTED = 204;

    const WALLET_TOPUP = 20;
    const WALLET_TRANSFER_OUT = 22;
    const WALLET_TRANSFER_OTHERS_AWALLET_OUT = 23;
    const WALLET_TRANSFER_OTHERS_AWALLET_IN = 24;
    const WALLET_TRANSFER_SELF_AWALLET = 25;
    const WALLET_TRANSFER_SELF_FWALLET = 26;
}
