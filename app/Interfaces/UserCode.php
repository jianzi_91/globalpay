<?php

namespace App\Interfaces;

interface UserCode
{
    const MEMBER = "100"; 
    const ADMIN  = "900";
    const SYSTEM = "9000";
}