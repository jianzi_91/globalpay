<?php

namespace App\Interfaces;

interface WithdrawalCode
{
    // Status
    const WITHDRAWAL_PENDING = 0;
    const WITHDRAWAL_PROCESSING = 1;
    const WITHDRAWAL_COMPLETE = 2;
    const WITHDRAWAL_CANCELLED = 3;
}