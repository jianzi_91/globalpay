<?php

namespace App\Malls;

use GuzzleHttp\Client;

class MiCool
{
    private $apiToken, $apiKey, $apiSecret, $url;

    public function __construct()
    {
        $this->apiToken = config('mall.token');
        $this->apiKey = config('mall.key');
        $this->apiSecret = config('mall.secret');
        $this->url = config('mall.url');
    }

    public function topup($data)
    {
        $email = $data["email"];
        $amount = $data["amount"];
        $wallet = $data["wallet"];
        $from = $data["from"];
        $ref = $data["ref"];

        $api_url = $this->url."/api/topup/vt_topup?api_token=" . $this->apiToken;

        $post_data = [
            "key" => $this->apiKey,
            "secret" => $this->apiSecret,
            "email" => $email,
            "vtoken" => $amount,
            "wallet" => $wallet,
            "from" => $from,
            "ref" => $ref
        ];

        $post_data["secureHash"] = md5($post_data['email'] . (string) $post_data['vtoken'] . $post_data['from'] . $post_data['wallet'] . 'Qwe&89))(^&');

        // post parameters
        $client = new Client([
            'timeout' => 10, 
            'connect_timeout' => 10, 
            'verify' => false, 
            'allow_redirects' => false,
            // disable http throw client exception
            'http_errors' => false
        ]);
        
        // handling
        $status = false;
        $return_status_code = $content = null;
        try {
            $response = $client->request("POST", $api_url, [
                'form_params' => $post_data
            ]);
            $return_status_code = $response->getStatusCode();
            $content = (string) $response->getBody();
        } catch (Exception $e) {
            //fail ignore
        }

        if ($return_status_code == "200") {
            $return_data = json_decode($content, true);

            if (isset($return_data["status"]) && $return_data["status"]) {
                $status = true;
            }
        }

        return [
            "status" => $status,
            "return_status_code" => $return_status_code,
            "return_data" => $content,
            "post_data" => $post_data,
        ];
    }

    public function confirm($data)
    {
        $ref = $data["ref"];

        $api_url = $this->url."/api/topup/confirm?api_token=" . $this->apiToken;

        $post_data = [
            "key" => $this->apiKey,
            "secret" => $this->apiSecret,
            "ref" => $ref
        ];

        // post parameters
        $client = new Client([
            'timeout' => 10, 
            'connect_timeout' => 10, 
            'verify' => false, 
            'allow_redirects' => false,
            // disable http throw client exception
            'http_errors' => false
        ]);
        
        // handling
        $status = false;
        $return_status_code = $content = null;
        try {
            $response = $client->request("POST", $api_url, [
                'form_params' => $post_data
            ]);
            $content = (string) $response->getBody();
            $return_status_code = $response->getStatusCode();
        } catch (Exception $e) {
            //fail ignore
        }

        if ($return_status_code == "200") {
            $return_data = json_decode($content, true);

            if (isset($return_data["status"]) && $return_data["status"]) {
                $status = true;
            }
        }

        return [
            "status" => $status,
            "return_status_code" => $return_status_code,
            "return_data" => $content,
            "post_data" => $post_data,
        ];
    }
}
