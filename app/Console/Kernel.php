<?php

namespace App\Console;

use DB;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Models\AdditionalRelease;
use App\Models\DownlineCache;

use App\Trades\TradeService;

use App\Utils\DateTimeTool;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        
        // $datetime = DateTimeTool::operationDateTime();

        // if ($datetime >= "2018-08-08") {
            // Commission Process
            // $schedule->call(function() {
            //     \App::call('App\Http\Controllers\Cron\CronController@runDailyCommTasks');
            // // })->everyMinute();
            // })->dailyAt('16:02');
        // }

        // $schedule->call(function(){
        //     \App::call('App\Http\Controllers\Cron\CronController@invalidateExpiredTrades');
        // })
        // ->everyMinute()
        // ->name('invalidate_expired_trades')
        // ->withoutOverlapping(5);

        // trade price daily update
        // $schedule->call(function() {
        //     \App::call('App\Http\Controllers\Cron\CronController@updateTradeSetting');
        // // })->everyMinute();
        // })->dailyAt('16:00');

        $schedule->call(function(){
            \App::call('App\Http\Controllers\Cron\CronController@fwalletRelease');
            \App::call('App\Http\Controllers\Cron\CronController@recalculate_rank');
            // \App::call('App\Http\Controllers\Cron\CronController@swalletRelease');

            // DB::table('mlm_member_additional_release')->update([
            //     'saving_amount' => 0,
            //     'spending_amount' => 0, 
            // ]);
            
        })
        ->dailyAt('16:00')
        ->name('wallet-release')
        ->withoutOverlapping(5);
    }
}
