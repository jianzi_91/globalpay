<?php

namespace App\Jobs;

use App\Models\Member;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SpendWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $spending;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, $spending)
    {
        $this->member = $member;
        $this->spending = $spending;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $gen = 1;

        while ($this->member->up != 0 && $gen < 11) {

            switch (true) {
                case ($gen <= 10 && $gen >= 6):
                    $release = $this->spending * 0.001;
                    break;
                case ($gen <= 5 && $gen >= 3):
                    $release = $this->spending * 0.005;
                    break;
                case ($gen == 2):
                    $release = $this->spending * 0.03;
                    break;
                case ($gen == 1):
                    // $release = $this->spending * 0.05;
                    $release = $this->spending * 0.5;
                    break;
                default:
                    $release = $this->spending * 0;
                    break;
            }

            $this->member = $this->member->upline;

            $additional_release = $this->member->additionalRelease()->firstOrCreate([
                'uid' => $this->member->id,
            ]);
            
            $additional_release = $this->member->additionalRelease;
            $additional_release->spending_amount += $release;
            $additional_release->spending_total += $release;
            $additional_release->save();

            $gen++;
        }
    }
}
