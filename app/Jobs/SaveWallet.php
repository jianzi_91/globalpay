<?php

namespace App\Jobs;

use DB;
use Exception;

use App\Models\AdditionalRelease;
use App\Models\Member;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SaveWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $saving;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, $saving)
    {
        $this->member = $member;
        $this->saving = $saving;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $gen = 1;

        while ($this->member->up != 0 || $gen < 11) {

            switch (true) {
                case ($gen <= 10 && $gen >= 6):
                    $release = $this->saving * 0.001;
                    break;
                case ($gen <= 5 && $gen >= 3):
                    $release = $this->saving * 0.005;
                    break;
                case ($gen == 2):
                    $release = $this->saving * 0.03;
                    break;
                case ($gen == 1):
                    $release = $this->saving * 0.05;
                    break;
                default:
                    $release = $this->saving * 0;
                    break;
            }

            $this->member = $this->member->upline;

            $additional_release = $this->member->additionalRelease()->firstOrCreate([
                'uid' => $this->member->id,
            ]);
            
            $additional_release = $this->member->additionalRelease;
            $additional_release->saving_amount += $release;
            $additional_release->saving_total += $release;
            $additional_release->save();

            $gen++;
        }

    }
}
