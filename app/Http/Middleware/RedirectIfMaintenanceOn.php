<?php

namespace App\Http\Middleware;

use Closure;

use App\Utils\DateTimeTool;

class RedirectIfMaintenanceOn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $datetime = DateTimeTool::operationDateTime();
        
        $hour = date("H", strtotime($datetime));
        $ip = request()->ip();
        $allow_ip_list = [];//"127.0.0.1"
        
        if (config('app.env') != 'local') {
            if ($datetime>="2016-12-09" && $hour>=0 && $hour<1 && !in_array($ip, $allow_ip_list)) {
                $start = date("Y-m-d 00:00:00", strtotime($datetime));
                $end = date("Y-m-d 01:00:00", strtotime($datetime));

                if (auth($guard)->check()) {
                    auth($guard)->logout();
                }
                
                return response()->view('errors.custom.maintenance', ["__info"=>["start"=>$start, "end"=>$end]]);
            }
        }

        return $next($request);
    }
}
