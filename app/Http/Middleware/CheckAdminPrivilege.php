<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminPrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $privilege = "")
	{
        $guard = "admins";
        $admin_id = auth($guard)->id();
        $user = auth($guard)->user();
        if ($privilege && !$user->can('admin-privilege', $privilege)) {
            return response('Unauthorized.', 401);
        }
        
		return $next($request);
	}
}