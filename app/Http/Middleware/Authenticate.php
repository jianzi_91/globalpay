<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Utils\Tool;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } 
            else {
                return redirect()->guest('login');
            }
        }

        // check user status
        $user = Auth::guard($guard)->user();
        if ($user->status != $user::$status_to_code["active"]) {
            Auth::guard($guard)->logout();
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } 
            else {
                return redirect()->guest('login');
            }
        }
        
        // if ($guard == "web") {
        //     // force change password
        //     if (session()->get("force_change_password")) {
        //         if (!request()->is("member/profile") && !request()->is("member/logout")) {
        //             $errmsg = [];
        //             if ($user->statuses->pw_change_status != 10) {
        //                 $errmsg[] = trans("general.page.user.profile.errmsg.password-change-required", ["field"=>trans("field.user.password")]);
        //             }
        //             if ($user->statuses->pw2_change_status != 10) {
        //                 $errmsg[] = trans("general.page.user.profile.errmsg.password-change-required", ["field"=>trans("field.user.password2")]);
        //             }
        //             return redirect("member/profile")->withErrors(collect($errmsg));
        //         }
        //     }
        // }
        if ($guard == "web") {
            // force mobile verify
            if ($user->memberStatus->mobile_status != 10) {
                if (!request()->is("member/mobile/verify") && !request()->is("member/logout")) {
                        return redirect("member/mobile/verify");
                }
            }
        }

        return $next($request);
    }
}
