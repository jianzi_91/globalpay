<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
	{
        $lang = session()->get('lang');
        if (!$lang) {
            if ($guard == "web") {
                $lang = "zh-cn";
            }
            else {
                $lang = config('app.locale');
            }
        }
		app()->setLocale($lang);
		return $next($request);
	}
}