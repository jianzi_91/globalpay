<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

use App\Utils\DateTimeTool;

class RedirectIfMarketClosed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dt = Carbon::parse(DateTimeTool::operationDateTime());

        if ($dt->hour >= 10 && $dt->hour < 19) {
            return $next($request);
        }

        return redirect()->route('buy-lobby');
    }
}
