<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticatePerIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($guard == "web") {
                $last_ips = explode(",", Auth::guard($guard)->user()->last_ip);
                $request_ips = explode(",", request()->ip());
                
                if (count(array_intersect($last_ips, $request_ips)) <= 0) {
                    auth($guard)->logout();
                    if ($request->ajax() || $request->wantsJson()) {
                        return response('Unauthorized.', 401);
                    } else {
                        return redirect()->guest('login');
                    }
                }
            }
        }

        return $next($request);
    }
}
