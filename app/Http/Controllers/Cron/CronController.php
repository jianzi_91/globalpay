<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;

use App\Models\AdditionalRelease;
use App\Models\Cron;
use App\Models\Commission;
use App\Models\ErrorLog;
use App\Models\Member;
use App\Models\MemberRelationship;
use App\Models\MemberWalletRecord;
use App\Models\TransactionLog;
use App\Models\Wallet;

// use App\Trades\TradeService;

use App\Utils\DateTimeTool;

use Carbon\Carbon;
use DB;
use Exception;
use Log;

class CronController extends Controller
{
    public function __construct(/*TradeService $tradeService*/)
    {
        // $this->tradeService = $tradeService;
    }

    // public function runDailyCommTasks($date = null)
    // {
    //     //set unlimited execution time daily task running
    //     ini_set('max_execution_time', 0);
        
    //     if ($date != date("Y-m-d", strtotime($date))) {
    //         $datetime = DateTimeTool::operationDateTime();
            
    //         //if cron runtime is next day: 00:00 - 23:59
    //         $date = strtotime($datetime. " - 1 day");
    //         //dd(date("Y-m-d H:i:s", $date));
    //         $date = date("Y-m-d", $date);
    //     }
        
    //     $cron = new Cron();
    //     $cron->copyWallet($date);
    //     $cron->unfreezeWallet($date);
    //     $cron->calcActiveBonus($date);
    //     $cron->calcPassiveBonus($date);
    //     $cron->distributeActiveBonus($date);
    //     $cron->distributePassiveBonus($date);
    // }

    // public function invalidateExpiredTrades()
    // {
    //     $trades = $this->tradeService->getAllPendingQueues();

    //     foreach($trades as $trade) {
            
    //         try {
    //             DB::beginTransaction();

    //             if (Carbon::now()->gt($trade->expired_at)){

    //                 $queue = $this->tradeService->voidActiveQueue($trade->id);

    //                 $sell = $this->tradeService->getTradeSellById($trade->sell_id);
                    
    //                 if ($sell->type == 100) {
    //                     $sell->update([
    //                         'status' => 'X',
    //                         'cancelled_at' => Carbon::now(),
    //                     ]);
    //                 }

    //                 $buy = $this->tradeService->getTradeBuyById($trade->buy_id);

    //                 if ($buy->type == 100) {
    //                     $buy->update([
    //                         'status' => 'X',
    //                         'cancelled_at' => Carbon::now(),
    //                     ]);
    //                 }
    //             }

    //             DB::commit();
    //         } catch (Exception $e) {
    //             DB::rollback();

    //             Log::error($e->getMessage());
    //         }
            
    //     }   
    // }

    // public function updateTradeSetting()
    // {
    //     $this->tradeService->updateTradePrice();
    //     $this->tradeService->updateTradeQuantityMax();
    // }

    public function fwalletRelease()
    {
        foreach (Member::active()->cursor() as $member){

            try {
                DB::beginTransaction();

                $member_fwallet = $member->wallet->fwallet;
                // $additionalRelease = $member->additionalRelease;

                if (($member_fwallet > 0) && ($member->user_type != 2)) {
                    $release = $member_fwallet * config('rates.release_fwallet');

                    try {
                        DB::beginTransaction();
            
                        Wallet::adjust([
                            'uid' => $member->id,
                            'type' => 'normal-release-0.02',
                            'trans_code' => Wallet::DAILY_RELEASE_REWARD,
                            'creator_user_type' => $member::$user_code,
                            'creator_uid' => $member->id,
                            'f_amt' => isset($release) ? -$release : 0,
                            'do_amt' => isset($release) ? $release : 0
                        ]);
            
                        DB::commit();
                    }
                    catch (Exception $e) {
                        DB::rollBack();
                    }

/* inactive due to this part moved to after purchase
                    if ($additionalRelease) {
                        $release += $member->additionalRelease->spending_amount;

                        if ($release >= $member_fwallet) {
                            $release = $member_fwallet;
                        }
                    }
*/
/*
                    $fwallet_descr = "User ID #" . $member->id . " finance wallet released " . $release . " points for daily releasal.";
                    $dowallet_descr = "User ID #" . $member->id . " gold wallet received " . $release . " points from daily releasal.";

                    $fwallet_descr_json = [
                        'uid' => $member->id,
                        'username' => $member->username,
                        'amount' => $release,
                        'date' => Carbon::now(),
                    ];

                    $dowallet_descr_json = [
                        'uid' => $member->id,
                        'username' => $member->username,
                        'amount' => $release,
                        'date' => Carbon::now(),
                    ];

                    $member->walletUpdate($member->id, 'fwallet', $release, 'd', 2, $fwallet_descr, $fwallet_descr_json, 1005);
                    $member->walletUpdate($member->id, 'dowallet', $release, 'c', 2, $dowallet_descr, $dowallet_descr_json, 1006);
*/

                }

                DB::commit();
            }
            catch (Exception $e) {

                DB::rollback();

                ErrorLog::create([
                    'sp_name' => 'fwallet_release',
                    'description' => $release . ' points is not release due to ' . $e->getMessage() . '(Member ID:' . $member->id .')',
                ]);
            }
            
        }
    }

    public function recalculate_rank()
    {
        foreach (Member::active()->cursor() as $member){

            $direct_sponsor_count = MemberRelationship::where('user_id', $member->id)->where('level_diff', 1)->get()->count();

            $total_repeat = TransactionLog::where('uid', $member->id)->where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->sum('f_amt');

            $member_fwallet = $member->wallet->fwallet;

            switch ($member_fwallet) {
                case ($member_fwallet >= 100000 && $direct_sponsor_count >= 6 && $total_repeat >= 100000):
                    $all_repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->sum('f_amt');
                    $reward = $all_repeat_amount * 0.09;
                    $member->rank = 100000;
                    break;
                case ($member_fwallet >= 50000 && $direct_sponsor_count >= 6 && $total_repeat >= 50000):
                    $member->rank = 50000;
                    break;
                case ($member_fwallet >= 10000 && $direct_sponsor_count >= 6 && $total_repeat >= 10000):
                    $member->rank = 10000;
                    break;
                case ($member_fwallet >= 5000 && $direct_sponsor_count >= 6 && $total_repeat >= 5000):
                    $member->rank = 5000;
                    break;
                case ($member_fwallet >= 1000 && $direct_sponsor_count >= 6 && $total_repeat >= 1000):
                    $member->rank = 1000;
                    break;
                default:
                    $member->rank = 0;
                    break;
            }

            $member->save();

            $this->quicken_release_fwallet($member);

            // $normal_user_total_repeat_amount = Member::where('rank', 100000)->get();
            // dump($normal_user_total_repeat_amount);
            
        }
    }

    public function quicken_release_fwallet($member)
    {
        $rank = $member->rank;
        $reward = null;

        switch ($rank) {
            case 100000:
                $members = Member::whereIn('rank', array(0, 1000, 5000, 10000, 50000))->get()->pluck('id');
                $repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->whereIn('uid', $members->toArray())->sum('f_amt');
                $reward = $repeat_amount * 0.09;
                break;
            case 50000:
                $members = Member::whereIn('rank', array(0, 1000, 5000, 10000))->get()->pluck('id');
                $repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->whereIn('uid', $members->toArray())->sum('f_amt');
                $reward = $repeat_amount * 0.08;
                break;
            case 10000:
                $members = Member::whereIn('rank', array(0, 1000, 5000))->get()->pluck('id');
                $repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->whereIn('uid', $members->toArray())->sum('f_amt');
                $reward = $repeat_amount * 0.07;
                break;
            case 5000:
                $members = Member::whereIn('rank', array(0, 1000))->get()->pluck('id');
                $repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->whereIn('uid', $members->toArray())->sum('f_amt');
                $reward = $repeat_amount * 0.06;
                break;
            case 1000:
                $members = Member::whereIn('rank', array(0))->get()->pluck('id');
                $repeat_amount = TransactionLog::where('trans_code', Wallet::WALLET_TRANSFER_SELF_FWALLET)->whereIn('uid', $members->toArray())->sum('f_amt');
                $reward = $repeat_amount * 0.05;
                break;
        }

        if ($rank != 0) {
            try {
                DB::beginTransaction();
    
                Wallet::adjust([
                    'uid' => $member->id,
                    'type' => 'quciken-release-fwallet-reward',
                    'trans_code' => Wallet::QUICKEN_RELEASE_FWALLET_REWARD,
                    'creator_user_type' => $member::$user_code,
                    'creator_uid' => $member->id,
                    'f_amt' => isset($reward) ? -$reward : 0,
                    'do_amt' => isset($reward) ? $reward : 0
                ]);
    
                DB::commit();
            }
            catch (Exception $e) {
                DB::rollBack();
            }
        }

    }

    public function swalletRelease()
    {
        foreach (Member::active()->cursor() as $member){

            try {
                DB::beginTransaction();

                $member_swallet = $member->wallet->swallet;
                $additionalRelease = $member->additionalRelease;
                
                if ($member_swallet > 0) {
                    $release = $member_swallet * config('rates.release_swallet');

                    if ($additionalRelease) {
                        $release += $member->additionalRelease->saving_amount;

                        if ($release >= $ $member_swallet) {
                            $release = $member_swallet;
                        }
                    }

                    $swallet_descr = "User ID #" . $member->id . " saving wallet released " . $release . " points for daily releasal.";
                    $dowallet_descr = "User ID #" . $member->id . " gold wallet received " . $release . " points from daily releasal.";

                    $swallet_descr_json = [
                        'uid' => $member->id,
                        'username' => $member->username,
                        'amount' => $release,
                        'date' => Carbon::now(),
                    ];

                    $dowallet_descr_json = [
                        'uid' => $member->id,
                        'username' => $member->username,
                        'amount' => $release,
                        'date' => Carbon::now(),
                    ];

                    $member->walletUpdate($member->id, 'swallet', $release, 'd', 2, $swallet_descr, $swallet_descr_json, 1005);
                    $member->walletUpdate($member->id, 'dowallet', $release, 'c', 2, $dowallet_descr, $dowallet_descr_json, 1006);

                }

                DB::commit();
            }
            catch (Exception $e) {

                DB::rollback();

                ErrorLog::create([
                    'sp_name' => 'swallet_release',
                    'description' => $release . ' points is not release due to ' . $e->getMessage() . '(Member ID:' . $member->id .')',
                ]);
            }
            
        }
    }
}
