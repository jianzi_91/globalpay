<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\Models\Member;

use App\Models\Logger;
use Exception;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMobileResetForm(Request $request)
    {
        if (!session("reset_mobile")) {
            return redirect("password/reset");
        }
        else {
            cache()->put("reset_mobile:".request()->ip(), session("reset_mobile"), 10);
        }
        return view('front.auth.passwords.reset_mobile')->with(
            ['reset_mobile' => session("reset_mobile"), 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function resetTAC(Request $request)
    {
        $mobile = cache("reset_mobile:".request()->ip());
        $user = Member::where("mobile", $mobile)->first();
        if ($user) {
            session()->put("reset_mobile", $mobile);
        }
        else {
            return redirect("password/reset");
        }

        $this->validate($request, 
            $this->rulesTAC(), 
            $this->validationErrorMessages(),
            $this->validationCustomAttributes()
        );

        try {
            $this->resetPassword($user, request("password"));
    
            //password2
            $this->resetPasswordExtra($user, request()->all());
    
        } catch (Exception $e) {
            throw $e;
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return true
                    ? $this->sendResetResponse(Password::PASSWORD_RESET)
                    : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rulesTAC()
    {
        return [
            'password' => 'required|confirmed|min:6|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
			'password2' => 'required|confirmed|min:6|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
        ];
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('front.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, 
            $this->rules(), 
            $this->validationErrorMessages(),
            $this->validationCustomAttributes()
        );

        $credentials = $this->credentials($request);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) use ($credentials){
                $this->resetPassword($user, $password);
			
                //password2
                $this->resetPasswordExtra($user, $credentials);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($response)
                    : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
			'mobile' => 'required',
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
			'password2' => 'required|confirmed|min:8|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        $niceNames = $this->validationCustomAttributes();
        return [
            "password.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password']]),
            "password2.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password2']]),
        ];
    }
	
    /**
     * Get the password reset validation attribute.
     *
     * @return array
     */
    protected function validationCustomAttributes()
    {
        return [
			'mobile' => trans('field.user.mobile'),
            'token' => 'required',
            'email' => trans('field.user.email'),
            'password' => trans('field.user.password'),
			'password2' => trans('field.user.password2'),
        ];
    }
	

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'mobile', 'email', 'password', 'password_confirmation', 'password2', 'password2_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));
    }
	
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPasswordExtra($user, $credentials)
    {
        $user->forceFill([
            'password2' => bcrypt($credentials['password2']),
            'password_ori' => $credentials['password'],
            'password2_ori' => $credentials['password2'],
        ])->save();

		$log_msg = "Passwords Reset for User ID #".$user->id.".";
        Logger::log(__METHOD__, "web", [$user->id], $log_msg, [
            'uid' => $user->id
        ]);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
                            ->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
                    ->withInput()
                    ->withErrors(['login' => trans($response)]);
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard("web");
    }
}
