<?php

namespace App\Http\Controllers\Front\Auth;

use DB;
use Exception;
use Log;
use Mail;
use Request;

use App\Mail\UserRegistered;

use App\Models\Member;
use App\Models\MemberRelationship;
use App\Models\Logger;
use App\Models\SessionKeyValue;

use App\Http\Controllers\Controller;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

use App\Repositories\CountryRepo;

use App\Utils\Tool;
use App\Utils\DateTimeTool;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $user_table = with(new Member)->getTable();
        $niceNames = [
            'name' => trans('field.user.name'),
            'email' => trans('field.user.email'),
            'password' => trans('field.user.password'),
            'password2' => trans('field.user.password2'),
            'mobile' => trans('field.user.mobile'),
            'nid' => trans('field.user.nid'),
            'ref' => trans('field.user.ref'),
        ];

        $niceMessages = [
            "mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
        ];

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'nid' => 'required|max:30',
            'email' => 'required|string|email|max:255',
            // 'mobile' => 'required|min:1|digits_between:6,100|regex:/^[1-9]([0-9]+)$/',
            'mobile' => 'required|min:1|digits_between:6,100|regex:/^[0-9]([0-9]+)$/',
            'password' => 'required|string|min:6|confirmed|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
            'password2' => 'required|min:8|max:255|confirmed|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
            'ref' => 'nullable|exists:'.$user_table.',username,primary_id,0',
        ], $niceMessages, $niceNames);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Member
     */
    protected function create(array $data)
    {
        //initiator
        $initiator = Tool::getInitiator();
        
        if ($data['sub_acc'] == true) {
            $data['sub_id'] = Member::where('mobile', $data['mobile'])->count();
        }

        try {
            DB::beginTransaction();
    
            //get user id
            $ref = Member::where('username', '=', $data['ref'])->first();
            // $data['ref'] = $ref ? $ref->id : Member::$default_ref;
    
            $member = Member::create([
                'itype' => Member::$user_code,
                'iid' => $ref->id,
                'editor_type' => Member::$user_code,
                'editor_id' => $ref->id,
                'username' => Member::genUniqueUsername(),
                'name' => $data['name'],
                'nid' => $data['nid'], 
                'email' => $data['email'],
                'mobile' => $data['mobile'],
                'sub_id' => $data['sub_id'],
                'country' => $data['country'],
                'password' => bcrypt($data['password']),
                'password2' => bcrypt($data['password2']),
                'password_ori' => $data['password'],
                'password2_ori' => $data['password2'],
                'ref' => $ref->id,
                'up' => $ref->id,
                'up_pos' => 1,
                'locale' => $data['locale'],
                'primary_acc' => Member::$primary_to_code['yes'], //take active status
                'status' => Member::$status_to_code['active'],
            ]);
    
            //create dependencies for user
            $member->wallet()->create();
            $member->memberStatus()->create();
            
            MemberRelationship::populate();

            $log_msg = "User Created for User ID #".$member->id.".";
            Logger::logWithoutGuard(__METHOD__, [$member->id], $log_msg, [
                'data' => [
                    'name' => $data['name'], 
                    'email' => $data['email'],  
                    'nid' => $data['nid'], 
                    'status' => Member::$status_to_code['active'], 
                    'primary_acc' => Member::$primary_to_code['yes'], 
                    'mobile' => $data['mobile'], 
                    'ref' => $data['ref']
                ], 
                'uid'=>$member->id
            ]);
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
        DB::commit();
        
        return $member;
    }

    public function register()
    {
        $this->validator(request()->all())->validate();

        event(new Registered($user = $this->create(request()->all())));

        try {

            $mail = Mail::to($user->email)
            ->send(new UserRegistered($user));

        }
        catch(Exception $e) {
            
            Log::error($e->getMessage());
            
        }
        
        // $this->guard()->login($user);

        return $this->registered(request(), $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $data = Request::only('referrer-code');

        if(!isset($data['referrer-code'])) {
            abort(404);
        }
        
        $referrer = Member::where('username', $data['referrer-code'])->first();

        if ($referrer) { 

            if ($data['referrer-code'] !== $referrer->username) {
                abort(404);
            }

        }
        else {
            abort(404);
        }

        $session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('create_nonce');
        $nonce = str_random(60);
        $session_key->putValue('create_nonce', $nonce);

        $fields['country']['options'] = CountryRepo::all_member_selectable_country();
        $fields['phone_code']['options'] = CountryRepo::all_member_selectable_phone_code();
        $fields['locale']['options'] = ["en"=>"English", "zh-cn"=>"中文"];
        $fields['__nonce']['value'] = $nonce;

        return view('front.auth.register', [
            "__fields" => $fields,
            "referrer" => $referrer,
        ]);
    }
}
