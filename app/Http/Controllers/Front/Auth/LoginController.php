<?php

namespace App\Http\Controllers\Front\Auth;

use Auth;
use Validator;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Utils\DateTimeTool;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'member/home';

    /**
     * Where to redirect users after logout
     *
     * @var string
     */
	protected $redirectAfterLogout = 'login';

    /**
     * Change user username field.
     *
     * @var string
     */
    protected $guard = 'web';
    
    protected $maxAttempts = 3; // Amount of bad attempts user can make
    protected $decayMinutes = 15; // Time for which user is going to be blocked in minutes
 
	
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => [
            'logout',
            'showRedirectLogin',
            'initRedirectLogin'
        ]]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard($this->guard);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect($this->redirectAfterLogout);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // dump(request()->ip());
        return view('front.auth.login');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * @see Illuminate\Foundation\Auth\AuthenticatesUsers@validateLogin
     */
    protected function validateLogin(Request $request)
    {
        $niceNames = [
            $this->username()  => trans("field.user.mobile"),
            'password' => trans('field.user.password'),
            '_captcha' => trans('general.field.captcha'),
        ];

       // Session::put('captcha_code', 'somthing');
        $v = Validator::make($request->all(), [
            $this->username() => 'required|string', 
            'password' => 'required|string', 
            '_captcha' => 'required|string',
            'terms' => 'accepted',
        ], [
            'terms.accepted' => trans("general.page.public.login.errmsg.terms-check")
        ], $niceNames);

        if ($v->fails()) {
            throw ValidationException::withMessages(
                $v->errors()->toArray()
            );
        }

        //custom validation
        $v->after(function($validator) use ($niceNames){
            $data = $validator->getData();
            
            //check captcha
            if ($data['_captcha'] && strtolower($data['_captcha']) != strtolower(session()->get('captcha_code'))) {
                $validator->errors()->add('_captcha', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['_captcha']]));
            }
            //dd($validator);
        });

        if ($v->fails()) {
            throw ValidationException::withMessages(
                $v->errors()->toArray()
            );
        }
    }

    /**
     * Handle a init login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initLogin(Request $request)
    {
        //remove remember me
        if ($request->has("remember")) {
            $request->request->remove("remember");
        }
        
        return $this->login($request);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        if (str_contains($request->login,'-')) {
            list($mobile, $sub_id) = explode('-',$request->login);
            
            $sub_user = Member::where('mobile',$mobile)
                ->where('sub_id', $sub_id)
                ->first();

            if ($sub_user) {

                if (Hash::check($request->password, $sub_user->password)) {

                    Auth::login($sub_user);

                    return $this->sendLoginResponse($request);
                }
                
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, Member $user)
    {
        
        //if user is authenticated
        $v = Validator::make($request->all(), [
            $this->username() => 'required', 
        ]);

        //custom validation
        $v->after(function($validator) use ($user) {
            //check captcha
            if ($user->status == Member::$status_to_code['active']) {
            }
            elseif($user->status == Member::$status_to_code['suspended']){
                $validator->errors()->add($this->username(), trans('general.page.public.login.errmsg.account-suspended'));
            }
            elseif($user->status == Member::$status_to_code['terminated']){
                $validator->errors()->add($this->username(), trans('general.page.public.login.errmsg.account-terminated'));
            }
            //dd($validator);
        });

        if ($v->fails()) {
            $this->guard()->logout();
            throw ValidationException::withMessages(
                $v->errors()->toArray()
            );
        }
        
        try {
            $user = $this->guard()->user();
            // if ($user->statuses->pw_change_status === 0 || $user->statuses->pw2_change_status === 0) {
            //     session()->put("force_change_password", true);
            // }
            // else {
            //     session()->forget("force_change_password");
            //     session()->forget("epassword_nonce");
            // }
            $user->last_ip = request()->ip();
            $user->login_at2 = $user->login_at;
            $user->login_at = DateTimeTool::systemDateTime();
            $user->save();
        } catch (\Exception $e) {
            //ignore error
            throw $e;
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $login = $request->get($this->username());
        $data["mobile"] = $login;
        
        return $data + $request->only('password');
    }
}
