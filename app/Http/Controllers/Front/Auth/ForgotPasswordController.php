<?php

namespace App\Http\Controllers\Front\Auth;

use Validator;

use App\Models\Member;
use App\Models\Code;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('front.auth.passwords.email');
    }

    /**
     * Display the form to request a password reset tac.
     *
     * @return \Illuminate\Http\Response
     */
    public function showTACRequestForm()
    {
        return view('front.auth.passwords.mobile');
    }

    /**
     * Redirect to reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function redirectResetForm(Request $request)
    {
        $niceNames = [
            'login' => trans('field.user.mobile'),
            'verification_code' => trans('field.code.verification_code'),
        ];
        $this->validate(
            $request, 
            ['login' => 'required',
            'verification_code' => 'required',], 
            [], 
            $niceNames
        );
        
        $user_table = with(new Member)->getTable();
        $this->validate(
            $request, 
            [
                'login' => 'exists:'.$user_table.',mobile,primary_id,0'
            ], 
            [], 
            $niceNames
        );
        
        // check tac
        $code_verify = Code::checkTac(request("verification_code"), request("login"), "reset_password");
        if (!$code_verify["status"]) {
            $message = "";
            if ($code_verify["msg"] == "not found") {
                $message = trans("sms_tac.error.not_found", ["attribute" => trans("field.code.verification_code")]);
            }
            elseif ($code_verify["msg"] == "not matched") {
                $message = trans("sms_tac.error.not_matched", ["attribute" => trans("field.code.verification_code")]);
            }
            $validator = validator([], []);
            $validator->after(function ($validator) use ($message) {
                $validator->errors()->add("verification_code", $message);
            });
        
            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
        }

        return redirect("password/reset/mobile")->with(["reset_mobile" => request("login")]);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $niceNames = ['login' => trans('field.user.mobile')." / ".trans('field.user.email')];
        $this->validate(
            $request, 
            ['login' => 'required'], 
            [], 
            $niceNames
        );
        
        $user_table = "mlm_members";
        $v_email = Validator::make($request->only('login'), ["login"=>"email"]);
        if ($v_email->fails()) {            
            $this->validate(
                $request, 
                ['login' => 'exists:'.$user_table.',mobile,primary_id,0'], 
                [], 
                $niceNames
            );
        }

        //check username or email
        $v_email = Validator::make($request->only('login'), ["login"=>"email"]);
        if ($v_email->fails()) {
            $data = ["username" => $request->get('login')];
        }
        else {
            $data = ["email" => $request->get('login')];
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $data, function (Message $message) { 
                $message->subject($this->getEmailSubject()); 
                $message->replyTo("no-reply@dongli88.com");
            }
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse($response)
    {
        return back()->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['login' => trans($response)]
        );
    }
}
