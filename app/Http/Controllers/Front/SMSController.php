<?php

namespace App\Http\Controllers\Front;

use DB;
use Exception;

use App\Http\Controllers\Controller;
use App\Models\Code;

class SMSController extends Controller
{
    public function sendTac() 
    {
        if (request()->get('lang')) {
            app()->setLocale(request()->get('lang'));
        }
        $result = Code::sendTac(request("mobile"), request("country"), request("action"));
        return response()->json($result);
    }
}
