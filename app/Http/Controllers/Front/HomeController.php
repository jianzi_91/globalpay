<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('login');
    }

    /**
     * Show the application terms and conditions.
     *
     * @return \Illuminate\Http\Response
     */
    public function showTNC()
    {
        return view('front.tnc');
    }

    public function setlocale()
    {
        Session::forget('lang');
        Session::put('lang', request()->get("lang"));
        return 'success';
    }

    public function locale($lang)
    {
        Session::forget('lang');
        Session::put('lang', $lang);

        return back();
    }
}
