<?php

namespace App\Http\Controllers\Front;

use DB;

use App\Models\Member;
use App\Models\Activation;

use App\Repositories\MemberRepo;

use App\Utils\StringTool;
use App\Utils\DateTimeTool;

class AjaxController extends Controller
{	
	public function getUserInfo($type)
    {
        //check login status
        $user = auth('admins')->user();
		if (!$user) {
		    $user = auth("web")->user();
            $guard_admin = false;
		}
        else {
            $guard_admin = true;
        }

		// if (!$user) {
		// 	return response()->json(['status'=>false, 'msg'=> trans('ajax.errmsg.logout')]);
		// }
        $data = request()->all();

        //check required parameter
        if (!isset($data['username']) || trim($data['username'])=='') {
			return response()->json(['status'=>false, 'msg'=> trans('ajax.errmsg.username-required')]);
		}

        //check username exist
        $user = Member::where('mobile', $data['username'])->orWhere('username', $data['username'])->first();
        $sub_user = null;
        
        if (!$user) {

            list($mobile, $sub_id) = explode('-',$data['username']);

            $sub_user = Member::where('mobile',$mobile)
                ->where('sub_id', $sub_id)
                ->first();

            if (!$sub_user) {
                return response()->json(['status'=>false, 'msg'=> trans('ajax.errmsg.user-not-found')]);
            }
			
		}

		switch ($type) {
			case 'wallet':
                //no sub account check
                if (!$user && !$sub_user) {
                    return response()->json(['status'=>false, 'msg'=> trans('ajax.errmsg.user-not-found')]);
                }

                if ($sub_user) {
                    $user = $sub_user;
                }

                $user_wallet = $user->wallet;
                
                if ($user_wallet) {
                    $msg = $user->username.' ('.StringTool::censored($user->name).') <br/>';
                    $msg .= '<br>'.trans('general.wallet.awallet').': '.$this->number_format($user_wallet->awallet2, 4, $guard_admin);
                    $msg .= '<br>'.trans('general.wallet.awallet2').': '.$this->number_format($user_wallet->awallet, 4, $guard_admin);
                    $msg .= '<br>'.trans('general.wallet.fwallet').': '.$this->number_format($user_wallet->fwallet, 4, $guard_admin);
                    $msg .= '<br>'.trans('general.wallet.swallet').': '.$this->number_format($user_wallet->swallet, 4, $guard_admin);
                    $msg .= '<br>'.trans('general.wallet.dowallet').': '.$this->number_format($user_wallet->dowallet, 4, $guard_admin);

                    return response()->json(['status'=>true, 'msg'=> $msg]);
                }
				break;
            case 'sponsor-genealogy':
                // use in sponsor genealogy
                $result = DB::table($user->getTable().' AS upline')->where('upline.ref', $user->id)->leftJoin($user->getTable().' AS downline', 'upline.id', '=', 'downline.ref')->groupBy('upline.id')->orderBy('upline.created_at')->select('upline.username', 'upline.rank', 'upline.crank', 'upline.created_at', 'downline.id')->get();
                $userlist = [];
                foreach ($result as $key=>$value) {
                    array_push($userlist, ['username'=>$value->username, 'rank'=>max($value->rank, $value->crank), 'rank-title'=>trans("general.user.rank.title.".max($value->rank, $value->crank)), "created_at_op-date-only"=>DateTimeTool::systemToOperationDateTime($value->created_at, "Y-m-d"), '__has'=>($value->id?true:false)]);
                }
                
    			return response()->json(['status'=>true, 'msg'=> $userlist]);
            default: 

                if ($user) { 
                    $msg = $user->username.' ('.StringTool::censored($user->name).')';
                }

                if ($sub_user) {
                    $msg = $sub_user->username.' ('.StringTool::censored($sub_user->name).')';
                }
                
    			return response()->json(['status'=>true, 'msg'=> $msg]);
		}
	}

    public function number_format($amount, $decimal = 4, $admin = false)
    {
        if (!$admin) {
            $assist_amount = pow(10, $decimal);
            $amount = floor($amount * $assist_amount) / $assist_amount;
            return number_format($amount, $decimal);
        }
        else {
            $decimal = 6;
            return number_format($amount, $decimal);
        }
    }

    public function isNewAccount()
    {
        $data = request()->only('mobile');

        $accounts = Member::where('mobile', $data['mobile'])
            ->get();

        if ($accounts->count() < 1) {

            return response()->json([
                'status' => true,
            ]);
        }

        return response()->json([
            'status' => false,
        ]);
    }
}
