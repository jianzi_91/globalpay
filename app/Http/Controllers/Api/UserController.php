<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use DB;
use Exception;
use Hash;
use JWTFactory;
use JWTAuth;

use App\Models\Member;

use Carbon\Carbon;

class UserController extends Controller
{
    public function info()
    {
        $user = JWTAuth::toUser();

        return response()->json([
            'status' => 'success',
            'member_info' => $user
        ]);
    }
}