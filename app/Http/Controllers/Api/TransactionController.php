<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Jobs\SpendWallet;

use App\Models\Member;
use App\Models\TransactionLog;
use App\Models\Wallet;

use Carbon\Carbon;

class TransactionController extends Controller
{
    public function detail()
    {
        $customAttributes = [
            'transaction_id' => trans('two.transaction_id')
        ];

        $data = request()->only(array_keys($customAttributes));
        $tranlog_tbl = with(new TransactionLog)->getTable();

        $validator = validator($data, [
            // 'transaction_id' => 'required|exists:'.$tranlog_tbl.',source_id',
            'transaction_id' => 'required',
        ], [], $customAttributes);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'msg' => $validator->getMessageBag()->first()
            ]);
        }

        $transaction = TransactionLog::where('source_id', $data['transaction_id'])
                        ->where('type', 'purchase')->first();

        if (!$transaction) {
            return response()->json([
                'status' => 'fail',
                'msg' => 'this transaction_id is not available'
            ]);
        }
/* removed from details [API]
        $spending_amount = $transaction->a_amt * -1;
        $member = Member::find($transaction->uid);
        
        SpendWallet::dispatch($member, $spending_amount); // Trigger spend wallet for daily release acceleration

        $transaction_count  = TransactionLog::where('uid', $member->id)->count();

        if ($transaction_count == 1) {
            $sponsor = $member->upline;

            if ($sponsor) {
                $sponsor_bonus = $spending_amount * config('rates.sponsor_bonus');
                $sponsor_desc = "User ID #" . $sponsor->id . " received  " . $sponsor_bonus . " to trading wallet from downline ID #" . $member->id . ' for the first purchase by spending ' . $spending_amount . ' of premium wallet.';
                $sponsor_desc_json = [
                    'sponsor_id' => $sponsor->id,
                    'downline_id' => $member->id,
                    'amount' => $spending_amount,
                    'bonus' => $sponsor_bonus,
                    'rate' => config('rates.sponsor_bonus'),
                ];

                $sponsor->walletUpdate($sponsor->id,'dowallet',$sponsor_bonus, 'c', 6, $sponsor_desc, $sponsor_desc_json, 1007);
            }
        }
*/
        return response()->json([
            'status' => 'success',
            'msg' => null,
            'data' => [
                'user_id' => $transaction->uid,
                'transaction_id' => $transaction->source_id,
                'a_amt' => $transaction->a_amt,
                'awallet_balance' => $transaction->a_balance,
                'a2_amt' => $transaction->a2_amt,
                'awallet2_balance' => $transaction->a2_balance
            ]
        ]);

    }
}