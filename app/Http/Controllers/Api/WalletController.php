<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use DB;
use Exception;
use Hash;
use JWTFactory;
use JWTAuth;
use Validator;

use App\Models\Member;
use App\Models\MemberWalletTransfer;
use App\Models\Wallet;

use Carbon\Carbon;

class WalletController extends Controller
{
    public function getWallet()
    {
        $user = JWTAuth::toUser();

        $customAttributes = [
            'lang' => trans('general.menu.user.language')
        ];

        $data = request()->only(array_keys($customAttributes));

        $validator = validator($data, [
            'lang' => 'required',
        ], [], $customAttributes);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'msg' => $validator->getMessageBag()->first()
            ]);
        }

        switch (request('lang')) {
            case 'cn':
                $lang = 'zh-cn';
                break;
            
            default:
                $lang = 'en';
                break;
        }

        app()->setLocale($lang);

        return response()->json([
            'status' => 'success',
            'username' => $user->username,
            'wallet_info' => [
                'wallet_sc' => [
                    'value' => $user->wallet->sc_wallet,
                    'name' => trans('general.wallet.scwallet'),
                ],
            ]
        ]);
    }
}