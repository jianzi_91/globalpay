<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Auth;
use DB;
use Hash;
use Validator;
use JWTFactory;
use JWTAuth;

use App\Models\Country;
use App\Models\Member;
use App\Models\AccessToken;
use App\Services\Jwt\Token;
use App\Utils\DateTimeTool;
use App\Utils\StringTool;

use Carbon\Carbon;

class LoginController extends Controller
{
    protected $tokenService;

    public function __construct(Token $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    public function login()
    {
        $credentials = request()->only('mobile', 'password');

        $nice_names = [
            'mobile' => trans('one.mobile'),
            'password' => trans('one.password')
        ];

        $validator = Validator::make($credentials, [
            'mobile' => 'required',
            'password' => 'required'
        ], [], $nice_names);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'msg' => $validator->getMessageBag()->first()
            ]);
        }

        $credentials = StringTool::sanitizePhoneNo($credentials);

        if (str_contains($credentials['mobile'],'-')) {
            list($mobile, $sub_id) = explode('-',$credentials['mobile']);
            
            $sub_user = Member::where('mobile',$mobile)
                ->where('sub_id', $sub_id)
                ->first();

            if ($sub_user) {

                if (Hash::check($credentials['password'], $sub_user->password)) {

                    Auth::login($sub_user);

                    return response()->json([
                        'status' => 'success',
                        // 'access_token' => $token->getAccessToken(),
                        'access_token' => JWTAuth::fromUser($sub_user),
                        'info' => $this->getAuthInformation($sub_user)
                    ]);
                }
                
            }

            return response()->json([
                'status' => 'fail',
                'msg' => trans('api.response.invalid-credential')
            ]);
        }

        $member = Member::where('mobile', $credentials['mobile'])
            // ->whereIn('sub_id', [0, 1])
            ->first();

        if (is_null($member) || !Hash::check($credentials['password'], $member->password)) {
            return response()->json([
                'status' => 'fail',
                'msg' => trans('api.response.invalid-credential')
            ]);
        }

        // $token = $this->tokenService->generateAccessToken($member);

        return response()->json([
            'status' => 'success',
            // 'access_token' => $token->getAccessToken(),
            'access_token' => JWTAuth::fromUser($member),
            'info' => $this->getAuthInformation($member)
        ]);

    }

    protected function getAuthInformation($member)
    {
        return [
            'username' => $member->username,
            'name' => $member->name,
            'mobile' => $member->mobile,
            'email' => $member->email,
            'country' => $member->country ? Country::selectableCountries()[$member->country] : '-'
        ];
    }

}