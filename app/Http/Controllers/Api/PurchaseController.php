<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use DB;
use Exception;
use Hash;
use JWTFactory;
use JWTAuth;
use GuzzleHttp;
use Validator;

use App\Jobs\SpendWallet;
use App\Models\Member;
use App\Models\MemberWalletTransfer;
use App\Models\Wallet;
use App\Models\TransactionLog;

use Carbon\Carbon;

class PurchaseController extends Controller
{
    public function confirmation_confirm()
    {
        $data = request()->all();
        $user = JWTAuth::toUser($data['token']);
        $trans_code = null;
        $endpoint = $data['response_url'];
        $client = new GuzzleHttp\Client();

        $arr_validation = [
            'security_code' => 'required',
        ];

        $arr_nicenames = [
            'security_code' => trans('two.security_code')
        ];

        $validator = validator($data, $arr_validation, [], $arr_nicenames);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        if (is_null($user) || !Hash::check($data['security_code'], $user->password2)) {
            return back()->withInput()->withErrors(trans('security code is wrong'));
        }

        if (isset($data['sc_amt'])) {

            $trans_code = Wallet::PURCHASE_CHECKOUT_SCWALLET_API;

            $balance = $user->wallet->insufficientWallet('sc_wallet', $data['sc_amt']);

            if(!$balance) {
                return response()->json([
                    'status' => 'fail',
                    'msg' => 'lack of scwallet'
                ]);
            }
        }

        try {
            DB::beginTransaction();

            $purchase = Wallet::adjust([
                'uid' => $user->id,
                'type' => 'purchase',
                'trans_code' => $trans_code,
                'source_id' => $data['transaction_id'],
                'creator_user_type' => $user::$user_code,
                'creator_uid' => $user->id,
                'sc_amt' => isset($data['sc_amt']) ? -$data['sc_amt'] : 0,
                // 'a2_amt' => isset($data['a2_amt']) ? -$data['a2_amt'] : 0
            ]);
            
            // $purchase_reward = Wallet::adjust([
            //     'uid' => $user->id,
            //     'type' => 'purchase-reward',
            //     'trans_code' => Wallet::PURCHASE_REWARD_X3_FWALLET,
            //     'source_id' => $data['transaction_id'],
            //     'creator_user_type' => $user::$user_code,
            //     'creator_uid' => $user->id,
            //     'f_amt' => isset($data['sc_amt']) ? ($data['sc_amt']*3) : 0,
            // ]);

            // $this->quicken_release($data, $user);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollBack();
            // dd($e);

            $result = $client->post($endpoint, [
                'form_params' => [
                    'status' => 'fail',
                    'transaction_id' => $data['transaction_id']
                ]
            ]);

            $checkout_status = 'fail';
            return view('member.checkout.checkout-result')->with(compact('data', 'checkout_status'));

        }

        $result = $client->post($endpoint, [
            'form_params' => [
                'status' => 'success',
                'transaction_id' => $data['transaction_id']
            ]
        ]);

        $checkout_status = 'success';
        return view('member.checkout.checkout-result')->with(compact('data', 'checkout_status'));
        
    }

    // public function quicken_release($data, $member)
    // {
    //     $gen = 1;
    //     $spending =  $data['sc_amt'];
    //     // dd($member->up);
    //     while ($member->up != 0 && $gen < 11) {

    //         switch (true) {
    //             case ($gen <= 10 && $gen >= 6):
    //                 $release = $spending * 0.001;
    //                 break;
    //             case ($gen <= 5 && $gen >= 3):
    //                 $release = $spending * 0.005;
    //                 break;
    //             case ($gen == 2):
    //                 $release = $spending * 0.03;
    //                 break;
    //             case ($gen == 1):
    //                 // $release = $this->spending * 0.05;
    //                 $release = $spending * 0.5;
    //                 break;
    //             default:
    //                 $release = $spending * 0;
    //                 break;
    //         }
    //         $member_upline = $member->upline;

    //         // $additional_release = $member->additionalRelease()->firstOrCreate([
    //         //     'uid' => $member->id,
    //         // ]);
            
    //         // $additional_release = $member->additionalRelease;
    //         // $additional_release->spending_amount += $release;
    //         // $additional_release->spending_total += $release;
    //         // $additional_release->save();

    //         if ($release > $member_upline->wallet->fwallet) {
    //             $release = $member_upline->wallet->fwallet;
    //         }

    //         $purchase_reward = Wallet::adjust([
    //             'uid' => $member->up,
    //             'type' => 'quicken-release-purchase',
    //             'trans_code' => Wallet::QUICKEN_RELEASE_PURCHASE_REWARD,
    //             'source_id' => $data['transaction_id'],
    //             'creator_user_type' => $member::$user_code,
    //             'creator_uid' => $member->id,
    //             'f_amt' => isset($release) ? -($release) : 0,
    //             'do_amt' => isset($release) ? ($release) : 0,
    //         ]);

    //         $member = $member->upline;

    //         $gen++;
    //     }
    // }

    public function confirmation()
    {
        $data = request()->all();

        switch ($data['data']['lang']) {
            case 'cn':
                $lang = 'zh-cn';
                break;
            
            default:
                $lang = 'en';
                break;
        }

        app()->setLocale($lang);        

        return view('mobile.checkout.confirmation', [
            'token' => $data['token'],
        ])->with(compact('data'));

    }

    public function purchase()
    {
        $customAttributes = [
            'transaction_id' => trans('two.transaction_id'),
            // 'a_amt' => trans('two.a_amt'),
            // 'a2_amt' => trans('two.a2_amt'),
            'sc_amt' => trans('general.wallet.scwallet'),
            'response_url' => trans('two.response_url'),
            'token' => trans('one.token'),
            'lang' => trans('general.menu.user.language'),
        ];

        $data = request()->only(array_keys($customAttributes));

        $tranlog_tbl = with(new TransactionLog)->getTable();

        $validator = validator($data, [
            // 'transaction_id' => 'required|unique:'.$tranlog_tbl.',source_id',
            'transaction_id' => 'required',
            'sc_amt' => 'required',
            // 'a_amt' => 'required',
            // 'a2_amt' => 'required_without:a_amt',
            'response_url' => 'required',
            'token' => 'required',
            'lang' => 'required',
        ], [], $customAttributes);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'msg' => $validator->getMessageBag()->first()
            ]);
        }

        switch (request('lang')) {
            case 'cn':
                $lang = 'zh-cn';
                break;
            
            default:
                $lang = 'en';
                break;
        }

        app()->setLocale($lang);

        $transaction_id_check = TransactionLog::where('type', 'purchase')
                                ->where('source_id', $data['transaction_id'])
                                ->first();
        
        if($transaction_id_check) {
            return response()->json([
                'status' => 'fail',
                'msg' => 'this transaction_id is not available'
            ]);
        }

        $user = null;
        try {

            $user = JWTAuth::toUser($data['token']);

        } catch (Exception $e) {

            if (is_null($user)) {
                return response()->json([
                    'status' => 'fail',
                    'msg' => 'Token is invalid'
                ]);
            }
        }

        // return response()->json([
        //     'status' => 'success',
        //     'msg' => null,
        //     'url' => route('checkout-confirmation', [
        //         'token' => $data['token'],
        //         'data' => $data
        //     ])
        // ]);

        return redirect()->route('checkout-confirmation', ['token' => $data['token'], 'data' => $data]);

    }
}