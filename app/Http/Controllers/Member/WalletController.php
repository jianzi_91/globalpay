<?php

namespace App\Http\Controllers\Member;

use Auth;
use DB;
use Exception;

use App\Jobs\SaveWallet;

use App\Malls\MiCool;

use App\Models\MemberWalletRecord;
use App\Models\MemberWalletTransfer;
use App\Models\TransInfo;
use App\Models\Member;
use App\Models\MemberPartnerTransfer;
use App\Models\SessionKeyValue;
use App\Models\Wallet;
use App\Utils\Tool;
use App\Utils\DateTimeTool;
use App\Trades\TradeService;

class WalletController extends Controller
{
    public function index($wallet)
    {
        $user = $this->user;
        
        $record = MemberWalletRecord::where('uid', $user->id)->where('wallet', $wallet)->where("status", 10)->orderBy('created_at', 'desc')->orderBy('id', 'desc')->paginate(20);
        foreach ($record as $key=>$value) {
            $record[$key]['descr-text'] = TransInfo::getDescrFromCode($value);
            $record[$key]['created_at_op'] = DateTimeTool::systemToOperationDateTime($value['created_at']);
        }
        return view('member.wallet.list', ['__records'=>$record, '__wallet' => $wallet]);
    }

    public function purchaseScwallet($wallet)
    {
        $user = $this->user;
        $to_user = null;
        $_guard = "";

        $user_table = $user->getTable();
        $wallet_list = Member::$purchase_wallet;
		$view = view('member.wallet.purchase-scwallet');
		$redirect = redirect()->action('Member\WalletController@purchaseScwallet',  ['wallet' => $wallet]);

        $step = "start";
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('transfer_nonce');
        $nonce = str_random(60);
        $session_key->putValue('transfer_nonce', $nonce);

		//options data and default
        $fields['__nonce']['value'] = $nonce;
         
        //request handling
        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
            
            $data = request()->all();
            // dd($data);

            if ($data['__req']=='1') {

                //get user id
                $to_user = Auth::user();
                $data['uid'] = Auth::user()->id;

                
                $niceNames = [
                    // 'wallet_to' => trans('field.wallet_record.wallet-to'),
                    'uid' => trans('field.user.mobile'),
                    'amount' => trans('field.wallet_record.amount'),
                    'remarks' => trans('field.wallet_record.remarks'),
                    'epassword' => trans('field.user.epassword')
                ];

                $rules =  [
                    // 'wallet_to' => 'required|in:'.implode(',', array_keys($to_wallet_list)),
                    'uid' => 'required|exists:'.$user_table.',id,primary_id,0',
                    'amount' => 'required|min:0.01|numeric',
                    'remarks' => 'required|string|max:2000',
                    'epassword' => 'required',
                ];

                $v = validator($data, $rules, [], $niceNames);
                
                //custom validation
                $v->after(function($validator) use ($niceNames, $wallet) {
                    $data = $validator->getData();
                    
                    //check password
                    $user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);

                    if ($data['amount'] > $user->wallet->awallet && $user->id != 1) {
                        $validator->errors()->add('uid', trans('validation.unmatch.awallet_not_enough_value'));
                    }

                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
                    }

                });
                if ($v->fails()) {
                    return back()->withInput()->withErrors($v);
                }

                $v = validator([], []);

                //custom validation part 2
                $data['amount'] = round($data['amount'], 2);
                $error_list = [];

                // canceling this validation [user are allowed to transfer to their awallet now]
                // if ($data["wallet_to"] == 'awallet' && $this->user->id ==  $data["uid"]) {
                //     $error_list[] = ['uid', trans('general.page.user.wallet-transfer.errmsg.same-account-wallet')];
                // }
                
				$user_wallets = $user->wallet;
                
                $v->after(function($validator) use ($error_list) {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });

                if ($v->fails())
                    return back()->withInput()->withErrors($v);
                
				//check next step 
                $step = "preview";

				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else{
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

                    //transfer
                    if ($wallet == 'awallet') { 

                        try {
                            DB::beginTransaction();
                
                            Wallet::adjust([
                                'uid' => $data['uid'],
                                'type' => 'purchase-scwallet',
                                'trans_code' => Wallet::PURCHASE_SCWALLET,
                                'creator_user_type' => $user::$user_code,
                                'creator_uid' => $data['uid'],
                                'a_amt' => isset($data['amount']) ? -$data['amount'] : 0,
                                'sc_amt' => isset($data['amount']) ? $data['amount'] : 0
                            ]);

                            $purchase_scwallet_reward = Wallet::adjust([
                                'uid' => $user->id,
                                'type' => 'purchase-scwallet-reward',
                                'trans_code' => Wallet::PURCHASE_SCWALLET_REWARD_X3_FWALLET,
                                // 'source_id' => $data['transaction_id'],
                                'creator_user_type' => $user::$user_code,
                                'creator_uid' => $user->id,
                                'f_amt' => isset($data['amount']) ? ($data['amount']*3) : 0,
                            ]);

                            $this->quicken_release_purchase($data, $user);
                
                            DB::commit();
                        }
                        catch (Exception $e) {
                            DB::rollBack();
                            dd($e);
                        }
                    }

                    return back()->with(['post' => $data, '__step'=>$step, '__to_user'=>$to_user])->with('success', trans('general.page.user.wallet-transfer.msg.transfer-success'));
                }
            }

			return $redirect->with(['__step'=>$step,'__user'=>$user,'__to_user' => $to_user])->withInput();
        }

        return $view->with(['__wallet' => $wallet, '__step'=>$step, '__user'=>$user, '__fields'=>$fields]);
    }

    public function quicken_release_purchase($data, $member)
    {
        $gen = 1;
        $spending =  $data['amount'];
        // dd($member->up);
        while ($member->up != 0 && $gen < 11) {

            switch (true) {
                case ($gen <= 10 && $gen >= 6):
                    $release = $spending * 0.001;
                    break;
                case ($gen <= 5 && $gen >= 3):
                    $release = $spending * 0.005;
                    break;
                case ($gen == 2):
                    $release = $spending * 0.03;
                    break;
                case ($gen == 1):
                    // $release = $this->spending * 0.05;
                    $release = $spending * 0.5;
                    break;
                default:
                    $release = $spending * 0;
                    break;
            }
            $member_upline = $member->upline;

            if ($release > $member_upline->wallet->fwallet) {
                $release = $member_upline->wallet->fwallet;
            }

            $purchase_reward = Wallet::adjust([
                'uid' => $member->up,
                'type' => 'quicken-release-purchase',
                'trans_code' => Wallet::QUICKEN_RELEASE_PURCHASE_REWARD,
                // 'source_id' => $data['transaction_id'],
                'creator_user_type' => $member::$user_code,
                'creator_uid' => $member->id,
                'f_amt' => isset($release) ? -($release) : 0,
                'do_amt' => isset($release) ? ($release) : 0,
            ]);

            $member = $member->upline;

            $gen++;
        }
    }

    public function transferRepeat($wallet)
    {
        $user = $this->user;
        $to_user = null;
        $_guard = "";

        $user_table = $user->getTable();
        $wallet_list = Member::$wallet_transfer_list;
		$view = view('member.wallet.transfer-repeat');
		$redirect = redirect()->action('Member\WalletController@transferRepeat',  ['wallet' => $wallet]);
        
        if (!array_key_exists($wallet, $wallet_list) || ($user->memberStatus->alliance_status != 10 && $wallet == "fwallet") ) {
            abort(404);
        }

        if ($wallet == 'awallet' && $user->id != config('app.company_main_account_id')) {
            abort(404);
        }

        $wallet_list = $wallet_list[$wallet];
        $to_wallet_list = [];
        for ($i=0; $i<count($wallet_list); $i++) {
            $to_wallet_list[$wallet_list[$i]] = trans("general.wallet.".$wallet_list[$i]);
        }

        if ($user->id != 1) {
            unset($to_wallet_list['awallet']);
        }
        
        $step = "start";
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('transfer_nonce');
        $nonce = str_random(60);
        $session_key->putValue('transfer_nonce', $nonce);

		//options data and default
 		$fields['wallet_to']['options'] = $to_wallet_list;
        $fields['__nonce']['value'] = $nonce;
         
        //request handling
        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {

            $data = request()->all();
            // dd($data);

            if ($data['__req']=='1') {

                if ($data["wallet_to"] == "swallet" || $data["wallet_to"] == "awallet2") {

                    if ($user->sub_id != 0) {
                        $data["uid"] = $user->mobile . '-' . $user->sub_id;
                    }
                    else {
                        $data["uid"] = $user->mobile;
                    }

                }

                //get user id
                if($data['uid']) {
                    
                    if (str_contains($data['uid'],'-')) {
                        list($mobile, $sub_id) = explode('-',$data['uid']);
                        
                        $to_user = Member::where('mobile',$mobile)
                            ->where('sub_id', $sub_id)
                            ->first();
                    }
                    else {
                        $to_user = Member::where('mobile', '=', $data['uid'])->first();
                    }
                    
				    $data['uid'] = $to_user?$to_user->id:'0';
                }
                else {
                    $to_user = Auth::user();
                    $data['uid'] = Auth::user()->id;
                }
                
                $niceNames = [
                    'wallet_to' => trans('field.wallet_record.wallet-to'),
                    'uid' => trans('field.user.mobile'),
                    'amount' => trans('field.wallet_record.amount'),
                    'remarks' => trans('field.wallet_record.remarks'),
                    'epassword' => trans('field.user.epassword')
                ];

                $rules =  [
                    'wallet_to' => 'required|in:'.implode(',', array_keys($to_wallet_list)),
                    'uid' => 'required|exists:'.$user_table.',id,primary_id,0',
                    'amount' => 'required|min:0.01|numeric',
                    'remarks' => 'required|string|max:2000',
                    'epassword' => 'required',
                ];

                $v = validator($data, $rules, [], $niceNames);
                
                //custom validation
                $v->after(function($validator) use ($niceNames, $wallet) {
                    $data = $validator->getData();
                    
                    //check password
                    $user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);

                    if (($data['wallet_to'] == 'fwallet') && $user->id != 1 ) {
                        if ($user->id != $data['uid']) {
                            $validator->errors()->add('uid', trans('validation.unmatch.fwallet_only_allow_transfer_to_own_account'));
                        }
                    }

                    if (($wallet == 'dowallet') && ($data['wallet_to'] == 'fwallet')) {
                        if ($data['amount'] % 10 != 0 ) {
                            $validator->errors()->add('amount', trans('validation.unmatch.repeat_transfer_value_multiply_10'));
                        }
                    }

                    if ($data['amount'] > $user->wallet->dowallet && $user->id != 1) {
                        $validator->errors()->add('uid', trans('validation.unmatch.not_enough_value'));
                    }

                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
                    }

                });
                if ($v->fails()) {
                    return back()->withInput()->withErrors($v);
                }

                $v = validator([], []);

                //custom validation part 2
                $data['amount'] = round($data['amount'], 2);
                $error_list = [];

				$user_wallets = $user->wallet;
                
                $v->after(function($validator) use ($error_list) {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });

                if ($v->fails())
                    return back()->withInput()->withErrors($v);
                
				//check next step 
                $step = "preview";

				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else{
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'fwallet') {

                        if ($user->id == $data['uid']) {

                            try {
                                DB::beginTransaction();
                    
                                Wallet::adjust([
                                    'uid' => $data['uid'],
                                    'trans_code' => Wallet::WALLET_TRANSFER_SELF_FWALLET,
                                    'creator_user_type' => $user::$user_code,
                                    'creator_uid' => $data['uid'],
                                    'f_amt' => isset($data['amount']) ? ($data['amount'] * 3) : 0,
                                    'do_amt' => isset($data['amount']) ? -$data['amount'] : 0,
                                    'sc_amt' => isset($data['amount']) ? ($data['amount'] * 0.1) : 0
                                ]);

                                $this->quicken_release($data, $user);
                    
                                DB::commit();
                            }
                            catch (Exception $e) {
                                DB::rollBack();
                                dd($e);
                            }
                        }
                    }
                  
                    return back()->with(['post' => $data, '__step'=>$step, '__to_user'=>$to_user])->with('success', trans('general.page.user.wallet-transfer.msg.transfer-success'));
                }
            }

			return $redirect->with(['__step'=>$step,'__user'=>$user,'__to_user' => $to_user])->withInput();
        }

        return $view->with(['__wallet' => $wallet, '__step'=>$step, '__user'=>$user, '__fields'=>$fields]);
    }

    public function transfer($wallet)
    {
        $user = $this->user;
        $to_user = null;
        $_guard = "";

        $user_table = $user->getTable();
        $wallet_list = Member::$wallet_transfer_list;
		$view = view('member.wallet.transfer');
		$redirect = redirect()->action('Member\WalletController@transfer',  ['wallet' => $wallet]);
        
        if (!array_key_exists($wallet, $wallet_list) || ($user->memberStatus->alliance_status != 10 && $wallet == "fwallet") ) {
            abort(404);
        }

        if ($wallet == 'awallet' && $user->id != config('app.company_main_account_id')) {
            abort(404);
        }

        $wallet_list = $wallet_list[$wallet];
        $to_wallet_list = [];
        for ($i=0; $i<count($wallet_list); $i++) {
            $to_wallet_list[$wallet_list[$i]] = trans("general.wallet.".$wallet_list[$i]);
        }

        if ($user->id != 1) {
            // unset($to_wallet_list['fwallet']);
        }

        $step = "start";
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('transfer_nonce');
        $nonce = str_random(60);
        $session_key->putValue('transfer_nonce', $nonce);

		//options data and default
 		$fields['wallet_to']['options'] = $to_wallet_list;
        $fields['__nonce']['value'] = $nonce;
         
        //request handling
        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
            

            // if ($wallet == "cwallet" && in_array($user->id, [51, 58])) {
            //     return back()->withInput()->withErrors(collect(["all"=> trans('general.page.user.wallet-transfer.errmsg.block')]));
            // }

            $data = request()->all();
            // dd($data);

            if ($data['__req']=='1') {
                //only can transfer to own wallet
                // if ($wallet != "rwallet" && $wallet != "fwallet") {
                //     $data["uid"] = $user->mobile;
                // }
                if ($data["wallet_to"] == "swallet" || $data["wallet_to"] == "awallet2") {

                    if ($user->sub_id != 0) {
                        $data["uid"] = $user->mobile . '-' . $user->sub_id;
                    }
                    else {
                        $data["uid"] = $user->mobile;
                    }

                }

                //get user id
                if($data['uid']) {
                    
                    if (str_contains($data['uid'],'-')) {
                        list($mobile, $sub_id) = explode('-',$data['uid']);
                        
                        $to_user = Member::where('mobile',$mobile)
                            ->where('sub_id', $sub_id)
                            ->first();
                    }
                    else {
                        $to_user = Member::where('mobile', '=', $data['uid'])->first();
                    }
                    
				    $data['uid'] = $to_user?$to_user->id:'0';
                }
                else {
                    $to_user = Auth::user();
                    $data['uid'] = Auth::user()->id;
                }
                
                $niceNames = [
                    'wallet_to' => trans('field.wallet_record.wallet-to'),
                    'uid' => trans('field.user.mobile'),
                    'amount' => trans('field.wallet_record.amount'),
                    'remarks' => trans('field.wallet_record.remarks'),
                    'epassword' => trans('field.user.epassword')
                ];

                $rules =  [
                    'wallet_to' => 'required|in:'.implode(',', array_keys($to_wallet_list)),
                    'uid' => 'required|exists:'.$user_table.',id,primary_id,0',
                    'amount' => 'required|min:0.01|numeric',
                    'remarks' => 'required|string|max:2000',
                    'epassword' => 'required',
                ];

                // if ($wallet == "awallet" && $data["wallet_to"] == "cwallet") {
                //     $rules["amount"] = 'required|min:300|max:5000|numeric';
                // }
                $v = validator($data, $rules, [], $niceNames);
                
                //custom validation
                $v->after(function($validator) use ($niceNames, $wallet) {
                    $data = $validator->getData();
                    
                    //check password
                    $user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);

                    if (($data['wallet_to'] == 'fwallet') && $user->id != 1 ) {
                        if ($user->id != $data['uid']) {
                            $validator->errors()->add('uid', trans('validation.unmatch.fwallet_only_allow_transfer_to_own_account'));
                        }
                    }

                    if (($wallet == 'dowallet') && ($data['wallet_to'] == 'fwallet')) {
                        if ($data['amount'] % 10 != 0 ) {
                            $validator->errors()->add('amount', trans('validation.unmatch.repeat_transfer_value_multiply_10'));
                        }
                    }

                    if ($data['amount'] > $user->wallet->dowallet && $user->id != 1) {
                        $validator->errors()->add('uid', trans('validation.unmatch.not_enough_value'));
                    }

                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
                    }

                });
                if ($v->fails()) {
                    return back()->withInput()->withErrors($v);
                }

                $v = validator([], []);

                //custom validation part 2
                $data['amount'] = round($data['amount'], 2);
                $error_list = [];

                // canceling this validation [user are allowed to transfer to their awallet now]
                // if ($data["wallet_to"] == 'awallet' && $this->user->id ==  $data["uid"]) {
                //     $error_list[] = ['uid', trans('general.page.user.wallet-transfer.errmsg.same-account-wallet')];
                // }
                
				$user_wallets = $user->wallet;
                
                $v->after(function($validator) use ($error_list) {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });

                if ($v->fails())
                    return back()->withInput()->withErrors($v);
                
				//check next step 
                $step = "preview";

				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else{
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

                    //transfer
                    if ($wallet == 'awallet') { 
                        $transfer_to_awallet = $data['amount'] * config('rates.register_awallet');
                        // $transfer_to_fwallet = $data['amount'] * config('rates.register_fwallet');

                        $awallet_transfer_result = $user->walletTransfer($user->id, $to_user->id, $transfer_to_awallet, $wallet, 'awallet', $data['remarks']);
                        // $fwallet_transfer_result = $user->walletTransfer($user->id, $to_user->id, $transfer_to_fwallet, $wallet, 'fwallet', $data['remarks']);
                    }

                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'awallet') {

                        if ($user->id != $data['uid']) {

                            try {
                                DB::beginTransaction();

                                $initiator = Tool::getInitiator($_guard);

                                $wt = MemberWalletTransfer::create([
                                    "itype" => $initiator["itype"],
                                    "iid" => $initiator["iid"],
                                    "from_wallet" => $wallet,
                                    "from_uid" => $user->id,
                                    "from_amount" => $data["amount"],
                                    "to_wallet" => $data["wallet_to"],
                                    "to_uid" => $data['uid'],
                                    "to_amount" => $data["amount"],
                                    "fee" => 0, // fee set to 0 as default
                                    "remarks" => $data["remarks"],
                                    "status" => 10
                                ]);

                                $transfer_data_from = [
                                    'uid' => $user->id,
                                    'trans_code' => Wallet::WALLET_TRANSFER_OTHERS_AWALLET_OUT,
                                    'source_id' => $wt->id,
                                    'do_amt' => isset($data['amount']) ? -$data['amount'] : 0,
                                    'creator_user_type' =>  $user::$user_code,
                                    'creator_uid' => $user->id
                                ];
                                Wallet::adjust($transfer_data_from);
                    
                                $transfer_data_to = [
                                    'uid' => $data['uid'],
                                    'trans_code' => Wallet::WALLET_TRANSFER_OTHERS_AWALLET_IN,
                                    'source_id' => $wt->id,
                                    'a_amt' => isset($data['amount']) ? $data['amount'] : 0,
                                    'creator_user_type' =>  $user::$user_code,
                                    'creator_uid' => $user->id
                                ];
                                Wallet::adjust($transfer_data_to);
                    
                                DB::commit();
                            }
                            catch (Exception $e) {
                                DB::rollBack();
                            }

                        }

                        if ($user->id == $data['uid']) {

                            try {
                                DB::beginTransaction();

                                $initiator = Tool::getInitiator($_guard);
                    
                                Wallet::adjust([
                                    'uid' => $data['uid'],
                                    'trans_code' => Wallet::WALLET_TRANSFER_SELF_AWALLET,
                                    'creator_user_type' => $user::$user_code,
                                    'creator_uid' => $data['uid'],
                                    'a_amt' => isset($data['amount']) ? ($data['amount'] * 0.8) : 0,
                                    'f_amt' => isset($data['amount']) ? ($data['amount'] * 0.2) : 0,
                                    'do_amt' => isset($data['amount']) ? -$data['amount'] : 0
                                ]);
                    
                                DB::commit();
                            }
                            catch (Exception $e) {
                                DB::rollBack();
                            }

                        }
                    }

                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'fwallet') {

                        if ($user->id == $data['uid']) {

                            // if ($data['amount'] % 10 != 0 ) {
                            //     return redirect()->back()->withInput()->with('error', trans('validation.unmatch.repeat_transfer_value_multiply_10'));
                            // }

                            // if ($data['amount'] > $user->wallet->dowallet) {
                            //     return redirect()->back()->withInput()->with('error', trans('validation.unmatch.not_enough_value'));
                            // }

                            try {
                                DB::beginTransaction();
                    
                                Wallet::adjust([
                                    'uid' => $data['uid'],
                                    'trans_code' => Wallet::WALLET_TRANSFER_SELF_FWALLET,
                                    'creator_user_type' => $user::$user_code,
                                    'creator_uid' => $data['uid'],
                                    'f_amt' => isset($data['amount']) ? ($data['amount'] * 3) : 0,
                                    'do_amt' => isset($data['amount']) ? -$data['amount'] : 0
                                ]);

                                $this->quicken_release($data, $user);
                    
                                DB::commit();
                            }
                            catch (Exception $e) {
                                DB::rollBack();
                                // dd($e);
                            }
                        }
                    }
/* jianzi's work temporary commented by kachoon
                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'swallet') {
                        $transfer_to_swallet = $data['amount'] * config('rates.transfer_dowallet_swallet');

                        SaveWallet::dispatch($user, $data['amount']);

                        $user->walletUpdate($user->id, $wallet, $data['amount'], 'd');
                        $user->walletUpdate($user->id, $data['wallet_to'], $transfer_to_swallet);
                    }

                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'awallet2') {

                        $user->walletTransfer($user->id, $to_user->id, $data['amount'], $wallet, $data['wallet_to']);
                    }

                    if ($wallet == 'dowallet' && $data['wallet_to'] == 'awallet') {

                        $dowallet_desc = "Transfer " . $data['amount'] . " from user ID #" . $user->id . " dowallet to user ID #" . $to_user->id;

                        $user->walletUpdate($user->id, 'dowallet', $data['amount'], 'd', 2, $dowallet_desc);

                        // Wallet splitting after received the transfer from user
                        $awallet_receive = $data['amount'] * config('rates.receive_awallet');

                        $awallet_desc = "User ID #" . $to_user->id . " received " . $awallet_receive . "(awallet) from user ID #" . $user->id . "  by transferring " . $data['amount'] . "(dowallet)";

                        $user->walletUpdate($to_user->id, 'awallet', $awallet_receive, 'c', 2, $awallet_desc);

                        $fwallet_receive = $data['amount'] * config('rates.receive_fwallet');

                        $fwallet_desc = "User ID #" . $to_user->id . " received " . $fwallet_receive . "(fwallet) from user ID #" . $user->id . "  by transferring " . $data['amount'] . "(dowallet)";

                        $user->walletUpdate($to_user->id, 'fwallet', $fwallet_receive, 'c', 2, $fwallet_desc);
                    }   
*/                    
                    return back()->with(['post' => $data, '__step'=>$step, '__to_user'=>$to_user])->with('success', trans('general.page.user.wallet-transfer.msg.transfer-success'));
                }
            }

			return $redirect->with(['__step'=>$step,'__user'=>$user,'__to_user' => $to_user])->withInput();
        }

        return $view->with(['__wallet' => $wallet, '__step'=>$step, '__user'=>$user, '__fields'=>$fields]);
    }

    public function quicken_release($data, $member)
    {
        $gen = 1;
        $spending =  $data['amount'];
        // dd($member->up);
        while ($member->up != 0 && $gen < 11) {

            switch (true) {
                case ($gen <= 10 && $gen >= 6):
                    $release = $spending * 0.001;
                    break;
                case ($gen <= 5 && $gen >= 3):
                    $release = $spending * 0.005;
                    break;
                case ($gen == 2):
                    $release = $spending * 0.03;
                    break;
                case ($gen == 1):
                    // $release = $this->spending * 0.05;
                    $release = $spending * 0.5;
                    break;
                default:
                    $release = $spending * 0;
                    break;
            }
            $member_upline = $member->upline;

            // $additional_release = $member->additionalRelease()->firstOrCreate([
            //     'uid' => $member->id,
            // ]);
            
            // $additional_release = $member->additionalRelease;
            // $additional_release->spending_amount += $release;
            // $additional_release->spending_total += $release;
            // $additional_release->save();

            if ($release > $member_upline->wallet->fwallet) {
                $release = $member_upline->wallet->fwallet;
            }

            $purchase_reward = Wallet::adjust([
                'uid' => $member->up,
                'type' => 'quicken-release-repeat',
                'trans_code' => Wallet::QUICKEN_RELEASE_REPEAT_REWARD,
                // 'source_id' => $data['transaction_id'],
                'creator_user_type' => $member::$user_code,
                'creator_uid' => $member->id,
                'f_amt' => isset($release) ? -($release) : 0,
                'do_amt' => isset($release) ? ($release) : 0,
            ]);

            $member = $member->upline;

            $gen++;
        }
    }

    public function verifyPartnerTransfer($data, $niceNames) 
    {
        $user = $this->user;
        $from_wallet = "swallet";
        $to_wallet = "vtoken";

        $v = validator($data, [
            'to_ref' => 'required|email',
            'amount' => 'required|min:1|integer',
            'remarks' => 'required|string|max:2000',
            'epassword' => 'required',
        ], [], $niceNames);
        
        $error_list = [];

        if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
            $error_list[] = ['epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']])];
        }

        if ($user->dstatus == Member::$dstatus_to_code["in-debt"]) {
            $error_list[] = ['all', trans('general.page.user.micool-transfer.errmsg.debt-account')];
        }
                
        $v->after(function($validator) use ($error_list) {
            foreach ($error_list as $key=>$value) {
                $validator->errors()->add($value[0], $value[1]); 
            }
        });

        if ($v->fails())
            return back()->withInput()->withErrors($v);
                
        $user_wallets = $user->wallet;
        if ($data['amount'] && $user_wallets->$from_wallet < $data['amount']) {
            $error_list[] = ['amount', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$from_wallet), 'required' => number_format($data['amount']), 'balance' => number_format($user_wallets->$from_wallet, 4)])];
        }
                
        $v->after(function($validator) use ($error_list) {
            foreach ($error_list as $key=>$value) {
                $validator->errors()->add($value[0], $value[1]); 
            }
        });

        if ($v->fails())
            return back()->withInput()->withErrors($v);
    }

    public function partnerTransfer()
    {
        $redirect = redirect("member/mall/micool-transfer");
		$session_key = new SessionKeyValue();
        $pre_nonce = $session_key->getValue('mall_transfer_nonce');

        $session_key->forgetKey('mall_transfer_nonce');

        if (request()->get("__req") == 1 && 
            $pre_nonce == request()->get('__nonce') && 
            $pre_nonce) {

            $niceNames = array(
                'to_ref' => trans('field.user.email'),
                'amount' => trans('field.wallet_record.amount'),
                'remarks' => trans('field.wallet_record.remarks'),
                'epassword' => trans('field.user.epassword')
            );

            $data = request()->only(array_keys($niceNames));
            $data['to_ref'] = trim($data['to_ref']);

            $redirect_verify = $this->verifyPartnerTransfer($data, $niceNames);
            if ($redirect_verify) {
                return $redirect_verify->with(["__step"=>"start"]);
            }

            if (request()->get("__step") == "preview") {
                return $redirect->with(["__step"=>"preview"])->withInput();
            }
            elseif (request()->get("__step") == "start") {
                return $redirect->with(["__step"=>"start"])->withInput();
            }
            elseif (request()->get("__step") == "confirmed") {
                $user = $this->user;
                $from_wallet = "swallet";
                $to_wallet = "vtoken";
                $errmsg = null;

                try {
                    $data["amount"] = round($data["amount"]);

                    $initiator = Tool::getInitiator($this->guard);
                    $partner_transfer = MemberPartnerTransfer::create([
                        "itype" => $initiator["itype"],
                        "iid" => $initiator["iid"],
                        "from_uid" => $user->id,
                        "from_amount" => $data["amount"],
                        "to_amount" => $data["amount"],
                        "to_ref" => $data["to_ref"],
                        "from_wallet" => $from_wallet,
                        "to_wallet" => $to_wallet,
                        "status" => MemberPartnerTransfer::$status_to_code["pending"],
                    ]);

                    DB::beginTransaction();
                    $topup_data = [
                        "email" => $partner_transfer->to_ref,
                        "amount" => $partner_transfer->to_amount,
                        "wallet" => $partner_transfer->from_wallet,
                        "from" => $user->username,
                        "ref" => $partner_transfer->id,
                    ];
                    
                    $descr_json = [
                        'from_wallet' => $partner_transfer->from_wallet,
                        'from_amount' => $partner_transfer->from_amount,
                        'to_wallet' => $partner_transfer->to_wallet,
                        'to_amount' => $partner_transfer->to_amount,
                        'from_uid' => $partner_transfer->from_uid,
                        'to_ref' => $partner_transfer->to_ref,
                        'trans-type' => "from",
                        'partner' => "micool",
                        'username' => $user->username,
                        'remarks' => $data['remarks'],
                    ]; 
                    
                    $proceed_status = $user->deductWallet($partner_transfer->from_uid, $partner_transfer->from_wallet, $partner_transfer->from_amount, 'Wallet Partner Transfer From '.$partner_transfer->from_amount." ".$partner_transfer->from_wallet." To MiCool ".$partner_transfer->to_amount." ".$partner_transfer->to_wallet.".", $descr_json, TransInfo::$trans_type_to_code['micool-transfer'])["status"];

                    if ($proceed_status) {

                        $partner = new MiCool();
                        $partner_return = $partner->topup($topup_data);

                        if (!$partner_return["status"]) {
                            $partner_transfer->return_body = $partner_return["return_data"];
                            $partner_transfer->return_status = $partner_return["return_status_code"];
                            $partner_return["return_data"] = json_decode($partner_return["return_data"], true);
                            if (isset($partner_return["return_data"]["message"])) {
                                $errmsg = $partner_return["return_data"]["message"];
                            }
                            else {
                                $errmsg = trans('general.page.user.micool-transfer.errmsg.transfer-error');
                            }
                            throw new Exception("Top Up: ".$errmsg);
                        }
                        $partner_return["return_data"] = json_decode($partner_return["return_data"], true);
                        $ref = isset($partner_return["return_data"]["ref"]) ? $partner_return["return_data"]["ref"] : null;
                        $partner_return = $partner->confirm(["ref"=>$ref]);
                        
                        if (!$partner_return["status"]) {
                            $partner_transfer->return_body = $partner_return["return_data"];
                            $partner_transfer->return_status = $partner_return["return_status_code"];
                            $partner_return["return_data"] = json_decode($partner_return["return_data"], true);
                            if (isset($partner_return["return_data"]["message"])) {
                                $errmsg = $partner_return["return_data"]["message"];
                            }
                            else {
                                $errmsg = trans('general.page.user.micool-transfer.errmsg.transfer-error');
                            }
                            throw new Exception("Confirm: ".$errmsg);
                        }

                    }

                    DB::commit();

                    $partner_transfer->return_status = $partner_return["return_status_code"];
                    $partner_transfer->return_body = $partner_return["return_data"];
                    $partner_transfer->status = MemberPartnerTransfer::$status_to_code["confirmed"];
                    $status = true;
                } catch (Exception $e) {

                    DB::rollBack();
                    $status = false;
                    $partner_transfer->status = MemberPartnerTransfer::$status_to_code["error"];
                }

                try {
                    $partner_transfer->save();
                } catch (Exception $e) {

                }
                
                if ($status) {
                    return $redirect->with(["__step"=>"confirmed", "success"=>trans("general.page.user.micool-transfer.msg.transfer-success")])->withInput();
                }
                else {
                    $v = validator([], []);
                    if (!$errmsg) {
                        $errmsg = trans("general.page.user.micool-transfer.errmsg.transfer-error");
                    }
                    $v->getMessageBag()->add('all', $errmsg);
                    return $redirect->with(["__step"=>"start"])->withInput()->withErrors($v);
                }
            }
        }
        return $redirect;
    }

    public function getPartnerTransferPage()
    {
        //action only valid once
        $nonce = str_random(60);
		$session_key = new SessionKeyValue();
        $session_key->putValue('mall_transfer_nonce', $nonce);
        
		//options data and default
 		$fields['__nonce']['value'] = $nonce;
 		$fields['__step']['value'] = session()->get("__step", "start");

        return view("member.mall.micool-transfer")->with(["__fields"=>$fields]);
    }
}
