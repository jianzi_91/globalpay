<?php

namespace App\Http\Controllers\Member;

use DB;

use App\Models\Member;
use App\Models\MemberRelationship;
use App\Models\Wallet;
use App\Models\News;
use App\Utils\DateTimeTool;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return redirect('member/home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $user = $this->user;
        $wallet = new Wallet;
        $wallet_table =  $wallet->getTable();
        $member_table =  $user->getTable();
        $relationship_table =  with(new MemberRelationship)->getTable();
        
        $downline_info = $wallet->setTable($wallet_table." AS w");
        $downline_info = $downline_info->join($relationship_table." AS r", "w.uid", "=", "r.downline_id")
            ->join($member_table." AS d", "d.id", "=", "w.uid")
            ->where("r.user_id", $user->id)
            ->where("d.status", Member::$status_to_code["active"])
            ->select([
                DB::raw("SUM(IF(r.pos = 1, 1, 0)) AS left_count"),
                DB::raw("SUM(IF(r.pos = 2, 1, 0)) AS right_count"),
                DB::raw("SUM(IF(r.pos = 1, w.awallet + w.awallet2, 0)) AS left_sum"),
                DB::raw("SUM(IF(r.pos = 2, w.awallet + w.awallet2, 0)) AS right_sum"),
            ])
            ->first();

        $downline_info->left_count = $downline_info->left_count?:0;
        $downline_info->right_count = $downline_info->right_count?:0;
        $downline_info->left_sum = $downline_info->left_sum ? floor($downline_info->left_sum * 100) / 100 :0;
        $downline_info->right_sum = $downline_info->right_sum? floor($downline_info->right_sum * 100) / 100 :0;

        //news record take only 2
        $news_list = News::getAllAvailable()
            ->orderBy('start_date', 'desc')
            ->paginate(2);
            
        $news_pop_ups = News::getAllAvailable()
            ->where("pop_up", 1)
            ->orderBy('start_date', 'desc')
            ->limit(1)
            ->get();
        $pop_ups = null;

        foreach ($news_pop_ups as $pop_up) {
            $pop_ups["news"][] = [
                "heading" => app()->isLocale('en') ? $pop_up->title_en : $pop_up->title_zh_cn,
                "body" => app()->isLocale('en') ? $pop_up->descr_en : $pop_up->descr_zh_cn,
                "link" => url('member/news', $pop_up->id)
            ];
        }

        return view('member.home', [
            "__news_records" => $news_list, 
            "__last_login" => DateTimeTool::systemToOperationDateTime($user->login_at2),
            "__popups" => $pop_ups,
            "__downline_info" => $downline_info
        ]);
    }
}
