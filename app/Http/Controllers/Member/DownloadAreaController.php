<?php

namespace App\Http\Controllers\Member;

class DownloadAreaController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    public function index()
    {
        return view('member.download-area.index');
    }
}
