<?php

namespace App\Http\Controllers\Member;

use DB;

use App\Models\Commission;
use App\Models\DistributeWallet;
use App\Models\Package;
use App\Models\Member;
use App\Models\MemberProfile;
use App\Models\MemberWalletTransfer;
use App\Models\TransactionLog;
use App\Models\Wallet;
use App\Models\Withdrawal;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TransactionHistoryController extends Controller
{
    public function index()
    {
        $transcode_list = TransactionLog::transcodeType();
        // dump($transcode_list);
        return view('member.transaction.transaction-history')->with('transcode_list', $transcode_list);
    }

    public function ajaxGetDataTable()
    {
        $user = $this->user;

        $from = request('from');
        $to = request('to');
        $transcode = request('transcode');

        $trans_log_model = new TransactionLog;
        $tbl_wallet_log = with($trans_log_model)->getTable().' as wl';
        $trans_log_model->setTable($tbl_wallet_log);

        $query = $trans_log_model->where('wl.uid', $user->id)
                                    ->where(function($query) use ($from, $to, $transcode) {
                                        if ($from) {
                                            $query->where('wl.created_at', '>=', DateTimeTool::getSystemStartDateTime($from, 'Y-m-d'));
                                        }
                                        
                                        if ($to) {
                                            $query->where('wl.created_at', '<=', DateTimeTool::getSystemEndDateTime($to, 'Y-m-d'));
                                        }

                                        if ($transcode) {
                                            $query->whereIn('wl.trans_code', $transcode);
                                        }
                                    })
                                    ->orderBy('wl.id', 'desc');
                                    

        
        return Datatables::of($query)
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at'], 'Y-m-d H:i'));
				})
                ->editColumn('description', function ($list) use ($user) {
                    $creator = Member::find($list['creator_uid']);
                    return $list->getDescription($user, $list["id"], $creator);
				})
                ->editColumn('awallet', function ($list) {
                    return number_format($list['a_amt'],2).'<br>---------------<br>'.number_format($list['a_balance'],2);
                })
                ->editColumn('fwallet', function ($list) {
                    return number_format($list['f_amt'],2).'<br>---------------<br>'.number_format($list['f_balance'],2);
                })
                ->editColumn('dowallet', function ($list) {
					return number_format($list['do_amt'],2).'<br>---------------<br>'.number_format($list['do_balance'],2);
                })
                ->editColumn('sc_wallet', function ($list) {
					return number_format($list['sc_amt'],2).'<br>---------------<br>'.number_format($list['sc_balance'],2);
                })
                ->rawColumns(['description', 'awallet', 'fwallet', 'dowallet', 'sc_wallet'])
                ->make(true);
    }
}
