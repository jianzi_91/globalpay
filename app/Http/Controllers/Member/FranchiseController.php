<?php

namespace App\Http\Controllers\Member;

use DB;
use Exception;

use App\Models\Member;
use App\Models\Activation;
use App\Models\TransInfo;
use App\Models\SessionKeyValue;
use App\Models\Logger;
use App\Repositories\CountryRepo;
use App\Repositories\WalletRepo;
use App\Utils\Tool;

class FranchiseController extends Controller
{
	protected $country_repo;
	protected $walletRepo;

	//Identifiers
	protected $log_identifiers = [];

	public function __construct(CountryRepo $country_repo, WalletRepo $walletRepo)
    {
		$this->country_repo = $country_repo;
		$this->walletRepo = $walletRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
    {
		$this->log_identifiers[] = $id;
	}

	protected function getIdentifiers()
    {
		$identifiers = $this->log_identifiers;
		$this->log_identifiers = [];
		return $identifiers;
	}

	private function isHighestPackageCountQualifyByUserId($user_id, $user_table)
	{	
		$activation = new Activation;
		$activation_table = $activation->getTable();
		$activation->setTable($activation_table." AS act");
		$count = $activation->where("act.upid", $user_id)
			->where("act.code", ">=" , "5100")
			->where("act.status", Activation::$status_to_code["confirmed"])
			->where("act.b_status", Activation::$b_status_to_code["confirmed"])
			->distinct("act.upid")
			->count('act.uid');

		$count += $activation->where("act.upid", $user_id)
			->where("act.code", ">=" , "5100")
			->where("act.act_type", Activation::$act_type_to_code["special"])
			->where("act.status", Activation::$status_to_code["confirmed"])
			->where("act.b_status", Activation::$b_status_to_code["pending"])
			->distinct("act.upid")
			->count('act.uid');
		return $count == 7;
	}

    public function create()
    {
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

		$franchise_main_user = $user = $this->user;
        if ($franchise_main_user->franchise_user_id) {
            $franchise_main_user = Member::findOrFail($franchise_main_user->franchise_user_id);
        }
		
		//set variable
		$redirect = redirect()->action('Member\FranchiseController@create');
		$view = view('member.franchise.create');
		$user_table = $user->getTable();
		$package_details = array_intersect_key(Activation::$package_code_to_pack, array_flip(Activation::$normal_package_code_list));
		$use_wallet = session()->get("__use_wallet", []);
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('franchise_create_nonce');
        $nonce = str_random(60);
        $session_key->putValue('franchise_create_nonce', $nonce);

		$step = 'package';
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}

		//options data and default
		$fields['code']['options'] = $package_details;
		$fields['country']['options'] = $this->country_repo->all_member_selectable_country();
		$fields['phone_code']['options'] = $this->country_repo->all_member_selectable_phone_code();
 		$fields['__nonce']['value'] = $nonce;

        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {

			// need to have 7 accounts with package at least DM-05
			if (!$this->isHighestPackageCountQualifyByUserId($user->id, $user_table)) {
				$v = validator([], []);
				$v->getMessageBag()->add('all', trans('general.page.user.franchise-account-register.errmsg.7-accounts-full-package', ["wallet" => trans("general.wallet.fwallet")]));
				return back()->with(['__step'=>$step])->withInput()->withErrors($v);
			}

            $data = request()->all();

			//preset data
			$data['code'] = isset($data['code'])?$data['code']:'';
			
			$package_codes = array_keys($package_details);
			$niceNames = [
				'name' => trans('field.user.name'),
				'email' => trans('field.user.email'),
				'username' => trans('field.user.username'),
				'password' => trans('field.user.password'),
				'password2' => trans('field.user.password2'),
				'mobile' => trans('field.user.mobile'),
				'nid' => trans('field.user.nid'),
				'ref' => trans('field.user.ref'),
				'up' => trans('field.user.up'),
				'up_pos' => trans('field.user.up_pos'),
				'code' => trans('field.activation.code-name'),
				'address1' => trans('field.user.address1'),
				'address2' => trans('field.user.address2'),
				'city' => trans('field.user.city'),
				'zip' => trans('field.user.zip'),
				'state' => trans('field.user.state'),
				'country' => trans('field.user.country'),
				'epassword' => trans('field.user.epassword'),
			];

			$niceMessages = [
				"username.regex" => trans('validation.username.pattern', ['attribute' => $niceNames['username']]),
				"password.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password']]),
				"password2.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password2']]),
				"mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
			];
			
			//select package
			if ($data['__req'] == 'package') {
				$v = validator($data, [
					'code' => 'required|in:'.implode(',', $package_codes),
				], [], $niceNames);
				if ($v->fails())
					return back()->withInput()->withErrors($v);

				$step = "start";
			}
			//processing
			elseif ($data['__req'] == '1') {
				$step = "start";

				//get package details
				$package =  isset($package_details[$data['code']])?$package_details[$data['code']]:null;
				
				$use_wallet["awallet"] = round($package['price'] * 50 / 100, 2);
				$use_wallet["fwallet"] = round($package['price'] - $use_wallet["awallet"], 2);

				//get user id
				$ref = Member::where('username', '=', $data['ref'])->first();
				$data['ref'] = $ref?$ref->id:'0';
				$up = Member::where('username', '=', $data['up'])->first();
				$data['up'] = $up?$up->id:'0';

				$v = validator($data, [
					'username' => 'required|alpha_num|min:4|max:100|regex:[^(?=.*[a-zA-Z])([a-zA-Z0-9]+)$]|unique:'.$user_table,
					'email' => 'required|email|max:255',
					'mobile' => 'required|min:1|digits_between:6,100|regex:/^[0-9]+$/',
					'address1' => 'max:100',
					'address2' => 'max:100',
					'city' => 'max:100',
					'zip' => 'max:10',
					'state' => 'max:100',
					'ref' => 'required|exists:'.$user_table.',id,primary_id,0',
					'up' => 'required|exists:'.$user_table.',id',
					'up_pos' => 'required|in:1,2',
					'code' => 'required|in:'.implode(',', $package_codes),
					'password' => 'required|min:8|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]|confirmed',
					'password2' => 'required|min:8|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]|confirmed',
					'epassword' => 'required',
				], $niceMessages, $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames, $use_wallet) {
					$data = $validator->getData();
					
					//check password
					$user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);
                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						}
						else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
					}
				
					//get auth user id
					$user_id = $user->id;
					
					//check network
					if (!Member::isDownlineOf($data['up'], $data['ref'])) {//Member::isDownlineOf($data['ref'], $data['up']) || 
						$validator->errors()->add('up', trans('validation.network.not-downline', ['attribute1' => $niceNames['up'], 'attribute2' => $niceNames['ref']]));
					}
					elseif (!Member::isDownlineOf($data['ref'], $user_id)) {
						$validator->errors()->add('ref', trans('validation.network.not-your-downline', ['attribute' => $niceNames['ref']]));
					}
					
					//check placement taken
					$up = Member::where('up', '=', $data['up'])
						->where('up_pos', '=', $data['up_pos'])
						->first();
					if ($up) {
						$validator->errors()->add('up_pos', trans('validation.unique', ['attribute' => $niceNames['up_pos']]));
					}

					$user_wallets = $this->walletRepo->getWalletsByUserId($user->id);
					
					//check wallet
					foreach ($use_wallet as $_wallet=>$_amount) {
						if ($user_wallets->$_wallet < $_amount) {
							$validator->errors()->add('wallet', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$_wallet), 'required' => number_format($_amount, 2), 'balance' => number_format($user_wallets->$_wallet, 4)]));
						}
					}
				});
				
				if ($v->fails())
					return back()->with([
						'__step' => $step,
						'__use_wallet' => $use_wallet,
					])->withInput()->withErrors($v);
	
				//check next step 
				$step = "preview";
				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else {
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

					try {
						DB::beginTransaction();
						//add member
						$new_user = Member::create([
							'itype' => $initiator['itype'],
							'iid' => $initiator['iid'],
							'editor_type' => $initiator['itype'],
							'editor_id' => $initiator['iid'],
							'name' => $franchise_main_user->name,
							'email' => $data['email'],
							'nid' => $franchise_main_user->nid,
							'username' => $data['username'],
							'address1' => $data['address1'],
							'address2' => $data['address2'],
							'city' => $data['city'],
							'zip' => $data['zip'],
							'state' => $data['state'],
							'country' => $franchise_main_user->country,
							'mobile' => $data['mobile'],
							'ref' => $data['ref'],
							'up' => $data['up'],
							'up_pos' => $data['up_pos'],
							'franchise_user_id' => $franchise_main_user->id,
							'password' => bcrypt($data['password']),
							'password_ori' => $data['password'],
							'password2' => bcrypt($data['password2']),
							'password2_ori' => $data['password2'],
							'primary_acc' => Member::$primary_to_code['yes'], //yes
							'status' => Member::$status_to_code['active'], //take active status
						]);

						// set token
						$new_user->forceFill([
							'remember_token' => str_random(60),
						])->save();
						
						//create wallets for user
						$this->walletRepo->createWalletsByUserId($new_user->id);

						$new_user->createSubAccount($new_user);

						$proceed = $new_user?true:false;

						if ($proceed) {    
							//deduct wallet
							foreach($use_wallet as $_wallet=>$_amount){
								$descr = "Package Purchased code (".$data['code'].") for User ID #".$new_user->id.".";
								$descr_json = [
									'wallet' => $_wallet,
									'amount' => $_amount,
									'uid' => $new_user->id,
									'username' => $new_user->username,
									'package_code' => $data['code'],
									'trans-type' => 'purchase',
								]; 

								$record = $user->deductWallet($user->id, $_wallet, $_amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
								if(!$record['status']){
									break;
								}
							}

							//Log Action
							$this->setIdentifiers($new_user->id);
							$log_msg = "User Created for User ID #".$new_user->id.".";
							Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['name'=>$franchise_main_user->name, 'email'=>$data['email'], 'username'=>$data['username'], "address1" => $data["address1"], "address2" => $data["address2"], "city" => $data["city"], "zip" => $data["zip"], "state" => $data["state"], 'country'=>$franchise_main_user->country, 'nid'=>$franchise_main_user->nid, 'franchise_user_id'=>$franchise_main_user->id, 'status'=> Member::$status_to_code['active'], 'primary_acc'=>Member::$primary_to_code['yes'], 'mobile'=>$data['mobile'], 'ref'=>$data['ref'], 'up'=>$data['up'], 'up_pos'=>$data['up_pos']], 'uid'=>$new_user->id]);

							$proceed = $record['status'];
						}
						
						if ($proceed) {
							//add activation
							$result = $user->addActivation($new_user->id, $data['code'], $user->id, ['use_wallet_json'=>$use_wallet]);
						}
					} catch (Exception $e) {
						//throw something here
						DB::rollBack();
						//throw $e;
						
						$v->getMessageBag()->add('all', trans('general.page.user.register.errmsg.register-error'));
						return back()->withInput()->withErrors($v);
					}
					DB::commit();
					
					return $view->with([
						'__fields' => $fields, 
						'__step' => $step, 
						'__user' => $user, 
						'__new_user' => $new_user, 
						'__activation' => $result['activation'],
						'__use_wallet' => $use_wallet, 
					])->with('success', trans('general.page.user.register.msg.register-success'));
				}
			}
			
			return $redirect->with([
				'__step' => $step,
				'__use_wallet' => $use_wallet, 
			])->withInput();
        }

        return $view->with([
			'__fields' => $fields, 
			'__step' => $step, 
			'__user' => $user, 
			'__franchise_main_user' => $franchise_main_user,
			'__use_wallet' => $use_wallet, 
		]);
    }

    public function upgradeTopUp()
    {
		$_method =  __METHOD__;

		$user = $this->user;

		//set variable
		$view = view('member.franchise.upgrade-topup');
		$user_table = $user->getTable();
		$package_details = Activation::$package_code_to_pack;
		$field_list = ['code', 'username', 'epassword'];
        $highest_package_id = 0;
		$use_wallet = [];

        //franchise user list
        $franchise_user_list = Member::getFranchiseAccountByUserID($user->id, false);
        
        $franchise_username_list = [];
        foreach ($franchise_user_list["main"] as $key=>$value) {
            $franchise_username_list[$value["username"]] = $value["username"];
        }
        foreach ($franchise_user_list["franchise"] as $key=>$value) {
            $franchise_username_list[$value["username"]] = $value["username"];
        }
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('franchise_upgrade_topup_nonce');
        $nonce = str_random(60);
        $session_key->putValue('franchise_upgrade_topup_nonce', $nonce);

		$step = 'user'; 

		//options data and default
 		$fields['username']['options'] = $franchise_username_list;
 		$fields['__nonce']['value'] = $nonce;

        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
			$data_raw = $data = request()->all();
			
			if ($data["__req"] != "back_user") {
				$user_table = $user->getTable();
				$package_codes = array_keys($package_details);
					
				$niceNames = [
					'code' => trans('field.activation.code-name'),
					'username' => trans('field.user.username'),
					'epassword' => trans('field.user.epassword')
				];
				
				//STEP 1: get user page
				//get user id

				$v = validator($data, [
					'username' => 'required|in:'.implode(",", array_keys($franchise_username_list)).'|exists:'.$user_table.',username',
					'epassword' => 'required',
				], [], $niceNames);
				
                //custom validation
                $error_list = [];

                //check password
				$epassword_nonce = session("epassword_nonce", []);
				if (!is_array($epassword_nonce)) {
					$epassword_nonce = [];
				}
				if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
					if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
						if (isset($data['__confirm']) && $data['__confirm'] == 1) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						}
					}
					else {
						$error_list[] = ['epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']])];
					}
				}
				else {
					$epassword_nonce[] = str_random(60);
					if (count($epassword_nonce) > 50) {
						array_shift($epassword_nonce);
					}
					session()->put("epassword_nonce", $epassword_nonce);
				}
                
                $v->after(function($validator) use ($error_list)
                {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });
				
				if ($v->fails())
					$view = $view->withErrors($v);
				
				if (!$v->fails()) {
				    //custom validation part 2
                    $to_user = Member::where("username", $data["username"])->first();
					
					// check non settled package
					$non_settle_package_found = Activation::where("uid", $to_user->id)
						->whereIn("act_type", [Activation::$act_type_to_code["contra"], Activation::$act_type_to_code["special"]])
						->where("status", Activation::$status_to_code["confirmed"])
						->where("loan_status", Activation::$loan_status_to_code["pending"])
						->exists();
						
					if ($non_settle_package_found) {
						return back()->withInput()->withErrors(collect(["User already had non-settled package."]));
					}

					// need to have 7 accounts with package at least DM-05
					if (!($to_user->primary_acc == Member::$primary_to_code["yes"] && $to_user->id == $user->id) &&
					!($to_user->primary_acc != Member::$primary_to_code["yes"] && $to_user->primary_id == $user->id) && 
					!$this->isHighestPackageCountQualifyByUserId($user->id, $user_table)) {
						$error_list[] = ['username', trans('general.page.user.franchise-account-topup.errmsg.7-accounts-full-package', ["wallet" => trans("general.wallet.fwallet")])];
					}

					//filter package purchase-able
					$highest_package = Activation::getHighestPackageByUser($to_user->id, "all");
					if ($highest_package) {
						$highest_package_rank = $highest_package->code;
						$highest_package_id = $highest_package->id;
					}
					else{
						$highest_package_rank = 0;
						$highest_package_id = 0;
					}
					$fields['code']['options'] = Activation::getUpgradablePackageByUser($to_user->id, $highest_package_rank);
					
					if (empty($fields['code']['options'] )) {
						$error_list[] = ['username', trans('validation.upgrade-topup.full-rank', ['attribute'=>$niceNames['username']])];
                    }

					if ($data['username'] && !Member::isDownlineOf($to_user->id, $user->id)) {
						$error_list[] = ['username', trans('validation.network.not-your-downline', ['attribute' => $niceNames['username']])];
					}
                
                    $v->after(function($validator) use ($error_list) {
                        foreach ($error_list as $key=>$value) {
                            $validator->errors()->add($value[0], $value[1]); 
                        }
                    });
                
                    if ($v->fails())
                        $view = $view->withErrors($v);
				}

				if (!$v->fails())
					$step = "package";
			}

			//Step 2: get package
			if ($data['__req'] == '1' && !$v->fails()) {

				//get package details
				$data['code'] = isset($data['code'])?$data['code']:'';
				$package =  isset($fields['code']['options'][$data['code']])?$fields['code']['options'][$data['code']]:null;
				
				$use_wallet["awallet"] = round($package['price'] * 50 / 100, 2);
				$use_wallet["fwallet"] = round($package['price'] - $use_wallet["awallet"], 2);

				$v = validator($data, [
					'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'])),
				], [], $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames, $use_wallet) {
					$data = $validator->getData();
					
					$user = $this->user;
					$user_wallets = $this->walletRepo->getWalletsByUserId($user->id);

					//check wallet
					foreach ($use_wallet as $_wallet=>$_amount) {
						if ($user_wallets->$_wallet < $_amount) {
							$validator->errors()->add('wallet', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$_wallet), 'required' => number_format($_amount, 2), 'balance' => number_format($user_wallets->$_wallet, 4)]));
						}
					}
					
					//dd($validator);
				});
				
				if ($v->fails())
					$view = $view->withErrors($v);
				
				if (!$v->fails()) {
					//check next step 
					$step = "preview";
					if (isset($data['__confirm'])) {
						if ($data['__confirm'] == 1) {
							$step = "confirmed";
						}
						else{
							$step = "package";
						}
					}
				}
				
				if ($step == "confirmed") {
					//deduct wallet
					foreach ($use_wallet as $_wallet=>$_amount) {
						$descr = "Package Purchased code (".$data['code'].") for User ID #".$to_user->id.".";
						$descr_json = [
							'wallet' => $_wallet,
							'amount' => $_amount,
							'uid' => $to_user->id ,
							'username' => $to_user->username,
							'package_code' => $data['code'],
							'trans-type' => 'purchase',
						]; 

						$record = $user->deductWallet($user->id, $_wallet, $_amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
						if (!$record['status']) {
							break;
						}
					}
					if ($record['status']) {
						//add activation
						$result = $user->addActivation($to_user->id, $data['code'], $user->id, ['use_wallet_json'=>$use_wallet, 'topup_activation_id'=>$highest_package_id, 'topup'=>true]);
					}
					
					$view = $view->with(['__activation'=>$result['activation']])->with('success', trans('general.page.user.upgrade-topup.msg.upgrade-topup-success'));
				}
			}
        }

		//set field value
		foreach ($field_list as $_field) {
			$fields[$_field]['value'] = isset($data_raw[$_field])?$data_raw[$_field]:"";
		}
        
        return $view->with([
			'__fields' => $fields, 
			'__step' => $step, 
			'__user' => $user, 
			'__use_wallet' => $use_wallet, 
			'__package_details' => $package_details, 
			"__highest_package_id" => $highest_package_id
		]);
    }

    public function franchiseTransfer()
    {
        $wallet = "fwallet";
        $user = $this->user;
        $user_table = $user->getTable();
        
		$view = view('member.franchise.internal-transfer');
		$redirect = redirect()->action('Member\FranchiseController@franchiseTransfer');

        $step = "start";
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('internal_transfer_nonce');
        $nonce = str_random(60);
        $session_key->putValue('internal_transfer_nonce', $nonce);

        //franchise user list
        $franchise_user_list = Member::getFranchiseAccountByUserID($user->id, true);
        
        $franchise_username_list = [];
        foreach ($franchise_user_list["main"] as $key=>$value) {
            $franchise_username_list[$value["username"]] = $value["username"];
        }
        foreach ($franchise_user_list["franchise"] as $key=>$value) {
            $franchise_username_list[$value["username"]] = $value["username"];
        }

		//options data and default
 		$fields['from_username']['options'] = $franchise_username_list;
 		$fields['to_username']['options'] = $franchise_username_list;
 		$fields['__nonce']['value'] = $nonce;
        
        //request handling
        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
            $data = request()->all();

            if ($data['__req']=='1') {

                $niceNames = [
                    'from_username' => trans('field.wallet_record.from-username'),
                    'to_username' => trans('field.wallet_record.to-username'),
                    'amount' => trans('field.wallet_record.amount'),
                    'remarks' => trans('field.wallet_record.remarks'),
                    'epassword' => trans('field.user.epassword')
                ];
                $v = validator($data, [
					'from_username' => 'required|exists:'.$user_table.',username,primary_id,0|in:'.implode(",", array_keys($fields['from_username']['options'])),
					'to_username' => 'required|exists:'.$user_table.',username,primary_id,0|in:'.implode(",", array_keys($fields['to_username']['options'])),
					'amount' => 'required|min:0.01|numeric',
					'remarks' => 'required|string|max:2000',
					'epassword' => 'required',
				], [], $niceNames);
                

                //custom validation
                $error_list = [];

                //check password
				$epassword_nonce = session("epassword_nonce", []);
				if (!is_array($epassword_nonce)) {
					$epassword_nonce = [];
				}
				if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
					if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
						unset($epassword_nonce[$key]);
						session()->put("epassword_nonce", $epassword_nonce);
					}
					else {
						$error_list[] = ['epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']])];
					}
				}
				else {
					$epassword_nonce[] = str_random(60);
					if (count($epassword_nonce) > 50) {
						array_shift($epassword_nonce);
					}
					session()->put("epassword_nonce", $epassword_nonce);
				}

                $v->after(function($validator) use ($error_list)
                {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });
                if ($v->fails())
                    return back()->withInput()->withErrors($v);

                //custom validation part 2
                $data['amount'] = round($data['amount'], 2);
                $error_list = [];

                $from_user = Member::where("username", $data["from_username"])->first();
				$from_user_wallets = $this->walletRepo->getWalletsByUserId($from_user->id);
                $to_user = Member::where("username", $data["to_username"])->first();

				// need to have 7 accounts with package at least DM-05
				if (!$this->isHighestPackageCountQualifyByUserId($from_user->id, $user_table)) {
					$v = validator([], []);
					$v->getMessageBag()->add('all', trans('general.page.user.wallet-internal-transfer.errmsg.7-accounts-full-package', ["from_username" => $data["from_username"]]));
					return back()->withInput()->withErrors($v);
				}

                //check wallet
                if ($data['amount'] && $from_user_wallets->$wallet < $data['amount']) {
                    $error_list[] = ['amount', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$wallet), 'required' => number_format($data['amount'], 2), 'balance' => number_format($from_user_wallets->$wallet, 4)])];
                }
                elseif ($data["to_username"] == $data["from_username"]) {
                    $error_list[] = ['to_username', trans('general.page.user.wallet-internal-transfer.errmsg.same-account-wallet')];
                }
                
                $v->after(function($validator) use ($error_list)
                {
                    foreach ($error_list as $key=>$value) {
                        $validator->errors()->add($value[0], $value[1]); 
                    }
                });
                if ($v->fails())
                    return back()->withInput()->withErrors($v);
                
				//check next step 
				$step = "preview";
				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else{
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

                    //transfer
                    $result = $user->walletTransfer($from_user->id, $to_user->id, $data['amount'], $wallet, $wallet, $data['remarks']);

                    //dd($data);
                    
                    return $view->with(['__wallet' => $wallet, '__step'=>$step, '__franchise_user_list'=>$franchise_user_list, '__fields'=>$fields, '__post' => $data])->with('success', trans('general.page.user.wallet-internal-transfer.msg.transfer-success'));
                }
            }

			return $redirect->with(['__step'=>$step])->withInput();
        }

        return $view->with(['__wallet' => $wallet, '__step'=>$step, '__franchise_user_list'=>$franchise_user_list, '__fields'=>$fields]);
    }
}
