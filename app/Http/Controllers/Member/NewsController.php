<?php

namespace App\Http\Controllers\Member;

use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class NewsController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    public function index()
    {
        $news = News::getAllAvailable()
            ->orderBy('start_date', 'desc')
            ->paginate(20);

        return view('member.news.index', [
            'news' => $news
        ]);
    }

    public function show($id)
    {
        $news = News::findOrFail($id);

        if ($news->status == 0 || ($news->end_date != null && $news->end_date <= Carbon::today())) {
            abort(404);
        }

        return view('member.news.view', [
            'news' => $news
        ]);
    }
}
