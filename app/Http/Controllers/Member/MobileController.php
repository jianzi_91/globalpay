<?php

namespace App\Http\Controllers\Member;

use DB;
use Exception;
use Illuminate\Validation\ValidationException;

use App\Models\Member;
use App\Models\Code;
use App\Models\SessionKeyValue;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

class MobileController extends Controller
{
    public function verify()
    {
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

        $user = $this->user;

		//set variable
		$view = view('member.mobile.verify');

        if (request()->isMethod('post') && request()->has('__req')) {
            $data = request()->all();
		
			$niceNames = [
				'verification_code' => trans('field.code.verification_code'),
				'epassword' => trans('field.user.epassword'),
			];

			$niceMessages = [];
			
			if ($data['__req'] == '1') {
				$step = "start";

				$v = validator($data, [
					'verification_code' => 'required',
					'epassword' => 'required',
				], $niceMessages, $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					
					//check password
					$user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);
                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
					}
				});
				
				if ($v->fails()) {
                    return back()->withInput()->withErrors($v);
                }

                // check tac
                $code_verify = Code::checkTac(request("verification_code"), $user->mobile, "verify_mobile");
                if (!$code_verify["status"]) {
                    $message = "";
                    if ($code_verify["msg"] == "not found") {
                        $message = trans("sms_tac.error.not_found", ["attribute" => trans("field.code.verification_code")]);
                    }
                    elseif ($code_verify["msg"] == "not matched") {
                        $message = trans("sms_tac.error.not_matched", ["attribute" => trans("field.code.verification_code")]);
                    }
                    $validator = validator([], []);
                    $validator->after(function ($validator) use ($message) {
                        $validator->errors()->add("verification_code", $message);
                    });
				
                    if ($validator->fails()) {
                        return back()->withInput()->withErrors($validator);
                    }
                }

                try {
                    DB::beginTransaction();
                    
                    $user->memberStatus->mobile_status = 10;
                    $user->memberStatus->save();
                } catch (Exception $e) {
                    //throw something here
                    DB::rollBack();
                    
                    $v->getMessageBag()->add('all', trans('general.page.user.mobile_verify.errmsg.mobile-verify-error'));
                    return back()->withInput()->withErrors($v);
                }
                DB::commit();
                
                return back()->with('success', trans('general.page.user.mobile_verify.msg.mobile-verify-success'));
            }
        }
		
        return $view->with([
			'__user' => $user,
		]);
    }
}
