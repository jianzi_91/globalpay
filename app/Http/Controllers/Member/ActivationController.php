<?php

namespace App\Http\Controllers\Member;

use App\Models\Activation;
use App\Models\Member;
use App\Utils\DateTimeTool;

class ActivationController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    public function purchaseList()
    {
        $user = \Auth::user();
        $record = Activation::where('aid', $user->id)
            ->where('status', Activation::$status_to_code['confirmed'])
            ->orderBy('created_at', 'desc')
            ->paginate(20);
       
        foreach ($record as $key=>$value) {
            $user = Member::find($value['uid']);
            $record[$key]['uid-username'] = ($user?$user->username:'-');
            unset($user);
            $record[$key]['created_at_op'] = DateTimeTool::systemToOperationDateTime($value['created_at']);
        }
        return view('member.activation.purchase-list', ['__records'=>$record]);
    }

    public function activateList()
    {
        $user = \Auth::user();
        $activation = new Activation();
        $activation->setTable($activation->getTable()." AS act");

        $record = $activation->join($user->getTable()." AS user", "act.uid", "=", "user.id")
                ->where(
                    function($query) use ($user){
                        $query->where('user.primary_id', $user->id)
                            ->orWhere('user.id', $user->id);
                    }
                )
                ->where('act.status', Activation::$status_to_code['confirmed'])
                ->orderBy('act.created_at', 'desc')
            ->select('act.*')
            ->paginate(20);
        
        foreach($record as $key=>$value){
        //    $value = json_decode(json_encode($value), true);
        //    $record[$key] = json_decode(json_encode($record[$key]), true);
            $user = Member::find($value['uid']);
            $record[$key]['uid-username'] = ($user?$user->username:'-');
            unset($user);
            $record[$key]['created_at_op'] = DateTimeTool::systemToOperationDateTime($value['created_at']);
        }
        return view('member.activation.activate-list', ['__records'=>$record]);
    }

    public function groupSalesList()
    {
        $user = \Auth::user();
        $user_table = $user->getTable();
        $network = 'placement';
        $suser = null;
        $view = view('member.activation.group-sales-list');

        //searching data
		$searching['yes'] = false;
		$searching['refresh_url'] = request()->url();

		//search fields
		$search_fields = [
			'uid' => [ 'label'=>trans('field.user.username'), 'value' => old('uid')],
			'up_pos' => ['label'=>trans('field.user.up_pos'), 'value' => old('up_pos')],

			'date_from' => ['label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.activation.created_at')]), 'value'=>old('date_from')],
			'date_to' => ['label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.activation.created_at')]), 'value'=>old('date_to')]
		];

        //validation
        $data = request()->all();
        if (request()->has('__src') && request()->isMethod('get')) {

            //change username to uid
			$uid = Member::where('username', '=', $data['uid'])->first();
			$data['uid'] = $uid?$uid->id:'0';

            $niceNames = array(
                'uid' => trans('field.user.username'),
				'up_pos' => trans('field.user.up_pos'),
				'date_from' => trans('general.search_field.field.date-from', ['field'=>trans('field.activation.created_at')]),
				'date_to' => trans('general.search_field.field.date-to', ['field'=>trans('field.activation.created_at')]),
            );
            $v = \Validator::make($data, [
                    'uid' => 'required|exists:'.$user_table.',id',
					'up_pos' => 'required|in:1,2',
					'date_from' => 'date_format:Y-m-d',
					'date_to' => 'date_format:Y-m-d',
                ], [], $niceNames);
            
            //custom validation
            $err_list = [];
                
            //check network
            if ($data['uid'] && !Member::isDownlineOf($data['uid'], $user->id, $network)) {
                $err_list = ['uid', trans('validation.network.not-your-downline', ['attribute' => $niceNames['uid']])];
            }

            $v->after(function($validator) use ($err_list) {
                if(!empty($err_list)){
                    for($i=0; $i<count($err_list); $i++){
                        $validator->errors()->add($err_list[0], $err_list[1]);
                    }
                }
            });
            
            if ($v->fails())
                return back()->withInput()->withErrors($v);
		    
            $suser = Member::where('up', $data['uid'])->where('up_pos', $data['up_pos'])->first();
            
            //check if user exist
            if (!$suser) {
                $err_list = ['up_pos', trans('validation.network.no-position-downline', ['attribute' => $niceNames['uid'], 'position_attribute' => $niceNames['up_pos']])];
            }

            $v->after(function($validator) use ($err_list) {
                if (!empty($err_list)) {
                    for ($i=0; $i<count($err_list); $i++) {
                        $validator->errors()->add($err_list[0], $err_list[1]);
                    }
                }
            });

            if ($v->fails())
                return back()->withInput()->withErrors($v);
            
            //to view and pagination
			$data = request()->only(array_merge(['__src'], array_keys($search_fields)));
            foreach ($search_fields as $key=>$value) {
                $search_fields[$key]["value"] = request()->get($key);
            }
			$searching['yes'] = true;
        }
		
        //list build
        $record = null;
        if ($suser) {
            $suserid_list = array_merge([$suser->id], Member::getAllDownlineId($suser->id));
            
            //get group sales
            $record = Activation::whereIn('uid', $suserid_list)
                ->where('status', Activation::$status_to_code['confirmed']);
            if ($data['date_from']) {
                 $record = $record->where("created_at", ">=", DateTimeTool::operationToSystemDateTime($data['date_from']." 00:00:00"));
            }
            if ($data['date_to']) {
                 $record = $record->where("created_at", "<=", DateTimeTool::operationToSystemDateTime($data['date_to']." 23:59:59"));
            }
            $record = $record->orderBy("created_at", "desc")
                ->orderBy('id', 'desc')
                ->paginate(20);
            
            //pagination link appends
            $record->appends($data);

            foreach ($record as $key=>$value) {
                $user = Member::find($value['uid']);
                $record[$key]['uid-username'] = ($user?$user->username:'-');
                unset($user);
                $record[$key]['created_at_op'] = DateTimeTool::systemToOperationDateTime($value['created_at']);
            }
        }
        return $view->with(['__records'=>$record, '__search_fields'=>$search_fields, '__searching'=>$searching]);
    }
}
