<?php

namespace App\Http\Controllers\Member;

use DB;
use GuzzleHttp\Client;
use JWTFactory;
use JWTAuth;
use Hash;

use App\Models\ManualWithdrawal;
use App\Models\Member;
use App\Models\MemberRelationship;
use App\Models\Wallet;
use App\Models\News;
use App\Models\TransactionLog;
use App\Utils\DateTimeTool;

class WithdrawalController extends Controller
{
    public function index()
    {
        return view('member.cashout.cashout');
    }

    public function withdrawal_confirm()
    {
        $data = request()->all();
        $user = $this->user;

        $manualwithdrawal_info = ManualWithdrawal::where('uid', $user->id)->where('status', false)->first();

        if ($manualwithdrawal_info) {
            return back()->withInput()->withErrors(trans('validation.cashout_in_progress'));
        }

        foreach ($data as $key => $value) {
			if ($value == '') {
				$data[$key] = null;
			} else {
				$data[$key] = trim(strip_tags($value));
			}
		}

        $arr_validation = [
            'amount' => 'required|integer|min:1',
            'security_code' => 'required',
        ];

        $arr_nicenames = [
            'amount' => trans('two.amount'),
            'security_code' => trans('two.security_code')
        ];

        $validator = validator($data, $arr_validation, [], $arr_nicenames);
 
        $validator->after(function($validator) use ($user, $arr_nicenames, $data) {
            if (!empty($data['security_code']) && !Hash::check($data['security_code'], $user->password2)) {
                $validator->errors()->add('security_code', trans('validation.security_password_notmatch'));
            }

            if ($user->wallet->insufficientWallet('dowallet', $data['amount']) == false) {
                $validator->errors()->add('insufficient_cash_point', trans('validation.insufficient_dowallet'));
            }

            if ($data['amount'] < 10) {
                $validator->errors()->add('min_withdrawal', trans('validation.min_withdrawal_10'));
            }

            if ($data['amount'] % 10 != 0 ) {
                $validator->errors()->add('amount', trans('validation.unmatch.repeat_transfer_value_multiply_10'));
            }

        });

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $receivable_amount = $data['amount'] * 0.8;
        $commission_amount = $data['amount'] * 0.2;

        return view('member.cashout.cashout-confirm', compact("receivable_amount", "commission_amount"));
    }

    public function withdrawal_process()
    {
        $data = request()->all();
        $user = $this->user;

        foreach ($data as $key => $value) {
			if ($value == '') {
				$data[$key] = null;
			} else {
				$data[$key] = trim(strip_tags($value));
			}
		}

        $arr_validation = [
            'receivable_amount' => 'required|integer|min:1',
            'commission_amount' => 'required|integer|min:1',
        ];

        $arr_nicenames = [
            'receivable_amount' => trans('field.withdrawal.receivable_amount'),
            'commission_amount' => trans('field.withdrawal.commission_amount'),
        ];

        $validator = validator($data, $arr_validation, [], $arr_nicenames);

        if ($validator->fails()) {
            return redirect()->route('member-withdrawal')->withInput()->withErrors($validator);
        }

        DB::transaction(function() use ($user, $data) {
            // convert into variables
            extract($data);

            $manualwithdrawal = ManualWithdrawal::create([
                'uid' => $user->id,
                'receiver_uid' => config('app.company_main_account_id'),
                'receivable_amount' => $data['receivable_amount'],
                'commission_amount' => $data['commission_amount'],
                'status' => ManualWithdrawal::WITHDRAWAL_PENDING,
                'creator_user_type' => $user::$user_code,
                'creator_uid' => $user->id,
                'editor_user_type' => $user::$user_code,
                'editor_uid' => $user->id,
            ]);

            Wallet::adjust([
                'uid' => $user->id,
                'trans_code' => Wallet::CASHOUT_MEMBER,
                'type' => 'manualwithdrawal-member',
                'source_id' => $manualwithdrawal->id,
                'creator_user_type' => $user::$user_code,
                'creator_uid' => $user->id,
                'do_amt' => -($data['receivable_amount'] + $data['commission_amount']),
            ]);
    
        });

        return redirect('member/withdrawal')->withSuccess(trans('field.withdrawal.success'));
    }

    public function withdrawal_history()
    {
        $user = $this->user;
        $manualwithdrawals = ManualWithdrawal::where('uid', $user->id)->get();

        if (!$manualwithdrawals->isEmpty()) {
            $manualwithdrawals = $manualwithdrawals->each(function($item) {
                // $item->created_at = DateTimeTool::systemToOperationDateTime($item->created_at, 'Y-m-d H:i');
                $item->status = ManualWithdrawal::statusTypes()[$item->status];
                
                return true;
            });
        }

        return view('member.cashout.cashout-history', compact('manualwithdrawals'));
    }
}