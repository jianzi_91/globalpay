<?php

namespace App\Http\Controllers\Member;

use DB;
use GuzzleHttp\Client;
use JWTFactory;
use JWTAuth;

use App\Models\Member;
use App\Models\MemberRelationship;
use App\Models\Wallet;
use App\Models\News;
use App\Models\TransactionLog;
use App\Utils\DateTimeTool;

class MallProductController extends Controller
{
    public function index() {

        $lang = app()->getLocale();

        switch ($lang) {
            case 'zh-cn':
                $lang = 'cn';
                break;
            
            default:
                $lang = 'en';
                break;
        }

        $client = new Client();
        $res = $client->request('GET', config('app.gpmall_product_listing_url'), [
            'query' => [
                'country_code' => 'CN',
                'lang' => $lang,
                ]
        ]);

        $products = json_decode($res->getBody()->getContents(), true)['data'];

        return view('member.mall.product_listing', compact("products"));
    }

    public function product_details()
    {
        $user = $this->user;
        $access_token = JWTAuth::fromUser($user);
        $product_id = request('id');

        $product_detail_url = config('app.gpmall_product_details_url');

        return view('member.mall.product_details', compact("product_detail_url", "access_token", "product_id"));
    }

    public function transaction_history()
    {
        $user = $this->user;

        $transaction_list = TransactionLog::where('uid', $user->id)->where('type', 'purchase')->get();

        return view('member.mall.transaction-history', compact("transaction_list"));
    }

    public function transaction_details()
    {
        $user = $this->user;
        $access_token = JWTAuth::fromUser($user);
        $transaction_id = request('transaction_id');

        $transaction_detail_url = config('app.gpmall_product_details_url');

        return view('member.mall.transaction_details', compact("transaction_detail_url", "access_token", "transaction_id"));
    }
}