<?php

namespace App\Http\Controllers\Member;

use Auth;
use Carbon\Carbon;
use DateTime;
use DB;
use Hash;
use Response;
use Request;
use Storage;
use Validator;

use App\Models\Member;
use App\Models\MemberStatus;
use App\Models\Wallet;
use App\Models\TradeBuy;
use App\Models\TradeQueue;
use App\Models\TradeSell;
use App\Models\TradeSetting;

use App\Trades\TradeService;

use App\Utils\DateTimeTool;

use Illuminate\Validation\ValidationException;

use Intervention\Image\ImageManagerStatic as Image;
use Yajra\Datatables\Datatables;

class TradeController extends Controller
{
    public function __construct(TradeService $tradeService)
    {
        Image::configure(array('driver' => 'imagick'));

        $this->tradeService = $tradeService;
        $this->restricted = true;

        $this->setting = $this->tradeService->getTradeSetting();

        $dt = Carbon::parse(DateTimeTool::operationDateTime());

        if ($dt->hour >= 10 && $dt->hour < 19) {
            $this->restricted = false;
        }
    }

    //---------- COINS BUYING FUNCTION ---------- //
    public function buy()
    {
        $sells = $this->tradeService->enterBuyingLobby();

        return view('member.trade.buy')->with('sells', $sells->paginate(5))->with('restricted', $this->restricted);
    }

    public function createTradeBuy()
    {
        $buyer = Auth::user();
        $curMarketPrice = $this->tradeService->getCurrentMarketPrice();

        $exist = $this->tradeService->getMemberCustomBuys($buyer->id);

        if ($exist->count() > 1) {
            return back()->with('error', trans('general.page.trade.error.multiple-trade'));
        }

        if (Request::isMethod('POST')) {

            $data = Request::only('min_qty','max_qty','payment');

            $v = Validator::make($data,[
                'min_qty' => 'required|min:'.config('trade.min_buy_limit').'|integer',
                'payment' => 'required|in:' . implode(',',$this->tradeService->getPaymentMethodArray()),
            ],[

            ],[
                'min_qty' => trans('general.page.trade.min-sell'),
                'max_qty' => trans('general.page.trade.max-sell'),
                'payment' => trans('general.page.trade.payment.method'),
            ]);

            $v->after(function($v) use ($data, $buyer){

                if ($data['payment'] == 'B' && $buyer->bank_acc_no == null) {
                    $v->errors()->add('error', trans('general.page.trade.error.bank-info-required'));
                }
                
            });

            if ($v->fails()) {
                return back()->withInput()->withErrors($v);
            }

            try {
                DB::beginTransaction();

                $data['price'] = $this->tradeService->getCurrentMarketPrice();

                $tradeSell = $this->tradeService->createTradeBuy($buyer->id, $data);

                DB::commit();

            } catch (Exception $e) {

                DB::rollback();

                return back()->with('errors', $e->getMessage());
            }

            return redirect()->action('Member\TradeController@sell')->with('success', trans('general.page.trade.success.sell-offer-created'))->with('curPrice', $curMarketPrice);
        }

        return view('member.trade.create-buy')->with('buyer', $buyer)->with('curPrice', $curMarketPrice);
    }

    public function buyRequest($orderId)
    {
        $tradeSell = $this->tradeService->getTradeSellById($orderId);
        $setting = $this->tradeService->getTradeSetting();

        // Prevent double queue
        $requests = $this->tradeService->getBuyRequestsForTradeSell($tradeSell->id);

        foreach ($requests as $request) {
            if ($request->buy_uid == Auth::user()->id) {
                return back()->with('error', trans('general.page.trade.error.tradebuy-queueing'));
            }
        }
        // End of prevention
        
        if (Request::isMethod('POST')) {

            $data = Request::only('unit');

            $v = Validator::make($data, [
                'unit' => 'required|numeric|min:' . $tradeSell->min_qty.'|max:' . $tradeSell->open_qty,
            ],[
                'unit.min' => trans('general.page.trade.error.min-buy', ['min_qty' => $tradeSell->min_qty]),
                'unit.max' => trans('general.page.trade.error.max-buy', ['max_qty' => $tradeSell->open_qty]),
            ]);

            if ($v->fails()) {
                return back()->withInput()->withErrors($v);
            }

            try {
                $buyer = Auth::user();

                $buyRequest = $this->tradeService->queueBuyingRequest($tradeSell->id, $buyer->id, $data['unit']);

                $buyRequest->update([
                    'STATUS' => TradeQueue::STATUS['PENDING'],
                ]);

                $tradeBuy = $buyRequest->tradeBuy;

                $tradeBuy->update([
                    'payment_method' => $tradeSell->payment_method,
                ]);

                return redirect()->route('my-trade-buy')->with('success', trans('general.page.trade.success.buy-request'));

            } catch (Exception $e) {

                return back()->with('errors', $e->getMessage());
            }
        }

        return view('member.trade.place-buying-offer', compact('tradeSell', 'setting'));
    }

    public function getMemberTradeBuy()
    {
        $requests = $this->tradeService->getQueueingBuys(Auth::user()->id);
        $customs = $this->tradeService->getMemberCustomBuys(Auth::user()->id);

        return view('member.trade.my-buying-offers', compact('requests','customs'));
    }

    public function buyDetails($queueId)
    {
        $queue = $this->tradeService->getTradeQueue($queueId);
        $ca_display = DateTimeTool::systemToOperationDateTime($queue->created_at);
        $ua_display = DateTimeTool::systemToOperationDateTime($queue->updated_at);

        if ($queue->tradeSell->payment_method == 'W') {
            $qr_code = Storage::disk('s3')->url('wechatpay/' . $queue->tradeSell->payment_ss);
        }
        elseif ($queue->tradeSell->payment_method == 'A') {
            $qr_code = Storage::disk('s3')->url('alipay/' . $queue->tradeSell->payment_ss);
        }
        else {
            $qr_code = null;
        }

        if ($queue->receipt) {
            $receipt = Storage::disk('s3')->url('receipts/' . $queue->receipt);

            if ($queue->status == 4) {
                $rated = $this->tradeService->checkMemberRatedTrade($queue->id, $queue->buy_uid);

                return view('member.trade.buy-details', compact('queue', 'receipt', 'ca_display', 'ua_display', 'rated', 'qr_code'));
            }

            return view('member.trade.buy-details', compact('queue', 'receipt', 'ca_display', 'qr_code'));
        }

        return view('member.trade.buy-details', compact('queue', 'ca_display', 'qr_code'));
    }

    public function payBuyRequest()
    {
        $data = Request::only('queueId', 'proof');

        $v = Validator::make($data, [
            'queueId' => 'required|exists:mlm_trade_queues,id',
        ]);

        if ($v->fails()) {
            return back()->withInput->withErrors($v);
        }    

        $queue = $this->tradeService->getTradeQueue($data['queueId']);

        $data['proof'] = Image::make($data['proof'])->encode('jpg', 75);
        
        $file_name = $queue->buyer->username . '_' . DateTimeTool::systemDateTime('Ymd') . '_' . str_random(6);
        $file_extension = $data['proof']->mime();

        try {
            DB::beginTransaction();

            $queue->update([
                'receipt' => $file_name . '.' . $file_extension,
                'status' => TradeQueue::STATUS['PAID'],
            ]);

            // $storeAs = $data['proof']->storeAs('receipts', $queue->receipt, 's3');

            $storeAs = Storage::disk('s3')->put('receipts/'.$queue->receipt, $data['proof']->__toString());

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
        
        return back()->with('success', trans('general.page.trade.success.paid'));
    }

    public function approveBuyRequest()
    {
        $data = Request::only('queue_id');

        $v = Validator::make($data, [
            'queue_id' => 'required|exists:mlm_trade_queues,id',
        ]);

        $tradeQueue = $this->tradeService->getTradeQueue($data['queue_id']);

        if ($tradeQueue->status == TradeQueue::STATUS['COMPLETE']) {
            return redirect()->route('my-trade-sell')->with('error', trans('general.page.trade.error.multiple-approve'));
        }

        if ($tradeQueue->status == TradeQueue::STATUS['CANCELLED']) {
            return redirect()->route('my-trade-sell')->with('error', trans('general.page.trade.error.multiple-cancel'));
        }

        $v->after(function($v) use ($data, $tradeQueue){

            if ($tradeQueue->sell_uid != Auth::user()->id) {
                $v->errors()->add('error', "Authentication failed!");
            }

            $seller = Auth::user();

            if ($seller->wallet->cwallet < $this->tradeService->coinsAfterCharges($tradeQueue->quantity)) {
                $v->errors()->add('error', trans('general.page.trade.error.insufficient_cwallet'));
            }

        });

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        try {
            DB::beginTransaction();

            $trans = $this->tradeService->executeTransaction($data['queue_id']);

            $receipt = Storage::disk('s3')->url('receipts/' . $tradeQueue->receipt);

            DB::commit();

            return redirect()->route('sell-details', ['queueId' => $tradeQueue->id])
                ->with('trans', $trans)
                ->with('queue', $tradeQueue)
                ->with('receipt', $receipt);
        }
        catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }

    public function rejectBuyRequest($queue_id)
    {
        $tradeQueue = $this->tradeService->getTradeQueue($queue_id);

        if ($tradeQueue->sell_uid != Auth::user()->id && $tradeQueue->buy_uid != Auth::user()->id) {
            abort(404);
        }

        if ($tradeQueue->status == TradeQueue::STATUS['PAID']) {
            return back()->with('error', trans('general.page.trade.error.cancel-paid-trade'));
        }

        if ($tradeQueue->status == TradeQueue::STATUS['COMPLETE']) {
            return redirect()->route('my-trade-buy')->with('error', trans('general.page.trade.error.multiple-approve'));
        }

        if ($tradeQueue->status == TradeQueue::STATUS['CANCELLED']) {
            return redirect()->route('my-trade-buy')->with('error', trans('general.page.trade.error.multiple-cancel'));
        }

        try {
            DB::beginTransaction();

            $queue = $this->tradeService->voidActiveQueue($queue_id);

            if ($tradeQueue->tradeBuy->type == 100) {
                $tradeBuy = $this->tradeService->endTradeBuy($tradeQueue->buy_id);
            }

            if ($tradeQueue->tradeSell->type == 100) {
                $tradeBuy = $this->tradeService->endTradeSell($tradeQueue->sell_id);
            }

            DB::commit();

            return redirect()->route('my-trade-buy')->with('success', trans('general.page.trade.success.buy-cancelled'));
        }
        catch (Exception $e) {
            DB::rollback();

            $receipt = Storage::disk('s3')->url('receipts/' . $tradeQueue->receipt);

            return back()->with('error', $e->getMessage());   
        }
    }

    public function stopTradeBuy()
    {
        $data = Request::only('buy_id');

        $v = Validator::make($data, [
            'buy_id' => 'required|exists:mlm_trade_buys,id',
        ]);

        $tradeBuy = $this->tradeService->getTradeBuyById($data['buy_id']);

        $v->after(function($v) use ($data, $tradeBuy){

            if ($tradeBuy->uid != Auth::user()->id) {
                $v->errors()->add('error', "Authentication failed!");
            }

        });

        try {
            DB::beginTransaction();

            $tradeBuy = $this->tradeService->endTradeBuy($data['buy_id']);

            if ($tradeBuy == null) {
                throw new Exception(trans('general.page.trade.error.cancel-buy'));
            }

            DB::commit();

            return redirect()->route('my-trade-buy')->with('success', trans('general.page.trade.success.buy-end'));
        }
        catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }

    public function viewTradeBuy($tradeBuyId)
    {
        $tradeBuy = $this->tradeService->getTradeBuyById($tradeBuyId);

        $requests = $this->tradeService->getSellRequestsForTradeBuy($tradeBuyId);

        $ca_display = DateTimeTool::systemToOperationDateTime($tradeBuy->created_at);

        return view('member.trade.view-trade-buy', compact('tradeBuy', 'requests', 'ca_display'));
    }
    //---------- END OF BUYING FUNCTION ----------//

    //---------- COIN SELLING FUNCTIONS ----------//
    public function sell()
    {
        $buys = $this->tradeService->enterSellingLobby();

        return view('member.trade.sell')->with('buys', $buys->paginate(5))->with('restricted', $this->restricted);
    }

    public function createTradeSell()
    {
        $seller = Auth::user();
        $curMarketPrice = $this->tradeService->getCurrentMarketPrice();

        $exist = $this->tradeService->getMemberCustomSells($seller->id);

        if ($exist->count() > 1) {
            return back()->with('error', trans('general.page.trade.error.multiple-trade'));
        }

        if (Request::isMethod('POST')) {
            $data = Request::only('min_qty', 'max_qty', 'payment', 'payment_ss', 'payment_id');

            $v = Validator::make($data,[
                'min_qty' => 'required|min:'.config('trade.min_sell_limit').'|integer',
                'max_qty' => 'required|min:'.$data['min_qty'].'|integer',
                'payment' => 'required|in:' . implode(',',$this->tradeService->getPaymentMethodArray()),
                'payment_ss' => 'required_if:payment,W,A',
                'payment_id' => 'required_if:payment,W,A',
            ],[
                'payment_ss.required_if' => trans('general.page.trade.error.payment-ss-required'),
                'payment_id.required_if' => trans('general.page.trade.error.payment-id-required'),
            ],[
                'min_qty' => trans('general.page.trade.min-buy'),
                'max_qty' => trans('general.page.trade.max-buy'),
                'payment' => trans('general.page.trade.payment.method'),
            ]);

            $v->after(function($v) use ($data, $seller){
                // Checking sellable amount due to the daily selling cap
                $total_sold = $this->tradeService->getTodayTotalSold();
                $selling_cap = $this->tradeService->getTodayTradingCap();
                $sellable = $selling_cap - $total_sold;

                if ($seller->company_status != true) {
                    if ($sellable <= 0) {
                        $v->errors()->add('error', trans('general.page.trade.error.selling-capped'));
                    }elseif ($data['max_qty'] > $sellable) {
                        $v->errors()->add('error', trans('general.page.trade.error.left-to-cap', ['left' => $sellable]));
                    }
                }
            });

            if ($v->fails()) {
                return back()->withInput()->withErrors($v);
            }

            if ($seller->company_status != true) {
                $v->after(function($v) use ($data, $seller){

                    $coins_required_to_sell = $this->tradeService->coinsAfterCharges($data['max_qty']);
                    $locked_coins = $this->tradeService->getMemberTotalSellingCoins($seller->id);
                    $available_coins = $seller->wallet->cwallet - $locked_coins;

                    if ($coins_required_to_sell > $available_coins) {
                        $v->errors()->add('error', trans('general.page.trade.error.selling-coin', ['locked' => $locked_coins, 'available' => $available_coins]));
                    }

                    // Checking sellable amount due to the processing and charity fee
                    if ($coins_required_to_sell > $seller->wallet->cwallet) {
                        $v->errors()->add('error', trans('general.page.trade.error.coin-required', ['amount' => $data['max_qty'], 'required' => $coins_required_to_sell]));
                    }

                    // Checking if bank account exist if bank transfer chosen
                    if ($data['payment'] == 'B') {
                        if ($seller->bank_acc_no == null) {
                            $v->errors()->add('error', trans('general.page.trade.error.bank-setup'));
                        }
                    }

                });

                if ($v->fails()) {
                    return back()->withInput()->withErrors($v);
                }
            }
            
            try {
                DB::beginTransaction();

                $data['price'] = $this->tradeService->getCurrentMarketPrice();
                $data['max_qty'] = $this->tradeService->coinsAfterCharges($data['max_qty']);

                $data['payment_ss'] = Image::make($data['payment_ss'])->encode('jpg', 75);

                $tradeSell = $this->tradeService->createTradeSell($seller->id, $data);

                // e-Wallet QR code handling
                if ($tradeSell->payment_method != 'B') {
                    
                    $file_extension = $data['payment_ss']->mime();

                    if ($tradeSell->payment_method == 'W') {
                        $file_name = 'wechatpay_' . $seller->username . '_' . $tradeSell->id . '_' . DateTimeTool::systemDateTime('Ymd');

                        $tradeSell->update([
                            'payment_ss' => $file_name . '.' . $file_extension,
                        ]);

                        // $storeAs = $data['payment_ss']->storeAs('wechatpay', $tradeSell->payment_ss, 's3');

                        $storeAs = Storage::disk('s3')->put('wechatpay/'.$tradeSell->payment_ss, $data['payment_ss']->__toString());
                    } 
                    elseif ($tradeSell->payment_method == 'A') {
                        $file_name = 'alipay_' . $seller->username . '_' . $tradeSell->id . '_' . DateTimeTool::systemDateTime('Ymd');

                        $tradeSell->update([
                            'payment_ss' => $file_name . '.' . $file_extension,
                        ]);

                        // $storeAs = $data['payment_ss']->storeAs('alipay', $tradeSell->payment_ss, 's3');

                        $storeAs = Storage::disk('s3')->put('alipay/'.$tradeSell->payment_ss, $data['payment_ss']->__toString());
                    }

                }
                // end of file handling
                
                DB::commit();

                return redirect()->action('Member\TradeController@buy')->with('success', trans('general.page.trade.success.sell-offer-created'))->with('curPrice', $curMarketPrice);

            } catch (Exception $e) {

                DB::rollback();

                return back()->with('errors', $e->getMessage());
            }
            
        }

        return view('member.trade.create-sell')->with('seller', $seller)->with('curPrice', $curMarketPrice);
    }

    public function stopTradeSell()
    {
        $data = Request::only('sell_id');

        $tradeSell = $this->tradeService->getTradeSellById($data['sell_id']);

        if ($tradeSell->uid != Auth::user()->id){
            return back()->with('error', 'Authentication failed!');
        }

        $v = Validator::make($data,[
            'sell_id' => 'required|exists:mlm_trade_sells,id',
        ]);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        try {
            DB::beginTransaction();

            $tradeSell = $this->tradeService->endTradeSell($data['sell_id']);

            if ($tradeSell == null) {
                throw new Exception(trans('general.page.trade.error.cancel-sell'));
            }

            DB::commit();

            return redirect()->route('my-trade-sell')->with('success', trans('general.page.trade.success.sell-end'));
        }
        catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }

    public function sellRequest($orderId)
    {
        $seller = Auth::user();
        $setting = $this->tradeService->getTradeSetting();

        $tradeBuy = $this->tradeService->getTradeBuyById($orderId);

        // Prevent double queue
        $requests = $this->tradeService->getSellRequestsForTradeBuy($tradeBuy->id);

        foreach ($requests as $request) {
            if ($request->sell_uid == Auth::user()->id) {
                return back()->with('error', trans('general.page.trade.error.tradesell-queueing'));
            }
        }
        // End of prevention

        if (Request::isMethod('POST')) {

            $data = Request::only('unit','payment_ss','payment_id');

            $v = Validator::make($data, [
                'unit' => 'required|numeric|min:' . $tradeBuy->min_qty.'|max:' . $tradeBuy->open_qty,
            ],[
                'unit.min' => trans('general.page.trade.error.min-buy', ['min_qty' => $tradeBuy->min_qty]),
                'unit.max' => trans('general.page.trade.error.max-buy', ['max_qty' => $tradeBuy->open_qty]),
            ]);

            $v->after(function($v) use ($data, $seller){
                // Checking sellable amount due to the daily selling cap
                $total_sold = $this->tradeService->getTodayTotalSold();
                $selling_cap = $this->tradeService->getTodayTradingCap();
                $sellable = $selling_cap - $total_sold;

                if ($seller->company_status != true) {
                    if ($sellable <= 0) {
                        $v->errors()->add('error', trans('general.page.trade.error.selling-capped'));
                    }elseif ($data['unit'] > $sellable) {
                        $v->errors()->add('error', trans('general.page.trade.error.left-to-cap', ['left' => $sellable]));
                    }
                }
            });

            if ($v->fails()) {
                return back()->withInput()->withErrors($v);
            }

            $v->after(function($v) use ($data, $seller, $tradeBuy){

                $coins_required_to_sell = $this->tradeService->coinsAfterCharges($data['unit']);
                $locked_coins = $this->tradeService->getMemberTotalSellingCoins($seller->id);
                $available_coins = $seller->wallet->cwallet - $locked_coins;

                if ($coins_required_to_sell > $available_coins) {
                    $v->errors()->add('error', trans('general.page.trade.error.selling-coin', ['locked' => $locked_coins, 'available' => $available_coins]));
                }

                // Checking sellable amount due to the processing and charity fee
                if ($coins_required_to_sell > $seller->wallet->cwallet) {
                    $v->errors()->add('error', trans('general.page.trade.error.coin-required', ['amount' => $data['unit'], 'required' => $coins_required_to_sell]));
                }

                if ($tradeBuy->payment_method != 'B') {
                    if (!isset($data['payment_ss'])) {
                        $v->errors()->add('error', trans('general.page.trade.error.payment-ss-required'));
                    }

                    if (!isset($data['payment_id'])) {
                        $v->errors()->add('error', trans('general.page.trade.error.payment-id-required'));
                    }
                }

            });

            if ($v->fails()) {
                return back()->withInput()->withErrors($v);
            }

            try {

                $sellRequest = $this->tradeService->queueSellingRequest($tradeBuy->id, $seller->id, $data['unit']);

                $sellRequest->update([
                    'STATUS' => TradeQueue::STATUS['PENDING'],
                ]);

                $tradeSell = $sellRequest->tradeSell;

                // e-Wallet QR code handling
                if ($tradeSell->payment_method != 'B') {
                    
                    $file_extension = $data['payment_ss']->extension();

                    if ($tradeSell->payment_method == 'W') {
                        $file_name = 'wechatpay_' . $seller->username . '_' . $tradeSell->id . '_' . DateTimeTool::systemDateTime('Ymd');

                        $tradeSell->update([
                            'payment_ss' => $file_name . '.' . $file_extension,
                        ]);

                        $storeAs = $data['payment_ss']->storeAs('wechatpay', $tradeSell->payment_ss, 's3');
                    } 
                    elseif ($tradeSell->payment_method == 'A') {
                        $file_name = 'alipay_' . $seller->username . '_' . $tradeSell->id . '_' . DateTimeTool::systemDateTime('Ymd');

                        $tradeSell->update([
                            'payment_ss' => $file_name . '.' . $file_extension,
                        ]);

                        $storeAs = $data['payment_ss']->storeAs('alipay', $tradeSell->payment_ss, 's3');
                    }

                }
                // end of file handling

                return redirect()->route('my-trade-sell')->with('success', trans('general.page.trade.success.sell-request'));

            } catch (Exception $e) {

                return back()->with('errors', $e->getMessage());
            }
        }

        return view('member.trade.place-selling-offer', compact('tradeBuy', 'setting'));
    }

    public function allSellingRequests()
    {
        $offers = $this->tradeService->getAllBuyingRequestsByUid(Auth::user()->id);

        return view('member.trade.my-buying-offers', compact('offers'));
    }

    public function sellDetails($queueId)
    {
        $queue = $this->tradeService->getTradeQueue($queueId);
        $ca_display = DateTimeTool::systemToOperationDateTime($queue->created_at);
        $ua_display = DateTimeTool::systemToOperationDateTime($queue->updated_at);

        if ($queue->sell_uid != Auth::user()->id) {
            return back()->with('error', 'Authorization failed!');
        }

        if ($queue->receipt) {
            $receipt = Storage::disk('s3')->url('receipts/' . $queue->receipt);

            if ($queue->status == 4) {
                $rated = $this->tradeService->checkMemberRatedTrade($queue->id, $queue->sell_uid);

                return view('member.trade.sell-details', compact('queue', 'receipt', 'ca_display', 'ua_display', 'rated'));
            }

            return view('member.trade.sell-details', compact('queue', 'receipt', 'ca_display'));
        }

        return view('member.trade.sell-details', compact('queue', 'ca_display'));
    }

    public function getMemberTradeSell()
    {
        $requests = $this->tradeService->getQueueingSells(Auth::user()->id);
        $customs = $this->tradeService->getMemberCustomSells(Auth::user()->id);

        return view('member.trade.my-selling-offers', compact('requests','customs'));
    }

    public function viewTradeSell($tradeSellId)
    {
        $tradeSell = $this->tradeService->getTradeSellById($tradeSellId);

        $requests = $this->tradeService->getBuyRequestsForTradeSell($tradeSellId);

        $ca_display = DateTimeTool::systemToOperationDateTime($tradeSell->created_at);

        return view('member.trade.view-trade-sell', compact('tradeSell', 'requests', 'ca_display'));
    }
    //---------- END OF SELLING FUNCTIONS ----------//

    public function getTradeRecords()
    {
        $records = $this->tradeService->getMemberTradeRecords(Auth::user()->id, 5);

        return view('member.trade.records', compact('records'));
    }

    public function rateTradeSell()
    {
        $data = Request::only('trade_id', 'star');

        $v = Validator::make($data,[
            'trade_id' => 'required|exists:mlm_trade_queues,id',
            'star' => 'required|integer|min:1,max:5',
        ]);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        $rate = $this->tradeService->rateSeller($data['trade_id'], $data['star']);

        return redirect()->route('buy-details', ['queueId' => $data['trade_id']])->with('success', trans('general.page.trade.success.rate'));
    }

    public function rateTradeBuy()
    {
        $data = Request::only('trade_id', 'star');

        $v = Validator::make($data,[
            'trade_id' => 'required|exists:mlm_trade_queues,id',
            'star' => 'required|integer|min:1,max:5',
        ]);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        $rate = $this->tradeService->rateBuyer($data['trade_id'], $data['star']);

        return redirect()->route('sell-details', ['queueId' => $data['trade_id']])->with('success', trans('general.page.trade.success.rate'));
    }
}
