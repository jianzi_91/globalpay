<?php

namespace App\Http\Controllers\Member;

use App\Models\Currency;
use App\Models\Exchange;
use Illuminate\Http\Request;

use App\Http\Requests;

class ExchangeController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    /**
     * Index page
     *
     * @return View
     */
    public function index(Request $request)
    {
        if ($request->has('amount')) {
            $result = Currency::convert($request->amount, $request->to_currency, $request->from_currency);
        } else {
            $result = null;
        }

        $exchange = Exchange::with('currency')
            ->where([
                ['status', '=', 1]
            ])->get();

        $currency_in_exchange = $exchange->pluck('currency_id')->toArray();
        $base_currency = Currency::getBaseCurrency();

        $currencies = Currency::all()
            ->whereIn('id', $currency_in_exchange)
            ->push($base_currency) // add base currency
            ->sortBy('name');

        return view('member.exchange.index', [
            'exchange' => $exchange,
            'currencies' => $currencies,
            'base_currency' => $base_currency,
            'result' => $result,
            'converter_request' => $request
        ]);
    }
}
