<?php

namespace App\Http\Controllers\Member;

use Auth;
use DB;
use Exception;
use QRCode;
use Illuminate\Support\Facades\Password;

use App\Models\Member;
use App\Models\MemberRelationship; 
use App\Models\MemberStatus;
use App\Models\MemberWalletRecord;
use App\Models\Country;
use App\Models\CountryBank;
use App\Models\TransInfo;
use App\Models\Logger;
use App\Models\SessionKeyValue;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

class UserController extends Controller
{
	protected $countryRepo;

	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}
	
	public function myRegisterQR()
	{

		return view('member.register-qr');
	}

    public function create()
    {
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

		$user = $this->user;

		//set variable
		$redirect = back();
		$view = view('member.create');
		$user_table = $user->getTable();
		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();

		// check if user have wallet
		if ($user->id != 1) {
			if (!MemberWalletRecord::where([
					"uid" => $user->id,
					"wallet" => "awallet2"
				])->exists() && 
				!MemberWalletRecord::where([
					"uid" => $user->id,
					"wallet" => "awallet"
				])->exists()
			) {
				return $view->with(["__step" => "error", "errmsg" => trans("general.page.user.register.errmsg.no-wallet-purchase")]);
			}
		}
        
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('create_nonce');
        $nonce = str_random(60);
        $session_key->putValue('create_nonce', $nonce);

		$step = 'start';
		if (session()->has('__step')) {
			$step = session()->get('__step');
		}
		
		//options data and default
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();
		$fields['phone_code']['options'] = $this->countryRepo->all_member_selectable_phone_code();
		$fields['locale']['options'] = ["en"=>"English", "zh-cn"=>"中文"];
 		$fields['__nonce']['value'] = $nonce;

		$datetime_op = DateTimeTool::operationDateTime();

        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
            $data = request()->all();
		
			$niceNames = [
				'name' => trans('field.user.name'),
				'email' => trans('field.user.email'),
				'password' => trans('field.user.password'),
				'password2' => trans('field.user.password2'),
				'mobile' => trans('field.user.mobile'),
				'nid' => trans('field.user.nid'),
				'up' => trans('field.user.up'),
				'up_pos' => trans('field.user.up_pos'),
				'address1' => trans('field.user.address1'),
				'address2' => trans('field.user.address2'),
				'city' => trans('field.user.city'),
				'zip' => trans('field.user.zip'),
				'state' => trans('field.user.state'),
				'country' => trans('field.user.country'),
				'epassword' => trans('field.user.epassword'),
			];

			$niceMessages = [
				"password.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password']]),
				"password2.regex" => trans('validation.password.pattern', ['attribute' => $niceNames['password2']]),
				"mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
			];
			
			if ($data['__req'] == '1') {
				$step = "start";

				$up = Member::where('mobile', '=', $data['up'])
					->orWhere('username', $data['up'])
					->first();
				$data['up'] = $up?$up->id:'0';

				$v = validator($data, [
					'name' => 'required|max:255',
					'nid' => 'required|max:30',
					'email' => 'required|email|max:255',
					'mobile' => 'required|min:1|digits_between:6,100|regex:/^[0-9]+$/|unique:'.$user_table,
					'address1' => 'max:100',
					'address2' => 'max:100',
					'city' => 'max:100',
					'zip' => 'max:10',
					'state' => 'max:100',
					'country' => 'required|exists:'.$country_table.','.$country_id,
					'up' => 'required|exists:'.$user_table.',id',
					'up_pos' => 'required|in:1,2',
					'password' => 'required|min:6|max:255|confirmed|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
					'password2' => 'required|min:6|max:255|confirmed|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
					'epassword' => 'required',
				], $niceMessages, $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					
					//check password
					$user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);
                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							unset($epassword_nonce[$key]);
							session()->put("epassword_nonce", $epassword_nonce);
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
					}

					//get auth user id
					$user_id = $user->id;

					//check network
					if ($data['up'] && !Member::isDownlineOf($data['up'], $user_id)) {
						$validator->errors()->add('up', trans('validation.network.not-your-downline', ['attribute' => $niceNames['up']]));
					}
				});
				
				//dd($v->fails());
				if ($v->fails())
					return back()->with([
						'__step' => $step,
					])->withInput()->withErrors($v);
				
				//check next step 
				$step = "preview";
				if (isset($data['__confirm'])) {
					if ($data['__confirm'] == 1) {
						$step = "confirmed";
					}
					else {
						$step = "start";
					}
				}

				//proceed the request
				if ($step=='confirmed') {

					try {
						DB::beginTransaction();
						$upline = Member::getDownMostUsername($data['up'], $data["up_pos"]);

						//add member
						$new_user = Member::create([
							'itype' => $initiator['itype'],
							'iid' => $initiator['iid'],
							'editor_type' => $initiator['itype'],
							'editor_id' => $initiator['iid'],
							'name' => $data['name'],
							'email' => $data['email'],
							'nid' => $data['nid'],
							'username' => Member::genUniqueUsername(),
							'country' => $data['country'],
							'mobile' => $data['mobile'],
							'ref' => $data['up'],
							'up' => $upline->id,
							'up_pos' => $data['up_pos'],
							"locale" => $data["locale"],
							'password' => bcrypt($data['password']),
							'password_ori' => $data['password'],
							'password2' => bcrypt($data['password2']),
							'password2_ori' => $data['password2'],
							'primary_acc' => Member::$primary_to_code['yes'], //yes
							'status' => Member::$status_to_code['active'], //take active status
						]);

						if (Member::where([
							"up" => $new_user->up, 
							"up_pos" => $new_user->up_pos])
							->count() > 1
						) {
							throw new Exception("Position occupied.");
						}

						// set token
						$new_user->forceFill([
							'remember_token' => str_random(60),
						])->save();
						
						//create wallets for user
						$new_user->wallet()->create();
						$new_user->memberStatus()->create();
						$new_user->additionalRelease()->create();
				
						MemberRelationship::populate();

						$proceed = $new_user?true:false;

						if ($proceed) {

							//Log Action
							$this->setIdentifiers($new_user->id);
							$log_msg = "User Created for User ID #".$new_user->id.".";
							Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data' => [
								'name' => $data['name'], 
								'email' => $data['email'], 
								'country' => $data['country'], 
								'nid' => $data['nid'], 
								'status' => Member::$status_to_code['active'], 
								'primary_acc' => Member::$primary_to_code['yes'], 
								'mobile' => $data['mobile'], 
								'ref' => $user->id, 
								'up' => $user->id, 
								'up_pos' => $data['up_pos'], 
								'locale' => $data['locale']
							], 'uid' => $new_user->id]);
						}
					} catch (Exception $e) {
						//throw something here
						DB::rollBack();
						throw $e;
						
						$v->getMessageBag()->add('all', trans('general.page.user.register.errmsg.register-error'));
						return back()->withInput()->withErrors($v);
					}
					DB::commit();
					

					//send email to new member
					// if (isset($new_user) && $new_user->email) {
					// 	try {
					// 		$pre_locale = app()->getLocale();
					// 		app()->setLocale($new_user->locale);
							
					// 		$mail_to = $new_user->email;
					// 		$mail_info = [
					// 			"username" => $new_user->username,
					// 			"password" => $data["password"],
					// 			"password2" => $data["password2"],
					// 			"name" => $new_user->name,
					// 			"nid" => $new_user->nid,
					// 			"mobile" => $new_user->mobile,
					// 			"email" => $new_user->email,
					// 			"country" => $this->countryRepo->getCountryByCode($new_user->country),
					// 			"package" => trans("general.activation.title.".$result['activation']->code),
					// 		];

					// 		\Mail::send('member.emails.new-user', ['__info' => $mail_info], function ($message) use ($mail_to)
					// 		{
					// 			//$message->bcc(["yee.yanhoh@mifun.my", "yanhoh@hotmail.com", "yee.mib@gmail.com"]);
					// 			$message->to($mail_to);
					// 			$message->replyTo("no-reply@dongli88.com");
					// 			$message->subject(trans('general.email.user.new-user.subject', ['sitename'=>trans('system.company')]));
					// 		});
					// 		app()->setLocale($pre_locale);
					// 	} catch (Exception $e) {
					// 		//ignore mail error
					// 	}
					// }
					
					return $view->with([
						'__fields' => $fields, 
						'__step' => $step, 
						'__new_user' => $new_user,
					])->with('success', trans('general.page.user.register.msg.register-success'));
				}
			}
			
			return $redirect->with([
				'__step' => $step,
			])->withInput();
        }
		
        return $view->with([
			'__fields' => $fields, 
			'__step' => $step, 
			'__user' => $user,
		]);
    }

    public function upgrade()
    {
		$user = $this->user;

		//set variable
		$view = view('member.upgrade');
		$user_table = $user->getTable();
		$package_details = Activation::$package_code_to_pack;
		$field_list = ['code', 'uid', "wallet_pay", 'epassword'];
		$use_wallet = [];

        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('upgrade_nonce');
        $nonce = str_random(60);
        $session_key->putValue('upgrade_nonce', $nonce);

		$step = 'user'; 

		//options data and default
		$fields['__nonce']['value'] = $nonce;
		$fields['code']['options'] = $package_details;
		$fields['wallet_pay']['options'] = [
			1 => "100% " . trans('general.wallet.cwallet'),
		];

		$datetime_op = DateTimeTool::operationDateTime();

        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {

			$data_raw = $data = request()->all();
			
			if ($data["__req"] != "back_user") {
				$user_table = $user->getTable();
				$package_codes = array_keys($package_details);
					
				$niceNames = [
					'code' => trans('field.activation.code-name'),
					'uid' => trans('field.user.username'),
					'wallet_pay' => trans('field.activation.wallet-pay'),
					'epassword' => trans('field.user.epassword')
				];
				
				//STEP 1: get user page
				//get user id
				$to_user = Member::where('username', '=', $data['uid'])->first();
				$data['uid'] = $to_user?$to_user->id:'0';

				$v = validator($data, [
					'uid' => 'required|exists:'.$user_table.',id',
					'epassword' => 'required',
				], [], $niceNames);

				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					
					//check password
					$user = $this->user;
                    $epassword_nonce = session("epassword_nonce", []);
                    if (!is_array($epassword_nonce)) {
                        $epassword_nonce = [];
                    }
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						if (($key = array_search($data['epassword'] , $epassword_nonce)) !== false) {
							if (isset($data['__confirm']) && $data['__confirm'] == 1) {
								unset($epassword_nonce[$key]);
								session()->put("epassword_nonce", $epassword_nonce);
							}
						} else {
							$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
						}
					}
					else {
						$epassword_nonce[] = str_random(60);
                        if (count($epassword_nonce) > 50) {
                            array_shift($epassword_nonce);
                        }
						session()->put("epassword_nonce", $epassword_nonce);
					}
					
					//get auth user id
					$user_id = $user->id;
					
					if ($data['uid'] && !Member::isDownlineOf($data['uid'], $user_id, "sponsor")) {
						$validator->errors()->add('uid', trans('validation.network.not-your-downline', ['attribute' => $niceNames['uid']]));
					}
				});
				
				if ($v->fails()) {
					$view = $view->withErrors($v);
				}
				else {
					$v = validator([], []);
				}
					
				if (!$v->fails()) {
					
					// check non settled package
					$non_settle_package_found = Activation::where("uid", $to_user->id)
						->whereIn("act_type", [Activation::$act_type_to_code["contra"], Activation::$act_type_to_code["special"]])
						->where("status", Activation::$status_to_code["confirmed"])
						->where("loan_status", Activation::$loan_status_to_code["pending"])
						->exists();

					if ($non_settle_package_found) {
						// dongli start: For SA1 account
						$member_status = MemberStatus::where('uid', $to_user->id)->value('sa_status');
						if ($member_status == 10) {
							return back()->withInput()->withErrors(trans('general.page.user.activate-special-account.errmsg.non-settled-package'));
						}
						// dongli end: For SA1 account 

						return back()->withInput()->withErrors(trans('general.page.user.upgrade-topup.errmsg.non-settled-package'));
					}
				}

				if (!$v->fails())
					$step = "package";
			}

			//Step 2: get package
			if ($data['__req'] == '1' && !$v->fails()) {

				//get package details
				$data['code'] = isset($data['code'])?$data['code']:'';
				$package =  isset($fields['code']['options'][$data['code']])?$fields['code']['options'][$data['code']]:null;
				$use_wallet = ['cwallet' => $package['price']];
				
				$v = validator($data, [
					'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'])),
					'wallet_pay' => 'required|in:'.implode(',', array_keys($fields['wallet_pay']['options'])),
				], [], $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames, $use_wallet) {
					$data = $validator->getData();
					
					$user = $this->user;

					$user_wallets = $user->wallet;

					//check wallet
					foreach ($use_wallet as $_wallet=>$_amount) {
						if ($user_wallets->$_wallet < $_amount) {
							$validator->errors()->add('wallet', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$_wallet), 'required' => number_format($_amount, 2), 'balance' => number_format($user_wallets->$_wallet, 4)]));
						}
					}
				});
				
				if ($v->fails())
					$view = $view->withErrors($v);
				
				if (!$v->fails()) {
					//check next step 
					$step = "preview";
					if (isset($data['__confirm'])) {
						if ($data['__confirm'] == 1) {
							$step = "confirmed";
						}
						else {
							$step = "package";
						}
					}
				}
				
				if ($step == "confirmed") {

					try {
						DB::beginTransaction();
						//deduct wallet
						foreach ($use_wallet as $_wallet=>$_amount) {
							if ($_amount > 0) {
								$descr = "Package Purchased code (".$data['code'].") for User ID #".$data['uid'].".";
								$descr_json = [
									'wallet' => $_wallet,
									'amount' => $_amount,
									'uid' => $data['uid'] ,
									'username' => $to_user->username,
									'package_code' => $data['code'],
									'trans-type' => 'purchase',
								]; 
	
								$record = $user->deductWallet($user->id, $_wallet, $_amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
								if (!$record['status']) {
									throw new Exception("Error occurred while deduct wallet.");
								}
							}
						}
						$result = $user->addActivation($data['uid'], $data['code'], $user->id, ['use_wallet_json'=>$use_wallet, 'topup'=>false]);
						
					} catch (Exception $e) {
						DB::rollBack();
						throw $e;
					}
					DB::commit();
					
					$view = $view->with(['__activation'=>$result['activation']])->with('success', trans('general.page.user.upgrade-topup.msg.upgrade-topup-success'));
				}
			}
        }
		elseif (request()->isMethod('get')) {
			$data_raw['uid'] = request()->get('uid', $user->username);
		}

		//set field value
		foreach ($field_list as $_field) {
			$fields[$_field]['value'] = isset($data_raw[$_field])?$data_raw[$_field]:"";
		}
		
        return $view->with([
			'__fields' => $fields, 
			'__step' => $step, 
			'__user' => $user, 
			'__use_wallet' => $use_wallet, 
			'__package_details' => $package_details, 
		]);
    }

    public function profileEdit()
    {
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

		$user = $this->user;
		$member_table = $user->getTable();

		$main_acc = null;

		if ($user->sub_id != 0) {
			$main_acc = Member::where('mobile', $user->mobile)
				->where('sub_id', 0)
				->first();
		}

		//options data and default
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();
		$fields['phone_code']['options'] = $this->countryRepo->all_member_selectable_phone_code();
		$fields['bonus_wallet_status']['options'] = [
			"0" => "awallet2",
			"10" => "cwallet",
		];
		$fields['bank_name']['options'] = CountryBank::where("country_code", $user->country)->pluck("bank_name")->all();
		$user_rank = Member::rankTypes()[$user->rank];

        if (request()->isMethod('post') && request()->has('__req')) {
            $data = request()->all();
			
			//profile
			if ($data['__req'] == '1') {
				$niceNames = [
					// 'name' => trans('field.user.name'),
					// 'nid' => trans('field.user.nid'),
					'email' => trans('field.user.email'),
					'address1' => trans('field.user.address1'),
					'address2' => trans('field.user.address2'),
					'city' => trans('field.user.city'),
					'zip' => trans('field.user.zip'),
					'state' => trans('field.user.state'),
					// 'country' => trans('field.user.country'),
					// 'mobile' => trans('field.user.mobile'),
					'epassword' => trans('field.user.epassword'),
				];
				$v = validator($data, [
					// 'name' => 'required|max:255',
					// 'nid' => 'required|max:30',
					'address1' => 'max:100',
					'address2' => 'max:100',
					'city' => 'max:100',
					'zip' => 'max:10',
					'state' => 'max:100',
					'email' => 'required|email|max:255',
					// 'country' => 'required|exists:'.$country_table.','.$country_id,
					// 'mobile' => 'required|min:1|digits_between:6,100|regex:/^[0-9]+$/|unique:'.$member_table.',mobile,'.$user->id,
					'epassword' => 'required',
				], [
					"mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
				], $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					$user = $this->user;
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
					}
				});
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'email' => $data['email'], 
					'mobile' => $data['mobile'], 
					'city' => $data['city'], 
					'zip' => $data['zip'], 
					'state' => $data['state'], 
					'address1' => $data['address1'], 
					'address2' => $data['address2']
				];
				$log_data["changes"] = Logger::retrieveChanges($user->toArray(), $log_data["data"]);

				if ($user->mobile != $data["mobile"]) {
					$user->memberStatus->mobile_status = null;
					$user->memberStatus->save();
				}

				// $user->name= isset($data['name']) ? $data['name'] : $user->name;
				// $user->nid= isset($data['nid']) ? $data['nid'] : $user->nid;
				$user->editor_type= $initiator['itype'];
				$user->editor_id= $initiator['iid'];
				$user->address1= $data['address1'];
				$user->address2= $data['address2'];
				$user->city= $data['city'];
				$user->zip= $data['zip'];
				$user->state= $data['state'];
				$user->email= $data['email'];
				// $user->mobile= $data['mobile'];
			//	$user->country= $data['country'];

				if ($user->save()) {
				    //Log Action
                    $this->setIdentifiers($user->id);
                    $log_msg = "General Info update for User ID #".$user->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $user->id]
					));
				}
				return back()->withSuccess(trans('general.page.user.profile.msg.general-info-updated'));
			}
			//password
			elseif ($data['__req'] == '2') {
				
				$niceNames = [
					'password' => trans('general.page.user.profile.content.password', array('field'=> trans('field.user.password'))),
					'epassword' => trans('field.user.epassword'),
				];

				$niceMessages = [
					'password.regex' => trans('validation.password.pattern', ['attribute' => $niceNames['password']]),
				];

				$v = validator($data, [
					'password' => 'required|confirmed|min:6|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
					'epassword' => 'required',
				], $niceMessages, $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					$user = $this->user;
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
					}
				});
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);
				
				$user->editor_type= $initiator['itype'];
				$user->editor_id= $initiator['iid'];
				$user->password = bcrypt($data['password']);
				$user->password_ori = $data['password'];
				$back_to_home = false;
				if ($user->save()) {
					// if ($user->statuses->pw2_change_status == 10) {
					// 	if (session()->get("force_change_password")) {
					// 		$back_to_home = true;
					// 	}
					// 	session()->forget("force_change_password");
					// }

					//Log Action
					$this->setIdentifiers($user->id);
					$log_msg = "Password Updated for User ID #".$user->id.".";
					Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, [
						'uid' => $user->id
					]);
				}
				if ($back_to_home) {
					return redirect("member/");
				}
				return back()->withSuccess(trans('general.page.user.profile.msg.password-updated'));
			}
			//security password
			elseif ($data['__req'] == '3') {

				$niceNames = [
					'new_password2' => trans('general.page.user.profile.content.password2', array('field'=> trans('field.user.password2'))),
					'epassword' => trans('field.user.epassword'),
				];

				$niceMessages = [
					'new_password2.regex' => trans('validation.password.pattern', ['attribute' => $niceNames['new_password2']]),
				];

				$v = validator($data, [
					'new_password2' => 'required|confirmed|min:6|max:255|regex:[(?=.*[0-9])(?=.*[a-zA-Z])]',
					'epassword' => 'required',
				], $niceMessages, $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					$user = $this->user;
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						$validator->errors()->add('epassword', trans('validation.unmatch.existing', ['attribute' => $niceNames['epassword']]));
					}
				});
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$user->editor_type= $initiator['itype'];
				$user->editor_id= $initiator['iid'];
				$user->password2 = bcrypt($data['new_password2']);
				$user->password2_ori = $data['new_password2'];
				$back_to_home = false;
				
				if ($user->save()) {
					// if ( 
					//Log Action
					$this->setIdentifiers($user->id);
					$log_msg = "Security Password Updated for User ID #".$user->id.".";
					Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, [
						'uid' => $user->id
					]);
				}
				if ($back_to_home) {
					return redirect("member/");
				}

				return back()->withSuccess(trans('general.page.user.profile.msg.password2-updated', array('field'=>trans('field.user.password2'))));
			}
			//bank information
			elseif ($data['__req'] == '4') {
				
				$niceNames = [
					'bank_name' => trans('field.user.bank_name'),
					'bank_branch_name' => trans('field.user.bank_branch_name'),
					'bank_acc_no' => trans('field.user.bank_acc_no'),
					'bank_sorting_code' => trans('field.user.bank_sorting_code'),
					'bank_iban' => trans('field.user.bank_iban'),
					'epassword' => trans('field.user.epassword'),
				];

				$v = validator($data, [
					'bank_name' => 'required|max:100',
					'bank_branch_name' => 'required|max:100',
					'bank_acc_no' => 'required|max:30',
					'bank_sorting_code' => 'max:30',
					'bank_iban' => 'max:30',
					'epassword' => 'required',
				], [], $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					$user = $this->user;
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						$validator->errors()->add('epassword', trans('validation.unmatch.existing', ['attribute' => $niceNames['epassword']]));
					}
				});
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'bank_name' => $data['bank_name'], 
					'bank_branch_name' => $data['bank_branch_name'], 
					'bank_acc_no' => $data['bank_acc_no'], 
					'bank_sorting_code' => $data['bank_sorting_code'], 
					'bank_iban' => $data['bank_iban']
				];
				$log_data["changes"] = Logger::retrieveChanges($user->toArray(), $log_data["data"]);

				$user->editor_type= $initiator['itype'];
				$user->editor_id= $initiator['iid'];
				$user->bank_name = $data['bank_name'];
				$user->bank_branch_name = $data['bank_branch_name'];
				$user->bank_acc_no = $data['bank_acc_no'];
				$user->bank_sorting_code = $data['bank_sorting_code'];
				$user->bank_iban = $data['bank_iban'];
				
				if ($user->save()) {
				    //Log Action
                    $this->setIdentifiers($user->id);
                    $log_msg = "Bank Info update for User ID #".$user->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $user->id]
					));
				}

				return back()->withSuccess(trans('general.page.user.profile.msg.bank-info-updated'));
			}
			//beneficiary information
			elseif ($data['__req'] == '5') {

				$niceNames = [
					'beneficiary_name' => trans('field.user.beneficiary_name'),
					'beneficiary_nid' => trans('field.user.beneficiary_nid'),
					'beneficiary_user_relationship' => trans('field.user.beneficiary_user_relationship'),
					'epassword' => trans('field.user.epassword'),
				];

				$v = validator($data, [
					'beneficiary_name' => 'max:255',
					'beneficiary_nid' => 'max:30',
					'beneficiary_user_relationship' => 'max:30',
					'epassword' => 'required',
				], [], $niceNames);
				
				//custom validation
				$v->after(function($validator) use ($niceNames) {
					$data = $validator->getData();
					$user = $this->user;
					if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
						$validator->errors()->add('epassword', trans('validation.unmatch.existing', ['attribute' => $niceNames['epassword']]));
					}
				});
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'beneficiary_name' => $data['beneficiary_name'], 
					'beneficiary_nid' => $data['beneficiary_nid'], 
					'beneficiary_user_relationship' => $data['beneficiary_user_relationship']
				];
				$log_data["changes"] = Logger::retrieveChanges($user->toArray(), $log_data["data"]);

				$user->editor_type= $initiator['itype'];
				$user->editor_id= $initiator['iid'];
				$user->beneficiary_name = $data['beneficiary_name'];
				$user->beneficiary_nid = $data['beneficiary_nid'];
				$user->beneficiary_user_relationship = $data['beneficiary_user_relationship'];
				
				if ($user->save()) {
				    //Log Action
                    $this->setIdentifiers($user->id);
                    $log_msg = "Beneficiary Info update for User ID #".$user->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $user->id]
					));
				}

				return back()->withSuccess(trans('general.page.user.profile.msg.beneficiary-info-updated'));
			}
			// setting information
			// elseif ($data['__req'] == '6') {

			// 	$niceNames = [
			// 		'bonus_wallet_status' => trans('field.member_status.bonus_wallet_status'),
			// 		'epassword' => trans('field.user.epassword'),
			// 	];

			// 	$v = validator($data, [
			// 		'bonus_wallet_status' => 'required|in:0,10',
			// 		'epassword' => 'required',
			// 	], [], $niceNames);
				
			// 	//custom validation
			// 	$v->after(function($validator) use ($niceNames) {
			// 		$data = $validator->getData();
			// 		$user = $this->user;
			// 		if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
			// 			$validator->errors()->add('epassword', trans('validation.unmatch.existing', ['attribute' => $niceNames['epassword']]));
			// 		}
			// 	});
				
			// 	if ($v->fails())
			// 	   return back()->withInput()->withErrors($v);

			// 	$log_data["data"] = [
			// 		'bonus_wallet_status' => $data['bonus_wallet_status'], 
			// 	];
			// 	$log_data["changes"] = Logger::retrieveChanges($user->memberStatus->toArray(), $log_data["data"]);

			// 	$user->editor_type= $initiator['itype'];
			// 	$user->editor_id= $initiator['iid'];
			// 	$user->memberStatus->bonus_wallet_status = $data['bonus_wallet_status'];
				
			// 	if ($user->save() && $user->memberStatus->save()) {
			// 	    //Log Action
            //         $this->setIdentifiers($user->id);
            //         $log_msg = "Setting update for User ID #".$user->id.".";
            //         Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
			// 			$log_data,
			// 			['uid' => $user->id]
			// 		));
			// 	}

			// 	return back()->withSuccess(trans('general.page.user.profile.msg.setting-updated'));
			// }
		}

		$downline_2ndGen_Total = MemberRelationship::where('user_id', $user->id)->where('level_diff', 2)->get()->count();
		$downline_3rdGen_Total = MemberRelationship::where('user_id', $user->id)->whereIn('level_diff', array(3, 4, 5, 6, 7, 8, 9, 10))->get()->count();

        return view('member.profile', ['__fields'=>$fields], compact('downline_1stGen', 'downline_2ndGen_Total', 'downline_3rdGen_Total', 'user_rank'))->withUser($user)->with('main',$main_acc);
	}
	
	public function sponsorEdit()
	{
		$user = $this->user;
		$user_ref = $user->ref;
		if ($user_ref !== 1) {
			$user_ref = Member::where("id", $user_ref)->value("mobile");
		}
		elseif (request()->isMethod('post')) {
			$member_table = $user->getTable();
			$niceNames = [
				"ref" => trans("field.user.ref"),
				'epassword' => trans('field.user.epassword'),
			];

			$data = request()->only(array_keys($niceNames));
			$v = validator($data, [
				"ref" => "required|exists:".$member_table.",mobile",
				'epassword' => 'required',
			], [], $niceNames);
				
			//custom validation
			$v->after(function($validator) use ($niceNames) {
				$data = $validator->getData();
				$user = $this->user;
				if ($data['epassword'] && ! \Hash::check($data['epassword'], $user->password2)) {
					$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
				}
			});

			if ($v->fails()) {
				return back()->withInput()->withErrors($v);
			}

			$ref_id = Member::where("mobile", $data["ref"])->value("id");
			$v = validator([
				"ref" => $ref_id
			], [
				"ref" => "not_in:".Member::$default_ref.",".$user->id
			], [], $niceNames);

			if ($v->fails()) {
				return back()->withInput()->withErrors($v);
			}

			// check if sponsor is your downline
			if (Member::isDownlineOf($ref_id, $user->id, "sponsor")) {
				$v->errors()->add("ref", trans('validation.network.is-your-downline', ['attribute' => $niceNames['ref']]));
				return back()->withInput()->withErrors($v);
			}

			// logger
			$log_data["data"] = [
				'ref' => $ref_id, 
			];
			$log_data["changes"] = Logger::retrieveChanges($user->toArray(), $log_data["data"]);

			$user->ref = $ref_id;
			$user->save();

			//Log Action
			$this->setIdentifiers($user->id);
			$log_msg = "Sponsor update for User ID #".$user->id.".";
			Logger::logWithoutGuard(__METHOD__, $this->getIdentifiers(), $log_msg, array_merge(
				$log_data,
				['uid' => $user->id]
			));
			return back()->withSuccess(trans('general.page.user.sponsor.msg.sponsor-info-updated'));

		}
		return view('member.sponsor', ['user_ref' => $user_ref]);
	}

	// Activate Special Account 1
	public function activateSpecialAccount()
	{
		$view = view('member.activate-special-account');

		if (request()->isMethod('post')) {
			$data = request()->all();

			foreach ($data as $key => $value) {
				if ($value == '') {
					$data[$key] = null;
				} else {
					$data[$key] = trim(strip_tags($value));
				}
			}

			$niceNames = [
				'uid' => trans('field.user.username'),
				'amount' => trans('general.wallet.awallet2'),
				'epassword' => trans('field.user.epassword')
			];

			$v = validator($data, [
				'uid' => 'required|exists:'.with(new Member)->getTable().',username',
				'amount' => 'required|integer',
				'epassword' => 'required',
			], [], $niceNames);

			if ($v->fails()) {
				return back()->withErrors($v)->withInput();
			}

			$from_user = $this->user;
			$to_user = Member::where('username', '=', $data['uid'])->firstOrFail();

			//custom validation
			$v->after(function($validator) use ($niceNames, $data, $from_user, $to_user) {
				$data = $validator->getData();

				// check wallet amount
				$user_wallets = $from_user->wallet;
				if ($data['amount'] > $user_wallets->awallet2) {
					$validator->errors()->add('amount', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.awallet2'), 'required' => $data['amount'], 'balance' => number_format($user_wallets->awallet2, 2)]));
				}
				
				//check password
				if ($data['epassword'] && ! \Hash::check($data['epassword'], $from_user->password2)) {
					$validator->errors()->add('epassword', trans('validation.unmatch.incorrect', ['attribute' => $niceNames['epassword']]));
				}
				
				if (strtolower($from_user->username) == "register1") {
					// ignore
				}
				elseif ($to_user && !Member::isDownlineOf($to_user->id, $from_user->id)) {
					$validator->errors()->add('uid', trans('validation.network.not-your-downline', ['attribute' => $niceNames['uid']]));
				}
			});
			
			if ($v->fails()) {
				return back()->withErrors($v)->withInput();
			}

			$return = with(new Activation)->clearLoanActivationForSpecialAcc($to_user->id, $data['amount'], ['payer_id' => $from_user->id]);

			if ($return['status'] == true && isset($return['attribute']['balance']) && $return['attribute']['balance'] > 0) {
				return back()->with('warning', trans('general.page.user.activate-special-account.msg.partial-activate', ['username' => $to_user->username, 'balance' => number_format($return['attribute']['balance'], 2)]));
			}
	
			return back()->with('success', trans('general.page.user.activate-special-account.msg.activate-success', ['username' => $to_user->username]));
		}

		return $view;
	}
}
