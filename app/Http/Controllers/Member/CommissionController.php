<?php

namespace App\Http\Controllers\Member;

use DB;

use App\Models\Commission;
use App\Models\Member;
use App\Models\MemberCFStatDaily;
use App\Utils\DateTimeTool;
use App\Utils\NumberTool;

class CommissionController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    public function summary()
    {
        $user = $this->user;
        $view = view('member.commission.summary');

        //searching data
		$searching['yes'] = false;
		$searching['refresh_url'] = request()->url();

		//search fields
		$search_fields = [
			'bdate_from' => ['label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'>=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'from'],
			'bdate_to' => ['label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'<=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'to'],
		];

		//search handling
		$data = [];
		$query = new Commission;
		$query->setTable($query->getTable().' AS comm');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'comm.uid');

        //fill necessary data
        list($data, $search_fields, $searching, $query) = $this->searchHandling($data, $query, $search_fields, $searching);

		//result build
		$query = $query->where(
                    function($query) use ($user){
                        $query->where('user.primary_id', $user->id)
                            ->orWhere('user.id', $user->id);
                    }
                )
                ->where('comm.status', Commission::$status_to_code['distributed'])
                ->orderBy('comm.type')
                ->groupBy('comm.type')
            ->selectRaw('SUM(comm.amount) as sum_amount, comm.type, max(bdate) as max_bdate');
        
		//dd($query->toSql());
        $record = $query->paginate(20);
		
		//pagination link appends
		$record->appends($data);

		$total_bonus = 0;
        //format data
        foreach ($record as $key=>$value) {
			$record[$key]['-sum_amount-ddd'] = NumberTool::displayDynamicDecimal($value->sum_amount, 2);
			$total_bonus += $value->sum_amount;
        }
		$records_extra["-total_bonus-ddd"] = NumberTool::displayDynamicDecimal($total_bonus, 2);
        
        return $view->with(['__records'=>$record, '__records_extra'=>$records_extra, "__search_fields"=>$search_fields, '__searching'=>$searching, "__query_data"=>$data]);
    }
	
	public function roiSummary()
    {
        $user = $this->user;
        $view = view('member.commission.roi-summary');

        //searching data
		$searching['yes'] = false;
		$searching['refresh_url'] = request()->url();

		//search fields
		$search_fields = [
			'bdate_from' => ['label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'>=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'from'],
			'bdate_to' => ['label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'<=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'to'],
		];

		//search handling
		$data = [];
		$query = new Commission;
		$query->setTable($query->getTable().' AS comm');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'comm.uid')
			->whereIn("comm.type", [2001]);

        //fill necessary data
        list($data, $search_fields, $searching, $query) = $this->searchHandling($data, $query, $search_fields, $searching);

		//result build
		$query = $query->where(
                    function($query) use ($user){
                        $query->where('user.primary_id', $user->id)
                            ->orWhere('user.id', $user->id);
                    }
                )
                ->where('comm.status', Commission::$status_to_code['distributed'])
                ->orderBy('comm.type')
                ->groupBy('comm.type')
            ->selectRaw('SUM(comm.amount) as sum_amount, comm.type, max(bdate) as max_bdate');
        
		//dd($query->toSql());
        $record = $query->paginate(20);
		
		//pagination link appends
		$record->appends($data);

		$total_bonus = 0;
        //format data
        foreach ($record as $key=>$value) {
			$record[$key]['-sum_amount-ddd'] = NumberTool::displayDynamicDecimal($value->sum_amount, 2);
			$total_bonus += $value->sum_amount;
        }
		$records_extra["-total_bonus-ddd"] = NumberTool::displayDynamicDecimal($total_bonus, 2);
        
        return $view->with(['__records'=>$record, '__records_extra'=>$records_extra, "__search_fields"=>$search_fields, '__searching'=>$searching, "__query_data"=>$data]);
    }

    public function searchHandling($data, $query, $search_fields, $searching)
    {
        if (request()->isMethod('get') && request()->has('__src')) {
			$data = request()->only(array_merge(['__src'], array_keys($search_fields)));

			if ($data['__src'] == 1) {

				foreach ($search_fields as $_name => $_detail) {

					if ($data[$_name]) {
						$value = $search_fields[$_name]['value'] = $data[$_name];

						//special control to value manipulation
						$value_type = (isset($_detail['type']) && $_detail['type']?$_detail['type']:"");
						if($value_type == 'date'){
							$value_role = (isset($_detail['role']) && $_detail['role']?$_detail['role']:"");
							if($value_role == 'from'){
								$value .= ' 00:00:00';
							}
							else{
								$value .= ' 23:59:59';
							}
							$value = DateTimeTool::operationToSystemDateTime($value);
							//dd($value);
						}

						$field_name = (isset($_detail['table']) && $_detail['table']?$_detail['table'].".":"");
						$field_name .= (isset($_detail['as']) && $_detail['as']?$_detail['as']:$_name);
						$search_type = isset($_detail['search']) && $_detail['search']?$_detail['search']:false;

						switch ($search_type) {
							case 'like':
								$query = $query->where($field_name, $search_type, '%'.$value.'%');
								break;
							case '>=':
							case '>':
							case '<':
							case '<=':
								$query = $query->where($field_name, $search_type, $value);
								break;
							case 'in':
								$query = $query->whereIn($field_name, $value);
								break;
							case '=':
							default:
								$query = $query->where($field_name, '=', $value);
						}
					}
				}
				$searching['yes'] = true;
			}
		}

        return [$data, $search_fields, $searching, $query];
    }
    
    public function detailList($comm_type)
    {
        $view = view('member.commission.'.$comm_type.'-detail-list');
        $user = $this->user;
        
        //searching data
		$searching['yes'] = false;
		$searching['refresh_url'] = request()->url();

		//search fields
		$search_fields = [
			'bdate_from' => ['label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'>=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'from'],
			'bdate_to' => ['label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.commission.bdate')]), 'value'=>'', 
                'search'=>'<=', 'type'=>'date', 'table'=>'comm', 'as'=>'bdate', 'role'=>'to'],
		];

		//search handling
		$data = [];
		$query = new Commission;
		$query->setTable($query->getTable().' AS comm');
        
        switch ($comm_type) {
            case Commission::$type_to_code["active-bonus"]: 
            case Commission::$type_to_code["passive-bonus"]: 
                $query = $query->join($user->getTable()." AS user", "comm.uid", "=", "user.id")
                    ->select('comm.*');
                break;
            default: 
        }

        //fill necessary data
        list($data, $search_fields, $searching, $query) = $this->searchHandling($data, $query, $search_fields, $searching);

		//result build
		$query = $query->where(
                    function($query) use ($user){
                        $query->where('user.primary_id', $user->id)
                            ->orWhere('user.id', $user->id);
                    }
                )
                ->where('comm.status', Commission::$status_to_code['distributed'])
                ->where('comm.type', $comm_type)
                ->orderBy('comm.bdate', 'desc')
                ->orderBy('comm.id', 'desc');
        
		//dd($query->toSql());
        $record = $query->paginate(20);
		
		//pagination link appends
		$record->appends($data);

        //get extra data
        foreach ($record as $key=>$value) {

            $user = Member::find($value['uid']);
            $record[$key]['uid-username'] = ($user?$user->username:'-');
            unset($user);

            $user = Member::find($value['from_uid']);
            $record[$key]['from_uid-username'] = ($user?$user->username:'-');
            unset($user);

			$record[$key]['-amount-ddd'] = NumberTool::displayDynamicDecimal($value->amount, 2);
        }
        
        return $view->with(['__records'=>$record, "__search_fields"=>$search_fields, '__searching'=>$searching, "__comm_type"=>$comm_type]);
    }
}
