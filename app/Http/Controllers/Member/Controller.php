<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller as BaseController;
use App\Utils\Tool;

class Controller extends BaseController
{
	protected $guard = "web";
	protected $user = null;

	public function __construct()
	{
		Tool::setInitiator($this->guard);
        $this->user = auth($this->guard)->user();
    }
}
