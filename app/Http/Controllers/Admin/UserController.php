<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Admin;
use App\Models\Activation;
use App\Models\Member;
use App\Models\MemberStat;
use App\Models\MemberChangeNetwork;
use App\Models\MemberRelationship;
use App\Models\Country;
use App\Models\TransInfo;
use App\Models\Logger;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class UserController extends Controller
{
	protected $countryRepo;
	
	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'nid' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.nid'), 'value'=>''],
			'email' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.email'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'country' => ['search'=>'=', 'label'=>trans('field.user.country'), 'as'=>"country", "table" => "user",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'status' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.status'), 
				'options'=>[
					'10' => trans('general.user.status.10'),
					'30' => trans('general.user.status.30'),
					'90' => trans('general.user.status.90')
				], 
				'value'=>["10"]
			],
			'user_type' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.user_type'), 
				'options'=>[
					'1' => trans('general.user.user_type.1'),
					'2' => trans('general.user.user_type.2'),
					'3' => trans('general.user.user_type.3')
				], 
				'value'=>["1"]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();

		$query = new Member;
		$query->setTable($member_table.' AS user');

		// result build
		$query = $query->select([
			'user.*',
		])->groupBy("user.id");
		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		$datatables = Datatables::of($query);
    	
		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();

		// process all showing data 
		return $datatables
				->addColumn('network-info', function ($list) {
					$return = "";
					$user = Member::find($list['up']);
					$return .= $user ? e($user->username) : "-";
					$return .= "<br/>". ($list->up_pos ? e(trans("general.user.up_pos.".$list->up_pos)) : "-");
					unset($user);
					return $return;
				})
				->addColumn('address-info', function ($list) {
					$return = "";
					if ($list['primary_acc']=='y') {
						$return .= e(trans("field.user.address1"). ": ".$list["address1"]). "<br>";
						$return .= e(trans("field.user.address2"). ": ".$list["address2"]). "<br>";
						$return .= e(trans("field.user.city"). ": ".$list["city"]). "<br>";
						$return .= e(trans("field.user.zip"). ": ".$list["zip"]). "<br>";
						$return .= e(trans("field.user.state"). ": ".$list["state"]). "<br>";
					}
					else {
						$return = "-";
					}
					return $return;
				})
				->editColumn('country', function ($list) use ($country_code_to_name) {
					return isset($country_code_to_name[$list["country"]]) ? $country_code_to_name[$list["country"]] : "-";
				})
				->editColumn('id', function ($list) {
					return $list['id'];
				})
				->editColumn('name', function ($list) {
					return $list["name"];
				})
				->editColumn('nid', function ($list) {
					return $list["nid"];
				})
				->editColumn('email', function ($list) {
					return $list["email"];
				})
				->editColumn('mobile', function ($list) {
					return $list["mobile"];
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return $return;
				})
				->editColumn('status', function ($list) {
					return trans("general.user.status.".$list["status"]);
				})
				->addColumn('iid-text', function ($list) {
					$return = "-";
					if ($list["itype"] == "900") {
						$user = Admin::find($list["iid"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["itype"] == "100") {
						$user = Member::find($list["iid"]);
						$return = $user ? $user->username : "-";
					}
					return $return;
				})
				->addColumn('action', function ($list) {
					$return = "";
					if ($list->company_status != Member::$company_status_to_code["yes"]) {
						if ($list['primary_acc'] == 'y') {
							if ($this->user->can("admin-privilege", "members/edit-profile")) {
								$return .= '<a href="'. e(url($this->admin_slug.'/members/profile', $list['id'])) .'" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> '. e(trans('general.button.edit')) .' </a>';
							}
						}
					}
					return $return;
				})
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->rawColumns(['network-info', 'action', 'address-info'])
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.member.list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function create()
    {
		$_method =  __METHOD__;
		$_guard =  "admins";

		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();
		
		//options data
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();
		$fields['phone_code']['options'] = $this->countryRepo->all_member_selectable_phone_code();

        if (request()->isMethod('post')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			
            $niceNames = [
				'name' => trans('field.user.name'),
				'email' => trans('field.user.email'),
				'password' => trans('field.user.password'),
				'password2' => trans('field.user.password2'),
				'mobile' => trans('field.user.mobile'),
				'address1' => trans('field.user.address1'),
				'address2' => trans('field.user.address2'),
				'city' => trans('field.user.city'),
				'zip' => trans('field.user.zip'),
				'state' => trans('field.user.state'),
				'nid' => trans('field.user.nid'),
				'ref' => trans('field.user.ref'),
				'up' => trans('field.user.up'),
				'up_pos' => trans('field.user.up_pos'),
				'country' => trans('field.user.country'),
            ];

			$niceMessages = [
				"mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
			];

            $v = validator($data, [
				'name' => 'required|max:255',
				'nid' => 'required|max:30',
				'email' => 'required|email|max:255',
				'mobile' => 'required|digits_between:6,100|regex:/^[1-9]([0-9]+)$/|unique:'.$user_table,
				'address1' => 'max:100',
				'address2' => 'max:100',
				'city' => 'max:100',
				'zip' => 'max:10',
				'state' => 'max:100',
				'up' => 'required|exists:'.$user_table.',mobile',
				'up_pos' => 'required|in:1,2',
				'country' => 'required|exists:'.$country_table.','.$country_id,
				'password' => 'required|min:6|confirmed',
				'password2' => 'required|min:6|confirmed',
			], $niceMessages, $niceNames);
			
            if ($v->fails())
                return back()->withInput()->withErrors($v);

			//initiator
			$initiator = Tool::getInitiator();
			// $data['up'] = 0;
			// $data['up_pos'] = 0;

			try {
				DB::beginTransaction();
				$ref = Member::where("mobile", $data["up"])->first(["id"]);
				$upline = Member::getDownMostUsername($ref->id, $data["up_pos"]);

				$member = Member::create([
					'itype' => $initiator["itype"],
					'iid' => $initiator["iid"],
					'editor_type' => $initiator["itype"],
					'editor_id' => $initiator["iid"],
					'username' => Member::genUniqueUsername(),
					'name' => $data['name'],
					'email' => $data['email'],
					'nid' => $data['nid'],
					'address1' => $data['address1'],
					'address2' => $data['address2'],
					'city' => $data['city'],
					'zip' => $data['zip'],
					'state' => $data['state'],
					'country' => $data['country'],
					'mobile' => $data['mobile'],
					'ref' => $ref->id,
					'up' => $upline->id,
					'up_pos' => $data['up_pos'],
					'password' => bcrypt($data['password']),
					'password2' => bcrypt($data['password2']),
					'password_ori' => $data['password'],
					'password2_ori' => $data['password2'],
					'primary_acc' => Member::$primary_to_code['yes'], //take active status
					'status' => Member::$status_to_code['active'], //take active status
				]);

				if (Member::where([
					"up" => $member->up, 
					"up_pos" => $member->up_pos])
					->count() > 1
				) {
					throw new Exception("Position occupied.");
				}
			
				// set token
				$member->forceFill([
					'remember_token' => str_random(60),
				])->save();

				//create dependencies for user
				$member->wallet()->create();
				$member->memberStatus()->create();
				
				MemberRelationship::populate();
				
				//Log Action
				$this->setIdentifiers($member->id);
				$log_msg = "User Created for User ID #".$member->id.".";
				Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, [
					'data' => [
						'name' => $data['name'], 
						'email' => $data['email'], 
						"address1" => $data["address1"], 
						"address2" => $data["address2"], 
						"city" => $data["city"], 
						"zip" => $data["zip"], 
						"state" => $data["state"], 
						'country' => $data['country'], 
						'nid' => $data['nid'], 
						'status' => Member::$status_to_code['active'], 
						'primary_acc' => Member::$primary_to_code['yes'], 
						'mobile' => $data['mobile'],
						'up' => $data['up'],
						'up_pos' => $data['up_pos'],
					], 
					'uid'=>$member->id
				]);
				DB::commit();
			} catch (Exception $e) {
				//throw something here
				DB::rollBack();
				
				$v->getMessageBag()->add('all', "There are some errors during process, please try again later.");
				return back()->withInput()->withErrors($v);
			}

            return back()->withInput()->with('success','Account is created');
        }

        return view('admin.member.create', ['__fields'=>$fields]);
    }

    public function editProfile($id)
    {
		$_method =  __METHOD__;
		$_guard =  "admins";
		
		//initiator
		$initiator = Tool::getInitiator($_guard);

		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();
        $member_q = Member::where("id", "=", $id)
			->where("primary_acc", "=", Member::$primary_to_code["yes"])
			->where("company_status", "!=", Member::$company_status_to_code["yes"]);

		// if (!$this->user->can('admin-privilege', "is-master")) {
		// 	$member_q = $member_q->where("status", "!=", Member::$status_to_code["terminated"]);
		// }

		$member = $member_q->firstOrFail();
		
		//options data
		$fields['status']['options'] = array_values(Member::$status_to_code);
		$fields['user_type']['options'] = array_values(Member::$user_type_to_code);
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();
		$fields['phone_code']['options'] = $this->countryRepo->all_member_selectable_phone_code();

        if (request()->isMethod('post') && request()->has('__req')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			
			//profile
			if ($data['__req'] == '1') {
				$niceNames = [
					'name' => trans('field.user.name'),
					'email' => trans('field.user.email'),
					'nid' => trans('field.user.nid'),
					'address1' => trans('field.user.address1'),
					'address2' => trans('field.user.address2'),
					'city' => trans('field.user.city'),
					'zip' => trans('field.user.zip'),
					'state' => trans('field.user.state'),
					'country' => trans('field.user.country'),
					'mobile' => trans('field.user.mobile'),
					'alliance_status' => trans('field.member_status.alliance_status'),
					'status' => trans('field.user.status'),
					'user_type' => trans('field.user.user_type')
				];

				$niceMessages = [
					"mobile.regex" => trans('validation.digits_between', ["attribute" => $niceNames["mobile"], "min"=>6, "max"=>100]),
				];

				$v = validator($data, [
					'name' => 'required|max:255',
					'nid' => 'max:30',
					'email' => 'nullable|email|max:255',
					// 'mobile' => 'required|min:1|digits_between:6,100|regex:/^[1-9]([0-9]+)$/|unique:'.$user_table.',mobile,'.$member->id,
					// 'mobile' => 'required|min:1|digits_between:6,100|regex:/^[1-9]([0-9]+)$/',
					'mobile' => 'required|min:1|digits_between:6,100|regex:/^[0-9]([0-9]+)$/',
					'address1' => 'max:100',
					'address2' => 'max:100',
					'city' => 'max:100',
					'zip' => 'max:10',
					'state' => 'max:100',
					'country' => 'required|exists:'.$country_table.','.$country_id,
					'alliance_status' => 'nullable|in:10',
					'status' => 'required|in:'.implode(',', $fields['status']['options']),
					'user_type' => 'required|in:'.implode(',', $fields['user_type']['options']),
				], $niceMessages, $niceNames);

				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				if (Member::where('mobile', '=', $data['mobile'])->where('id', '!=' , $member->id)->exists() && $member->sub_id == 0) {
					$v->after(function($v) use ($data, $member) {
                        $v->errors()->add('mobile', trans('mobile number is not available'));
					});
					
					if ($v->fails())
				   		return back()->withInput()->withErrors($v);
				}

				try {
					// checkbox
					$data["alliance_status"] = request("alliance_status");

					DB::beginTransaction();
					$log_data["data"] = [
						'name' => $data['name'], 
						'email' => $data['email'], 
						'address1' => $data['address1'], 
						'address2' => $data['address2'], 
						'city' => $data['city'], 
						'zip' => $data['zip'], 
						'state' => $data['state'], 
						'country' => $data['country'], 
						'mobile' => $data['mobile'], 
						'nid' => $data['nid'], 
						'alliance_status' => $data["alliance_status"],
						'status' => $data['status'], 
						'user_type' => $data['user_type']
					];	
					$log_data["changes"] = Logger::retrieveChanges($member->toArray(), $log_data["data"]);

					if ($member->mobile != $data["mobile"]) {
						$member->memberStatus->mobile_status = null;
						$member->memberStatus->save();
					}

					$member->editor_type = $initiator['itype'];
					$member->editor_id = $initiator['iid'];
					$member->name = $data['name'];
					$member->email = $data['email'];
					$member->mobile = $data['mobile'];
					$member->address1 = $data['address1'];
					$member->address2 = $data['address2'];
					$member->city = $data['city'];
					$member->zip = $data['zip'];
					$member->state = $data['state'];
					$member->country = $data['country'];
					$member->nid = $data['nid'];
					$member->status = $data['status'];
					$member->user_type = $data['user_type'];
					$member->memberStatus->alliance_status = $data["alliance_status"];
					
					if ($member->save() && $member->memberStatus->save()) {
						//Log Action
						$this->setIdentifiers($member->id);
						$log_msg = "General Info update for User ID #".$member->id.".";
						Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
							$log_data,
							['uid' => $member->id]
						));
					}
					DB::commit();

				} catch (Exception $e) {
					DB::rollBack();
					
					$v->getMessageBag()->add('all', "There are some errors during process, please try again later.");
					return back()->withInput()->withErrors($v);
				}
				
				return back()->withSuccess('General Info updated.');
			}
			//bank information
			elseif ($data['__req'] == '4') {

				$niceNames = [
					'bank_name' => trans('field.user.bank_name'),
					'bank_branch_name' => trans('field.user.bank_branch_name'),
					'bank_acc_no' => trans('field.user.bank_acc_no'),
					'bank_sorting_code' => trans('field.user.bank_sorting_code'),
					'bank_iban' => trans('field.user.bank_iban'),
				];

				$v = validator($data, [
					'bank_name' => 'required|max:100',
					'bank_branch_name' => 'required|max:100',
					'bank_acc_no' => 'required|max:30',
					'bank_sorting_code' => 'max:30',
					'bank_iban' => 'max:30',
				], [], $niceNames);

				$log_data["data"] = [
					'bank_name' => $data['bank_name'], 
					'bank_branch_name' => $data['bank_branch_name'], 
					'bank_acc_no' => $data['bank_acc_no'], 
					'bank_sorting_code' => $data['bank_sorting_code'], 
					'bank_iban' => $data['bank_iban']
				];	
				$log_data["changes"] = Logger::retrieveChanges($member->toArray(), $log_data["data"]);

				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$member->editor_type= $initiator['itype'];
				$member->editor_id= $initiator['iid'];
				$member->bank_name = $data['bank_name'];
				$member->bank_branch_name = $data['bank_branch_name'];
				$member->bank_acc_no = $data['bank_acc_no'];
				$member->bank_sorting_code = $data['bank_sorting_code'];
				$member->bank_iban = $data['bank_iban'];
				
				if ($member->save()) {
				    //Log Action
                    $this->setIdentifiers($member->id);
                    $log_msg = "Bank Info update for User ID #".$member->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $member->id]
					));
				}

				return back()->withSuccess(trans('Bank Information Updated.'));
			}
			//beneficiary information
			elseif ($data['__req'] == '5') {
				$niceNames = [
					'beneficiary_name' => trans('field.user.beneficiary_name'),
					'beneficiary_nid' => trans('field.user.beneficiary_nid'),
					'beneficiary_user_relationship' => trans('field.user.beneficiary_user_relationship'),
				];

				$v = validator($data, [
					'beneficiary_name' => 'max:255',
					'beneficiary_nid' => 'max:30',
					'beneficiary_user_relationship' => 'max:30',
				], [], $niceNames);

				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'beneficiary_name' => $data['beneficiary_name'], 
					'beneficiary_nid' => $data['beneficiary_nid'], 
					'beneficiary_user_relationship' => $data['beneficiary_user_relationship']
				];
				$log_data["changes"] = Logger::retrieveChanges($member->toArray(), $log_data["data"]);

				$member->editor_type= $initiator['itype'];
				$member->editor_id= $initiator['iid'];
				$member->beneficiary_name = $data['beneficiary_name'];
				$member->beneficiary_nid = $data['beneficiary_nid'];
				$member->beneficiary_user_relationship = $data['beneficiary_user_relationship'];
				
				if ($member->save()) {
				    //Log Action
                    $this->setIdentifiers($member->id);
                    $log_msg = "Beneficiary Info update for User ID #".$member->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $member->id]
					));
				}

				return back()->withSuccess(trans('Beneficiary Information Updated'));
			}
        }
        return view('admin.member.profile', ['__fields'=>$fields])->withMember($member);
    }

	public function editAccountNetwork($id)
	{
		$_method =  __METHOD__;
		$_guard =  "admins";

		//initiator
		$initiator = Tool::getInitiator($_guard);
		
        $member = Member::where("id", $id)
			->where("primary_acc", Member::$primary_to_code["yes"])
			->where("status", "!=", Member::$status_to_code["terminated"])
			->firstOrFail();
		$user_table = $member->getTable();
		
		if (request()->isMethod("post")) {
			$data = request()->only("ref", "up", "up_pos");

			$niceNames = [
				'ref' => trans('field.user.ref'),
				'up' => trans('field.user.up'),
				'up_pos' => trans('field.user.up_pos'),
			];
            $v = validator($data, [
				'ref' => 'required|exists:'.$user_table.',username,primary_id,0|not_in:'.$member->username,
				'up' => 'required|exists:'.$user_table.',username|not_in:'.$member->username,
				'up_pos' => 'required|in:1,2',
			], [], $niceNames);
			
            if ($v->fails())
                return back()->withInput()->withErrors($v);

			//custom validation part 2
			$error_list = [];
			
			$ref_id = Member::where("username", $data["ref"])->value("id");
			$up_id = Member::where("username", $data["up"])->value("id");

			//check placement taken
			if ($member->up!=$up_id || $member->up_pos != $data["up_pos"]) {
				$user = Member::where('up', '=', $up_id)
					->where('up_pos', '=', $data['up_pos'])
					->first();
				if($user){
					$error_list[] = ['up_pos', trans('validation.unique', ['attribute' => $niceNames['up_pos']])];
				}
			} 

			//check network
			if (!Member::isDownlineOf($up_id, $ref_id)) {
				$error_list[] = ['up', e($data["ref"])." cannot be sponsor of any member under ".e($data["up"])." due to ".e($data["up"])." is not under ".e($data["ref"])." (Placement Tree).."];
			}
			if (Member::isDownlineOf($up_id, $member->id)) {
				$error_list[] = ['up', e($member->username)." cannot be placed under of ".e($data["up"])." due to ".e($data["up"])." is under ".e($member->username)." (Placement Tree)."];
			}
			
			if (Member::isDownlineOf($ref_id, $member->id)) {
				$error_list[] = ['ref', e($data["ref"])." cannot be sponsor of ".e($member->username)." due to ".e($data["ref"])." is under ".e($member->username)." (Placement Tree)."];
			}
			if (Member::isDownlineOf($ref_id, $member->id, "sponsor")) {
				$error_list[] = ['ref', e($data["ref"])." cannot be sponsor of ".e($member->username)." due to ".e($data["ref"])." is under ".e($member->username)." (Sponsor Tree)."];
			}

			if (!$this->user->can('admin-privilege', "is-master") && 
			($data["up_pos"] != $member->up_pos || $up_id != $member->up)
			) {
				$member_stat = MemberStat::where("uid", $member->id)->first();
				if ($member_stat && $member_stat->leg1_acc_amt + $member_stat->leg2_acc_amt > 0 ) {
					$error_list[] = ["all", e($member->username)." cannot be changed placement due to it has carry forward."];
				}
			}

			if ($up_id != $member->up) {

				if (count($error_list) == 0) {
					$all_downline_id = Member::getAllDownlineId($member->id, "placement");
					$err_downline_sponsor = [];
					foreach ($all_downline_id as $downline_id) {
						$downline = Member::find($downline_id);
						if ($downline->primary_id == 0 && !Member::isDownlineOf($downline->ref, $member->id)) {
							if ( !Member::isDownlineOf($downline->ref, $up_id) && !Member::isDownlineOf($up_id, $downline->ref)) {
								$err_downline_sponsor[] = $downline->username; 
							}
						}
					}
					if (count($err_downline_sponsor) > 0) {
						$last_downline = array_pop($err_downline_sponsor);
						$downline_str = implode(", ", $err_downline_sponsor);
						$downline_str .= ($downline_str? " and ": ""). $last_downline;
						$error_list[] = ["up", e($member->username)." cannot be changed placement due to different group line between sponsor and placement and the group line breaks at your downlines of ".e($downline_str).". Please change sponsor for ".e($downline_str)." as well in order to make sponsor and placement into same group line."];
					}
				}
			}

			$v->after(function($validator) use ($error_list){
				foreach ($error_list as $key=>$value) {
					$validator->errors()->add($value[0], $value[1]); 
				}
			});
			
            if ($v->fails())
                return back()->withInput()->withErrors($v);

			if ($member->ref != $ref_id ||
				$member->up != $up_id ||
				$member->up_pos != $data['up_pos'] 
			) {
				try {
					DB::beginTransaction();
					$old_data = ['ref'=>$member->ref, 'up'=>$member->up, 'up_pos'=>$member->up_pos];

					$date_op = DateTimeTool::operationDateTime("Y-m-d");
					if ($member->ref != $ref_id ) {
						$member_change_network = MemberChangeNetwork::firstOrNew([
							"uid" => $member->id,
							"network" => "ref",
							"tdate" => $date_op,
						]);
						if (!$member_change_network->from_up) {
							$member_change_network->from_up = $old_data["ref"];
						}
						$member_change_network->latest_editor_type = $initiator["itype"];
						$member_change_network->latest_editor_id = $initiator["iid"];
						$member_change_network->to_up = $ref_id;
						$member_change_network->downline_cache_flag = 0;
						$member_change_network->save();
					}

					if ($member->up != $up_id ||
					$member->up_pos != $data['up_pos'] ) {
						$member_change_network = MemberChangeNetwork::firstOrNew([
							"uid" => $member->id,
							"network" => "up",
							"tdate" => $date_op,
						]);
						if (!$member_change_network->from_up) {
							$member_change_network->from_up = $old_data["up"];
							$member_change_network->from_pos = $old_data["up_pos"];
						}
						$member_change_network->latest_editor_type = $initiator["itype"];
						$member_change_network->latest_editor_id = $initiator["iid"];
						$member_change_network->to_up = $up_id;
						$member_change_network->to_pos = $data['up_pos'];
						$member_change_network->downline_cache_flag = 0;
						$member_change_network->save();
					}

					$member->editor_type = $initiator['itype'];
					$member->editor_id = $initiator['iid'];
					$member->ref = $ref_id;
					$member->up = $up_id;
					$member->up_pos = $data['up_pos'];
					
					if ($member->save()) {
						//Log Action
						$this->setIdentifiers($id);
						$log_msg = "Network update for User ID #".$member->id.".";
						Logger::log($_method, $_guard, $this->getIdentifiers(), $log_msg, ['data'=>['ref'=>$ref_id, 'up'=>$up_id, 'up_pos'=>$data['up_pos']], 'old_data'=>$old_data, 'uid'=>$member->id]);
					}
					DB::commit();

					return back()->with('success', 'Network is changed');
				} catch (Exception $e) {
					DB::rollBack();
					
					$v = validator([], []);
					$v->getMessageBag()->add('all', "There are some errors during process, please try again later.");
					return back()->withInput()->withErrors($v);
				}
			}
			return back();
		}

		// get sponsor/placement
		$member["ref-username"] = Member::where("id", $member->ref)->value("username");
		$member["up-username"] = Member::where("id", $member->up)->value("username");

        return view('admin.member.change-network', ['__member'=>$member]);
	}

	public function getSendWelcomeEmailPage()
	{
		return view('admin.member.send-welcome-email');
	}

	public function sendWelcomeEmail() 
	{
		$niceNames = [
			"username" => trans("field.user.username"),
		];

		$data = request()->only(array_keys($niceNames));
		
		$v = validator($data, ["username"=>"required"]);
		if ($v->fails())
			return back()->withInput()->withErrors($v);

		$user = Member::where("username", $data["username"])
			->where("primary_acc", Member::$primary_to_code["yes"])
			->first();

		$error_list = [];

		if (!$user) {
			$error_list[] = ['username', trans('validation.exists', ['attribute' => $niceNames['username']])];
		}
		elseif (!$user->email) {
			$error_list[] = ['username', "Email Address is not set."];
		}

		$v->after(function($validator) use ($error_list){
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		if ($v->fails())
			return back()->withInput()->withErrors($v);

		try {
			$activation_code = Activation::where("status", Activation::$status_to_code["confirmed"])
				->where("uid", $user->id)
				->orderBy("id")
				->value("code");

			$pre_locale = app()->getLocale();
			app()->setLocale($user->locale);
			
			$mail_to = $user->email;
			$mail_info = [
				"username" => $user->username,
				"password" => $user["password_ori"],
				"password2" => $user["password2_ori"],
				"name" => $user->name,
				"nid" => $user->nid,
				"mobile" => $user->mobile,
				"email" => $user->email,
				"country" => $this->countryRepo->getCountryByCode($user->country),
				"package" => $activation_code ? trans("general.activation.title.".$activation_code) : "-",
			];

			\Mail::send('member.emails.new-user', ['__info' => $mail_info], function ($message) use ($mail_to)
			{
				$message->to($mail_to);
				$message->replyTo("no-reply@dongli88.com");
				$message->subject(trans('general.email.user.new-user.subject', ['sitename'=>trans('system.company')]));
			});
			app()->setLocale($pre_locale);
			return redirect($this->admin_slug.'/members/send-welcome-email')->with("success", "Email Sent to ".$user->email);

		} catch (Exception $e) {
			//ignore mail error
			$v = validator([], []);
			$v->getMessageBag()->add('all', "There are some errors during process, please try again later.");
			return back()->withInput()->withErrors($v);
		}
	}
}
