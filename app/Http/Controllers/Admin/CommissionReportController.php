<?php
namespace App\Http\Controllers\Admin;

use App\Models\Activation;
use App\Models\MemberWalletRecord;
use App\Models\Commission;
use App\Models\TransInfo;

use DB;

use Yajra\Datatables\Datatables;

class CommissionReportController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

	public function ajaxGetCommissionReportDataTable()
	{
        $collect_ary = [];
    	$act_table = with(new Activation)->getTable();
		$act_sql = "SELECT SUM(use_rwallet) AS rwallet_amount, SUM(use_awallet) AS awallet_amount, SUM(use_fwallet) AS fwallet_amount, SUM(price) AS total_price, SUM(amount) AS total_amount, DATE_FORMAT(DATE_ADD(act.bonus_at, INTERVAL 8 HOUR), '%Y %b') AS bonus_month, MONTH(DATE_ADD(act.bonus_at, INTERVAL 8 HOUR)) AS month, YEAR(DATE_ADD(act.bonus_at, INTERVAL 8 HOUR)) AS year FROM ".$act_table." act WHERE act.status = ".Activation::$status_to_code["confirmed"]." AND act.b_status = ".Activation::$b_status_to_code["confirmed"]." GROUP BY DATE_FORMAT(DATE_ADD(act.bonus_at, INTERVAL 8 HOUR), '%Y %b') ORDER BY year DESC, month DESC";

        $act_result = DB::select($act_sql);
        foreach ($act_result as $result) {
            $collect_ary[$result->bonus_month] = collect($result)->toArray();
        }

    	$comm_table = with(new Commission)->getTable();
		$comm_sql = "SELECT SUM(IF(comm.type = 2001, 0, comm.amount)) AS total_commission, SUM(IF(comm.type = 2001, comm.amount, 0)) AS total_roi, DATE_FORMAT(comm.bdate, '%Y %b') AS bonus_month FROM ".$comm_table." comm GROUP BY DATE_FORMAT(comm.bdate, '%Y %b')";

        $comm_result = DB::select(DB::raw($comm_sql));
        foreach ($comm_result as $result) {
            if (isset($collect_ary[$result->bonus_month])) {
                $collect_ary[$result->bonus_month] = array_merge($collect_ary[$result->bonus_month], collect($result)->toArray());
            }
            else {
                $collect_ary[$result->bonus_month] = collect($result)->toArray();
            }
        }

        $collect = collect(array_values($collect_ary)); 
        
        return Datatables::of($collect)
            ->editColumn('bonus_month', function ($list) {
                return $list["bonus_month"];
            })
            ->editColumn('total_amount', function ($list) {
                $total_amount = isset($list["total_amount"]) ? $list["total_amount"] : 0;
                return number_format($total_amount, 2);
            })
            ->editColumn('total_price', function ($list) {
                $total_price = isset($list["total_price"]) ? $list["total_price"] : 0;
                return number_format($total_price, 2);
            })
            ->editColumn('total_commission', function ($list) {
                $total_commission = isset($list["total_commission"]) ? $list["total_commission"] : 0;
                return number_format($total_commission, 2);
            })
            ->editColumn('total_roi', function ($list) {
                $total_roi = isset($list["total_roi"]) ? $list["total_roi"] : 0;
                return number_format($total_roi, 2);
            })
            ->editColumn('rwallet_amount', function ($list) {
                $rwallet_amount = isset($list["rwallet_amount"]) ? $list["rwallet_amount"] : 0;
                return number_format($rwallet_amount, 2);
            })
            ->editColumn('awallet_amount', function ($list) {
                $awallet_amount = isset($list["awallet_amount"]) ? $list["awallet_amount"] : 0;
                return number_format($awallet_amount, 2);
            })
            ->addColumn('total_payout_ratio', function ($list) {
                $total_price = isset($list["total_price"]) ? $list["total_price"] : 0;
                $total_commission = isset($list["total_commission"]) ? $list["total_commission"] : 0;
                $total_payout_ratio = $total_price ? ($total_commission) / $total_price * 100 : 0;
                 
                return number_format($total_payout_ratio, 2);
            })
            ->editColumn('fwallet_amount', function ($list) {
                $fwallet_amount = isset($list["fwallet_amount"]) ? $list["fwallet_amount"] : 0;
                return number_format($fwallet_amount, 2);
            })
            ->make(true);
	}

    public function salesReport()
    {
		$view = view('admin.report.sales-commission');

        return $view;
    }
}
