<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Activation;
use App\Models\Member;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class PersonalActivationController extends Controller
{
	protected $countryRepo;
    
	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'uid' => ['search'=>'raw', 'label'=>trans('field.user.id'), 
                    "as" => "((user.id = ? OR primary_user.id = ?) or (user.ref = ? AND user.primary_id != ?))", 
                    'value'=>''],
			'username' => ['search'=>'raw', 'label'=>trans('field.user.username'), 
                    "as" => "((user.username = ? OR primary_user.username = ?) OR (ref_user.username = ? AND (primary_user.username IS NULL OR primary_user.username != ?)))", 
                    'value'=>''],
			'act_type' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.act_type'), 
				'options'=>[
					'10' => trans('general.activation.act_type.10'), 
					'30' => trans('general.activation.act_type.30'),
					'70' => trans('general.activation.act_type.70'),
					'50' => trans('general.activation.act_type.50'),
					'90' => trans('general.activation.act_type.90'),
				], 
				'value'=>[]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.activation.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.activation.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Activation;
		$query->setTable($query->getTable().' AS act');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid');
		$query = $query->leftjoin(with(new Member)->getTable().' AS primary_user', 'user.primary_id', '=', 'primary_user.id');
		$query = $query->leftjoin(with(new Member)->getTable().' AS ref_user', 'user.ref', '=', 'ref_user.id');

		$search_fields = $this->getSearchAttributes();

        if (request()->has("uid") || request()->has("username")) {
            $query = $query->where("act.status", Activation::$status_to_code["confirmed"])
                ->where("act.b_status", Activation::$status_to_code["confirmed"]);
		    $query = $this->buildORMFilter($query, $search_fields);
        }
        else {
            $query  = $query->whereRaw(DB::raw("1 = 0"));
        }
        
		$total_query = $query->select([
			DB::raw("SUM(act.price) AS sum_price"),
			DB::raw("SUM(act.amount) AS sum_amount"),
		])->get();
		$price_sum = $total_query[0]["sum_price"] ? : 0;
		$amount_sum = $total_query[0]["sum_amount"] ? : 0;

		$query = $query->select([
			'act.*', 
			'user.username AS user_username',
			'user.name AS user_name',
			'user.nid AS user_nid',
			'user.ref AS user_ref',
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END AS user_country'),
		]);

		$datatables = Datatables::of($query);

		$datatables = $datatables->with([
			'grand_total' => [
				'price' => e(number_format($price_sum, 2)),
				'amount' => e(number_format($amount_sum, 2)),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('uid', function ($list) {
					return e($list['uid']);
				})
				->addColumn('uid-username', function ($list) {
					return e($list["user_username"]);
				})
				->addColumn('uid-name', function ($list) {
					return e($list["user_name"]);
				})
				->addColumn('uid-nid', function ($list) {
					return e($list["user_nid"]);
				})
				->addColumn('uid-country', function ($list) use ($country_code_to_name) {
					return e(isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-");
				})
				->addColumn('uid-ref', function ($list) {
					return e($list["user_ref"] ? $list["user_ref"] : "-");
				})
				->addColumn('ref-username', function ($list) {
					$user = Member::find($list["user_ref"]);
					return e($user ? $user->username : "-");
				})
				->editColumn('code', function ($list) {
					return e(trans("general.activation.title.".$list["code"]));
				})
				->editColumn('price', function ($list) {
					return e(number_format($list["price"], 2));
				})
				->editColumn('amount', function ($list) {
					return e(number_format($list["amount"], 2));
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.activation.personal-activations');

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
