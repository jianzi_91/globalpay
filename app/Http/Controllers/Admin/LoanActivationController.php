<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Admin;
use App\Models\Activation;
use App\Models\Member;
use App\Models\Wallet;
use App\Models\TransInfo;
use App\Models\Logger;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class LoanActivationController extends Controller
{	
	//Identifiers
	protected $identifiers = [];

	public function __construct()
	{
		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'rank' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.rank'), 
				'options'=>[
					'0' => trans('general.user.rank.title.0'), 
					'1100' => trans('general.user.rank.title.1100'), 
					'2100' => trans('general.user.rank.title.2100'),
					'3100' => trans('general.user.rank.title.3100'),
					'4100' => trans('general.user.rank.title.4100'),
					'5100' => trans('general.user.rank.title.5100'),
				], 
				'value'=>[]
			],
			'loan_status' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.loan_status'), 
				'options'=>[
					'10' => trans('general.activation.loan_status.10'), 
					'30' => trans('general.activation.loan_status.30'), 
				], 
				'value'=>[]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Member;
		$query->setTable($query->getTable().' AS user');
		$query = $query->join(with(new Wallet)->getTable().' AS wallet', function ($join) {
			$join->on("user.id", "=", "wallet.uid");
			$join->on("user.status", "!=", DB::raw(Member::$status_to_code["terminated"]));
		});
		$query = $query->join(with(new Member)->getTable().' AS sub_user', function ($join) {
			$join->on("sub_user.id", "=", "user.id");
			$join->orOn("sub_user.primary_id", "=", "user.id");
		});
		$query = $query->join(with(new Activation)->getTable().' AS act', "sub_user.id", "=", "act.uid");

		$search_fields = $this->getSearchAttributes();
		$query = $query->where("act.act_type", "=", Activation::$act_type_to_code["contra"])
			->where("act.status", "=", Activation::$status_to_code["confirmed"]);

		$query = $this->buildORMFilter($query, $search_fields);

		// result build
		$query = $query->select([
            'user.id',
            DB::raw('act.code AS act_code'),
            "user.rank",
            "user.crank",
            "user.username",
            "user.name",
            "user.ref",
            "user.up",
            "user.up_pos",
            'user.dstatus',
            'user.dpercent',
            DB::raw('wallet.dwallet AS wallet_dwallet'),
            DB::raw('act.id AS act_id'),
            DB::raw('act.aid AS act_aid'),
            DB::raw('act.loan_status AS act_loan_status'),
            DB::raw('act.loan_settle_at AS act_loan_settle_at'),
            "user.created_at",
        ]);
		$datatables = Datatables::of($query);
    	
		// process all showing data 
		return $datatables
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('act_code', function ($list) {
					$return = e(trans("general.activation.title.".$list["act_code"]));
					return $return;
				})
				->editColumn('rank', function ($list) {
					return e(trans('general.user.rank.title.'.$list['rank']));
				})
				->editColumn('crank', function ($list) {
					return e(trans('general.user.rank.title.'.$list['crank']));
				})
				->editColumn('username', function ($list) {
					return e($list['username']);
				})
				->editColumn('name', function ($list) {
					return e($list["name"]);
				})
                ->editColumn('ref', function ($list) {
					$user = Member::find($list['ref']);
					$return = e($user ? $user->username : "-");
					return $return;
				})
                ->editColumn('up', function ($list) {
					$user = Member::find($list['up']);
					$return = e($user ? $user->username : "-");
					return $return;
				})
                ->editColumn('up_pos', function ($list) {
					$user = Member::find($list['up_pos']);
					$return = e(trans("general.user.up_pos.".$list["up_pos"]));
					return $return;
				})
                ->editColumn('wallet_dwallet', function ($list) {
					$return = e(number_format($list["wallet_dwallet"], 4));
					return $return;
				})
                ->editColumn('act_aid', function ($list) {
					$user = Member::find($list['act_aid']);
					$return = e($user ? $user->username : "-");
					return $return;
				})
				->editColumn('act_loan_status', function ($list) {
					$return = e(trans("general.activation.loan_status.".$list["act_loan_status"]));
					return $return;
				})
				->editColumn('act_loan_settle_at', function ($list) {
					$return = e(DateTimeTool::systemToOperationDateTime($list["act_loan_settle_at"] > 0 ? $list["act_loan_settle_at"] : 0));
					return $return;
				})
				->editColumn('created_at', function ($list) {
					$return = e(DateTimeTool::systemToOperationDateTime($list["created_at"]));
					return $return;
				})
				->addColumn('action', function ($list) {
					$return = "";
					if ($this->user->can("admin-privilege", "loan-activations/distribute-loan-activation")) {
						$v = $this->validateDistributeLoanActivation($list["act_id"]);
						
						$user = Member::find($list['id']);	
						if (!$v->fails()) {
							$return .= '<button class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-distribute-loan-activation"
									data-id="'.e($list['act_id']).'" 
									data-package-name="'.e(trans('general.activation.title.'.$list['act_code'])) .'"
									data-user-id="'.e($list['id']) .' ('. e($user->username) .')"
									><i class="fa fa-pencil"></i> ' . e(trans("general.page.admin.loan-accounts-list.content.distribute_loan_sales")) . '</button>';
						}
					}
					return $return;
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.loan-activation.index');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }

	public function validateDistributeLoanActivation($id)
	{
		$error_list = [];

		$activation = Activation::find($id);
		if (!$activation) {
			$error_list[] = ["id", "No activation found."];
		}
		elseif ($activation->status != Activation::$status_to_code["confirmed"]) {
			$error_list[] = ["id", "Activation is not confirmed."];
		}
		elseif ($activation->act_type != Activation::$act_type_to_code["contra"]) {
			$error_list[] = ["id", "Activation is not loan activation."];
		}
		elseif ($activation->loan_status == Activation::$loan_status_to_code['confirmed']) {
			$error_list[] = ["id", "Activation is settled."];
		}
		elseif ($activation->distribute_after_loan == Activation::$distribute_after_loan_to_code['no'] 
		&& $activation->distribute_after_loan2 == Activation::$distribute_after_loan2_to_code['no']) {
			$error_list[] = ["id", "Activation is distributed."];
		}

		$v = validator([], []);

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});
		
		return $v;
	}

	public function distributeLoanActivation($id)
	{
		$v = $this->validateDistributeLoanActivation($id);
		if ($v->fails())
			return back()->withInput()->withErrors($v);
			
		try {
			with(new Member)->distributeLoanActivation($id);
			$activation = Activation::find($id);
			return back()->with(["success"=>"Sales #".$id." ".trans('general.activation.title.'.$activation['code'])." has been distributed."]);
		} catch (Exception $e) {

			return back()->with(["errors"=>collect(["all"=>"There is error during process, please try again later."])]);
		}

	}

	public function earlySettlement()
	{
		$field["from_wallet"]["options"] = ["rwallet", "awallet"];
		$niceNames = [
			"to_username" => "To Username (Debt Wallet)",
			"from_username" => "Paid By Username (Register Wallet)",
			"from_wallet" => "Paid By Wallet",
			"amount" => "Amount",
			"remarks" => trans("field.wallet_record.remarks"),
			"aremarks" => trans("field.wallet_record.aremarks"),
		];

		$data = request()->only(array_keys($niceNames));
		$v = validator($data, [
			"to_username" => "required",
			"from_username" => "",
			"from_wallet" => "in:".implode(",", $field["from_wallet"]["options"]),
			"amount" => "required|numeric|min:0.0001",
			"remarks" => "required|max:2000",
			"aremarks" => "max:2000",
		], [], $niceNames);

		if ($v->fails())
			return back()->withInput()->withErrors($v);
		
		$error_list = [];
		$to_user = Member::where("username", $data["to_username"])->first();
		if (!$to_user || !$to_user_wallet = Wallet::find($to_user->id)) {
			$error_list[] = ["to_username", e("To Username is not exists.")];
		}

		$user_pay = $user_pay_wallet = null;
		if ($data["from_username"]) {
			$user_pay = Member::where("username", $data["from_username"])->first();
			if (!$user_pay || !$user_pay_wallet = Wallet::find($user_pay->id)) {
				$error_list[] = ["from_username", e("Paid By Username is not exists.")];
			}
		}
		$amount = round($data["amount"], 6);

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		if ($v->fails())
			return back()->withInput()->withErrors($v);

		if (round($to_user_wallet->dwallet, 6) <= 0) {
			$error_list[] = ["amount", e("To Username do not have Debt Wallet balance.")];
		}
		elseif (round($amount, 6) > round($to_user_wallet->dwallet, 6)) {
			$error_list[] = ["amount", e("Amount is more than loan user's Debt Wallet balance.")];
		} 
		elseif ($user_pay_wallet && $data["from_wallet"] && round($amount, 6) > round($user_pay_wallet->{$data["from_wallet"]}, 6)) {
			$error_list[] = ["amount", e("Amount is more than Pay By User's ".trans("general.wallet.".$data["from_wallet"])." balance.")];
		}

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		if ($v->fails())
			return back()->withInput()->withErrors($v);

		try {
			DB::beginTransaction();
			$extra = [];
			if ($data["from_username"] && $data["from_wallet"]) {
				$descr_json = [
					'wallet' => $data["from_wallet"],
					'amount' => $amount,
					'from_uid' => $user_pay->id,
					'from_username' => $user_pay->username,
					'to_uid' => $to_user->id,
					'to_username' => $to_user->username,
					'remarks' => $data['remarks'],
					'aremarks' => $data['aremarks'],
				]; 
				$user_pay->deductWallet($user_pay->id, $data["from_wallet"], $amount, 'Early Settlement', $descr_json, TransInfo::$trans_type_to_code['package-early-settlement']);
				$extra = ["payer_id"=>$user_pay->id];
			}

			$descr_json = [
				'wallet' => "dwallet",
				'amount' => $amount,
				'from_uid' => $user_pay? $user_pay->id: null,
				'from_username' => $user_pay? $user_pay->username: null,
				'to_uid' => $to_user->id,
				'to_username' => $to_user->username,
				'remarks' => $data['remarks'],
				'aremarks' => $data['aremarks'],
			]; 
			$to_user->deductWallet($to_user->id, "dwallet", $amount, 'Early Settlement', $descr_json, TransInfo::$trans_type_to_code['package-early-settlement']);

			with(new Activation)->clearLoanActivation($to_user->id, $amount, $extra);
			
			DB::commit();
			return redirect($this->admin_slug."/loan-activations/settlement")->with("success", "Early Settlement is done.");
		} catch (Exception $e) {
			DB::rollBack();
			
			return back()->withInput()->with("errors", collect(["all" => "There is error during process, please try again later."]));
		}
	}

	public function getEarlySettlement()
	{
		$field["from_wallet"]["options"] = ["rwallet", "awallet"];
		$view = view("admin.loan-activation.settlement", ["__field" => $field]);
		return $view;
	}
}
