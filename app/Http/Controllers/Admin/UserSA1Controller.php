<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Member;
use App\Models\MemberStatus;
use App\Models\Activation;
use App\Models\Country;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class UserSA1Controller extends Controller
{
	protected $countryRepo;

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'nid' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.nid'), 'value'=>''],
			'email' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.email'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'country' => ['search'=>'=', 'label'=>trans('field.user.country'), 'as'=>"country", "table" => "user",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'sa_status' => ['search'=>'in', 'table'=>'ms', 'label'=>trans('field.member_status.sa_status'), 
				'options'=>[
					'10' => trans('general.member_status.sa_status.10'),
					'0' => trans('general.member_status.sa_status.0'),
                ],
                "value" => [],
			],
			'status' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.status'), 
				'options'=>[
					'10' => trans('general.user.status.10'),
					'30' => trans('general.user.status.30'),
					'90' => trans('general.user.status.90')
				], 
				'value' => ["10"]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();
		$status_table = with(new MemberStatus)->getTable();
		$activation_table = with(new Activation)->getTable();

		$query = new Member;
		$query->setTable($member_table.' AS user');
		$query = $query->join($status_table.' AS ms', "ms.uid", "=", "user.id")
			->join($activation_table.' AS act', function ($join) {
				$join->on("act.upid", "=", "user.id");
				$join->on("act.act_type", "=", DB::raw(Activation::$act_type_to_code["special"]));
			})->select([
				"user.id",
				"user.username",
				"user.name",
				"user.nid",
				"user.country",
				"user.email",
				"user.mobile",
				"ms.sa_status",
				"user.status",
				"user.created_at",
				DB::raw("MIN(act.created_at) AS min_package_date"),
				DB::raw("MAX(act.bonus_at) AS max_activated_date"),
				DB::raw("GROUP_CONCAT(DISTINCT aid) AS group_concat_aid"),
			])->groupBy("user.id");

		// filter build
		$search_fields = $this->getSearchAttributes();

		$query = $this->buildORMFilter($query, $search_fields);
		$datatables = Datatables::of($query);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		// process all showing data 
		return $datatables
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('username', function ($list) {
					return e($list['username']);
				})
				->editColumn('name', function ($list) {
					return e($list["name"]);
				})
				->editColumn('nid', function ($list) {
					return e($list['nid']);
				})
				->editColumn('country', function ($list) use ($country_code_to_name) {
					return e(isset($country_code_to_name[$list["country"]]) ? $country_code_to_name[$list["country"]] : "-");
				})
				->editColumn('email', function ($list) {
					return e($list["email"]);
				})
				->editColumn('mobile', function ($list) {
					return e($list["mobile"]);
				})
				->editColumn('sa_status', function ($list) {
					return e(trans("general.member_status.sa_status.".$list["sa_status"]));
				})
				->editColumn('status', function ($list) {
					return e(trans("general.user.status.".$list["status"]));
				})
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->editColumn('min_package_date', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['min_package_date']));
				})
				->editColumn('max_activated_date', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['max_activated_date'] != "0000-00-00 00:00:00" ? $list['max_activated_date'] : null));
				})
				->editColumn('group_concat_aid', function ($list) {
					$activator_ids = explode(",", $list["group_concat_aid"]);
					$activator_usernames = [];
					foreach ($activator_ids as $activator_id) {
						if ($activator_id == 0) {
							continue;
						}
						$member_username = Member::where("id", $activator_id)->value("username");
						if ($member_username) {
							$activator_usernames[] = $member_username;
						}
					}
					$activator_usernames = implode(", ", $activator_usernames);
					return e($activator_usernames);
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.member.sa1-list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
