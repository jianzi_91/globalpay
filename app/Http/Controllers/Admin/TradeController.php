<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;

use App\Repositories\TradeRepo;
use App\Repositories\MemberRepo;
use App\Models\Member;
use App\Models\Admin;

use Yajra\Datatables\Datatables;

class TradeController extends Controller
{
    private $tradeRepo;
    private $memberRepo;

    public function __construct(TradeRepo $tradeRepo, MemberRepo $memberRepo)
    {
        $this->tradeRepo = $tradeRepo;
        $this->memberRepo = $memberRepo;

        parent::__construct();
    }

    public function editTradeUnit()
    {
        return view('admin.trade.edit-unit');
    }

    public function postEditTradeUnit()
    {
        $input = request()->all();

        $niceNames = [
            'username'  => trans('field.user.username'),
            'qty' => trans('trade.qty'),
            'price' => trans('trade.price'),
        ];

        $validator = Validator::make($input, [
            'username' => 'required|string|max:100',
            'price' => 'required|numeric|min:'.config('trade.min_limit'),
            'qty' => 'required|integer|min:1',
            'type' => 'required|in:c,d',
            'remark' => 'required|string|max:255',
            'admin_remark' => 'string|max:255',
        ], [], $niceNames);

        if ($validator->fails()) {
            $this->throwValidationException(request(), $validator);
        }

        $uid = $this->memberRepo->getUidByUsername($input['username']);

        // business rule validation
        $validator->after(function($validator) use ($uid, $input, $niceNames) {
            if (!$uid) {
                $validator->errors()->add('username', trans('validation.exists', ['attribute' => $niceNames['username']]));
            }
        });

        if ($validator->fails()) {
            $this->throwValidationException(request(), $validator);
        }

        if ($input['type'] == 'd') {
            $input['qty'] = -$input['qty'];
        }

        // nullify admin remark if it's empty
        if (empty($input['admin_remark'])) {
            $input['admin_remark'] = null;
        }

        $this->tradeRepo->editTradeUnit($uid, $input['qty'], $input['price'], $input['remark'], $input['admin_remark'], Admin::$user_code, auth('admins')->id());

        return redirect()->back()->with([
            'success' => abs($input["qty"]) . ' ' . trans_choice('trade.unit', $input["qty"]) . ' of eShare ' . trans_choice('common.have', $input["qty"]) . ' been ' . ($input['type'] == 'c' ? 'credited.' : 'debited.')
        ]);
    }
}