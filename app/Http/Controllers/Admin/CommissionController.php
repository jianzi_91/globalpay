<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Commission;
use App\Models\Member;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class CommissionController extends Controller
{
	protected $countryRepo;

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'like', 'table'=>'comm', 'label'=>trans('field.commission.id'), 'value'=>''],
			'uid' => ['search'=>'=', 'table'=>'comm', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'nid' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.nid'), 'value'=>''],
			'country' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.country'), 'as'=>"country",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'type' => ['search'=>'in', 'table'=>'comm', 'label'=>trans('field.commission.type'), 
				'options'=>[
					'1001' => trans('general.commission.type.1001'), 
					'2001' => trans('general.commission.type.2001'),
				], 
				'value'=>[]
			],
			'status' => ['search'=>'in', 'table'=>'comm', 'label'=>trans('field.commission.status'), 
				'options'=>[
					'10' => trans('general.commission.status.10'), 
					'30' => trans('general.commission.status.30'),
					'90' => trans('general.commission.status.90'),
				], 
				'value'=>[]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'comm', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.commission.bdate')]), 'value'=>"", 'as'=>'bdate', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'comm', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.commission.bdate')]), 'value'=>"", 'as'=>'bdate', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Commission();
		$query->setTable($query->getTable().' AS comm');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'comm.uid');

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		$query = $query->select([
			'comm.*', 
			'user.username AS user_username',
			'user.name AS user_name',
			'user.nid AS user_nid',
			'user.country AS user_country',
		]);

		$datatables = Datatables::of($query);

		$amount_sum = $query->sum("comm.amount");

		$datatables = $datatables->with([
			'grand_total' => [
				'amount' => number_format($amount_sum, 4),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('id', function ($list) {
					return $list["id"];
				})
				->editColumn('uid', function ($list) {
					return $list["uid"];
				})
				->addColumn('uid-username', function ($list) {
					return $list["user_username"] ? $list["user_username"] : "-";
				})
				->addColumn('uid-name', function ($list) {
					return $list["user_name"] ? $list["user_name"] : "-";
				})
				->addColumn('uid-nid', function ($list) {
					return $list["user_nid"] ? $list["user_nid"] : "-";
				})
				->addColumn('uid-country', function ($list) use ($country_code_to_name) {
					return isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-";
				})
				->editColumn('amount', function ($list) {
					return number_format($list["amount"], 4);
				})
				->editColumn('percent', function ($list) {
					return number_format($list["percent"], 2);
				})
				->editColumn('type', function ($list) {
					return trans('general.commission.type.'.$list['type']);
				})
				->editColumn('bdate', function ($list) {
					return $list["bdate"];
				})
				->editColumn('status', function ($list) {
					return trans("general.commission.status.".$list["status"]);
				})
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.commission.list');

		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }
}
