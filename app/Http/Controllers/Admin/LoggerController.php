<?php

namespace App\Http\Controllers\Admin;

use DB;
use Schema;

use App\Models\Logger;
use App\Models\Member;
use App\Models\Admin;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class LoggerController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

	private function getSearchAttributes()
	{
		//search fields
		return [
			'itype' => ['search'=>'in', 'table'=>"logger", 'label' => trans('field.logger.itype'),
				'options'=>[
					'900' => trans("system.admin"), 
					'100' => trans("system.member"), 
				], 
				'value'=>[]
			],
			'iid' => ['search'=>'ignore', 'label' => trans('field.logger.iid'), "value" => ""],

			'id1' => ['search'=>'like', 'table'=>'logger', "as" => "id1", 'label' => trans('field.logger.id1'), "value" => ""],
			'id2' => ['search'=>'like', 'table'=>'logger', "as" => "id2", 'label' => trans('field.logger.id2'), "value" => ""],
			'path' => ['search'=>'like', 'table'=>'logger', "as" => "path", 'label' => trans('field.logger.path'), "value" => ""],
			'ip' => ['search'=>'like', 'table'=>'logger', "as" => "ip", 'label' => trans('field.logger.ip'), "value" => ""],

			'archive' => ['search'=>'ignore', 'label'=>trans('general.page.admin.logger.content.archive'),
				'options'=>[
					'0' => trans('general.page.admin.logger.content.no'), 
					'1' => trans('general.page.admin.logger.content.yes'), 
				], 
				'value'=>""
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'logger', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'logger', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
        $get_archive = request()->get("archive");

        $query = new Logger;
        $table = $query->getTable();
        $table_archive = $query->getTable(). "_archive";

        if (Schema::hasTable($table_archive)) {
		    $query->setTable($table_archive." AS logger");
            $archive_time = $query->max("created_at");
        }
        else {
            $archive_time = null;
        }

        if ($get_archive && Schema::hasTable($table_archive)) {
		    $query->setTable($table_archive." AS logger");
        }
        else {
            $query->setTable($table. " AS logger");
        }

		$query = $query->leftJoin(with(new Member)->getTable().' AS iid_user', function($join) {
			$join->on('logger.iid', '=', 'iid_user.id');
			$join->on('logger.itype', '=', DB::raw("100"));
		});
		$query = $query->leftJoin(with(new Admin)->getTable().' AS iid_admin', function($join) {
			$join->on('logger.iid', '=', 'iid_admin.id');
			$join->on('logger.itype', '=', DB::raw("900"));
		});

		if ($value = request()->get("iid")) {
			$query = $query->where(function($query) use ($value) {
				$query->where('iid_user.username', 'like', "%".$value."%")
					->orWhere('iid_admin.name', 'like', "%".$value."%");
			});
		}

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		// result build
		$query = $query->select([
            "logger.itype",
            "logger.iid",
            "logger.id1",
            "logger.id2",
            "logger.log_data",
            "logger.path",
            "logger.action",
            "logger.ip",
            "logger.created_at",
            "logger.descr",
        ]);
		$datatables = Datatables::of($query);

		$datatables = $datatables->with([
			'info' => [
				'last_archive' => e(DateTimeTool::systemToOperationDateTime($archive_time)),
			]
		]);
    	
		return $datatables
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return e($return);
				})
				->addColumn('iid-text', function ($list) {
					$return = "-";
					if ($list["itype"] == "900") {
						$user = Admin::find($list["iid"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["itype"] == "100") {
						$user = Member::find($list["iid"]);
						$return = $user ? $user->username : "-";
					}
					return e($return);
				})
				->editColumn('id1', function ($list) {
					$return = $list["id1"] ? : "-";
					return e($return);
				})
				->editColumn('id2', function ($list) {
					$return = $list["id2"] ? : "-";
					return e($return);
				})
                ->editColumn('action', function ($list) {
                    $return = $this->getActionTranslate($list["action"]);
                    return e($return);
                })
                ->editColumn('log_data', function ($list) {
                    $return = $this->getActionLogTranslate($list);
                    return nl2br(e($return));
                })
				->editColumn('ip', function ($list) {
					return e($list["ip"]);
                })
                ->rawColumns(['log_data'])
            ->make(true);
	}
	
    public function index()
    {
		$view = view('admin.logger.index');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function getActionTranslate($function_name)
    {
        $function_to_name = [
            "App\Models\Member::walletUpdate" => "Wallet Update",
            "App\Models\Member::updateUserStat" => "Update Member Stats",
            "App\Models\Member::addActivation" => "Add Package",
            "App\Models\Member::upgradeUserRank" => "Update Member Package Rank",
            "App\Models\Member::autoBuyShare" => "Auto Buy System Share",
            "App\Models\Member::autoPlaceBuyShare" => "Auto Place Buy Order",
            "App\Http\Controllers\Admin\UserController::editAccountNetwork" =>"Edit Network",
            "App\Http\Controllers\Admin\ActivationController::addLoanActivation" => "Add Loan Package",
            "App\Models\Activation::clearLoanActivation" => "Settle Loan Package",
            "App\Http\Controllers\Member\UserController::create" => "Register Account",
            "App\Http\Controllers\Member\FranchiseController::create" => "Register Franchise Account",
            "App\Models\Member::createSubAccount" => "Add Sub Accounts",
            "App\Models\Member::cancelActivation" => "Cancel Package",
            "App\Http\Controllers\Admin\AdminUserController::create" => "Add New Admin",
            "App\Http\Controllers\Admin\AdminUserController::update" => "Edit Admin",
            "App\Http\Controllers\Admin\WithdrawalController::edit" => "Edit Withdrawal",
            "App\Http\Controllers\Admin\AdminUserController::profileEdit" => "Admin Edit Profile",
            "App\Http\Controllers\Admin\UserController::create" => "Add New Member",
            "App\Models\Member::walletTransfer" => "Wallet Transfer",
            "App\Http\Controllers\Member\UserController::profileEdit" => "Edit Profile",
            "App\Http\Controllers\Admin\UserController::editProfile" => "Edit Profile",
            "App\Http\Controllers\Admin\UserController::editSubAccountProfile" => "Edit Sub Account Profile",
            "App\Http\Controllers\Auth\PasswordController::resetPasswordExtra" => "Reset Passwords",
            "App\Http\Controllers\Admin\UserPasswordsController::update" => "Reset Member Passwords",
        ];

        $return = isset($function_to_name[$function_name]) ? $function_to_name[$function_name] : "Unformatted - ".$function_name;

        return $return;
    }

    public function getActionLogTranslate($log)
    {
        $return = "";

        switch ($log["action"]) {
            case "App\Models\Member::walletUpdate":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Type: " . (isset($data["data"]["type"]) ? ($data["data"]["type"] == "d" ? "Debit" : "Credit") : "-"). "\n";
                $return .= "Wallet Type: " . (isset($data["data"]["wallet"]) ? trans("general.wallet.".$data["data"]["wallet"]) : ""). "\n";
                $return .= "Amount: " . (isset($data["data"]["amount"]) ? number_format($data["data"]["amount"], 2) : ""). "\n";
                break;

            case "App\Models\Member::updateUserStat":
                $data = json_decode($log["log_data"], true);

                $return .= "Activation ID: " . (isset($data["data"]["activation_id"]) ? $data["data"]["activation_id"] : ""). "\n";

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Price: " . (isset($data["data"]["price"]) ? number_format($data["data"]["price"], 2) : ""). "\n";
                $return .= "Amount: " . (isset($data["data"]["amount"]) ? number_format($data["data"]["amount"], 2) : ""). "\n";
                break;
            
            case "App\Models\Member::addActivation":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Activation: " . (isset($data["data"]["package_code"]) ? trans("general.activation.title.".$data["data"]["package_code"]) : ""). "\n";
                break;

            case "App\Models\Member::upgradeUserRank":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Purchase Rank: " . (isset($data["data"]["rank"]) ? trans("general.user.rank.title.".$data["data"]["rank"]) : ""). "\n";
                break;

            case "App\Models\Member::autoBuyShare":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Wallet Type: " . (isset($data["data"]["wallet"]) ? trans("general.wallet.".$data["data"]["wallet"]) : ""). "\n";
                $return .= "Amount: " . (isset($data["data"]["amount"]) ? number_format($data["data"]["amount"], 2) : ""). "\n";
                $return .= "eShare Price: " . (isset($data["data"]["share_price"]) ? number_format($data["data"]["share_price"], 3) : ""). "\n";
                break;
            
            case "App\Models\Member::autoPlaceBuyShare":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["user_id"])) {
                    $user_id = $data["data"]["user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Amount: " . (isset($data["data"]["amount"]) ? number_format($data["data"]["amount"], 2) : ""). "\n";
                $return .= "eShare Price: " . (isset($data["data"]["share_price"]) ? number_format($data["data"]["share_price"], 3) : ""). "\n";
                break;

            case "App\Http\Controllers\Admin\UserController::editAccountNetwork":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";

                if (isset($data["old_data"]["ref"])) {
                    $user_id = $data["old_data"]["ref"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Sponsor From: ".$username." (" . $user_id . ")";

                if (isset($data["data"]["ref"])) {
                    $user_id = $data["data"]["ref"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= " To: ".$username." (" . $user_id . ")\n";

                if (isset($data["old_data"]["up"])) {
                    $user_id = $data["old_data"]["up"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Placement From: ".$username." (" . $user_id . ")";

                if (isset($data["data"]["up"])) {
                    $user_id = $data["data"]["up"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= " To: ".$username." (" . $user_id . ")\n";
                $return .= "Placement Position From: " . (isset($data["old_data"]["up_pos"]) ? trans("general.user.up_pos.".$data["old_data"]["up_pos"]) : ""). 
                    " To: " . (isset($data["data"]["up_pos"]) ? trans("general.user.up_pos.".$data["data"]["up_pos"]) : "") . "\n";
                break;

            case "App\Http\Controllers\Admin\ActivationController::addLoanActivation":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Debt Status: ".(isset($data["data"]["dstatus"]) ? trans("general.user.dstatus.".$data["data"]["dstatus"]) : "")."\n";
                $return .= "Deduct Percentage: ".(isset($data["data"]["dpercent"]) ? number_format($data["data"]["dpercent"], 2) : "")."%\n";
                break; 
            
            case "App\Models\Activation::clearLoanActivation":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Amount: " . (isset($data["data"]["deduct_amount"]) ? number_format($data["data"]["deduct_amount"], 2) : ""). "\n";
                break;
            
            case "App\Http\Controllers\Member\UserController::create":
                $data = json_decode($log["log_data"], true);

                $return .= "Username: " . (isset($data["data"]["username"]) ? $data["data"]["username"] : ""). "\n";
                $return .= "Name: " . (isset($data["data"]["name"]) ? $data["data"]["name"] : ""). "\n";
                $return .= "IC / Passport No: " . (isset($data["data"]["nid"]) ? $data["data"]["nid"] : ""). "\n";
                $return .= (isset($data["data"]["dob"]) ? "Date of Birth: ".$data["data"]["dob"]. "\n" : "");
                $return .= "Email: " . (isset($data["data"]["email"]) ? $data["data"]["email"] : ""). "\n";
                $return .= "Mobile No: " . (isset($data["data"]["mobile"]) ? $data["data"]["mobile"] : ""). "\n";
                $return .= "Country: " . (isset($data["data"]["country"]) ? $data["data"]["country"] : ""). "\n";

                if (isset($data["data"]["ref"])) {
                    $user_id = $data["data"]["ref"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Sponsor: ".$username." (" . $user_id . ")\n";

                if (isset($data["data"]["up"])) {
                    $user_id = $data["data"]["up"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Upline: ".$username." (" . $user_id . ")\n";
                $return .= "Placement Position: " . (isset($data["data"]["up_pos"]) ? trans("general.user.up_pos.".$data["data"]["up_pos"]) : "") . "\n";
                break;

            case "App\Http\Controllers\Member\FranchiseController::create":
                $data = json_decode($log["log_data"], true);

                $return .= "Username: " . (isset($data["data"]["username"]) ? $data["data"]["username"] : ""). "\n";
                $return .= "Name: " . (isset($data["data"]["name"]) ? $data["data"]["name"] : ""). "\n";
                $return .= "IC / Passport No: " . (isset($data["data"]["nid"]) ? $data["data"]["nid"] : ""). "\n";
                $return .= (isset($data["data"]["dob"]) ? "Date of Birth: ".$data["data"]["dob"]. "\n" : "");
                $return .= "Email: " . (isset($data["data"]["email"]) ? $data["data"]["email"] : ""). "\n";
                $return .= "Mobile No: " . (isset($data["data"]["mobile"]) ? $data["data"]["mobile"] : ""). "\n";
                $return .= "Country: " . (isset($data["data"]["country"]) ? $data["data"]["country"] : ""). "\n";

                if (isset($data["data"]["franchise_user_id"])) {
                    $user_id = $data["data"]["franchise_user_id"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Franchise Main ID: ".$username." (" . $user_id . ")\n";

                if (isset($data["data"]["ref"])) {
                    $user_id = $data["data"]["ref"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Sponsor: ".$username." (" . $user_id . ")\n";

                if (isset($data["data"]["up"])) {
                    $user_id = $data["data"]["up"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Upline: ".$username." (" . $user_id . ")\n";
                $return .= "Placement Position: " . (isset($data["data"]["up_pos"]) ? trans("general.user.up_pos.".$data["data"]["up_pos"]) : "") . "\n";
                break;

            case "App\Models\Member::createSubAccount":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                break;

            case "App\Models\Member::cancelActivation":
                $data = json_decode($log["log_data"], true);
                $return .= "Activation ID: " . (isset($data["data"]["id"]) ? $data["data"]["id"] : ""). "\n";
                break;

            case "App\Http\Controllers\Admin\AdminUserController::create":
                $data = json_decode($log["log_data"], true);
                $return .= "Username: " . (isset($data["data"]["username"]) ? $data["data"]["username"] : ""). "\n";
                $return .= "Name: " . (isset($data["data"]["name"]) ? $data["data"]["name"] : ""). "\n";
                $return .= "Email: " . (isset($data["data"]["email"]) ? $data["data"]["email"] : ""). "\n";
                $return .= "Country: " . (isset($data["data"]["country"]) ? $data["data"]["country"] : ""). "\n";
                break;

            case "App\Http\Controllers\Admin\AdminUserController::update":
                $data = json_decode($log["log_data"], true);

                $return .= (isset($data["data"]["username"]) ? "Username: " . $data["data"]["username"] . "\n" : "");
                $return .= (isset($data["data"]["name"]) ? "Name: " . $data["data"]["name"] . "\n" : "");
                $return .= (isset($data["data"]["email"]) ? "Email: " . $data["data"]["email"] . "\n" : "");
                $return .= (isset($data["data"]["country"]) ? "Country: " . $data["data"]["country"] . "\n" : "");
                $return .= (isset($data["data"]["privileges"]) ? "Privileges: " . implode(", ", $data["data"]["privileges"]) . "\n" : "");
                
                break;
            
            case "App\Http\Controllers\Admin\WithdrawalController::edit":
                $data = json_decode($log["log_data"], true);

                $return .= (isset($data["data"]["aremarks"]) ? "Admin Remarks: " . $data["data"]["aremarks"] . "\n" : "");
                $return .= (isset($data["data"]["status"]) ? "Withdrawal Status: " . trans("general.withdrawal.status.".$data["data"]["status"]) . "\n" : "");
                break;
            
            case "App\Http\Controllers\Admin\AdminUserController::profileEdit":
                $data = json_decode($log["log_data"], true);
                
                $return .= "Name: " . (isset($data["data"]["name"]) ? $data["data"]["name"] : ""). "\n";
                $return .= "Email: " . (isset($data["data"]["email"]) ? $data["data"]["email"] : ""). "\n";
                $return .= "Country: " . (isset($data["data"]["country"]) ? $data["data"]["country"] : ""). "\n";
                break;

            case "App\Http\Controllers\Admin\UserController::create":
                $data = json_decode($log["log_data"], true);

                $return .= "Username: " . (isset($data["data"]["username"]) ? $data["data"]["username"] : ""). "\n";
                $return .= "Name: " . (isset($data["data"]["name"]) ? $data["data"]["name"] : ""). "\n";
                $return .= "IC / Passport No: " . (isset($data["data"]["nid"]) ? $data["data"]["nid"] : ""). "\n";
                $return .= (isset($data["data"]["dob"]) ? "Date of Birth: ".$data["data"]["dob"]. "\n" : "");
                $return .= "Email: " . (isset($data["data"]["email"]) ? $data["data"]["email"] : ""). "\n";
                $return .= "Mobile No: " . (isset($data["data"]["mobile"]) ? $data["data"]["mobile"] : ""). "\n";
                $return .= "Country: " . (isset($data["data"]["country"]) ? $data["data"]["country"] : ""). "\n";

                if (isset($data["data"]["ref"])) {
                    $user_id = $data["data"]["ref"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Sponsor: ".$username." (" . $user_id . ")\n";

                if (isset($data["data"]["up"])) {
                    $user_id = $data["data"]["up"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "Upline: ".$username." (" . $user_id . ")\n";
                $return .= "Placement Position: " . (isset($data["data"]["up_pos"]) ? trans("general.user.up_pos.".$data["data"]["up_pos"]) : "") . "\n";
                $return .= "Offer Rank: " . (isset($data["data"]["crank"]) ? trans("general.user.rank.title.".$data["data"]["crank"]) : "") . "\n";
                break;

            case "App\Models\Member::walletTransfer":
                $data = json_decode($log["log_data"], true);

                if (isset($data["data"]["from_uid"])) {
                    $user_id = $data["data"]["from_uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "From User ID: ".$username." (" . $user_id . ")\n";

                if (isset($data["data"]["to_uid"])) {
                    $user_id = $data["data"]["to_uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "To User ID: ".$username." (" . $user_id . ")\n";
                $return .= "Amount: " . (isset($data["data"]["amount"]) ? number_format($data["data"]["amount"], 2) : ""). "\n";
                $return .= "From Wallet: " . (isset($data["data"]["wallet"]) ? trans("general.wallet.".$data["data"]["wallet"]) : ""). "\n";
                $return .= "To Wallet: " . (isset($data["data"]["to_wallet"]) ? trans("general.wallet.".$data["data"]["to_wallet"]) : ""). "\n";
                break;
            
            case "App\Http\Controllers\Member\UserController::profileEdit":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                
                if (!isset($data["changes"])) {
                    if (isset($data["data"])) {
                        $return .= (isset($data["data"]["email"]) ? "Email: " . $data["data"]["email"]. "\n" : "");
                        $return .= (isset($data["data"]["mobile"]) ? "Mobile No: " . $data["data"]["mobile"]. "\n" : "");
                        $return .= (isset($data["data"]["city"]) ? "City: " . $data["data"]["city"]. "\n" : "");
                        $return .= (isset($data["data"]["zip"]) ? "Zip: " . $data["data"]["zip"]. "\n" : "");
                        $return .= (isset($data["data"]["state"]) ? "State: " . $data["data"]["state"]. "\n" : "");
                        $return .= (isset($data["data"]["address1"]) ? "Address 1: " . $data["data"]["address1"]. "\n" : "");
                        $return .= (isset($data["data"]["address2"]) ? "Address 2: " . $data["data"]["address2"]. "\n" : "");

                        $return .= (isset($data["data"]["bank_name"]) ? "Bank Name: " . $data["data"]["bank_name"]. "\n" : "");
                        $return .= (isset($data["data"]["bank_branch_name"]) ? "Bank Branch Name: " . $data["data"]["bank_branch_name"]. "\n" : "");
                        $return .= (isset($data["data"]["bank_acc_no"]) ? "Account No: " . $data["data"]["bank_acc_no"]. "\n" : "");
                        $return .= (isset($data["data"]["bank_sorting_code"]) ? "Bank Sorting Code: " . $data["data"]["bank_sorting_code"]. "\n" : "");
                        $return .= (isset($data["data"]["bank_iban"]) ? "Bank IBAN Code: " . $data["data"]["bank_iban"]. "\n" : "");

                        $return .= (isset($data["data"]["beneficiary_name"]) ? "Beneficiary Name: " . $data["data"]["beneficiary_name"]. "\n" : "");
                        $return .= (isset($data["data"]["beneficiary_nid"]) ? "Beneficiary IC / Passport No: " . $data["data"]["beneficiary_nid"]. "\n" : "");
                        $return .= (isset($data["data"]["beneficiary_user_relationship"]) ? "Relation With Beneficiary: " . $data["data"]["beneficiary_user_relationship"]. "\n" : "");
                    }
                    else {
                        $return .= $log["descr"];
                    }
                }
                else {
                    $changes_data = $data["changes"];
                    $return .= (isset($changes_data["old_data"]["email"]) ? "Email (From): " . $changes_data["old_data"]["email"] . " (To): " . $changes_data["new_data"]["email"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["mobile"]) ? "Mobile No (From): " . $changes_data["old_data"]["mobile"] . " (To): " . $changes_data["new_data"]["mobile"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["city"]) ? "City (From): " . $changes_data["old_data"]["city"] . " (To): " . $changes_data["new_data"]["city"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["zip"]) ? "Zip (From): " . $changes_data["old_data"]["zip"] . " (To): " . $changes_data["new_data"]["zip"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["state"]) ? "State (From): " . $changes_data["old_data"]["state"] . " (To): " . $changes_data["new_data"]["state"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["address1"]) ? "Address 1 (From): " . $changes_data["old_data"]["address1"] . " (To): " . $changes_data["new_data"]["address1"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["address2"]) ? "Address 2 (From): " . $changes_data["old_data"]["address2"] . " (To): " . $changes_data["new_data"]["address2"] . "\n" : "");

                    $return .= (isset($changes_data["old_data"]["bank_name"]) ? "Bank Name (From): " . $changes_data["old_data"]["bank_name"] . " (To): " . $changes_data["new_data"]["bank_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_branch_name"]) ? "Bank Branch Name (From): " . $changes_data["old_data"]["bank_branch_name"] . " (To): " . $changes_data["new_data"]["bank_branch_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_acc_no"]) ? "Account No (From): " . $changes_data["old_data"]["bank_acc_no"] . " (To): " . $changes_data["new_data"]["bank_acc_no"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_sorting_code"]) ? "Bank Sorting Code (From): " . $changes_data["old_data"]["bank_sorting_code"] . " (To): " . $changes_data["new_data"]["bank_sorting_code"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_iban"]) ? "Bank IBAN Code (From): " . $changes_data["old_data"]["bank_iban"] . " (To): " . $changes_data["new_data"]["bank_iban"] . "\n" : "");

                    $return .= (isset($changes_data["old_data"]["beneficiary_name"]) ? "Beneficiary Name (From): " . $changes_data["old_data"]["beneficiary_name"] . " (To): " . $changes_data["new_data"]["beneficiary_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["beneficiary_nid"]) ? "Beneficiary IC / Passport No (From): " . $changes_data["old_data"]["beneficiary_nid"] . " (To): " . $changes_data["new_data"]["beneficiary_nid"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["beneficiary_user_relationship"]) ? "Relation With Beneficiary (From): " . $changes_data["old_data"]["beneficiary_user_relationship"] . " (To): " . $changes_data["new_data"]["beneficiary_user_relationship"] . "\n" : "");
                }
                break;
            
            case "App\Http\Controllers\Admin\UserController::editProfile":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }

                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                
                if (!isset($data["changes"])) {
                    $return .= (isset($data["data"]["username"]) ? "Username: " . $data["data"]["username"]. "\n" : "");
                    $return .= (isset($data["data"]["name"]) ? "Name: " . $data["data"]["name"]. "\n" : "");
                    $return .= (isset($data["data"]["nid"]) ? "IC / Passport No: " . $data["data"]["nid"]. "\n" : "");
                    $return .= (isset($data["data"]["dob"]) ? "Date of Birth: ".$data["data"]["dob"]. "\n" : "");
                    $return .= (isset($data["data"]["email"]) ? "Email: " . $data["data"]["email"]. "\n" : "");
                    $return .= (isset($data["data"]["mobile"]) ? "Mobile No: " . $data["data"]["mobile"]. "\n" : "");
                    $return .= (isset($data["data"]["city"]) ? "City: " . $data["data"]["city"]. "\n" : "");
                    $return .= (isset($data["data"]["zip"]) ? "Zip: " . $data["data"]["zip"]. "\n" : "");
                    $return .= (isset($data["data"]["state"]) ? "State: " . $data["data"]["state"]. "\n" : "");
                    $return .= (isset($data["data"]["address1"]) ? "Address 1: " . $data["data"]["address1"]. "\n" : "");
                    $return .= (isset($data["data"]["address2"]) ? "Address 2: " . $data["data"]["address2"]. "\n" : "");
                    $return .= (isset($data["data"]["country"]) ? "Country: " . $data["data"]["country"]. "\n" : "");
                    $return .= (isset($data["data"]["crank"]) ? "Offer Rank: " . trans("general.user.rank.title.".$data["data"]["crank"]) . "\n" : "");
                    $return .= (isset($data["data"]["dstatus"]) ? "Debt Status: " . trans("general.user.dstatus.".$data["data"]["dstatus"]) . "\n" : "");
                    $return .= (isset($data["data"]["dpercent"]) ? "Deduct Percentage: " . number_format($data["data"]["dpercent"]) . "%\n" : "");
                    $return .= (isset($data["data"]["status"]) ? "Status: " . trans("general.user.status.".$data["data"]["status"]) . "\n" : "");

                    $return .= (isset($data["data"]["bank_name"]) ? "Bank Name: " . $data["data"]["bank_name"]. "\n" : "");
                    $return .= (isset($data["data"]["bank_branch_name"]) ? "Bank Branch Name: " . $data["data"]["bank_branch_name"]. "\n" : "");
                    $return .= (isset($data["data"]["bank_acc_no"]) ? "Account No: " . $data["data"]["bank_acc_no"]. "\n" : "");
                    $return .= (isset($data["data"]["bank_sorting_code"]) ? "Bank Sorting Code: " . $data["data"]["bank_sorting_code"]. "\n" : "");
                    $return .= (isset($data["data"]["bank_iban"]) ? "Bank IBAN Code: " . $data["data"]["bank_iban"]. "\n" : "");

                    $return .= (isset($data["data"]["beneficiary_name"]) ? "Beneficiary Name: " . $data["data"]["beneficiary_name"]. "\n" : "");
                    $return .= (isset($data["data"]["beneficiary_nid"]) ? "Beneficiary IC / Passport No: " . $data["data"]["beneficiary_nid"]. "\n" : "");
                    $return .= (isset($data["data"]["beneficiary_user_relationship"]) ? "Relation With Beneficiary: " . $data["data"]["beneficiary_user_relationship"]. "\n" : "");
                }  
                else {
                    $changes_data = $data["changes"];
                    $return .= (isset($changes_data["old_data"]["username"]) ? "Username (From): " . $changes_data["old_data"]["username"] . " (To): " . $changes_data["new_data"]["username"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["name"]) ? "Name (From): " . $changes_data["old_data"]["name"] . " (To): " . $changes_data["new_data"]["name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["nid"]) ? "IC / Passport No (From): " . $changes_data["old_data"]["nid"] . " (To): " . $changes_data["new_data"]["nid"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["dob"]) ? "Date of Birth (From): " . $changes_data["old_data"]["dob"] . " (To): " . $changes_data["new_data"]["dob"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["email"]) ? "Email (From): " . $changes_data["old_data"]["email"] . " (To): " . $changes_data["new_data"]["email"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["mobile"]) ? "Mobile No (From): " . $changes_data["old_data"]["mobile"] . " (To): " . $changes_data["new_data"]["mobile"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["city"]) ? "City (From): " . $changes_data["old_data"]["city"] . " (To): " . $changes_data["new_data"]["city"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["zip"]) ? "Zip (From): " . $changes_data["old_data"]["zip"] . " (To): " . $changes_data["new_data"]["zip"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["state"]) ? "State (From): " . $changes_data["old_data"]["state"] . " (To): " . $changes_data["new_data"]["state"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["address1"]) ? "Address 1 (From): " . $changes_data["old_data"]["address1"] . " (To): " . $changes_data["new_data"]["address1"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["address2"]) ? "Address 2 (From): " . $changes_data["old_data"]["address2"] . " (To): " . $changes_data["new_data"]["address2"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["country"]) ? "Country (From): " . $changes_data["old_data"]["country"] . " (To): " . $changes_data["new_data"]["country"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["crank"]) ? "Offer Rank (From): " . trans("general.user.rank.title.".$changes_data["old_data"]["crank"]) . " (To): " . trans("general.user.rank.title.".$changes_data["new_data"]["crank"]) . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["dstatus"]) ? "Debt Status (From): " . trans("general.user.dstatus.".$changes_data["old_data"]["dstatus"]) . " (To): " . trans("general.user.dstatus.".$changes_data["new_data"]["dstatus"]) . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["dpercent"]) ? "Deduct Percentage (From): " . number_format($changes_data["old_data"]["dpercent"]) . "% (To): " . number_format($changes_data["new_data"]["dpercent"]) . "%\n" : "");
                    $return .= (isset($changes_data["old_data"]["status"]) ? "Status (From): " . trans("general.user.status.".$changes_data["old_data"]["status"]) . " (To): " . trans("general.user.status.".$changes_data["new_data"]["status"]) . "\n" : "");

                    $return .= (isset($changes_data["old_data"]["bank_name"]) ? "Bank Name (From): " . $changes_data["old_data"]["bank_name"] . " (To): " . $changes_data["new_data"]["bank_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_branch_name"]) ? "Bank Branch Name (From): " . $changes_data["old_data"]["bank_branch_name"] . " (To): " . $changes_data["new_data"]["bank_branch_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_acc_no"]) ? "Account No (From): " . $changes_data["old_data"]["bank_acc_no"] . " (To): " . $changes_data["new_data"]["bank_acc_no"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_sorting_code"]) ? "Bank Sorting Code (From): " . $changes_data["old_data"]["bank_sorting_code"] . " (To): " . $changes_data["new_data"]["bank_sorting_code"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["bank_iban"]) ? "Bank IBAN Code (From): " . $changes_data["old_data"]["bank_iban"] . " (To): " . $changes_data["new_data"]["bank_iban"] . "\n" : "");

                    $return .= (isset($changes_data["old_data"]["beneficiary_name"]) ? "Beneficiary Name (From): " . $changes_data["old_data"]["beneficiary_name"] . " (To): " . $changes_data["new_data"]["beneficiary_name"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["beneficiary_nid"]) ? "Beneficiary IC / Passport No (From): " . $changes_data["old_data"]["beneficiary_nid"] . " (To): " . $changes_data["new_data"]["beneficiary_nid"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["beneficiary_user_relationship"]) ? "Relation With Beneficiary (From): " . $changes_data["old_data"]["beneficiary_user_relationship"] . " (To): " . $changes_data["new_data"]["beneficiary_user_relationship"] . "\n" : "");
                }
                break;

            case "App\Http\Controllers\Auth\PasswordController::resetPasswordExtra":
            case "App\Http\Controllers\Admin\UserPasswordsController::update":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }
                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                $return .= $log["descr"] . "\n";
                
                if (isset($data["changes"])) {
                    $changes_data = $data["changes"];
                    $return .= (isset($changes_data["old_data"]["password_ori"]) ? "Login Password (From): " . $changes_data["old_data"]["password_ori"] . " (To): " . $changes_data["new_data"]["password_ori"] . "\n" : "");
                    $return .= (isset($changes_data["old_data"]["password2_ori"]) ? "Security Password (From): " . $changes_data["old_data"]["password2_ori"] . " (To): " . $changes_data["new_data"]["password2_ori"] . "\n" : "");
                }

                break;
            
            case "App\Http\Controllers\Admin\UserController::editSubAccountProfile":
                $data = json_decode($log["log_data"], true);

                if (isset($data["uid"])) {
                    $user_id = $data["uid"];
                    $user = Member::find($user_id);
                    $username = $user ? $user->username : "-";
                    unset($user);
                }
                else {
                    $username = "-";
                    $user_id = "-";
                }
                $return .= "User ID: ".$username." (" . $user_id . ")\n";
                
                if (!isset($data["changes"])) {
                    $return .= (isset($data["data"]["crank"]) ? "Offer Rank: " . trans("general.user.rank.title.".$data["data"]["crank"]) . "\n" : "");
                }  
                else {
                    $changes_data = $data["changes"];
                    $return .= (isset($changes_data["old_data"]["crank"]) ? "Offer Rank (From): " . trans("general.user.rank.title.".$changes_data["old_data"]["crank"]) . " (To): " . trans("general.user.rank.title.".$changes_data["new_data"]["crank"]) . "\n" : "");
                }

                break;
            
            default:
                $return .= " Unformatted - ".$log["log_data"];
        }
        return $return;
    }
}
