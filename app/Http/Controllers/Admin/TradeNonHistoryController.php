<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Interfaces\UserCode;
use App\Models\MemberNonTradeLog;
use App\Models\Member;
use App\Models\Admin;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TradeNonHistoryController extends Controller implements UserCode
{
    private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'mtl', 'label'=>trans('field.wallet_record.id'), 'value'=>''],
			'uid' => ['search'=>'=', 'table'=>'mtl', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'=', 'table'=>'u', 'label'=>trans('field.user.username'), 'value'=>''],
			'tx_type' => ['search'=>'in', 'table'=>'mtl', 'label'=>trans('field.wallet_record.type'), 
				'options'=>[
					'c' => trans('general.wallet-record.type.c'), 
					'd' => trans('general.wallet-record.type.d'),
				], 
				'value'=>[]
			],

			'itype' => ['search'=>'in', 'table'=>"mtl", 'label' => trans("field.wallet_record.itype"),
				'options'=>[
					UserCode::ADMIN => trans("system.admin"), 
					UserCode::MEMBER => trans("system.member"),
                    UserCode::SYSTEM => trans('system.system')
				], 
				'value'=>[]
			],

			'iid' => ['search'=>'or-like', 'as'=> ['a.username', 'u.username'], 'label' => trans("field.wallet_record.iid"), "value" => ""],

            'date' => [
                'type' => 'date_range',
                'label' => trans('general.search_field.field.date'),
       			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'mtl', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],
    			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'mtl', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
            ],
		];
	}

    public function index()
    {
		$view = view('admin.trade.non-trade-history');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxNonTradeHistoryDataTable()
	{
        $member_non_trade_log_tbl = new MemberNonTradeLog;
        $member_tbl = new Member;
        $admin_tbl = new Admin;

		$query = $member_non_trade_log_tbl;
		$query->setTable($query->getTable().' AS mtl');
		$query = $query->leftJoin(with($member_tbl)->getTable().' AS u', 'u.id', '=', 'mtl.uid');
        $query = $query->leftJoin(with($admin_tbl)->getTable().' AS a', 'a.id', '=', 'mtl.iid');

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
        
        // declare credit_sum & debit_sum for balance calculation
        $sum_query = $query->select([
                DB::raw('SUM(IF(mtl.qty > 0, mtl.qty, 0)) AS credit_sum'),
                DB::raw('SUM(IF(mtl.qty < 0, -mtl.qty, 0)) AS debit_sum')
            ])
            ->get();

        $credit_sum = e(number_format($sum_query[0]->credit_sum ? $sum_query[0]->credit_sum : 0));
        $debit_sum = e(number_format($sum_query[0]->debit_sum ? $sum_query[0]->debit_sum : 0));
        $balance = e(number_format(isset($sum_query[0]->credit_sum) && isset($sum_query[0]->debit_sum) ? $sum_query[0]->credit_sum - $sum_query[0]->debit_sum : 0));
        
        // result build
		$query = $query->select(
            'mtl.id',
            'mtl.created_at',
            'mtl.qty',
            'mtl.uid AS member_id',
            'u.username AS member_username',
            'mtl.remark',
            'mtl.type', 
            'mtl.tx_type',
            'mtl.itype',
            'a.username AS admin_username',
            "mtl.descr_json"
        );

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn('created_at', function ($dt) {
                return DateTimeTool::systemToOperationDateTime($dt->created_at);
            })
            ->editColumn('tx_type', function ($dt) {
                return $dt->tx_type == 'c' ? '+' : '-';
            })
            ->editColumn('qty', function ($dt) {
                return '<span class="calc">'.number_format($dt->qty).'</span>';
            })
            ->editColumn('itype', function ($dt) {
                if ($dt["itype"] == UserCode::ADMIN) {
                    $return = trans("system.admin");
                } elseif ($dt["itype"] == UserCode::MEMBER) {
                    $return = trans("system.member");
                } elseif ($dt["itype"] == UserCode::SYSTEM) {
                    $return = trans("system.system");
                } else {
                    $return = "-";
                }
                return e($return);
            })
            ->editColumn('iid', function ($dt) {
                if ($dt["itype"] == UserCode::ADMIN) {
                    $return = $dt->admin_username;
                } elseif ($dt["itype"] == UserCode::MEMBER) {
                    $return = $dt->member_username;
                } else {
                    $return = "-";
                }
                return e($return);
            })
            ->editColumn('descr', function($dt) {
                $descr = '';
                switch ($dt->type) {
                    case 10:
                        $json = json_decode($dt->descr_json, true);
                        $split_by = ($json["split_by"]?:2);
                        $descr = trans('trade.nonTradeStatements.splitDescription', [
                            'newQty' => number_format($dt->qty), 
                            'qty' => number_format($dt->qty/$split_by), 
                            "split_by" => $split_by,
                            "from_price" => $json["from_price"],
                            "to_price" => $json["to_price"],
                        ]);
                        break;
                    case 20:
                        $json = json_decode($dt->descr_json, true);
                        return trans('trade.nonTradeStatements.nonTradeDistributeDescription', [
                            'formula' => "(".number_format($json["initial_qty"]?:0)." x ".number_format($json["percent"]?:0)."%)", 
                            'day_count' => "(".number_format($json["day_count"]?:0)."/".number_format($json["day_max"]?:0).")",
                        ]);
                    default:
                        break;
                }
                
                if ($dt->remark) {
                    if (!empty($descr)) $descr .= '<br>';
                    $descr .= "<strong>Remark</strong>: {$dt->remark}";
                }

                return $descr;
            })
            ->with([
                'credit_sum' => $credit_sum,
                'debit_sum' => $debit_sum,
                'balance' => $balance
            ])
            ->rawColumns(['descr', 'qty'])
            ->make(true);
	}
}