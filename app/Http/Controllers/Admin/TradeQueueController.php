<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;

use App\Models\Country;
use App\Models\Member;
use App\Models\TradeQueue;

use App\Trades\TradeService;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TradeQueueController extends Controller
{
    public function __construct(TradeService $tradeService)
    {
        $this->tradeService = $tradeService;
    }

    private function getSearchAttributes()
	{
		return [

            'id' => ['search'=>'=', 'table'=>'u1', 'label'=>trans('field.user.id') . " (" . trans('trade.buy') . ")", 'value'=>''],
			'username' => ['search'=>'=', 'table'=>'u1', 'label'=>trans('field.user.username') . " (" . trans('trade.buy') . ")", 'value'=>''],
            'price' => ['search'=>'=', 'table'=>'tq', 'label'=>trans('trade.price'), 'value'=>''],
            'status' => ['search' => 'in', 'table' => 'tq', 'label' => trans('general.table.header.status'), 
                'options' => [
                    1 => trans("general.page.trade.request.initiated"),
                    2 => trans("general.page.trade.request.pending"),
                    3 => trans("general.page.trade.request.paid"),
                    4 => trans("general.page.trade.request.completed"),
                    5 => trans("general.page.trade.request.cancelled"),
                ],
                'value' => []
            ],

            'buy_date' => [
                'type' => 'date_range',
                'label' => trans('trade.buy_date'),
    			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'tq', 'label'=>trans('general.search_field.field.from'), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],
                'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'tq', 'label'=>trans('general.search_field.field.to'), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
            ],

		];
	}

    public function index()
    {
		$search_fields = $this->getSearchAttributes();

        return view('admin.trade.queue')->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxTradeQueueDataTable()
	{
        $tbl_trade_queue = new TradeQueue;
        $tbl_member = with(new Member)->getTable();
        $tbl_country = with(new Country)->getTable();

		$query = $tbl_trade_queue;
		$query->setTable($query->getTable().' AS tq');
        $query = $query->leftjoin($tbl_member.' AS u1', 'u1.id', '=', 'tq.buy_uid')
            ->leftjoin($tbl_member.' AS u2', 'u2.id', '=', 'tq.sell_uid');

		$search_fields = $this->getSearchAttributes();
        $query = $this->buildORMFilter($query, $search_fields);
        
        $total_matched = e(number_format($query->sum('tq.quantity')));

        // result build
		$query = $query->select(
            'tq.id',
            'tq.buy_id',
            'u1.id AS buyer_uid',
            'u1.username AS buyer_username',
            'u1.name AS buyer_name',
            'tq.sell_id',
            'u2.id AS seller_uid',
            'u2.username AS seller_username',
            'u2.name AS seller_name',
            'tq.created_at AS queue_datetime',
            'tq.quantity',
            'tq.price',
            'tq.status'
        );

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn('queue_datetime', function ($dt) {
                return DateTimeTool::systemToOperationDateTime($dt->queue_datetime);
            })
            ->editColumn('quantity', function ($dt) {
                return '<span class="add">'.number_format($dt->quantity).'</span>';
            })
            ->editColumn('price', function ($dt) {
                return number_format($dt->price, 3);
            })
            ->editColumn('status', function ($dt) {
                $return = "";
                switch ($dt->status) {
                    case 1 :
                        $return = trans("general.page.trade.request.initiated");
                        break;
                    case 2 :
                        $return = trans("general.page.trade.request.pending");
                        break;
                    case 3 :
                        $return = trans("general.page.trade.request.paid");
                        break;
                    case 4 :
                        $return = trans("general.page.trade.request.completed");
                        break;
                    case 5 :
                        $return = trans("general.page.trade.request.cancelled");
                        break;
                    default:
                }
                return $return;
            })
            ->editColumn('action', function ($dt) {
                $return = "";
                
                $cancel_link = "/admin/trade/" . $dt->id . "/cancel";
                $cancel_btn = "<a class='btn btn-xs btn-danger' href='" . $cancel_link . "'>" . trans('general.button.cancel') . "</a>";

                $confirm_link = "/admin/trade/" . $dt->id . "/confirm";
                $confirm_btn = "<a class='btn btn-xs btn-primary' href='" . $confirm_link . "'>" . trans('general.button.confirm') . "</a>";

                if ($dt->status == TradeQueue::STATUS['PENDING'] || $dt->status == TradeQueue::STATUS['PAID']) {
                    $return = $confirm_btn . $cancel_btn;
                }

                return $return;
            })
            ->with([
                'total_matched' => $total_matched
            ])
            ->rawColumns(['quantity','action'])
            ->make(true);;
	}

    public function approveBuyRequest($queue_id)
    {

        $tradeQueue = $this->tradeService->getTradeQueue($queue_id);

        // Prevent double action on completed or cancelled trade
        if ($tradeQueue->status == TradeQueue::STATUS['COMPLETE']) {
            return back()->with('error', trans('general.page.trade.error.multiple-approve'));
        }

        if ($tradeQueue->status == TradeQueue::STATUS['CANCELLED']) {
            return back()->with('error', trans('general.page.trade.error.multiple-cancel'));
        }
        // End of prevention

        try {
            DB::beginTransaction();

            $trans = $this->tradeService->executeTransaction($queue_id);

            DB::commit();

            return back()->with('success', trans('general.page.trade.success.trade'));
        }
        catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }

    public function rejectBuyRequest($queue_id)
    {
        $tradeQueue = $this->tradeService->getTradeQueue($queue_id);

        // Prevent double action on completed or cancelled trade
        if ($tradeQueue->status == TradeQueue::STATUS['COMPLETE']) {
            return back()->with('error', trans('general.page.trade.error.multiple-approve'));
        }

        if ($tradeQueue->status == TradeQueue::STATUS['CANCELLED']) {
            return back()->with('error', trans('general.page.trade.error.multiple-cancel'));
        }
        // End of preventionreturn back()->with('error', trans('general.page.trade.error.multiple-cancel'));

        try {
            DB::beginTransaction();

            $queue = $this->tradeService->voidActiveQueue($queue_id);

            if ($tradeQueue->tradeBuy->type == 100) {
                $tradeBuy = $this->tradeService->endTradeBuy($tradeQueue->buy_id);
            }

            if ($tradeQueue->tradeSell->type == 100) {
                $tradeBuy = $this->tradeService->endTradeSell($tradeQueue->sell_id);
            }

            DB::commit();

            return back()->with('success', trans('general.page.trade.success.buy-cancelled'));
        }
        catch (Exception $e) {
            DB::rollback();

            $receipt = Storage::disk('s3')->url('receipts/' . $tradeQueue->receipt);

            return back()->with('error', $e->getMessage());   
        }
    }
}