<?php

namespace App\Http\Controllers\Admin;

use App\Models\Member;
use App\Models\MemberStat;
use App\Utils\DateTimeTool;

class GenealogyController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function sponsorGenealogy()
    {
        $user_table = with(new Member)->getTable();

        $data = request()->only(["uid"]);
        if (isset($data['uid']) && request()->isMethod('get')) {

            //change username to uid
			$uid = Member::where('username', '=', $data['uid'])->first();
			$data['uid'] = $uid?$uid->id:'0';

            $niceNames = [
                'uid' => trans('field.user.username'),
            ];
            $v = validator($data, [
                'uid' => 'required|exists:'.$user_table.',id',
            ], [], $niceNames);
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            $suser = Member::where('id', $data['uid'])->first();
        }
        else {
            $suser = Member::first();
        }
        $username = ($suser ? $suser->username : null);
        $rank = ($suser ? max($suser->rank, $suser->crank) : 0);
        $created_at_op = ($suser ? DateTimeTool::systemToOperationDateTime($suser->created_at, "Y-m-d") : "-");

        $return = ["username"=>$username, "rank"=>$rank, "created_at_op-date-only"=>$created_at_op];

        return view('admin.genealogy.sponsor-genealogy', ['__suser' => $return]);
    }

    public function placementGenealogy()
    {
        $user_table = with(new Member)->getTable();

        $data = request()->only(["uid"]);
        if (isset($data['uid']) && request()->isMethod('get')) {

            //change mobile to uid
			$uid = Member::where('mobile', '=', $data['uid'])->first();
			$data['uid'] = $uid?$uid->id:'0';

            $niceNames = [
                'uid' => trans('field.user.mobile'),
            ];
            $v = validator($data, [
                'uid' => 'required|exists:'.$user_table.',id',
            ], [], $niceNames);
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            $suser = Member::where('id', $data['uid'])->first();
        }
        else {
            $suser = Member::first();
        }

        $t_level = 4;
        
        //get result
        $cur_level = [$suser->id];
        $user_id = $suser->id;
        $next_level = null;
        for ($i=0; $i<$t_level; $i++) {
            for ($j=0; $j<count($cur_level); $j++) {
                $cur_user = Member::find($cur_level[$j]);
                
                if ($cur_user) {
                    //get leg 1
                    $next1 = Member::where('up', $cur_user->id)->where('up_pos', '1')->first();
                    if ($next1) {
                        $next1_id = $next1->id;
                        $next_level[] = $next1->id;
                    }
                    else {
                        $next1_id = null;
                        $next_level[] = null;
                    }
                    
                    //get leg 2
                    $next2 = Member::where('up', $cur_user->id)->where('up_pos', '2')->first();
                    if ($next2) {
                        $next2_id= $next2->id;
                        $next_level[] = $next2->id;
                    }
                    else {
                        $next2_id = null;
                        $next_level[] = null;
                    }
                    
                    //get upline
                    $up_user = Member::where('id', $cur_user->up)->first();
                    if ($up_user) {
                        if ($up_user->primary_id>0) {
                            $up_user = Member::where('id', $up_user->primary_id)->first();
                        }
                    }
                    $up_username=$up_user?$up_user->mobile:null;

                    //get stat
                    $member_stat = MemberStat::where('uid', $cur_user->id)->first();
                    if ($member_stat) {
                        $stat_info = $member_stat->toArray();
                    }
                    else {
                        $stat_info = ['today_leg1_amt'=>0, 'today_leg2_amt'=>0, 'leg1_acc_amt'=>0, 'leg2_acc_amt'=>0, 'leg1_cf'=>0, 'leg2_cf'=>0];
                    }
                    
                    $user_list[$i+1][$j+1] = [
                        'user' => [
                            'username'=>$cur_user->username, 
                            'mobile'=>$cur_user->mobile, 
                            'upline'=>$cur_user->up, 
                            'up-username'=>$up_username, 
                            'position'=>$cur_user->up_pos, 
                            'id' => $cur_user->id, 
                            'rank' => max($cur_user->rank, $cur_user->crank)
                            ], 
                        'next' => ['1'=> $next1_id, '2'=> $next2_id],
                        'stat_info' => $stat_info,
                        ];
                }
                else {
                    $user_list[$i+1][$j+1] = null;
                    $next_level[] = null;
                    $next_level[] = null;
                }
            }
            $cur_level = $next_level;
            $next_level = null;
        }

        //dd($user_list);
        return view('admin.genealogy.placement-genealogy', ['__tree'=> ['full_width'=>850, 'node_width'=>100, 'line_width'=>2, 'leg'=>2], '__user_list'=>$user_list]);
    }
}
