<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Activation;
use App\Models\Admin;
use App\Models\Member;
use App\Models\MemberStatus;
use App\Models\Logger;
use App\Models\TransInfo;
use App\Models\Wallet;
use App\Repositories\CountryRepo;
use App\Repositories\WalletRepo;
use App\Trades\TradeService;
use App\Utils\DateTimeTool;
use App\Utils\Tool;

use Yajra\Datatables\Datatables;

class ActivationController extends Controller
{
    private $leader_usernames = ["ZSR", "LJZ", "LJB8", "WSD", "LBS668", "WLL", "WSL", "ZKX", "ZY001", "L7777777", "WML", "LXHA", "LCH99", "SMQ", "YYN888", "XH01", "ZS01", "LYF01"];
	protected $walletRepo;
	protected $countryRepo;
    protected $tradeService;

	//Identifiers
	protected $identifiers = [];

	public function __construct(WalletRepo $walletRepo, 
	CountryRepo $countryRepo, 
	TradeService $tradeService)
	{
		$this->countryRepo = $countryRepo;
		$this->walletRepo = $walletRepo;
        $this->tradeService = $tradeService;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'like', 'table'=>'act', 'label'=>trans('field.activation.id'), 'value'=>''],
			'uid' => ['search'=>'=', 'table'=>'act', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'country' => ['search'=>'raw', 'label'=>trans('field.user.country'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END) = ?",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'aid_username' => ['search'=>'like', 'table'=>'act_user', 'as'=>"username", 'label'=>trans('field.activation.aid-username'), 'value'=>''],
			'code' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.code-name'), 
				'options'=>[
					'1100' => trans('general.activation.title.1100'), 
					'2100' => trans('general.activation.title.2100'),
					'3100' => trans('general.activation.title.3100'),
				], 
				'value'=>[]
			],
			'act_type' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.act_type'), 
				'options'=>[
					'10' => trans('general.activation.act_type.10'), 
					'30' => trans('general.activation.act_type.30'),
					'70' => trans('general.activation.act_type.70'),
					'50' => trans('general.activation.act_type.50'),
					'90' => trans('general.activation.act_type.90'),
				], 
				'value'=>[]
			],
			'status' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.status'), 
				'options'=>[
					'10' => trans('general.activation.status.10'),
					'90' => trans('general.activation.status.90'),
				], 
				'value'=>['10']
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.activation.created_at')]), 'value'=>"", 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.activation.created_at')]), 'value'=>"", 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable($list_type)
	{
		$member_table = with(new Member)->getTable();
		$query = new Activation;
		$query->setTable($query->getTable().' AS act');
		$query = $query->join($member_table .' AS user', 'user.id', '=', 'act.uid');
		$query = $query->leftjoin($member_table .' AS primary_user', 'act.upid', '=', 'primary_user.id');
		$query = $query->leftJoin($member_table .' AS act_user', 'act.aid', '=', 'act_user.id');

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(act.use_cwallet) AS sum_use_cwallet"),
			DB::raw("SUM(act.get_dwallet) AS sum_get_dwallet"),
			DB::raw("SUM(act.get_swallet) AS sum_get_swallet"),
			DB::raw("SUM(act.price) AS sum_price"),
			DB::raw("SUM(act.amount) AS sum_amount"),
		])->get();
		$sum_use_cwallet = $total_query[0]["sum_use_cwallet"] ? : 0;
		$sum_get_dwallet = $total_query[0]["sum_get_dwallet"] ? : 0;
		$sum_get_swallet = $total_query[0]["sum_get_swallet"] ? : 0;
		$sum_price = $total_query[0]["sum_price"] ? : 0;
		$sum_amount = $total_query[0]["sum_amount"] ? : 0;

		$query = $query->select([
			'act.*', 
			'user.username AS user_username',
			'user.name AS user_name',
			'user.nid AS user_nid',
			'user.ref AS user_ref',
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.id ELSE user.id END AS primary_id'),
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END AS user_country'),
			'act_user.username AS act_user_username',
			'act_user.name AS act_user_name',
		]);

		$datatables = Datatables::of($query);

		$datatables = $datatables->with([
			'grand_total' => [
				'use_cwallet' => e(number_format($sum_use_cwallet, 2)),
				'get_dwallet' => e(number_format($sum_get_dwallet, 2)),
				'get_swallet' => e(number_format($sum_get_swallet, 2)),
				'price' => e(number_format($sum_price, 2)),
				'amount' => e(number_format($sum_amount, 2)),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->editColumn('id', function ($list) {
					return $list['id'];
				})
				->editColumn('uid', function ($list) {
					return $list['uid'];
				})
				->addColumn('uid-username', function ($list) {
					return $list["user_username"];
				})
				->addColumn('uid-name', function ($list) {
					return $list["user_name"];
				})
				->addColumn('uid-nid', function ($list) {
					return $list["user_nid"];
				})
				->addColumn('uid-country', function ($list) use ($country_code_to_name) {
					return isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-";
				})
				->addColumn('ref-username', function ($list) {
					$user = Member::find($list["user_ref"]);
					return $user ? $user->username : "-";
				})
				->editColumn('aid', function ($list) {
					return $list["aid"] ? $list["aid"] : "-";
				})
				->addColumn('aid-username', function ($list) {
					return $list["act_user_username"] ? $list["act_user_username"] : "-";
				})
				->addColumn('aid-name', function ($list) {
					return $list["act_user_name"] ? $list["act_user_name"] : "-";
				})
				->editColumn('code', function ($list) {
					return trans("general.activation.title.".$list["code"]);
				})
				->editColumn('price', function ($list) {
					return number_format($list["price"], 2);
				})
				->editColumn('amount', function ($list) {
					return number_format($list["amount"], 2);
				})
				->editColumn('use_cwallet', function ($list) {
					return number_format($list["use_cwallet"], 2);
				})
				->editColumn('get_dwallet', function ($list) {
					return number_format($list["get_dwallet"], 2);
				})
				->editColumn('get_swallet', function ($list) {
					return number_format($list["get_swallet"], 2);
				})
				->editColumn('act_type', function ($list) {
					return trans("general.activation.act_type.".$list["act_type"]);
				})
				->editColumn('status', function ($list) {
					return trans("general.activation.status.".$list["status"]);
				})
				->editColumn('bonus_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['bonus_at'] > 0 ? $list['bonus_at'] : null);
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return $return;
				})
				->addColumn('iid-text', function ($list) {
					$return = "-";
					if ($list["itype"] == "900") {
						$user = Admin::find($list["iid"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["itype"] == "100") {
						$user = Member::find($list["iid"]);
						$return = $user ? $user->username : "-";
					}
					return $return;
				})
				->addColumn('cwallet', function ($list) {
					$user_wallets = Wallet::find($list["primary_id"]);
					$return = $user_wallets ? number_format($user_wallets->cwallet, 4) : "-";
					return $return;
				})
				->addColumn('dwallet', function ($list) {
					$user_wallets = Wallet::find($list["primary_id"]);
					$return = $user_wallets ? number_format($user_wallets->dwallet, 4) : "-";
					return $return;
				})
				->addColumn('swallet', function ($list) {
					$user_wallets = Wallet::find($list["primary_id"]);
					$return = $user_wallets ? number_format($user_wallets->swallet, 4) : "-";
					return $return;
				})
				->addColumn('action', function ($list) use ($list_type) {
					$return = "";
					
					if ($list_type == "cancel-list") {
						if ($this->user->can('admin-privilege', "activations/cancel")) {
							$user = Member::find($list['uid']);	
							$return = '<button class="btn btn-xs btn-danger" data-toggle="modal" data-target=".bs-cancel-activation"
									data-id="'.e($list['id']).'" 
									data-package-name="'.e(trans('general.activation.title.'.$list['code'])) .'"
									data-act-type="'.e($list["act_type"]) .'"
									data-loan-deduct="'.e(number_format($list["loan_deduct"], 4)) .'"
									data-user-id="'.e($list['uid']) .' ('. e($user->username) .')"
									><i class="fa fa-trash-o"></i> '. e(trans("general.button.cancel")) .'</button>';

							$v = $this->cancelValidation($list['id']);
							if ($v->fails()) {
								$errors = $v->errors()->all();
								$return = '<ul style="padding-left:10px;">';
								foreach ($errors as $msg) {
									$return .= "<li>".e($msg)."</li>";
								}
								$return .= '</ul>';
							}
						}

						if ($list["b_status"] == Activation::$status_to_code["confirmed"] && 
						$list["status"] == Activation::$b_status_to_code["confirmed"] && 
						(DateTimeTool::systemToOperationDateTime($list["bonus_at"]) < DateTimeTool::operationDateTime("Y-m-d") || 
						$list["act_type"] == Activation::$act_type_to_code["contra"] ) && 
						$this->user->can('admin-privilege', "is-master")) {
							$return .= '<button class="btn btn-xs btn-danger" data-toggle="modal" data-target=".bs-cancel-confirmed-activation"
									data-id="'.e($list['id']).'" 
									data-package-name="'.e(trans('general.activation.title.'.$list['code'])) .'"
									data-act-type="'.e($list["act_type"]) .'"
									data-user-id="'.e($list['uid']) .' ('. e($user->username) .')"
									><i class="fa fa-trash-o"></i> Cancel Confirmed Sales</button>';
						}
					}
					return $return;
				})
				->rawColumns(['action'])
            ->make(true);
	}

    public function index($list_type)
    {
		//twist
		switch ($list_type) {
			case "cancel-list":
				$view = view('admin.activation.cancel-list');
				break;
			default:
				$view = view('admin.activation.list');
		}

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }

	public function cancel($id)
	{
		$v = $this->cancelValidation($id);
		if ($v->fails())
			return back()->withInput()->withErrors($v);

		with(new Member)->cancelActivation($id, ["guard"=>"admins"] + request()->only("credit_back"));
		return back()->with(["success"=>"Sales #".$id." has been cancelled."]);
	}

	public function cancelConfirmed($id)
	{
		try {
			with(new Member)->cancelConfirmedActivation($id);
			return back()->with(["success"=>"Sales #".$id." has been cancelled."]);
		} catch (Exception $e) {
			
			return back()->with(["errors"=>collect(["all"=>"There is error during process, please try again later."])]);
		}
	}

	public function cancelValidation($id)
	{
		$activation = Activation::findOrFail($id);
		if ($activation->aid) {
			$data["aid"] = $activation->aid;
		}
		$data["uid"] = $activation->uid;
		$activatee_primary = $activatee = Member::find($activation->uid);
		$user_table = $activatee->getTable();
		$datetime = DateTimeTool::systemDateTime();
		
		$niceNames = [
			"aid" => trans("field.activation.aid"),
			"uid" => trans("field.user.id"),
		];
		$v = validator($data, [
					'aid' => 'exists:'.$user_table.',id,primary_acc,'.Member::$primary_to_code["yes"],
					'uid' => 'required|exists:'.$user_table.',id',
				], [], $niceNames);

		if ($v->fails())
			return $v;
		
		// custom validation part 2
		$error_list = [];

		// check status
		if ($activation->status == Activation::$status_to_code["cancelled"]) {
			$error_list[] = ["id", "Activation has been cancelled."];
		}

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		if ($v->fails())
			return $v;

		// only today sales can be cancel
		if (DateTimeTool::systemToOperationDateTime($activation->created_at, "Y-m-d") != DateTimeTool::systemToOperationDateTime($datetime, "Y-m-d")) {
			if ($activation->act_type != Activation::$act_type_to_code["contra"] ||
			!$this->user->can('admin-privilege', "is-master")) {
				$error_list[] = ["id", "Activation is not created within today."];
			}
		}

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		if ($v->fails())
			return $v;

		// no upgrade based activaton allow
		if ($upgrade_activation = Activation::where([["b_aid", $activation->id], ["status", "!=", Activation::$status_to_code["cancelled"]]])->first()) {
			$error_list[] = ["id", "Activation #".$upgrade_activation->id." is still active."];
		}

		// get target user
		if ($activatee_primary->primary_acc == Member::$primary_to_code["no"]) {
			$activatee_primary = Member::find($activatee_primary->primary_id);
		}

		if (!$activatee || !$activatee_primary) {
			$error_list[] = ["uid", trans("validation.exists", ["attribute"=>$niceNames["uid"]])];
		}

		if ($activation->loan_status == Activation::$loan_status_to_code["confirmed"] && 
			in_array($activation->act_type, [Activation::$act_type_to_code["contra"], Activation::$act_type_to_code["special"]])) {
			$error_list[] = ["id", "Activation has settled."];
		}

		//check wallet
		$get_wallet = $activation->getGetWallets();
		$activatee_primary_wallets = $this->walletRepo->getWalletsByUserId($activatee_primary->id);
		$debt_wallet = "dwallet";
		
		// if contra account select distribute later, only debt wallet distribute
		if ($activation->act_type == Activation::$act_type_to_code["contra"] && 
			$activation->distribute_after_loan == Activation::$distribute_after_loan_to_code["yes"] &&
			$activation->loan_status == Activation::$loan_status_to_code["pending"]) {
			$get_wallet = [$debt_wallet => $get_wallet[$debt_wallet]];
		}

		if ($activation->act_type == Activation::$act_type_to_code["contra"]) {
			$get_wallet[$debt_wallet] -= $activation->loan_deduct;
		}

		foreach ($get_wallet as $_wallet=>$_amount) {
			if ($activatee_primary_wallets->$_wallet < $_amount) {
				$error_list[] = ["uid", trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$_wallet), 'required' => number_format($_amount, 2), 'balance' => number_format($activatee_primary_wallets->$_wallet, 4)])];
			}
		}

		$v->after(function($validator) use ($error_list) {
			foreach ($error_list as $key=>$value) {
				$validator->errors()->add($value[0], $value[1]); 
			}
		});

		return $v;
	}

    public function addLoanActivation()
    {
		$_method =  __METHOD__;

		$package_details = array_intersect_key(Activation::$package_code_to_pack, array_flip(Activation::$normal_package_code_list));
		
		//set needed fields
		$fields['code']['options'] = $package_details;
		$fields['dpercent']['options'] = [10=>'10%', 20=>'20%', 30=>'30%', 40=>'40%', 50=>'50%', 60=>'60%', 70=>'70%', 80=>'80%', 90=>'90%', 100=>'100%'];
		$fields['share_convert']['options'] = array_values(Activation::$share_convert_to_code);
		$fields['distribute_after_loan']['options'] = array_values(Activation::$distribute_after_loan_to_code);
		$fields['distribute_after_loan2']['options'] = array_values(Activation::$distribute_after_loan2_to_code);

        if (request()->isMethod('post')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			
			//get user id
			$to_user = Member::where('username', '=', $data['uid'])->first();
			$data['uid'] = $to_user?$to_user->id:'0';

			//validation
			$niceNames = [
				'uid' => trans('field.user.username'),
				// 'topup' => 'Top Up Package By Differences',
				'code' => trans('field.activation.code-name'),
				'dpercent' => trans('field.user.dpercent'),
				'share_convert' => trans('field.activation.share_convert'),
				'share_price' => trans('field.activation.share_price'),
				'distribute_after_loan' => trans('field.activation.distribute_after_loan'),
				'distribute_after_loan2' => trans('field.activation.distribute_after_loan2'),
				'aremarks' => trans('field.activation.aremarks'),
			];
			$v = validator($data, [
				'uid' => 'required|exists:'.$user_table.',id',
				// 'topup' => 'required|in:0,1',
				'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'] )),
				'distribute_after_loan' => 'required|in:'.implode(',', $fields['distribute_after_loan']['options']),
				'distribute_after_loan2' => 'required|in:'.implode(',', $fields['distribute_after_loan2']['options']),
				'share_convert' => 'required|in:'.implode(',', $fields['share_convert']['options']),
				'share_price' => 'min:0.001|numeric',
				'dpercent' => 'required|in:'.implode(',', array_keys($fields['dpercent']['options'])),
				'aremarks' => 'string|max:2000',
			], [], $niceNames);
			
			if ($v->fails())
				return back()->withInput()->withErrors($v);
			
			// check other package
			$other_package_found = Activation::where("uid", $to_user->id)
				->where("act_type", "!=", Activation::$act_type_to_code["contra"])
				->where("status", Activation::$status_to_code["confirmed"])
				->select("act_type")
				->exists();
			if ($other_package_found) {
				return back()->withInput()->withErrors(collect(["User already had others package."]));
			}

			// check settled package
			$settle_package_found = Activation::where("uid", $to_user->id)
				->where("act_type", Activation::$act_type_to_code["contra"])
				->where("status", Activation::$status_to_code["confirmed"])
				->where("loan_status", Activation::$loan_status_to_code["confirmed"])
				->select("act_type")
				->exists();
			if ($settle_package_found) {
				return back()->withInput()->withErrors(collect(["User already had settled package."]));
			}
			
			//check if top up, only larger package is selectable
			$topup_setup = [];
			if (true) { // if($data['topup'] == 1){
				$highest_package = Activation::getHighestPackageByUser($to_user->id, 'all');

				if ($highest_package) {
					$fields['code']['options'] = Activation::getUpgradablePackageByUser($to_user->id, $highest_package->code);
					$topup_setup = ['topup_activation_id'=>$highest_package->id, 'topup'=>true];

					$v = validator($data, [
						'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'] )),
					], [], $niceNames);

					if ($v->fails())
						return back()->withInput()->withErrors($v);
				}
			}
			
			// add activation
			$user = Member::find($data['uid']);
			$result = $user->addActivation($data['uid'], $data['code'], 0, ['act_type'=>"contra", 'aremarks'=>$data['aremarks'], 'share_convert'=>$data['share_convert'], 'share_price'=>$data['share_price'], 'distribute_after_loan'=>$data['distribute_after_loan'], 'distribute_after_loan2'=>$data['distribute_after_loan2']] + $topup_setup);

			$user = Member::find($data['uid']);
		
			if ($result['status']) {
				//get primary account
				if ($user->primary_acc == Member::$primary_to_code['yes']) {
					$primary_user = $user;
				}
				else {
					$primary_user = Member::findOrFail($user->primary_id);
				}
				$primary_user->dstatus = Member::$dstatus_to_code['in-debt'];
				$primary_user->dpercent= $data['dpercent'];
				
				if ($primary_user->save()) {
					//Log Action
					$this->setIdentifiers($primary_user->id);
					$this->setIdentifiers($result['activation']->id);
					$log_msg = "User ID #".$primary_user->id." change to in-debt with deduct percent (".$data['dpercent']."%)";

					Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['dpercent'=>$data['dpercent'], 'dstatus'=>Member::$dstatus_to_code['in-debt']], 'uid'=>$primary_user->id]);
				}
			}

			return back()->withSuccess('Loan Activation added.');
        }
        return view('admin.activation.add-loan-activation', ['__fields'=>$fields]);
    }

    public function addBVFreeActivation()
    {
		$package_details = array_intersect_key(Activation::$package_code_to_pack, array_flip(Activation::$normal_package_code_list));

		foreach ($package_details as $key=>$pack) {
			$package_details[$key]["amount"] = 0;
		}

		//set needed fields
		$fields['code']['options'] = $package_details;
		$fields['share_convert']['options'] = array_values(Activation::$share_convert_to_code);

        if (request()->isMethod('post')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			$package_codes = array_keys($package_details);
			
			//get user id
			$to_user = Member::where('username', '=', $data['uid'])->first();
			$data['uid'] = $to_user?$to_user->id:'0';

			//validation
			$niceNames = [
				'uid' => trans('field.user.username'),
				// 'topup' => 'Top Up Package By Differences',
				'code' => trans('field.activation.code-name'),
				'share_convert' => trans('field.activation.share_convert'),
				'share_price' => trans('field.activation.share_price'),
				'aremarks' => trans('field.activation.aremarks'),
			];
			$v = validator($data, [
				'uid' => 'required|exists:'.$user_table.',id',
				// 'topup' => 'required|in:0,1',
				'code' => 'required|in:'.implode(',', $package_codes),
				'share_convert' => 'required|in:'.implode(',', $fields['share_convert']['options']),
				'share_price' => 'min:0.001|numeric',
				'aremarks' => 'string|max:2000',
			], [], $niceNames);

			if ($v->fails())
				return back()->withInput()->withErrors($v);

			// check non settled package
			$non_settle_package_found = Activation::where("uid", $to_user->id)
				->whereIn("act_type", [Activation::$act_type_to_code["contra"], Activation::$act_type_to_code["special"]])
				->where("status", Activation::$status_to_code["confirmed"])
				->where("loan_status", Activation::$loan_status_to_code["pending"])
				->select("act_type")
				->exists();
			if ($non_settle_package_found) {
				return back()->withInput()->withErrors(collect(["User already had non-settled package."]));
			}
			
			//check if top up, only larger package is selectable
			$topup_setup = [];
			if (true) { // if($data['topup'] == 1){
				$highest_package = Activation::getHighestPackageByUser($to_user->id, 'all');

				if ($highest_package) {
					$fields['code']['options'] = Activation::getUpgradablePackageByUser($to_user->id, $highest_package->code);
					$topup_setup = ['topup_activation_id'=>$highest_package->id, 'topup'=>true];

					$v = validator($data, [
						'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'] )),
					], [], $niceNames);

					if ($v->fails())
						return back()->withInput()->withErrors($v);
				}
			}
			
			//wallet adjustment
			$user = Member::find($data['uid']);
			$user->addActivation($data['uid'], $data['code'], 0, ['act_type'=>"bvfree", 'aremarks'=>$data['aremarks'], 'share_convert'=>$data['share_convert'], 'share_price'=>$data['share_price']] + $topup_setup);

			return back()->withSuccess('Zero BV Activation added.');
        }
        return view('admin.activation.add-bvfree-activation', ['__fields'=>$fields]);
    }

    public function addSpecialActivation()
    {
		$package_details = array_intersect_key(Activation::$package_code_to_pack, array_flip(Activation::$normal_package_code_list));

		foreach ($package_details as $key=>$pack) {
			$package_details[$key]["amount"] = 0;
		}

		//set needed fields	
		$fields['code']['options'] = $package_details;
		$fields['share_convert']['options'] = array_values(Activation::$share_convert_to_code);

        if (request()->isMethod('post')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			$package_codes = array_keys($package_details);
			
			//get user id
			$to_user = Member::where('username', '=', $data['uid'])->first();
			$data['uid'] = $to_user?$to_user->id:'0';

			$niceNames = [
				'uid' => trans('field.user.username'),
				'code' => trans('field.activation.code-name'),
				'share_convert' => trans('field.activation.share_convert'),
				'share_price' => trans('field.activation.share_price'),
				'aremarks' => trans('field.activation.aremarks'),
			];
			$v = validator($data, [
				'uid' => 'required|exists:'.$user_table.',id',
				'code' => 'required|in:'.implode(',', $package_codes),
				'share_convert' => 'required|in:'.implode(',', $fields['share_convert']['options']),
				'share_price' => 'required_if:share_convert,'.Activation::$share_convert_to_code['yes'].'|numeric|between:'.config('trade.min_limit').','.config('trade.max_limit'),
				'aremarks' => 'string|max:2000',
			], [], $niceNames);

			if ($v->fails())
				return back()->withInput()->withErrors($v);
			
			// check other package
			$other_package_found = Activation::where("uid", $to_user->id)
				->where("act_type", "!=", Activation::$act_type_to_code["special"])
				->where("status", Activation::$status_to_code["confirmed"])
				->select("act_type")
				->exists();
			if ($other_package_found) {
				return back()->withInput()->withErrors(collect(["User already had others package."]));
			}
			
			// check settled package
			$settle_package_found = Activation::where("uid", $to_user->id)
				->where("act_type", Activation::$act_type_to_code["special"])
				->where("status", Activation::$status_to_code["confirmed"])
				->where("loan_status", Activation::$loan_status_to_code["confirmed"])
				->select("act_type")
				->exists();
			if ($settle_package_found) {
				return back()->withInput()->withErrors(collect(["User already had settled package."]));
			}

			//check if top up, only larger package is selectable
			$topup_setup = [];
			if (true) { // if($data['topup'] == 1){
				$highest_package = Activation::getHighestPackageByUser($to_user->id, 'all');

				if ($highest_package) {
					$fields['code']['options'] = Activation::getUpgradablePackageByUser($to_user->id, $highest_package->code);
					$topup_setup = ['topup_activation_id'=>$highest_package->id, 'topup'=>true];

					$v = validator($data, [
						'code' => 'required|in:'.implode(',', array_keys($fields['code']['options'] )),
					], [], $niceNames);

					if ($v->fails())
						return back()->withInput()->withErrors($v);
				}
			}
			
			// add activation
			$user = Member::find($data['uid']);
			$user_wallet = $this->walletRepo->getWalletsByUserId($user->id);

			$share_price = 0;
			if (Activation::$share_convert_to_code['yes'] == $data['share_convert']) {
				$get_wallet1 = $package_details[$data['code']]['get_wallet']['wallet1'];
				$share_price = round($data['share_price'], config('trade.decimal_point'));
			}

			$result = $user->addActivation($data['uid'], $data['code'], 0, ['act_type'=>"special", 'aremarks'=>$data['aremarks'], 'share_convert'=>$data['share_convert'], 'share_price'=>$share_price] + $topup_setup);

			if ($result['status']) {
				//get primary account
				if ($user->primary_acc == Member::$primary_to_code['yes']) {
					$primary_user = $user;
				}
				else {
					$primary_user = Member::findOrFail($user->primary_id);
				}

				$member_status = MemberStatus::find($primary_user->id);
				if (!$member_status) {
					MemberStatus::create([
						"uid" => $primary_user->id
					]);
					$member_status = MemberStatus::find($primary_user->id);
				}
				$member_status->sa_status = 10;
				
				if ($member_status->save()) {
					//Log Action
					$this->setIdentifiers($primary_user->id);
					$this->setIdentifiers($result['activation']->id);
					$log_msg = "User ID #".$primary_user->id." change to Special Account.";

					Logger::logWithoutGuard(__METHOD__, $this->getIdentifiers(), $log_msg, ['data'=>['sa_status' => 10], 'uid'=>$primary_user->id]);
				}
			}

			return back()->withSuccess('Special Package added.');
        }
        return view('admin.activation.add-special-activation', ['__fields'=>$fields]);
    }
}
