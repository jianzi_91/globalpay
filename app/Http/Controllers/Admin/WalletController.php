<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\MemberWalletRecord;
use App\Models\Member;
use App\Models\Admin;
use App\Models\TransInfo;
use App\Models\SessionKeyValue;
use App\Models\Wallet;

use App\Repositories\MemberRepo;

use App\Trades\TradeService;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class WalletController extends Controller
{
	public function __construct(MemberRepo $memberRepo)
	{
		parent::__construct();

		$this->memberRepo = $memberRepo;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'like', 'table'=>'user_wr', 'label'=>trans('field.wallet_record.id'), 'value'=>''],
			'uid' => ['search'=>'=', 'table'=>'user_wr', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'wallet' => ['search'=>'in', 'table'=>'user_wr', 'label'=>trans('field.wallet_record.wallet'),
				'options'=>[
					'awallet2' => trans('general.wallet.awallet2'), 
					'awallet' => trans('general.wallet.awallet'),   
					'fwallet' => trans('general.wallet.fwallet'),
					'swallet' => trans('general.wallet.swallet'),  
					'dowallet' => trans('general.wallet.dowallet'),  
				], 
				'value'=>[]
			],
			'trans_code' => ['search'=>'in', 'table'=>'user_wr', 'label'=>trans('field.wallet_record.trans_code'), 
				'options'=>[
					'1001' => trans('general.wallet-record.trans_code.1001'), 
					'1002' => trans('general.wallet-record.trans_code.1002'),
					'1004' => trans('general.wallet-record.trans_code.1004'),
					'1005' => trans('general.wallet-record.trans_code.1005'),
					'1008' => trans('general.wallet-record.trans_code.1008'),
				], 
				'value'=>[]
			],
			'type' => ['search'=>'in', 'table'=>'user_wr', 'label'=>trans('field.wallet_record.type'), 
				'options'=>[
					'c' => trans('general.wallet-record.type.c'), 
					'd' => trans('general.wallet-record.type.d'),
				], 
				'value'=>[]
			],
			'company_status' => ['search'=>'in', 'table'=>"user", 'label'=>trans("field.user.company_status"),
				'options'=>[
					'1' => trans("general.user.company_status.1"), 
					'0' => trans("general.user.company_status.0"), 
				], 
				'value'=>[]
			],
			'itype' => ['search'=>'in', 'table'=>"user_wr", 'label' => trans("field.wallet_record.itype"),
				'options'=>[
					'900' => trans("system.admin"), 
					'100' => trans("system.member"), 
				], 
				'value'=>[]
			],
			'iid' => ['search'=>'ignore', 'label' => trans("field.wallet_record.iid"), "value" => ""],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'user_wr', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'user_wr', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new MemberWalletRecord;
		$query->setTable($query->getTable().' AS user_wr');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'user_wr.uid');
		$query = $query->leftJoin(with(new Member)->getTable().' AS iid_user', function($join) {
			$join->on('user_wr.iid', '=', 'iid_user.id');
			$join->on('user_wr.itype', '=', DB::raw("100"));
		});
		$query = $query->leftJoin(with(new Admin)->getTable().' AS iid_admin', function($join) {
			$join->on('user_wr.iid', '=', 'iid_admin.id');
			$join->on('user_wr.itype', '=', DB::raw("900"));
		});
		
		if ($value = request()->get("iid")) {
			$query = $query->where(function($query) use ($value) {
				$query->where('iid_user.username', 'like', "%".$value."%")
					->orWhere('iid_admin.name', 'like', "%".$value."%");
			});
		}

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(IF(user_wr.type='c', user_wr.amount, -user_wr.amount)) AS sum_amount"),
		])->get();
		$sum_amount = $total_query[0]["sum_amount"] ? : 0;

		// result build
		$query = $query->select([
			'user_wr.*',
			'user.username AS user_username',
			'user.name AS user_name',
			'user.mobile AS user_mobile',
		]);

		$datatables = Datatables::of($query);
		
		$datatables = $datatables->with([
			'grand_total' => [
				'amount' => e(number_format($sum_amount, 4)),
			]
		]);
    	
		return $datatables
				->editColumn('user_username', function ($list) {
					return $list->user_username;
				})
				->editColumn('user_name', function ($list) {
					return $list->user_name;
				})
				->editColumn('user_mobile', function ($list) {
					return $list->user_mobile;
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return $return;
				})
				->addColumn('iid-text', function ($list) {
					$return = "-";
					if ($list["itype"] == "900") {
						$user = Admin::find($list["iid"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["itype"] == "100") {
						$user = Member::find($list["iid"]);
						$return = $user ? $user->username : "-";
					}
					return $return;
				})
				->addColumn('type', function ($list) {
					return $list['type']=='c'?'+':'-';
				})
				->editColumn('uid', function ($list) {
					return $list["uid"];
				})
				->editColumn('id', function ($list) {
					return $list["id"];
				})
				->editColumn('wallet', function ($list) {
					return trans("general.wallet.".$list["wallet"]);
				})
				->editColumn('balance', function ($list) {
					return number_format($list["balance"], 4);
				})
				->editColumn('price', function ($list) {
					return $list->price ? number_format($list->price, 4) : null;
				})
				->editColumn('amount', function ($list) {
					return number_format($list["amount"], 4);
				})
				->editColumn('descr-text', function ($list) {
					return nl2br(e(TransInfo::getDescrFromCode($list, "admins")));
				})
				->editColumn('trans_code', function ($list) {
					return trans("general.wallet-record.trans_code.".$list["trans_code"]);
				})
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->rawColumns(['descr-text'])
            ->make(true);
	}
	
    public function index()
    {
		$view = view('admin.wallet.list');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function edit()
    {
		$user = $this->user;
        //action only valid once
		$session_key = new SessionKeyValue();
		$pre_nonce =  $session_key->getValue('admin_edit_wallet_nonce');
        $nonce = str_random(60);
        $session_key->putValue('admin_edit_wallet_nonce', $nonce);

		//options data and default
		$fields = [];
		$fields["wallet"]["options"] = [];
		$fields["type"]["options"] = [];
		$fields['__nonce']['value'] = $nonce;

		// wallet control
		if ($this->user->can("admin-privilege", "wallets/edit")) {
			$fields["wallet"]["options"] = Member::$wallet;
			$fields["type"]["options"] = ["c", "d"];
		}

        if (request()->isMethod('post') && request()->has('__req')
        && request()->get('__nonce') === $pre_nonce && $pre_nonce) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			
			//get user id
			$member = $uid = Member::where('mobile', '=', $data['uid'])->first();
			$sub_user = null;
        
			if (!$member) {

				list($mobile, $sub_id) = explode('-',$data['uid']);

				$sub_user = Member::where('mobile',$mobile)
					->where('sub_id', $sub_id)
					->first();

				$member = $uid = $sub_user;
				
			}
			$data['uid'] = $uid?$uid->id:'0';

			$niceNames = [
				'uid' => trans('field.user.mobile'),
				'amount' => trans('field.wallet_record.amount'),
				'wallet' => trans('field.wallet_record.wallet'),
				'type' => trans('field.wallet_record.type'),
				'price' => trans('field.wallet_record.price'),
				'remarks' => trans('field.wallet_record.remarks'),
				'aremarks' => trans('field.wallet_record.aremarks'),
			];
			$v = validator($data, [
				'uid' => 'required|exists:'.$user_table.',id,primary_id,0',
				'wallet' => 'required|in:'.implode(',', $fields["wallet"]["options"]),
				'amount' => 'required|min:0.0001|numeric',
				'type' => 'required|in:'.implode(",", $fields["type"]["options"]),
				'price' => 'nullable|numeric|min:0',
				'remarks' => 'string|max:2000',
				'aremarks' => 'string|max:2000',
			], [], $niceNames);

			if ($v->fails())
				return back()->withInput()->withErrors($v);
			
			$v = validator([], []);
			// custom validation
			$v->after(function($validator) use ($niceNames, $member, $data) {
				
				if ($data['type'] === 'd') {
					$user_wallets = $member->wallet;

					//validation swallet only for wine user
					if ($data['wallet'] == "swallet" && $member->user_type != 3) {
						$validator->errors()->add('wallet', trans('validation.swallet_transfer_for_wine_user_only'));
					}

					// check wallet
					if ($data['wallet'] == "cwallet") {
						$trade_service = app()->make(TradeService::class);
	
						// cwallet got pending sell for locking
						$locked_amount = $trade_service->getMemberTotalSellingCoins($member->id);
						if ((double) $data['amount'] && (double) $data['amount'] > (double) ($user_wallets->{$data['wallet']} - $locked_amount) ) {
							$validator->errors()->add('wallet', trans('validation.wallet-insufficient-locked', [
								'wallet' => trans('general.wallet.'.$data['wallet']), 
								'required' => number_format($data['amount'], 2), 
								'balance' => number_format($user_wallets->{$data['wallet']} - $locked_amount, 4),
								'locked' => number_format($locked_amount, 4),
							]));
						}
					}
					else {
						// check wallet
						if ((double) $user_wallets->{$data['wallet']} < (double) $data['amount'] && $data['wallet'] != Wallet::WALLET_DO) {
							$validator->errors()->add('wallet', trans('validation.wallet-insufficient', ['wallet' => trans('general.wallet.'.$data['wallet']), 'required' => number_format($data['amount'], 2), 'balance' => number_format($user_wallets->{$data['wallet']}, 6)]));
						}
					}
				}
				else {
					//validation swallet only for wine user
					if ($data['wallet'] == "swallet" && $member->user_type != 3) {
						$validator->errors()->add('wallet', trans('validation.swallet_transfer_for_wine_user_only'));
					}

					if ($data["uid"] != Member::$wallet_uid) {
						$user_wallets = Member::find(Member::$wallet_uid)->wallet;
						
						// check wallet
						// if ((double) $user_wallets->rwallet < (double) $data['amount']) {
						// 	$validator->errors()->add('wallet', "System does not have the balance.");
						// }
					}
				}
			});
			if ($v->fails())
				return back()->withInput()->withErrors($v);
			// dd($data['wallet']);
			try {
				DB::beginTransaction();
				$descr_json = [
					'wallet' => $data['wallet'],
					'amount' => $data['amount'],
					'uid' => $data['uid'],
					'username' => $member->username,
					'remarks' => $data['remarks'],
					'aremarks' => $data['aremarks'],
				];

				if ($data['type'] === 'c') {

					$arr_input = [
						'uid' => $member->id,
						'trans_code' => Wallet::WALLET_EDIT_ADMIN_ADD,
						'creator_user_type' => $user::$user_code,
						'creator_uid' => $user->id,
						'remarks' => $data['remarks'],
						'admin_remarks' => $data['aremarks'],
						'type' => 'edit-wallet-admin-add',
					];

					switch ($data['wallet']) {
						case Wallet::WALLET_A:
							$arr_input = array_add($arr_input, 'a_amt', $data['amount']);
							break;
						case Wallet::WALLET_F:
							$arr_input = array_add($arr_input, 'f_amt', $data['amount']);
							break;
						case Wallet::WALLET_DO:
							$arr_input = array_add($arr_input, 'do_amt', $data['amount']);
							break;
						case Wallet::WALLET_S:
							$arr_input = array_add($arr_input, 's_amt', $data['amount']);
							break;
					}

					Wallet::adjust($arr_input);

					/* replace by new workaround
					$member->addWallet(
						$data['uid'], 
						$data['wallet'], 
						$data['amount'], 
						'Wallet Top Up', 
						$descr_json, 
						TransInfo::$trans_type_to_code['wallet-topup'],
						false,
						8,
						["price" => $data["price"]]
					);
					*/
	
					// if ($data["uid"] != Member::$wallet_uid) {
					// 	$member->deductWallet(
					// 		Member::$wallet_uid, 
					// 		"rwallet", 
					// 		$data['amount'], 
					// 		'Wallet Top Up', 
					// 		$descr_json, 
					// 		TransInfo::$trans_type_to_code['wallet-topup'],
					// 		false,
					// 		8,
					// 		[
					// 			"tuid" => $data["uid"], 
					// 			"price" => $data["price"]
					// 		]
					// 	);
					// }
				}
				else {

					$arr_input = [
						'uid' => $member->id,
						'trans_code' => Wallet::WALLET_EDIT_ADMIN_SUBTRACT,
						'creator_user_type' => $user::$user_code,
						'creator_uid' => $user->id,
						'remarks' => $data['remarks'],
						'admin_remarks' => $data['aremarks'],
						'type' => 'edit-wallet-admin-deduct',
					];

					switch ($data['wallet']) {
						case Wallet::WALLET_A:
							$arr_input = array_add($arr_input, 'a_amt', -$data['amount']);
							break;
						case Wallet::WALLET_F:
							$arr_input = array_add($arr_input, 'f_amt', -$data['amount']);
							break;
						case Wallet::WALLET_DO:
							$arr_input = array_add($arr_input, 'do_amt', -$data['amount']);
							break;
						case Wallet::WALLET_S:
							$arr_input = array_add($arr_input, 's_amt', -$data['amount']);
							break;
					}

					Wallet::adjust($arr_input);
/*
					if ($data["uid"] != Member::$wallet_uid) {
						$member->addWallet(
							Member::$wallet_uid, 
							"rwallet", 
							$data['amount'], 
							'Wallet Deduct', 
							$descr_json, 
							TransInfo::$trans_type_to_code['wallet-deduct'],
							false,
							8,
							[
								"tuid" => $data["uid"], 
								"price" => $data["price"]
							]
						);
					}
*/
				}

			} catch (Exception $e) {
				DB::rollBack();
				throw $e;
			}
			DB::commit();

			return back()->withSuccess(trans('general.page.admin.wallet-edit.success'));
        }
        return view('admin.wallet.edit', ['__fields'=>$fields]);
	}
	
	// public function releaseInitialRP()
    // {
	// 	$members = Member::where('package', '!=', null)
	// 		->where('status', 10)
	// 		->get();

    //     foreach($members as $member) {

	// 		try {
	// 			DB::beginTransaction();

	// 			$rp_release = $member->memberPackage->price * 0.6;

	// 			$descr = "User ID #" . $member->id . " received RP (" . $rp_release . ") for registration with package " . $member->package;

	// 			$descr_array = [
	// 				'uid' => $member->id,
	// 				'rwallet' => $rp_release,
	// 				'package' => $member->package,
	// 			];

	// 			$member->walletUpdate($member->id,'rwallet',$rp_release,'c', 2, $descr, $descr_array, "1008");
				
	// 			DB::commit();
	// 		}
	// 		catch (Exception $e) {

	// 			DB::rollback();
				
	// 		}
			
	// 	}
	// 	return back()->with('success', 'Success');
    // }
}
