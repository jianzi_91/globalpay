<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Country;
use App\Models\Logger;
use App\Utils\Tool;
use App\Utils\DateTimeTool;
use App\Repositories\CountryRepo;

use Yajra\Datatables\Datatables;

class AdminUserController extends Controller
{
	protected $countryRepo;
	
	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'username' => ['search'=>'like', 'table'=>'adminuser', 'label'=>trans('field.admin.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'adminuser', 'label'=>trans('field.admin.name'), 'value'=>''],
			'status' => ['search'=>'in', 'table'=>'adminuser', 'label'=>trans('field.admin.status'), 
				'options'=>[
					'10' => trans('general.admin.status.10'),
					'30' => trans('general.admin.status.30'),
					'90' => trans('general.admin.status.90')
				], 
				'value'=>[]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Admin;
		$query->setTable($query->getTable().' AS adminuser');

		// result build
		$query = $query->select('adminuser.*');

		$search_fields = $this->getSearchAttributes();
		$datatables = $this->buildORMFilter($query, $search_fields);

		// remove developer
		$query = $query->whereRaw("IF(admin_type is null, 0, admin_type) != 2");
		
		$datatables = Datatables::of($query);
    	
		return $datatables->editColumn('status', function ($list) {
					return trans("general.admin.status.".$list["status"]);
				})
				->editColumn('username', function ($list) {
					return $list["username"];
				})
				->editColumn('name', function ($list) {
					return $list["name"];
				})
				->editColumn('email', function ($list) {
					return $list["email"];
				})
				->editColumn('action', function ($list) {
					$return = "";

					if (($list->isMaster() && $this->user->can("admin-privilege", "is-master")) || !$list->isMaster())
						$return .= '<a href="'.e(url($this->admin_slug.'/adminusers/profile', $list["id"])) .'" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i>'.e(trans('general.button.edit')).'</a>';

					return $return;
				})
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->rawColumns(['action'])
            ->make(true);
	}

    public function index()
    {
        $view = view('admin.adminuser.list');

		//search fields
		$search_fields = $this->getSearchAttributes();

		return $view->with(['__search_fields'=>$search_fields]);
    }

    public function create()
    {
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

        $admin_table = with(new Admin)->getTable();

		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();

		//options data
		$fields['privileges']['options'] = Admin::$privileges_to_code;
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();

        if(request()->isMethod('post') && request()->has('__req')) {
            $niceNames = [
                'name' => trans('field.admin.name'),
                'email' => trans('field.admin.email'),
                'username' => trans('field.admin.username'),
                'country' => trans('field.admin.country'),
                'password' => trans('field.admin.password'),
                'password_confirmation' => trans('field.admin.password'),
                'privileges' => trans('field.admin.privileges'),
			];

            $data = request()->only(array_keys($niceNames));
			$data['privileges'] = isset($data["privileges"]) ? array_intersect($data["privileges"], $fields['privileges']['options']) : [];

            $v = validator($data, [
				'username' => 'required|alpha_num|min:2|max:100|unique:'.$admin_table,
				'name' => 'required|max:255',
				'email' => 'required|email|max:255|unique:'.$admin_table,
				'country' => 'required|exists:'.$country_table.','.$country_id,
				'password' => 'required|confirmed|min:6',
			], [], $niceNames);

            if ($v->fails())
                return back()->withInput()->withErrors($v);

            $adminuser = Admin::create([
                'itype' => $initiator['itype'],
                'iid' => $initiator['iid'],
                'editor_type' => $initiator['itype'],
                'editor_id' => $initiator['iid'],
                'name' => $data['name'],
                'email' => $data['email'],
				"privileges" => json_encode($data['privileges']),
                'username' => $data['username'],
                'status' => Admin::$status_to_code['active'], //active
                'country' => $data['country'],
                'password' => bcrypt($data['password']),
            ]);

			// set token
			$adminuser->forceFill([
				'remember_token' => str_random(60),
			])->save();

            if($adminuser){
				//Log Action
				$this->setIdentifiers($adminuser->id);
				$log_msg = "Admin User Created for User ID #".$adminuser->id.".";
				Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['name'=>$data['name'], 'privileges'=>$data['privileges'], 'email'=>$data['email'], 'username'=>$data['username'], 'country'=>$data['country'], 'status'=> Admin::$status_to_code['active']], 'aid'=>$adminuser->id]);
            }

            return redirect($this->admin_slug.'/adminusers')->with('success','Admin User is created');
        }

        return view('admin.adminuser.create', ['__fields'=>$fields]);
    }

    public function edit($id)
    {
        $adminuser = Admin::whereRaw("IF(admin_type is null, 0, admin_type) != 2")
			->where("id", "=", $id)
			->firstOrFail();
			
		if ($adminuser->isMaster() && !$this->user->can("admin-privilege", "is-master")) {
            return response('Unauthorized.', 401);
		}
		
		$adminuser["privilege-codes"] = $adminuser->privileges ? json_decode($adminuser->privileges, true) : [];

		$view = view('admin.adminuser.profile');

		//options data
		$fields['privileges']['options'] = Admin::$privileges_to_code;
		$fields['status']['options'] = array_values(Admin::$status_to_code);
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();
		
		return $view->with(['__adminuser'=>$adminuser, '__fields'=>$fields]);
    }

	public function update($id)
	{
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();

		$redirect = redirect($this->admin_slug."/adminusers/profile/".$id);

        $adminuser = Admin::whereRaw("IF(admin_type is null, 0, admin_type) != 2")
			->where("id", "=", $id)
			->firstOrFail();
			
		if ($adminuser->isMaster() && !$this->user->can("admin-privilege", "is-master")) {
            return response('Unauthorized.', 401);
		}
		
        $admin_table = $adminuser->getTable();

		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();

		$fields['privileges']['options'] = Admin::$privileges_to_code;
		$fields['status']['options'] = array_values(Admin::$status_to_code);
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();

		$data = request()->all();

		if ($data['__req'] == '1') {
			//general info

			$niceNames = [
				'name' => trans('field.admin.name'),
				'email' => trans('field.admin.email'),
				'username' => trans('field.admin.username'),
				'country' => trans('field.admin.country'),
				'status' => trans('field.admin.status'),
			];

			$v = validator($data, [
				'username' => 'required|alpha_num|min:2|max:100|unique:'.$admin_table.',username,'.$adminuser->id,
				'name' => 'required|max:255',
				'email' => 'required|email|max:255|unique:'.$admin_table.',email,'.$adminuser->id,
				'country' => 'required|exists:'.$country_table.','.$country_id,
				'status' => 'required|in:'.implode(',', $fields['status']['options']),
			], [], $niceNames);
			
			if ($v->fails())
				return back()->withInput()->withErrors($v);
			
			$adminuser->editor_type = $initiator['itype'];
			$adminuser->editor_id = $initiator['iid'];
			$adminuser->name = $data['name'];
			$adminuser->email = $data['email'];
			$adminuser->username = $data['username'];
			$adminuser->country = $data['country'];
			$adminuser->status = $data['status'];
			
			if ($adminuser->save()) {
				//Log Action
				$this->setIdentifiers($id);
				$log_msg = "Admin General Info update for User ID #".$adminuser->id.".";
				Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, [
					'data'=>['name'=>$data['name'], 'email'=>$data['email'], 'username'=>$data['username'], 'country'=>$data['country'], 'status'=>$data['status']], 
					'aid'=>$id
				]);
			}

			return $redirect->withSuccess('General Info is updated.');
		}
		elseif ($data['__req'] == '2') {
			//password

			$niceNames = [
				'password' => trans('field.admin.password'),
			];

			$v = validator($data, [
				'password' => 'required|confirmed|min:6',
			], [], $niceNames);
			
			if ($v->fails())
				return back()->withInput()->withErrors($v);

			$adminuser->editor_type = $initiator['itype'];
			$adminuser->editor_id = $initiator['iid'];
			$adminuser->password = bcrypt($data['password']);
			$adminuser->save();

			return $redirect->withSuccess('Password is updated.');
		}
		elseif ($data["__req"] == "3") {
			
			$data['privileges'] = isset($data["privileges"]) ? array_intersect($data["privileges"], $fields['privileges']['options']) : [];

			$adminuser->editor_type = $initiator['itype'];
			$adminuser->editor_id = $initiator['iid'];
			$adminuser->privileges = json_encode($data['privileges']);
			
			if ($adminuser->save()) {
				//Log Action
				$this->setIdentifiers($id);
				$log_msg = "Admin Privileges update for User ID #".$adminuser->id.".";
				Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, [
					'data'=>['privileges'=>$data['privileges']], 
					'aid'=>$id
				]);
			}

			return $redirect->withSuccess('Privileges is updated.');
		}
	}

    public function profileEdit()
    {
		$_method =  __METHOD__;
		$_guard =  "admins";

		//initiator
		$initiator = Tool::getInitiator($_guard);

        $admin = $this->user;
        $admin_table = $admin->getTable();

		$country_table = with(new Country)->getTable();
		$country_id = with(new Country)->getKeyName();
		
        $view = view('admin.profile');

		//options data
		$fields['country']['options'] = $this->countryRepo->all_member_selectable_country();

        if(request()->isMethod('post') && request()->has('__req')) {
            $data = request()->all();
			
			//profile
			if($data['__req'] == '1'){
				$niceNames = array(
					'name' => trans('field.admin.name'),
					'email' => trans('field.admin.email'),
					'country' => trans('field.admin.country'),
					);
				$v = \Validator::make($data, [
										'name' => 'required|max:255',
										'email' => 'required|email|max:255|unique:'.$admin_table.',email,'.$admin->id,
						    			'country' => 'required|exists:'.$country_table.','.$country_id,
									]);
				$v->setAttributeNames($niceNames);
				if ($v->fails())
				   return back()->withInput()->withErrors($v);
				
				$admin->editor_type= $initiator['itype'];
				$admin->editor_id= $initiator['iid'];
				$admin->name= $data['name'];
				$admin->email= $data['email'];
				$admin->country= $data['country'];
				//$admin->status= $data['status'];
                
                if($admin->save()){
                    //Log Action
                    $this->setIdentifiers($admin->id);
                    $log_msg = "Admin General Info update for User ID #".$admin->id.".";
                    Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['name'=>$data['name'], 'email'=>$data['email'], 'country'=>$data['country']], 'aid'=>$admin->id]);
                }

				return $view->with(['__fields'=>$fields])->withAdmin($admin)->withSuccess('General Info updated.');
			}
			//password
			elseif($data['__req'] == '2'){
				$niceNames = array(
					'password' => trans('field.admin.password-old'),
					'new_password' =>  trans('field.admin.password'),
					);

				$v = \Validator::make($data, [
										'password' => 'required',
										'new_password' => 'required|confirmed|min:6',
									]);
				
				//custom validation
				$v->after(function($validator) use ($niceNames){
					$data = $validator->getData();
					$admin = \Auth::guard('admins')->user();
					if ($data['password'] && ! \Auth::guard('admins')->attempt(['username'=>$admin->username, 'password'=>$data['password']])) {
						$validator->errors()->add('password', trans('validation.unmatch.existing', ['attribute' => $niceNames['password']]));
					}
				});

				//$v->errors()->add('password', 'unmatch');
				//$validation->messages()
				//dd($v->messages());
				
				$v->setAttributeNames($niceNames);
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$admin->editor_type= $initiator['itype'];
				$admin->editor_id= $initiator['iid'];
				$admin->password = bcrypt($data['new_password']);
				$admin->save();
				return $view->with(['__fields'=>$fields])->withAdmin($admin)->withSuccess('Password updated.');
			}
        }
        return $view->with(['__fields'=>$fields])->withAdmin($admin);
    }
}
