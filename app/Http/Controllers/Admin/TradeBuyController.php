<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;

use App\Models\Country;
use App\Models\Member;
use App\Models\TradeBuy;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TradeBuyController extends Controller
{
    private function getSearchAttributes()
	{
		return [

            'id' => ['search'=>'=', 'table'=>'u1', 'label'=>trans('field.user.id'), 'value'=>''],
            'username' => ['search'=>'=', 'table'=>'u1', 'label'=>trans('field.user.username'), 'value'=>''],
            'status' => ['search' => 'in', 'table' => 'tb', 'label' => trans('general.table.header.status'), 
                'options' => [
                    'P' => trans('trade.status.pending'),
                    'F' => trans('trade.status.fulfilled'),
                    'X' => trans('trade.status.cancelled'),
                ],
                'value' => []
            ],

            'buy_date' => [
                'type' => 'date_range',
                'label' => trans('trade.buy_date'),
    			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'tb', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],
                'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'tb', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
            ],

		];
	}

    public function index()
    {
		$search_fields = $this->getSearchAttributes();

        return view('admin.trade.buy')->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxTradeBuyDataTable()
	{
        $tbl_trade_buy = new TradeBuy;
        $tbl_member = with(new Member)->getTable();
        $tbl_country = with(new Country)->getTable();

		$query = $tbl_trade_buy;
		$query->setTable($query->getTable().' AS tb');
        $query = $query->join($tbl_member.' AS u1', 'u1.id', '=', 'tb.uid')
                        ->leftJoin($tbl_country.' AS c', 'c.country_code_2', '=', 'u1.country');

		$search_fields = $this->getSearchAttributes();
        $query = $this->buildORMFilter($query, $search_fields);
        
		$total_query = $query->select([
			DB::raw("SUM(tb.initial_qty) AS sum_initial_qty"),
			DB::raw("SUM(tb.open_qty) AS sum_open_qty"),
		])->get();
        $total_initial_qty = e(number_format($total_query[0]["sum_initial_qty"] ? : 0));
        $total_open_qty = e(number_format($total_query[0]["sum_open_qty"] ? : 0));

        // result build
		$query = $query->select([
            'tb.id AS buy_id',
            'u1.id AS uid',
            'u1.username',
            'u1.name',
            'c.country_en AS country',
            'tb.created_at AS buy_datetime',
            'tb.price',
            'tb.initial_qty',
            'tb.open_qty',
            'tb.status',
        ]);

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn('buy_id', function ($dt) {
                return $dt->buy_id;
            })
            ->editColumn('uid', function ($dt) {
                return $dt->uid;
            })
            ->editColumn('username', function ($dt) {
                return $dt->username;
            })
            ->editColumn('name', function ($dt) {
                return $dt->name;
            })
            ->editColumn('country', function ($dt) {
                return $dt->country;
            })
            ->editColumn('buy_datetime', function ($dt) {
                return DateTimeTool::systemToOperationDateTime($dt->buy_datetime);
            })
            ->editColumn('initial_qty', function ($dt) {
                return number_format($dt->initial_qty);
            })
            ->editColumn('open_qty', function ($dt) {
                return number_format($dt->open_qty);
            })
            ->editColumn('price', function ($dt) {
                return number_format($dt->price, 3);
            })
            ->editColumn('status', function ($dt) {
                $return = "";
                switch ($dt->status) {
                    case "P" :
                        $return = trans("trade.status.pending");
                        break;
                    case "F" :
                        $return = trans("trade.status.fulfilled");
                        break;
                    case "X" :
                        $return = trans("trade.status.cancelled");
                        break;
                    default:
                }
                return $return;
            })
            ->with([
                'total_initial_qty' => $total_initial_qty,
                'total_open_qty' => $total_open_qty
            ])
            ->make(true);;
	}
}