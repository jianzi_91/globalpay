<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Admin;
use App\Models\Member;
use App\Models\Country;
use App\Models\Logger;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class UserPasswordsController extends Controller
{
	protected $countryRepo;

	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;
		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'nid' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.nid'), 'value'=>''],
			'email' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.email'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'country' => ['search'=>'=', 'label'=>trans('field.user.country'), 'as'=>"country", "table" => "user",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'status' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.status'), 
				'options'=>[
					'10' => trans('general.user.status.10'),
					'30' => trans('general.user.status.30'),
					'90' => trans('general.user.status.90')
				], 
				'value'=>["10"]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Member;
		$query->setTable($query->getTable().' AS user');

        $columns_select = [
            'user.id',
            'user.username',
            'user.name',
            'user.nid',
            'user.email',
            'user.mobile',
            'user.country',
			'user.status',
			'user.created_at',
        ];

        if ($this->user->can("admin-privilege", "members/view-passwords")) {
            $columns_select[] = "user.password_ori";
            $columns_select[] = "user.password2_ori";
        }
		// result build
		$query = $query->select($columns_select);
        $query = $query->where("primary_acc", Member::$primary_to_code["yes"]);

		// filter build
		$search_fields = $this->getSearchAttributes();

		$query = $this->buildORMFilter($query, $search_fields);
		$datatables = Datatables::of($query);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		// process all showing data 
		return $datatables
				->editColumn('password_ori', function ($list) {
					$return = e($list["password_ori"]);
					return $return;
				})
				->editColumn('password2_ori', function ($list) {
					$return = e($list["password2_ori"]);
					return $return;
				})
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('username', function ($list) {
					return e($list['username']);
				})
				->editColumn('name', function ($list) {
					return e($list["name"]);
				})
				->editColumn('nid', function ($list) {
					return e($list['nid']);
				})
				->editColumn('country', function ($list) use ($country_code_to_name) {
					return e(isset($country_code_to_name[$list["country"]]) ? $country_code_to_name[$list["country"]] : "-");
				})
				->editColumn('email', function ($list) {
					return e($list["email"]);
				})
				->editColumn('mobile', function ($list) {
					return e($list["mobile"]);
				})
				->editColumn('status', function ($list) {
					return e(trans("general.user.status.".$list["status"]));
				})
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->addColumn('action', function ($list) {
					$return = "";
					if ($list['status'] != Member::$status_to_code["terminated"]) {
                        $return .= '<a href="'. e(url($this->admin_slug.'/members/passwords/edit', $list['id'])) .'" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> '.trans("general.page.admin.member-password-list.content.edit_password").' </a>';
                    }
					return $return;
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.member.password.index');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function edit($id)
    {
        $member = Member::where("id", "=", $id)
			->where("primary_acc", "=", Member::$primary_to_code["yes"])
			->where("status", "!=", Member::$status_to_code["terminated"])
			->firstOrFail();
            
        return view('admin.member.password.edit')->withMember($member);
    }

    public function update($id)
    {
        $redirect = redirect($this->admin_slug.'/members/passwords/edit/'.$id);
		//initiator
		$initiator = Tool::getInitiator();

        $member = Member::where("id", "=", $id)
			->where("primary_acc", "=", Member::$primary_to_code["yes"])
			->where("status", "!=", Member::$status_to_code["terminated"])
			->firstOrFail();

        if (request()->has('__req')) {
            $data = request()->all();
			$user_table = with(new Member)->getTable();
			
			if ($data['__req'] == '1') {
				$niceNames = [
					'password' => trans('field.user.password'),
                ];

				$v = validator($data, [
                    'password' => 'required|confirmed|min:6|max:255',
                ], [], $niceNames);
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'password_ori' => $data['password']
				];	
				$log_data["changes"] = Logger::retrieveChanges(["password_ori"=>$member->password_ori], $log_data["data"]);

				$member->editor_type = $initiator['itype'];
				$member->editor_id = $initiator['iid'];
				$member->password= bcrypt($data['password']);
				$member->password_ori= $data['password'];

				if ($member->save()) {
					//Log Action
					$this->setIdentifiers($member->id);
					$log_msg = "Password Updated for User ID #".$member->id.".";
					Logger::logWithoutGuard(__METHOD__, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $member->id]
					));
				}

				$redirect = $redirect->withSuccess('Password updated.');
			}
			//security password
			elseif ($data['__req'] == '2') {
				$niceNames = [
					'password2' => trans('field.user.password2'),
                ];

				$v = validator($data, [
                    'password2' => 'required|confirmed|min:6|max:255',
                ], [], $niceNames);
				
				if ($v->fails())
				   return back()->withInput()->withErrors($v);

				$log_data["data"] = [
					'password2_ori' => $data['password2']
				];	
				$log_data["changes"] = Logger::retrieveChanges(["password2_ori"=>$member->password2_ori], $log_data["data"]);

				$member->editor_type = $initiator['itype'];
				$member->editor_id = $initiator['iid'];
				$member->password2= bcrypt($data['password2']);
				$member->password2_ori= $data['password2'];

				if ($member->save()) {
					//Log Action
					$this->setIdentifiers($member->id);
					$log_msg = "Security Password Updated for User ID #".$member->id.".";
					Logger::logWithoutGuard(__METHOD__, $this->getIdentifiers(), $log_msg, array_merge(
						$log_data,
						['uid' => $member->id]
					));
				}

				$redirect = $redirect->withSuccess('Security Password updated.');
			}
        }
        return $redirect;
    }
}
