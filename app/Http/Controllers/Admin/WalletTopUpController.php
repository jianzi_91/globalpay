<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\MemberWalletRecord;
use App\Models\Member;
use App\Models\Admin;
use App\Models\TransInfo;
use App\Models\SessionKeyValue;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class WalletTopUpController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'uid' => ['search'=>'=', 'table'=>'user_wr', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'company_status' => ['search'=>'in', 'table'=>"user", 'label'=>trans("field.user.company_status"),
				'options'=>[
					'1' => trans("general.user.company_status.1"), 
					'0' => trans("general.user.company_status.0"), 
				], 
				'value'=>[]
			],
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'user_wr', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'user_wr', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new MemberWalletRecord;
		$query->setTable($query->getTable().' AS user_wr');
        $query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'user_wr.uid')
            ->where("wallet", "rwallet")
            ->where("type", "c")
            ->where("trans_code", TransInfo::$trans_type_to_code["wallet-topup"]);

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(amount) AS sum_amount"),
		])->get();
		$sum_amount = isset($total_query[0]["sum_amount"]) ? $total_query[0]["sum_amount"] : 0;

		// result build
		$query = $query->select([
			DB::raw('SUM(user_wr.amount) AS amount'),
			'user_wr.uid',
			'user.username AS user_username',
			'user.name AS user_name',
			'user.mobile AS user_mobile',
		])
		->groupBy("user.id");

		$datatables = Datatables::of($query);
		
		$datatables = $datatables->with([
			'grand_total' => [
				'amount' => e(number_format($sum_amount, 4)),
			]
		]);
    	
		return $datatables
				->editColumn('user_username', function ($list) {
					return $list->user_username;
				})
				->editColumn('user_name', function ($list) {
					return $list->user_name;
				})
				->editColumn('user_mobile', function ($list) {
					return $list->user_mobile;
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return $return;
				})
				->editColumn('uid', function ($list) {
					return $list["uid"];
				})
				->editColumn('id', function ($list) {
					return $list["id"];
				})
				->editColumn('amount', function ($list) {
					return number_format($list["amount"], 4);
				})
            ->make(true);
	}
	
    public function index()
    {
		$view = view('admin.wallet.topup-list');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }
}
