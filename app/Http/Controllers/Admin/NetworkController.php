<?php
namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Member;
use App\Models\Activation;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class NetworkController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'mobile' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'rank' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.rank'), 
				'options'=>[
					'0' => trans('general.user.rank.title.0'), 
					'1100' => trans('general.user.rank.title.1100'), 
					'2100' => trans('general.user.rank.title.2100'),
					'3100' => trans('general.user.rank.title.3100'),
				], 
				'value'=>[]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();

		$query = new Member;
		$query->setTable($member_table.' AS user');
		$query = $query->where("user.primary_acc", Member::$primary_to_code["yes"]);
		$query = $query->where("user.status", Member::$status_to_code["active"]);

		// filter build
		$search_fields = $this->getSearchAttributes();

		$query = $this->buildORMFilter($query, $search_fields);
		$datatables = Datatables::of($query);

		$query = $query->select([
			"user.id",
			"user.rank",
			"user.crank",
			"user.mobile",
			"user.name",
			"user.ref",
			"user.up",
			"user.up_pos",
			"user.created_at",
		]);

		$datatables = Datatables::of($query);
    	
		return $datatables
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('rank', function ($list) {
					return e(trans("general.user.rank.title.".$list['rank']));
				})
				->editColumn('crank', function ($list) {
					return e(trans("general.user.rank.title.".$list["crank"]));
				})
				->editColumn('mobile', function ($list) {
					return e($list["mobile"]);
				})
				->editColumn('name', function ($list) {
					return e($list["name"]);
				})
				->addColumn('account-type', function ($list) {
					$return = $dlm = "";

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["normal"],
						"status" => Activation::$status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count() || Activation::where([
						"act_type" => Activation::$act_type_to_code["contra"],
						"status" => Activation::$status_to_code["confirmed"],
						"loan_status" => Activation::$loan_status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.normal");
						$dlm = ", ";
					}

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["contra"],
						"status" => Activation::$status_to_code["confirmed"],
						"loan_status" => Activation::$loan_status_to_code["pending"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.contra");
						$dlm = ", ";
					}

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["zero-bv"],
						"status" => Activation::$status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.zbv");
						$dlm = ", ";
					}

					if (false) {
						$return .= $dlm.trans("general.user.account-type.parking");
						$dlm = ", ";
					}

					if ($list["crank"]) {
						$return .= $dlm.trans("general.user.account-type.free");
						$dlm = ", ";
					}

					if ($return == "") {
						$return = "-";
					}
					return e($return);
				})
				->editColumn('ref', function ($list) {
					$username = Member::where('id', $list["ref"])->value('username');
					if ($username) {
						return e($username);
					} else {
						return '-';
					}
				})
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->addColumn('action', function ($list) {
					$return = "";
					
					if ($this->user->can("admin-privilege", "members/edit-network")) {
						$return .= '<a href="'. e(url($this->admin_slug.'/members/change-network', $list['id'])) .'" class="btn btn-white btn-sm"> <i class="fa fa-pencil"></i> '. trans("general.page.admin.network-list.content.change_network") .' </a>';
					}
					return $return;
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.member.network-list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }
    
    public function checkGroup()
    {
        return view("admin.network.check-group");
    }

    public function isSameGroup()
    {
        $user_table = with(new Member)->getTable();

        $niceNames = [
            'username1' => "Username 1",
            'username2' => "Username 2",
        ];
        $data = request()->only(array_keys($niceNames));
        $v = validator($data, [
            'username1' => 'required|exists:'.$user_table.',mobile',
            'username2' => 'required|exists:'.$user_table.',mobile',
        ], [], $niceNames);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        $username1 = $data["username1"];
        $username2 = $data["username2"];
        $user_id1 = Member::where("username", $username1)->value("id");
        $user_id2 = Member::where("username", $username2)->value("id");
        
        if (Member::isDownlineOf($user_id1, $user_id2, "sponsor")) {
            $msg = $username1 . " is under " . $username2 . " sponsor tree.";
        }
        elseif (Member::isDownlineOf($user_id2, $user_id1, "sponsor")) {
            $msg = $username2 . " is under " . $username1 . " sponsor tree.";
        }
        else {
            $msg = $username1 . " and " . $username2 . " are not in the same group.";
        }

        return back()->with(["success" => $msg]);
    }
}
