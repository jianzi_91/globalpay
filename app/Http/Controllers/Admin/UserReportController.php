<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Activation;
use App\Models\Member;
use App\Models\Wallet;
use App\Models\MemberWalletRecord;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class UserReportController extends Controller
{
	protected $countryRepo;

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	private function getCountryReportSearchAttributes()
	{
		//search fields
		return [
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.user.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.user.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetCountryReportDataTable()
	{
		$query = new Member;
		$query->setTable($query->getTable().' AS user');
		$query = $query->join($this->countryRepo->getTable().' AS country', 'user.country', '=', 'country.country_code_2');
        $query = $query->where("primary_acc", Member::$primary_to_code["yes"])
                ->where("status", Member::$status_to_code["active"])
                ->groupBy("country");

		// result build
        $query = $query->select(DB::raw('count(user.id) AS total_member'), 'user.country AS country', 'country.country_en AS country_name', 'country.continent_en AS continent_name');
		$search_fields = $this->getCountryReportSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		$datatables = Datatables::of($query);

		// process all showing data 
		return $datatables->editColumn('total_member', function ($list) {
                    return e(number_format($list['total_member']));
				})
				->editColumn('country', function ($list) {
					return e($list['country']);
				})
				->editColumn('country_name', function ($list) {
					return e($list['country_name']);
				})
				->editColumn('continent_name', function ($list) {
					return e($list['continent_name']);
				})
                ->with([
					'current_datetime' => e(DateTimeTool::operationDateTime())
                ])
            ->make(true);
	}

    public function countryReport()
    {
		$view = view('admin.report.member-country');

		//search fields
		$search_fields = $this->getCountryReportSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

	private function getWalletReportSearchAttributes()
	{
		if ($this->user->can("admin-privilege", "report/member-wallet")) {
			$wallets = [
				'awallet2' => trans('general.wallet.awallet2'), 
				'awallet' => trans('general.wallet.awallet'), 
				'rwallet' => trans('general.wallet.rwallet'),  
				'fwallet' => trans('general.wallet.fwallet'),  
				'cwallet' => trans('general.wallet.cwallet'),  
				'swallet' => trans('general.wallet.swallet'),  
				'dowallet' => trans('general.wallet.dowallet'),  
			];
		}
		else {
            return response('Unauthorized.', 401);
		}

		//search fields
		return [
			'wallet' => ['search'=>"ignore", 'label'=>trans('field.wallet_record.wallet'),
				'options' => [
					'summary' => trans("general.selection.all")
				] + $wallets, 
				'value'=>'summary'
			],
			'id' => ['search'=>"like", "table"=>"user", 'label'=>trans('field.user.id'), "as"=>"id", 'value'=>''],
			'username' => ['search'=>"like", "table"=>"user", 'label'=>trans('field.user.username'), "as"=>"username", 'value'=>''],
			'country' => ['search'=>'=', "table"=>"user", 'label'=>trans('field.user.country'), 'as'=>"country",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'company_status' => ['search'=>'in', 'table'=>"user", 'label'=>trans("field.user.company_status"),
				'options'=>[
					'1' => trans("general.user.company_status.1"), 
					'0' => trans("general.user.company_status.0"), 
				], 
				'value'=>[]
			],
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.user.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'user', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.user.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetWalletReportDataTable()
	{
		$collect = collect();
		
		if ($this->user->can("admin-privilege", "report/member-wallet")) {
			$wallets = ['awallet2', 'awallet', 'rwallet', 'fwallet', 'cwallet', 'swallet', 'dowallet'];
		}
		else {
            return response('Unauthorized.', 401);
		}

		for ($i = 0; $i < count($wallets); $i++) {
			$query = new Member;
			$query->setTable($query->getTable().' AS user');
			$query = $query->join(with(new Wallet)->getTable().' AS wallet', 'user.id', '=', 'wallet.uid')
					->where("wallet.".$wallets[$i], ">", 0)
					->select(DB::raw("SUM(wallet.".$wallets[$i].") AS sum_amount"), DB::raw("COUNT(wallet.uid) AS user_count"));
			
			$search_fields = $this->getWalletReportSearchAttributes();
			$query = $this->buildORMFilter($query, $search_fields);
			$rs = $query->first();
			
            $collect->push([
				'wallet'       => $wallets[$i],
                'user_count'         => $rs->user_count,
                'sum_amount'      => $rs->sum_amount,
			]);
        }

		// result build
		$datatables = Datatables::of($collect);

		// process all showing data 
		return $datatables->editColumn('user_count', function ($list) {
                    return e(number_format($list['user_count']));
				})
				->editColumn('wallet', function ($list) {
					return e(trans("general.wallet.".$list["wallet"]));
				})
				->editColumn('sum_amount', function ($list) {
					return e(number_format($list['sum_amount'], 4));
				})
            ->make(true);
	}

	public function ajaxGetSubWalletReportDataTable()
	{
		$data = request()->only("wallet");
		
		if ($this->user->can("admin-privilege", "report/member-wallet")) {
			$wallets = ['awallet2', 'awallet', 'rwallet', 'fwallet', 'cwallet', 'swallet', 'dowallet'];
		}

		if (!in_array($data["wallet"], $wallets)) {
            return response('Unauthorized.', 401);
		}

		$query = new Member;
		$query->setTable($query->getTable().' AS user');
		$query = $query->join(with(new Wallet)->getTable().' AS wallet', 'user.id', '=', 'wallet.uid')
			->where("wallet.".$data["wallet"], ">", 0)
			->select(DB::raw("wallet.".$data["wallet"]." AS amount"), "username", "id");

		$search_fields = $this->getWalletReportSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		// result build
		$datatables = Datatables::of($query);

		// process all showing data 
		return $datatables->editColumn('id', function ($list) {
                    return e($list['id']);
				})
				->editColumn('username', function ($list) {
					return e($list["username"]);
				})
				->editColumn('amount', function ($list) {
					return e(number_format($list['amount'], 4));
				})
            ->make(true);
	}

    public function walletReport()
    {
		$view = view('admin.report.member-wallet');

		//search fields
		$search_fields = $this->getWalletReportSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }
}
