<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Member;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class UplineController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'mobile' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.mobile'), 'value'=>''],
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();
        $select_cols = ["id", "mobile", "name", "rank", "crank", "ref", "primary_acc", "created_at"];

		$query = new Member;
		$query->setTable($member_table.' AS user');
        $query = $query->select($select_cols);

		// filter build
		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMfilter($query, $search_fields);

        $target_user = $query->first();
        $network = "ref";
		
        if ($target_user && 
        (request()->get("id") || request()->get("mobile"))) {
		    $collect = collect();
			$primary_acc_filter = request()->get("primary_acc");
			$level = 0;
            while ($target_user) {
				if ($primary_acc_filter && in_array($target_user->primary_acc, $primary_acc_filter))
	                $collect->push($target_user->toArray() + ["level" => $level++]);
				elseif (!$primary_acc_filter)
	                $collect->push($target_user->toArray() + ["level" => $level++]);
                
                $target_user = Member::where("id", "=", $target_user->$network)
                    ->select($select_cols)
                    ->first();
            }
            
		    $datatables = Datatables::of($collect);
        }
        else {
            $query  = $query->whereRaw(DB::raw("1 = 0"));
		    $datatables = Datatables::of($query);
        }

		// process all showing data 
		return $datatables
				->addColumn('network-info', function ($list) {
					$return = "";
					$user = Member::find($list['ref']);
					$return .= $user ? e($user->mobile) : "-";
					return $return;
				})
				->editColumn('id', function ($list) {
					return $list['id'];
				})
				->editColumn('mobile', function ($list) {
					return $list['mobile'];
				})
				->editColumn('rank', function ($list) {
					return trans('general.user.rank.title.'.max($list['rank'], $list['crank']));
				})
				->editColumn('name', function ($list) {
					return $list["name"];
				})
				->editColumn('level', function ($list) {
					return $list["level"];
				})
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->rawColumns(['network-info'])
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.upline.index');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
