<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateOrUpdateExchageRequest;
use App\Models\Currency;
use App\Models\Exchange;
use App\Models\ExchangeHistory;

class ExchangeController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    public function index()
    {
        $exchange = Exchange::paginate(20);

        return view('admin.exchange.index',[
            'exchange' => $exchange
        ]);
    }

    public function create()
    {
        $currencies = Currency::orderBy('name', 'asc')->get();

        return view('admin.exchange.create', [
            'currencies' => $currencies
        ]);
    }

    public function store(CreateOrUpdateExchageRequest $request)
    {
        $exchange = Exchange::create([
            'status' => $request->status,
            'currency_id' => $request->currency,
            'buy_rate' => $request->buy_rate,
            'sell_rate' => $request->sell_rate,
            'min_withdrawal_amount' => $request->min_withdrawal_amount,
            'admin_id' => $this->user->id
        ]);

        ExchangeHistory::add($exchange, 'Add');

        return redirect($this->admin_slug.'/exchange')->with('success', 'New exchange added.');
    }

    public function edit($id)
    {
        $exchange = Exchange::findOrFail($id);
        $currencies = Currency::orderBy('name', 'asc')->get();

        return view('admin.exchange.edit', [
            'exchange' => $exchange,
            'currencies' => $currencies
        ]);
    }

    public function update(CreateOrUpdateExchageRequest $request, $id)
    {
        $exchange = Exchange::findOrFail($id);
        $original = $exchange;

        $exchange->status = $request->status;
        $exchange->buy_rate = $request->buy_rate;
        $exchange->sell_rate = $request->sell_rate;
        $exchange->min_withdrawal_amount = $request->min_withdrawal_amount;


        if ($exchange->save()) {
            ExchangeHistory::add($original, 'Edit');

            return redirect($this->admin_slug.'/exchange')->with('success', 'Exchange has been update.');
        } else {
            return back()->with('errors', ['Something failed when update. Please try again.']);
        }
    }

    public function delete($id)
    {
        $exchange = Exchange::findOrFail($id);
        $original = $exchange;

        $exchange->delete();

        ExchangeHistory::add($original, 'Delete');

        return redirect($this->admin_slug.'/exchange')->with('success', 'Exchange has been delete.');
    }

    public function history()
    {
        $histories = ExchangeHistory::orderBy('created_at', 'desc')->paginate(20);

        return view('admin.exchange.history', [
            'histories' => $histories
        ]);
    }
}
