<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;

use App\Models\Country;
use App\Models\Member;
use App\Models\MemberTrade;
use App\Models\MemberTradeLog;
use App\Models\Withdrawal;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TradeSummaryController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    private function getSearchAttributes()
	{
		return [
			'id' => ['search'=>'like', 'table'=>'u1', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'u1', 'label'=>trans('field.user.username'), 'value'=>''],
			'country' => ['search'=>'like', 'table'=>'c', 'label'=>trans('field.user.country'), 'value'=>'', 'as'=>'country_en'],
            'sponsor' => ['search'=>'like', 'table'=>'u2', 'label'=>trans('field.user.ref'), 'value'=>'', 'as'=>'username'],

            'date' => [
                'type' => 'date_range',
                'label' => trans('general.search_field.field.date'),
    			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'mtl', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'tx_datetime', 'role'=>'from'],
    			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'mtl', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>'', 'as'=>'tx_datetime', 'role'=>'to']
            ],
		];
	}

    public function index()
    {
		$search_fields = $this->getSearchAttributes();

        return view('admin.trade.summary')->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxTradeSummaryDataTable()
	{
        $tbl_member = with(new Member)->getTable();
        $tbl_member_trade = with(new MemberTrade)->getTable();
        $tbl_member_trade_log = with(new MemberTradeLog)->getTable();
        $tbl_country = with(new Country)->getTable();

		$query = new Member;
		$query->setTable($tbl_member.' AS u1');
        $query = $query->leftJoin($tbl_country.' AS c', 'c.country_code_2', '=', 'u1.country')
                        ->leftJoin($tbl_member.' AS u2', 'u1.ref', '=', 'u2.id')
		                ->leftJoin($tbl_member_trade.' AS mt', 'mt.uid', '=', 'u1.id')
                        ->rightJoin(DB::raw('(select uid, MAX(created_at) AS tx_datetime from '.$tbl_member_trade_log.' WHERE type = "N" GROUP BY uid) mtl'), function($join) {
                            $join->on('mtl.uid', '=', 'u1.id');
                        })
                        ->where('u1.primary_acc', Member::$primary_to_code['yes']);

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		// result build
		$query = $query->select(
            'mtl.tx_datetime',
            'u1.id AS uid',
            'u1.username',
            'u1.name',
            'c.country_en AS country',
            'u2.username AS sponsor',
            'mt.amt_sold AS total_trade_income'
        );

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn('tx_datetime', function ($dt) {
                return DateTimeTool::systemToOperationDateTime($dt->tx_datetime);
            })
            ->editColumn('total_investment_amt', function ($dt) {
                return number_format(Member::getUserTotalInvestment($dt->uid));
            })
            ->editColumn('total_trade_income', function ($dt) {
                return number_format($dt->total_trade_income, 3);
            })
            ->editColumn('total_pending_withdrawal', function ($dt) {
                return number_format(Withdrawal::getTotalPendingByUserId($dt->uid), 2);
            })
            ->editColumn('total_paid_withdrawal', function ($dt) {
                return number_format(Withdrawal::getTotalPaidByUserId($dt->uid), 2);
            })
            ->make(true);;
	}
}