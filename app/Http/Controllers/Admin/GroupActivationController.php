<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Activation;
use App\Models\Member;
use App\Models\DownlineCache;
use App\Models\Wallet;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class GroupActivationController extends Controller
{
	protected $countryRepo;
    
	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'uid' => ['search'=>'raw', 'label'=>trans('field.user.id'),  'value'=>''],
			'username' => ['search'=>'like', 'label'=>trans('field.user.username'), 'value'=>''],
			'up_pos' => ['search'=>'in', 'label'=>trans('field.user.up_pos'), 
				'options'=>[
					'1' => trans('general.user.up_pos.1'), 
					'2' => trans('general.user.up_pos.2'),
				], 
                'value'=>''
            ],
			
			'code' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.code-name'), 
				'options'=>[
					'1100' => trans('general.activation.title.1100'), 
					'2100' => trans('general.activation.title.2100'),
					'3100' => trans('general.activation.title.3100'),
					'4100' => trans('general.activation.title.4100'),
					'5100' => trans('general.activation.title.5100'),
				], 
				'value'=>[]
			],
			'act_type' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.act_type'), 
				'options'=>[
					'10' => trans('general.activation.act_type.10'), 
					'30' => trans('general.activation.act_type.30'),
					'70' => trans('general.activation.act_type.70'),
					'50' => trans('general.activation.act_type.50'),
					'90' => trans('general.activation.act_type.90'),
				], 
				'value'=>[]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.activation.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.activation.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();
		$act_table = with(new Activation)->getTable();
		$downline_cache_table = with(new DownlineCache)->getTable();

		$downline_cache_flag = DownlineCache::count() > 1;

		$query = new Activation;
		$query->setTable($act_table.' AS act');
		if ($downline_cache_flag) {
			$query = $query->join($downline_cache_table.' AS dc', 'dc.downline_id', '=', 'act.uid');
		}
		$query = $query->join($member_table.' AS user', 'user.id', '=', 'act.uid');
		$query = $query->leftjoin($member_table.' AS primary_user', 'user.primary_id', '=', 'primary_user.id');

		$search_fields = $this->getSearchAttributes();

        $target_user_id = null;

        if ((request()->has("uid") || request()->has("username")) && request()->has("up_pos")){
            $user_id = request()->get("uid");
            $username = request()->get("username");
            $up_pos = request()->get("up_pos");
            
			$target_user_query = new Member;
			$target_user_query->setTable($member_table.' AS user');
			
			if (!$downline_cache_flag) {
				$target_user_query = $target_user_query->join($member_table.' AS up_user', 'up_user.id', '=', 'user.up');
				
				if ($user_id) {
					$target_user_query = $target_user_query->where('up_user.id', $user_id);
				}
				if ($username) {
					$target_user_query = $target_user_query->where('up_user.username', $username);
				}
           		$target_user_query = $target_user_query->where('user.up_pos', $up_pos);

			}
			else {
				if ($user_id) {
					$target_user_query = $target_user_query->where('user.id', $user_id);
				}
				if ($username) {
					$target_user_query = $target_user_query->where('user.username', $username);
				}
			}
			
			$target_user = $target_user_query->select("user.id")->first();
			$target_user_id = $target_user ? $target_user->id : null;
        }

        if ($target_user_id) {
            unset($search_fields["up_pos"], $search_fields["uid"], $search_fields["username"]);

            $query = $query->where("act.status", Activation::$status_to_code["confirmed"])
                ->where("act.b_status", Activation::$status_to_code["confirmed"]);
			
			if ($downline_cache_flag) {
                $query = $query->where("dc.user_id", $target_user_id)
                ->where("dc.position", $up_pos);
			}
			else {
				$target_group_user = array_merge([$target_user_id], Member::getAllDownlineId($target_user_id)); 
				$query = $query->whereIn("act.uid", $target_group_user);
			}

		    $query = $this->buildORMFilter($query, $search_fields);
        }
        else {
            $query  = $query->whereRaw(DB::raw("1 = 0"));
        }
        
		$total_query = $query->select([
			DB::raw("SUM(act.price) AS sum_price"),
			DB::raw("SUM(act.amount) AS sum_amount"),
		])->get();
		$price_sum = isset($total_query[0]["sum_price"]) ? $total_query[0]["sum_price"] : 0;
		$amount_sum = isset($total_query[0]["sum_amount"]) ? $total_query[0]["sum_amount"] : 0;

		$query = $query->select([
			'act.*', 
			'user.username AS user_username',
			'user.name AS user_name',
			'user.nid AS user_nid',
			'user.ref AS user_ref',
			'user.up AS user_up',
			'user.up_pos AS user_up_pos',
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END AS user_country'),
		]);

		$datatables = Datatables::of($query);

		$datatables = $datatables->with([
			'grand_total' => [
				'price' => e(number_format($price_sum, 2)),
				'amount' => e(number_format($amount_sum, 2)),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('uid', function ($list) {
					return e($list['uid']);
				})
				->addColumn('uid-username', function ($list) {
					return e($list["user_username"]);
				})
				->addColumn('uid-name', function ($list) {
					return e($list["user_name"]);
				})
				->addColumn('uid-nid', function ($list) {
					return e($list["user_nid"]);
				})
				->addColumn('uid-country', function ($list) use ($country_code_to_name) {
					return e(isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-");
				})
				->addColumn('ref', function ($list) {
					$username = Member::where('id', $list["user_ref"])->value('username');
					if ($username) {
						return $list["user_ref"]
							.' <br>'.$username;
					} else {
						return '-';
					}

				})
				->addColumn('up', function ($list) {
					$username = Member::where('id', $list["user_up"])->value('username');
					if ($username) {
						return $list["user_up"]
							.' <br>'.$username
							.' <br>'.trans("general.user.up_pos.".$list["user_up_pos"]);
					} else {
						return '-';
					}
				})
				->editColumn('code', function ($list) {
					return e(trans("general.activation.title.".$list["code"]));
				})
				->editColumn('price', function ($list) {
					return e(number_format($list["price"], 2));
				})
				->editColumn('amount', function ($list) {
					return e(number_format($list["amount"], 2));
				})
				->rawColumns(['ref', 'up'])
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.activation.group-activations');

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
