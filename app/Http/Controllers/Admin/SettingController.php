<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Setting;
use App\Models\TradeSetting;
use App\Models\CommCondition;

class SettingController extends Controller
{
    public function general()
    {	
        $settings = Setting::firstOrFail();
        $trade_settings = TradeSetting::firstOrFail();
		$view = view('admin.setting.general');

        if (request()->isMethod('post')) {
            $niceNames = [
                'bonus_swallet_percent' => trans('field.setting.bonus_swallet_percent', ["wallet" => trans("general.wallet.swallet")]),
                'unfreeze_wallet_percent' => trans('field.setting.unfreeze_wallet_percent', ["wallet" => trans("general.wallet.awallet2")]),
                'qty_per_price' => trans('field.trade_setting.qty_per_price'),
                'price_change' => trans('field.trade_setting.price_change'),
                'platform_charge' => trans('field.trade_setting.platform_charge'),
                'charity_percent' => trans('field.trade_setting.charity_percent'),
            ];

            $data = request()->only(array_keys($niceNames));
            
            // validation 
            $v = validator($data, [
                'bonus_swallet_percent' => 'required|min:0.01|numeric',
                'unfreeze_wallet_percent' => 'required|min:0.01|numeric',
                'qty_per_price' => 'required|min:1|integer',
                'price_change' => 'required|min:0.001|numeric',
                'platform_charge' => 'required|min:0.01|numeric',
                'charity_percent' => 'required|min:0.01|numeric',
            ], [], $niceNames);
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            $settings->bonus_swallet_percent = $data["bonus_swallet_percent"];
            $settings->unfreeze_wallet_percent = $data["unfreeze_wallet_percent"];
            $settings->save();

            $trade_settings->qty_per_price = $data["qty_per_price"];
            $trade_settings->price_change = $data["price_change"];
            $trade_settings->platform_charge = $data["platform_charge"];
            $trade_settings->charity_percent = $data["charity_percent"];
            $trade_settings->save();

            return back()->withSuccess("General Setting is updated");
        }
		
        return $view->with(compact("settings", "trade_settings"));
    }
    
    public function activeBonus()
    {	
        $comm_condition_table = with(new CommCondition)->getTable();
        $active_conditions = CommCondition::where("type", 1)->orderBy("amt_min")->get();
		$view = view('admin.setting.active_bonus');

        if (request()->isMethod('post')) {
            $niceNames = [
                'id' => trans('field.comm_condition.id'),
                'amt_min' => trans('field.comm_condition.amt_min'),
                'amt_max' => trans('field.comm_condition.amt_max'),
                'percent' => trans('field.comm_condition.percent'),
            ];

            $data = request()->only(array_keys($niceNames));
            
            // validation 
            $v = validator($data, [
                'id' => 'nullable|exists:'.$comm_condition_table.",id,type,1",
                'amt_min' => 'required|min:0.01|numeric',
                'amt_max' => 'required|min:0.01|numeric',
                'percent' => 'required|min:0.01|numeric',
            ], [], $niceNames);
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            if ($data["id"]) {
                $comm_cond = CommCondition::where("id", $data["id"])
                    ->where("type", 1)
                    ->firstOrFail();
            }
            else {
                $comm_cond = new CommCondition();
                $comm_cond->type = 1;
            }

            $comm_cond->amt_min = $data["amt_min"];
            $comm_cond->amt_max = $data["amt_max"];
            $comm_cond->percent = $data["percent"];
            $comm_cond->save();

            return back()->withSuccess("Active Bonus Condition is updated");
        }
		
        return $view->with(compact("active_conditions"));
	}
    
    public function deleteActiveBonus($id)
    {	
        CommCondition::where("type", 1)->where("id", $id)->delete();
		
        return back()->withSuccess("Active Bonus Condition is deleted");
	}
    
    public function passiveBonus()
    {	
        $comm_condition_table = with(new CommCondition)->getTable();
        $passive_conditions = CommCondition::where("type", 2)->orderBy("amt_min")->get();
		$view = view('admin.setting.passive_bonus');

        if (request()->isMethod('post')) {
            $niceNames = [
                'id' => trans('field.comm_condition.id'),
                'amt_min' => trans('field.comm_condition.amt_min'),
                'amt_max' => trans('field.comm_condition.amt_max'),
                'percent' => trans('field.comm_condition.percent'),
            ];

            $data = request()->only(array_keys($niceNames));
            
            // validation 
            $v = validator($data, [
                'id' => 'nullable|exists:'.$comm_condition_table.",id,type,2",
                'amt_min' => 'required|min:0.01|numeric',
                'amt_max' => 'required|min:0.01|numeric',
                'percent' => 'required|min:0.01|numeric',
            ], [], $niceNames);
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            if ($data["id"]) {
                $comm_cond = CommCondition::where("id", $data["id"])
                    ->where("type", 2)
                    ->firstOrFail();
            }
            else {
                $comm_cond = new CommCondition();
                $comm_cond->type = 2;
            }

            $comm_cond->amt_min = $data["amt_min"];
            $comm_cond->amt_max = $data["amt_max"];
            $comm_cond->percent = $data["percent"];
            $comm_cond->save();

            return back()->withSuccess("Passive Bonus Condition is updated");
        }
		
        return $view->with(compact("passive_conditions"));
	}
    
    public function deletePassiveBonus($id)
    {	
        CommCondition::where("type", 2)->where("id", $id)->delete();
		
        return back()->withSuccess("Passive Bonus Condition is deleted");
	}
}
