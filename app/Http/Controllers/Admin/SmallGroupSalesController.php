<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Member;
use App\Models\MemberCFStatDaily;
use App\Models\MemberStat;

use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class SmallGroupSalesController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'uid' => ['search'=>'=', 'label'=>trans('field.user.id'), 'as'=>"uid", 'table'=>'cf_daily', 'value'=>''],
			'username' => ['search'=>'like', 'label'=>trans('field.user.username'), 'as'=>"username", 'table'=>'user', 'value'=>''],
            'name' => ['search'=>'like', 'label'=>trans('field.user.name'), 'as'=>"name", 'table'=>'user', 'value'=>''],
			'primary_acc' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.primary_acc'), 
				'options'=>[
					'y' => trans('general.user.primary_acc.y'),
					'n' => trans('general.user.primary_acc.n'),
				], 
				'value'=>[]
			],
            
			'amount_from' => ['search'=>'ignore', 'label'=>trans('general.search_field.field.field-from', ['field'=>trans('general.table.header.amount')]), 'value'=>''],
			'amount_to' => ['search'=>'ignore', 'label'=>trans('general.search_field.field.field-to', ['field'=>trans('general.table.header.amount')]), 'value'=>''],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'cf_daily', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'tdate', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'cf_daily', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'tdate', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new MemberCFStatDaily();
		$query->setTable($query->getTable().' AS cf_daily');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'cf_daily.uid');
		$query = $query->join(with(new MemberStat)->getTable().' AS ms', 'cf_daily.uid', '=', 'ms.uid');

        $query = $query->where("cf_daily.paired_max", ">", 0);
        
		$search_fields = $this->getSearchAttributes();
        $query = $this->buildORMFilter($query, $search_fields);

		$query = $query->groupBy("cf_daily.uid");
        
        if (request()->get("amount_from")) {
            $query = $query->having("SUM_sleg_amt", ">=", request()->get("amount_from"));
        } 
        if (request()->get("amount_to")) {
            $query = $query->having("SUM_sleg_amt", "<=", request()->get("amount_to"));
        }
		$query = $query->select([
			DB::raw('user.id AS user_id'),
			DB::raw('user.username AS user_username'),
			DB::raw('user.name AS user_name'),
			DB::raw('MAX(cf_daily.tdate) AS cf_daily_tdate'),
			DB::raw('IF(ms.leg1_cf>0, SUM(cf_daily.today_leg2_amt), SUM(cf_daily.today_leg1_amt)) AS SUM_sleg_amt'),
			DB::raw('IF(ms.leg1_cf>0, 2, 1) AS SUM_sleg_position'),
		]);

        $datatables = Datatables::of($query);

		return $datatables
                ->editColumn('user_id', function ($list){
                    return e($list["user_id"]);
                }) 
                ->editColumn('user_username', function ($list){
                    return e($list["user_username"]);
                })
                ->editColumn('user_name', function ($list){
                    return e($list["user_name"]);
                }) 
                ->editColumn('cf_daily_tdate', function ($list){
                    return e($list["cf_daily_tdate"]);
                }) 
                ->editColumn("SUM_sleg_amt", function ($list) {
                    return e(number_format($list["SUM_sleg_amt"], 2));
                })
                ->editColumn("SUM_sleg_position", function ($list) {
                    return e(trans("general.user.up_pos.".$list["SUM_sleg_position"]));
                })
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.activation.small-group-activation-summary');

		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }
}
