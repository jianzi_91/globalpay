<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;
use Image;
use Storage;

use App\Interfaces\ManualWithdrawalCode;
use App\Models\ManualWithdrawal;
use App\Models\Withdrawal;
use App\Models\Admin;
use App\Models\Member;
use App\Models\Logger;
use App\Models\TransInfo;
use App\Models\Wallet;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class ManualWithdrawalController extends Controller
{
    protected $countryRepo;
	
	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
    }

    private function getSearchAttributes()
	{
		//search fields
        return [
            'id' => ['search'=>'like', 'table'=>'mw', 'label' => trans('field.user.id'), 'value'=>''],
            'name' => ['search'=>'like', 'table'=>'user', 'label' => trans('field.user.name'), 'value'=>''],
            'mobile' => ['search'=>'like', 'table'=>'user', 'label' => trans('field.user.mobile'), 'value'=>''],
			'status' => ['search'=>'in', 'table'=>'mw', 'label'=>trans('field.user.status'), 
				'options'=> ManualWithdrawal::statusTypes(),
				'value'=>[ManualWithdrawalCode::WITHDRAWAL_PENDING]
			],
            'date' => [
                'type' => 'date_range',
                'label' => trans('field.user.created_at'),
                'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'mw', 'label'=>trans('general.search_field.field.start_date'), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],
                'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'mw', 'label'=>trans('general.search_field.field.end_date'), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
            ],
        ];
	}
    
    public function index()
    {
		$view = view('admin.manualwithdrawal.list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        
        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxGetDataTable()
	{
		$query = new ManualWithdrawal;
		$query->setTable($query->getTable().' AS mw');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'mw.uid');
						//->join(with(new MemberProfile)->getTable().' AS mp', 'user.id', '=', 'with')

		// result build
		$query = $query->select([
			'mw.*',
			"user.username AS member_username",
            "user.name AS member_name",
            "user.mobile as member_mobile"
		]);

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		return Datatables::of($query)
				->editColumn('name', function ($list) {
					return e($list["member_name"]);
                })
                ->editColumn('mobile', function ($list) {
					return e($list["member_mobile"]);
                })
                ->editColumn('receivable_amount', function ($list) {
					return e(number_format($list["receivable_amount"], 2));
                })
                ->editColumn('commission_amount', function ($list) {
					return e(number_format($list["commission_amount"], 2));
                })
                ->editColumn('currency_amount', function ($list) {
					return e(number_format($list["receivable_amount"]*7, 2));
				})
				->editColumn('status', function ($list) {
                    // $status = '';

                    // if ($list['status'] == ManualWithdrawal::WITHDRAWAL_COMPLETE) {
                    //     $status = '<label class="label label-success">' . ManualWithdrawal::statusTypes()[$list['status']] . '</label>';
                    // }
                    // elseif ($list['status'] == ManualWithdrawal::WITHDRAWAL_PENDING) {
                    //     $status = '<label class="label label-warning">' . ManualWithdrawal::statusTypes()[$list['status']] . '</label>';
                    // }
                    // else {
                    //     $status = '<label class="label label-danger">' . ManualWithdrawal::statusTypes()[$list['status']] . '</label>';
                    // }

                    // return $status;
					return ManualWithdrawal::statusTypes()[$list["status"]];
				})
				->editColumn('action', function ($list) {
					$return = '';
                    if ($list["status"] == 0) {
                        $return = '<a href="'. e(url($this->admin_slug.'/wallet-manualwithdrawal/'.$list['id'].'/edit')) .'" class="btn btn-info btn-sm" target="_blank"><i class="fa fa-pencil"></i> '. e(trans('general.button.edit')) .' </a>';
                    } else {
                        $return = '<a href="'. e(url($this->admin_slug.'/wallet-manualwithdrawal/'.$list['id'].'/view')) .'" class="btn btn-info btn-sm" target="_blank"><i class="fa fa-pencil"></i> '. e(trans('general.button.view')) .' </a>';
                    }
					return $return;
				})
            	->make(true);
    }
    
    public function getEdit()
    {
        $manualwithdrawal_id = request('id');

        $manualwithdrawal = ManualWithdrawal::where('id', $manualwithdrawal_id)->first();
        $user_info = Member::where('id', $manualwithdrawal->uid)->first();

        return view('admin.manualwithdrawal.edit', compact('manualwithdrawal', 'user_info'));
    }

    public function postEdit()
    {
        $data = request()->all();
        $admin = $this->user;

        $customAttributes = [
            'image' => trans('field.withdrawal.image')
        ];

        $validator = validator($data, [
            'image' => 'required|mimes:jpeg,jpg,png,bmp',
        ], [], $customAttributes);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $manualwithdrawal_id = request('id');
        $manualwithdrawal = ManualWithdrawal::where('id', request('id'))->first();

        $msg = '';

        $image = Image::make(request()->file('image'))->encode('jpg');
        $path = 'manualwithdrawal/admin/' . time() . '.jpg';

        Storage::disk('s3')->put($path, (string)$image, 'public');
        
        switch ($data['status']) {
            case 'confirm':
            $msg = trans('field.withdrawal.confirm_success');
            $manualwithdrawal->update([
                'status' => 1,
                'receipt_url' => Storage::disk('s3')->url($path)
            ]);

            Wallet::adjust([
                'uid' => config('app.company_main_account_id'),
                'trans_code' => Wallet::CASHOUT_ADMIN_APPROVED,
                'type' => 'manualwithdrawal-admin-approved',
                'source_id' => request('id'),
                'creator_user_type' => $admin::$user_code,
                'creator_uid' => $admin->id,
                'do_amt' => $manualwithdrawal->commission_amount,
            ]);

                break;

            case 'cancel':
            $msg = trans('field.withdrawal.cancel_success');
            $manualwithdrawal->update([
                'status' => 2,
                'receipt_url' => Storage::disk('s3')->url($path)
            ]);

            Wallet::adjust([
                'uid' => $manualwithdrawal->uid,
                'trans_code' => Wallet::CASHOUT_ADMIN_REJECTED,
                'type' => 'manualwithdrawal-admin-rejected',
                'source_id' => request('id'),
                'creator_user_type' => $admin::$user_code,
                'creator_uid' => $admin->id,
                'do_amt' => ($manualwithdrawal->receivable_amount + $manualwithdrawal->commission_amount),
            ]);
                break;
                
			default:
				throw new Exception('status not found');
				break;
        }
        
        return redirect('admin/wallet-manualwithdrawal')->with('success', $msg);

    }

    public function view()
    {
        $manualwithdrawal_id = request('id');

        $manualwithdrawal = ManualWithdrawal::where('id', $manualwithdrawal_id)->first();
        $user_info = Member::where('id', $manualwithdrawal->uid)->first();

        return view('admin.manualwithdrawal.view', compact('manualwithdrawal', 'user_info'));
    }
}