<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\MemberTrade;
use App\Models\Member;
use App\Repositories\CountryRepo;

use Yajra\Datatables\Datatables;

class TradeBalanceController extends Controller
{
	protected $countryRepo;

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

    private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'u', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'=', 'table'=>'u', 'label'=>trans('field.user.username'), 'value'=>''],
            'country' => ['search'=>'=', 'label'=>trans('field.user.country'), 'as'=>"country", "table" => "u",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
        ];
	}

    public function index()
    {
		$view = view('admin.trade.balance');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxTradeBalanceDataTable()
	{
        $member_trade_tbl = new MemberTrade;
        $member_tbl = new Member;

		$query = $member_trade_tbl;
		$query->setTable($query->getTable().' AS mt');
		$query = $query->join(with($member_tbl)->getTable().' AS u', 'u.id', '=', 'mt.uid');

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
        
        // declare credit_sum & debit_sum for balance calculation
        $sum_query = $query->select([
                DB::raw('SUM(unit_held) AS unit_held_sum'),
            ])
            ->get();

        $unit_held_sum = e(number_format($sum_query[0]->unit_held_sum ? $sum_query[0]->unit_held_sum : 0));

        // result build
		$query = $query->select(
            "u.id",
            "u.username",
            "u.name",
            "u.country",
            "mt.unit_held"
        );

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn("id", function ($list) {
                return e($list["id"]);
            })
            ->editColumn("username", function ($list) {
                return e($list["username"]);
            })
            ->editColumn("name", function ($list) {
                return e($list["name"]);
            })
            ->editColumn('country', function ($list) use ($country_code_to_name) {
                return e(isset($country_code_to_name[$list["country"]]) ? $country_code_to_name[$list["country"]] : "-");
            })
            ->editColumn("unit_held", function ($list) {
                return e(number_format($list["unit_held"]));
            })
            ->with([
                'unit_held_sum' => $unit_held_sum
            ])
            ->make(true);
	}
}