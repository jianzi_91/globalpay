<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\TradeSell;
use App\Models\Member;
use App\Models\Country;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class TradeSellController extends Controller
{
    private function getSearchAttributes()
	{
		//search fields
		return [

			'id' => ['search'=>'=', 'table'=>'s', 'label'=>trans('general.table.header.sell_id'), 'value'=>''],
			'uid' => ['search'=>'=', 'table'=>'s', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'=', 'table'=>'u', 'label'=>trans('field.user.username'), 'value'=>''],
			'price' => ['search'=>'=', 'table'=>'s', 'label'=>trans('trade.price'), 'value'=>''],

            'status' => ['search'=>'in', 'table'=>'s', 'label'=>trans('general.table.header.status'),
				'options'=>[
					'P' => trans("trade.status.pending"),
					'F' => trans("trade.status.fulfilled"),
                    'X' => trans("trade.status.cancelled"),
				], 
				'value'=>[]
			],

            'sell_date' => [
                'type' => 'date_range',
                'label' => trans("general.search_field.field.date"),
                'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'s', 'label'=> trans("general.search_field.field.from"), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],
                'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'s', 'label'=>trans("general.search_field.field.to"), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
            ],

		];
	}

    public function index()
    {
		$view = view('admin.trade.sell');
		
		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function ajaxTradeSellDataTable()
	{
        $tbl_trade_sell = new TradeSell;
        $tbl_member = with(new Member)->getTable();
        $tbl_country = with(new Country)->getTable();

		$query = $tbl_trade_sell;
		$query->setTable($query->getTable().' AS s');
		$query = $query->join($tbl_member.' AS u', 'u.id', '=', 's.uid')
                        ->leftJoin($tbl_country.' AS c', 'c.country_code_2', '=', 'u.country');


		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

        // initialize total_sold & total_pending
        $total_query = $query->select([
                DB::raw("SUM(IF(s.status != 'X', s.open_qty, 0)) AS total_pending"),
            ])
            ->get();
        $total_pending = e(number_format($total_query[0]->total_pending ? : 0));

		// result build
		$query = $query->select(
            's.id AS sell_id',
            's.uid',
            'u.username',
            'u.name',
            'c.country_en',
            's.initial_qty',
            's.open_qty',
            's.price',
            's.status',
            's.created_at AS sell_datetime'
        );

		return Datatables::of($query)
            ->addIndexColumn()
            ->editColumn('initial_qty', function ($dt) {
                return '<span class="add-placed">'.number_format($dt->initial_qty).'</span>';
            })
            ->editColumn('open_qty', function ($dt) {
                return '<span class="add-pending">'.number_format($dt->open_qty).'</span>';
            })
            ->editColumn('price', function ($dt) {
                return number_format($dt->price, 3);
            })
            ->editColumn('status', function ($dt) {
                switch ($dt->status) {
                    case 'F':
                        return trans('trade.status.fulfilled');
                    case 'P':
                        return trans('trade.status.pending');
                    case 'X':
                    case 'HX':
                        return trans('trade.status.cancelled');
                    default:
                        return '-';
                }
            })
            ->editColumn('sell_datetime', function ($dt) {
                return DateTimeTool::systemToOperationDateTime($dt->sell_datetime);
            })
            ->with([
                'total_pending' => $total_pending,
            ])
            ->rawColumns(['initial_qty', 'open_qty'])
            ->make(true);;
	}
}