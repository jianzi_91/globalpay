<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateOrUpdateNewsRequest;
use App\Models\News;
use App\Utils\Tool;
use Illuminate\Http\Request;

use App\Http\Requests;

class NewsController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

    /**
     * Index page. List of news &event
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $news = News::query();

        $search_fields = $request->only([
            'title_en',
            'title_zh_cn',
            'content_en',
            'content_zh_cn',
            "pop_up",
            'status'
        ]);

        // check if request has parameter from search form
        if (!empty($search_fields)) {
            foreach ($search_fields as $field => $value) {
                if (!empty($value)) {
                    // check status
                    if ($field == 'status') {
                        if (count($value) < 2) {
                            if (array_has($value, 'published')) {
                                $news->where($field, $value['published']);
                            }

                            if (array_has($value, 'not_published')) {
                                $news->where($field, $value['not_published']);
                            }
                        }
                    } else {
                        $news = $news->where($field, 'like', "%$value%");
                    }
                }
            }
        }

        $news->orderBy('updated_at', 'desc');

        return view('admin.news.index', [
            'news' => $news->paginate(20),
            'search' => $request
        ]);
    }

    /**
     * Create page.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Save new data into database.
     *
     * @param CreateOrUpdateNewsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateOrUpdateNewsRequest $request)
    {
        $initiator = Tool::getInitiator();
        News::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'pop_up' => $request->pop_up,
            'status' => $request->status,
            'title_en' => $request->title_en,
            'title_zh_cn' => $request->title_cn,
            'descr_en' => $request->description_en,
            'descr_zh_cn' => $request->description_cn,
            'content_en' => $request->content_en,
            'content_zh_cn' => $request->content_cn,
        ]);

        return redirect($this->admin_slug.'/news')->with(['success' => 'News has been created.']);
    }

    /**
     * Edit page.
     *
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $item = News::find($id);

        return view('admin.news.edit', ['item' => $item]);
    }

    /**
     * Update news.
     *
     * @param CreateOrUpdateNewsRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateOrUpdateNewsRequest $request, $id)
    {
        $item = News::find($id);

        $item->status = $request->status;
        $item->pop_up = $request->pop_up;
        $item->start_date = $request->start_date;
        $item->end_date = $request->end_date;
        $item->title_en = $request->title_en;
        $item->title_zh_cn = $request->title_cn;
        $item->descr_en = $request->description_en;
        $item->descr_zh_cn = $request->description_cn;
        $item->content_en = $request->content_en;
        $item->content_zh_cn = $request->content_cn;

        if ($item->save()) {
            return redirect($this->admin_slug.'/news')->with(['success' => 'News has been updated.']);
        } else {
            return back()->with('errors', ['Something failed when update. Please try again.']);
        }
    }
}
