<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\MemberCFStatDaily;
use App\Models\Member;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class MemberCFStatDailyController extends Controller
{
    public function __construct() 
    {
		parent::__construct();
    }

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'stat', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.member_cf_stat_daily.tdate')]), 'value'=>"", 'as'=>'tdate', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'stat', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.member_cf_stat_daily.tdate')]), 'value'=>"", 'as'=>'tdate', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new MemberCFStatDaily();
		$query->setTable($query->getTable().' AS stat');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'stat.uid');

		$search_fields = $this->getSearchAttributes();
		$query = $query->where('paired_amt', '>', 0)->select('stat.*');
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(stat.paired_amt) AS sum_paired_amt"),
		])->get();
		$sum_paired_amt = $total_query[0]["sum_paired_amt"] ? : 0;
		
		// result build
		$query = $query->select([
			'stat.*'
		]);

		$datatables = Datatables::of($query);
		
		$datatables = $datatables->with([
			'grand_total' => [
				'paired_amt' => e(number_format($sum_paired_amt, 2)),
			]
		]);
    	
		return $datatables
				->editColumn('id', function ($list) {
					return e($list["id"]);
				})
				->editColumn('uid', function ($list) {
					return e($list["uid"]);
				})
				->addColumn('uid-username', function ($list) {
					$user = Member::find($list['uid']);	
					return e($user ? $user->username : "-");
				})
				->addColumn('leg1_cf-before', function ($list) {
					return e(number_format($list["leg1_cf"] + $list["today_leg1_amt"], 2));
				})
				->addColumn('leg2_cf-before', function ($list) {
					return e(number_format($list["leg2_cf"] + $list["today_leg2_amt"], 2));
				})
				->addColumn('leg1_cf-after', function ($list) {
					return e(number_format($list["leg1_cf"] + $list["today_leg1_amt"] - $list["paired_amt"], 2));
				})
				->addColumn('leg2_cf-after', function ($list) {
					return e(number_format($list["leg2_cf"] + $list["today_leg2_amt"] - $list["paired_amt"], 2));
				})
				->editColumn('paired_amt', function ($list) {
					return e(number_format($list["paired_amt"], 2));
				})
				->editColumn('paired_max', function ($list) {
					return e(number_format($list["paired_max"], 2));
				})
				->editColumn('tdate', function ($list) {
					return e($list['tdate']);
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.commission.cf-stat-daily-list');

		//search fields
		$search_fields = $this->getSearchAttributes();
		
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
