<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Commission;
use App\Models\Member;
use App\Models\Activation;
use App\Models\Withdrawal;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class CommissionAnalysisController extends Controller
{
	protected $countryRepo;

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;
		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'uid' => ['search'=>'raw-like', 'label'=>trans('field.user.id'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.id ELSE user.id END) LIKE ?", 'value'=>''],
			'username' => ['search'=>'raw-like', 'label'=>trans('field.user.username'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.username ELSE user.username END) LIKE ?", 'value'=>''],
			'name' => ['search'=>'raw-like', 'label'=>trans('field.user.name'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.name ELSE user.name END) LIKE ?", 'value'=>''],
			'nid' => ['search'=>'raw-like', 'label'=>trans('field.user.nid'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.nid ELSE user.nid END) LIKE ?", 'value'=>''],
			'country' => ['search'=>'raw', 'label'=>trans('field.user.country'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END) = ?",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'comm', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.commission.bdate')]), 'value'=>"", 'as'=>'bdate', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'comm', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.commission.bdate')]), 'value'=>"", 'as'=>'bdate', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Commission();
		$query->setTable($query->getTable().' AS comm');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'comm.uid');
		$query = $query->leftjoin(with(new Member)->getTable().' AS primary_user', 'user.primary_id', '=', 'primary_user.id');

        $query = $query->where("comm.status", Commission::$status_to_code["distributed"]);
        
		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

        $grand_total["comm_amount_sum"] = $query->sum(DB::raw("CASE WHEN comm.type != 2001 THEN comm.amount ELSE 0 END"));
        $grand_total["comm_roi_amount_sum"] = $query->sum(DB::raw("CASE WHEN comm.type = 2001 THEN comm.amount ELSE 0 END"));

        $query = $query->groupBy(DB::raw("CASE WHEN primary_user.id IS NULL THEN user.id ELSE primary_user.id END"));
		$query = $query->select([
			DB::raw('CASE WHEN primary_user.id IS NULL THEN user.id ELSE primary_user.id END AS user_id'),
			DB::raw('CASE WHEN primary_user.id IS NULL THEN user.username ELSE primary_user.username END AS user_username'),
			DB::raw('CASE WHEN primary_user.id IS NULL THEN user.name ELSE primary_user.name END AS user_name'),
			DB::raw('CASE WHEN primary_user.id IS NULL THEN user.nid ELSE primary_user.nid END AS user_nid'),
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END AS user_country'),
			DB::raw('CASE WHEN primary_user.id IS NULL THEN user.created_at ELSE primary_user.created_at END AS user_created_at'),
			DB::raw('SUM(CASE WHEN comm.type != 2001 THEN comm.amount ELSE 0 END) AS comm_amount_sum'),
			DB::raw('SUM(CASE WHEN comm.type = 2001 THEN comm.amount ELSE 0 END) AS comm_roi_amount_sum'),
			DB::raw('MAX(comm.bdate) AS comm_bdate_last'),
		]);

		$datatables = Datatables::of($query);

        $total_sales_query = Activation::where("status", Activation::$status_to_code["confirmed"])
                        ->where("b_status", Activation::$b_status_to_code["confirmed"]);

        if (request()->has("date_from")) {
            $date_from = request()->get("date_from");
            $total_sales_query = $total_sales_query->where("bonus_at", ">=", DateTimeTool::operationToSystemDateTime($date_from." 00:00:00"));
        }

        if (request()->has("date_to")) {
            $date_to = request()->get("date_to");
            $total_sales_query = $total_sales_query->where("bonus_at", "<=", DateTimeTool::operationToSystemDateTime($date_to." 23:59:59"));
        }

    	$total_sales = $total_sales_query->sum("price");

		if ($total_sales > 0) {
			$grand_total["comm_amount_sum_percent"] = $grand_total["comm_amount_sum"] / $total_sales * 100;
		}
		else {
			$grand_total["comm_amount_sum_percent"] = 100;
		}

		$datatables = $datatables->with([
			'grand_total' => [
				'comm_amount_sum' => e(number_format($grand_total["comm_amount_sum"], 4)),
				'comm_amount_sum_percent' => e(number_format($grand_total["comm_amount_sum_percent"], 4)."%"),
				'comm_roi_amount_sum' => e(number_format($grand_total["comm_roi_amount_sum"], 4)),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();

		return $datatables
                ->editColumn('user_id', function ($list){
                    return $list["user_id"];
                }) 
                ->editColumn('user_username', function ($list){
                    return $list["user_username"];
                }) 
                ->editColumn('user_username', function ($list){
                    return $list["user_username"];
                }) 
                ->editColumn('user_name', function ($list){
                    return $list["user_name"];
                }) 
                ->editColumn('user_nid', function ($list){
                    return $list["user_nid"];
                }) 
				->editColumn('user_country', function ($list) use ($country_code_to_name) {
					return isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-";
				})
                ->editColumn('user_created_at', function ($list){
                    return DateTimeTool::systemToOperationDateTime($list["user_created_at"]);
                }) 
                ->editColumn("comm_amount_sum", function ($list) {
                    return number_format($list["comm_amount_sum"], 4);
                })
                ->editColumn("comm_roi_amount_sum", function ($list) {
                    return number_format($list["comm_roi_amount_sum"], 4);
                })
                ->editColumn("comm_bdate_last", function ($list) {
                    return $list["comm_bdate_last"];
                })
                ->addColumn("comm_amount_sum-percent", function ($list) use ($total_sales) {
                    
                    if ($total_sales > 0 ) {
                        $return = $list["comm_amount_sum"] / $total_sales * 100;
                    }
                    else {
                        $return = 100;
                    }

                    return number_format($return, 4)."%";
                })
                ->addColumn("act_amount-sum", function ($list) {

                    $query = new Activation();
		            $query->setTable($query->getTable().' AS act');
		            $query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid');
                    $query = $query->where("act.status", Activation::$status_to_code["confirmed"])
                        ->where("act.b_status", Activation::$b_status_to_code["confirmed"])
                        ->whereRaw(DB::raw("CASE WHEN user.primary_acc = ? THEN user.id ELSE user.primary_id END = ?"), [Member::$primary_to_code["yes"], $list["user_id"]]);
   
                    if (request()->has("date_from")) {
                        $date_from = request()->get("date_from");
                        $query = $query->where("bonus_at", ">=", DateTimeTool::operationToSystemDateTime($date_from." 00:00:00"));
                    }

                    if (request()->has("date_to")) {
                        $date_to = request()->get("date_to");
                        $query = $query->where("bonus_at", "<=", DateTimeTool::operationToSystemDateTime($date_to." 23:59:59"));
                    }

                    $return = $query->sum("act.price");
                    return number_format($return ? $return : 0, 4) ;
                })
                ->addColumn("withdrawal_pending_amount-sum", function ($list) {
                    
                    $query = Withdrawal::where("uid", $list["user_id"])
                        ->where("status", Withdrawal::$status_to_code["pending"]);
   
                    if (request()->has("date_from")) {
                        $date_from = request()->get("date_from");
                        $query = $query->where("created_at", ">=", DateTimeTool::operationToSystemDateTime($date_from." 00:00:00"));
                    }

                    if (request()->has("date_to")) {
                        $date_to = request()->get("date_to");
                        $query = $query->where("created_at", "<=", DateTimeTool::operationToSystemDateTime($date_to." 23:59:59"));
                    }

                    $return = $query->sum("receivable_amount");

                    return number_format($return ? $return : 0, 2) ;
                })
                ->addColumn("withdrawal_paid_amount-sum", function ($list) {
                    
                    $query = Withdrawal::where("uid", $list["user_id"])
                        ->where("status", Withdrawal::$status_to_code["paid"]);
   
                    if (request()->has("date_from")) {
                        $date_from = request()->get("date_from");
                        $query = $query->where("created_at", ">=", DateTimeTool::operationToSystemDateTime($date_from." 00:00:00"));
                    }

                    if (request()->has("date_to")) {
                        $date_to = request()->get("date_to");
                        $query = $query->where("created_at", "<=", DateTimeTool::operationToSystemDateTime($date_to." 23:59:59"));
                    }

                    $return = $query->sum("receivable_amount");

                    return number_format($return ? $return : 0, 2) ;
                })
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.commission.analysis');

		//search fields
		$search_fields = $this->getSearchAttributes();

        return $view->with(['__search_fields'=>$search_fields]);
    }
}
