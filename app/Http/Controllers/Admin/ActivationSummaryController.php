<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Models\Activation;
use App\Models\Admin;
use App\Models\Member;
use App\Repositories\CountryRepo;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class ActivationSummaryController extends Controller
{
	protected $countryRepo;

	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'code' => ['label'=>trans('field.activation.code-name'),
				'options'=>[
					'' => trans("general.selection.all"), 
					'1100' => trans('general.activation.title.1100'), 
					'2100' => trans('general.activation.title.2100'),
					'3100' => trans('general.activation.title.3100'),
					'4100' => trans('general.activation.title.4100'),
					'5100' => trans('general.activation.title.5100'),
				], 
				'value'=>''
			],
			'uid' => ['search'=>'=', 'table'=>'act', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'country' => ['search'=>'raw', 'label'=>trans('field.user.country'), 'as'=>"(CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END) = ?",
				'options' => ["" => trans("general.selection.all")] + $this->countryRepo->all_member_selectable_country()->toArray(),
				'value'=>''
			],
			'act_type' => ['search'=>'in', 'table'=>'act', 'label'=>trans('field.activation.act_type'), 
				'options'=>[
					'10' => trans('general.activation.act_type.10'), 
					'30' => trans('general.activation.act_type.30'),
					'70' => trans('general.activation.act_type.70'),
					'50' => trans('general.activation.act_type.50'),
					'90' => trans('general.activation.act_type.90'),
				], 
				'value'=>[]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'bonus_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'act', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('general.table.header.date')]), 'value'=>"", 'as'=>'bonus_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Activation;
		$query->setTable($query->getTable().' AS act');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid');
		$query = $query->leftjoin(with(new Member)->getTable().' AS primary_user', 'user.primary_id', '=', 'primary_user.id');

        $query = $query->where("act.status", Activation::$status_to_code["confirmed"])
            ->where("act.b_status", Activation::$b_status_to_code["confirmed"]);

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);

		$query = $query->select([
            "act.code",
            DB::raw("MAX(act.bonus_at) AS bonus_at_last"),
            DB::raw("COUNT(act.id) AS act_count"),
            DB::raw("SUM(act.use_rwallet) AS use_rwallet_sum"),
            DB::raw("SUM(act.use_awallet) AS use_awallet_sum"),
            // DB::raw("SUM(act.use_awallet2) AS use_awallet2_sum"),
            DB::raw("SUM(act.use_fwallet) AS use_fwallet_sum"),
            DB::raw("SUM(act.use_dowallet) AS use_dowallet_sum"),
            DB::raw("SUM(act.get_wallet1) AS get_wallet1_sum"),
            DB::raw("SUM(act.get_wallet2) AS get_wallet2_sum"),
            DB::raw("SUM(act.amount) AS amount_sum"),
            DB::raw("SUM(act.price) AS price_sum"),
		])
        ->groupBy("act.code")
        ->orderBy("act.code");

		$datatables = Datatables::of($query);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('code', function ($list) {
					return trans("general.activation.title.".$list["code"]);
				})
				->editColumn('bonus_at_last', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['bonus_at_last']);
				})
				->editColumn('act_count', function ($list) {
					return number_format($list["act_count"]);
				})
				->editColumn('price_sum', function ($list) {
					return number_format($list["price_sum"], 2);
				})
				->editColumn('amount_sum', function ($list) {
					return number_format($list["amount_sum"], 2);
				})
				->editColumn('use_rwallet_sum', function ($list) {
					return number_format($list["use_rwallet_sum"], 2);
				})
				->editColumn('use_awallet_sum', function ($list) {
					return number_format($list["use_awallet_sum"], 2);
				})
				// ->editColumn('use_awallet2_sum', function ($list) {
				// 	return number_format($list["use_awallet2_sum"], 2));
				// })
				->editColumn('use_fwallet_sum', function ($list) {
					return number_format($list["use_fwallet_sum"], 2);
				})
				->editColumn('use_dowallet_sum', function ($list) {
					return number_format($list["use_dowallet_sum"], 2);
				})
				->editColumn('get_wallet1_sum', function ($list) {
					return number_format($list["get_wallet1_sum"], 2);
				})
				->editColumn('get_wallet2_sum', function ($list) {
					return number_format($list["get_wallet2_sum"], 2);
				})
            ->make(true);
	}

	public function ajaxGetSubDataTable()
	{
		$query = new Activation;
		$query->setTable($query->getTable().' AS act');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid');
		$query = $query->leftjoin(with(new Member)->getTable().' AS primary_user', 'user.primary_id', '=', 'primary_user.id');
		$query = $query->leftJoin(with(new Member)->getTable().' AS act_user', 'act.aid', '=', 'act_user.id');

        $query = $query->where("act.status", Activation::$status_to_code["confirmed"])
            ->where("act.b_status", Activation::$b_status_to_code["confirmed"]);

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(act.use_rwallet) AS sum_use_rwallet"),
			DB::raw("SUM(act.use_awallet) AS sum_use_awallet"),
			// DB::raw("SUM(act.use_awallet2) AS sum_use_awallet2"),
			DB::raw("SUM(act.use_fwallet) AS sum_use_fwallet"),
			DB::raw("SUM(act.use_dowallet) AS sum_use_dowallet"),
			DB::raw("SUM(act.get_dwallet) AS sum_get_dwallet"),
			DB::raw("SUM(act.get_wallet1) AS sum_get_wallet1"),
			DB::raw("SUM(act.get_wallet2) AS sum_get_wallet2"),
			DB::raw("SUM(act.price) AS sum_price"),
			DB::raw("SUM(act.amount) AS sum_amount"),
		])->get();

		$use_rwallet_sum = $total_query[0]["sum_use_rwallet"] ? : 0;
		$use_awallet_sum = $total_query[0]["sum_use_awallet"] ? : 0;
		// $use_awallet2_sum = $total_query[0]["sum_use_awallet2"] ? : 0;
		$use_fwallet_sum = $total_query[0]["sum_use_fwallet"] ? : 0;
		$use_dowallet_sum = $total_query[0]["sum_use_dowallet"] ? : 0;
		$get_dwallet_sum = $total_query[0]["sum_get_dwallet"] ? : 0;
		$get_wallet1_sum = $total_query[0]["sum_get_wallet1"] ? : 0;
		$get_wallet2_sum = $total_query[0]["sum_get_wallet2"] ? : 0;
		$price_sum = $total_query[0]["sum_price"] ? : 0;
		$amount_sum = $total_query[0]["sum_amount"] ? : 0;

		$query = $query->select([
			'act.*', 
			'user.username AS user_username',
			'user.name AS user_name',
			'user.nid AS user_nid',
			DB::raw('CASE WHEN primary_user.id IS NOT NULL THEN primary_user.country ELSE user.country END AS user_country'),
			'act_user.username AS act_user_username',
			'act_user.name AS act_user_name',
		]);

		$datatables = Datatables::of($query);

		$datatables = $datatables->with([
			'grand_total' => [
				'use_rwallet' => e(number_format($use_rwallet_sum, 2)),
				'use_awallet' => e(number_format($use_awallet_sum, 2)),
				// 'use_awallet2' => e(number_format($use_awallet2_sum, 2)),
				'use_fwallet' => e(number_format($use_fwallet_sum, 2)),
				'use_dowallet' => e(number_format($use_dowallet_sum, 2)),
				'get_dwallet' => e(number_format($get_dwallet_sum, 2)),
				'get_wallet1' => e(number_format($get_wallet1_sum, 2)),
				'get_wallet2' => e(number_format($get_wallet2_sum, 2)),
				'price' => e(number_format($price_sum, 2)),
				'amount' => e(number_format($amount_sum, 2)),
			]
		]);

		$country_code_to_name = $this->countryRepo->all_member_selectable_country()->toArray();
    	
		return $datatables
				->editColumn('created_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['created_at']);
				})
				->editColumn('id', function ($list) {
					return $list['id'];
				})
				->editColumn('uid', function ($list) {
					return $list['uid'];
				})
				->addColumn('uid-username', function ($list) {
					return $list["user_username"];
				})
				->addColumn('uid-name', function ($list) {
					return $list["user_name"];
				})
				->addColumn('uid-nid', function ($list) {
					return $list["user_nid"];
				})
				->addColumn('uid-country', function ($list) use ($country_code_to_name) {
					return isset($country_code_to_name[$list["user_country"]]) ? $country_code_to_name[$list["user_country"]] : "-";
				})
				->editColumn('aid', function ($list) {
					return $list["aid"] ? $list["aid"] : "-";
				})
				->addColumn('aid-username', function ($list) {
					return $list["act_user_username"] ? $list["act_user_username"] : "-";
				})
				->addColumn('aid-name', function ($list) {
					return $list["act_user_name"] ? $list["act_user_name"] : "-";
				})
				->editColumn('code', function ($list) {
					return trans("general.activation.title.".$list["code"]);
				})
				->editColumn('price', function ($list) {
					return number_format($list["price"], 2);
				})
				->editColumn('amount', function ($list) {
					return number_format($list["amount"], 2);
				})
				->editColumn('use_rwallet', function ($list) {
					return number_format($list["use_rwallet"], 2);
				})
				->editColumn('use_awallet', function ($list) {
					return number_format($list["use_awallet"], 2);
				})
				// ->editColumn('use_awallet2', function ($list) {
				// 	return e(number_format($list["use_awallet2"], 2));
				// })
				->editColumn('use_fwallet', function ($list) {
					return number_format($list["use_fwallet"], 2);
				})
				->editColumn('use_dowallet', function ($list) {
					return number_format($list["use_dowallet"], 2);
				})
				->editColumn('get_dwallet', function ($list) {
					return number_format($list["get_dwallet"], 2);
				})
				->editColumn('get_wallet1', function ($list) {
					return number_format($list["get_wallet1"], 2);
				})
				->editColumn('get_wallet2', function ($list) {
					return number_format($list["get_wallet2"], 2);
				})
				->editColumn('act_type', function ($list) {
					return trans("general.activation.act_type.".$list["act_type"]);
				})
				->editColumn('status', function ($list) {
					return trans("general.activation.status.".$list["status"]);
				})
				->editColumn('bonus_at', function ($list) {
					return DateTimeTool::systemToOperationDateTime($list['bonus_at'] > 0 ? $list['bonus_at'] : null);
				})
				->editColumn('itype', function ($list) {
					if ($list["itype"] == "900") {
						$return = trans("system.admin");
					} elseif ($list["itype"] == "100") {
						$return = trans("system.member");
					} else {
						$return = "-";
					}
					return $return;
				})
				->addColumn('iid-text', function ($list) {
					$return = "-";
					if ($list["itype"] == "900") {
						$user = Admin::find($list["iid"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["itype"] == "100") {
						$user = Member::find($list["iid"]);
						$return = $user ? $user->username : "-";
					}
					return $return;
				})
            ->make(true);
	}

    public function index()
    {
        $view = view("admin.activation.summary");
		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }
}
