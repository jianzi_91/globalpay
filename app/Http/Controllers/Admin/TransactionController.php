<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\TransactionLog;
use App\Models\Member;
use App\Models\MemberProfile;
use App\Models\MemberWalletTransfer;

use Yajra\Datatables\Datatables;
use App\Utils\DateTimeTool;

class TransactionController extends Controller
{
    public function index()
    {
        $transcode_list = TransactionLog::transcodeType();
    	return view('admin.member.transaction-history')->with('transcode_list', $transcode_list);
    }

    public function ajaxGetDataTable()
    {
    	$name = request('name');
    	$mobile = request('mobile');
        $from = request('from');
        $to = request('to');
        $transcode = request('transcode');

        $trans_log_model = new TransactionLog;
        $member_tbl = new Member;

        $query = $trans_log_model;
        $query->setTable($query->getTable().' AS wl');
        $query = $query->leftJoin(with($member_tbl)->getTable().' AS u', 'u.id', '=', 'wl.uid');

        $query = $query->select([
			'wl.*',
			"u.id AS member_id",
            "u.name AS member_name",
            "u.mobile as member_mobile"
		]);

        $query = $query->where(function($query) use ($from, $to, $name, $mobile, $transcode) {
            if ($from) {
                $query->where('wl.created_at', '>=', DateTimeTool::getSystemStartDateTime($from, 'Y-m-d'));
            }
            
            if ($to) {
                $query->where('wl.created_at', '<=', DateTimeTool::getSystemEndDateTime($to, 'Y-m-d'));
            }

            if ($name) {
        		$query->where('u.name', 'LIKE', '%'.$name.'%');
            }

            if ($mobile) {
            	$query->where('u.mobile', $mobile);
            }

            if ($transcode) {
                $query->whereIn('wl.trans_code', $transcode);
            }
        })
        ->orderBy('wl.created_at', 'desc');
                                           
        return Datatables::of($query)
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at'], 'Y-m-d H:i'));
				})
				->editColumn('name', function ($list) {
					return $list->member_name;
				})
                ->editColumn('description', function ($list) {
                    $creator = Member::find($list['creator_uid']);
                    return $list->getDescription();
				})
                ->editColumn('awallet', function ($list) {
                    return number_format($list['a_amt'],2).'<br>---------------<br>'.number_format($list['a_balance'],2);
                })
                ->editColumn('fwallet', function ($list) {
                    return number_format($list['f_amt'],2).'<br>---------------<br>'.number_format($list['f_balance'],2);
                })
                ->editColumn('dowallet', function ($list) {
					return number_format($list['do_amt'],2).'<br>---------------<br>'.number_format($list['do_balance'],2);
                })
                ->editColumn('sc_wallet', function ($list) {
					return number_format($list['sc_amt'],2).'<br>---------------<br>'.number_format($list['sc_balance'],2);
                })
                ->rawColumns(['description', 'awallet', 'fwallet', 'dowallet', 'sc_wallet'])
                ->make(true);
    }
}
