<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SaveWallet;

use App\Repositories\MemberRepo;

class HomeController extends Controller 
{
    public function __construct(MemberRepo $memberRepo) 
    {
        parent::__construct();
        
        $this->member = $memberRepo;
    }

    public function index()
    {
        return redirect($this->admin_slug.'/home');
    }

    public function home()
    {
        return view('admin.home');
    }
}
