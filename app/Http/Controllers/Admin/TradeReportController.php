<?php
namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Admin\Controller;
use App\Trades\TradeService;
use App\Models\MemberTrade;
use App\Models\Member;

use Yajra\Datatables\Datatables;

class TradeReportController extends Controller
{
	protected $tradeService;

	public function __construct(TradeService $tradeService)
	{
        $this->tradeService = $tradeService;
        
        parent::__construct();
	}

	public function ajaxGetTradeReportDataTable()
	{
        // build result array
        $user_types = ["Company", "Member"];
        $collect_array = []; 
        $trade_price = $this->tradeService->getCurrentMarketPrice();
        $sum_unit = 0;

		for ($i = 0; $i < count($user_types); $i++) {
            $query = new Member;
            $query->setTable($query->getTable().' AS user');
            $query = $query->join(with(new MemberTrade)->getTable().' AS trade', 'user.id', '=', 'trade.uid');
            if ($user_types[$i] == "Company") {
                $query = $query->where("user.company_status", Member::$company_status_to_code["yes"]);
            }
            else {
                $query = $query->where("user.company_status", Member::$company_status_to_code["no"]);
            }
            $query = $query->select(DB::raw("SUM(trade.unit_held) as sum_unit"));
			$rs = $query->first();
			
            $collect_array[] = [
				'user_type' => $user_types[$i],
                'sum_unit' => $rs->sum_unit,
                'sum_value' => $rs->sum_unit * $trade_price,
			];
            $sum_unit += $rs->sum_unit;
        }

        $collect_array[0]["ratio"] = ($sum_unit ? $collect_array[0]["sum_unit"] / $sum_unit * 100 : 0);
        $collect_array[1]["ratio"] = ($sum_unit ? $collect_array[1]["sum_unit"] / $sum_unit * 100 : 0);

        // build collection
		$collect = collect($collect_array);

		// result build
		$datatables = Datatables::of($collect);

		// process all showing data 
		return $datatables->editColumn('user_type', function ($list) {
                    return e($list['user_type'] == "Company" ?  trans("general.page.admin.trade-overview.content.company") : trans("general.page.admin.trade-overview.content.member"));
				})
				->editColumn('sum_unit', function ($list) {
                    return e(number_format($list['sum_unit']));
				})
				->editColumn('sum_value', function ($list) {
                    return e(number_format($list['sum_value'], 3));
				})
				->editColumn('ratio', function ($list) {
                    return e(number_format($list['ratio'], 2)." %");
				})
                ->with([
                    "trade_price" => e(number_format($trade_price, 3))
                ])
            ->make(true);
	}

	public function ajaxGetCompanyAccountReportDataTable()
	{
        
        // build query
        $query = new Member;
        $query->setTable($query->getTable().' AS user');
        $query = $query->leftJoin(with(new MemberTrade)->getTable().' AS trade', 'user.id', '=', 'trade.uid');
        $query = $query
            ->where("user.company_status", Member::$company_status_to_code["yes"])
            ->select("user.username", "user.id", "user.name", "trade.unit_held");

		// result build
		$datatables = Datatables::of($query);

		// process all showing data 
		return $datatables->editColumn('username', function ($list) {
                    return e($list['username']);
				})
				->editColumn('id', function ($list) {
                    return e($list['id']);
				})
				->editColumn('name', function ($list) {
                    return e($list['name']);
				})
				->editColumn('unit_held', function ($list) {
                    return e(number_format($list['unit_held']));
				})
            ->make(true);
	}

    public function tradeReport()
    {
        //return view
		$view = view('admin.report.trade-allocation');

        return $view;
    }
}
