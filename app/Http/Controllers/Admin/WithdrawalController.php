<?php

namespace App\Http\Controllers\Admin;

use DB;
use Exception;

use App\Models\Withdrawal;
use App\Models\Admin;
use App\Models\Member;
use App\Models\Logger;
use App\Models\TransInfo;
use App\Repositories\CountryRepo;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class WithdrawalController extends Controller
{
    protected $countryRepo;

	//Identifiers
	protected $identifiers = [];

	public function __construct(CountryRepo $countryRepo)
	{
		$this->countryRepo = $countryRepo;

		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'like', 'table'=>'withdrawal', 'label'=>trans('field.withdrawal.id'), 'value'=>''],
			"uid" => ['search'=>'=', 'table'=>'withdrawal', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'status' => ['search'=>'in', 'table'=>'withdrawal', 'label'=>trans('field.withdrawal.status'), 
				'options'=>[
					'10' => trans('general.withdrawal.status.10'), 
					'30' => trans('general.withdrawal.status.30'),
					'50' => trans('general.withdrawal.status.50'),
					'70' => trans('general.withdrawal.status.70'),
				], 
				'value'=>["30", "50"]
			],
			
			'date_from' => ['search'=>'>=', 'type'=>'date', 'table'=>'withdrawal', 'label'=>trans('general.search_field.field.date-from', ['field'=>trans('field.withdrawal.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'from'],

			'date_to' => ['search'=>'<=', 'type'=>'date', 'table'=>'withdrawal', 'label'=>trans('general.search_field.field.date-to', ['field'=>trans('field.withdrawal.created_at')]), 'value'=>'', 'as'=>'created_at', 'role'=>'to']
		];
	}

	public function ajaxGetDataTable()
	{
		$query = new Withdrawal;
		$query->setTable($query->getTable().' AS withdrawal');
		$query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'withdrawal.uid');

		$search_fields = $this->getSearchAttributes();
		$query = $this->buildORMFilter($query, $search_fields);
		
		$total_query = $query->select([
			DB::raw("SUM(withdrawal.amount) AS sum_amount"),
			DB::raw("SUM(withdrawal.receivable_amount) AS sum_receivable_amount"),
			DB::raw("SUM(withdrawal.fee) AS sum_fee"),
			DB::raw("SUM(withdrawal.currency_amount) AS sum_currency_amount"),
			DB::raw("SUM(withdrawal.currency_receivable_amount) AS sum_currency_receivable_amount"),
			DB::raw("SUM(withdrawal.currency_fee) AS sum_currency_fee"),
		])->get();
		$sum_amount = $total_query[0]["sum_amount"] ? : 0;
		$sum_receivable_amount = $total_query[0]["sum_receivable_amount"] ? : 0;
		$sum_fee = $total_query[0]["sum_fee"] ? : 0;
		$sum_currency_amount = $total_query[0]["sum_currency_amount"] ? : 0;
		$sum_currency_receivable_amount = $total_query[0]["sum_currency_receivable_amount"] ? : 0;
		$sum_currency_fee = $total_query[0]["sum_currency_fee"] ? : 0;

		// result build
		$query = $query->select([
			'withdrawal.*',
			"user.username AS user_username",
			"user.name AS user_name",
		]);

		$datatables = Datatables::of($query);
		
		$datatables = $datatables->with([
			'grand_total' => [
				'amount' => e(number_format($sum_amount, 2)),
				'receivable_amount' => e(number_format($sum_receivable_amount, 2)),
				'fee' => e(number_format($sum_fee, 2)),
				'currency_amount' => e(number_format($sum_currency_amount, 2)),
				'currency_receivable_amount' => e(number_format($sum_currency_receivable_amount, 2)),
				'currency_fee' => e(number_format($sum_currency_fee, 2)),
			]
		]);
        
		$country_text = $this->countryRepo->all_member_selectable_country();
    	
		return $datatables
				->addColumn('multi-select-box', function ($list) {
					$disabled = null;
					if (in_array($list->status, [Withdrawal::$status_to_code["cancelled"], Withdrawal::$status_to_code["paid"]])) {
						return "";
					}
					return "<input type='checkbox' name='withdrawal_ids' value='".$list->id."' ".$disabled.">";
				})
				->editColumn('uid', function ($list) {
					return e($list ? $list->uid : "-");
				})
				->editColumn('user_username', function ($list) {
					return e($list ? $list->user_username : "-");
				})
				->editColumn('user_name', function ($list) {
					return e($list ? $list->user_name : "-");
				})
				->editColumn('id', function ($list) {
					return e($list["id"]);
				})
				->editColumn('bank_country', function ($list) use ($country_text) {
					return e(isset($country_text[$list["bank_country"]]) ? $country_text[$list["bank_country"]] : "-");
				})
				->editColumn('bank_name', function ($list) {
					return e($list["bank_name"]);
				})
				->editColumn('bank_branch_name', function ($list) {
					return e($list["bank_branch_name"]);
				})
				->editColumn('bank_payee_name', function ($list) {
					return e($list["bank_payee_name"]);
				})
				->editColumn('bank_acc_no', function ($list) {
					return e($list["bank_acc_no"]);
				})
				->editColumn('bank_sorting_code', function ($list) {
					return e($list["bank_sorting_code"]);
				})
				->editColumn('bank_iban', function ($list) {
					return e($list["bank_iban"]);
				})
				->editColumn('receivable_amount', function ($list) {
					return e(number_format($list["receivable_amount"], 2));
				})
				->editColumn('amount', function ($list) {
					return e(number_format($list["amount"], 2));
				})
				->editColumn('fee', function ($list) {
					return e(number_format($list["fee"], 2));
				})
				->editColumn('currency', function ($list) {
					return e($list["currency"]?$list["currency"]:"-");
				})
				->editColumn('currency_amount', function ($list) {
					return e(number_format($list["currency_amount"], 2));
				})
				->editColumn('currency_receivable_amount', function ($list) {
					return e(number_format($list["currency_receivable_amount"], 2));
				})
				->editColumn('currency_fee', function ($list) {
					return e(number_format($list["currency_fee"], 2));
				})
				->editColumn('remarks', function ($list) {
					return nl2br(e($list["remarks"]));
				})
				->editColumn('aremarks', function ($list) {
					return nl2br(e($list["aremarks"]));
				})
				->editColumn('action', function ($list) {
					if ($this->user->can("admin-privilege", "withdrawals/edit"))
						if ($list['status'] == Withdrawal::$status_to_code['pending'] || $list['status'] == Withdrawal::$status_to_code['processing']) {
							return "<a href='".url($this->admin_slug.'/withdrawals/'.$list["id"].'/edit')."' class='btn btn-white btn-sm'><i class='fa fa-pencil'></i>".e(trans('general.button.edit'))."</a>";
						}
					else
						return "";
				})
				->editColumn('status', function ($list) {
					return e(trans("general.withdrawal.status.".$list["status"]));
				})
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->addColumn('editor_id-text', function ($list) {
					$return = "-";
					if ($list["editor_type"] == "900") {
						$user = Admin::select(["name"])->find($list["editor_id"]);
						$return = $user ? $user->name : "-";
					} elseif ($list["editor_type"] == "100") {
						$return = "-";
					}
					return e($return);
				})
				->rawColumns(['multi-select-box', 'action'])
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.withdrawal.list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }

    public function editWithdrawal($id)
    {	
		// dump(back()->getTargetUrl());
		//initiator
        $withdrawal = Withdrawal::findOrfail($id);
        $member = Member::findOrfail($withdrawal->uid);
		$view = view('admin.withdrawal.edit');
		$redirect = redirect($this->admin_slug."/withdrawals");
		
		//options data
		$fields['status']['options'] = array_values(Withdrawal::$status_to_code);
		$fields['status']['to_code'] = Withdrawal::$status_to_code;
		$country_text = $this->countryRepo->all_member_selectable_country();

        if (request()->isMethod('post') && request()->has('__req')) {
            $data = request()->all();
			
			//withdrawal
			if ($data['__req'] == '1') {
				$niceNames = [
					'aremarks' => trans('field.withdrawal.aremarks'),
					'status' => trans('field.withdrawal.status'),
				];

				$data = request()->only(array_keys($niceNames));
				
				//validation part 1
				$v = \Validator::make($data, [
                                        'aremarks' => 'string|max:2000',
										'status' => 'required|in:'.implode(',', $fields['status']['options']),
									], [], $niceNames);
				if ($v->fails())
				   return back()->withInput()->withErrors($v);
				
				//validation part 2
				if ($data["status"] == Withdrawal::$status_to_code["cancelled"]) {
					$v = \Validator::make($data, [
											'aremarks' => 'required',
										], [], $niceNames);
					if ($v->fails())
					return back()->withInput()->withErrors($v);
				}

				//if withdrawal is not complete then update
				if (!in_array($withdrawal->status, [Withdrawal::$status_to_code["paid"], Withdrawal::$status_to_code["cancelled"]])) {
					try {
						$this->edit($withdrawal->id, [
							"status" => $data["status"], 
							"aremarks" => $data["aremarks"]
						]);
						
						//dd($member->toArray());
						return $redirect->withSuccess('Withdrawal updated.');
					} catch (Exception $e) {
						// ignore
						throw $e;
					}
				}
			}
        }
		
		//build display data
		$withdrawal["bank_country-text"] = isset($country_text[$withdrawal["bank_country"]])?$country_text[$withdrawal["bank_country"]]:"-";
		$withdrawal['created_at_op'] = DateTimeTool::systemToOperationDateTime($withdrawal['created_at']);
		$withdrawal['updated_at_op'] = DateTimeTool::systemToOperationDateTime($withdrawal['updated_at']);
		$withdrawal['paid_at_op'] = DateTimeTool::systemToOperationDateTime($withdrawal['paid_at']);
		
		//initiator
		switch ($withdrawal['editor_type']) {
			case Admin::$user_code:
				$user = Admin::find($withdrawal["editor_id"]);
				$withdrawal['editor_id-text'] = "(".trans("system.admin").")";
				break;
			case Member::$user_code:
				$user = Member::find($withdrawal["editor_id"]);
				$withdrawal['editor_id-text'] = "(".trans("system.member").")";
				break;
			default:
				$withdrawal['editor_id-text'] = '';
		}
		if ($user) {
			$withdrawal['editor_id-text'] = $user->username." ".$withdrawal['editor_id-text'];
		}
		else {
			$withdrawal['editor_id-text'] .= "-";
		}
		unset($user);

        return $view->with(['__fields'=>$fields, "__withdrawal"=>$withdrawal, "__user"=>$member]);
	}
	
	public function paidMultiWithdrawal()
	{
		$withdrawal_ids = Withdrawal::whereIn("id", request()->get('withdrawal_ids'))
			->whereNotIn("status", [Withdrawal::$status_to_code["paid"], Withdrawal::$status_to_code["cancelled"]])
			->select("id")
			->pluck("id")
			->toArray();
		
		$success_count = 0;
		$fail_count = 0;
		foreach ($withdrawal_ids as $withdrawal_id) {
			try {
				$this->edit($withdrawal_id, [
					"status" => Withdrawal::$status_to_code["paid"], 
				]);
				$success_count ++;
			} catch (Exception $e) {
				$fail_count ++;
			}
		}

		if ($success_count > 0) {
			$message = $success_count.' Withdrawal updated. ' .($fail_count > 0 ? $fail_count.' Withdrawal failed to be updated.':"");
			return back()->withSuccess($message);
		}
		elseif ($fail_count > 0) {
			$message = $fail_count.' Withdrawal failed to be updated.';
			return back()->withErrors(collect([
				"all" => $message
			]));
		}
		else {
			return back();
		}
	}

	public function edit($withdrawal_id, $withdrawal_data = [])
	{
		$_method =  __METHOD__;
		$initiator = Tool::getInitiator();

		try {
			DB::beginTransaction();

			$withdrawal = Withdrawal::findOrFail($withdrawal_id);
			$member = Member::findOrFail($withdrawal->uid);

			$update_data = [
				"editor_type" => $initiator['itype'],
				"editor_id" => $initiator['iid'],
				"aremarks" => isset($withdrawal_data['aremarks']) ? $withdrawal_data['aremarks'] : $withdrawal->aremarks,
				"status" => $withdrawal_data['status'],
			];
			if ($withdrawal_data["status"] == Withdrawal::$status_to_code["paid"]) {
				// update to paid
				$datetime = DateTimeTool::systemDateTime();
				$update_data["paid_at"] = $datetime;
				$update_data["paid_by_type"] = $initiator['itype'];
				$update_data["paid_by_id"] = $initiator['iid'];
			}

			$proceed = Withdrawal::where("id", $withdrawal_id)
				->whereNotIn("status", [Withdrawal::$status_to_code["paid"], Withdrawal::$status_to_code["cancelled"]])
				->update($update_data);
			
			if (!$proceed) {
				throw new Exception("Error while updating withdrawal.");
			}
				
			if ($withdrawal_data["status"] == Withdrawal::$status_to_code["cancelled"]) {
				//refund to member
				$descr = "Cancelled Withdrawal ID #".$withdrawal->id.", Amount".$withdrawal->amount." with Fee ".$withdrawal->fee." for User ID #".$member->id.".";
				$descr_json = [
					'wallet' => $withdrawal->wallet,
					'amount' => $withdrawal->amount,
					'uid' => $member->id ,
					'username' => $member->username,
					'withdrawal-id' => $withdrawal->id,
					'trans-type' => 'refund',
				]; 
	
				$result = $member->addWallet($member->id, $withdrawal->wallet, $withdrawal->amount, $descr, $descr_json, TransInfo::$trans_type_to_code['wallet-withdraw']);
				if (!$result['status']) {
					throw new Exception("Error while adding user wallet.");
				}
			}
			DB::commit();
			
			//Log Action
			$this->setIdentifiers($withdrawal->id);
			$this->setIdentifiers($withdrawal->uid);
			$log_msg = "Withdrawal ID #".$withdrawal->id." updated for User ID #".$member->id.".";
			Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>$withdrawal_data, 'uid'=>$withdrawal->uid]);

		} catch (Exception $e) {
			DB::rollBack();
			throw $e;
		}
		
	}
}
