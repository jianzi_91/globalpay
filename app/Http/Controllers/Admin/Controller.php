<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Http\Controllers\Controller as BaseController;
use App\Utils\DateTimeTool;
use App\Utils\Tool;

class Controller extends BaseController
{
	protected $guard = "admins";
	protected $user = null;
	protected $admin_slug;

	public function __construct()
	{
		Tool::setInitiator($this->guard);
        $this->user = auth($this->guard)->user();
		$this->admin_slug = config('app.admin_slug');
    }
	
    public function buildORMFilter($query, $search_fields)
    {
		// restructure $search_fields array for date range capability
		foreach ($search_fields as $key => $item) {
			if (isset($item['type']) && $item['type'] == 'date_range') {
				$search_fields = array_add($search_fields, $key.'_from', $item['date_from']);
				$search_fields = array_add($search_fields, $key.'_to', $item['date_to']);
			}
		}

		$this->data = request()->only(array_keys($search_fields));

		foreach ($search_fields as $_name => $_detail) {
			if (!isset($this->data[$_name])) {
				continue;
			}

			$value = $this->data[$_name];
			$search_type = isset($_detail['search']) && $_detail['search'] ? strtolower($_detail['search']) : false;

			if ($value && $search_type != "ignore") {
				$value_type = (isset($_detail['type']) && $_detail['type'] ? $_detail['type'] : "");
				$value_role = (isset($_detail['role']) && $_detail['role'] ? $_detail['role'] : "");

				$field_name = (isset($_detail['table']) && $_detail['table'] ? $_detail['table']."." : "");
				if ($search_type != 'or-like')
					$field_name .= (isset($_detail['as']) && $_detail['as'] ? $_detail['as'] : $_name);

				// special control to value manipulation
				if ($value_type == 'date') {
					if ($value_role == 'from') {
						$value .= ' 00:00:00';
					}
					else {
						$value .= ' 23:59:59';
					}
					$value = DateTimeTool::operationToSystemDateTime($value);
				}

				// append where clause
				switch ($search_type) {
					case 'raw':
						$occurances = substr_count($field_name, "?");
						$values = [];
						for ($i = 0; $i < $occurances; $i ++) {
							$values[] = $value;
						}
						$query->whereRaw($field_name, $values);
						break;
					case 'raw-like':
						$occurances = substr_count($field_name, "?");
						$values = [];
						for ($i = 0; $i < $occurances; $i ++) {
							$values[] = '%'.$value.'%';
						}
						$query->whereRaw($field_name, $values);
						break;
					case 'like':
						$query->where($field_name, $search_type, '%'.$value.'%');
						break;
					case 'or-like':
						if (isset($_detail['as']) && count($_detail['as']) > 0) {
							$field_name = $_detail['as'][0];
							$query->where(function($q) use ($_detail, $field_name, $value) {
								$q->where($field_name, 'like', '%'.$value.'%');
								
								foreach ($_detail['as'] as $key => $search_name) {
									if ($key == 0) continue;
									$q->orWhere($search_name, 'like', '%'.$value.'%');
								}
							});
						}
						break;
					case '>=':
					case '>':
					case '<':
					case '<=':
						$query->where($field_name, $search_type, $value);
						break;
					case 'in':
						$query->whereIn($field_name, $value);
						break;
					case '=':
					default:
						$query->where($field_name, '=', $value);
				}
			}
		}
		
        return $query;
    }
}
