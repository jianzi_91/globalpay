<?php

namespace App\Http\Controllers\Admin;

use App\Models\Member;
use App\Models\Activation;
use App\Models\Logger;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

use Yajra\Datatables\Datatables;

class FranchiseController extends Controller
{
	//Identifiers
	protected $identifiers = [];

	public function __construct()
	{
		parent::__construct();
	}

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	private function getSearchAttributes()
	{
		//search fields
		return [
			'id' => ['search'=>'=', 'table'=>'user', 'label'=>trans('field.user.id'), 'value'=>''],
			'username' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.username'), 'value'=>''],
			'name' => ['search'=>'like', 'table'=>'user', 'label'=>trans('field.user.name'), 'value'=>''],
			'rank' => ['search'=>'in', 'table'=>'user', 'label'=>trans('field.user.rank'), 
				'options'=>[
					'0' => trans('general.user.rank.title.0'), 
					'1100' => trans('general.user.rank.title.1100'), 
					'2100' => trans('general.user.rank.title.2100'),
					'3100' => trans('general.user.rank.title.3100'),
					'4100' => trans('general.user.rank.title.4100'),
					'5100' => trans('general.user.rank.title.5100'),
				], 
				'value'=>[]
			],
		];
	}

	public function ajaxGetDataTable()
	{
		$member_table = with(new Member)->getTable();

		$query = new Member;
		$query->setTable($member_table.' AS user');
		$query = $query->where("user.primary_acc", Member::$primary_to_code["yes"]);
		$query = $query->where("user.status", Member::$status_to_code["active"]);

		// filter build
		$search_fields = $this->getSearchAttributes();

		$query = $this->buildORMFilter($query, $search_fields);
		$datatables = Datatables::of($query);

		$query = $query->select([
			"user.id",
			"user.rank",
			"user.crank",
			"user.username",
			"user.name",
			"user.ref",
			"user.up",
			"user.up_pos",
			"user.franchise_user_id",
			"user.created_at",
		]);

		$datatables = Datatables::of($query);
    	
		return $datatables
				->editColumn('id', function ($list) {
					return e($list['id']);
				})
				->editColumn('rank', function ($list) {
					return e(trans("general.user.rank.title.".$list['rank']));
				})
				->editColumn('crank', function ($list) {
					return e(trans("general.user.rank.title.".$list["crank"]));
				})
				->editColumn('username', function ($list) {
					return e($list["username"]);
				})
				->editColumn('name', function ($list) {
					return e($list["name"]);
				})
				->addColumn('account-type', function ($list) {
					$return = $dlm = "";

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["normal"],
						"status" => Activation::$status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count() || Activation::where([
						"act_type" => Activation::$act_type_to_code["contra"],
						"status" => Activation::$status_to_code["confirmed"],
						"loan_status" => Activation::$loan_status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.normal");
						$dlm = ", ";
					}

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["contra"],
						"status" => Activation::$status_to_code["confirmed"],
						"loan_status" => Activation::$loan_status_to_code["pending"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.contra");
						$dlm = ", ";
					}

					if (Activation::where([
						"act_type" => Activation::$act_type_to_code["zero-bv"],
						"status" => Activation::$status_to_code["confirmed"],
						"uid" => $list["id"],
					])->count()) {
						$return .= $dlm.trans("general.user.account-type.ZBV");
						$dlm = ", ";
					}

					if (false) {
						$return .= $dlm.trans("general.user.account-type.parking");
						$dlm = ", ";
					}

					if ($list["crank"]) {
						$return .= $dlm.trans("general.user.account-type.free");
						$dlm = ", ";
					}

					if ($return == "") {
						$return = "-";
					}
					return e($return);
				})
				->editColumn('ref', function ($list) {
					$username = Member::where('id', $list["ref"])->value('username');
					if ($username) {
						return e($username);
					} else {
						return '-';
					}
				})
				->editColumn('up', function ($list) {
					$username = Member::where('id', $list["up"])->value('username');
					if ($username) {
						return e($username);
					} else {
						return '-';
					}
				})
				->editColumn('up_pos', function ($list) {
					return e(trans("general.user.up_pos.".$list["up_pos"]));
				})
				->editColumn('franchise_user_id', function ($list) {
					$username = Member::where('id', $list["franchise_user_id"])->value('username');
					if ($username) {
						return e($username);
					} else {
						return '-';
					}
				})
				->editColumn('created_at', function ($list) {
					return e(DateTimeTool::systemToOperationDateTime($list['created_at']));
				})
				->addColumn('action', function ($list) {
					$return = "";
					
					if ($this->user->can("admin-privilege", "members/edit-franchise")) {
						$return .= '<a href="'. e(url($this->admin_slug.'/members/change-franchise', $list['id'])) .'" class="btn btn-white btn-sm"> <i class="fa fa-pencil"></i> '. trans("general.page.admin.franchise-list.content.change_franchise")  .' </a>';
					}
					return $return;
				})
            ->make(true);
	}

    public function index()
    {
		$view = view('admin.member.franchise-list');

		//search fields
		$search_fields = $this->getSearchAttributes();
        return $view->with(['__search_fields'=>$search_fields]);
    }

	public function editFranchiseNetwork($id)
	{
		$_method =  __METHOD__;

		//initiator
		$initiator = Tool::getInitiator();
		
        $member = Member::where("id", $id)
			->where("primary_acc", Member::$primary_to_code["yes"])
			->firstOrFail();
		$user_table = $member->getTable();

        //franchise user list
        $franchise_user_list = Member::getFranchiseAccountByUserID($member->id, true);
		
		if (request()->isMethod("post")) {
			$data = request()->only("franchise_user_id");

			$niceNames = [
				'franchise_user_id' => trans('field.user.franchise_user_id'),
			];

            $v = validator($data, [
				'franchise_user_id' => 'exists:'.$user_table.',username,primary_id,0|not_in:'.$member->username,
			], [], $niceNames);

            $error_list = [];

            // if member is group leader
			if (Member::where("franchise_user_id", $member->id)->count()>0) {
				$error_list[] = ['franchise_user_id', "Remove all franchise account under member before make the changes."];
			}

			$v->after(function($validator) use ($error_list)
			{
				foreach ($error_list as $key=>$value) {
					$validator->errors()->add($value[0], $value[1]); 
				}
			});
			
            if ($v->fails())
                return back()->withInput()->withErrors($v);

            $franchise_user_id = null;

            //get franchise group leader
			if ($data["franchise_user_id"]) {
                $franchise_user = Member::where("username", $data["franchise_user_id"])->first();
                if ($franchise_user) {
                    $franchise_user_id = $franchise_user->franchise_user_id ? $franchise_user->franchise_user_id : $franchise_user->id;
                }
            }

			if ($member->franchise_user_id != $franchise_user_id) {
				//change franchise group
				$old_data = ['franchise_user_id'=>$member->franchise_user_id];

				$member->editor_type = $initiator['itype'];
				$member->editor_id = $initiator['iid'];
				$member->franchise_user_id = $franchise_user_id;
				
				if ($member->save()) {
					//Log Action
					$this->setIdentifiers($id);
					$log_msg = "Franchise Network update for User ID #".$member->id.".";
					Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['franchise_user_id'=>$franchise_user_id], 'old_data'=>$old_data, 'uid'=>$member->id]);
				}
			}

            return back()->with('success', 'Franchise Network is changed');
		}

		// get group leader username
		$member["franchise_user_id-username"] = Member::where("id", $member->franchise_user_id)->value("username");

        return view('admin.member.change-franchise', ['__member'=>$member, '__franchise_user_list'=>$franchise_user_list]);
	}
}
