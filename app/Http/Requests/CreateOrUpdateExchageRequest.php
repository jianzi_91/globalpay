<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateOrUpdateExchageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency' => 'required_unless:_method,PATCH|unique:mlm_currency_exchange,currency_id',
            'buy_rate' => 'required|numeric|min:0',
            'sell_rate' => 'required|numeric|min:0',
            'min_withdrawal_amount' => 'required|numeric|min:0',
            'status' => 'required'
        ];
    }
}
