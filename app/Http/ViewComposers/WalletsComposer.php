<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class WalletsComposer
{
    /**     
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = auth("web")->user();
        $user_id = $user->id;

        $user_type = $user->user_type;


        $wallets = $user->wallet;
        unset($user->wallet);
        $wallets["-rwallet-2d"] = "";
        $wallets["-fwallet-2d"] = "";
        $wallets["-awallet-2d"] = "";
        $wallets["-awallet2-2d"] = "";
        $wallets["-swallet-2d"] = "";
        $wallets["-cwallet-2d"] = "";
        $wallets["-dowallet-2d"] = "";
        $wallets["-sc_wallet-2d"] = "";
        unset($wallets->uid);
        
        if ($wallets) {
            foreach ($wallets->getAttributes() as $key => $value) {
                if (preg_match("/^-(.*)-2d$/", $key)) {
                    $key_ary = explode("-", $key);
                    $wallets->{$key} = floor(str_replace(",", "", $wallets->{$key_ary[1]})*1000/10)/100;
                    $wallets->{$key} = number_format($wallets->{$key}, 2);
                }
                else {
                    $wallets->{$key} = number_format($value, 4);
                }
            }
        }

        $view->with('wallets', $wallets)
             ->with('user_type', $user_type)
             ->with('lang', app()->getLocale());
    }
}