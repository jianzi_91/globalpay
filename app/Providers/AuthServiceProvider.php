<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Models\Admin;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('admin-privilege', function ($user, $privileges) {
            if ($user instanceof Admin) {
                if ($user->isFullAccess()) {
                    return true;
                }

                if ($privileges == "is-master") {
                    return false;
                }

                $privileges = explode("|", $privileges);
                $privileges = array_intersect_key(Admin::$privileges_to_code, array_flip($privileges));
                $privilege_codes = array_values($privileges);

                $admin_privileges = json_decode($user->privileges, true);
                
                if ($admin_privileges && array_intersect($admin_privileges, $privilege_codes)) {
                    return true;
                }

                return false;
            }

            return false;
        });
    }
}
