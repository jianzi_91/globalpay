<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer(
            [
                'member.layouts.app',
                'member.layouts.app-nowallet',
                'member.create',
                'member.upgrade',
                'member.wallet.transfer',
                'member.wallet.transfer-repeat',
                'member.wallet.purchase-scwallet',
                'member.withdrawal.withdraw',
            ], 
            'App\Http\ViewComposers\WalletsComposer'
        );

        // Using Closure based composers...
        // view()->composer('dashboard', function ($view) {
        //     //
        // });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}