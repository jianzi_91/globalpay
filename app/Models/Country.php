<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'mlm_countries';
    protected $fillable = [
        
    ];
    protected $primaryKey = 'country_code_2';
    public $incrementing = false;
	
	// ---------- RELATIONSHIP ---------- //

    public function member()
    {
        return $this->hasMany('App\Models\Member', 'country', 'country_code_2');
    }

    // ---------- END OF RELATIONSHIP ---------- //

        
    static function selectableCountries()
    {
        $country_locale = 'country_'.str_replace('-', '_', app()->getLocale());

        return Country::orderBy('country_en')
        ->pluck($country_locale, 'country_code_2');
    }
}
