<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberStatus extends Model
{
    protected $table = 'mlm_member_status';
    protected $primaryKey = 'uid';

    protected $guarded = [];
    public $timestamps = false;

    const ALLIANCE = [
    	'YES' => 10,
    	'NO' => null,
    ];

	public static $sa_status_to_code = [
		'yes' => 10,
		'no' => 0,
	];

}