<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    protected $table = 'mlm_access_tokens';

    public $incrementing = false;

    protected $dates = ['created_at', 'updated_at', 'expires_at'];

    public function member()
    {
        return $this->belongsTo(Member::class, 'uid');
    }

    public function refreshToken()
    {
        return $this->hasOne(RefreshToken::class, 'access_token_id');
    }

    public function remove()
    {
        if ($this->refreshToken) {
            $this->refreshToken->delete();
        }

        $this->delete();
    }
}