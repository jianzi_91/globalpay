<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberNonTrade extends Model
{
    protected $table = 'mlm_member_non_trades';
}