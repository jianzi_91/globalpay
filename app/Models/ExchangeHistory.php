<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangeHistory extends Model
{
    protected $table = 'mlm_exchange_history';

    protected $fillable = [
        'currency_id',
        'buy_rate',
        'sell_rate',
        'min_withdrawal_amount',
        'status',
        'date_origin',
        'admin_id',
        'action'
    ];

    /**
     * Currency relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    /**
     * Admin relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    /**
     * Add currency exhange history
     *
     * @param $exchange
     * @param $action
     * @return static
     */
    public static function add($exchange, $action)
    {
        return self::create([
            'currency_id' => $exchange->currency_id,
            'buy_rate' => $exchange->buy_rate,
            'sell_rate' => $exchange->sell_rate,
            'min_withdrawal_amount' => $exchange->min_withdrawal_amount,
            'status' => $exchange->status,
            'date_origin' => $exchange->updated_at,
            'admin_id' => $exchange->admin_id,
            'action' => $action
        ]);
    }
}