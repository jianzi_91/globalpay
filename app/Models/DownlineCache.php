<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class DownlineCache extends Model
{
    protected $table = 'mlm_downline_caches';
    protected $fillable = [];

    public function populate()
    {
        ini_set('max_execution_time', 0);

        $time_start = microtime(true);
        $return = DB::select("CALL usp_populate_downline_cache()")[0];
        $return = collect($return);

        if (!$return["status"]) {
            try {
                \Mail::raw(json_encode($return), function ($message) {
                    $message->to("yee.yanhoh@mifun.my")
                        ->subject("ERROR: ".__METHOD__);
                });
            }
            catch (\Exception $e) {}
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        // dump($time);
    }   
}
