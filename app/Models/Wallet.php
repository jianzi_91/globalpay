<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\WalletCode;
use App\Models\TransactionLog;
use App\Utils\Tool;

class Wallet extends Model implements WalletCode
{
    protected $table = 'mlm_member_wallets';
    protected $primaryKey = 'uid';

    protected $guarded = [];
    public $timestamps = false;

    /**
     * Get the member that owns the wallet.
     */
    public function member()
    {
        return $this->belongsTo('App\Models\Member','uid');
    }

    public function insufficientWallet($type, $point)
    {
        $point = round($point, 2);
        $balance = round($this->{$type}, 2);

        if ($balance >= $point) {
            return true;
        }

        return false;
    }
    
    static function adjust($data)
	{
		DB::transaction(function() use ($data) {
			$member_wallets = static::find($data['uid']);

			if (is_null($member_wallets)) {
				$member_wallets = new Wallet;
			}

			$awallet = isset($data['a_amt']) ? $data['a_amt'] : 0;
			// $awallet2 = isset($data['a2_amt']) ? $data['a2_amt'] : 0;
			$dowallet = isset($data['do_amt']) ? $data['do_amt'] : 0;
			$fwallet = isset($data['f_amt']) ? $data['f_amt'] : 0;
			$swallet = isset($data['s_amt']) ? $data['s_amt'] : 0;
			$sc_wallet = isset($data['sc_amt']) ? $data['sc_amt'] : 0;

			$member_wallets->awallet += $awallet;
			// $member_wallets->awallet2 += $awallet2;
			$member_wallets->dowallet += $dowallet;
			$member_wallets->fwallet += $fwallet;
			$member_wallets->swallet += $swallet;
			$member_wallets->sc_wallet += $sc_wallet;
			$member_wallets->save();

			TransactionLog::create([
				'uid' => $data['uid'],
				'trans_code' => $data['trans_code'],
				'type' => isset($data['type']) ? $data['type'] : null,
				'source_id' => isset($data['source_id']) ? $data['source_id'] : null,
				'a_amt' => $awallet,
				'a_balance' => $member_wallets->awallet,
				// 'a2_amt' => $awallet2,
				// 'a2_balance' => $member_wallets->awallet2,
				'do_amt' => $dowallet,
				'do_balance' => $member_wallets->dowallet,
				'f_amt' => $fwallet,
				'f_balance' => $member_wallets->fwallet,
				's_amt' => $swallet,
				's_balance' => $member_wallets->swallet,
				'sc_amt' => $sc_wallet,
				'sc_balance' => $member_wallets->sc_wallet,
				'remarks' => isset($data['remarks']) ? $data['remarks'] : null,
				'admin_remarks' => isset($data['admin_remarks']) ? $data['admin_remarks'] : null,
				'creator_user_type' => $data['creator_user_type'],
				'creator_uid' => isset($data['creator_uid']) ? $data['creator_uid'] : null,
			]);
		});
	}

}