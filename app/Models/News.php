<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'mlm_news';

    protected $fillable = [
        'itype',
        'iid',
        'title_en',
        'title_zh_cn',
        'descr_en',
        'descr_zh_cn',
        'content_en',
        'content_zh_cn',
        "pop_up",
        'status',
        'start_date',
        'end_date'
    ];

    protected $dates = ['start_date', 'end_date'];

    public static function getAllAvailable()
    {
        return self::where('status', 1)
            ->where('start_date', '<=', Carbon::today())
            ->where(function ($q) {
                $q->where('end_date', '>', Carbon::today())
                    ->orWhereNull('end_date');
            });
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] =  Carbon::parse($value);
    }

    public function setEndDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['end_date'] = Carbon::parse($value);
        } else {
            $this->attributes['end_date'] = null;
        }
    }
}
