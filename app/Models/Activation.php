<?php

namespace App\Models;

use DB;
use Exception; 
use Illuminate\Database\Eloquent\Model;

use App\Models\Logger;
use App\Models\Wallet;
use App\Models\Member;
use App\Utils\DateTimeTool;

class Activation extends Model
{
    protected $table = 'mlm_activations';
    protected $guarded = [];
    protected $primaryKey = 'id';
	
	//normal package
	public static $package_code_to_pack = [
		'1100' => ['price' => 100, 'amount' => 100, 
			'get_wallet' => ['swallet' => 100], 
			],
		'2100' => ['price' => 500, 'amount' => 500, 
			'get_wallet' => ['swallet' => 600], 
			],
		'3100' => ['price' => 1000, 'amount' => 1000, 
			'get_wallet' => ['swallet' => 1300], 
			],
	];

	//normal package code
	public static $normal_package_code_list = [
		"1100",
		"2100",
		"3100",
	];

	//special package code
	public static $special_package_code_list = [
	];

	//status
	public static $status_to_code = [
		'confirmed' => 10,
		'pending' => 30,
		'cancelled' => 90,
	];

	//bonus status
	public static $b_status_to_code = [
		'confirmed' => 10,
		'pending' => 50,
	];

	//loan status
	public static $loan_status_to_code = [
		'confirmed' => 10,
		'pending' => 30,
	];

	// roi status
	public static $roi_status_to_code = [
		'confirmed' => 10,
		'pending' => 30,
		'cancelled' => 90
	];

	//active type
	public static $act_type_to_code = [
		'normal' => 10,
		'contra' => 30,
		'special' => 50,
		'zero-bv' => 70,
		'special2' => 90,
	];
	
	//share purchase directly
	public static $share_convert_to_code = [
		'no' => 0,
		'yes' => 10,
	];

	//distribute wallet after loan settle - wallet 1
	public static $distribute_after_loan_to_code = [
		'no' => 0,
		'yes' => 10,
	];
	
	//distribute wallet after loan settle - wallet 2
	public static $distribute_after_loan2_to_code = [
		'no' => 0,
		'yes' => 10,
	];

	//use wallet in activation table
	public static $use_wallet_types = [
		"cwallet"
	];
	
	//Identifiers
	protected $identifiers = [];

	protected function setIdentifiers($id)
	{
		$this->identifiers[] = $id;
	}

	protected function getIdentifiers()
	{
		$identifiers = $this->identifiers;
		$this->identifiers = [];
		return $identifiers;
	}

	//get use wallet
	public function getUseWallets()
	{
		$use_wallets = [];
		foreach ($this::$use_wallet_types AS $wallet) {
			if ($this->{"use_".$wallet} > 0) {
				$use_wallets[$wallet] = $this->{"use_".$wallet};
			}
		}
		return $use_wallets;
	}

	public function getGetWallets()
	{
		$get_wallets = [];
		if ($this->get_swallet > 0) {
			$get_wallets["swallet"] = $this->get_swallet;
		}
		return $get_wallets;
	}
	//get user package rank
	public static function getUserPackageRank($user_id, $act_type = "all")
	{
		$highest_package = static::getHighestPackageByUser($user_id, $act_type);
		return $highest_package ? $highest_package->code : null;
	}

	public static function getUpgradablePackageByUser($user_id, $package_rank=null)
	{
		//$package_rank = static::getUserPackageRank($act_type);
		//cannot get only rank since it is depend on it

		$package_details = static::$package_code_to_pack;
		$package_details = array_intersect_key($package_details, array_flip(static::$normal_package_code_list));

		//no rank escape
		if (!$package_rank) {
		}
		//no defined rank escape
		elseif (!isset($package_details[$package_rank])) {
			$package_details = [];
		}
		else {
			$compare_package =  $package_details[$package_rank];

			foreach ($package_details as $_code=>$_detail) {
				if ($package_rank >= $_code) {
					unset($package_details[$_code]);
				}
				else {
					$current_package = $package_details[$_code];
					$current_package['price'] = $current_package['price'] - $compare_package['price'];
					$current_package['amount'] = $current_package['amount'] - $compare_package['amount'];
					$get_wallet = $current_package['get_wallet'];
					foreach ($get_wallet as $_wallet=>$_amount) {
						$get_wallet[$_wallet] = $_amount - $compare_package['get_wallet'][$_wallet];
					}
					$current_package['get_wallet'] = $get_wallet;
					$current_package['sponsor_double_bonus'] = $current_package['sponsor_double_bonus'] - $compare_package['sponsor_double_bonus'];
					$current_package['upgrade_double_bonus'] = $current_package['upgrade_double_bonus'] - $compare_package['upgrade_double_bonus'];
					$package_details[$_code] = $current_package;
				}
			}
		}

		return $package_details;
	}

	//get highest package by user
	public static function getHighestPackageByUser($user_id, $act_type='!contra')
	{
		$query = static::where('uid', $user_id)->where('status', static::$status_to_code['confirmed']);
		switch ($act_type) {
			case "contra":
				$query->where('act_type', '=', static::$act_type_to_code['contra']);
				break;
			case "!contra":
				$query->where('act_type', '!=', static::$act_type_to_code['contra']);
				break;
			case "all":
			default:
		}
		return $query->orderBy('code', 'desc')->orderBy('id', 'desc')->first();
	}

	//clear loan activation
	public function clearLoanActivation($user_id, $deduct_amount, $extra = [])
	{
		$_method = __METHOD__;

		$status = true;
		$errmsg = "";
		$log_msg = "";
		$payer_id = isset($extra["payer_id"]) ? $extra["payer_id"] : null;

		$query = new Activation();
		$query->setTable($query->getTable().' as act');
		$act_list = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid')
                ->where(
                    function($query) use ($user_id){
                        $query->where('user.primary_id', $user_id)
                            ->orWhere('user.id', $user_id);
                    }
				)
				->where('act.act_type', static::$act_type_to_code["contra"])
				->where('act.status', static::$status_to_code['confirmed'])
				->where('act.loan_status', static::$loan_status_to_code['pending'])
			->select('act.*')
			->orderBy('act.created_at')
			->orderBy('act.id');

		$primary_user = Member::find($user_id);
		$datetime = DateTimeTool::systemDateTime();

		$full_amount = $deduct_amount;
		foreach ($act_list->cursor() as $act) {
			$amount = $act->price - $act->loan_deduct;
			$loan_settled = false; 
			$u_data = [];
			if (round($full_amount, 6) < round($amount, 6)) {
				$amount = $full_amount;
			}
			else {
				$u_data = [
					"loan_status" => static::$loan_status_to_code['confirmed'],
					"loan_settle_at" => $datetime,
					"b_status" => static::$b_status_to_code['confirmed'],
					"bonus_at" => $datetime, 
					];
				if ($payer_id) {
					$u_data["aid"] = $payer_id;
				}
				$loan_settled = true;
			}
			$proceed = static::where('id', $act->id)
					->whereRaw("price >= loan_deduct + ?", [$amount])
					->update(["loan_deduct"=>DB::raw("loan_deduct + ".$amount)]+$u_data);

			//distribute package wallet after loan
			if ($loan_settled && ($act->distribute_after_loan == static::$distribute_after_loan_to_code['yes'] ||
			$act->distribute_after_loan2 == static::$distribute_after_loan2_to_code['yes']
			)) {
				$cur_user = Member::find($act->uid);

				if ($primary_user && $cur_user) {
					$get_wallet = [];
					if ($act->distribute_after_loan == static::$distribute_after_loan_to_code['yes']) {
						$get_wallet["wallet1"] = $act->get_wallet1;
					}
					if ($act->distribute_after_loan2 == static::$distribute_after_loan2_to_code['yes']) {
						$get_wallet["wallet2"] = $act->get_wallet2;
					}

					//credit wallet
					foreach ($get_wallet as $wallet=>$amount) {
						$descr = "Package Purchased code (".$act->code.") for User ID #".$cur_user->id.".";
						$descr_json = [
							'wallet' => $wallet,
							'amount' => $amount,
							'uid' => $cur_user->id,
							'username' => $cur_user->username,
							'to_uid' => $primary_user->id,
							'to_username' => $primary_user->username,
							'package_code' => $act->code,
							'activation_id' => $act->id,
							'trans-type' => 'product',
						]; 

						$record = $primary_user->addWallet($primary_user->id, $wallet, $amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
					}
				}

				// update to involve roi calculation
				Activation::where("id", $act->id)
					->where("roi_status", Activation::$roi_status_to_code["pending"])
					->update([
						"roi_status" => Activation::$roi_status_to_code["confirmed"]
					]);
			}

			$full_amount -= $amount;
			if ($full_amount <= 0) {
				break;
			}
		}

		//change user back to normal
		$primary_user = Member::find($primary_user->id);
        $primary_user_wallets = Wallet::where("uid", $primary_user->id)->first();
		if ($primary_user_wallets->dwallet <= 0) {
			$primary_user->dstatus = 0;
			$primary_user->save();
			$log_msg .= "User ID #".$primary_user->id." Debt is settled.\n\n";
		}

		//Log Action
		$this->setIdentifiers($primary_user->id);
		$log_msg .= "Clear Loan for User ID #".$primary_user->id." has been run.\n\n";

		Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>["user_id"=>$user_id, "deduct_amount"=>$deduct_amount, "extra"=>$extra], "uid"=>$primary_user->id]);
		
		return ['status'=>$status, 'errmsg'=>$errmsg];

	}

	public function clearLoanActivationForSpecialAcc($user_id, $deduct_amount, $extra = [])
	{
		$_method = __METHOD__;

		$status = true;
		$attribute = [];
		$errmsg = "";
		$log_msg = "";
		$payer_id = isset($extra["payer_id"]) ? $extra["payer_id"] : $this->user>id;

		$query = new Activation();
		$query->setTable($query->getTable().' as act');
		$act_list = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid')
                ->where(
                    function($query) use ($user_id){
                        $query->where('user.primary_id', $user_id)
                            ->orWhere('user.id', $user_id);
                    }
				)
				->where('act.act_type', static::$act_type_to_code["special"])
				->where('act.status', static::$status_to_code['confirmed'])
				->where('act.loan_status', static::$loan_status_to_code['pending'])
			->select('act.*')
			->orderBy('act.created_at')
			->orderBy('act.id');

		$primary_user = Member::find($user_id);
		$datetime = DateTimeTool::systemDateTime();

		//Log Action
		$this->setIdentifiers($primary_user->id);

		$full_amount = $deduct_amount;
		foreach ($act_list->cursor() as $act) {
			$amount = $act->price - $act->loan_deduct;
			$loan_settled = false; 
			$u_data = [];
			if (round($full_amount, 6) < round($amount, 6)) {
				$amount = $full_amount;

				$this->setIdentifiers($act->id);
				$log_msg .= "Settling Special Package ({$act->id}) for User ID #".$primary_user->id.".\n\n";
			}
			else {
				$u_data = [
					"loan_status" => static::$loan_status_to_code['confirmed'],
					"loan_settle_at" => $datetime,
					"b_status" => static::$b_status_to_code['confirmed'],
					"bonus_at" => $datetime, 
					];
				if ($payer_id) {
					$u_data["aid"] = $payer_id;
				}
				$loan_settled = true;

				//Log Action
				$this->setIdentifiers($act->id);
				$log_msg .= "Special Package ({$act->id}) for User ID #".$primary_user->id." is settled.\n\n";
			}

			$payer = Member::find($payer_id);
			$wallet = 'awallet2';
			$descr = "Special Package ({$act->code}) activated for User ID #".$primary_user->id;
			$descr_json = [
				'wallet' => $wallet,
				'amount' => $amount,
				'uid' => $payer_id,
				'username' => $payer->username,
				'to_uid' => $primary_user->id,
				'to_username' => $primary_user->username,
				'package_code' => $act->code,
				'activation_id' => $act->id,
				'trans-type' => 'product',
			]; 

			$record = $payer->deductWallet($payer_id, $wallet, $amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-special-activation']);

			if ($record["status"]) {
				$proceed = static::where('id', $act->id)
						->whereRaw("price >= loan_deduct + ?", [$amount])
						->update(["loan_deduct"=>DB::raw("loan_deduct + ".$amount)]+$u_data);
				$full_amount -= $amount;
			}
			
			if ($full_amount <= 0) {
				break;
			}
		}

		$attribute['balance'] = 0;
		foreach ($act_list->cursor() as $act) {
			$attribute['balance'] = $attribute['balance'] + $act->price - $act->loan_deduct;
		}

		if ($attribute['balance'] <= 0) {
			$member_status = MemberStatus::find($primary_user->id);
			if ($member_status) {
				$member_status->sa_status = 0;
			}
			
			if ($member_status->save()) {
				//Log Action
				$log_msg .= "Special Account for User ID #".$primary_user->id." activated.\n\n";
			}
		}

		//Logger
		Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>["user_id"=>$user_id, "deduct_amount"=>$deduct_amount, "extra"=>$extra], "uid"=>$primary_user->id]);

		return ['status'=>$status, 'attribute' => $attribute, 'errmsg'=>$errmsg];
	}

	public function isFirstPackage()
	{
		return $this->b_aid == 0;
	}
}
