<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryBank extends Model
{
    protected $table = 'mlm_country_banks';
    protected $fillable = [
        
    ];
    public $incrementing = false;
    public $timestamps = false;
}
