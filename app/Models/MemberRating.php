<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberRating extends Model
{
    protected $table = 'mlm_member_ratings';
    protected $guarded = [];

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }
}
