<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Interfaces\WalletCode;

class TransactionLog extends Model implements WalletCode
{
    protected $table = 'mlm_transaction_logs';
    protected $guarded = [];
    public $timestamps = false;

    static function transcodeType()
    {
        return [
            WalletCode::PURCHASE_SCWALLET => trans('member.transaction_type.purchase_scwallet'),
            WalletCode::PURCHASE_CHECKOUT_SCWALLET_API => trans('member.transaction_type.purchase_checkout_scwallet_api'),
            WalletCode:: PURCHASE_SCWALLET_REWARD_X3_FWALLET => trans('member.transaction_type.purchase_scwallet_reward_x3_fwallet'),
            WalletCode:: QUICKEN_RELEASE_PURCHASE_REWARD => trans('member.transaction_type.quicken_release_purchase_reward'),
            WalletCode:: QUICKEN_RELEASE_REPEAT_REWARD => trans('member.transaction_type.quicken_release_repeat_reward'),
            WalletCode:: DAILY_RELEASE_REWARD => trans('member.transaction_type.daily_release_reward'),
            WalletCode:: QUICKEN_RELEASE_FWALLET_REWARD => trans('member.transaction_type.quicken_release_fwallet_reward'),
            WalletCode:: WALLET_EDIT_ADMIN_ADD => trans('member.transaction_type.wallet_edit_admin_add'),
            WalletCode:: WALLET_EDIT_ADMIN_SUBTRACT => trans('member.transaction_type.wallet_edit_admin_subtract'),
            WalletCode:: WALLET_TOPUP => trans('member.transaction_type.wallet_topup'),
            WalletCode:: WALLET_TRANSFER_OUT => trans('member.transaction_type.wallet_transfer_out'),
            WalletCode:: WALLET_TRANSFER_OTHERS_AWALLET_OUT => trans('member.transaction_type.wallet_transfer_others_awallet_out'),
            WalletCode:: WALLET_TRANSFER_OTHERS_AWALLET_IN => trans('member.transaction_type.wallet_transfer_others_awallet_in'),
            WalletCode:: WALLET_TRANSFER_SELF_AWALLET => trans('member.transaction_type.wallet_transfer_self_awallet'),
            WalletCode:: WALLET_TRANSFER_SELF_FWALLET => trans('member.transaction_type.wallet_transfer_self_fwallet'),
            WalletCode:: CASHOUT_MEMBER => trans('member.transaction_type.cashout_member'),
            WalletCode:: CASHOUT_ADMIN_APPROVED => trans('member.transaction_type.cashout_admin_approved'),
            WalletCode:: CASHOUT_ADMIN_REJECTED => trans('member.transaction_type.cashout_admin_rejected'),
        ];
    }

    
    public function member()
    {
        return $this->belongsTo(Member::class, 'uid', 'id');
    }

    public function getDescription($user = null, $trans_id = null, $creator = null)
    {
        $msg = '';

        switch($this->trans_code) {
            case Wallet::PURCHASE_WALLET_A_API:
                $msg .= trans('member.transaction.purchase');
            break;
            case Wallet::PURCHASE_SCWALLET_REWARD_X3_FWALLET:
                $msg .= trans('member.transaction.purchase_scwallet_reward_x3_fwallet');
            break;
            case Wallet::QUICKEN_RELEASE_PURCHASE_REWARD:
                $msg .= trans('member.transaction.quicken_release_purchase_reward');
            break;
            case Wallet::QUICKEN_RELEASE_REPEAT_REWARD:
                $msg .= trans('member.transaction.quicken_release_repeat_reward');
            break;
            case Wallet::DAILY_RELEASE_REWARD:
                $msg .= trans('member.transaction.daily_release_reward');
            break;
            case Wallet::WALLET_TOPUP:
                $msg .= trans('member.transaction.wallet_topup');
            break;
            case Wallet::WALLET_TRANSFER_OUT:
                $msg .= trans('member.transaction.wallet_transfer_out');
            break;
            case Wallet::WALLET_TRANSFER_OTHERS_AWALLET_OUT:
                $msg .= trans('member.transaction.wallet_transfer_others_awallet_out');
            break;
            case Wallet::WALLET_TRANSFER_OTHERS_AWALLET_IN:
            $msg .= trans('member.transaction.wallet_transfer_others_awallet_in');
            break;
            case Wallet::WALLET_TRANSFER_SELF_AWALLET:
                $msg .= trans('member.transaction.wallet_transfer_self_awallet');
            break;
            case Wallet::WALLET_TRANSFER_SELF_FWALLET:
                $msg .= trans('member.transaction.wallet_transfer_self_fwallet');
            break;
            case Wallet::PURCHASE_SCWALLET:
                $msg .= trans('member.transaction.purchase_scwallet');
            break; 
            case Wallet::QUICKEN_RELEASE_FWALLET_REWARD:
                $msg .= trans('member.transaction.quicken_release_fwallet_reward');
            break;
            case Wallet::PURCHASE_CHECKOUT_SCWALLET_API:
                $msg .= trans('member.transaction.purchase_checkout_scwallet_api');
            break;
            case Wallet::WALLET_EDIT_ADMIN_ADD:
            $msg .= trans('member.transaction.wallet_edit_admin_add');
                break;
            case Wallet::WALLET_EDIT_ADMIN_SUBTRACT:
                $msg .= trans('member.transaction.wallet_edit_admin_subtract');
            break;
            case Wallet::CASHOUT_MEMBER:
                $msg .= trans('member.transaction.cashout_member');
            break;
            case Wallet::CASHOUT_ADMIN_APPROVED:
                $msg .= trans('member.transaction.cashout_admin_approved');
            break;
            case Wallet::CASHOUT_ADMIN_REJECTED:
                $msg .= trans('member.transaction.cashout_admin_rejected');
            break;
        }

        if ($this->remarks) {
            $msg .= '<br><br>'.trans('field.wallet_record.remarks').': '.e($this->remarks);
        }

        return $msg;
    }
}