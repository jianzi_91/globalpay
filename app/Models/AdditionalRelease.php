<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalRelease extends Model
{
    protected $table = 'mlm_member_additional_release';
    protected $primaryKey = 'uid';

    protected $guarded = [];
    public $timestamps = false;

    public function additionalRelease()
    {
        return $this->belongsTo('App\Models\Member', 'uid');
    }
}
