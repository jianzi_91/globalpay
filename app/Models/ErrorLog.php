<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ErrorLog extends Model
{
    protected $table = 'mlm_error_logs';
    protected $guarded = [];
    public $timestamps = false;
    
}
