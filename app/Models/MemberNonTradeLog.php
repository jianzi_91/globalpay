<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberNonTradeLog extends Model
{
    protected $table = 'mlm_member_non_trade_log';
}
