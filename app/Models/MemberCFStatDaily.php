<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberCFStatDaily extends Model
{
    protected $table = 'mlm_member_cf_stats_daily';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
