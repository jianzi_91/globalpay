<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ROIManual extends Model
{
    protected $table = 'mlm_roi_manual';

    protected $guarded = [];
    public $timestamps = false;
}