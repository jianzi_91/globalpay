<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Exception;

use App\Utils\Tool;
use App\Utils\DateTimeTool;

class SessionKeyValue extends Model
{
    protected $table = 'mlm_session_key_values';
    protected $primaryKey = 'id';
    protected $duration = 10;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'itype', 'iid', 'key', "value"
    ];
	
	public function putValue($key, $value)
	{
        $initiator = Tool::getInitiator();
        for ($count = 0; $count < 3; $count++) {
            try {
                $this->forgetKey($key);
                $this->create([
                    "itype" => $initiator["itype"],
                    "iid" => $initiator["iid"],
                    "key" => $key,
                    "value" => $value,
                ]);
                break;
            } catch (Exception $e) {
                if ($count+1 == 3) {
                    throw $e;
                }
                else {
                    $status = $this->where([
                        "itype" => $initiator["itype"],
                        "iid" => $initiator["iid"],
                        "key" => $key,
                    ])->update([
                        "value" => $value,
                    ]);

                    if ($status) {
                        break;
                    }
                }
            }
        }
	}

    public function getValue($key, $delete_flag = true)
    {
        $initiator = Tool::getInitiator();
        $record = $this->where([
            "itype" => $initiator["itype"],
            "iid" => $initiator["iid"],
            "key" => $key
        ])->first();

        if ($delete_flag) {
            $status = $this->where([
                "itype" => $initiator["itype"],
                "iid" => $initiator["iid"],
                "key" => $key,
                "value" => $record["value"]
            ])->delete();
        }
        else {
            $status = true;
        }
        return $record && $status ? $record["value"] : null;
    }
	
	public function forgetKey($key)
	{
        $initiator = Tool::getInitiator();
        try {
            return $this->where([
                "itype" => $initiator["itype"],
                "iid" => $initiator["iid"],
                "key" => $key
            ])->delete();
        } catch (Exception $e) {
            throw $e;
        }
	}

    public function deleteRoutine()
    {
        $time = strtotime(DateTimeTool::systemDateTime()) - ($this->duration * 60);
        $datetime = date("Y-m-d H:i:s", $time);
        $this->where("created_at", "<=", $datetime)->delete();
    }
}
