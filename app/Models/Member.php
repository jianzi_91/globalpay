<?php

namespace App\Models;

use DB;
use Exception;

use Illuminate\Notifications\Notifiable;
use App\Notifications\MemberResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Activation;
use App\Models\MemberStat;
use App\Models\MemberWalletRecord;
use App\Models\MemberWalletTransfer;
use App\Models\MemberStatus;
use App\Models\TransInfo;
use App\Models\Logger;
use App\Interfaces\UserCode;
use App\Repositories\TradeRepo;
use App\Trades\TradeService;
use App\Utils\Tool;
use App\Utils\DateTimeTool;

class Member extends Authenticatable implements UserCode
{
    use Notifiable;
    protected $guard = "user";

    protected $table = 'mlm_members';
    protected $primaryKey = 'id';
    
    protected static $_network = ['sponsor'=>'ref', 'placement'=>'up'];
    static $user_code = UserCode::MEMBER;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'password2', 'password_ori', 'password2_ori',
    ];

    //account rank
    public static $rank_list = [
        // '0', '1100', '2100', '3100'
        '0', '1000', '5000', '10000', '50000', '100000'
    ];

    //account rank to value
    public static $rank_to_value = [
        // '0' => 0,
        // '1100' => 100, 
        // '2100' => 500, 
        // '3100' => 1000, 
        '0' => 0,
        '1000' => 1000, 
        '5000' => 5000, 
        '10000' => 10000,
        '50000' => 50000,
        '100000' => 100000 
    ];

    static function rankTypes()
    {
        return [
            0 => trans("general.user.rank.title.0"),
            1000 => trans("general.user.rank.title.1000"),
            5000 => trans("general.user.rank.title.5000"),
            10000 => trans("general.user.rank.title.10000"),
            50000 => trans("general.user.rank.title.50000"),
            100000 => trans("general.user.rank.title.100000")
        ];
    }

    //account status
    public static $status_to_code = [
        'active' => 10,
        'suspended' => 30,
        'terminated' => 90,
    ];

    //account status
    public static $user_type_to_code = [
        'normal user' => 1,
        'pan user' => 2,
        'wine user' => 3,
    ];

    //account debt status
    public static $dstatus_to_code = [
        'in-debt' => 10,
    ];

    //primary status
    public static $primary_to_code = [
        'yes' => 'y',
        'no' => 'n',
    ];

    public static $company_status_to_code = [
        'yes' => 1,
        'no' => 0,
    ];

    public static $purchase_wallet = ['sc_wallet']; 
    public static $wallet = ['awallet', 'swallet', 'dowallet', 'fwallet']; 
    public static $wallet_transfer_list = [
        "awallet" => ["fwallet"],
        "dowallet" => ["awallet","fwallet"],
        // "dowallet" => ["awallet","awallet2","swallet"],
        // "fwallet" => ["awallet2", "swallet"],
        // "cwallet" => ["awallet2", "swallet"],
    ];

    //debt wallet
    public static $debt_wallet = ['dwallet'];
    // public static $user_code = 100;

    public static $wallet_uid = 1;
    
    //Identifiers
    protected $identifiers = [];

    // ---------- RELATIONSHIP ---------- //

    public function tradeBuy()
    {
        return $this->belongsTo('App\Models\TradeBuy');
    }

    public function tradeSell()
    {
        return $this->belongsTo('App\Models\TradeSell');
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet', 'uid');
    }

    public function additionalRelease()
    {
        return $this->hasOne('App\Models\AdditionalRelease', 'uid');
    }

    public function rating()
    {
        return $this->hasMany('App\Models\MemberRating', 'rated_uid', 'id');
    }

    public function countryInfo()
    {
        return $this->belongsTo('App\Models\Country','country','country_code_2');
    }

    public function memberStatus()
    {
        return $this->hasOne('App\Models\MemberStatus', 'uid');
    }

    public function walletRecords()
    {
        return $this->hasMany(MemberWalletRecord::class,'id','uid');
    }

    public function transactionLogs()
    {
        return $this->hasMany('App\Models\TransactionLog', 'id', 'uid');
    }

    public function upline()
    {
        return $this->hasOne(self::class, 'id', 'up');
    }

    public function downlines()
    {
        return $this->hasMany(self::class, 'up');
    }

    public function allDownlines()
    {
        return $this->downlines()->with('downlines');
    }

    public function accessTokens()
    {
        return $this->hasMany(AccessToken::class, 'uid')->orderBy('created_at', 'desc');
    }

    public function memberRelationship()
    {
        return $this->hasMany('App\Models\MemberRelationship', 'user_id', 'id');
    }
    
    // ---------- END OF RELATIONSHIP ---------- //

    // ---------- SCOPE ---------- //

    public function scopeActive($query)
    {
        return $query->where('status', Member::$status_to_code['active']);
    }

    // ---------- END OF SCOPE -------- //

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MemberResetPasswordNotification($token));
    }

    public function isCompanyAcc($id)
    {
        $acc = self::find($id);

        if ($acc->company_status == true) {
            return true;
        }
        else {
            return false;
        }
    }

    protected function setIdentifiers($id)
    {
        $this->identifiers[] = $id;
    }

    protected function getIdentifiers()
    {
        $identifiers = $this->identifiers;
        $this->identifiers = [];
        return $identifiers;
    }

    public static function isDownlineOf($downline_id, $upline_id, $network = 'placement')
    {
        $network_field = static::$_network[$network];
        $user_list = [];

        $upline = static::find($upline_id);
        $upline_id = $upline? $upline_id: 0 ;
        while ($downline_id != 0 && $upline_id != 0) {
            if ($downline_id == $upline_id) {
                return true;
            }

            // closed loop check
            if (in_array($downline_id, $user_list)) {
                break;
            }
            $user_list[] = $downline_id;

            // get next upline user
            $downline = static::find($downline_id);
            $downline_id = $downline? $downline->$network_field: 0 ;
        }
        return false;
    }

    public static function getAllDownlineId($upline_id, $network = 'placement')
    {
        $network_field = static::$_network[$network];
        $user_list = [];

        $upline = static::find($upline_id);
        if ($upline) {
            $rs = static::where($network_field, $upline_id)->select('id')->get();
            
            foreach ($rs as $key=>$value) {
                $user_list[] = $value->id;
                $user_list = array_merge($user_list, static::getAllDownlineId($value->id, $network));
            }

        }
        return $user_list;
    }

    public static function getDownMostUsername($upline_id, $position = null)
    {
        // check position
        if (!in_array($position, [1, 2])) {
            throw new Exception("Undefined position: ".$position);
        }

        // get first user
        $upline = static::where([
            "id" => $upline_id
        ])->select([
            "username",
            "name",
            "id"
        ])->first();
        if (!$upline) {
            return null;
        }

        // loop through downline
        while ($downline = static::where([
            "up" => $upline->id,
            "up_pos" => $position
        ])->select([
            "username",
            "name",
            "id"
        ])->first()) {
            $upline = $downline;
        }
        return $upline;
    }

    public function createSubAccount(Member $member)
    {
        $_method =  __METHOD__;

        $primary_id = $member->id;
        $username = $member->username;

        //initiator
        $initiator = Tool::getInitiator();

        //create 6 sub account
        $sub_member_2 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-2',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $primary_id,
            'up' => $primary_id,
            'up_pos' => 1,
            'status' => static::$status_to_code['active'], //take active status
        ]);

        $sub_member_3 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-3',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $primary_id,
            'up' => $primary_id,
            'up_pos' => 2,
            'status' => static::$status_to_code['active'], //take active status
        ]);

        $sub_member_4 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-4',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $sub_member_2->id,
            'up' =>  $sub_member_2->id,
            'up_pos' => 1,
            'status' => static::$status_to_code['active'], //take active status
        ]);

        $sub_member_5 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-5',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $sub_member_2->id,
            'up' =>  $sub_member_2->id,
            'up_pos' => 2,
            'status' => static::$status_to_code['active'], //take active status
        ]);

        $sub_member_6 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-6',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $sub_member_3->id,
            'up' =>  $sub_member_3->id,
            'up_pos' => 1,
            'status' => static::$status_to_code['active'], //take active status
        ]);

        $sub_member_7 = Member::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'editor_type' => $initiator["itype"],
            'editor_id' => $initiator["iid"],
            'username' => $username.'-7',
            'primary_acc' => static::$primary_to_code['no'],
            'primary_id' => $primary_id,
            'ref' => $sub_member_3->id,
            'up' =>  $sub_member_3->id,
            'up_pos' => 2,
            'status' => Member::$status_to_code['active'], //take active status
        ]);

        //Log Action
        $this->setIdentifiers($member->id);

        $log_msg = "Created 6 Sub Account under User ID #".$member->id.".";
        Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['uid'=>$member->id]);
    }
    
    // Basic Wallet Control
    public function walletUpdate($user_id, $wallet, $amount, $type = 'c', $decimal = 6, $descr = "", $descr_json = array(), $trans_code = "", $negative = false, $extra = [])
    {
        $_method =  __METHOD__;

        // wallet adjustment
        $user = static::findOrFail($user_id);       
        $user_wallets = $user->wallet;      
        $amount = round($amount, $decimal);     
        $proceed = false;
        $errmsg = "";
        
        // wallet check
        if (!in_array($wallet, static::$wallet) || !$user_wallets) {
            
            throw new Exception("Wallet Type does not exist in User Wallet List.");
        }
        
        // wallet adjustment
        $balance = $user_wallets? $user_wallets->$wallet: 0;

        if ($type == 'd') {
            $type = 'd';
            $query = $user_wallets->where("uid", $user_id);
            if (! $negative) {
                $query = $query->where($wallet, '>=' , $amount);
            }
            $proceed = $query->decrement($wallet, $amount);
            $balance = $balance - $amount;

            $log_msg = "User ID #".$user_id." deduct ".$amount." from wallet (".$wallet.").";
        }
        else {
            $type = 'c';
            $proceed = $user_wallets->increment($wallet, $amount);
            $balance = $balance + $amount;

            $log_msg = "User ID #".$user_id." add ".$amount." to wallet (".$wallet.").";
        }
        
        $wallet_record = new MemberWalletRecord();
        // wallet history add
        if ($proceed) {
            // initiator
            $initiator = Tool::getInitiator();
            $wallet_record->itype = $initiator["itype"];
            $wallet_record->iid = $initiator["iid"];
            $wallet_record->uid = $user_id;
            $wallet_record->amount = $amount;
            $wallet_record->balance = $balance;
            $wallet_record->wallet = $wallet;
            $wallet_record->price = isset($extra["price"]) ? $extra["price"] : null;
            $wallet_record->type = $type;
            $wallet_record->descr = $descr;

            if (isset($descr_json["remarks"])) {
                $wallet_record->remarks = $descr_json["remarks"];
                unset($descr_json["remarks"]);
            }
            if (isset($descr_json["aremarks"])) {
                $wallet_record->aremarks = $descr_json["aremarks"];
                unset($descr_json["aremarks"]);
            }

            $wallet_record->descr_json = json_encode($descr_json);

            $wallet_record->trans_code = $trans_code;
            $wallet_record->status = 10; //take active status

            // extra info
            if (isset($extra['tuid'])) {
                $wallet_record->tuid = $extra['tuid'];
            }
            
            $this->setIdentifiers($user_id);
            if ($wallet_record->save()) {
                $this->setIdentifiers($wallet_record->id);
            }
            else {
                throw new Exception("Error when recording Wallet Record.");
            }

            //Log Action
            Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['user_id'=>$user_id, 'wallet'=>$wallet, 'amount'=>$amount, 'type'=>$type, 'decimal'=>$decimal, 'descr'=>$descr, 'descr_json'=>$descr_json, 'trans_code'=>$trans_code, 'negative'=>$negative, 'extra'=>$extra], 'uid'=>$user_id]);
        }
        else {
            throw new Exception("Error when Wallet Adjustment executed for User ID #{$user_id}.");
        }
        
        return ['status'=>$proceed, 'errmsg'=>$errmsg, 'wallet_record'=>$wallet_record];
    }
    
    public function addWallet($user_id, $wallet, $amount, $descr = "", $descr_json = array(), $trans_code = "", $negative = false, $decimal = 8, $extra = [])
    {
        //add wallet
        return $this->walletUpdate($user_id, $wallet, $amount, 'c', $decimal, $descr, $descr_json, $trans_code, $negative, $extra);
    }
    
    public function deductWallet($user_id, $wallet, $amount, $descr = "", $descr_json = array(), $trans_code = "", $negative = false, $decimal = 8, $extra = [])
    {
        //deduct wallet
        return $this->walletUpdate($user_id, $wallet, $amount, 'd', $decimal, $descr, $descr_json, $trans_code, $negative, $extra);
    }
    
    //Transfer Control
    public function walletTransfer($from_uid, $to_uid, $amount, $wallet, $to_wallet, $remarks='', $fee=0)
    {
        $_method = __METHOD__;
        $_guard = "";
        
        $to_amount = $amount-$fee;
        $proceed = true;
        $errmsg = '';

        //get related user
        $from_user = static::findOrFail($from_uid);
        $to_user = static::findOrFail($to_uid);
        
        //deduct wallet
        $log_msg = $descr = "Transfer (".$amount.") with Fee ($fee) from User ID #".$from_uid." $wallet to User ID #".$to_uid." $to_wallet.";
        $descr_json = [
            'from_wallet' => $wallet,
            'to_wallet' => $to_wallet,
            'from_uid' => $from_uid,
            'to_uid' => $to_uid,
            'from_username' => $from_user->username,
            'to_username' => $to_user->username,
            'amount' => $amount,
            'fee' => $fee,
            'to_amount' => $to_amount,
            'remarks' => $remarks,
            'role' => 'from',
            'trans-type' => ($wallet==$to_wallet?'transfer':'convert'),
        ]; 

        $trans_code = $wallet==$to_wallet ? TransInfo::$trans_type_to_code['wallet-transfer'] : TransInfo::$trans_type_to_code['wallet-convert'];

        $record = $from_user->deductWallet($from_uid, $wallet, $amount, $descr, $descr_json, $trans_code, false, 8, ['tuid'=>$to_uid]);
        $proceed = $record['status'];

        if ($proceed) {
            //add wallet
            $descr_json['role'] = 'to';
            $record = $to_user->addWallet($to_uid, $to_wallet, $to_amount, $descr, $descr_json, $trans_code, false, 8, ['tuid'=>$from_uid]);
            if(!$proceed = $record['status']){
                $errmsg .= "Error while adding user wallet.\n\n";
            }
        }
        else {
            $errmsg .= "Error while deducting user wallet.\n\n";
        }
        
        //wallet transfer record add
        $wallet_transfer = new MemberWalletTransfer();
        if ($proceed) {
            //initiator
            $initiator = Tool::getInitiator($_guard);

            $wallet_transfer->itype = $initiator["itype"];
            $wallet_transfer->iid = $initiator["iid"];
            $wallet_transfer->from_uid = $from_uid;
            $wallet_transfer->to_uid = $to_uid;
            $wallet_transfer->from_amount = $amount;
            $wallet_transfer->from_wallet = $wallet;
            $wallet_transfer->to_amount = $to_amount;
            $wallet_transfer->to_wallet = $to_wallet;
            $wallet_transfer->fee = $fee;
            $wallet_transfer->remarks = $remarks;
            $wallet_transfer->status = 10; //take active status
            if ($wallet_transfer->save()) {
                $this->setIdentifiers($wallet_transfer->id);
            }
            else{
                $errmsg .= "Error when recording Wallet Transfer Record.\n\n";
            }

            //Log Action
            $this->setIdentifiers($from_uid);
            $this->setIdentifiers($to_uid);

            Logger::log($_method, $_guard, $this->getIdentifiers(), $log_msg, [
                'data' => [
                    'from_uid' => $from_uid, 
                    'to_uid' => $to_uid, 
                    'amount' => $amount, 
                    'wallet' => $wallet, 
                    'to_wallet' => $to_wallet, 
                    'remarks' => $remarks
                ]
            ]);
        }
        else {
            $errmsg .= "Error when Wallet Adjustment executed for User ID #{$user_id}.\n\n";
        }

        return ['status'=>$proceed, 'errmsg'=>$errmsg, 'transfer_record'=>$wallet_transfer];
    }

    public function cancelActivation($activation_id, $extra = [])
    {
        $_method = __METHOD__;
        $_guard = Tool::$guard;

        $status = true;
        $credit_back = isset($extra["credit_back"]) ? $extra["credit_back"] : false;
        $errmsg = "";
        $debt_wallet = "dwallet";
        $initiator = Tool::getInitiator();

        $datetime = DateTimeTool::systemDateTime();

        $activation = Activation::find($activation_id);
        
        // get related needed info
        $activator = $activatee = null;
        if ($activation) {
            // get activator/payer
            if ($activation->aid) {
                $activator = Member::find($activation->aid);
                if (!$activator) {
                    $status = false;
                    $errmsg .= "Error: Activator cannot be found."."\n\n";
                }
            }

            // get target user
            $activatee_primary = $activatee = Member::find($activation->uid);
            if ($activatee_primary->primary_acc == Member::$primary_to_code["no"]) {
                $activatee_primary = Member::find($activatee_primary->primary_id);
            }

            if (!$activatee || !$activatee_primary) {
                $status = false;
                $errmsg .= "Error: User cannot be found."."\n\n";
            }

            // check status
            if ($activation->status == Activation::$status_to_code["cancelled"]) {
                $status = false;
                $errmsg .= "Error: Activation has been cancelled."."\n\n";
            } 
            elseif ($activation->loan_status == Activation::$loan_status_to_code["confirmed"] && 
            $activation->act_type == Activation::$act_type_to_code["contra"]) {
                $status = false;
                $errmsg .= "Error: Activation has settled."."\n\n";
            }
        }
        else{
            $status = false;
            $errmsg .= "Error: Activation cannot be found."."\n\n";
        }

        // continue to cancel activation
        if ($status) {
            try {
                DB::beginTransaction();
                $proceed = true;
                $trans_code = TransInfo::$trans_type_to_code['package-cancel'];

                // STEP 1: deduct wallet from activatee
                $get_wallet = $activation->getGetWallets();

                // if contra account select distribute later, only debt wallet distribute
                if ($activation->act_type == Activation::$act_type_to_code["contra"] &&
                    $activation->loan_status == Activation::$loan_status_to_code["pending"]) {
                        
                    if ($activation->distribute_after_loan == Activation::$distribute_after_loan_to_code['yes']) {
                        if (isset($get_wallet["wallet1"])) {
                            unset($get_wallet["wallet1"]);
                        }
                    }
                    
                    if ($activation->distribute_after_loan2 == Activation::$distribute_after_loan2_to_code['yes']) {
                        if (isset($get_wallet["wallet2"])) {
                            unset($get_wallet["wallet2"]);
                        }
                    }
                }
                
                $activatee_primary_wallets = $activatee_primary->wallet;
                foreach ($get_wallet as $wallet=>$amount) {
                    if ($amount > 0 ) {
                        if ($wallet == $debt_wallet) {
                            $amount -= $activation->loan_deduct;
                        }
    
                        if ($activatee_primary_wallets->$wallet >= $amount && $proceed) {
                            $descr = "Package Cancel for User ID #".$activatee->id.".";
                            $descr_json = [
                                'wallet' => $wallet,
                                'amount' => $amount,
                                'uid' => $activatee->id,
                                'username' => $activatee->username,
                                'to_uid' => $activatee_primary->id,
                                'to_username' => $activatee_primary->username,
                                'package_code' => $activation->code,
                                'activation_id' => $activation->id,
                                'trans-type' => 'product',
                            ];
    
                            $rs = $activatee_primary->deductWallet($activatee_primary->id, $wallet, $amount, $descr, $descr_json, $trans_code);
                        }
                        else {
                            $proceed = false;
                            $errmsg .= "Error: User ID #".$activatee_primary->id." has insufficient (".$wallet.") wallet."."\n\n";
                            break;
                        }
                            
                        if (!$rs["status"]) {
                            $proceed = false;
                            $errmsg .= "Error: User ID #".$activatee_primary->id." fail to deduct (".$wallet.") wallet."."\n\n";
                            break;
                        }
                    }
                }

                // STEP 2: update activation to cancel 
                if ($proceed) {
                    $activation->status = Activation::$status_to_code["cancelled"];
                    $activation->cancelled_user_type = $initiator["itype"];
                    $activation->cancelled_user_id = $initiator["iid"];
                    $activation->cancelled_at = $datetime;
                    if (!$activation->save()) {
                        $proceed = false;
                        $errmsg .= "Error: Activation ID #".$activation->id." cannot be updated."."\n\n";
                    } 
                } 

                // STEP 3: update user rank
                if ($proceed) {
                    //up rank full condition
                    $activatee = Member::find($activatee->id);
                    $activatee->upgradeUserRank($activatee->id, true);
                }

                // STEP 4: update user debt status
                if ($proceed) {

                    if ($activation->act_type == Activation::$act_type_to_code["contra"]){
                        //change user back to normal
                        $activatee_primary = Member::find($activatee_primary->id);
                        $activatee_primary_wallets = $activatee_primary->wallet;
                        if ($activatee_primary_wallets->$debt_wallet<=0) {
                            $activatee_primary->editor_type = $initiator["itype"];
                            $activatee_primary->editor_id = $initiator["iid"];
                            $activatee_primary->dstatus = 0;
                            $activatee_primary->save();
                        }
                    }
                    elseif ($activation->act_type == Activation::$act_type_to_code["special"]) {
                        //check sa account
                        $sa_account = Activation::where("upid", $activatee_primary->id)
                            ->where("act_type", Activation::$act_type_to_code["special"])
                            ->where("status", Activation::$status_to_code["confirmed"])
                            ->select([DB::raw("SUM(loan_deduct - price) AS total_sa_sum_left")])
                            ->first();
                        $sa_amount = 0;
                        if ($sa_account) {
                            $sa_amount = $sa_account["total_sa_sum_left"];
                        }

                        if ($sa_amount == 0) {
                            MemberStatus::where("uid", $activatee_primary->id)
                                ->update([
                                    "sa_status" => 0
                                ]);
                        }
                    }
                }

                // STEP 5: add wallet to activator
                if ($proceed && $activator) {
                    $use_wallet = $activation->getUseWallets();
                    
                    foreach ($use_wallet as $wallet=>$amount) {
                        if ($amount > 0) {
                            $descr = "Package Cancel for User ID #".$activatee->id.".";
                            $descr_json = [
                                'wallet' => $wallet,
                                'amount' => $amount,
                                'uid' => $activatee->id,
                                'username' => $activatee->username,
                                'to_uid' => $activator->id,
                                'to_username' => $activator->username,
                                'package_code' => $activation->code,
                                'activation_id' => $activation->id,
                                'trans-type' => 'refund',
                            ];

                            $rs = $activator->addWallet($activator->id, $wallet, $amount, $descr, $descr_json, $trans_code);
                            
                            if (!$rs["status"]) {
                                $proceed = false;
                                $errmsg .= "Error: User ID #".$activator->id." fail to add (".$wallet.") wallet."."\n\n";
                                break;
                            }
                        }
                    }
                }

                // STEP 6: add wallet to activatee
                if ($credit_back) {
                    $wallet = "cwallet";
                    $amount = $activation->loan_deduct;

                    if ($amount > 0) {
                        $descr = "Package Cancel for User ID #".$activatee->id.".";
                        $descr_json = [
                            'wallet' => $wallet,
                            'amount' => $amount,
                            'uid' => $activatee->id,
                            'username' => $activatee->username,
                            'to_uid' => $activatee_primary->id,
                            'to_username' => $activatee_primary->username,
                            'package_code' => $activation->code,
                            'activation_id' => $activation->id,
                            'trans-type' => 'refund',
                        ];

                        $rs = $activatee_primary->addWallet($activatee_primary->id, $wallet, $amount, $descr, $descr_json, $trans_code);
                            
                        if (!$rs["status"]) {
                            $proceed = false;
                            $errmsg .= "Error: User ID #".$activatee_primary->id." fail to add (".$wallet.") wallet."."\n\n";
                        }
                    }
                }

                //Log Action
                $log_msg = "Package ID #".$activation->id." Cancel code (".$activation->code.") for User ID #".$activation->uid.".";
                $this->setIdentifiers($activation->id);
                $this->setIdentifiers($activation->uid);
                
                Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['id'=>$activation_id, 'extra'=>$extra], 'uid'=>$activation->uid]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw $e;
            }
        }

        return ['status'=>$status, 'errmsg'=>$errmsg];
    }

    public function cancelConfirmedActivation($activation_id, $extra = []) 
    {
        $_guard = Tool::$guard;
        $initiator = Tool::getInitiator();

        $datetime = DateTimeTool::systemDateTime();

        $activation = Activation::find($activation_id);
        
        // get related needed info
        $activator = $activatee = null;
        if (!$activation) {
            throw new Exception("Error: Activation cannot be found.");
        }

        // get target user
        $activatee_primary = $activatee = Member::find($activation->uid);
        if ($activatee_primary->primary_acc == Member::$primary_to_code["no"]) {
            $activatee_primary = Member::find($activatee_primary->primary_id);
        }

        if (!$activatee || !$activatee_primary) {
            throw new Exception("Error: User cannot be found.");
        }

        // check status
        if ($activation->status != Activation::$status_to_code["confirmed"]
        || $activation->b_status != Activation::$b_status_to_code["confirmed"]
        || (DateTimeTool::systemToOperationDateTime($activation->bonus_at) >= DateTimeTool::operationDateTime("Y-m-d")
        && $activation->act_type != Activation::$act_type_to_code["contra"] )) {
            throw new Exception("Error: Activation is not confirmed.");
        }

        try {
            DB::beginTransaction();
            $trans_code = TransInfo::$trans_type_to_code['package-confirmed-cancel'];

            // STEP 1: deduct wallet from activatee
        
            if (round($activation->roi_day_counter) < round(100)) {
                $activatee_primary_wallets = $activatee_primary->wallet;
                $amount = $activation->get_wallet2;
                $wallet = "wallet2";
                if ($activatee_primary_wallets->$wallet >= $amount && $amount>0) {
                    $descr = "Package Cancel for User ID #".$activatee->id.".";
                    $descr_json = [
                        'wallet' => $wallet,
                        'amount' => $amount,
                        'uid' => $activatee->id,
                        'username' => $activatee->username,
                        'to_uid' => $activatee_primary->id,
                        'to_username' => $activatee_primary->username,
                        'package_code' => $activation->code,
                        'activation_id' => $activation->id,
                        'trans-type' => 'product',
                    ];

                    $rs = $activatee_primary->deductWallet($activatee_primary->id, $wallet, $amount, $descr, $descr_json, $trans_code);
                }
                else {
                    throw new Exception("Error: User ID #".$activatee_primary->id." has insufficient (".$wallet.") wallet.");
                }
            }

            // STEP 2: update activation to cancel 
            $activation->status = Activation::$status_to_code["cancelled"];
            $activation->cancelled_user_type = $initiator["itype"];
            $activation->cancelled_user_id = $initiator["iid"];
            $activation->cancelled_at = $datetime;
            if (!$activation->save()) {
                throw new Exception("Error: Activation ID #".$activation->id." cannot be updated.");
            } 

            // STEP 3: update user rank
            $activatee = Member::find($activatee->id);
            $activatee->upgradeUserRank($activatee->id, true);

            //Log Action
            $log_msg = "Package ID #".$activation->id." Cancel code (".$activation->code.") for User ID #".$activation->uid.".";
            $this->setIdentifiers($activation->id);
            $this->setIdentifiers($activation->uid);
            Logger::logWithoutGuard(__METHOD__, $this->getIdentifiers(), $log_msg, ['data'=>['id'=>$activation_id, 'extra'=>$extra], 'uid'=>$activation->uid]);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    public function distributeLoanActivation($activation_id)
    {
        try {
            DB::beginTransaction();

            $activation = Activation::find($activation_id);

            $primary_user = $cur_user = Member::find($activation->uid);
            if ($primary_user->primary_acc != Member::$primary_to_code["yes"] ) {
                $primary_user = Member::find($primary_user->primary_id);
            }

            if ($primary_user && $cur_user) {
                $get_wallet = $activation->getGetWallets();
                if (isset($get_wallet["dwallet"])) {
                    unset($get_wallet["dwallet"]);
                }

                if ($activation->distribute_after_loan == Activation::$distribute_after_loan_to_code['no']) {
                    if (isset($get_wallet["wallet1"])) {
                        unset($get_wallet["wallet1"]);
                    }
                }
                
                if ($activation->distribute_after_loan2 == Activation::$distribute_after_loan2_to_code['no']) {
                    if (isset($get_wallet["wallet2"])) {
                        unset($get_wallet["wallet2"]);
                    }
                }

                $activation->distribute_after_loan = Activation::$distribute_after_loan_to_code['no'];
                $activation->distribute_after_loan2 = Activation::$distribute_after_loan2_to_code['no'];
                $activation->roi_status = Activation::$roi_status_to_code["confirmed"];
                $proceed = $activation->save();

                if ($proceed) {

                    //credit wallet
                    foreach ($get_wallet as $wallet => $amount) {
                        if ($amount > 0) {
                            $descr = "Package Purchased code (".$activation->code.") for User ID #".$cur_user->id.".";
                            $descr_json = [
                                'wallet' => $wallet,
                                'amount' => $amount,
                                'uid' => $cur_user->id,
                                'username' => $cur_user->username,
                                'to_uid' => $primary_user->id,
                                'to_username' => $primary_user->username,
                                'package_code' => $activation->code,
                                'activation_id' => $activation->id,
                                'trans-type' => 'product',
                            ];
    
                            $record = $primary_user->addWallet($primary_user->id, $wallet, $amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    //Activation Control
    public function addActivation($user_id, $package_code, $activator_id = 0, $extra = [])
    {
        $_method = __METHOD__;
        $_guard = Tool::$guard;

        $user = static::findOrFail($user_id);
        
        //get primary account
        if ($user->primary_acc == static::$primary_to_code['yes']) {
            $primary_user = $user;
        }
        else{
            $primary_user = static::findOrFail($user->primary_id);
        }

        $topup_baid = $errmsg = "";
        $contra = $free = 0;
        $datetime = DateTimeTool::systemDateTime();
        $datetime_op = DateTimeTool::operationDateTime();
        
        //set variable
        $package_details = Activation::$package_code_to_pack;

        //top up by differences
        if (isset($extra['topup']) && $extra['topup'] && isset($extra['topup_activation_id']) && $extra['topup_activation_id']) {
            $base_activation = Activation::find($extra['topup_activation_id']); 
            if ($base_activation) {
                $topup_baid = $base_activation->id;
                $package_details = Activation::getUpgradablePackageByUser($user->id, $base_activation->code);
            }
        }

        $package = $package_details[$package_code];
        $get_wallet = $package_details[$package_code]['get_wallet'];
        $act_type = Activation::$act_type_to_code['normal'];
        $b_status = Activation::$b_status_to_code['confirmed'];
        $bonus_at = $datetime;
        $loan_status = 0;
        $aremarks = isset($extra['aremarks']) ? $extra['aremarks'] : '';
        $share_price = isset($extra['share_price']) ? $extra['share_price'] : '';
        $share_qty = isset($extra['share_qty']) ? $extra['share_qty'] : '';
        $share_convert = isset($extra['share_convert']) ? $extra['share_convert'] : '';
        $distribute_after_loan = isset($extra['distribute_after_loan']) ? $extra['distribute_after_loan'] : '';
        $distribute_after_loan2 = isset($extra['distribute_after_loan2']) ? $extra['distribute_after_loan2'] : '';
        
        $roi_status = Activation::$roi_status_to_code["confirmed"];
        $debt_wallet = "dwallet";
        
        if (!isset($extra['use_wallet_json'])) {
            $extra['use_wallet_json'] = [];
        }
        
        //initiator
        $initiator = Tool::getInitiator();

        if ($_guard === 'admins' && isset($extra["act_type"])) {

            //special activation
            if ($extra["act_type"] == "bvfree") {
                //zero bv
                $act_type = Activation::$act_type_to_code['zero-bv'];

                $package['amount'] = 0;
                $package["upgrade_double_bonus"] = 0;
                $package["sponsor_double_bonus"] = 0;
            }
            elseif ($extra["act_type"] == "contra") {
                //contra/loan
                $act_type = Activation::$act_type_to_code['contra'];
                $loan_status = Activation::$loan_status_to_code['pending'];
                $b_status = Activation::$b_status_to_code['pending'];
                $bonus_at = '';

                $get_wallet += [$debt_wallet => $package['price']];

                // roi status
                if ($distribute_after_loan2 == Activation::$distribute_after_loan2_to_code["yes"]) {
                    $roi_status = Activation::$roi_status_to_code["pending"];
                }
            }
            elseif ($extra["act_type"] == "special") {
                //SA
                $act_type = Activation::$act_type_to_code['special'];
                $b_status = Activation::$b_status_to_code['pending'];
                $bonus_at = '';

                // wallet
                $get_wallet['wallet2'] = 0;

                if (Activation::$share_convert_to_code['yes'] == $extra['share_convert']) {
                    $share_qty = floor($package_details[$package_code]['get_wallet']['wallet1'] / $share_price);
                    
                }

                // in loan
                $loan_status = Activation::$loan_status_to_code['pending'];

                // no bv
                $package['amount'] = 0;
                $package["upgrade_double_bonus"] = 0;
                $package["sponsor_double_bonus"] = 0;

                // roi status
                $roi_status = Activation::$roi_status_to_code["pending"];
            }
        }
        
        //add activation
        $activation = Activation::create([
            'itype' => $initiator["itype"], //initiator code
            'iid' => $initiator["iid"], //initiator
            'aid' => $activator_id, 
            'uid' => $user_id,
            'upid' => $primary_user->id,
            'code' => $package_code, 
            'price' => $package['price'], 
            'amount' => $package['amount'], 
            'act_type' => $act_type, 
            'b_status' => $b_status, 
            'bonus_at' => $bonus_at, 
            'loan_status' => $loan_status, 
            'share_convert' => $share_convert, 
            'share_price' => $share_price,
            'share_qty' => $share_qty,
            'sponsor_double_bonus' => 0, 
            'upgrade_double_bonus' => 0,
            'distribute_after_loan' => $distribute_after_loan, 
            'distribute_after_loan2' => $distribute_after_loan2, 
            'roi_status' => $roi_status, 
            'aremarks' => $aremarks, 
            'b_aid' => $topup_baid, 
            'status' => Activation::$status_to_code['confirmed'], //take active status
        ]);

        try {
            $use_wallet_fields = [];
            foreach ($extra['use_wallet_json'] as $wallet => $amount) {
                $use_wallet_fields["use_".$wallet] = $amount;
            }

            $get_wallet_fields = [];
            foreach ($get_wallet as $wallet => $amount) {
                $get_wallet_fields["get_".$wallet] = $amount;
            }

            $activation->forceFill($use_wallet_fields + $get_wallet_fields)->save();

            
        } catch (Exception $e) {

        }
        
        $status = $activation ? true : false;
        if ($status) {
            if ($activation->act_type == Activation::$act_type_to_code['contra']) {
                if ($distribute_after_loan == Activation::$distribute_after_loan_to_code["yes"]) {
                    unset($get_wallet["swallet"]);
                }
            }

            //credit wallet
            foreach ($get_wallet as $wallet=>$amount) {
                if ($amount > 0) {
                    $descr = "Package Purchased code (".$package_code.") for User ID #".$user_id.".";
                    $descr_json = [
                        'wallet' => $wallet,
                        'amount' => $amount,
                        'uid' => $user_id,
                        'username' => $user->username,
                        'to_uid' => $primary_user->id,
                        'to_username' => $primary_user->username,
                        'package_code' => $package_code,
                        'activation_id' => $activation->id,
                        'trans-type' => 'product',
                    ]; 
                    
                    //loan account add debt wallet
                    if ($wallet == $debt_wallet) {
                        $descr_json = [
                            'wallet' => $wallet,
                            'amount' => $amount,
                            'uid' => $user_id,
                            'username' => $user->username,
                            'to_uid' => $primary_user->id,
                            'to_username' => $primary_user->username,
                            'package_code' => $package_code,
                            'activation_id' => $activation->id,
                            'trans-type' => 'debt',
                        ]; 
                    }
                    
                    $record = $this->addWallet($primary_user->id, $wallet, $amount, $descr, $descr_json, TransInfo::$trans_type_to_code['package-purchase']);
    
                    $errmsg .= $record['errmsg'];
                }
            }

            //up rank full condition
            $this->upgradeUserRank($user_id, false);

            // if (!in_array($activation->act_type, [Activation::$act_type_to_code['contra'], Activation::$act_type_to_code['special']])) {
            //  //update user stat
            //  $this->updateUserStat([
            //      "activation_id" => $activation->id, 
            //      "user_id" => $activation->uid, 
            //      "price" => $activation->price, 
            //      "amount" => $activation->amount,
            //      "new_user" => false
            //  ]);
            // }

            //Log Action
            $log_msg = "Package Purchased code (".$package_code.") for User ID #".$user_id.".";
            $this->setIdentifiers($activation->id);
            $this->setIdentifiers($activation->uid);
            
            Logger::logWithoutGuard($_method, $this->getIdentifiers(), $log_msg, ['data'=>['user_id'=>$user_id, 'package_code'=>$package_code, 'extra'=>$extra], 'uid'=>$activation->uid]);
        }
        else {
            $errmsg .= "Error when adding Activation for User ID #".$user_id."\n\n";
        }
        
        return ['status'=>$status, 'errmsg'=>$errmsg, 'activation'=>$activation];
    }

    public function upgradeUserRank($user_id, $downgrade=false)
    {
        $_method = __METHOD__;
        
        $user = static::find($user_id);
        $status = true; $errmsg = "";

        if ($user) {
            // //up rank full condition
            // $total_investment = Activation::where('status', Activation::$status_to_code['confirmed'])
            //  ->where('uid', $user_id)
            //  ->sum('price');

            // //check user latest rank
            // $rank_to_value = static::$rank_to_value;
            // asort($rank_to_value);
            // $new_rank = '0';
            // foreach ($rank_to_value as $rank => $value) {
            //  if ($value <= $total_investment) {
            //      $new_rank = $rank;
            //  }
            // }

            $highest_package = Activation::getHighestPackageByUser($user_id, "all");
            $new_rank = $highest_package ? $highest_package->code : 0; 
            
            if ($user->rank != $new_rank) {
                if ($user->rank < $new_rank || $downgrade === true) {
                    $user->rank = $new_rank;
                    if (!$user->save()) {
                        $status = false;
                        $errmsg .= "Error when up user rank for User ID #{$user_id}.\n\n";
                    }
                    else {
                        //Log Action
                        $log_msg = "User ID #".$user_id." rank to ".$new_rank.".";
                        
                        Logger::logWithoutGuard($_method, [$user->id], $log_msg, ['data'=>['user_id'=>$user->id, 'rank'=>$new_rank], 'uid'=>$user->id]);
                    }
                }
            }
        }
        else{
            $status = false;
            $errmsg .= "Error when User ID #{$user_id} not found.\n\n";
        }
        
        return ['status'=>$status, 'errmsg'=>$errmsg];
    }

    public function autoBuyShare($user_id, $amount, $extra = []) 
    {
        $_method = __METHOD__;
        $_guard = Tool::$guard;

        $datetime = DateTimeTool::operationDateTime();
        
        try {
            $trade_remark = isset($extra["trade_remark"]) ? $extra["trade_remark"] : null;
            $trade_remark_replace = isset($extra["trade_remark_replace"]) ? $extra["trade_remark_replace"] : null;
            DB::beginTransaction();
            
            $share_price = 0.1;
            $unit = floor(round($amount / $share_price, 2));
            $use_amount = round($unit * $share_price, 2);
            $wallet = "wallet1";
            
            if ($use_amount > 0) {
                
                $user = Member::find($user_id);
                $descr_json = [
                    'wallet' => $wallet,
                    'amount' => $use_amount,
                    'uid' => $user->id,
                    'username' => $user->username,
                ]; 

                $proceed = $user->deductWallet($user->id, $wallet, $use_amount, 'eShare purchase after spilt at '.number_format($share_price, 3), $descr_json, TransInfo::$trans_type_to_code['ebond-purchase']);

                if ($proceed["status"]) {
                    $admin_remark = $remark = $use_amount." / ".number_format($share_price, 3)." = ".$unit;

                    if ($trade_remark) {
                        $trade_remark_replace += [
                            ":unit" => number_format($unit),
                            ":date" => date("d/m", strtotime($datetime)),
                            ":wallet_amount" => number_format($use_amount, 2),
                            ":share_price" => number_format($share_price, 3),
                        ];
                        $trade_remark = str_replace(array_keys($trade_remark_replace), array_values($trade_remark_replace), $trade_remark);
                        $remark = $trade_remark;
                    }

                    $trade_repo = new TradeRepo();
                    $trade_repo->addTradeUnit2($user_id, $unit, $remark, $admin_remark, $share_price, "S", static::$user_code, auth()->id());
                                
                    //Log Action
                    $log_msg = "User ID #".$user->id." auto buy System Share ".$remark.".";
                    Logger::log($_method, $_guard, [$user->id], $log_msg, ['data'=>[
                        'user_id' => $user->id, 
                        'amount' => $amount, 
                        'use_amount' => $use_amount, 
                        'wallet' => $wallet, 
                        'share_price' => $share_price,
                        'extra' => $extra
                    ], 'uid'=>$user->id]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function autoPlaceBuyShare($user_id, $amount, $extra = []) 
    {
        try {
            DB::beginTransaction();
            $trade_repo = new TradeRepo();
            $tradeService = app()->make(TradeService::class);
            $trade_closed = $tradeService->isMarketClosed();
            $share_price = $trade_repo->getPendingOpenQtyPrice();
            if ($share_price > 0) {
                $unit = floor(round($amount / $share_price, 2));
                $direct_place = false;
            }
            else {
                $share_price = config("trade.next_min");
                if ($share_price > 0) {
                    $direct_place = true;
                    $unit = floor(round($amount / $share_price, 2));
                }
                else {
                    $unit = 0;
                }
            }
                
            if ($unit > 0) {
                
                $user = Member::find($user_id);
                $buy_data = [
                    'buy_qty' => $unit,
                    'daily_limit_flag' => false,
                ];

                if (isset($extra["activation_id"])) {
                    $buy_data["activation_id"] = $extra["activation_id"];
                }
                elseif (isset($extra["buy_type"])) {
                    $buy_data["buy_type"] = $extra["buy_type"];
                }

                if (!$direct_place) {
                    $return = $tradeService->buy($user->id, $buy_data);
                    if ($return["max_limit_hit"]) {
                        $share_price = config("trade.next_min");
                        if ($share_price > 0) {
                            $unit = floor(round(($amount - $return["used_cash"]) / $share_price, 2));
                            $buy_data['uid'] = $user->id;
                            $buy_data['buy_qty'] = $unit;
                            $trade_repo->createTradeBuy($buy_data);
                        }
                    }
                }
                else {
                    $buy_data['uid'] = $user->id;
                    $buy_data['buy_qty'] = $unit;
                    $trade_repo->createTradeBuy($buy_data);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateUserStat($stat_info)
    {
        $_method = __METHOD__;
        
        $status = true;
        $errmsg = "";

        // get data from stat_info
        $stat_activation_id = $stat_info['activation_id'];
        $stat_user_id = $stat_info['user_id'];
        $stat_price = $stat_info['price'];
        $stat_amount = $stat_info['amount'];
        $stat_new_user = $stat_info['new_user'];
        
        //only update today sales amount which will flush in Commission::accumulateSales
        // Conditions:
        // 1. Bonus able activation only (exclude: loan and cancelled)
        // 2. Bonus receivable user only (exclude: terminated and no rank)

        $from_user = Member::find($stat_user_id);
        if ($from_user) {

            //update placement sales stat
            $upline_id = $from_user->up;
            $upline_pos = $from_user?$from_user->up_pos:0;

            while ($upline_id) {
                $upline = Member::find($upline_id);
                // if ($primary_upline && $primary_upline->primary_acc != Member::$primary_to_code['yes']) {
                //  $primary_upline = Member::find($primary_upline->primary_id);
                // }
                // $upline_rank = max($upline->rank, $upline->crank);

                if ($upline->status != Member::$status_to_code['terminated']) {
                    $member_stats = MemberStat::where('uid', $upline->id)->first();
                    //accumulate
                    if (!$member_stats) {
                        $member_stats = MemberStat::create(['uid'=>$upline->id]);
                    }

                    if ($member_stats) {
                        $proceed = MemberStat::where('uid', $upline->id)
                            ->update([
                                'today_leg'.$upline_pos.'_price' => DB::raw('today_leg'.$upline_pos.'_price + '.$stat_price), 
                                'today_leg'.$upline_pos.'_amt' => DB::raw('today_leg'.$upline_pos.'_amt + '.$stat_amount)
                            ]);

                        if (!$proceed) {
                            //error: today sales cannot save
                            $errmsg .= "Member stat #".$member_stats->id." cannot update for user #".$upline->id.".\n\n";
                            $status = false;
                        }
                    }
                    else{
                        //error: no member stats record found
                        $errmsg .= "No member stat for user #".$upline->id.".\n\n";
                        $status = false;
                    }
                }

                $upline_id = $upline->up;
                $upline_pos = $upline?$upline->up_pos:0;
            }
        }
        else {
            //error: no user found
            $errmsg .= "No user found #".$stat_user_id.".\n\n";
            $status = false;
        }

        if ($status) {
            //Log Action
            $log_msg = "Member Stat Update";
            Logger::logWithoutGuard($_method, [], $log_msg, ['data'=>$stat_info]);
        }

        return ['status'=>$status, 'errmsg'=>$errmsg];
    }

    public static function getUserTotalInvestment($user_id)
    {
        $query = new Activation();
        $query->setTable($query->getTable().' as act');
        $query = $query->join(with(new Member)->getTable().' AS user', 'user.id', '=', 'act.uid')
                ->where(
                    function($query) use ($user_id){
                        $query->where('user.primary_id', $user_id)
                            ->orWhere('user.id', $user_id);
                    }
                )
                ->where('act.status', Activation::$status_to_code['confirmed']);
        $activation_investment = $query->sum('act.price');

        // investment based on rank given
        $crank_investment = 0;

        if ($activation_investment <= 0) {
            $cur_user = Member::find($user_id);
            if ($cur_user) {
                if (isset(self::$rank_to_value[$cur_user->crank])) {
                    $crank_investment = self::$rank_to_value[$cur_user->crank];
                }
            }
        }

        $total_investment = max($activation_investment, $crank_investment);

        return $total_investment;
    }

    public static function getUserTotalInvestmentForWithdrawal($user_id)
    {
        $total_investment = 0;
        $member_accounts = Member::where('id', $user_id)
                                    ->orWhere('primary_id', $user_id)
                                    ->select('rank', 'crank')
                                    ->get();

        foreach ($member_accounts as $acc) {
            $total_investment += max(self::$rank_to_value[$acc->rank], self::$rank_to_value[$acc->crank]);
        }

        return $total_investment;
    }

    public static function getFranchiseAccountByUserID($user_id, $primary_account_only = true)
    {
        $user_list = ["main"=>[], "franchise"=>[]];

        //get franchise main id
        $franchise_user = Member::find($user_id);
        
        if ($franchise_user->franchise_user_id) {
            $franchise_user = Member::find($franchise_user->franchise_user_id);
        }
        $franchise_user_wallets = $franchise_user->wallet;

        if ($franchise_user) {

            //build franchise main user
            $user_list["main"][] = ["id"=>$franchise_user->id, "username"=>$franchise_user->username, "rank"=>$franchise_user->rank, "crank"=>$franchise_user->crank, "fwallet"=>$franchise_user_wallets->fwallet, "primary_id"=>$franchise_user->primary_id];
            if ($primary_account_only !== true) {
                $result = Member::where("primary_id", $franchise_user->id)->orderBy("id");
                foreach ($result->cursor() as $sub_member) {
                    $user_list["main"][] = ["id"=>$sub_member->id, "username"=>$sub_member->username, "rank"=>$sub_member->rank, "crank"=>$sub_member->crank, "fwallet"=>0, "primary_id"=>$sub_member->primary_id];
                } 
            }

            //build franchise user
            $query_account = Member::where("franchise_user_id", $franchise_user->id)->orderBy("id");

            foreach ($query_account->cursor() as $member) {
                $member_wallets = $member->wallet;
                $user_list["franchise"][] = ["id"=>$member->id, "username"=>$member->username, "rank"=>$member->rank, "crank"=>$member->crank, "fwallet"=>$member_wallets->fwallet, "primary_id"=>$member->primary_id];
                if ($primary_account_only !== true) {
                    $result = Member::where("primary_id", $member->id)->orderBy("id");
                    foreach ($result->cursor() as $sub_member) {
                        $user_list["franchise"][] = ["id"=>$sub_member->id, "username"=>$sub_member->username, "rank"=>$sub_member->rank, "crank"=>$sub_member->crank, "fwallet"=>0, "primary_id"=>$sub_member->primary_id];
                    } 
                }
            }
        }
        return $user_list;
    }

    public static function genUniqueUsername()
    {
        $str = str_random(10);
        while (static::where("username", $str)->exists()){
            $str = str_random(10);
        }
        return $str;
    }
}
