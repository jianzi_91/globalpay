<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommCondition extends Model
{
    protected $table = 'mlm_comm_conditions';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
