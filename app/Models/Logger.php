<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Utils\Tool;

class Logger extends Model
{
    protected $table = 'mlm_logger';
    protected $primaryKey = 'id';
	
    public $timestamps = false;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'itype', 'iid', 'id1', 'id2', 'id3', 'id4', 'id5', 'id6', 'action', 'domain', 'path', 'ip', 'request_data', 'log_data', 'method', 'user_agent' , 'descr',
    ];
	
	public static function log($action, $guard, $identifiers, $descr, $log_data)
	{
        //identifiers
        $ids = [];
        $ids_length = count($identifiers)>6?6:count($identifiers);
        for($i=0; $i<$ids_length; $i++){
            $ids['id'.($i+1)] = $identifiers[$i];
        }

        //get initiator
        $initiator = Tool::getInitiator($guard);

        static::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'descr' => $descr,
            'log_data' => json_encode($log_data),
            'action' => $action,
            'request_data' => json_encode(request()->except(['_token', 'password', 'password_confirmation', 'password2', 'password2_confirmation', 'epassword', 'new_password', 'new_password_confirmation'])),
            'user_agent' => request()->header('User-Agent'),
            'method' => request()->method(),
            'domain' => request()->root(),
            'path' => request()->path(),
            'ip' => request()->ip()
        ] + $ids);
	}
	
	public static function logWithoutGuard($action, $identifiers, $descr, $log_data)
	{
        //identifiers
        $ids = [];
        $ids_length = count($identifiers)>6?6:count($identifiers);
        for($i=0; $i<$ids_length; $i++){
            $ids['id'.($i+1)] = $identifiers[$i];
        }

        //get initiator
        $initiator = Tool::getInitiator();

        static::create([
            'itype' => $initiator["itype"],
            'iid' => $initiator["iid"],
            'descr' => $descr,
            'log_data' => json_encode($log_data),
            'action' => $action,
            'request_data' => json_encode(request()->except(['_token', 'password', 'password_confirmation', 'password2', 'password2_confirmation', 'epassword', 'new_password', 'new_password_confirmation'])),
            'user_agent' => request()->header('User-Agent'),
            'method' => request()->method(),
            'domain' => request()->root(),
            'path' => request()->path(),
            'ip' => request()->ip()
        ] + $ids);
	}

    public static function retrieveChanges($old_data, $new_data)
    {
        $old_data = array_intersect_key($old_data, $new_data);
        $change_data = array_diff_assoc($old_data, $new_data);
        
        return [
            "old_data" => array_intersect_key($old_data, $change_data),
            "new_data" => array_intersect_key($new_data, $change_data)
        ];
    }
}
