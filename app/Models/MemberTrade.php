<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTrade extends Model
{
    protected $table = 'mlm_member_trades';
    protected $primaryKey = 'uid';
}