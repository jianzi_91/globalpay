<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $table = 'mlm_currency_exchange';

    protected $fillable = [
        'currency_id',
        'buy_rate',
        'sell_rate',
        'min_withdrawal_amount',
        'status',
        'admin_id'
    ];

    /**
     * Currency relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    /**
     * Admin relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    public static function findByCurrencyId($id)
    {
        $obj =  self::where('currency_id', $id)->first();

        if ($obj == null) {
            return back()->withErrors(['Currency not in exchange lists.']);
        }

        return $obj;
    }
}
