<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberWalletTransfer extends Model
{
    protected $table = 'mlm_member_wallet_transfer';
    protected $fillable = [
        'itype', 'iid', 'from_uid', 'from_wallet', 'to_uid', 'to_wallet', 'from_amount', 'to_amount', 'fee', 'remarks', 'status'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
