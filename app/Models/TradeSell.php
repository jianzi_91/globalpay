<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeSell extends Model
{
    protected $table = 'mlm_trade_sells';
    protected $guarded = ['is_locked', 'is_processed', 'fulfilled_at'];

    const STATUS = [
        'FULFILLED' => 'F',
        'PENDING' => 'P',
        'REJECTED' => 'R',
        'CANCELLED' => 'X', 
    ];

    const TYPE = [
        'SYSTEM' => 100,
        'CUSTOM' => 200,
    ];

    const PAYMENT = [
        'BANK_TRANSFER' => 'B',
        'WECHAT_PAY' => 'W',
        'ALIPAY' => 'A',
    ];

    // ---------- RELATIONSHIP ---------- //

    public function seller()
    {
        return $this->hasOne('App\Models\Member', 'id', 'uid');
    }

    public function queue()
    {
        return $this->belongsTo('App\Models\TradeQueue')->withDefault();
    }

    public function currencyCode()
    {
        return $this->hasOne('App\Models\Currency', 'code', 'currency');
    }

    public function pendingRequest()
    {
        return $this->hasManyThrough('App\Models\TradeBuy', 'App\Models\TradeQueue', 'sell_id', 'id', 'id', 'buy_id')->where('mlm_trade_queues.status', 2);
    }

    public function paidRequest()
    {
        return $this->hasManyThrough('App\Models\TradeBuy', 'App\Models\TradeQueue', 'sell_id', 'id', 'id', 'buy_id')->where('mlm_trade_queues.status', 3);
    }

    public function completedRequest()
    {
        return $this->hasManyThrough('App\Models\TradeBuy', 'App\Models\TradeQueue', 'sell_id', 'id', 'id', 'buy_id')->where('mlm_trade_queues.status', 4);
    }

    public function cancelledRequest()
    {
        return $this->hasManyThrough('App\Models\TradeBuy', 'App\Models\TradeQueue', 'sell_id', 'id', 'id', 'buy_id')->where('mlm_trade_queues.status', 5);
    }

    // ---------- END OF RELATIONSHIP ----------//

    // ---------- SCOPE ---------- //

    public function scopeCustom($query)
    {
        return $query->where('type', self::TYPE['CUSTOM']);
    }

    public function scopeSystem($query)
    {
        return $query->where('type', self::TYPE['SYSTEM']);
    }

    public function scopePending($query)
    {
        return $query->where('status', self::STATUS['PENDING']);
    }
    // ---------- END OF SCOPE ---------- //
}
