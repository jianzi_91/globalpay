<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Commission;
use App\Utils\Tool;

class Cron extends Model
{

    protected $table = 'mlm_crons';
    protected $guarded = [];
    protected $primaryKey = 'id';

	//status to code
	public static $status_to_code = [
		'success' => 10,
		'pending' => 30,
		'error' => 90,
	];

    //task type to code
    public static $task_type_to_code = [
        // routine task :before bonus
        'copy-wallet' => 1001,
        'unfreeze-wallet' => 1002,
        
        // bonus
        'calc-active-bonus' => 2001,
        'calc-passive-bonus' => 2002,

        // roi
        
        // distribute bonus / roi
        'distribute-active-bonus' => 4001,
        'distribute-passive-bonus' => 4002,

        // fwallet release
        'fwallet-release' => 5001,
    ];

    //cache for task type status
    protected $task_type_on_date_status = [];
    
    //monthly task
    protected static $monthly_task_type = [];

    protected function initiateCron($task_type, $date)
    {
        $cron_id = 0;
        $datetime = \Carbon\Carbon::now()->toDateTimeString();

        $initiator = Tool::getInitiator();
        //check if task run success
        $status = $this->checkCronTaskSuccess($task_type, $date);
        if (!$status) {
            $cron = static::create([
                "itype" => $initiator["itype"],
                "iid" => $initiator["iid"],
                "task_type" => $task_type,
                "target_date" => $date,
                "start_datetime" => $datetime,
                "status" => static::$status_to_code["pending"],
            ]);

            if ($cron) {
                $cron_id = $cron->id;
                $this->task_type_on_date_status[$cron->target_date][$cron->task_type] = $cron->status;
            }
        }

        return $cron_id;
    }

    protected function checkCronTaskSuccess($task_type, $date)
    {
        $status = false;
        $task_type_on_date_status = $this->task_type_on_date_status;

        $task_type_status = isset($task_type_on_date_status[$date][$task_type])?$task_type_on_date_status[$date][$task_type]:'';
        if (!$task_type_status) {
            $query = static::where([
                "task_type" => $task_type,
                "status" => static::$status_to_code["success"],
            ]);

            if (in_array($task_type, static::$monthly_task_type)) {
                //monthly run once
                $query = $query->where("target_date", ">=", date("Y-m-01", strtotime($date)))
                        ->where("target_date", "<=", date("Y-m-t", strtotime($date)));
            }
            else {
                //daily run once
                $query = $query->where("target_date", $date);
            }
            $cron = $query->first();

            if ($cron) {
                $task_type_status = $cron->status;
            }
        }
        if ($task_type_status == static::$status_to_code["success"]) {
            $status = true;
        }

        return $status;
    } 

    protected function updateCronTask($cron_status, $remarks, $cron_id)
    {
        $status = false;
        $errmsg = "";

        $datetime = \Carbon\Carbon::now()->toDateTimeString();
        $status = static::where([
            "id"=>$cron_id,
            "status" => static::$status_to_code["pending"],
        ])->update([
            "status"=>$cron_status,
            "remarks"=>$remarks,
            "end_datetime" => $datetime,
        ]);

        if ($status) {
            $cron = static::find($cron_id);
            $this->task_type_on_date_status[$cron->target_date][$cron->task_type] = $cron->status;
        }
        else {
            //error: cron cannot be updated
            $errmsg = "Cron #".$cron_id." cannot be updated.\n\n";
        }

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function copyWallet($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type = static::$task_type_to_code['copy-wallet'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);
        if ($cron_id) {

            //run task
            $comm = new Commission();
            $result = $comm->copyWallet($date);
            $status = $result["status"];
            $errmsg .= $result["errmsg"];
            if ($status) {
                $cron_status = static::$status_to_code["success"];
                $cron_remarks = "";
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $cron_remarks = $errmsg;
            }
            
            //record result
            $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
            $errmsg .= $result["errmsg"];    
        }

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function unfreezeWallet($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type = static::$task_type_to_code['unfreeze-wallet'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["copy-wallet"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->unfreezeWallet($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
            
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Copy Wallet is not success.\n\n";
            }
            
            //record result
            $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
            $errmsg .= $result["errmsg"];    
        }

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function calcActiveBonus($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type = static::$task_type_to_code['calc-active-bonus'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["copy-wallet"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->calcActiveBonus($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
            
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Copy Wallet is not success.\n\n";
            }
            
            //record result
            $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
            $errmsg .= $result["errmsg"];    
        }

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function calcPassiveBonus($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type = static::$task_type_to_code['calc-passive-bonus'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["copy-wallet"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->calcPassiveBonus($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
                
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Copy Wallet is not success.\n\n";
            }
            
            //record result
            $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
            $errmsg .= $result["errmsg"];    
        }

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function distributeActiveBonus($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type_to_code = static::$task_type_to_code;
        $task_type = static::$task_type_to_code['distribute-active-bonus'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);

        $monthly_check = true;
        
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["calc-active-bonus"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->distributeActiveBonus($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
                
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Calculation for active bonus is not success.\n\n";
            }
        }
                
        //record result
        $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
        $errmsg .= $result["errmsg"];  

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function distributePassiveBonus($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type_to_code = static::$task_type_to_code;
        $task_type = static::$task_type_to_code['distribute-passive-bonus'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);

        $monthly_check = true;
        
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["calc-passive-bonus"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->distributePassiveBonus($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
                
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Calculation for passive bonus is not success.\n\n";
            }
        }
                
        //record result
        $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
        $errmsg .= $result["errmsg"];  

        return ["status"=>$status, "errmsg"=>$errmsg];
    }

    public function fwalletRelease($date)
    {
        $status = false;
        $errmsg = "";
        
        $task_type_to_code = static::$task_type_to_code;
        $task_type = static::$task_type_to_code['fwallet-release'];
        
        //initiate task
        $cron_id = $this->initiateCron($task_type, $date);
        
        if ($cron_id) {
            if ($this->checkCronTaskSuccess(static::$task_type_to_code["fwallet-release"], $date)) {

                //run task
                $comm = new Commission();
                $result = $comm->distributePassiveBonus($date);
                $status = $result["status"];
                $errmsg .= $result["errmsg"];
                if ($status) {
                    $cron_status = static::$status_to_code["success"];
                    $cron_remarks = "";
                }
                else {
                    $cron_status = static::$status_to_code["error"];
                    $cron_remarks = $errmsg;
                }
                
            }
            else {
                $cron_status = static::$status_to_code["error"];
                $errmsg .= "Calculation for passive bonus is not success.\n\n";
            }
        }
                
        //record result
        $result = $this->updateCronTask($cron_status, $errmsg, $cron_id);
        $errmsg .= $result["errmsg"];  

        return ["status"=>$status, "errmsg"=>$errmsg];
    }
}
