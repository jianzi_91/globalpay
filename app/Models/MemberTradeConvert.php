<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTradeConvert extends Model
{
    protected $table = 'mlm_member_trade_convert';
    protected $primaryKey = 'uid';
    protected $guarded = [];
}