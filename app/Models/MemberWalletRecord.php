<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberWalletRecord extends Model
{
    protected $table = 'mlm_member_wallet_record';
    protected $fillable = [
        'itype', 'iid', 'uid', 'wallet', 'type', 'tuid', 'trans_code', 'amount', 'descr', 'descr_code', 'status'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;

    const TYPE = [
    	'CREDIT' => 'c',
    	'DEBIT' => 'd',
    ];

    const ITYPE = [
    	'USER' => 100,
    	'ADMIN' => 900,
    ];

    const WALLET = [
    	'CASH' => 'cwallet',
    	'HOT' => 'awallet',
    	'COLD' => 'awallet2',
    	'CHARITY' => 'dowallet',
    	'SPEND' => 'swallet',
    	'REGISTER' => 'rwallet',
    ];
}
