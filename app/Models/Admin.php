<?php

namespace App\Models;

use Exception;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = "admins";

    protected $table = 'mlm_admins';
    protected $primaryKey = 'id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'country', 'status', 'itype', 'iid', 'editor_type', 'editor_id', "privileges"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static $user_code = 900;

    //account status
    public static $status_to_code = [
		'active' => 10,
		'suspended' => 30,
		'terminated' => 90,
    ];
    
    public static $privileges_to_code = [
        "members/list" => "1000",
        "members/create" => "1010",
        "members/edit-profile" => "1020",
        // "members/edit-sub-profile" => "1030",
        // "members/edit-network" => "1040",
        // "members/edit-franchise" => "1050",
        // "members/send-welcome-email" => "1060",
        // "members/sa1-list" => "1070",

        "members/edit-passwords" => "1110",
        "members/view-passwords" => "1120",

        // "genealogy/sponsor" => "1200",
        "genealogy/placement" => "1300",
        // "uplines" => "1400",
        // "networks/check-group" => "1510",
        
        // "activations/list" => "2000",
        // "bvfree/activations/add" => "2010",
        // "loan/activations/add" => "2020",
        // "special/activations/add" => "2030",

        // "group-activations" => "2100",
        // "personal-activations" => "2200",
        // "small-group-activations-summary" => "2300",
        // "activations-summary" => "2900",

        // "activations/cancel-list" => "3000",
        // "activations/cancel" => "3010",

        // "loan-activations" => "4000", 
        // "loan-activations/settlement" => "4010",
        // "loan-activations/distribute-loan-activation" => "4020",

        "wallet-records" => "5000",
        "wallets/edit" => "5010",

        // "withdrawals" => "6000",
        // "withdrawals/edit" => "6010",

        "commissions" => "7000",
        // "commissions-analysis" => "7100",
        "commissions-summary" => "7900",

        // "pairing-history" => "8000",
        // "paired-max-summary" => "8010",

        "news" => "9000",
        // "exchange" => "10000",

        "trade/history/queue" => "11020",
        "trade/history/sell" => "11021",
        "trade/history/buy" => "11022",
        // "trade/history/all-buy" => "11023",
        // "trade/history/non-trade" => "11024",
        // "trade/summary" => "11030",
        // "trade/balance" => "11040",
        // "trade/edit-unit" => "11010",

        "report/member-country" => "12000",
        "report/member-wallet" => "13000",
        // "report/trade-allocation" => "14000",
        // "report/sales-commission" => "15000",

        "loggers" => "16000",

        "adminusers" => "99000",
    ];

    public function isMaster()
    {
        return $this->admin_type == 1;
    }

    public function isDeveloper()
    {
        return $this->admin_type == 2;
    }

    public function isFullAccess()
    {
        try {
            return $this->isMaster() || $this->isDeveloper();
        } catch (Exception $e) {
            return false;
        }
    }
}
