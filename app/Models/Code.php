<?php

namespace App\Models;

use Exception;

use App\Models\Member;
use App\Sms\SmsService;
use App\Repositories\CountryRepo;
use App\Utils\SmsTool;
use App\Utils\StringTool;
use App\Utils\DateTimeTool;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'mlm_codes';
    protected $guarded = [];
    public $timestamps = false;

    public static function checkTac($tac_code, $mobile, $action)
    {
        $code = Code::where([
            'mobile' => $mobile,
            'action' => $action,
            'used_at' => null,
            'is_invalid' => false
        ])
        ->where('expired_datetime', '<>', null)
        ->orderBy('expired_datetime', 'desc')
        ->first();
        
        if (!$code || 
        DateTimeTool::systemDateTime() > $code->expired_datetime) {
            return [
                'status' => 0,
                'msg' => 'not found',
            ];
        }

        // compare tac with input
        if ($tac_code !== $code->verification_code) {
            return [
                'status' => 0,
                'msg' => 'not matched',
            ];
        }

        $code->used_at = DateTimeTool::systemDateTime();
        $code->save();

        return [
            "status" => 1,
            "msg" => "SUCCESS",
        ];
    }

    public static function sendTac($mobile, $country, $action, $extra = [])
    {
        $status = 1;
        $data = ["mobile" => $mobile, "country" => $country, "action" => $action];
        $member_table = with(new Member)->getTable();
        $errmsg = [];

        $niceNames = [
            'mobile' => trans('field.user.mobile'),
            'country' => trans('field.user.country'),
            'action' => trans('field.code.action'),
        ];
        $validator = validator($data, [
            'mobile' => 'required|string',
            'country' => 'nullable|string',
            'action' => 'required|string',
        ], [], $niceNames);

        if ($validator->fails()) {
            return [
                'status' => 0,
                'errmsg' => $validator->errors()->all()
            ];
        }

        // specific validation for each action 
        switch ($data["action"]) {
            case "register":        
            case "update_mobile":        
                $validator = validator($data, [
                    'mobile' => 'required|unique:'.$member_table.',mobile',
                ], [], $niceNames);

                if ($validator->fails()) {
                    return [
                        'status' => 0,
                        'errmsg' => $validator->errors()->all()
                    ];
                }
                break;
            case "verify_mobile":
            case "reset_password":

                if (!Member::where("mobile", $data["mobile"])->exists()) {
                    return [
                        'status' => 0,
                        'errmsg' => [trans("validation.in", ["attribute" => trans("field.user.mobile")])],
                    ];
                }

                if ($data["action"] == "reset_password") {
                    $data["country"] = Member::where("mobile", $data["mobile"])->value("country");
                }
                break;
            default:
                return [
                    'status' => 0,
                    'errmsg' => [trans("validation.in", ["attribute" => trans("field.code.action")])],
                ];
        }

        $sent_datetime = Code::where([
            'mobile' => $data["mobile"],
            'action' => $data["action"],
            'used_at' => null,
            'is_invalid' => false
        ])
        ->where("sent_at", '<>', null)
        ->orderBy('sent_at', 'desc')
        ->value('sent_at');

        if ($sent_datetime) {
            $sent_timestamp = strtotime($sent_datetime);
            $time_diff = time() - $sent_timestamp;

            if ($time_diff < config('tac.resend_interval')) {
                $status = 0;
                $errmsg[] = trans('sms_tac.error.resend');

                return compact('status', 'mobile', 'errmsg');
            }
        }

        // generate verify code
        $tac = SmsTool::generateTac();
        $message = trans('sms_tac.sms.'.$data["action"], ['tac' => $tac]);
		$phone_code = CountryRepo::all_member_selectable_phone_code();
        $mobile_no = ltrim($data["mobile"], '0');
        $phone_code = isset($phone_code[$data["country"]]) ? $phone_code[$data["country"]]->phone_code : "";
        if (substr($mobile_no, 0, strlen($phone_code)) != $phone_code) {
            $mobile_no = $phone_code . $mobile_no;
        }
        // dump(app()->getLocale());
        // dd($message);
        // save device verification tac
        try {
            // invalidate previous code
            Code::where([
                'mobile' => $data["mobile"],
                'action' => $data["action"],
                'used_at' => null,
                'is_invalid' => false
            ])
            ->where('expired_datetime', '<>', null)
            ->update(['is_invalid' => true]);

            // create new code
            $code = Code::create([
                'mobile' => $data["mobile"],
                'action' => $data["action"],
                'verification_code' => $tac,
                'message' => $message
            ]);
        } catch (Exception $e) {
            $status = 0;
            $errmsg[] = trans('sms_tac.error.system_error');

            return compact('status', 'mobile', 'errmsg');
        }

        // send an sms with verification code to target mobile
        try {
            $smsService = new SmsService();

            // if sending is successful, record sent time
            
            // if (config('app.debug')) {
            //     $datetime = DateTimeTool::systemDateTime();
            //     $code->sent_at = $datetime;
            //     $code->expired_datetime = date('Y-m-d H:i:s', strtotime($datetime) + config('tac.ttl'));
            //     $code->save();
            // }
            // else
            if ($smsService->send($mobile_no, $message, $tac, $data["action"])) {
                $datetime = DateTimeTool::systemDateTime();
                $code->sent_at = $datetime;
                $code->expired_datetime = date('Y-m-d H:i:s', strtotime($datetime) + config('tac.ttl'));
                $code->save();
            }
            else {
                throw new Exception("Transmission Error");
            }
        } catch (Exception $e) {
            $status = 0;
            $errmsg[] = trans('sms_tac.error.transmission');

            return compact('status', 'errmsg');
        }
        return [
            "status" => 1,
            "mobile" => $mobile_no,
            "message" => trans("sms_tac.msg.success", [
                "mobile" => StringTool::censored($mobile_no),
                "mobile_field" => trans("field.user.mobile"),
                "attribute" => trans("field.code.verification_code"),
                ]),
            "server_date" => $code->sent_at,
            "expired_at" => $code->expired_datetime,
        ];
    }
}