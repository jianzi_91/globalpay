<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberPartnerTransfer extends Model
{
    protected $table = 'mlm_member_partner_transfer';
    protected $fillable = [
        'itype', 'iid', 'editor_type', 'editor_id', 'from_uid', 'from_wallet', 'from_amount', 'to_ref', 'to_wallet', 'to_amount', "remarks", 'status'
    ];
    protected $primaryKey = 'id';

    public static $status_to_code = [
        "confirmed" => 10,
        "pending" => 30,
        "processing" => 50,
        "cancelled" => 70,
        "error" => 90,
    ];
}
