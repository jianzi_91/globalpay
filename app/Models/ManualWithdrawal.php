<?php

namespace App\Models;

use App\Interfaces\ManualWithdrawalCode;
use Illuminate\Database\Eloquent\Model;

class ManualWithdrawal extends Model implements ManualWithdrawalCode
{
    protected $table = 'mlm_manual_withdrawal';

    protected $guarded = [];

    public static function hasPending($user_id)
    {
        return static::where([
            'uid' => $user_id,
            'status' => static::WITHDRAWAL_PENDING,
        ])->exists();
    }

    static function statusTypes()
    {
        return [
            ManualWithdrawalCode::WITHDRAWAL_PENDING => trans("field.withdrawal.pending"),
            ManualWithdrawalCode::WITHDRAWAL_COMPLETE => trans("field.withdrawal.completed"),
            ManualWithdrawalCode::WITHDRAWAL_CANCELLED => trans("field.withdrawal.cancelled")
        ];
    }
}