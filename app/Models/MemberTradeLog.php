<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTradeLog extends Model
{
    protected $table = 'mlm_member_trade_log';
}
