<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownlineSales extends Model
{
    protected $table = 'mlm_downline_sales';
    protected $fillable = [];
    protected $primaryKey = 'id';
}
