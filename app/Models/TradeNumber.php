<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeNumber extends Model
{
    protected $table = 'mlm_trade_numbers';
    protected $guarded = [];
}