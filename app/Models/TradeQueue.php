<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeQueue extends Model
{
    protected $table = 'mlm_trade_queues';
    protected $guarded = [];

    const STATUS = [
        'INITIATED' => 1,
        'PENDING' => 2,
        'PAID' => 3,
        'COMPLETE' => 4,
        'CANCELLED' => 5,
    ];

    // Define relationship

    public function tradeSell()
    {
        return $this->hasOne('App\Models\TradeSell', 'id', 'sell_id');
    }

    public function tradeBuy()
    {
        return $this->hasOne('App\Models\TradeBuy', 'id', 'buy_id');
    }

    public function seller()
    {
        return $this->hasOne('App\Models\Member', 'id', 'sell_uid');
    }

    public function buyer()
    {
        return $this->hasOne('App\Models\Member', 'id', 'buy_uid');
    }

    // Define scope
    public function scopePending($query)
    {
        return $query->where('status', self::STATUS['PENDING']);
    }

    public function scopePaid($query)
    {
        return $query->where('status', self::STATUS['PAID']);
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [self::STATUS['PENDING'],self::STATUS['PAID']]);
    }

    public function scopeInactive($query)
    {
        return $query->whereIn('status', [self::STATUS['COMPLETE'],self::STATUS['CANCELLED']]);
    }
}
