<?php

namespace App\Models;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    protected $table = 'mlm_withdrawal';
    protected $fillable = [
        'itype', 'iid', 'editor_type', 'editor_id', 'uid', "wallet", "amount", "receivable_amount", "fee", "currency", "currency_amount", "currency_receivable_amount", "currency_fee", "bank_country", "bank_name", "bank_branch_name", "bank_province_name", "bank_payee_name", "bank_acc_no", "bank_sorting_code", "bank_iban", "remarks", "aremarks", "status", "paid_at", "paid_by_type", "paid_by_id",
    ];
    protected $primaryKey = 'id';

    //status to code
    public static $status_to_code = [
        "paid" => 10,
        "pending" => 30,
        "processing" => 50,
        "cancelled" => 70,
    ];

    public static $withdraw_wallet_fee_percent = [
        "cwallet" => 5
    ];

    public static function calcFeeByAmount($amount, $wallet)
    {
        $fee_percent = static::$withdraw_wallet_fee_percent[$wallet];
        return ceil($amount*$fee_percent)/100;
    }

	public static function checkUserBankInformation($user_id)
	{
		$status = false;
		$user = Member::find($user_id);

        if ($user) {
            if (!trim($user->name) 
                || !trim($user->country) 
                || !trim($user->bank_name) 
                || !trim($user->bank_branch_name) 
                || !trim($user->bank_acc_no) 
                ) {
                $status = false;
            }
            else{
                $status = true;
            }
        }
		return $status;
	}

    static function getTotalPendingByUserId($user_id)
    {
        return static::where('uid', $user_id)
            ->where(function($q) {
                $q->where('status', static::$status_to_code['pending'])
                    ->orWhere('status', static::$status_to_code['processing']);
            })
            ->sum('amount');
    }

    static function getTotalPaidByUserId($user_id)
    {
        return static::where([
            'uid' => $user_id,
            'status' => static::$status_to_code['paid']
        ])->sum('amount');
    }
}
