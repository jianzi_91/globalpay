<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberStat extends Model
{
    protected $table = 'mlm_member_stats';
    protected $fillable = [
        'uid', 'personal_acc_amt', 'personal_acc_price', 'leg1_acc_amt', 'leg2_acc_amt', 'leg1_acc_price', 'leg2_acc_price', 'ref_acc_price', 'ref_acc_amt', 'ref_downline', 'up_downline', 'today_leg1_price', 'today_leg2_price', 'today_leg1_amt', 'today_leg2_amt', 'today_ref_price', 'today_ref_amt',  'leg1_cf',  'leg2_cf',
    ];
    protected $primaryKey = 'id';
}
