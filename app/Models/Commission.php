<?php

namespace App\Models;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;

use App\Models\Member;
use App\Models\Setting;
use App\Models\TransInfo;

class Commission extends Model
{
    protected $table = 'mlm_comms';
    protected $guarded = [];
    protected $primaryKey = 'id';

    public static $status_to_code = [
        "distributed" => 10,
        "pending" => 30,
        "error" => 90,
    ];

    public static $type_to_code = [
        'active-bonus' => '1001',
        'passive-bonus' => '2001',
    ];

    // bonus comm type
    public static $bonus_comm_list = [
        '1001', '2001'
    ];

    //bonus wallet distribution
    public static $bonus_wallet_distribution = [
        "default" => ["swallet" => 5],
    ];

	//Identifiers
	protected $_identifiers = [];

	protected function _setIdentifiers($id)
	{
		$this->_identifiers[] = $id;
	}

	protected function _getIdentifiers()
	{
		$identifiers = $this->_identifiers;
		$this->_identifiers = [];
		return $identifiers;
	}

    public function copyWallet($date)
    {
		$_guard = null;

        //set general variable
        $status = true;
        $errmsg = "";

        $return = DB::select("CALL `usp_copy_wallet`(?)", [$date])[0];
        $return = collect($return);

        $log_msg = "Copy Wallet is successully run on ".$date;
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }

    public function unfreezeWallet($date)
    {
		$_guard = null;

        // set general variable
        $status = true;
        $errmsg = "";
        $unfreeze_wallet_percent = 5;
        $settings = Setting::first();
        if ($settings && $settings->unfreeze_wallet_percent > 0) {
            $unfreeze_wallet_percent = $settings->unfreeze_wallet_percent;
        }
        // dd($unfreeze_wallet_percent);

        $return = DB::select("CALL `usp_convert_cold_to_hot_wallet`(?,?)", [$unfreeze_wallet_percent, $date])[0];
        $return = collect($return);

        $log_msg = "Unfreeze Wallet is successully run on ".$date." ".$unfreeze_wallet_percent;
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }

    public function calcActiveBonus($date)
    {
		$_guard = null;

        //set general variable
        $status = true;
        $errmsg = "";

        $comm_type = static::$type_to_code["active-bonus"];
        $return = DB::select("CALL `usp_calc_active_bonus`(?,?)", [$date, $comm_type])[0];
        $return = collect($return);

        $log_msg = "Calculate Active Bonus is successully run on ".$date;
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }

    public function calcPassiveBonus($date)
    {
		$_guard = null;

        //set general variable
        $status = true;
        $errmsg = "";

        $comm_type = static::$type_to_code["passive-bonus"];
        $return = DB::select("CALL `usp_calc_passive_bonus`(?,?)", [$date, $comm_type])[0];
        $return = collect($return);

        $log_msg = "Calculate Passive Bonus is successully run on ".$date;
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }

    public function distributeActiveBonus($date)
    {
		$_guard = null;

        //set general variable
        $status = true;
        $errmsg = "";

        $comm_type = static::$type_to_code["active-bonus"];
        $bonus_wallet_distribution = isset(static::$bonus_wallet_distribution[$comm_type]) ? static::$bonus_wallet_distribution[$comm_type] : static::$bonus_wallet_distribution["default"];
        $settings = Setting::first();
        if ($settings && $settings->bonus_swallet_percent > 0) {
            $bonus_wallet_distribution["swallet"] = $settings->bonus_swallet_percent;
        }

        $return = DB::select("CALL `usp_distribute_comm`(?,?,?,?,?,?)", [
            $date, 
            $date, 
            TransInfo::$trans_type_to_code["commission-distribution"], 
            $comm_type, 
            $bonus_wallet_distribution["swallet"], 
            Member::$wallet_uid
        ])[0];
        $return = collect($return);

        $log_msg = "Distribute Active Bonus is successully run on ".$date. " ". $bonus_wallet_distribution["swallet"];
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }

    public function distributePassiveBonus($date)
    {
		$_guard = null;

        //set general variable
        $status = true;
        $errmsg = "";

        $comm_type = static::$type_to_code["passive-bonus"];
        $bonus_wallet_distribution = isset(static::$bonus_wallet_distribution[$comm_type]) ? static::$bonus_wallet_distribution[$comm_type] : static::$bonus_wallet_distribution["default"];
        $settings = Setting::first();
        if ($settings && $settings->bonus_swallet_percent > 0) {
            $bonus_wallet_distribution["swallet"] = $settings->bonus_swallet_percent;
        }

        $return = DB::select("CALL `usp_distribute_comm`(?,?,?,?,?,?)", [
            $date, 
            $date, 
            TransInfo::$trans_type_to_code["commission-distribution"], 
            $comm_type, 
            $bonus_wallet_distribution["swallet"], 
            Member::$wallet_uid
        ])[0];
        $return = collect($return);

        $log_msg = "Distribute Passive Bonus is successully run on ".$date. " ". $bonus_wallet_distribution["swallet"];
        Logger::logWithoutGuard(__METHOD__, $this->_getIdentifiers(), $log_msg, ['data'=>['date'=>$date]]);

        return [
            "status" => $return["status"],
            "errmsg" => $return["sql_state"]." ".$return["stage"]." ".$return["error_msg"],
        ];
    }
}
