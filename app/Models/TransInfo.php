<?php

namespace App\Models;

use Exception;

class TransInfo
{
    //transaction type
    public static $trans_type_to_code = [
        'wallet-topup' => '1001',
        'wallet-deduct' => '1002',
        'package-purchase' => '1003',
        'wallet-transfer' => '1004',
        'commission-distribution' => '1005',
        'roi-deduct' => '1006',
        'roi-distribution' => '1007',
        'wallet-convert' => '1008',
        'wallet-withdraw' => '1009',
        'package-cancel' => '1010',
        'micool-transfer' => '1011',
        'package-confirmed-cancel' => '1012',
        'package-early-settlement' => '1013',
        'package-special-activation' => '1014',
        'panda-coin-transfer' => '1015',
        'trade-buy' => '1020',
        'trade-sell' => '1030',
        'trade-maintenance' => '1040',
        'trade-charity' => '1041',
    ];

    public static function getDescrFromCode($descr, $guard="")
    {
        $msg = $descr->descr;
        $descr_json = json_decode($descr->descr_json, true);
        
        //translate to readable description to user
        try {
            switch ($descr->trans_code) {
                case static::$trans_type_to_code['wallet-topup']:
                    if ($guard == "admins") {
                        $msg = trans("field.wallet_record.remarks").": ".($descr->remarks ?: "-");
                        $msg .= "\n".trans("field.wallet_record.aremarks").": ".($descr->aremarks ?: "-");
                    }
                    else {
                        $msg = $descr->remarks ?: "-";
                    }
                    break;
                case static::$trans_type_to_code['wallet-deduct']:
                case static::$trans_type_to_code['package-early-settlement']:
                    if ($guard == "admins") {
                        $msg = trans("field.wallet_record.remarks").": ".($descr->remarks ?: "-");
                        $msg .= "\n".trans("field.wallet_record.aremarks").": ".($descr->aremarks ?: "-");
                    }
                    else {
                        $msg = $descr->remarks ?: "-";
                    }
                    break;
                case static::$trans_type_to_code['package-purchase']:
                    if ($descr_json['trans-type'] == 'debt') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.debt', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    elseif ($descr_json['trans-type'] == 'product') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.product', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    else {
                        //purchase
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.purchase', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    break;
                case static::$trans_type_to_code['wallet-transfer']:
                    if ($descr_json['role'] == 'from') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.from', array(
                            'username' => $descr_json['to_username'],
                            'wallet' => trans('general.wallet.'.$descr_json['to_wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    else {
                        //to
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.to', array(
                            'username' => $descr_json['from_username'],
                            'wallet' => trans('general.wallet.'.$descr_json['from_wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    break;
                case static::$trans_type_to_code['commission-distribution']:
                    if ($descr_json['trans-type'] == 'deduct-debt') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.deduct-debt', array(
                            'username' => $descr_json['username'],
                            'commission' => trans('general.commission.type.'.$descr_json['comm_type']),
                            'date' => $descr_json['date'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 4),
                        ));

                    }
                    else {
                        //bonus
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.bonus', array(
                            'username' => $descr_json['username'],
                            'commission' => trans('general.commission.type.'.$descr_json['comm_type']),
                            'date' => $descr_json['date'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 4),
                        ));
                    }
                    break;
                case static::$trans_type_to_code['roi-deduct']:
                    $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code, array(
                        'username' => $descr_json['username'],
                        'commission' => trans('general.commission.type.'.$descr_json['comm_type']),
                        'package' => trans('general.activation.title.'.$descr_json['package_code']),
                        'date' => isset($descr_json['date'])?$descr_json['date']:'',
                        'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                        'amount' => number_format($descr_json['amount'], 4),
                    ));
                    break;
                case static::$trans_type_to_code['roi-distribution']:
                    //bonus
                    $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.distribute', array(
                        'username' => $descr_json['username'],
                        'commission' => trans('general.commission.type.'.$descr_json['comm_type']),
                        'date' => $descr_json['date'],
                        'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                        'amount' => number_format($descr_json['amount'], 4),
                    ));
                    break;
                case static::$trans_type_to_code['wallet-convert']:
                    if ($descr_json['role'] == 'from') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.from', array(
                            'username' => $descr_json['to_username'],
                            'wallet' => trans('general.wallet.'.$descr_json['from_wallet']),
                            'wallet2' => trans('general.wallet.'.$descr_json['to_wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    else {
                        //to
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.to', array(
                            'username' => $descr_json['from_username'],
                            'wallet' => trans('general.wallet.'.$descr_json['to_wallet']),
                            'wallet2' => trans('general.wallet.'.$descr_json['from_wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    break;
                case static::$trans_type_to_code['wallet-withdraw']:
                    if ($descr_json['trans-type'] == 'refund') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.refund', array(
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    else {
                        //withdraw
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.withdraw', array(
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    break;
                case static::$trans_type_to_code['package-cancel']:
                    if ($descr_json['trans-type'] == 'refund') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.refund', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    else {
                        //withdraw
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.product', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    break;
                case static::$trans_type_to_code['micool-transfer']:
                    if ($descr_json['trans-type'] == 'cancel-from') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.cancel-from', array(
                            'from_wallet' => trans('general.wallet.'.$descr_json['from_wallet']),
                            'to_wallet' => trans('partner.micool.wallet.'.$descr_json['to_wallet']),
                            'username' => $descr_json['username'],
                            'to_ref' => $descr_json["to_ref"],
                            'partner' => trans('partner.micool.name'),
                            'from_amount' => number_format($descr_json['from_amount']),
                            'to_amount' => number_format($descr_json['to_amount']),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    else {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.from', array(
                            'from_wallet' => trans('general.wallet.'.$descr_json['from_wallet']),
                            'to_wallet' => trans('partner.micool.wallet.'.$descr_json['to_wallet']),
                            'username' => $descr_json['username'],
                            'to_ref' => $descr_json["to_ref"],
                            'partner' => trans('partner.micool.name'),
                            'from_amount' => number_format($descr_json['from_amount']),
                            'to_amount' => number_format($descr_json['to_amount']),
                            'remarks' => $descr->remarks ?: '-',
                        ));
                    }
                    break;
                case static::$trans_type_to_code['package-confirmed-cancel']:
                    if ($descr_json['trans-type'] == 'refund') {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.refund', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    else {
                        //withdraw
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.product', array(
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['username'],
                            'wallet' => trans('general.wallet.'.$descr_json['wallet']),
                            'amount' => number_format($descr_json['amount'], 2),
                        ));
                    }
                    break;
                case static::$trans_type_to_code["ebond-purchase"]:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.buy', [
                            "price" => isset($descr_json["price"]) ? number_format($descr_json["price"], config('trade.decimal_point')) : null,
                        ]);

                        if ($descr_json['trans-type'] == 'special-account') {
                            $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.special-account', [
                                "price" => isset($descr_json["price"]) ? number_format($descr_json["price"], config('trade.decimal_point')) : null,
                                "qty" => isset($descr_json["qty"]) ? number_format($descr_json["qty"]) : null,
                            ]);
                        }
                        elseif ($descr_json['trans-type'] == 'package-auto-buy') {
                            $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.package-auto-buy', [
                                "price" => isset($descr_json["price"]) ? number_format($descr_json["price"], config('trade.decimal_point')) : null,
                                "qty" => isset($descr_json["qty"]) ? number_format($descr_json["qty"]) : null,
                            ]);
                        }
                        elseif ($descr_json['trans-type'] == 'comm-auto-buy') {
                            $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.comm-auto-buy', [
                                "price" => isset($descr_json["price"]) ? number_format($descr_json["price"], config('trade.decimal_point')) : null,
                                "qty" => isset($descr_json["qty"]) ? number_format($descr_json["qty"]) : null,
                            ]);
                        }
                    }
                    break;
                case static::$trans_type_to_code['package-special-activation']:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.product', [
                            'package' => trans('general.activation.title.'.$descr_json['package_code']),
                            'username' => $descr_json['to_username'],
                        ]);
                    }
                    break;
                case static::$trans_type_to_code['panda-coin-transfer']:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.topup', [
                            'amount' => $descr_json['amount'],
                        ]);
                    }
                    break;
                case static::$trans_type_to_code['ebond-split']:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.ebond-split', [
                            'amount' => number_format($descr_json['amount']),
                            'wallet' => trans("general.wallet.".$descr_json["wallet"]),
                            'trade_name' => trans("system.trade_name"),
                            'from_price' => $descr_json["from-share-price"],
                            'to_price' => $descr_json["share-price"],
                            'formula' => $descr_json["share-unit"] . " x " . $descr_json["split-by"] . " x " . $descr_json["share-price"] ,
                        ]);
                    }    
                break;
                case static::$trans_type_to_code['trade-buy']:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.ebond-split', [
                            'amount' => number_format($descr_json['amount']),
                            'wallet' => trans("general.wallet.".$descr_json["wallet"]),
                            'trade_name' => trans("system.trade_name"),
                            'from_price' => $descr_json["from-share-price"],
                            'to_price' => $descr_json["share-price"],
                            'formula' => $descr_json["share-unit"] . " x " . $descr_json["split-by"] . " x " . $descr_json["share-price"] ,
                        ]);
                    }    
                break;
                case static::$trans_type_to_code['trade-sell']:
                    if ($descr_json) {
                        $msg = trans('general.wallet-record.trans_code_descr.'.$descr->trans_code.'.ebond-split', [
                            'amount' => number_format($descr_json['amount']),
                            'wallet' => trans("general.wallet.".$descr_json["wallet"]),
                            'trade_name' => trans("system.trade_name"),
                            'from_price' => $descr_json["from-share-price"],
                            'to_price' => $descr_json["share-price"],
                            'formula' => $descr_json["share-unit"] . " x " . $descr_json["split-by"] . " x " . $descr_json["share-price"] ,
                        ]);
                    }    
                break;
                default:
                    break;
            }
        } catch (Exception $e) {
        }

        return $msg;
    }
}
