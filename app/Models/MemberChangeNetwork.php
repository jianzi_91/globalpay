<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberChangeNetwork extends Model
{
    protected $table = 'mlm_member_change_network';
    protected $fillable = ["uid", "from_up", "from_pos", "to_up", "to_pos", "network", "tdate"];
    protected $primaryKey = 'id';
}
