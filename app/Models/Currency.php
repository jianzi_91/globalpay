<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'mlm_currencies';

    /**
     * Exchange relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function exchange()
    {
        return $this->hasOne('App\Models\Exchange');
    }

    public function tradeBuy()
    {
        return $this->belongsTo('App\Models\TradeBuy');
    }

    public function tradeSell()
    {
        return $this->belongsTo('App\Models\TradeSell');
    }

    const SYMBOL = [
        'CNY' => '¥',
    ];
    /**
     * Convert currency
     *
     * @param $amount
     * @param $to_currency
     * @param null $from_currency
     */
    public static function convert($amount, $to_currency, $from_currency = null)
    {
        if ($to_currency == $from_currency) {
            return $amount;
        }

        $to = Exchange::findByCurrencyId($to_currency);

        if ($to->status == 0) {
            abort(404);
        }

        $base_currency_id = self::getBaseCurrency()->id;

        if ($from_currency == null || $from_currency == $base_currency_id) {
            return $amount * $to->sell_rate;
        }

        $from = Exchange::findByCurrencyId($from_currency);

        if ($from->status == 0) {
            abort(404);
        }

        if ($to_currency == $base_currency_id) {
            return $amount * $from->buy_rate;
        }

        return ($amount * $from->buy_rate) * $to->sell_rate;
    }

    public static function findByCode($code)
    {
        return self::where('code', $code)->first();
    }

    public static function getBaseCurrency()
    {
        $base_currency = 'USD';

        return self::findByCode($base_currency);
    }
}
