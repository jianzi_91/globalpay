<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class MemberRelationship extends Model
{
    protected $table = 'mlm_member_relationships';
    protected $guarded = [];

    /* Relationships */
    public function uplineAccount()
    {
        return $this->belongsTo('App\Models\Member', 'user_id');
    }

    public function downlineAccount()
    {
        return $this->belongsTo('App\Models\Member', 'downline_id');
    }

    public function downline_info()
    {
        return $this->belongsTo('App\Models\Member', 'downline_id', 'id');
    }
    /* End Relationships */

    public static function populate()
    {
        ini_set('max_execution_time', 0);

        $time_start = microtime(true);
        $return = DB::select("CALL usp_populate_member_relationships()")[0];
        $return = collect($return);

        if (!$return["status"]) {
            try {
                \Mail::raw(json_encode($return), function ($message) {
                    $message->to(config('app.developer_emails'))
                        ->subject("ERROR: ".__METHOD__);
                });
            }
            catch (\Exception $e) {}
            
            throw new \Exception();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        // dump($time);
    }

    public static function getUpline($downline_id)
    {
        return static::where('downline_id', $downline_id)->get();
    }

    public static function isDownlineOf($user_id, $downline_id)
    {
        return static::where([
            'user_id' => $user_id,
            'downline_id' => $downline_id
        ])->exists();
    }
}
