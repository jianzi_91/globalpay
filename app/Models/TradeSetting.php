<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeSetting extends Model
{
    protected $table = 'mlm_trade_settings';
    public $timestamps = false;
}
