<?php

namespace App\Sms;

class juhesms
{

    private $url;
    private $tpl_id;
    private $key;

    function __construct()
    {
        $this->url = 'http://v.juhe.cn/sms/send';
        $this->key='7cf55150e0f68a9decc9d1cea848929a';
        $this->tpl_id = 90667; 

    }

    public function send($mobile_no, $tac, $action)
    {
        $sendUrl = 'http://v.juhe.cn/sms/send'; //短信接口的URL

        $tpl_id = $this->tpl_id;
        if ($action == "reset_password") {
            $tpl_id = 90668;
        }

        $smsConf = array(
            'key' => $this->key, //您申请的APPKEY
            'mobile' => $mobile_no, //接受短信的用户手机号码
            'tpl_id' => $tpl_id, //您申请的短信模板ID，根据实际情况修改
            'tpl_value' => '#code#='.$tac.'&dtype=json' //您设置的模板变量，根据实际情况修改
        );

        $content = $this->juhecurl($sendUrl, $smsConf, 1); //请求发送短信
        
        if ($content)
        {
            $result = json_decode($content, true);
            $error_code = $result['error_code'];
            if ($error_code == 0)
            {
                //状态为0，说明短信发送成功
                //echo "短信发送成功,短信ID：" . $result['result']['sid'];
                return true;
            }
            elseif($error_code!=205401)
            {
                //状态非0，说明失败
                $msg = $result['reason']; 
                
                // \Mail::raw($msg,  function ($m){
                //     $m->from('info@v-life.asia', 'Vlife');
                //     $m->to('kean.yeoh@mifun.my')->subject('JuHe SMS Error!');
                // }); 
            }
            return false;
        }
        else
        {
            //返回内容异常，以下可根据业务逻辑自行修改
            //echo "请求发送短信失败";
            return false;
        }
    }

    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    private function juhecurl($url, $params = false, $ispost = 0)
    {
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($ispost)
        {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        else
        {
            if ($params)
            {
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
            }
            else
            {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        $response = curl_exec($ch);
        if ($response === FALSE)
        {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        return $response;
    }

}
