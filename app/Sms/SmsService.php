<?php

namespace App\Sms;

use App\Sms\ISms;
use App\Sms\juhesms;

class SmsService {
    private $isms, $juhesms;

    public function __construct()
    {
        $this->isms = new ISms();
        $this->juhesms = new juhesms();
    }

    public function send($mobile_no, $message, $tac = null, $action)
    {
        if (substr($mobile_no, 0, strlen("86")) === "86") {
            $mobile_no = substr($mobile_no, 2);
            $sent_flag = $this->juhesms->send($mobile_no, $tac, $action);
        }
        else {
            $sent_flag = $this->isms->send($mobile_no, $message);
        }

        return $sent_flag;
    }
}