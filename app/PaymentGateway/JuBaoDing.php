<?php

namespace App\PaymentGateway;

class JuBaoDing
{
    private $merchant_id, $md5_key, $payment_url;

    public function __construct()
    {
        $this->merchant_id = config("jubaoding.merchant_id");
        $this->md5_key = config("jubaoding.md5_key");
        $this->payment_url = config("jubaoding.payment_url");
    }

    public function getRequestPost($data)
    {
        $ref_id = $data["ref_id"];
        $amount = $data["amount"];
        $return_url = $data["return_url"];
        $notify_url = $data["notify_url"];
        $goods_subject = $data["goods_subject"];
        $goods_description = $data["goods_description"];
        $remark = $data["remark"];
        $customer_name = $data["customer_name"];

        $post_data = [
            "MerNo" => $this->merchant_id,
            "BillNo" => $this->merchant_id . $ref_id,
            "Currency" => 1,
            "Amount" => $amount,
            "ReturnURL" => $return_url,
            "NotifyURL" => $notify_url,
            "Remark" => $remark,
            "GoodsSubject" => $goods_subject,
            "GoodsDescription" => $goods_description,
            "ConsumeType" => 1,
            "CustName" => $customer_name,
        ];

        $post_data["Md5info"] = strtoupper(md5(
            $post_data['MerNo'] . (string) $post_data['BillNo'] . 
            $post_data['Currency'] . $post_data['Amount'] . $post_data['ReturnURL'] . 
            $post_data['NotifyURL'] . $this->md5_key
        ));

        return $post_data;
    }

    public function getPaymentURL()
    {
        return $this->payment_url;
    }

    public function verifyReturnData($data)
    {
        $data = array_change_key_case($data, CASE_UPPER);
        $md5_info = strtoupper(md5(
            $data["FLOWID"] . $data["BILLNO"] . $data["CURRENCY"] . 
            $data["AMOUNT"] . $data["STATUS"] . $data["BANK"] . 
            $data["REMARK"] . $this->md5_key
        ));

        return strtoupper($data["MD5INFO"]) == $md5_info;
    }
}
