<?php

namespace App\Utils;

class SmsTool {
    static function generateTac()
    {
        return str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
    }

    static function sanitizePhoneNumber($phone_number, $country_phone_code)
    {
        // trim phone no. leading 0s
        $phone_number = ltrim($phone_number, '0');

        // get country phone code length
        $phone_code_count = strlen($country_phone_code);

        // get phone no. starting substring length the same as phone code length
        $phone_number_start = substr($phone_number, 0, $phone_code_count);

        // then compare them if they are the same
        if ($phone_number_start != $country_phone_code) {
            $phone_number = $country_phone_code . $phone_number;
        }

        return $phone_number;
    }
}