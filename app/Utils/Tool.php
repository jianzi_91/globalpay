<?php

namespace App\Utils;

class Tool
{
    public static $guard = null;

    static function getInitiator($guard=null)
    {        
        $initiator = null;

        if ($guard !== null) {
            $initiator = auth($guard)->user();
        } 
        elseif (static::$guard !== null) {
            $initiator = auth(static::$guard)->user();
        }
        
        if (!$initiator) {
            if (auth("admins")->check()) {
                $initiator = auth("admins")->user();
            }
            elseif (auth("web")->check()) {
                $initiator = auth("web")->user();
            }
            else {
                $initiator = auth()->user();
            }
        } 
        
        if ($initiator) {
            $return = ["itype"=>$initiator::$user_code, "iid"=>$initiator->id];
        }
        elseif (!$initiator) {
            $return = ["itype"=>9000, "iid"=>0];
        }

        return $return;
    }

    /**
     * Set login guard
     */
    static function setInitiator($guard = null)
    {
        static::$guard = $guard;
    }
}
