<?php

namespace App\Utils;

class StringTool
{
    static function censored($str)
    {   
        $charset = mb_detect_encoding($str, 'auto');
        $str_length = mb_strlen(preg_replace('/\s+/', '', $str), $charset);
        $replace_length = floor($str_length/2);
        $start_replace = ceil($replace_length/2);
        //dd([$start_replace, $replace_length, $str_length]);
        $new_str = "";
        $char_at = 0;

        for ($i = 0; $i<mb_strlen($str, $charset) ; $i++) {
            $char = mb_substr($str, $i, 1, $charset);
            if (preg_match('/\s+/', $char)) {
                $new_str .= $char;
                continue; 
            }

            if ($char_at >= $start_replace && $char_at < $start_replace+$replace_length) {
                $new_str .= "x";
            }
            else{
                $new_str .= $char; 
            }
            $char_at++;
        }

        return $new_str;
    }

    static function sanitizePhoneNo($array)
    {
        $arr_filter = ['+', '-'];

        if (isset($array['phone_code'])) {
            $array['phone_code'] = str_replace('+', '', $array['phone_code']);
        }

        if (isset($array['mobile'])) {
            $array['mobile'] = preg_replace('/\s+/u', '', $array['mobile']);

            if (isset($array['phone_code']) && !empty($array['phone_code'])) {
                $array['mobile'] = str_replace($arr_filter, '', $array['mobile']);

                if (starts_with($array['mobile'], $array['phone_code'])) {
                    $array['mobile'] = substr($array['mobile'], strlen($array['phone_code']), strlen($array['mobile']));
                }

                $array['mobile'] = ltrim($array['mobile'], '0');
            }
        }

        if (isset($array['phone_no'])) {
            $array['phone_no'] = preg_replace('/\s+/u', '', $array['phone_no']);

            if (isset($array['phone_code']) && !empty($array['phone_code'])) {
                $array['phone_no'] = str_replace($arr_filter, '', $array['phone_no']);

                if (starts_with($array['phone_no'], $array['phone_code'])) {
                    $array['phone_no'] = substr($array['phone_no'], strlen($array['phone_code']), strlen($array['phone_no']));
                }

                $array['phone_no'] = ltrim($array['phone_no'], '0');
            }
        }

        return $array;
    }
}