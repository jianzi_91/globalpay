<?php

namespace App\Utils;

class NumberTool
{
    static function displayDynamicDecimal($amount, $min_dec = 2)
    {
        if ($amount) {
            $amount = number_format($amount, 6, ".", "");
            $temp_amount = rtrim($amount, '0');
            $dec = strlen(substr(strrchr($temp_amount, "."), 1));
            $min_dec = $min_dec>$dec? $min_dec: $dec;
        }
        return number_format($amount, $min_dec);
    }
}