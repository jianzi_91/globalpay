<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
        'password2',
        'password2_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof TokenMismatchException) {
            // handle for csrf mismatch exception
            return back()->withInput()->with('error', 'Your session has expired');
        }

        if($this->isHttpException($e))
        {
            switch ($e->getStatusCode()) 
                {
                // not found
                case 404:
                    return response()->view('errors.404', [], 404);
                    break;

                // internal error
                case '500':
                    return response()->view('errors.500', [], 500);
                    break;

                default:
                    return $this->renderHttpException($e);
                break;
            }
        }
        // if (config('app.debug') === false) {
        //     if (!$this->isHttpException($e)) {
        //         if ($developer_emails = config("app.developer_emails", null)) {
        //             try {
        //                 $json_data = [
        //                     'admin_user' => auth("admins")->check() ? auth("admins")->user()->username : null,
        //                     'web_user' => auth("web")->check() ? auth("web")->user()->username : null,
        //                     'request_data' => json_encode(request()->except(['password', 'password_confirmation', 'password2', 'password2_confirmation', 'epassword', 'new_password', 'new_password_confirmation'])),
        //                     'method' => request()->method(),
        //                     'domain' => request()->root(),
        //                     'path' => request()->path(),
        //                     "exception" => collect($e),
        //                 ];
        //                 $replace = [
        //                     '\\n' => '<br>', 
        //                     '\\\\' => '\\',
        //                     '\\/' => '/', 
        //                     '\\"' => '"' 
        //                 ];
        //                 \Mail::send([], [], function ($message) use ($developer_emails, $replace, $json_data) {
        //                     $message->to($developer_emails)
        //                     ->subject("Error in ".request()->root())
        //                     ->setBody(str_replace(array_keys($replace), array_values($replace), json_encode($json_data)), "text/html");
        //                 });
        //             } catch (Exception $e) {
        //                 // dump($e);
        //             }
        //         }
        //     }
        // }

        return parent::render($request, $e);
    }
}
