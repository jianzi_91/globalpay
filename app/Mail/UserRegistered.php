<?php

namespace App\Mail;

use App\Models\Member;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locale = config('app.locale');
        
        if ($locale == 'zh-cn') {
            $locale = str_replace('-','_',$locale);
        }

        return $this->from(config('mail.from'))
            ->subject(trans('email.register.subject'))
            ->view('member.emails.member-registration')
            ->with([
                'username' => $this->member->username,
                'name' => $this->member->name,
                'nid' => $this->member->nid,
                'mobile' => $this->member->mobile,
                'email' => $this->member->email,
                'country' => $this->member->countryInfo->{'country_'.$locale},
            ]);
    }
}
