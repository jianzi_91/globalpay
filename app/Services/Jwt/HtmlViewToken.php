<?php

namespace App\Services\Jwt;

use App\Models\AccessToken;
use App\Models\Member;

class HtmlViewToken extends Token
{
    public function generateNewToken(Member $member)
    {
        $this->generateAccessToken($member);

        return $this->getAccessToken();
    }

    public function getUserFromToken($jwt)
    {
        $token = AccessToken::findOrFail($this->retrievePayload($jwt)->jti);

        $user = $token->member;

        // OTP token
        $token->remove();

        return $user;
    }
}