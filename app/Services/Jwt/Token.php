<?php

namespace App\Services\Jwt;

use App\Models\AccessToken;
use App\Models\Member;
use App\Models\RefreshToken;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class Token
{
    protected $accessToken;
    protected $refreshToken;

    protected function getSecretKey()
    {
        return config('jwt.secret_key');
    }

    public function getAccessTokenTtl()
    {
        return config('jwt.access_token_ttl');
    }

    public function getRefreshTokenTtl()
    {
        return config('jwt.refresh_token_ttl');
    }

    public function generateAccessToken(Member $member)
    {
        $this->setAccessToken($member);

        return $this;
    }

    public function retrievePayload($token)
    {
        $payload = JWT::decode($token, $this->getSecretKey(), ['HS256']);

        $this->validatePayload($payload);

        return $payload;
    }

    protected function generateIdentifier()
    {
        return bin2hex(random_bytes(40));
    }

    protected function getExpireAt()
    {
        return Carbon::now()->addMinutes($this->getAccessTokenTtl())->toDateTimeString();
    }

    protected function getRefreshTokenExpireAt()
    {
        return Carbon::now()->addMinutes($this->getRefreshTokenTtl())->toDateTimeString();
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    protected function setAccessToken($member)
    {
        $accessToken = AccessToken::forceCreate([
            'id' => $this->generateIdentifier(),
            'uid' => $member->id,
            'expires_at' => $this->getExpireAt()
        ]);

        $this->accessToken = JWT::encode([
            'sub' => $accessToken->uid,
            'iat' => $accessToken->created_at->timestamp,
            'nbf' => $accessToken->created_at->timestamp,
            'exp' => $accessToken->expires_at->timestamp,
            'jti' => $accessToken->id
        ], $this->getSecretKey());

        //$this->setRefreshToken($accessToken);
    }

    protected function setRefreshToken($accessToken)
    {
        $refreshToken = RefreshToken::forceCreate([
            'id' => $this->generateIdentifier(),
            'access_token_id' => $accessToken->id,
            'expires_at' => $this->getRefreshTokenExpireAt()
        ]);

        $this->refreshToken = JWT::encode([
            'sub' => $accessToken->id,
            'iat' => $accessToken->created_at->timestamp,
            'nbf' => $accessToken->created_at->timestamp,
            'exp' => $refreshToken->expires_at->timestamp,
            'jti' => $refreshToken->id
        ], $this->getSecretKey());
    }

    protected function validatePayload($payload)
    {
        if (property_exists($payload, 'sub') &&
            property_exists($payload, 'iat') &&
            property_exists($payload, 'nbf') &&
            property_exists($payload, 'exp') &&
            property_exists($payload, 'jti')) {
            return true;
        }

        response()->json([
            'status' => 'fail',
            'message' => 'invalid token'
        ])->send();

        exit();
    }

    public function invalidSignatureResponse()
    {
        response()->json([
            'status' => 'fail',
            'message' => 'signature verification failed'
        ])->send();

        exit;
    }

    public function expiredExceptionResponse()
    {
        response()->json([
            'status' => 'fail',
            'message' => 'refresh token expired'
        ])->send();

        exit;
    }
}