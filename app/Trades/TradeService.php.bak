<?php

namespace App\Trades;

use Exception;

use App\Repositories\TradeRepo;
use App\Repositories\MemberRepo;
use App\Utils\DateTimeTool;

class TradeService
{
    private $tradeRepo;
    private $memberRepo;

    private $curMarketPrice;

    private $availableBuyingQty;
    private $separatePurchases = false;

    private $tmpCountAtThisPrice;
    private $actualSellingQty;
    private $partialSale = false;
    private $remainingUnits = 0;

    public function __construct(TradeRepo $tradeRepo, MemberRepo $memberRepo)
    {
        $this->tradeRepo = $tradeRepo;
        $this->memberRepo = $memberRepo;

        $this->curMarketPrice = round($this->getCurrentMarketPrice(), config('trade.decimal_point'));
    }

    /**
     * Get trade stats for trade chart.
     *
     * @return array
     */
    public function buildTradeChart()
    {
        dd('tet');
        $trade_stats = $this->tradeRepo->getTradeStatsForChart();
        $lastest_stats = $trade_stats->last(); 

        if (is_null($lastest_stats)) {
            $today_date = date('Y-m-d');
        }
        else {
            $today_date = date('Y-m-d', strtotime($lastest_stats->date . '+1 day'));
        }
        // dump($trade_stats);
        // dump($today_date);

        $today = [ 
            'price' => (string)$this->curMarketPrice,
            'total_volume' => $this->tradeRepo->getTotalQtyToday() ?: 0,
            'date' => $today_date
        ];

        $trade_stats->push($today);

        return $trade_stats;
    }

    /**
     * Get latest trade info.
     *
     * @return mixed
     */
    public function getLatestTradeStatsInfo()
    {
        $tradeStat = $this->tradeRepo->getLatestTradeStat();
        
        $change = number_format($this->getCurrentMarketPrice() - $tradeStat->open_price, config("trade.decimal_point"));
        if ($tradeStat->previous == 0) {
            $changePercentage = 100;
            
            if ($tradeStat->open_price == 0) {
                $tradeStat->open_price = config('trade.min_limit');
                $changePercentage = 0;
            }

            $change = number_format($tradeStat->open_price, config("trade.decimal_point"));
        } else {
            if ($tradeStat->open_price == 0) {
                $changePercentage = 0;
            }
            else {
                $changePercentage = round((($this->getCurrentMarketPrice() - $tradeStat->open_price) / $tradeStat->open_price) * 100, 2);
            }
        }
        $changePercentage = number_format($changePercentage, 2);

        $tradeStat->previous = number_format($tradeStat->previous, config("trade.decimal_point"));
        $tradeStat->open_price = number_format($tradeStat->open_price, config("trade.decimal_point"));

        $tradeStat->highest_price = '&mdash;';
        $highestPrice = $this->tradeRepo->getHighestPriceToday();
        if ($highestPrice) {
            $tradeStat->highest_price = number_format($highestPrice, config("trade.decimal_point"));
        }


        $tradeStat->lowest_price = '&mdash;';
        $lowestPrice = $this->tradeRepo->getLowestPriceToday();
        if ($lowestPrice) {
            $tradeStat->lowest_price = number_format($lowestPrice, config("trade.decimal_point"));
        }

        $tradeStat->last_price = '&mdash;';
        $lastPrice = $this->tradeRepo->getLastPrice();
        if ($lastPrice) {
            $tradeStat->last_price = number_format($lastPrice, config("trade.decimal_point"));
        }

        $tradeStat->current_value = number_format($this->getCurrentMarketPrice(), config("trade.decimal_point"));
        $tradeStat->change = $change . ' (' . $changePercentage . '%)';
        $tradeStat->intraday_volume = number_format($this->tradeRepo->getTotalQtyToday());

        return $tradeStat;
    }

    /**
     * Get available prices in array for drop down box
     *
     * @return array
     */
    public function arrSellingPriceDropDown($break_point = null)
    {
        $currentPrice = $this->getCurrentMarketPrice() ?: config('trade.min_limit');
        $arrPrices = [];
        for ($i = 1; ; $i++) {
            // format price to 3 decimal
            $currentPrice += config('trade.unit_step');
            
            if ($break_point) {
                if (round($currentPrice, config('trade.decimal_point')) > round($break_point, config('trade.decimal_point')))
                    break;
            } else {
                if ($i > config('trade.max_step') || round($currentPrice, config("trade.decimal_point")) > (round(config('trade.max_sell_limit'), config("trade.decimal_point"))))
                    break;
            }
            $strPrice = number_format($currentPrice, config("trade.decimal_point"));
            array_push($arrPrices, $strPrice);
        }

        return $arrPrices;
    }

    /**
     * Get current market price
     *
     * @return decimal
     */
    public function getCurrentMarketPrice()
    {
        $unit_step = config("trade.unit_step");
        $batch = config("trade.batch");
        $price = $this->tradeRepo->getPendingOpenQtyPrice();

        if (empty($price)) {
            $price = $this->tradeRepo->getLastSoldPrice($batch);
            $price = $price ? round($price + $unit_step, config("trade.decimal_point")) : $price;
        }

        if (empty($price))
            return config("trade.min_limit");

        return $price;
    }

    /***************************************** BUY **********************************************/
    /**
     * Check if this purchase trade has separate trades at different prices
     *
     * @return boolean
     */
    public function hasSeparatePurchases()
    {
        return $this->separatePurchases === true;
    }

    /**
     * Primary trading buy function
     *
     * @param  int     $user_id
     * @param  array   $arrBuy
     * @return string  error message
     */
    public function buy($user_id, $arrBuy)
    {
        $buy_qty = $required_qty = $arrBuy['buy_qty'];
        $buy_daily_limit_flag = isset($arrBuy["daily_limit_flag"]) ? $arrBuy["daily_limit_flag"] : true;
        $buy_price = $this->curMarketPrice;
        $max_limit_hit = false;
        
        $user_wallets = $this->memberRepo->getWalletsByUserId($user_id);
        if ($user_wallets) {
            $available_cash = $user_wallets->wallet1;
        }
        $actual_buy_qty = 0;

        do {
            $available_qty = $this->getAvailableForSaleAtPrice($buy_price);

            if ($available_qty > 0) {
                $can_buy_units = floor($available_cash / $buy_price);
                if ($available_qty >= $required_qty) {
                    if ($can_buy_units >= $required_qty) {
                        $actual_buy_qty += $required_qty;
                        break;
                    } else {
                        $actual_buy_qty += $can_buy_units;

                        // insufficient fund, qty is reduced
                        $this->separatePurchases = true;
                        break;
                    }
                } else {
                    if ($can_buy_units >= $available_qty) {
                        $actual_buy_qty += $available_qty;
                        $available_cash -= $available_qty * $buy_price;
                    } else {
                        $actual_buy_qty += $can_buy_units;
                        break;
                    }

                    // different prices, purchases are separated
                    $this->separatePurchases = true;

                    // $required_qty is deducted for the next price
                    $required_qty -= $available_qty;
                }
            }

            // increment price for next round of buying if there's any
            $buy_price += config('trade.unit_step');

            // break loop if over max price limit
            if ($buy_price > config('trade.max_limit')) {
                $max_limit_hit = true;
                break;
            }

            // for dongli requirement, cannot buy more than 5 prices
            if (round($buy_price, config('trade.decimal_point')) == round($this->getDailyMaxPrice(), config('trade.decimal_point'))
                && $buy_daily_limit_flag) {
                break;
            }

        } while ($required_qty > 0);

        if ($actual_buy_qty == 0) {
            return [
                "status" => false,
                "tradeBuy" => null,
                "max_limit_hit" => $max_limit_hit,
                "used_cash" => $user_wallets->wallet1 - $available_cash,
                "errmsg" => trans('trade.panel.msg.insufficientFund'),
            ];
        }

        $arrBuy['uid'] = $user_id;
        $arrBuy['buy_qty'] = $actual_buy_qty;

        return [
            "status" => true,
            "tradeBuy" => $this->tradeRepo->createTradeBuy($arrBuy),
            "max_limit_hit" => $max_limit_hit,
            "used_cash" => $user_wallets->wallet1 - $available_cash,
            "errmsg" => null,
        ];
    }

    /**
     * Get available trade unit at price
     *
     * @param  decimal  $price
     * @return int
     */
    private function getAvailableForSaleAtPrice($price)
    {
        $sellingTotal = $this->tradeRepo->getPendingSellTradeOpenQtyAtPrice($price);

        if (empty($sellingTotal)) $sellingTotal = 0;

        return $sellingTotal;
    }

    /**
     * Check if user has enough fund to buy
     *
     * @param  int      $user_id
     * @param  decimal  $value
     * @return boolean
     */
    private function hasInsufficientFund($user_id, $value)
    {
        return $this->memberManager->getWallet1ByUserId($user_id) < $value;
    }
    
    /**
     * Get trade buy orders for pending buy orders table.
     *
     * @param  $offset int
     * @return array
     */
    public function getBuyingOrdersList($offset = 0)
    {
        return $this->tradeRepo->getPendingTradeBuy($offset);
    }
    
    /**
     * Get trade buy orders buy id
     *
     * @param  $offset int
     * @return array
     */
    public function getBuyingOrderById($buyId)
    {
        return $this->tradeRepo->getTradeBuyById($buyId);
    }


    /***************************************** SELL **********************************************/
    /**
     * Check if user account is a debt accout
     *
     * @param  int      $user_id
     * @return boolean
     */
    public function isDebtAccount($user_id)
    {
        return $this->memberRepo->getDebtStatusByUserId($user_id) > 0;
    }

    /**
     * Check if user already had a pending sale trade
     *
     * @param  int      $user_id
     * @return boolean
     */
    public function hasPendingSellTrade($user_id)
    {
        return $this->tradeRepo->getPendingSellTradeCountByUserId($user_id) > 0;
    }

    /**
     * Check if user already had a fulfilled sale trade at this price
     *
     * @param  int      $user_id
     * @return boolean
     */
    public function hasSoldTradeAtPrice($user_id, $price)
    {
        return $this->tradeRepo->getSoldTradeCountAtPriceByUserId($user_id, $price) > 0;
    }

    /**
     * Check if the selling price is within marketable range
     *
     * @param  decimal  $sellingPrice
     * @return boolean
     */
    public function isNotWithinMarketableRange($sellingPrice)
    {
        if (round($sellingPrice, config('trade.decimal_point')) < round($this->curMarketPrice, config('trade.decimal_point')))
            return true;

        // if ($sellingPrice > config('trade.max_limit'))
        //     return true;

        // if ($sellingPrice > ($this->curMarketPrice + (config('trade.unit_step') * config('trade.max_step'))))
        //     return true;

        return false;
    }

    /**
     * Check if today's trade has been sold
     *
     * @param  integer  $user_id
     * @return boolean
     */
    public function hasSoldTradeToday($user_id)
    {
        return $this->tradeRepo->getTodaySoldTrade($user_id)->exists();
    }

    /**
     * Check if user has the qty to sell
     *
     * @param  int      $user_id
     * @param  int      $qty
     * @return boolean
     */
    public function hasInsufficientTradeUnit($user_id, $qty)
    {
        // debug purpose
        // return $qty > ($this->tradeRepo->getMemberTradeUnitHeld($user_id));
        
        return $qty > ($this->tradeRepo->getMemberTradeUnitHeld($user_id) - $this->tradeRepo->getMemberQtyOnSale($user_id));
    }

    /**
     * Check if there is no more units to sell at this price (reached limit)
     *
     * @return boolean
     */
    public function hasNoRoomToSellAtPrice($price, $use_reserved = false)
    {
        $qty_limit = $this->qtyLimitAtPrice($price, $use_reserved);

        return $qty_limit - $this->tradeRepo->getTotalCountAtPrice($price) <= 0;
    }

    /**
     * Check if the sale is over current member's permitted limit
     *
     * @param  int      $user_id
     * @param  decimal  $value
     * @return boolean
     */
    public function isOverMemberInvestmentLimit($user_id, $value)
    {
        // 20170225: check only current trade percentage of total investment, not accumulative
        // $tradeSold = $this->memberRepo->getAmtSoldByUserId($user_id);
        // $totalLimit = $tradeSold + $value;

        // return $totalLimit > config('trade.investment_rate') * $this->memberManager->getTotalInvestmentByUserId($user_id);

        return $value > $this->getMemberInvestmentLimit($user_id);
    }

    public function getMemberInvestmentLimit($user_id) 
    {
        return config('trade.investment_rate') * $this->memberRepo->getTotalInvestmentByUserId($user_id);
    }

    public function getTotalMemberInvestmentLimit($user_id)
    {
        return $this->memberRepo->getTotalInvestmentByUserId($user_id);
    }

    /**
     * Check if this sale trade is a partial trade
     *
     * @return boolean
     */
    public function isPartialSale()
    {
        return $this->partialSale === true;
    }

    /**
     * Return actual sale qty if it's a partial sale
     *
     * @return int
     */
    public function getActualSellingQtyIfPartialSale()
    {
        return $this->actualSellingQty;
    }

    /**
     * Primary trading sell function
     *
     * @param  int     $user_id
     * @param  array   $arrSell
     * @return string  error message
     */
    public function sell($user_id, $arrSell)
    {
        $use_reserved = isset($arrSell["use_reserved"]) ? $arrSell["use_reserved"] : false;
        if ($this->isOverMaxLimitAtPrice($arrSell['sell_qty'], $arrSell['sell_price'], $use_reserved)) {
            $this->partialSale = true;
            $arrSell['sell_qty'] = $this->actualSellingQty = $this->remainingSellableUnits($arrSell['sell_price'], $use_reserved);
            
            // if sell_qty is empty after substracting remaining sellable units, return
            if ($arrSell['sell_qty'] == 0) return;
        }

        // attach user id into array
        $arrSell['uid'] = $user_id;

        $sell_qty_limit = $this->qtyLimitAtPrice($arrSell['sell_price'], $use_reserved);
        
        try {
            $this->tradeRepo->createTradeSell($arrSell, $sell_qty_limit, $use_reserved);
        } catch (Exception $e) {
            return "hasNoRoomToSell";
        }
    }

    /**
     * Check if selling qty is over maximum limit at this price (Default at 100000)  
     *
     * @param  int      $qty
     * @param  decimal  $price
     * @return boolean
     */
    private function isOverMaxLimitAtPrice($qty, $price, $use_reserved = false)
    {
        $this->tmpCountAtThisPrice = $this->tradeRepo->getTotalCountAtPrice($price) ?: 0;

        $qty_limit = $this->qtyLimitAtPrice($price, $use_reserved);

        if (($this->tmpCountAtThisPrice + $qty) > $qty_limit)
            return true;

        return false;
    }

    /**
     * Check remaining number of units can be sold at price (members' limit)
     *
     * @return int
     */
    private function remainingSellableUnits($price, $use_reserved = false)
    {
        if (empty($this->tmpCountAtThisPrice)) {
            $this->tmpCountAtThisPrice = $this->tradeRepo->getTotalCountAtPrice($price) ?: 0;
        }

        $qty_limit = $this->qtyLimitAtPrice($price, $use_reserved);

        if ($qty_limit > $this->tmpCountAtThisPrice) {
            return $qty_limit - $this->tmpCountAtThisPrice;
        }

        return 0;
    }

    /**
     * To obtain qty limit set at that price (usually a price range)
     *
     * @return int
     */
    private function qtyLimitAtPrice($price, $use_reserved = false)
    {
        $qty_limit = config('trade.qty_limit_per_price');

        // if (round($price, config('trade.decimal_point')) > round(config('trade.max_limit'), config('trade.decimal_point'))) {
        //     $qty_limit = config('trade.qty_limit_per_price_after_max');
        // } elseif (round($price, config('trade.decimal_point')) >= 0.2) {
        //     $qty_limit = config('trade.qty_limit_per_price2');
        // }

        if ($use_reserved == true) {
            $qty_limit -= $this->tradeRepo->getTotalReservedAtPrice($price);
        }

        return $qty_limit;
    }

    /**
     * Get trade sell by member id
     *
     * @param  int  $user_id
     * @return TradeSell
     */
    public function getMemberTradeSellOrder($user_id)
    {
        return $this->tradeRepo->getTradeSellsByMemberId($user_id);
    }
    
    public function isUseReservedPortion($country = null)
    {
        return in_array($country, ["TW", "CN"]);
    }
    
    public function hasSellOrderAtPrice($user_id, $price)
    {
        $trade_sell = $this->getMemberTradeSellOrder($user_id)->first();
        if (empty($trade_sell))
            return false;

        return $trade_sell->price == $price;
    }

    /**
     * Get trade sell by sell id
     *
     * @param $sellId int
     */
    // public function findTradeSellById($sellId)
    // {
    //     return $this->tradeRepo->findTradeSellById($sellId);
    // }

    /**
     * Cancel sell trade
     *
     * @param  int      $sell_id
     * @param  int      $user_id
     * @return boolean
     */
    public function cancelSell($sell_id, $user_id, $unrestricted = false, $use_reserved = false)
    {
        $sell = $this->tradeRepo->getTradeSellById($sell_id);
        if (!empty($sell)) {
            if ($sell->uid != $user_id && auth('web')->check())
                return 'auth';
            // elseif (auth('web')->check() && !$unrestricted) {
            //     if (strtotime($sell->created_at) + (60 * 3) < strtotime(DateTimeTool::systemDateTime())) {
            //         return 'time_past';
            //     }
            // }

            if ($sell->status == 'P') {
                // check if it's already started selling
                if ($sell->initial_qty != $sell->open_qty) {
                    return 'portion_sold';
                }

                // 20170602: the following is deprecated
                // set status to HX (half-cancel)
                // $sell->status = 'HX';

                // 20170602: set status to X (cancelled)
                $sell->status = 'X';
                $sell->is_processed = true;
                $sell->cancelled_at = DateTimeTool::systemDateTime();

                if (auth('admins')->check()) {
                    $sell->cancel_itype = "900";
                    $sell->cancel_iid = auth('admins')->id();
                }

                return $this->tradeRepo->cancelTradeSell($sell, $use_reserved);
            }

            return $sell->status;
        }

        return false;
    }

    /*************************************** Selling Price/Volume Table **********************************************/

    /**
     * Get trade sell orders for sell orders table.
     *
     * @return array
     */
    public function getTradeSellingPrices($take = 10)
    {
        return $this->tradeRepo->getTradeSellingPricesVol($take);
    }

    /**
     * Get trade sell orders for sell orders table.
     *
     * @param  $price  decimal
     * @param  $offset int
     * @return array
     */
    public function getSellingOrdersListAtPrice($price, $offset = 0)
    {
        return $this->tradeRepo->getPendingFulfilledTradeSellAtPrice($price, $offset);
    }

    /**
     * Get member's trade matched
     *
     * @param  $user_id      int
     * @return mixed
     */
    public function getMemberTradeHistory($user_id)
    {
        return $this->tradeRepo->getMemberTransactionStatus($user_id);
    }

    /**
     * Get member's trade statement
     *
     * @param  int     $user_id
     * @return MemberTradeLog
     */
    public function getTradeStatements($user_id)
    {
        return $this->tradeRepo->getTradeLogsByMemberId($user_id);
    }
    
    /**
     * Get member's non trade statement
     *
     * @param  int     $user_id
     * @return MemberNonTradeLog
     */
    public function getNonTradeStatements($user_id)
    {
        return $this->tradeRepo->getNonTradeLogsByMemberId($user_id);
    }

    /**
     * Get member's total unit.
     *
     * @param  int     $user_id
     * @return int
     */
    public function getMemberTotalUnit($user_id)
    {
        $withheld = $this->tradeRepo->getMemberTradeUnitWithheld($user_id) ?: 0;

        return $this->getMemberTradeableUnit($user_id) + $withheld;
    }

    /**
     * Get member's tradeable unit.
     *
     * @param  int     $user_id
     * @return int
     */
    public function getMemberTradeableUnit($user_id)
    {
        return $this->tradeRepo->getMemberTradeUnitHeld($user_id) ?: 0;
    }

    /**
     * Get member's with-held unit (floating).
     *
     * @param  int     $user_id
     * @return int
     */
    public function getMemberWithheldUnit($user_id)
    {
        return $this->tradeRepo->getMemberTradeUnitWithheld($user_id) ?: 0;
    }

    /**
     * Check if member had existing buy trade
     *
     * @param  int     $user_id
     * @return bool
     */
    public function hasExistingBuyTrade($user_id)
    {
        return $this->tradeRepo->getPendingBuyByUid($user_id)->exists();
    }
    
    public function isMarketClosed() 
    {
        $tradeSetting = $this->tradeRepo->getTradeSetting();

        // debugging
        // dump(DateTimeTool::operationToSystemDateTime($tradeSetting->open_date));
        // dump(DateTimeTool::systemDateTime());
        
        if ($tradeSetting) {
            if ($tradeSetting->open_date_enabled == true && !empty($tradeSetting->open_date) && DateTimeTool::operationToSystemDateTime($tradeSetting->open_date) <= DateTimeTool::systemDateTime()) {
                $tradeSetting->market_open = true;
                $tradeSetting->open_date_enabled = false;
                $tradeSetting->save();
            }

            if ($tradeSetting->close_date_enabled == true && !empty($tradeSetting->close_date) && DateTimeTool::operationToSystemDateTime($tradeSetting->close_date) <= DateTimeTool::systemDateTime()) {
                $tradeSetting->market_open = false;
                $tradeSetting->close_date_enabled = false;
                $tradeSetting->save();
            }
        }

        // return config("trade.suspended") || !$this->tradeRepo->getMarketOpen();
        return config("trade.suspended") || !$this->tradeRepo->getMarketOpen() || ( round($this->curMarketPrice, config('trade.decimal_point')) > round(config("trade.max_limit"), config('trade.decimal_point')) );
    }

    public function hasStartedSelling($sell_id)
    {
        $sell_trade = $this->tradeRepo->getSellTradeById($sell_id);
        if ($sell_trade && $sell_trade->status == 'P') {

        }
    }

    public function getPendingSellTotalUnitByUid($user_id)
    {
        return $this->tradeRepo->getMemberQtyOnSale($user_id);
    }

    public function editMemberTradeUnit2($user_id, $qty, $price, $remark, $admin_remark, $itype, $iid, $type, $json = null) 
    {
        return $this->tradeRepo->editTradeUnit2($user_id, $qty, $price, $remark, $admin_remark, $itype, $iid, $type, $json);
    }

    public function editMemberTradeConvert($user_id, $unit, $value, $max_value) 
    {
        return $this->tradeRepo->editMemberTradeConvert($user_id, $unit, $value, $max_value) ;
    }

    public function getMemberTradeConvertValueByUid($user_id)
    {
        $trade_convert = $this->tradeRepo->getMemberTradeConvertByUid($user_id);
        return $trade_convert ? $trade_convert->value_converted : 0;
    }

    protected function getDailyMaxPrice()
    {
        return $this->tradeRepo->getTradeSetting()->daily_max_price;
    }

    /**
     * Check if trading has reached max limit per day
     *
     * @return bool
     */
    public function hasReachedDailyMaxPrice()
    {
        $max_price = $this->getDailyMaxPrice();

        if (is_null($max_price) || $this->curMarketPrice < $max_price) {
            return false;
        }
      
        return true;
    }
}