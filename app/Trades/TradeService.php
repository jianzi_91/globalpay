<?php

namespace App\Trades;

use Exception;

use App\Repositories\TradeRepo;
use App\Repositories\MemberRepo;
use App\Repositories\WalletRepo;

use App\Utils\DateTimeTool;

class TradeService
{
    private $tradeRepo;
    private $memberRepo;
    private $walletRepo;

    private $curMarketPrice;

    private $availableBuyingQty;
    private $separatePurchases = false;

    private $tmpCountAtThisPrice;
    private $actualSellingQty;
    private $partialSale = false;
    private $remainingUnits = 0;

    public function __construct(TradeRepo $tradeRepo, MemberRepo $memberRepo, WalletRepo $walletRepo)
    {
        $this->tradeRepo = $tradeRepo;
        $this->memberRepo = $memberRepo;
        $this->walletRepo = $walletRepo;

        $this->curMarketPrice = $this->getCurrentMarketPrice();

        $this->setting = $this->getTradeSetting();

        $this->charity = $this->getCharityRate() / 100;
        $this->maintenance = $this->getPlatformCharge() / 100;

        $this->todayTradingCap = $this->getTodayTradingCap();
    }

    public function createTradeSell($uid, $data)
    {
        return $this->tradeRepo->createTradeSell($uid, $data);
    }

    public function createTradeBuy($uid, $data)
    {
        return $this->tradeRepo->createTradeBuy($uid, $data);
    }

    public function queueBuyingRequest($tradesell_id, $buyer_id, $qty)
    {
        return $this->tradeRepo->requestBuying($tradesell_id, $buyer_id, $qty);
    }

    public function queueSellingRequest($tradebuy_id, $seller_id, $qty)
    {
        return $this->tradeRepo->requestSelling($tradebuy_id, $seller_id, $qty);
    }

    public function getAllBuyingRequestsByUid($member_id)
    {
        return $this->tradeRepo->getMemberPendingAndPaidBuyingRequests($member_id);
    }

    public function getTradeQueue($queue_id)
    {
        return $this->tradeRepo->getTradeQueueById($queue_id);
    }

    public function getTradeBuyQueue($tradebuy_id)
    {
        return $this->tradeRepo->getQueueingTradeByTradeBuyId($tradebuy_id);
    }

    public function getTradeSellQueue($tradesell_id)
    {
        return $this->tradeRepo->getQueueingTradeByTradeSellId($tradesell_id);
    }

    public function getMemberTradeSellsByMemberId($member_id)
    {
        return $this->tradeRepo->getMemberTradeSells($member_id);
    }

    public function getMemberTradeBuysByMemberId($member_id)
    {
        return $this->tradeRepo->getMemberTradeBuys($member_id);
    }

    public function getMemberTradeBuyRequests($member_id)
    {
        return $this->tradeRepo->getMemberPendingAndPaidBuyingRequests($member_id);
    }

    public function enterBuyingLobby()
    {
        return $this->tradeRepo->getPendingTradeSell();
    }

    public function enterSellingLobby()
    {
        return $this->tradeRepo->getPendingTradeBuy();
    }

    public function getTradeSellById($sell_id)
    {
        return $this->tradeRepo->getTradeSellById($sell_id);
    }

    public function getTradeBuyById($buy_id)
    {
        return $this->tradeRepo->getTradeBuyById($buy_id);
    }

    public function cancelBuyRequest($buy_id)
    {
        return $this->tradeRepo->deleteTradeBuyById($buy_id);
    }

    public function buyTrade($queue_id)
    {
        $tradeQueue = $this->tradeRepo->getTradeQueueById($queue_id);
    }

    public function getMemberCustomBuys($member_id)
    {
        return $this->tradeRepo->getMemberCustomBuys($member_id);
    }

    public function getQueueingBuys($member_id)
    {
        return $this->tradeRepo->getMemberQueueingBuys($member_id);
    }

    public function getMemberCustomSells($member_id)
    {
        return $this->tradeRepo->getMemberCustomSells($member_id);
    }

    public function getQueueingSells($member_id)
    {
        return $this->tradeRepo->getMemberQueueingSells($member_id);
    }

    public function executeTransaction($queue_id)
    {
        $queue = $this->tradeRepo->getTradeQueueById($queue_id);

        $tradeSell = $this->tradeRepo->getTradeSellById($queue->sell_id);
        $tradeBuy = $this->tradeRepo->getTradeBuyById($queue->buy_id);

        $charity = $queue->quantity * $this->charity;
        $maintenance = $queue->quantity * $this->maintenance;

        // 1. Update tradeSell info
        $tradeSell->locked_qty -= ($queue->quantity + $charity + $maintenance);
        $tradeSell->save();

        if (($tradeSell->open_qty == 0 && $tradeSell->locked_qty == 0) || $tradeSell->type == 100) {
            $this->tradeRepo->fulfillTradeSell($tradeSell->id);
        }

        // 2. Update tradeBuy info
        $tradeBuy->open_qty -= $queue->quantity;
        $tradeBuy->save();

        if ($tradeBuy->open_qty == 0 || $tradeBuy->type == 100){
            $this->tradeRepo->fulfillTradeBuy($tradeBuy->id);
        }
        
        // 3. Update tradeQueue info
        $this->tradeRepo->completeTrade($queue_id);

        // 4. Update buyer account
        $this->walletRepo->buyCoin($queue);

        // 5. Update seller account
        $this->walletRepo->sellCoin($queue);

        return [
            'price' => $queue->price,
            'qty' => $queue->quantity,
            'charity' => $charity,
            'maintenance' => $maintenance,
            'total' => $queue->quantity + $charity + $maintenance,
        ];
    }

    public function voidActiveQueue($queue_id)
    {
        $queue = $this->tradeRepo->getTradeQueueById($queue_id);

        $charity = $queue->quantity * $this->charity;
        $maintenance = $queue->quantity * $this->maintenance;

        // Release locked quantity requested by buyer
        $tradeSell = $queue->tradeSell;
        if($tradeSell->type == 200) {
            $tradeSell->locked_qty -= ($queue->quantity + $maintenance + $charity);
            $tradeSell->open_qty += ($queue->quantity + $maintenance + $charity);
            $tradeSell->save();
        }

        // Release locked quantity requested by seller
        $tradeBuy = $queue->tradeBuy;
        if($tradeBuy->type == 200) {
            $tradeBuy->open_qty += $queue->quantity;
            $tradeBuy->save();
        }        
        
        return $this->tradeRepo->cancelTradeQueue($queue_id);
    }

    public function endTradeSell($tradesell_id)
    {
        $tradeSell = $this->tradeRepo->getTradeSellById($tradesell_id);

        if ($tradeSell->type == 100) {
            return $this->tradeRepo->cancelTradeSell($tradesell_id);
        }

        $buys_in_queue = $this->tradeRepo->countQueueingBuys($tradesell_id);

        if ($buys_in_queue != 0)
        {
            return null;
        }

        return $this->tradeRepo->cancelTradeSell($tradesell_id);
    }

    public function endTradeBuy($tradebuy_id)
    {
        $tradeBuy = $this->tradeRepo->getTradeBuyById($tradebuy_id);

        if ($tradeBuy->type == 100) {
            return $this->tradeRepo->cancelTradeBuy($tradebuy_id);
        }

        $sells_in_queue = $this->tradeRepo->countQueueingSells($tradebuy_id);

        if ($sells_in_queue != 0)
        {
            return null;
        }

        return $this->tradeRepo->cancelTradeBuy($tradebuy_id);
    }

    public function getBuyRequestsForTradeSell($tradesell_id)
    {
        return $this->tradeRepo->getTradeSellActiveQueues($tradesell_id);
    }

    public function getSellRequestsForTradeBuy($tradebuy_id)
    {
        return $this->tradeRepo->getTradeBuyActiveQueues($tradebuy_id);
    }

    public function getMemberTradeRecords($member_id, $chunk = 10)
    {
        return $this->tradeRepo->paginatedMemberInactiveQueues($member_id, $chunk);
    }

    public function rateBuyer($queue_id, $rate, $comment = null)
    {
        $trade = $this->tradeRepo->getTradeQueueById($queue_id);

        return $this->memberRepo->rateMember($trade->id, $trade->sell_uid, $trade->buy_uid, $rate);
    }

    public function rateSeller($queue_id, $rate, $comment = null)
    {
        $trade = $this->tradeRepo->getTradeQueueById($queue_id);

        return $this->memberRepo->rateMember($trade->id, $trade->buy_uid, $trade->sell_uid, $rate);
    }

    public function checkMemberRatedTrade($queue_id, $member_id)
    {
        return $this->memberRepo->getMemberRatedTrade($queue_id, $member_id);
    }

    public function getMemberRating($member_id)
    {
        return $this->memberRepo->getMemberRating($member_id);
    }

    public function getAllPendingQueues()
    {
        return $this->tradeRepo->getAllPendingQueues();
    }

    public function getMemberTotalSellingCoins($member_id)
    {
        $member_sells = $this->tradeRepo->getMemberPendingTradeSell($member_id);

        $selling_coins = 0;

        foreach ($member_sells as $member_sell) {
            $selling_coins += $member_sell->open_qty + $member_sell->locked_qty;
        }

        return $selling_coins;
    }

    public function getPaymentMethodArray()
    {
        return $this->tradeRepo->getPaymentMethodArray();
    }

    public function updateTradePrice()
    {
        return $this->tradeRepo->raiseTradePrice();
    }

    public function updateTradeQuantityMax()
    {
        $tradable_amount = $this->memberRepo->getTradableWallet();
        $percent = 0.1;
        $system_tradable_amount = round($tradable_amount * $percent);
        return $this->tradeRepo->updateTradeQuantityMax($system_tradable_amount);
    }

    public function getTodayTotalSold()
    {
        $completed_trades = $this->tradeRepo->getTodayCompletedTrades();
        $total_sold = 0;

        foreach ($completed_trades as $trade) {
            $total_sold = $trade->quantity;
        }

        return $total_sold;
    }

    public function getTradeSetting()
    {
        return $this->tradeRepo->getTradeSetting();
    }

    public function getCurrentMarketPrice()
    {
        return $this->tradeRepo->getTradeSetting()->price;
    }

    public function getCharityRate()
    {
        return $this->tradeRepo->getTradeSetting()->charity_percent;
    }

    public function getPlatformCharge()
    {
        return $this->tradeRepo->getTradeSetting()->platform_charge;
    }

    public function getTodayTradingCap()
    {
        return $this->tradeRepo->getTradeSetting()->qty_per_price;
    }

    public function coinsAfterCharges($qty)
    {
        return $qty * (1 + $this->charity + $this->maintenance);
    }

    public function coinsBeforeCharges($qty)
    {
        return $qty / (1 + $this->charity + $this->maintenance);
    }
}