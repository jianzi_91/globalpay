<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TradeStatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mlm_trade_stats')->truncate();

        $years = 5;

        $result = [];
        $value = 0;
        $volume = 0;
        $minVal = 0.001;
        $maxVal = 0.25;
        $minVol = 10000;
        $maxVol = 150000000;
        $previous = 0;
        $date = Carbon::today()->subYears($years);
        $diff = $date->diffInDays(Carbon::today());

        for ($i = 0; $i < $diff; $i++) {
            $data = [];
            $date->addDay();

            if ($i == 0) {
                $value = rand($minVal * 1000, $maxVal * 1000) / 1000;
                $volume = rand(1, 15) * 10000000;
                $data['previous'] = $previous;
            } else {
                $data['previous'] = $value;

                $value = $value + (rand(-10, 10) / 1000);
                $volume = $volume + (rand(-10, 10) * 10000);

                if ($value < $minVal) {
                    $value = $minVal;
                } elseif ($value > $maxVal) {
                    $value = $maxVal;
                }

                if ($volume < $minVol) {
                    $volume = $minVol;
                } elseif ($volume > $maxVol) {
                    $volume = $maxVol;
                }
            }

            $data['created_at'] = $date->format('Y-m-d');
            //$data['updated_at'] = $date->format('Y-m-d');
            $data['open_price'] = $data['previous'];
            //$data['highest_price'] = $value + (rand(0, 5) / 1000);
            //$data['lowest_price'] = $value + (rand(-5, 0) / 1000);
            //$data['last_price'] = $value;
            //$data['current_value'] = $value;
            //$data['change'] = $data['current_value'] - $data['previous'];
            $data['total_volume'] = $volume;

            array_push($result, $data);
        }

        DB::table('mlm_trade_stats')->insert($result);
    }
}
