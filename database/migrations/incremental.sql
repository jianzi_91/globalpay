# 2018-07-31 member relationship table
CREATE TABLE `mlm_member_relationships` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'user id',
  `downline_id` int(10) unsigned NOT NULL COMMENT 'user id',
  `pos` tinyint(1) unsigned NOT NULL,
  `level_diff` int(5) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`downline_id`),
  KEY `member_relationships__level__idx` (`level_diff`),
  KEY `member_relationships__upline__idx` (`downline_id`),
  KEY `member_relationships__acc__idx` (`user_id`),
  KEY `member_relationships__acc__downline__leg_pos__idx` (`user_id`,`downline_id`,`pos`),
  KEY `member_relationships__upline__acc__leg_pos__idx` (`downline_id`,`user_id`,`pos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `mlm_error_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `stage` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# 2018-08-06 wallet update
ALTER TABLE `mlm_member_wallet_record` 
ADD COLUMN `price` DECIMAL(9,6) UNSIGNED NULL AFTER `trans_code`;

ALTER TABLE `mlm_member_wallets` 
CHANGE COLUMN `rwallet` `rwallet` DECIMAL(17,8) UNSIGNED NOT NULL DEFAULT '0.00000000' COMMENT '注册钱包' ,
CHANGE COLUMN `fwallet` `fwallet` DECIMAL(17,8) UNSIGNED NOT NULL DEFAULT '0.00000000' COMMENT '理财钱包' ;

# 2018-08-09 bonus calculation : ee
CREATE TABLE `mlm_member_wallets_copy` (
  `uid` int(10) unsigned NOT NULL,
  `awallet` decimal(17,8) unsigned NOT NULL DEFAULT '0.00000000' COMMENT '热持币钱包',
  `awallet2` decimal(17,8) unsigned NOT NULL DEFAULT '0.00000000' COMMENT '冷持币钱包',
  `bonus1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bdate` date DEFAULT NULL COMMENT 'Bonus Date',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `mlm_comm_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amt_max` decimal(17,8) unsigned NOT NULL DEFAULT '0.00000000',
  `amt_min` decimal(17,8) unsigned NOT NULL DEFAULT '0.00000000',
  `percent` decimal(5,3) unsigned NOT NULL DEFAULT '0.000',
  `type` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `mlm_comms` 
CHANGE COLUMN `amount` `amount` DECIMAL(17,8) NOT NULL ,
CHANGE COLUMN `from_amount` `from_amount` DECIMAL(17,8) NOT NULL ,
CHANGE COLUMN `percent` `percent` DECIMAL(10,6) NOT NULL ;

ALTER TABLE `mlm_member_status` 
ADD COLUMN `bonus_wallet_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' AFTER `alliance_status`;

# 2018-08-23 general settings : ee
CREATE TABLE `mlm_settings` (
  `id` enum('') COLLATE utf8_unicode_ci NOT NULL,
  `bonus_swallet_percent` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `unfreeze_wallet_percent` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# 2018-08-24 trade settings : ee
ALTER TABLE `mlm_trade_settings` 
ADD COLUMN `platform_charge` DECIMAL(5,2) UNSIGNED NOT NULL AFTER `qty_per_price`,
ADD COLUMN `charity_percent` DECIMAL(5,2) UNSIGNED NOT NULL AFTER `platform_charge`,
ADD COLUMN `price_change` DECIMAL(8,6) NOT NULL AFTER `charity_percent`;

INSERT INTO `mlm_settings` (`id`, `bonus_swallet_percent`, `unfreeze_wallet_percent`) VALUES ('', '5.00', '5.00');
INSERT INTO `mlm_trade_settings` (`id`, `market_open`, `open_date_enabled`, `close_date_enabled`, `qty_per_price`, `platform_charge`, `charity_percent`, `price_change`) VALUES ('', '0', '0', '0', '25000', '5.00', '1.00', '0.120000');

# 2018-08-28 trading platform
CREATE TABLE `mlm_member_ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trade_id` int(10) DEFAULT NULL,
  `rater_uid` int(10) unsigned NOT NULL,
  `rated_uid` int(10) unsigned NOT NULL,
  `rate` tinyint(3) unsigned NOT NULL,
  `comment` tinytext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `mlm_trade_buys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `initial_qty` int(10) unsigned NOT NULL,
  `min_qty` int(10) unsigned NOT NULL DEFAULT '1',
  `open_qty` int(10) unsigned NOT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,6) unsigned NOT NULL,
  `status` enum('P','F','X') COLLATE utf8_unicode_ci NOT NULL COMMENT 'P - pending / partially fulfilled\nF - fulfilled\nX - cancelled',
  `type` smallint(6) NOT NULL DEFAULT '200',
  `fulfilled_at` datetime DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tb_uid_idx` (`uid`),
  KEY `tb_open_qty_idx` (`open_qty`),
  KEY `tb_status_open_qty_processed_id_idx` (`status`,`open_qty`,`id`)
);

CREATE TABLE `mlm_trade_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `buy_id` int(11) DEFAULT NULL,
  `buy_uid` int(11) DEFAULT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `sell_uid` int(11) DEFAULT NULL,
  `receipt` varchar(45) DEFAULT NULL COMMENT 'storing URL for proof for payment',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - initiated\n2 - pending\n3 - paid\n4 - cancelled',
  `quantity` decimal(17,8) NOT NULL,
  `price` decimal(8,6) NOT NULL,
  `expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `mlm_trade_sells` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `initial_qty` int(10) unsigned NOT NULL,
  `min_qty` int(10) unsigned NOT NULL DEFAULT '1',
  `open_qty` int(10) unsigned NOT NULL,
  `locked_qty` int(10) NOT NULL DEFAULT '0',
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,6) unsigned NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '200' COMMENT '1 - SYSTEM\n2 - CUSTOM',
  `status` enum('P','F','X') COLLATE utf8_unicode_ci NOT NULL COMMENT 'P - pending / partially fulfilled\nX - cancelled\nF - fulfilled',
  `fulfilled_at` datetime DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ts_price_status_idx` (`price`,`status`),
  KEY `ts_open_qty_idx` (`open_qty`),
  KEY `ts_status_price_cdate_idx` (`status`,`price`,`created_at`),
  KEY `ts_status_open_qty_price_cdate_idx` (`status`,`open_qty`,`price`,`created_at`),
  KEY `ts_uid_status_idx` (`uid`,`status`),
  KEY `ts_status_open_qty_processed_price_id_idx` (`status`,`open_qty`,`price`,`id`),
  KEY `ts_batch_status_price_id_idx` (`status`,`price`,`id`),
  KEY `ts_uid_batch_status_price_idx` (`uid`,`status`,`price`)
);

ALTER TABLE `mlm_currencies` 
ADD COLUMN `symbol` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `code`;

# 04 Sept 2018 by Jianzi
# Alter member wallet record balance to unsigned
ALTER TABLE `mlm_member_wallet_record` 
CHANGE COLUMN `balance` `balance` DECIMAL(17,8) UNSIGNED NOT NULL ;

# 2018-09-06 trade setting : ee
ALTER TABLE `mlm_trade_settings` 
ADD COLUMN `price` DECIMAL(8,6) UNSIGNED NOT NULL AFTER `price_change`,
ADD COLUMN `qty_sold` INT(10) UNSIGNED NOT NULL AFTER `price`;

# 06 Sept 2018 by Jianzi
# Added column at trade sell and buy for payment method
ALTER TABLE `mlm_trade_sells` 
ADD COLUMN `payment_method` ENUM('B', 'W', 'A') NOT NULL DEFAULT 'B' AFTER `price`;

ALTER TABLE `mlm_trade_buys` 
ADD COLUMN `payment_method` ENUM('B', 'W', 'A') NOT NULL DEFAULT 'B' AFTER `price`;

ALTER TABLE `mlm_trade_sells` 
ADD COLUMN `payment_ss` VARCHAR(255) NULL AFTER `payment_method`,
ADD COLUMN `payment_id` VARCHAR(45) NULL AFTER `payment_ss`;

# 2018-09-07 trade price large range : ee
ALTER TABLE `mlm_trade_settings` 
CHANGE COLUMN `price` `price` DECIMAL(10,6) UNSIGNED NOT NULL ;

# 2018-09-10 by Jianzi
# Alter the quantity of trade sell and buy to enable decimal places
ALTER TABLE `mlm_trade_sells` 
CHANGE COLUMN `initial_qty` `initial_qty` DECIMAL(17,8) UNSIGNED NOT NULL ,
CHANGE COLUMN `min_qty` `min_qty` DECIMAL(17,8) UNSIGNED NOT NULL DEFAULT '1' ,
CHANGE COLUMN `open_qty` `open_qty` DECIMAL(17,8) UNSIGNED NOT NULL ,
CHANGE COLUMN `locked_qty` `locked_qty` DECIMAL(17,8) NOT NULL DEFAULT '0' ;

ALTER TABLE `mlm_trade_buys` 
CHANGE COLUMN `initial_qty` `initial_qty` DECIMAL(17,8) UNSIGNED NOT NULL ,
CHANGE COLUMN `min_qty` `min_qty` DECIMAL(17,8) UNSIGNED NOT NULL DEFAULT '1' ,
CHANGE COLUMN `open_qty` `open_qty` DECIMAL(17,8) UNSIGNED NOT NULL ;

# 2019-03-05 KaChoon
# =================
# Access token
DROP TABLE IF EXISTS `mlm_access_tokens`;
CREATE TABLE `mlm_access_tokens` (
  `id` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `revoked` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# 2019-03-06 KaChoon
# ==================
# transaction_logs

CREATE TABLE `mlm_transaction_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `trans_code` tinyint(3) unsigned NOT NULL,
  `source_id` varchar(45) unsigned DEFAULT NULL,
  `a_amt` decimal(11,4) NOT NULL,
  `a_balance` decimal(11,4) unsigned NOT NULL,
  `a2_amt` decimal(11,2) NOT NULL,
  `a2_balance` decimal(11,2) unsigned NOT NULL,
  `remarks` text,
  `admin_remarks` text,
  `creator_user_type` tinyint(3) unsigned NOT NULL,
  `creator_uid` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `transaction_logs__uid__trans_code__source_id__idx` (`uid`,`trans_code`,`source_id`),
  KEY `transaction_logs__created_at__uid__idx` (`created_at`,`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8;

# 2019-03-13 Jianzi
# =================
# additional release

CREATE TABLE `mlm_member_additional_release` (
  `uid` int(11) NOT NULL,
  `saving_amount` decimal(17,8) NOT NULL DEFAULT '0.00000000',
  `saving_total` decimal(17,8) NOT NULL DEFAULT '0.00000000',
  `spending_amount` decimal(17,8) NOT NULL DEFAULT '0.00000000',
  `spending_total` decimal(17,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# 2019-03-20 KaChoon
====================
# update transaction_logs table

ALTER TABLE `mlm_transaction_logs` 
ADD COLUMN `do_amt` DECIMAL(11,4) NOT NULL AFTER `a2_balance`,
ADD COLUMN `do_balance` DECIMAL(11,4) UNSIGNED NOT NULL AFTER `do_amt`,
ADD COLUMN `f_amt` DECIMAL(11,4) NOT NULL AFTER `do_balance`,
ADD COLUMN `f_balance` DECIMAL(11,4) UNSIGNED NOT NULL AFTER `f_amt`;
ADD COLUMN `type` VARCHAR(45) NULL AFTER `trans_code`;

# 2019-03-22 KaChoon
====================
# added mlm_manual_withdrawal table

CREATE TABLE `mlm_manual_withdrawal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `creator_uid` int(10) unsigned NOT NULL,
  `editor_user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `editor_uid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL COMMENT 'user id',
  `receiver_uid` int(10) unsigned NOT NULL COMMENT 'receiver id',
  `receivable_amount` decimal(15,6) NOT NULL,
  `commission_amount` decimal(15,6) NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_amount` decimal(15,6) unsigned DEFAULT NULL,
  `rate` decimal(15,6) unsigned DEFAULT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'developer remark',
  `remarks` text COLLATE utf8_unicode_ci,
  `admin_remarks` text COLLATE utf8_unicode_ci,
  `status` tinyint(3) unsigned NOT NULL,
  `paid_by_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_by_uid` int(10) unsigned DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# 2019-03-22 KaChoon
====================
# added mlm_member_wallets table

ALTER TABLE `mlm_member_wallets` 
CHANGE COLUMN `dowallet` `dowallet` DECIMAL(17,8) UNSIGNED NULL DEFAULT '0.00000000' COMMENT '慈善钱包' ;

ALTER TABLE `mlm_members` 
ADD COLUMN `user_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' AFTER `crank`;

# 2019-03-26 KaChoon
====================
# updated mlm_manual_withdrawal

ALTER TABLE `mlm_manual_withdrawal` 
ADD COLUMN `receipt_url` VARCHAR(255) NOT NULL AFTER `receiver_uid`;

# 2019-04-01 KaChoon
====================
# updated mlm_transaction_logs

ALTER TABLE `mlm_transaction_logs` 
ADD COLUMN `s_amt` DECIMAL(11,4) NOT NULL AFTER `f_balance`,
ADD COLUMN `s_balance` DECIMAL(11,4) UNSIGNED NOT NULL AFTER `s_amt`;

ALTER TABLE `mlm_transaction_logs` 
CHANGE COLUMN `do_balance` `do_balance` DECIMAL(11,4) NOT NULL ;

# 2019-04-02 KaChoon
=====================
# updated mlm_member_wallets

ALTER TABLE `mlm_member_wallets` 
ADD COLUMN `sc_wallet` DECIMAL(17,8) UNSIGNED NOT NULL DEFAULT '0.00000000' AFTER `iwallet`;

ALTER TABLE `mlm_members` 
CHANGE COLUMN `rank` `rank` INT(10) UNSIGNED NOT NULL COMMENT '0=normal,\n1000=platinum,\n5000=gold,\n10000=Perkin,\n50000=diamond,\n100000=Crown' ;

ALTER TABLE `mlm_transaction_logs` 
ADD COLUMN `sc_amt` DECIMAL(11,4) NOT NULL AFTER `s_balance`,
ADD COLUMN `sc_balance` DECIMAL(11,4) UNSIGNED NOT NULL AFTER `sc_amt`;
