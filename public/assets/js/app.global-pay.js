jQuery(document).ready(function($){
	
	var Overlay = document.createElement('div');
	Overlay.className = 'overlay';
	document.getElementsByTagName('body')[0].appendChild(Overlay);

	
	$(".menu-toggle").click(function(){
		$(".menu").addClass("visible");
		$(".overlay").addClass("visible");
	});	
	$(".overlay").click(function(){
		$(".menu").removeClass("visible");
		$(".overlay").removeClass("visible");
	});
	
	$('a.page-scroll').on('click', function(event){
		// open primary navigation on mobile
		event.preventDefault();
		$('.MainMenu').removeClass('visible');
		$('.overlay').removeClass('visible');
	});
	
	$(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 120)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });
	
	$('.page-title').insertBefore('.menu-toggle');
	$('.back-toggle').insertBefore('.brand');
	
	
	$(document).scroll(function() {
		var y = $(this).scrollTop();
		if (y > 50) {
			$('.main-header').addClass("scrolled");
		} else {
			$('.main-header').removeClass("scrolled");
		}
	});
		
});

