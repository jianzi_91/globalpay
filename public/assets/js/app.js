$(document).ready(function() {
	
	
	var MqL = 769;
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});
	
	moveEshare();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveEshare, 300) : window.requestAnimationFrame(moveEshare);
	});
	
	var overlay = document.createElement('div');
	overlay.className = 'overlay';
	document.getElementsByTagName('body')[0].appendChild(overlay);
	
	
	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window, 
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}
	
	function moveNavigation(){
		var navigation = $('.menu');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertBefore('.menu-trigger');
		} else {
			navigation.detach();
			navigation.insertAfter('.main-header');
		}
	}
	
	function moveEshare(){
		var navigation = $('.eshare-btn');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertAfter('.sub-nav');
		} else {
			navigation.detach();
			navigation.insertAfter('.menu');
		}
	}
	
	$(document).ready(function() {  
		$(window).scroll(sticky_nav());
		sticky_nav(); // Force the first call on refresh 
	});

	function sticky_nav() {
		if( $(this).scrollTop() > 150) {
			$(".main-header").addClass("fixed");
			} else {
			$(".main-header").removeClass("fixed");
		}
	}
	$(window).on("scroll", function() {
		var fromTop = $(window).scrollTop();
		$(".main-header").toggleClass("fixed", (fromTop > 150));
	});
	
	$(".menu-trigger").click(function(){
		$(".menu").toggleClass("visible");
		$(".overlay").toggleClass("visible");
	});
	$(".overlay").click(function(){
		$(".menu").removeClass("visible");
		$(".overlay").removeClass("visible");
	});
	
	$(".wallet-trigger").click(function(e){
		$(".user-info").addClass("visible");
		e.stopPropagation();
	});
	$(document).mouseup(function(e){
		var container = $(".user-info");

		// if the target of the click isn't the container nor a descendant of the container
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			$(".user-info").removeClass("visible");
		}
	});
	
	$(".dropdown-menu").click(function(e){
		e.stopPropagation();
	});
	
	$('a[data-toggle="extra"]').click(function(){
		$(this).parent().toggleClass("open");
	});
	
})