// utility functions
Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function generateId() {
    return getRandomInt(1, 9999).toString();
}

function autoLogout(type, timeout) {
	var margin;
	if (type == 'normal') {
		margin = 1000;
	} else {
		margin = -2000;
	}

	var countDown;
	var lifetime = timeout * 1000 * 60 + margin;

	var expiring = function() {
		var timestamp = localStorage.getItem('timeout');
		var now = Date.now();
		countDown = setTimeout(function() {
			location.reload(true);
			clearTimeout(countDown);
			localStorage.setItem('timeout', Date.now() + lifetime);
			expiring();
		}, timestamp - now);
	}

	window.onload = function() {
		localStorage.setItem('timeout', Date.now() + lifetime);
		expiring();
	}

	window.onfocus = expiring;

	window.onblur = function() {
		clearTimeout(countDown);
	}
}

// UI functions
function refreshCaptcha() {
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("/"))+"/"+Math.random()*1000;
}

function setLocale(url) {
	$(".locale").click(function() {
		var lang = $(this).data('lang');
		$.ajax({
			type: 'GET',
			data: { 'lang': lang },
			url: url,
			success: function (responseText) {
				if (responseText && responseText == "success") {
					window.location.reload(true);
				} 
			},
			error: function(data) {
				var errors = data.responseJSON;
			}
		});
	});
}

// UI behaviors
$(function() {

 	$(".sidebar-toggle").click(function (z) {
		$("#sidebar").addClass("sidebar-open");
		$(".sidebar-overlay").addClass("sidebar-open");
	});

	$(".sidebar-overlay").click(function (z) {
		$("#sidebar").removeClass("sidebar-open");
		$(".sidebar-overlay").removeClass("sidebar-open");
	});

	$(".btn , .navbar-nav > li > a, .trade-cta").click(function (e) {
  		// Remove any old one
		$(".ripple").remove();

		// Setup
		var posX = $(this).offset().left,
			posY = $(this).offset().top,
			buttonWidth = $(this).width(),
			buttonHeight =  $(this).height();
		
		// Add the element
		$(this).prepend("<span class='ripple'></span>");

		
		// Make it round!
		if(buttonWidth >= buttonHeight) {
			buttonHeight = buttonWidth;
		} else {
			buttonWidth = buttonHeight; 
		}
		
		// Get the center of the element
		var x = e.pageX - posX - buttonWidth / 2;
		var y = e.pageY - posY - buttonHeight / 2;
		
		
		// Add the ripples CSS and start the animation
		$(".ripple").css({
			width: buttonWidth,
			height: buttonHeight,
			top: y + 'px',
			left: x + 'px'
		}).addClass("rippleEffect");
	});

    $('.navbar a.dropdown-toggle').on('click', function(e) {
		var $el = $(this);
		var $parent = $(this).offsetParent(".dropdown-menu");
		$(this).parent("li").toggleClass('open');

		if(!$parent.parent().hasClass('nav')) {
			$el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
		}

		$('.nav li.open').not($(this).parents("li")).removeClass("open");

		return false;
	});
    
});


