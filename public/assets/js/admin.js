$(function() {

	$.fn.sumHTML = function(type, absOperand, absFinal) {
        var sum = 0;
        this.each(function() {
			var num = +$(this).html().replace(',', '');
			switch (type) {
				case 'c':
					if (num > 0)
						sum += (num || 0);
					break;
				case 'd':
					if (num < 0)
						sum += (num || 0);
					break;
				default:
					sum += (num || 0);
					break;
			}

			if (absOperand === true)
				$(this).html(Math.abs(num).toLocaleString());
        });
       	
		if (absFinal === true)
			sum = Math.abs(sum);
		   
		return sum; 
    };
	
});

