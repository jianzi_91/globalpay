<?php

return [
    'url'       =>  env("MALL_URL", null),
    'key'       =>  env("MALL_KEY", null),
    'secret'    =>  env("MALL_SECRET", null),
    'token'     =>  env("MALL_TOKEN", null),
];