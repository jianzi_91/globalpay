<?php

return [
    'trade_acc_id' => 1,
    'suspended' => env('TRADE_SUSPENDED', false),
    'batch' => env('TRADE_BATCH'),
    'decimal_point' => env('TRADE_DECIMAL_POINT', 2),
    'min_limit' => env('TRADE_MIN_LIMIT', 0.1),
    'max_limit' => env('TRADE_MAX_LIMIT', 0.299),
    'max_sell_limit' => env('TRADE_MAX_SELL_LIMIT', 0.301),
    'unit_step' => env('TRADE_UNIT_STEP', 0.001),
    'max_step' => env('TRADE_MAX_STEP', 10),
    'investment_rate' => env('TRADE_INVESTMENT_RATE', 0.2),
    'next_min' => env('TRADE_NEXT_MIN', 0.2),
    'qty_limit_per_price' => env('TRADE_QTY_LIMIT_PER_PRICE', 100000),
    'qty_limit_per_price2' => env('TRADE_QTY_LIMIT_PER_PRICE2', 200000),
    'qty_limit_per_price_after_max' => env('TRADE_QTY_LIMIT_PER_PRICE_AFTER_MAX', 400000),
    'min_sell_limit' => 1,
    'min_buy_limit' => 1,
    'charity_rate' => 0.01,
    'processing_rate' => 0.05,
];