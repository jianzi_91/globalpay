<?php

return [

    'register_awallet' => 1.0, //0.8,
    'register_fwallet' => 0.2,
    'release_fwallet' => 0.002,
    'release_swallet' => 0.002,
    'transfer_dowallet_swallet' => 6.0,
    'receive_awallet' => 0.8,
    'receive_fwallet' => 0.2,
    'sponsor_bonus' => 0.1,

];