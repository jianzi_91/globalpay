<?php

return [

    /*
    |--------------------------------------------------------------------------
    | TAC Resend interval
    |--------------------------------------------------------------------------
    |
    | Specify the length of time (in seconds) that a new TAC can be resent.
    | Defaults to 15 seconds
    |
    */

    'resend_interval' => 55,

    /*
    |--------------------------------------------------------------------------
    | TAC time to live
    |--------------------------------------------------------------------------
    |
    | Specify the length of time (in seconds) that the tac will be valid for.
    | Defaults to 300 seconds (5 mins)
    |
    */

    'ttl' => 30000000,

    /*
    |--------------------------------------------------------------------------
    | TAC retry count
    |--------------------------------------------------------------------------
    |
    | Specify the number of times to retry sending.
    | Max retry is 3 if it is exceeded. Min is 1 regardless.
    |
    */

    'retry_send_count' => 100,

    "isms_username" => env("ISMS_USERNAME", null),
    "isms_password" => env("ISMS_PASSWORD", null),
];