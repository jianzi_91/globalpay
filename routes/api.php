<?php

// Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {

//     Route::group(['prefix' => 'public-nuclear/v1', 'namespace' => 'PublicNuclear\v1'], function () {

//         Route::post('login', 'AuthController@authenticate');

//         Route::post('coin-transfer-request', 'NuclearController@verifyUsername');
//         Route::post('coin-transfer-proceed', 'NuclearController@coinTransfer');

//         // for debugging
//         Route::get('test', 'NuclearController@test');
//         // Route::get('encode', 'NuclearController@encodeData');
//         // Route::get('sign', 'NuclearController@signTest');
//     });

// });
Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::post('login', 'LoginController@login');
    Route::post('checkout/confirmation/confirm', 'PurchaseController@confirmation_confirm')->name("checkout-confirmation-confirm");
    Route::post('purchase', 'PurchaseController@purchase');
    Route::post('transaction/detail', 'TransactionController@detail');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user/info', 'UserController@info');
        Route::get('wallet', 'WalletController@getWallet');
        // Route::post('purchase', 'PurchaseController@purchase');
        // Route::post('transaction/detail', 'TransactionController@detail');

        //checkout-confirm
        Route::get('checkout/confirmation', 'PurchaseController@confirmation')->name("checkout-confirmation");
        // Route::post('checkout/confirmation/confirm', 'PurchaseController@confirmation_confirm')->name("checkout-confirmation-confirm");
    });
});
