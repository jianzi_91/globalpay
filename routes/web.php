<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['middleware' => 'maintenance'], function() {

    // default route
    Route::get('/', 'Front\HomeController@index');

    // Authentication Routes...
    Route::get('/login', 'Front\Auth\LoginController@showLoginForm');
    Route::post('login', 'Front\Auth\LoginController@initLogin');
    // Route::any('login', function () {
    // 	return trans("system.coming-soon");
    // });

    Route::get('terms-and-conditions', 'Front\HomeController@showTNC');

    // public is not allowed to register
    // Registration Routes...
    Route::get('register', 'Front\Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Front\Auth\RegisterController@register');

    // Password Reset Routes...
    // Route::post('password/email', 'Front\Auth\ForgotPasswordController@sendResetLinkEmail');
    // Route::get('password/reset/{token}', 'Front\Auth\ResetPasswordController@showResetForm');
    // Route::get('password/reset', 'Front\Auth\ForgotPasswordController@showLinkRequestForm');
    // Route::post('password/reset', 'Front\Auth\ResetPasswordController@reset');
    Route::post('password/mobile', 'Front\Auth\ForgotPasswordController@redirectResetForm');
    Route::get('password/reset/mobile', 'Front\Auth\ResetPasswordController@showMobileResetForm');
    Route::get('password/reset', 'Front\Auth\ForgotPasswordController@showTACRequestForm');
    Route::post('password/reset', 'Front\Auth\ResetPasswordController@resetTAC');
// });

Route::get('/register/check-new-account', 'Front\AjaxController@isNewAccount');

Route::get('captcha/{rand}', 'Front\CaptchaController@index');

// Send TAC
Route::get('send-tac', 'Front\SMSController@sendTac');

// Ajax
Route::get('{type}/user-info', 'Front\AjaxController@getUserInfo');

// Set Lang Locale
Route::get('locale/{lang}', 'Front\HomeController@locale');
Route::get('setlocale', 'Front\HomeController@setlocale');

Route::get('test', 'Admin\HomeController@test');

//cron testing
// Route::get('cron/normal_release/testing', 'Cron\CronController@fwalletRelease');
// Route::get('cron/normal_release/testing', 'Cron\CronController@recalculate_rank');
// Route::get('cron/quicken_release_fwallet', 'Cron\CronController@quicken_release_fwallet');
