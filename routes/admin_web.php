<?php

/*
|--------------------------------------------------------------------------
| Admin Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin route

Route::get(config('app.admin_slug').'/login', 'Admin\Auth\LoginController@getLogin');
Route::post(config('app.admin_slug').'/login', 'Admin\Auth\LoginController@initLogin');

// Route::post(config('app.admin_slug').'/password/reset', 'Admin\Auth\PasswordController@reset');
// Route::get(config('app.admin_slug').'/password/reset/{token?}', 'Admin\Auth\PasswordController@showResetForm');
// Route::post(config('app.admin_slug').'/password/email', 'Admin\Auth\PasswordController@sendResetLinkEmail');

Route::group(array('prefix' => config('app.admin_slug'), 'middleware' => ['admin']), function()
{	
	// User Manage
	Route::get('members', 'Admin\UserController@index')
		->middleware("admin-privilege:members/list");
	Route::get('members/datatable', 'Admin\UserController@ajaxGetDataTable')
		->middleware("admin-privilege:members/list");
	Route::any('members/create', 'Admin\UserController@create')
		->middleware("admin-privilege:members/create");
	Route::any('members/profile/{member}', 'Admin\UserController@editProfile')
		->middleware("admin-privilege:members/edit-profile");
	Route::any('members/profile-sub-account/{member}', 'Admin\UserController@editSubAccountProfile')
		->middleware("admin-privilege:members/edit-sub-profile");

	Route::get('members/send-welcome-email', 'Admin\UserController@getSendWelcomeEmailPage')
		->middleware("admin-privilege:members/send-welcome-email");
	Route::post('members/send-welcome-email', 'Admin\UserController@sendWelcomeEmail')
		->middleware("admin-privilege:members/send-welcome-email");
	
	Route::get("members/passwords", "Admin\UserPasswordsController@index")
		->middleware("admin-privilege:members/edit-passwords");
	Route::get('members/passwords/datatable', 'Admin\UserPasswordsController@ajaxGetDataTable')
		->middleware("admin-privilege:members/edit-passwords");
	Route::get('members/passwords/edit/{id}', 'Admin\UserPasswordsController@edit')
		->middleware("admin-privilege:members/edit-passwords");
	Route::put('members/passwords/edit/{id}', 'Admin\UserPasswordsController@update')
		->middleware("admin-privilege:members/edit-passwords");
	
	Route::get("members/sa1-list", "Admin\UserSA1Controller@index")
		->middleware("admin-privilege:members/sa1-list");
	Route::get('members/sa1-list/datatable', 'Admin\UserSA1Controller@ajaxGetDataTable')
		->middleware("admin-privilege:members/sa1-list");
	
	// Genealogy
	Route::get('sponsor-genealogy', 'Admin\GenealogyController@sponsorGenealogy')
		->middleware("admin-privilege:genealogy/sponsor");
	Route::get('placement-genealogy', 'Admin\GenealogyController@placementGenealogy')
		->middleware("admin-privilege:genealogy/placement");
	Route::get('uplines', 'Admin\UplineController@index')
		->middleware("admin-privilege:uplines");
	Route::get('uplines/datatable', 'Admin\UplineController@ajaxGetDataTable')
		->middleware("admin-privilege:uplines");
	Route::get('networks/check-group', 'Admin\NetworkController@checkGroup')
		->middleware("admin-privilege:networks/check-group");
	Route::post('networks/check-group', 'Admin\NetworkController@isSameGroup')
		->middleware("admin-privilege:networks/check-group");
	Route::get('members/franchise-list', 'Admin\FranchiseController@index')
		->middleware("admin-privilege:members/edit-franchise");
	Route::get('members/franchise-list/datatable', 'Admin\FranchiseController@ajaxGetDataTable')
		->middleware("admin-privilege:members/edit-franchise");
	Route::any('members/change-franchise/{member}', 'Admin\FranchiseController@editFranchiseNetwork')
		->middleware("admin-privilege:members/edit-franchise");
	Route::get('members/network-list', 'Admin\NetworkController@index')
		->middleware("admin-privilege:members/edit-network");
	Route::get('members/network-list/datatable', 'Admin\NetworkController@ajaxGetDataTable')
		->middleware("admin-privilege:members/edit-network");
	Route::any('members/change-network/{member}', 'Admin\UserController@editAccountNetwork')
		->middleware("admin-privilege:members/edit-network");

	// Transaction History
	Route::get('members/transaction-history', 'Admin\TransactionController@index');
	Route::get('members/transaction-history/datatable', 'Admin\TransactionController@ajaxGetDataTable');

	// Activation Manage
	Route::get('activations/{list}', 'Admin\ActivationController@index')
		->middleware("admin-privilege:activations/list|activations/cancel-list");
	Route::get('activations/{list}/datatable', 'Admin\ActivationController@ajaxGetDataTable')
		->middleware("admin-privilege:activations/list|activations/cancel-list");
	
	Route::post('activations/cancel/{id}', 'Admin\ActivationController@cancel')
		->middleware("admin-privilege:activations/cancel");
	Route::post('activations/cancel-confirmed/{id}', 'Admin\ActivationController@cancelConfirmed')
		->middleware("admin-privilege:is-master");

	Route::get('activations-summary', 'Admin\ActivationSummaryController@index')
		->middleware("admin-privilege:activations-summary");
	Route::get('activations-summary/datatable', 'Admin\ActivationSummaryController@ajaxGetDataTable')
		->middleware("admin-privilege:activations-summary");
	Route::get('activations-summary/sub-datatable', 'Admin\ActivationSummaryController@ajaxGetSubDataTable')
		->middleware("admin-privilege:activations-summary");

	Route::get('personal-activations', 'Admin\PersonalActivationController@index')
		->middleware("admin-privilege:personal-activations");
	Route::get('personal-activations/datatable', 'Admin\PersonalActivationController@ajaxGetDataTable')
		->middleware("admin-privilege:personal-activations");

	Route::get('group-activations', 'Admin\GroupActivationController@index')
		->middleware("admin-privilege:group-activations");
	Route::get('group-activations/datatable', 'Admin\GroupActivationController@ajaxGetDataTable')
		->middleware("admin-privilege:group-activations");

	Route::get('small-group-activations-summary', 'Admin\SmallGroupSalesController@index')
		->middleware("admin-privilege:small-group-activations-summary");
	Route::get('small-group-activations-summary/datatable', 'Admin\SmallGroupSalesController@ajaxGetDataTable')
		->middleware("admin-privilege:small-group-activations-summary");

	Route::get('loan-activations', 'Admin\LoanActivationController@index')
		->middleware("admin-privilege:loan-activations");
	Route::get('loan-activations/datatable', 'Admin\LoanActivationController@ajaxGetDataTable')
		->middleware("admin-privilege:loan-activations");
	Route::post("loan-activations/distribute-loan-activation/{id}", "Admin\LoanActivationController@distributeLoanActivation")
		->middleware("admin-privilege:loan-activations/distribute-loan-activation");
	Route::get('loan-activations/settlement', 'Admin\LoanActivationController@getEarlySettlement')
		->middleware("admin-privilege:loan-activations/settlement");
	Route::post('loan-activations/settlement', 'Admin\LoanActivationController@earlySettlement')
		->middleware("admin-privilege:loan-activations/settlement");

	Route::any('bvfree/activations/add', 'Admin\ActivationController@addBVFreeActivation')
		->middleware("admin-privilege:bvfree/activations/add");
	Route::any('loan/activations/add', 'Admin\ActivationController@addLoanActivation')
		->middleware("admin-privilege:loan/activations/add");
	Route::any('special/activations/add', 'Admin\ActivationController@addSpecialActivation')
		->middleware("admin-privilege:special/activations/add");
	
	// Wallet Adjustment
	Route::any('wallets/edit', 'Admin\WalletController@edit')
		->middleware("admin-privilege:wallets/edit|wallets/edit/wallet/rwallet|wallets/edit/wallet/awallet|wallets/edit/wallet/awallet2|wallets/edit/type/debit");
	
	// Wallet History
	Route::get('wallet-records', 'Admin\WalletController@index')
		->middleware("admin-privilege:wallet-records");
	Route::get('wallet-records/datatable', 'Admin\WalletController@ajaxGetDataTable')
		->middleware("admin-privilege:wallet-records");
	Route::get('wallet-topups', 'Admin\WalletTopUpController@index')
		->middleware("admin-privilege:wallet-records");
	Route::get('wallet-topups/datatable', 'Admin\WalletTopUpController@ajaxGetDataTable')
		->middleware("admin-privilege:wallet-records");
		
	// ManualWithdrawal
	Route::get('wallet-manualwithdrawal', 'Admin\ManualWithdrawalController@index')
	->middleware("admin-privilege:manualwithdrawal");
	Route::get('wallet-manualwithdrawal/datatable', 'Admin\ManualWithdrawalController@ajaxGetDataTable')
	->middleware("admin-privilege:manualwithdrawal");
	Route::get('wallet-manualwithdrawal/{id}/edit', 'Admin\ManualWithdrawalController@getEdit')
	->middleware("admin-privilege:manualwithdrawal");
	Route::post('wallet-manualwithdrawal/{id}/edit', 'Admin\ManualWithdrawalController@postEdit')
	->middleware("admin-privilege:manualwithdrawal");
	Route::get('wallet-manualwithdrawal/{id}/view', 'Admin\ManualWithdrawalController@view')
	->middleware("admin-privilege:manualwithdrawal");

	// Withdrawal
	Route::get('withdrawals', 'Admin\WithdrawalController@index')
		->middleware("admin-privilege:withdrawals");
	Route::get('withdrawals/datatable', 'Admin\WithdrawalController@ajaxGetDataTable')
		->middleware("admin-privilege:withdrawals");
	Route::post('withdrawals/multi-update/paid', 'Admin\WithdrawalController@paidMultiWithdrawal')
		->middleware("admin-privilege:withdrawals");
	Route::any('withdrawals/{withdrawal}/edit', 'Admin\WithdrawalController@editWithdrawal')
		->middleware("admin-privilege:withdrawals/edit");

	// Commission
	Route::get('commissions', 'Admin\CommissionController@index')
		->middleware("admin-privilege:commissions");
	Route::get('commissions/datatable', 'Admin\CommissionController@ajaxGetDataTable')
		->middleware("admin-privilege:commissions");

	Route::get('commissions-summary', 'Admin\CommissionSummaryController@index')
		->middleware("admin-privilege:commissions-summary");
	Route::get('commissions-summary/datatable', 'Admin\CommissionSummaryController@ajaxGetDataTable')
		->middleware("admin-privilege:commissions-summary");
	Route::get('commissions-summary/sub-datatable', 'Admin\CommissionSummaryController@ajaxGetSubDataTable')
		->middleware("admin-privilege:commissions-summary");

	Route::get('commissions-analysis', 'Admin\CommissionAnalysisController@index')
		->middleware("admin-privilege:commissions-analysis");
	Route::get('commissions-analysis/datatable', 'Admin\CommissionAnalysisController@ajaxGetDataTable')
		->middleware("admin-privilege:commissions-analysis");
	
	Route::get('pairing-history', 'Admin\MemberCFStatDailyController@index')
		->middleware("admin-privilege:pairing-history");
	Route::get('pairing-history/datatable', 'Admin\MemberCFStatDailyController@ajaxGetDataTable')
		->middleware("admin-privilege:pairing-history");
	
	Route::get('paired-max-summary', 'Admin\PairedMaxSummaryController@index')
		->middleware("admin-privilege:paired-max-summary");
	Route::get('paired-max-summary/datatable', 'Admin\PairedMaxSummaryController@ajaxGetDataTable')
		->middleware("admin-privilege:paired-max-summary");


	// News
	Route::get('news', 'Admin\NewsController@index')
		->middleware("admin-privilege:news");
	Route::get('news/create', 'Admin\NewsController@create')
		->middleware("admin-privilege:news");
	Route::post('news/store', 'Admin\NewsController@store')
		->middleware("admin-privilege:news");
	Route::get('news/{id}/edit', 'Admin\NewsController@edit')
		->middleware("admin-privilege:news");
	Route::patch('news/{id}', 'Admin\NewsController@update')
		->middleware("admin-privilege:news");

	// Currency Exchange
	Route::get('exchange', 'Admin\ExchangeController@index')
		->middleware("admin-privilege:exchange");
	Route::get('exchange/create', 'Admin\ExchangeController@create')
		->middleware("admin-privilege:exchange");
	Route::post('exchange', 'Admin\ExchangeController@store')
		->middleware("admin-privilege:exchange");
	Route::get('exchange/{id}/edit', 'Admin\ExchangeController@edit')
		->middleware("admin-privilege:exchange");
	Route::patch('exchange/{id}', 'Admin\ExchangeController@update')
		->middleware("admin-privilege:exchange");
	Route::delete('exchange/{id}', 'Admin\ExchangeController@delete')
		->middleware("admin-privilege:exchange");
	Route::get('exchange/history', 'Admin\ExchangeController@history')
		->middleware("admin-privilege:exchange");

	// Trade
	// Route::get('trade/history', 'Admin\TradeHistoryController@index')
	// 	->middleware("admin-privilege:trade/history");
	// Route::get('trade/history/datatable', 'Admin\TradeHistoryController@ajaxTradeHistoryDataTable')
	// 	->middleware("admin-privilege:trade/history");
	// Route::get('trade/history/non-trade', 'Admin\TradeNonHistoryController@index')
	// 	->middleware("admin-privilege:trade/history/non-trade");
	// Route::get('trade/history/non-trade/datatable', 'Admin\TradeNonHistoryController@ajaxNonTradeHistoryDataTable')
	// 	->middleware("admin-privilege:trade/history/non-trade");
	Route::get('trade/{queueId}/confirm', 'Admin\TradeQueueController@approveBuyRequest');
	Route::get('trade/{queueId}/cancel', 'Admin\TradeQueueController@rejectBuyRequest');
	Route::get('trade/history/sell', 'Admin\TradeSellController@index')
		->middleware("admin-privilege:trade/history/sell");
	Route::get('trade/history/sell/datatable', 'Admin\TradeSellController@ajaxTradeSellDataTable')
		->middleware("admin-privilege:trade/history/sell");
	Route::get('trade/history/queue', 'Admin\TradeQueueController@index')
		->middleware("admin-privilege:trade/history/queue");
	Route::get('trade/history/queue/datatable', 'Admin\TradeQueueController@ajaxTradeQueueDataTable')
		->middleware("admin-privilege:trade/history/queue");
	Route::get('trade/history/buy', 'Admin\TradeBuyController@index')
		->middleware("admin-privilege:trade/history/buy");
	Route::get('trade/history/buy/datatable', 'Admin\TradeBuyController@ajaxTradeBuyDataTable')
		->middleware("admin-privilege:trade/history/buy");
	// Route::get('trade/summary', 'Admin\TradeSummaryController@index')
	// 	->middleware("admin-privilege:trade/summary");
	// Route::get('trade/summary/datatable', 'Admin\TradeSummaryController@ajaxTradeSummaryDataTable')
	// 	->middleware("admin-privilege:trade/summary");
	// Route::get('trade/balance', 'Admin\TradeBalanceController@index')
	// 	->middleware("admin-privilege:trade/balance");
	// Route::get('trade/balance/datatable', 'Admin\TradeBalanceController@ajaxTradeBalanceDataTable')
	// 	->middleware("admin-privilege:trade/balance");
	// Route::get('trade/edit-unit', 'Admin\TradeController@editTradeUnit')
	// 	->middleware("admin-privilege:trade/edit-unit");
	// Route::post('trade/edit-unit', 'Admin\TradeController@postEditTradeUnit')
	// 	->middleware("admin-privilege:trade/edit-unit");

	// Report
	Route::get('report/member-country', 'Admin\UserReportController@countryReport')
		->middleware("admin-privilege:report/member-country");
	Route::get('report/member-country/datatable', 'Admin\UserReportController@ajaxGetCountryReportDataTable')
		->middleware("admin-privilege:report/member-country");
	Route::get('report/member-wallet', 'Admin\UserReportController@walletReport')
		->middleware("admin-privilege:report/member-wallet|report/member-wallet/only-swallet");
	Route::get('report/member-wallet/datatable', 'Admin\UserReportController@ajaxGetWalletReportDataTable')
		->middleware("admin-privilege:report/member-wallet|report/member-wallet/only-swallet");
	Route::get('report/member-wallet/sub/datatable', 'Admin\UserReportController@ajaxGetSubWalletReportDataTable')
		->middleware("admin-privilege:report/member-wallet|report/member-wallet/only-swallet");
	Route::get('report/trade-allocation', 'Admin\TradeReportController@tradeReport')
		->middleware("admin-privilege:report/trade-allocation");
	Route::get('report/trade-allocation/datatable', 'Admin\TradeReportController@ajaxGetTradeReportDataTable')
		->middleware("admin-privilege:report/trade-allocation");
	Route::get('report/trade-allocation/company-account/datatable', 'Admin\TradeReportController@ajaxGetCompanyAccountReportDataTable')
		->middleware("admin-privilege:report/trade-allocation");
	Route::get('report/sales-commission', 'Admin\CommissionReportController@salesReport')
		->middleware("admin-privilege:report/sales-commission");
	Route::get('report/sales-commission/datatable', 'Admin\CommissionReportController@ajaxGetCommissionReportDataTable')
		->middleware("admin-privilege:report/sales-commission");

	// Log History
	Route::get('loggers', 'Admin\LoggerController@index')
		->middleware("admin-privilege:loggers");
	Route::get('loggers/datatable', 'Admin\LoggerController@ajaxGetDataTable')
		->middleware("admin-privilege:loggers");

	// Admin Manage
	Route::get('adminusers', 'Admin\AdminUserController@index')
		->middleware("admin-privilege:adminusers");
	Route::get('adminusers/datatable', 'Admin\AdminUserController@ajaxGetDataTable')
		->middleware("admin-privilege:adminusers");
	Route::any('adminusers/create', 'Admin\AdminUserController@create')
		->middleware("admin-privilege:adminusers");
	Route::get('adminusers/profile/{admin}', 'Admin\AdminUserController@edit')
		->middleware("admin-privilege:adminusers");
	Route::put('adminusers/profile/{admin}', 'Admin\AdminUserController@update')
		->middleware("admin-privilege:adminusers");
	
	// Setting
	Route::match(["get", "post"], 'settings', 'Admin\SettingController@general')
		->middleware("admin-privilege:is-master");
	Route::match(["get", "post"], 'settings/active_bonus', 'Admin\SettingController@activeBonus')
		->middleware("admin-privilege:is-master");
	Route::delete('settings/active_bonus/delete/{id}', 'Admin\SettingController@deleteActiveBonus')
		->middleware("admin-privilege:is-master");
	Route::match(["get", "post"], 'settings/passive_bonus', 'Admin\SettingController@passiveBonus')
		->middleware("admin-privilege:is-master");
	Route::delete('settings/passive_bonus/delete/{id}', 'Admin\SettingController@deletePassiveBonus')
		->middleware("admin-privilege:is-master");

	/* General Routes...*/
	// Home
	Route::get('/', 'Admin\HomeController@index');
	Route::get('home', 'Admin\HomeController@home');
	
	// Profile
	Route::any('profile', 'Admin\AdminUserController@profileEdit');
	
	// Logout
	Route::post('logout', 'Admin\Auth\LoginController@logout');
});
