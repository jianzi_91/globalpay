<?php

/*
|--------------------------------------------------------------------------
| Member Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('member')
	->middleware(['auth:web', 'auth.per.ip:web'])
	->group(function() {
	
	// Register & upgrade
    // Route::match(["get", "post"], 'register', 'Member\UserController@create');
    Route::get('register', 'Member\UserController@myRegisterQR');
	// Route::any('upgrade', 'Member\UserController@upgrade');

    // Route::get('purchase-history', 'Member\ActivationController@purchaseList');
    // Route::get('package-history', 'Member\ActivationController@activateList');
    // Route::get('group-sales-history', 'Member\ActivationController@groupSalesList');

    // Activate SA1
    // Route::any('activate-special-account', 'Member\UserController@activateSpecialAccount');
    
    // Genealogy
    // Route::get('sponsor-genealogy', 'Member\GenealogyController@sponsorGenealogy');
    // Route::get('placement-genealogy', 'Member\GenealogyController@placementGenealogy');

	// Commission
	// Route::get('commission-summary', 'Member\CommissionController@summary');
	// Route::get('roi-summary', 'Member\CommissionController@roiSummary');
	// Route::get('{type}/commission-list', 'Member\CommissionController@detailList')
    	// ->where('type', '(1001|2001)');

    // Franchise
    // Route::any('franchise/register', 'Member\FranchiseController@create');
    // Route::any('franchise/upgrade-topup', 'Member\FranchiseController@upgradeTopUp');
    // Route::any('franchise/fwallet/internal-transfer', 'Member\FranchiseController@franchiseTransfer');

    // Trade
    // Route::get('trade', 'Member\TradeController@index');
    // Route::get('trade/chart', 'Member\TradeController@tradeChart');
    // Route::get('trade/buy', 'Member\TradeController@buy')->name('buy-lobby');
    // Route::any('trade/buy/create', 'Member\TradeController@createTradeBuy')->name('create-trade-buy');
    // Route::get('trade/buy/list', 'Member\TradeController@getMemberTradeBuy')->name('my-trade-buy');
    // Route::any('trade/buy/{orderId}/request', 'Member\TradeController@buyRequest')->middleware('market');
    // Route::get('trade/buy/{orderId}/request/reject', 'Member\TradeController@rejectBuyRequest')->name('reject-buy-request');
    // Route::any('trade/buy/details/{queueId}', 'Member\TradeController@buyDetails')->name('buy-details');
    // Route::post('trade/buy/pay', 'Member\TradeController@payBuyRequest')->middleware('market');
    // Route::post('trade/buy/delete', 'Member\TradeController@stopTradeBuy');
    // Route::post('trade/buy/rate', 'Member\TradeController@rateTradeBuy');
    // Route::get('trade/buy/{orderId}/view', 'Member\TradeController@viewTradeBuy');

    // Route::get('trade/sell', 'Member\TradeController@sell');
    // Route::any('trade/sell/create', 'Member\TradeController@createTradeSell');
    // Route::get('trade/sell/list', 'Member\TradeController@getMemberTradeSell')->name('my-trade-sell');
    // Route::any('trade/sell/{orderId}/request', 'Member\TradeController@sellRequest')->middleware('market');
    // Route::get('trade/sell/{orderId}/request/reject', 'Member\TradeController@rejectSellRequest')->name('reject-sell-request');
    // Route::any('trade/sell/details/{queueId}', 'Member\TradeController@sellDetails')->name('sell-details');
    // Route::post('trade/sell/approve', 'Member\TradeController@approveBuyRequest')->middleware('market');
    // Route::post('trade/sell/delete', 'Member\TradeController@stopTradeSell');
    // Route::get('trade/sell/{orderId}/view', 'Member\TradeController@viewTradeSell');
    // Route::post('trade/sell/rate', 'Member\TradeController@rateTradeSell');

    // Route::get('trade/records', 'Member\TradeController@getTradeRecords')->name('trade-records');

	// Wallet
	// Route::get('{wallet}/wallet-history', 'Member\WalletController@index')
    // 	->where('wallet', '(rwallet|fwallet|awallet|awallet2|cwallet|swallet|dowallet)');
	// Route::match(["get", "post"], '{wallet}/wallet-transfer', 'Member\WalletController@transfer')
    //     ->where('wallet', '(rwallet|fwallet|awallet|cwallet)');
    Route::get('{wallet}/wallet-history', 'Member\WalletController@index')
    	->where('wallet', '(awallet|dowallet|swallet|cwallet)');
	Route::match(["get", "post"], '{wallet}/wallet-transfer', 'Member\WalletController@transfer')
        ->where('wallet', '(awallet|dowallet)');
    Route::match(["get", "post"], '{wallet}/wallet-transfer/repeat', 'Member\WalletController@transferRepeat')
        ->where('wallet', '(dowallet)');
    Route::match(["get", "post"], '{wallet}/wallet-transfer/purchase-scwallet', 'Member\WalletController@purchaseScwallet')
    	->where('wallet', '(awallet)');
	
	// Route::get('mall/micool-transfer', 'Member\WalletController@getPartnerTransferPage');
	// Route::post('mall/micool-transfer', 'Member\WalletController@partnerTransfer');

    // Withdrawal
    // Route::get('{wallet}/wallet-withdraw-history', 'Member\WithdrawalController@index')
    //  ->where('wallet', '(cwallet)');
    // Route::any('{wallet}/wallet-withdraw', 'Member\WithdrawalController@withdraw')
    //  ->where('wallet', '(cwallet)');

    // News & Event
    // Route::get('news', 'Member\NewsController@index');
    // Route::get('news/{id}', 'Member\NewsController@show');

    // Currency Exchange
    // Route::get('exchange', 'Member\ExchangeController@index');
    // Route::post('exchange/convert', 'Member\ExchangeController@convert');

	/* General Routes...*/
	// Home
	Route::get('/', 'Member\HomeController@index')->name("home");
	Route::get('home', 'Member\HomeController@home');
	
	// Download Area
	// Route::get('download-area', 'Member\DownloadAreaController@index');

	// Profile
	Route::match(["get", "post"], 'profile', 'Member\UserController@profileEdit');
	// Route::get('profile/sponsor', 'Member\UserController@sponsorEdit');
	// Route::post('profile/sponsor', 'Member\UserController@sponsorEdit');

	Route::match(["get", "post"], 'mobile/verify', 'Member\MobileController@verify');
	
	// Logout
    Route::post('logout', 'Front\Auth\LoginController@logout');
    
    //Mall Product Listing
    Route::get('product_listing_mall', 'Member\MallProductController@index');
    Route::get('product_details_mall/{id}', 'Member\MallProductController@product_details');
    Route::get('transaction_history', 'Member\MallProductController@transaction_history');
    Route::get('transaction_details_mall/{transaction_id}', 'Member\MallProductController@transaction_details');

    //cashout
    Route::get('withdrawal', 'Member\WithdrawalController@index')->name('member-withdrawal');
    Route::post('withdrawal/confirm', 'Member\WithdrawalController@withdrawal_confirm');
    Route::post('withdrawal/process', 'Member\WithdrawalController@withdrawal_process');
    Route::get('withdrawal/history', 'Member\WithdrawalController@withdrawal_history');

    //transaction history
    Route::get('transaction-history', 'Member\TransactionHistoryController@index')->name('member-transaction-history');
    Route::get('transaction-history/datatable', 'Member\TransactionHistoryController@ajaxGetDataTable');

});
