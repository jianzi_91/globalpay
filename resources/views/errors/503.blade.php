@extends('errors.layouts.app')

@section('content')
<!-- <div class="general-container">
    <div class="styled-heading">
        Notice
    </div>
    <div class="alert alert-warning">
        <p>
            System is under maintenance for serve you better. Thank you for your patience and understanding.
        </p>
    </div>
</div> -->
<div class="general-container">
    <div class="styled-heading">
        注意
    </div>
    <div class="alert alert-warning">
        <p>
            为了更好的为您服务，系统正在维修中。感谢您的忍耐与体谅。
        </p>
    </div>
</div>
@endsection