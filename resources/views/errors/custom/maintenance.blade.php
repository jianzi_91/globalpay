@extends('errors.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans("general.page.errors.custom.maintenance.bar-title.maintenance") }}</div>
                <div class="panel-body">
                    {{ trans("general.page.errors.custom.maintenance.content.maintenance") }}
                    
                    @if (isset($__info["start"]) && isset($__info["end"]))
                    ({{ $__info["start"] }} - {{ $__info["end"] }})
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection