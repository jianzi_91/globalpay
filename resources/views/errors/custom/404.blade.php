@extends('errors.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans("general.page.errors.custom.404.bar-title.404") }}</div>
                <div class="panel-body">
                    {{ trans("general.page.errors.custom.404.content.404") }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection