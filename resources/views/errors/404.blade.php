@extends('errors.layouts.app')

@section('content')
<div class="general-container">
    <div class="styled-heading">
        {{ trans("general.page.errors.custom.404.bar-title.404") }}
    </div>
    <div class="alert alert-warning">
        <p>{{ trans("general.page.errors.custom.404.content.404") }}</p>
    </div>
</div>
@endsection