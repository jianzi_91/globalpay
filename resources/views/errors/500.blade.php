@extends('errors.layouts.app')

@section('content')
<div class="general-container">
    <div class="styled-heading">
        {{ trans("general.page.errors.custom.general-error.bar-title.general-error") }}
    </div>
    <div class="alert alert-warning">
        <p>{{ trans("general.page.errors.custom.general-error.content.general-error") }}</p>
    </div>
</div>
@endsection