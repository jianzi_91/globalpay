<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="theme-color" content="#1e1e23" />

    <title>{{ trans('system.company') }}</title>
	<link rel="shortcut icon" href="assets/images/favicon.png">

    <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/default/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/plugin/pace/pace_minimal.css">
	<link rel="stylesheet" href="/assets/css/globalpay_style.css?v6">
</head>
<body> 
    <div class="page-wrap auth">
        <div class="content">
            <div class="auth-container">
                <div class="inner">
                    <div class="title" style="background-image:url(/assets/images/auth/login_bg.jpg);">
                        <img src="assets/images/gpay.png" class="brand">
                        {{ trans('general.page.public.login.bar-title.login') }}
                    </div>
                    <div class="form">

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
                        <form role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('login') ? ' has-error' : '' }}">
							<i class="icon-phone"></i>
                            <input id="login" type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name="login" value="{{ old('login') }}" tabindex="1" autofocus=true>
                            @if ($errors->has('login'))
                                <span class=help-block>{{ $errors->first('login') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <i class="icon-password"></i>
                            <input id="password" type="password" class="form-control" name="password" tabindex="2" placeholder="{{ trans('field.user.password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('_captcha') ? ' has-error' : '' }}">
                            <i class="icon-captcha"></i>
                            <input id="_captcha" type="text" class="form-control" name="_captcha" tabindex="3" autocomplete="off" placeholder="{{ trans('general.field.verification-code') }}">
                            <img src="{{ url('captcha', rand()) }}" id='captchaimg'  class="captcha-img">
                            <span class="captcha-refresh"> {!! trans('general.page.public.login.content.captcha-helper', array('link'=>"<a href='javascript: refreshCaptcha();'>", 'end_link'=>"</a>")) !!}</span>

                            @if ($errors->has('_captcha'))
                                <span class="help-block">{{ $errors->first('_captcha') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
                            <label class="checkbox">
                                <input type="checkbox" name="terms">
                                <span>{!! trans('general.page.public.login.content.terms', ["link"=>url("terms-and-conditions")]) !!}</span>
                            </label>
                            @if ($errors->has('terms'))
                                <span class="help-block">{{ $errors->first('terms') }}</span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary btn-block" tabindex="5">{{ trans('general.button.login') }}</button> 
                        
                        </form>

                        <div class="other-action">
                            <a href="{{ url('/password/reset') }}">{{ trans('general.page.public.login.content.forgot password') }}</a>
                        </div>
                        
                    </div>
                </div>

                <ul class="language">
                    <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
                    <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
                </ul>

            </div>
        </div>
    </div> 

    <!-- JavaScripts -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
    <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/app.global-pay.js"></script>
    <script type="text/javascript" src="/plugin/pace/pace.min.js"></script>

	{{--  <script type="text/javascript" src="/assets/plugins/particlesjs/particles.js"></script>  --}}
	<script type="text/javascript" src="/assets/plugins/particlesjs/app.js"></script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
        autoLogout('login', {{ config('session.lifetime') }});
        $(function() {
            setLocale('{{ url("setlocale") }}');

            $('ins#ins-terms').keypress(function(e) {
                if (e.keyCode == 0 || e.keyCode == 32) {
                    // `0` works in mozilla and `32` in other browsers
                    $('ins#ins-terms').click();
                    e.preventDefault();
                }
            });
        });
    </script>
    @include('partials.general-script')
</body>
</html>