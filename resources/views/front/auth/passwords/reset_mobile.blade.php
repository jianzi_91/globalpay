@extends('layouts.generic')

@section('content')
<div class="auth-container">
    <div class="inner">
        <div class="title" style="background-image:url(/assets/images/auth/login_bg.jpg);">
            <img src="/assets/images/gpay.png" class="brand">
            {{ trans('general.page.public.reset.bar-title.reset password') }}
        </div>

        <div class="form normal">
            <form role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                    <label for="mobile" class="control-label">{{ trans('field.user.mobile') }}</label>
                    <p class="form-control-static">{{ $reset_mobile }}</p>
                </div>
                        
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">{{ trans('field.user.password') }}</label>
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="control-label">{{ trans('field.user.password_confirmation') }}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password2') ? ' has-error' : '' }}">
                    <label for="password2" class="control-label">{{ trans('field.user.password2') }}</label>
                    <input id="password2" type="password" class="form-control" name="password2">

                    @if ($errors->has('password2'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password2') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password2_confirmation') ? ' has-error' : '' }}">
                    <label for="password2-confirm" class="control-label">{{ trans('field.user.password2_confirmation') }}</label>
                    <input id="password2-confirm" type="password" class="form-control" name="password2_confirmation">

                    @if ($errors->has('password2_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password2_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    <i class="fa fa-btn fa-refresh"></i> {{ trans('general.button.submit') }}
                </button>
            </form>
            
        </div>
    </div>
    
    <ul class="language">
        <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
        <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
    </ul>
</div>
@endsection
