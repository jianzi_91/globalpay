@extends('layouts.generic')

@section('content')

<div class="auth-container">
    <div class="inner">
        <div class="title" style="background-image:url(/assets/images/auth/login_bg.jpg);">
                    
            <img src="/assets/images/gpay.png" class="brand">
            {{ trans('general.page.public.email.bar-title.reset password') }}
        </div>
        <div class="form normal">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                    

            <form role="form" method="POST" action="{{ url('/password/mobile') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                    <label for="login" class="control-label">{{ trans('field.user.mobile') }}</label>
                    <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}">

                    @if ($errors->has('login'))
                        <span class="help-block">
                            <strong>{{ $errors->first('login') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="form-group{{ $errors->has('verification_code') ? ' has-error' : '' }}">
                    <label class="control-label">{{ trans('field.code.verification_code') }}</label>
                    <div class="input-container">
                        <input type="text" class="form-control" name='verification_code' value='' autocomplete="off">
                        <button id="btn-request_tac" type="button">{{ trans("general.button.request_TAC") }}</button>
                    </div>

                    <div id="tac_msg"></div>

                    @if ($errors->has('verification_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('verification_code') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    <i class="fa fa-btn fa-envelope"></i> {{ trans('general.button.submit') }}
                </button>

                <div class="other-action">
                    <a href="{{ url('/login') }}">{{ trans('general.page.public.login.content.back_login') }}</a>
                </div>
                        
            </form>
            
        </div>
    </div>
    <ul class="language">
        <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
        <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
    </ul>
</div>
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('auth');
</script>
<script>
    $(function () {

        function countdown(expired_date, server_date)
        {
            var nowx = new Date(server_date).getTime();
            var expiredDate = new Date(expired_date).getTime();
            var dateDiffTime = expiredDate - nowx;
            var fiveMinTime = (5 * 60 * 1000);

            var expired = new Date().getTime() + dateDiffTime;

            if (dateDiffTime > fiveMinTime)
                expired = new Date().getTime() + fiveMinTime;

            var x = setInterval(function() {
                var now = new Date().getTime();
                var distance = expired - now;
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                var message = "{{ trans('sms_tac.content.timer') }}";
                message = message.replace(":minute", minutes);
                message = message.replace(":second", seconds);

                $("#btn-request_tac").html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + message).prop('disabled', true);

                if (distance < 0) {
                    clearInterval(x);
                    $("#btn-request_tac").html("{{ trans('general.button.request_TAC') }}").prop('disabled', false);
                }
            }, 1000);
        }

        $('#btn-request_tac').click(function() {
            var mobile = $("input[name=login]").val();
            var lang = '{{ app()->getLocale() }}';

            $.get("/send-tac", {mobile: mobile, lang: lang, action: 'reset_password'})
            .done(function( data ) {
                if (data.status == '0') {
                    $('#tac_msg').removeClass('alert alert-success');
                    $('#tac_msg').addClass('alert alert-danger');

                    $('#tac_msg').text(data.errmsg[0]);
                } else {
                    $('#tac_msg').removeClass('alert alert-danger');
                    $('#tac_msg').addClass('alert alert-success');

                    $('#tac_msg').text(data.message);

                    // Call countdown
                    countdown(data.expired_at, data.server_date);
                }
            });
        });
    });
</script>
@endsection