@extends('layouts.generic')

@section('content')
<div class="general-container">
    <div class="styled-heading">{{ trans('general.page.user.register.bar-title.register') }}</div>
    
    @include('member.common.errors')

    <form id="form-create-user" role="form" action="{{ url('/register') }}" method="POST">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name" class="control-label">{{ trans('field.user.name') }} <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" name='name' value='{{old('name')}}'>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nid" class="control-label">{{ trans('field.user.nid') }} <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" name='nid' value='{{old('nid')}}'>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.email') }} <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" name='email' value='{{old('email')}}'>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.locale') }}</label>
                    <select class="form-control" name="locale">
                    @foreach ($__fields['locale']['options'] AS $_code=>$_locale)
                        <option value='{{ $_code }}' {{ ((old('locale')) == $_code ) ?'selected':'' }}>{{ $_locale }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.country') }} <span style="color:red;">*</span></label>
                    <select class="form-control" name="country" onchange="return countryMobilePlaceholder(this.options[this.selectedIndex].value)">
                        <option value="CN">@lang('general.selection.country.china')</option>
                        <option value="MY">@lang('general.selection.country.malaysia')</option>
                        <option disabled><hr></option>
                    @foreach ($__fields['country']['options'] AS $_code=>$_country)
                        <option value='{{ $_code }}'>{{ $_country }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nid" class="control-label">{{ trans('field.user.mobile') }} <span style="color:red;">*</span></label>
                    <input id="mobile" type="text" class="form-control" name='mobile' value='{{old('mobile')}}' placeholder="{{ trans('field.placeholder.mobile.CN') }}" onkeypress="return numberOnly(event)">
                    <input id="sub-acc" type="hidden" class="form-control" name='sub_acc' value="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.up') }}</label>
                    <div class="input-container">
                        <input type="text" class="form-control" name="up" value="{{ $referrer->username }}" disabled>
                        <input type="hidden" name="ref" value="{{ $referrer->username }}">
                    </div>
                </div>
            </div>
            <input type="hidden" value="1" name="up_pos">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.password') }} <span style="color:red;">*</span></label>
                    <input type="password" class="form-control" name='password' value=''>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.password_confirmation') }} <span style="color:red;">*</span></label>
                    <input type="password" class="form-control" name='password_confirmation' value=''>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.password2') }} <span style="color:red;">*</span></label>
                    <input type="password" class="form-control" name='password2' value=''>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.password2_confirmation') }} <span style="color:red;">*</span></label>
                    <input type="password" class="form-control" name='password2_confirmation' value=''>
                </div>
            </div>
        </div>

        <div class="form-action">
            <button id="btn-submit" class="btn btn-primary" type="button">{{ trans('general.button.proceed') }}</button>
        </div>
    </form>
    
</div>
@endsection

@section('script')
<script src="/plugin/bootbox/bootbox.min.js"></script>
<script>
function numberOnly(e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return (k >= 48 && k <= 57);
}

function countryMobilePlaceholder(country) {

    console.log(country);

    if (country == 'CN') {
        $('#mobile').attr('placeholder', "{!! trans('field.placeholder.mobile.CN') !!}");
    }
    else if (country == 'MY') {
        $('#mobile').attr('placeholder', "{!! trans('field.placeholder.mobile.MY') !!}");
    }
    else {
        $('#mobile').attr('placeholder', "{{ trans('field.placeholder.mobile.default') }}");
    }

}

$(document).ready(function(){
    $('#btn-submit').click(function() {
        var mobile = $('#mobile').val();

        $.ajax({
            type: 'GET',
            dataType: "json",
            data: { 'mobile': mobile},
            url: "{{ url('register/check-new-account') }}",
            onEscape: true,
            backdrop: true,
            success: function (dataJson) {

                if (dataJson.status == true) {
                    $('#sub-acc').val(false);
                    $('#form-create-user').submit();
                }
                else {
                    bootbox.confirm({ 
                        title: "{{ trans('general.page.user.register.bar-title.register-sub-account') }}",
                        message: "{{ trans('general.page.user.register.msg.create-sub-alert') }}",
                        buttons: {
                            confirm: {
                                label: "{{ trans('general.button.confirm') }}",
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: "{{ trans('general.button.cancel') }}",
                                className: 'btn-default'
                            }
                        },
                        callback: function(result){

                            if (result) {
                                $('#sub-acc').val(true);
                                $('#form-create-user').submit();
                            }
                            else {
                                $('#sub-acc').val(false);
                            }

                        },
                    });
                }
            }
        });
    });
});

</script>
@endsection