@extends('layouts.generic')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('general.page.public.register.bar-title.register')</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">@lang("field.user.name")</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nid') ? ' has-error' : '' }}">
                            <label for="nid" class="col-md-4 control-label">@lang("field.user.nid")</label>

                            <div class="col-md-6">
                                <input id="nid" type="text" class="form-control" name="nid" value="{{ old('nid') }}" autofocus>

                                @if ($errors->has('nid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nid') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang("field.user.email")</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">@lang("field.user.mobile")</label>

                            <div class="col-md-6">
                                <select class="form-control"  name="phone_code">
                                    <option value=''>-- {{ trans('general.field.phone_code') }} --</option>
                                @foreach($__fields['phone_code']['options'] AS $_code=>$_phone_code)
                                    <option value='{{ $_code }}' phone-code='{{ $_phone_code['phone_code'] }}'>{{ $_phone_code['phone_code_concat'] }}</option>
                                @endforeach
                                </select>

                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang("field.user.password")</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">@lang("field.user.password_confirmation")</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password2') ? ' has-error' : '' }}">
                            <label for="password2" class="col-md-4 control-label">@lang("field.user.password2")</label>

                            <div class="col-md-6">
                                <input id="password2" type="password" class="form-control" name="password2">

                                @if ($errors->has('password2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password2-confirm" class="col-md-4 control-label">@lang("field.user.password2_confirmation")</label>

                            <div class="col-md-6">
                                <input id="password2-confirm" type="password" class="form-control" name="password2_confirmation">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ref') ? ' has-error' : '' }}">
                            <label for="ref" class="col-md-4 control-label">@lang("field.user.ref")</label>

                            <div class="col-md-6">
                                <input id="ref" type="text" class="form-control" name="ref">
                                <button class="btn btn-sm btn-primary" type="button" for='ref'>{{ trans('general.button.check') }}</button>
                                <span id='ref-text'></span>

                                @if ($errors->has('ref'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ref') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang("general.button.register")
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
<script>
    $(function () {
        // check username
        $( "button[for=ref]" ).on("click", function() {
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else {
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        //phone code helper
        $("select[name=phone_code]").on("change", function(){
            var val = $($(this).find('option[value='+$(this).val()+']')).attr('phone-code');
            $("input[name=mobile][type=text]").val(val);
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection
