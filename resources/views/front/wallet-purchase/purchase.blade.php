@extends('layouts.generic', ["___login" => false])
@section('title', e(trans('general.pagetitle.user.wallet-purchase', ['wallet'=>trans('general.wallet.rwallet')])))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.wallet-purchase.bar-title.wallet-purchase', ['wallet'=>trans('general.wallet.rwallet')]) }}</h2>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">
            @if ($__fields['__step']['value'] == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-purchase.bar-title.wallet-purchase-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.ref_id') }}</td><td>{{ $__wallet_purchase->ref_id }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.amount') }} ({{ trans("general.wallet.rwallet") }})</td><td>{{ number_format($__wallet_purchase->amount, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.rmb-amount') }}</td><td>{{ number_format($__wallet_purchase->amount_currency, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.remarks') }}</td><td>{{ $__wallet_purchase->remarks }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.status-jubaoding') }}</td><td>{{ trans("general.wallet_purchase.jubaoding_status.".$__return_data["STATUS"]) }}</td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="#" onclick="location.reload();" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif ($__fields['__step']['value'] == 'redirect')
            {{-- ######## REDIRECT ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-purchase.bar-title.wallet-purchase-info') }}
                </div>

                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" id="payment-form" action="{{ $__post_url }}" method="POST">
                        @foreach($__post_data as $_field => $_value)
						    <input type='hidden' name='{{ $_field }}' value='{{ $_value }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.redirect') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            {{-- ######## END REDIRECT ########## --}}

            @elseif ($__fields['__step']['value'] == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-purchase.bar-title.wallet-purchase-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.amount') }} ({{ trans("general.wallet.rwallet") }})</td><td>{{ number_format(old('amount'), 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.rmb-amount') }}</td><td>{{ number_format($__amount_currency, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_purchase.remarks') }}</td><td>{{ old('remarks') }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__step' value='redirect'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        @foreach(['amount', 'remarks', 'username', 'epassword'] as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}
            
            @elseif ($__fields['__step']['value'] == 'start')
            {{-- ######## START ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-purchase.bar-title.wallet-purchase', ['wallet'=>trans('general.wallet.rwallet')]) }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__step' value='preview'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_purchase.amount') }} ({{ trans("general.wallet.rwallet") }})</label>
                            <div class="col-md-6">
                                <input type="number" step=".01" placeholder="{{ trans('field.wallet_record.amount') }}" class="form-control" name='amount' value='{{ old('amount') }}'>
                                <span class="help-block m-b-none">{{ trans('general.page.user.wallet-purchase.content.amount-helper') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_purchase.rmb-amount') }}</label>
                            <div class="col-md-6">
                                <input type="number" step=".01" placeholder="{{ trans('field.wallet_purchase.rmb-amount') }}" class="form-control" id="rmb-amount" value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_purchase.remarks') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_purchase.remarks') }}" class="form-control" name='remarks' value='{{ old('remarks') }}'>
                            </div>
                        </div>

                        <hr/>
						
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='username' value='{{ old('username') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('_captcha') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label" for="_captcha">{{ trans('general.field.verification-code') }}</label>
                            <div class="col-md-6">
                                <input id="_captcha" type="text" class="form-control" name="_captcha" autocomplete="off">
                                <span class="highlight"></span>
                                <div class="captcha">
                                    <img src="{{ url('captcha', rand()) }}" id='captchaimg'>
                                    <span> {!! trans('general.page.public.login.content.captcha-helper', array('link'=>"<a href='javascript: refreshCaptcha();'>", 'end_link'=>"</a>")) !!}</span>
                                </div>
                            </div>
                            
                            @if ($errors->has('_captcha'))
                                <span class="help-block">{{ $errors->first('_captcha') }}</span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END START ########### --}}
            @endif
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //back button
        $("#back-button").click(function(e) {
            $("input[name=__step]").val("start");
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });

        $("input[name=amount][type=number],input#rmb-amount[type=number]").change(function (e) {
            var rate = {{ $__currency_rate }};
            
            if (rate > 0) {
                if ($(this).attr("name") == "amount") {
                    var amount = $("input[name=amount][type=number]").val();
                    var amount_rmb = 0;
                    if (isNumber(amount)) {
                        amount_rmb = Math.round(Math.ceil(amount * rate * 100)) / 100;
                    }
                    $("input#rmb-amount[type=number]").val(amount_rmb);
                } else {
                    var amount_rmb = $("input#rmb-amount[type=number]").val();
                    var amount = 0;
                    if (isNumber(amount_rmb)) {
                        amount = Math.round(Math.floor(amount_rmb / rate * 100)) / 100;
                    }
                    $("input[name=amount][type=number]").val(amount);
                }
            }
        });

        $("input[name=amount][type=number]").change();

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
        
        @if ($__fields['__step']['value'] == 'redirect')
            $("form#payment-form").submit();
        @endif
    });
    
</script>
@endsection