<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#1e1e23" />

    <title>{{ trans('system.company') }}</title>
    <link rel="shortcut icon" href="/assets/images/favicon.png">

    <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/default/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/plugin/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="/plugin/pace/pace_minimal.css">
    <link rel="stylesheet" href="/assets/css/globalpay_style.css?v6">
	@yield('style')
</head>
<body> 
    <div class="page-wrap public">
        <header class="main-header">
            <a href="{{ url('/') }}"><img src="/assets/images/gpay.png" alt="@lang('system.company')"  class="brand"></a>
            
            <div class="menu-toggle">
                <span></span>
            </div>
        </header>

        <div class="menu">
            <ul>
                <li class="dropdown">
                    <a href="javascript:void" data-toggle="dropdown">{{trans('system.language')}} <b class="caret"></b></a>
                    <ul class="dropdown-menu language">
                        <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
                        <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
                    </ul>
                </li>
                @if (!isset($___login) || $___login)
                <li><a href="{{ url('/login') }}">{{ trans('general.menu.public.login') }}</a></li>
                @endif
            </ul>
        </div>

		<div class="content">
            @yield('content')
        </div>

    </div>

    <!-- JavaScripts -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
    <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/plugin/pace/pace.min.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/app.global-pay.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
	<script>
        autoLogout('login', {{ config('session.lifetime') }});
        $(function() {
            setLocale('{{ url("setlocale") }}');
        });
	</script>
	@yield('script')
    @include('partials.general-script') 
</body>
</html>
