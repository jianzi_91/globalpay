@if (isset($__search_fields))
<a id="search-form-toggle" role="button" data-toggle="collapse" href="#search-form" aria-expanded="true" aria-controls="search-form">@lang("general.button.toggle_search_form")</a>

<form id='search-form' class="form-horizontal collapse in" role="form" method="GET">
    @foreach ($__search_fields as $_name=>$_field)
        @if (isset($_field['type']) && $_field['type'] == 'date_range')
        <div class="form-group">
            <label class="col-md-2 control-label">{{ $_field['label'] }}</label>
            <div class="col-md-6">
                <label class="radio-inline">
                    <input type="radio" name="radio_{{ $_name }}" onclick="inputTodayDate('{{ $_name }}')">{{ trans("general.search_field.field.today") }}
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radio_{{ $_name }}" checked onclick="toggleDateInput('{{ $_name }}', false)">{{ trans("general.search_field.field.all_times") }}
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radio_{{ $_name }}" onclick="toggleDateInput('{{ $_name }}', true)">{{ trans("general.search_field.field.other") }}
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-2">
                <div class="input-group">
                    <div class="input-group-addon">{{ $_field['date_from']['label'] }}</div>
                    <input type="text" placeholder="{{ $_field['date_from']['label'] }}" class="form-control datepicker" name="{{ $_name }}_from" value="{{ $_field['date_from']['value'] }}" disabled>
                    <div class="input-group-addon">{{ $_field['date_to']['label'] }}</div>
                    <input type="text" placeholder="{{ $_field['date_to']['label'] }}" class="form-control datepicker" name="{{ $_name }}_to" value="{{ $_field['date_to']['value'] }}" disabled>
                </div>
            </div>
        </div>
        @else
        <div class="form-group">
            <label class="col-md-2 control-label">{{ $_field['label'] }}</label>
            <div class="col-md-6">
                @if (isset($_field['options']) && is_array($_field['options']))
                    @if (is_array($_field['value']))
                        @foreach ($_field['options'] as $_value=>$_label)
                            <div class="checkbox">
                                <label><input type="checkbox" name="{{ $_name }}[]" value="{{ $_value }}" {{in_array($_value, $_field['value'])?'checked':'' }}>{{ $_label }}</label>
                            </div>
                        @endforeach
                    @else
                        <select class="form-control"  name="{{ $_name }}">
                            @foreach ($_field['options'] as $_value=>$_label)
                                <option value='{{ $_value }}' {{$_value == $_field['value']?'selected':'' }}>{{ $_label }}</option>
                            @endforeach
                        </select>
                    @endif
                @elseif (isset($_field['type']) && $_field['type']=='date')
                    <input type="text" placeholder="{{ $_field['label'] }}" class="form-control datepicker" name='{{ $_name }}' value="{{ $_field['value'] }}">
                @else
                    <input type="text" placeholder="{{ $_field['label'] }}" class="form-control" name='{{ $_name }}' value="{{ $_field['value'] }}">
                @endif
            </div>
        </div>
        @endif
    @endforeach

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
        </div>
    </div>
</form>
<hr/>
@endif

<script>
    function toggleDateInput(id, sw) {
        $('input[name="'+id+'_from"], input[name="'+id+'_to"]').prop('readonly', '');
        if (sw === true)
            $('input[name="'+id+'_from"], input[name="'+id+'_to"]').prop('disabled', '');
        else
            $('input[name="'+id+'_from"], input[name="'+id+'_to"]').prop('disabled', 'disabled');
    }

    function inputTodayDate(id) {
        $('input[name="'+id+'_from"], input[name="'+id+'_to"]').prop('disabled', '').prop('readonly', 'readonly');
        $('input[name="'+id+'_from"], input[name="'+id+'_to"]').val(moment().format("YYYY-MM-DD"));
    }
</script>