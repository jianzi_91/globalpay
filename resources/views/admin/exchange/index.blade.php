@extends('admin.layouts.app')
@section('title', trans("general.page.admin.exchange.bar-title.exchange"))

@section('content')
    <div class="container">
        @include('admin.common.success')
        @include('admin.common.errors')
        <h1>{{ trans("general.page.admin.exchange.bar-title.exchange") }}</h1>
        <hr>
        <div>
            {{ $exchange->total() }} {{ trans('general.table.listing.record') }}
            <a href="{{ url(config('app.admin_slug').'/exchange/create') }}" class="btn btn-primary btn-xs">{{ trans('general.button.add-new') }}</a>
        </div>

        <br>

        <!-- Rates -->

        <table class="table table-striped">
            <thead>
            <tr>
                <th>{{ trans("field.exchange.currency") }}</th>
                <th>{{ trans("field.exchange.buy_rate") }}</th>
                <th>{{ trans("field.exchange.sell_rate") }}</th>
                <th>{{ trans("field.exchange.min_withdrawal_amount") }}</th>
                <th>{{ trans("field.exchange.status") }}</th>
                <th>{{ trans("field.exchange.updated_at") }}</th>
                <th>{{ trans("general.page.admin.exchange.content.last_update") }}</th>
                <th>{{ trans("general.table.header.action") }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($exchange as $item)
                <tr>
                    <td>
                        {{ $item->currency->code }}<br>
                        <small>{{ $item->currency->name }}</small>
                    </td>
                    <td><code>{{ $item->buy_rate }}</code></td>
                    <td><code>{{ $item->sell_rate }}</code></td>
                    <td><code>{{ $item->min_withdrawal_amount }}</code></td>
                    <td>{!! $item->status == 1 ? "<span class='label label-primary'>".trans('general.exchange.status.1')."</span>" : "<span class='label label-default'>".trans('general.exchange.status.0')."</span>" !!}</td>
                    <td>{{ $item->admin->name }}</td>
                    <td>{{ $item->updated_at }}</td>
                    <td>
                        <a href="{{ url(config('app.admin_slug').'/exchange/'.$item->id.'/edit') }}" class="btn btn-xs btn-default">
                            <i class="fa fa-pencil"></i> {{ trans('general.button.edit') }}
                        </a>
                        @if($item->status == 0)
                        <button class="btn btn-xs btn-danger" data-toggle="modal" data-target=".bs-delete-rate"
                            data-id="{{ $item->id }}" data-currency-name="{{ $item->currency->name }} ({{ $item->currency->code }})"
                            ><i class="fa fa-trash-o"></i> {{ trans("general.button.delete") }}</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($exchange) == 0)
            <div class="text-center text-muted">No Data</div>
        @endif
        <!-- end Rates -->


        <!-- Delete rates model -->
        <div class="modal fade bs-delete-rate" tabindex="-1" role="dialog" aria-labelledby="deleteRateModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ trans("general.page.admin.exchange.bar-title.delete") }}</h4>
                    </div>
                    <form class="form-horizontal" id="delete-form" action="" method="post">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <div class="modal-body text-center">
                            <p>{{ trans("general.page.admin.exchange.content.delete") }}</p>
                            <div class=" alert alert-warning">
                                <h4 id="delete-currency"></h4>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans("general.button.cancel") }}</button>
                            <button type="submit" class="btn btn-danger">{{ trans("general.button.confirm") }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end Delete rates model -->
    </div>
@endsection

@section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>

    <script>
        $(document).ready(function () {

            $('.footable').footable();
            $('.datepicker[type=text]').datepicker({
                dateFormat: 'yy-mm-dd',
            });

            $('.bs-delete-rate').on('shown.bs.modal', function (e) {
                var id = $(e.relatedTarget).data('id');
                var action = "{{ url(config('app.admin_slug').'/exchange') }}" + "/" + id ;

                $('#delete-form').attr('action', action);
                $('#delete-currency').html($(e.relatedTarget).data('currency-name'));
            })

        });
    </script>
@endsection