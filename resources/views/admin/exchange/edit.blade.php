@extends('admin.layouts.app')
@section('title', 'News')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Exchange</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('admin.common.errors')
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Exchange: {{ $exchange->currency->code }}</div>
                    <div class="panel-body">

                        <form class="form-horizontal" action="{{ url(config('app.admin_slug').'/exchange', $exchange->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="status">Status</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="status" name="status">
                                        <option value="1" {{ $exchange->status == 1 ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ $exchange->status == 0 ? 'selected' : '' }}>Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="curency">Currency</label>
                                <div class="col-md-7">
                                    <label class="control-label">{{ $exchange->currency->name }} ({{ $exchange->currency->code }})</label>
                                    {{--<select class="form-control" id="currency" name="currency">--}}
                                        {{--@foreach($currencies as $currency)--}}
                                            {{--<option value="{{ $currency->id }}" {{ $currency->id == $exchange->currency_id ? 'selected' : '' }}>{{ $currency->name }} ({{ $currency->code }})</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="buy_rate">Buy Rate</label>
                                <div class="col-md-7">
                                    <input type="text" id="buy_rate" name="buy_rate" class="form-control" value="{{ $exchange->buy_rate }}" placeholder="Buy Rate">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="sell_rate">Sell Rate</label>
                                <div class="col-md-7">
                                    <input type="text" id="sell_rate" name="sell_rate" class="form-control" value="{{ $exchange->sell_rate }}" placeholder="Sell Rate">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="min_withdrawal_amount">Minimum Withdrawal Amount</label>
                                <div class="col-md-7">
                                    <input type="text" id="min_withdrawal_amount" name="min_withdrawal_amount" class="form-control" value="{{ $exchange->min_withdrawal_amount }}" placeholder="Minimum Withdrawal Amount">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">{{ trans('general.button.edit') }}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection