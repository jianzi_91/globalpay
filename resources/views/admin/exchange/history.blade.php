@extends('admin.layouts.app')
@section('title', trans("general.page.admin.exchange-history.bar-title.exchange-history"))

@section('content')
    <div class="container">
        @include('admin.common.success')
        @include('admin.common.errors')
        <h1>{{ trans("general.page.admin.exchange-history.bar-title.exchange-history") }}</h1>
        <hr>
        <div>
            {{ $histories->total() }} {{ trans('general.table.listing.record') }}
        </div>

        <br>

        <!-- Rates -->

        <table class="table table-striped">
            <thead>
            <tr>
                <th>{{ trans("field.exchange.currency") }}</th>
                <th>{{ trans("field.exchange.buy_rate") }}</th>
                <th>{{ trans("field.exchange.sell_rate") }}</th>
                <th>{{ trans("field.exchange.min_withdrawal_amount") }}</th>
                <th>{{ trans("field.exchange.status") }}</th>
                <th>{{ trans("general.page.admin.exchange-history.content.updated_by") }}</th>
                <th>{{ trans("general.page.admin.exchange-history.content.activity") }}</th>
                <th>{{ trans("field.exchange.updated_at") }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($histories as $history)
                <tr>
                    <td>
                        {{ $history->currency->code }}<br>
                        <small>{{ $history->currency->name }}</small>
                    </td>
                    <td><code>{{ $history->buy_rate }}</code></td>
                    <td><code>{{ $history->sell_rate }}</code></td>
                    <td><code>{{ $history->min_withdrawal_amount }}</code></td>
                    <td>{!! $history->status == 1 ? "<span class='label label-primary'>". trans("general.exchange.status.1") . "</span>" : "<span class='label label-default'>". trans("general.exchange.status.0") . "</span>" !!}</td>
                    <td>{{ $history->admin->name }}</td>
                    <td>{{ $history->action }}</td>
                    <td>{{ $history->updated_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $histories->links() }}
        @if(count($histories) == 0)
            <div class="text-center text-muted">{{ trans("general.page.admin.exchange-history.content.no_data") }}</div>
        @endif
        <!-- end Rates -->
    </div>
@endsection

@section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>

    <script>
        $(document).ready(function () {

            $('.footable').footable();
            $('.datepicker[type=text]').datepicker({
                dateFormat: 'yy-mm-dd',
            });
        });
    </script>
@endsection