@extends('admin.layouts.app')
@section('title', "General Setting")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>General Setting</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/settings') }}">General Setting</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    General Setting
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" method="POST">
						{{ csrf_field() }}
                        <h3 class="text-center">General</h3>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.setting.bonus_swallet_percent', ['wallet' => trans('general.wallet.swallet')]) }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.setting.bonus_swallet_percent', ['wallet' => trans('general.wallet.swallet')]) }}" class="form-control" name='bonus_swallet_percent' value="{{old('bonus_swallet_percent', $settings->bonus_swallet_percent) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.setting.unfreeze_wallet_percent', ['wallet' => trans('general.wallet.awallet2')]) }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.setting.unfreeze_wallet_percent', ['wallet' => trans('general.wallet.awallet2')]) }}" class="form-control" name='unfreeze_wallet_percent' value="{{old('unfreeze_wallet_percent', $settings->unfreeze_wallet_percent)}}">
                            </div>
                        </div>
                        <hr/>
                        <h3 class="text-center">Trade Market</h3>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.trade_setting.qty_per_price') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade_setting.qty_per_price') }}" class="form-control" name='qty_per_price' value="{{old('qty_per_price', $trade_settings->qty_per_price) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.trade_setting.price_change') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade_setting.price_change') }}" class="form-control" name='price_change' value="{{old('price_change', $trade_settings->price_change) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.trade_setting.platform_charge') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade_setting.platform_charge') }}" class="form-control" name='platform_charge' value="{{old('platform_charge', $trade_settings->platform_charge) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.trade_setting.charity_percent') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade_setting.charity_percent') }}" class="form-control" name='charity_percent' value="{{old('charity_percent', $trade_settings->charity_percent) }}">
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection