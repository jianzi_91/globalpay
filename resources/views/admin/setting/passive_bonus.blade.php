@extends('admin.layouts.app')
@section('title', "Passive Bonus Conditions")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Passive Bonus Conditions</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/settings/passive_bonus') }}">Passive Bonus Conditions</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                Passive Bonus Conditions
                </div>

                <div class="panel panel-body">

                    <form class="form-horizontal" role="form" method="POST" id="form-passive-bonus">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.comm_condition.amt_min') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.comm_condition.amt_min') }}" class="form-control" name='amt_min' value="{{old('amt_min') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.comm_condition.amt_max') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.comm_condition.amt_max') }}" class="form-control" name='amt_max' value="{{old('amt_max') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.comm_condition.percent') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.comm_condition.percent') }}" class="form-control" name='percent' value="{{old('percent') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button type="submit" class="btn btn-sm btn-primary" id="btn-passive-bonus">{{ trans("general.button.add-new") }}</button>
                            </div>
                        </div>  
                    </form>
                    
                    <hr/>
                    
                    <button class="btn btn-sm btn-primary" onclick="create()">{{ trans('general.button.add-new') }}</button>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" style="border:1px solid #ddd;">
                            <thead>
                                <tr>
                                    <th>{{ trans("general.table.header.action") }}</th>
                                    <th>{{ trans("field.comm_condition.amt_min") }}</th>
                                    <th>{{ trans("field.comm_condition.amt_max") }}</th>
                                    <th>{{ trans("field.comm_condition.percent") }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($passive_conditions as $condition)
                                <tr>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-white btn-sm" data="{{ json_encode($condition->toArray()) }}"
                                        onclick="edit(this)"><i class="fa fa-pencil"></i> {{ trans("general.button.edit") }}</a>
                                        <a href="javascript:void(0);" class="btn btn-white btn-sm" data="{{ json_encode($condition->toArray()) }}"
                                        onclick="remove(this)"><i class="fa fa-trash-o"></i> {{ trans("general.button.delete") }}</a>
                                    </td>
                                    <td class="text-right">{{ number_format($condition->amt_min, 8) }}</td>
                                    <td class="text-right">{{ number_format($condition->amt_max, 8) }}</td>
                                    <td class="text-right">{{ number_format($condition->percent, 3) }} %</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });

    var form = $("#form-passive-bonus");
    var btn = $("#btn-passive-bonus");

    function create()
    {
        form.find("input[name!=_token]").val("");
        btn.html("{{ trans('general.button.add-new') }}");
        $('html, body').animate({
            scrollTop: form.offset().top
        }, 100);
    }

    function edit(obj) 
    {
        var data = JSON.parse($(obj).attr("data"));
        btn.html("{{ trans('general.button.edit') }}");
        $("input[type=hidden][name=id]").val(data.id);
        $("input[type=text][name=amt_min]").val(data.amt_min);
        $("input[type=text][name=amt_max]").val(data.amt_max);
        $("input[type=text][name=percent]").val(data.percent);
        $('html, body').animate({
            scrollTop: form.offset().top
        }, 100);

    }

    function remove(obj)
    {
        var data = JSON.parse($(obj).attr("data"));
        bootbox.confirm({
            title: "Delete Passive Bonus Condition",
            message: '<div class="text-center">Your have selected (' + data.amt_min + ' - ' + data.amt_max + ') condition to delete. Do you want to proceed?</div>',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> {{ trans("general.button.no") }}'
                },
                confirm: {
                    className: "btn-primary",
                    label: '<i class="fa fa-check"></i> {{ trans("general.button.yes") }}'
                }
            },
            callback: function (result) {
                $('.bootbox-confirm').prop('disabled', true);
                var executed = false;
                if (result === true && executed === false) {
                    executed = true;
                    $('<form method="POST" action="{{ url(config("app.admin_slug")."/settings/passive_bonus/delete") }}/' + data.id + '">'+
                    '{{ csrf_field() }}'+
                    '{{ method_field("DELETE") }}'+
                    '</form>').appendTo('body').submit();
                }
            }
        });
    }
</script>
@endsection