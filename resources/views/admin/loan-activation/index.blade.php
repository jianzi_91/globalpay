@extends('admin.layouts.app')
@section('title', trans("general.page.admin.loan-accounts-list.bar-title.loan-accounts-list"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.loan-accounts-list.bar-title.loan-accounts-list") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')
                    
                    @include('admin.partials.search-form')
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.activation.code') }}</th>
                                <th>{{ trans('field.user.rank') }}</th>
                                <th>{{ trans('field.user.crank') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.ref-username') }}</th>
                                <th>{{ trans('field.user.up-username') }}</th>
                                <th>{{ trans('field.user.up_pos') }}</th>
                                <th>{{ trans('general.wallet.dwallet') }}</th>
                                <th>{{ trans('field.activation.aid-username') }}</th>
                                <th>{{ trans('field.activation.loan_status') }}</th>
                                <th>{{ trans('field.activation.loan_settle_at') }}</th>
                                <th>{{ trans('field.user.created_at') }}</th>
                                <th>{{ trans('general.table.header.action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <!-- Distribute Loan Activation model -->
        <div class="modal fade bs-distribute-loan-activation" tabindex="-1" role="dialog" aria-labelledby="DistributeLoanActivationModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Distribute Loan Activation</h4>
                    </div>
                    <form class="form-horizontal" id="distribute-loan-activation-form" action="" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body text-center">
                            <p>Are you confirm want to distribute this loan activation?</p>
                            <div class="alert alert-warning">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.user.id") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="distribute-user-id"></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.activation.code-name") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="distribute-package-name"></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end Distribute Loan Activation model -->
    </div>
</div>
@endsection

@section('script')
<script>

$(document).ready( function () {

    var selected = [];
    var columns = [
        {data: 'id' },
        {data: 'act_code' },
        {data: 'rank' },
        {data: 'crank' },
        {data: 'username' },
        {data: 'name' },
        {data: 'ref', orderable: false },
        {data: 'up', orderable: false },
        {data: 'up_pos', orderable: false },
        {data: 'wallet_dwallet' },
        {data: 'act_aid', orderable: false },
        {data: 'act_loan_status' },
        {data: 'act_loan_settle_at' },
        {data: 'created_at' },
        {data: 'action', orderable:false },
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'tf'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/loan-activations/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            { className: "text-right", "targets": [9] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('.bs-distribute-loan-activation').on('shown.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var action = "{{ url(config('app.admin_slug').'/loan-activations/distribute-loan-activation') }}" + "/" + id ;

        $('#distribute-loan-activation-form').attr('action', action);
        $('#distribute-package-name').html($(e.relatedTarget).data('package-name'));
        $('#distribute-user-id').html($(e.relatedTarget).data('user-id'));
    });

    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
});
</script>
@endsection 