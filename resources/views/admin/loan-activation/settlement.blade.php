@extends('admin.layouts.app')
@section('title', "Early Settlement")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Early Settlement</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/loan-activations/settlement') }}">Early Settlement</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Early Settlement
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/loan-activations/settlement') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">To Username (Debt Wallet)</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="To Username (Debt Wallet)" class="form-control" name='to_username' value='{{ old('to_username') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='to_username'>{{ trans('general.button.check') }}</button>
                                <span id='to_username-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Pay By Username</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="Pay By Username (Register Wallet)" class="form-control" name='from_username' value='{{ old('from_username') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='from_username'>{{ trans('general.button.check') }}</button>
                                <span id='from_username-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Pay By Wallet</label>
                            <div class="col-md-6">
                                <select class="form-control" name='from_wallet'>
                                    <option value="">-- Please Select --</option>
                                    @foreach ($__field["from_wallet"]["options"] as $_wallet)
                                    <option value="{{ $_wallet }}" {{old("from_wallet") == $_wallet ? "selected": ""}}>{{ trans("general.wallet.".$_wallet) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Amount</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="Amount" class="form-control" name='amount' value='{{ old('amount') }}'>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.remarks') }}</label>
							<div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.remarks') }}" class="form-control" name='remarks' value='{{ old('remarks') }}'>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.aremarks') }}</label>
							<div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.aremarks') }}" class="form-control" name='aremarks' value='{{ old('aremarks') }}'>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //check username
        $( "button[for=to_username]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('wallet-dwallet/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        $( "button[for=from_username]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('wallet-settlement-pay-by/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
    });

    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
</script>
@endsection