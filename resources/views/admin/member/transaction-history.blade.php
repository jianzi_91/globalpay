@extends('admin.layouts.app')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
@include('member.common.success')
@include('member.common.errors')
<div class="container">
    <h2>@lang('field.user.member_transaction_history')</h2>
    <div class="panel panel-default">
        <div class="panel-body">
            <form id="search-form" method="GET">
                <div class="form-general form-search">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>@lang('field.user.name')</label>
                            <input name="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>@lang('field.user.mobile')</label>
                            <input name="mobile" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label>@lang('general.search_field.field.transcode')</label>
                        <div class="transaction-method">
                            @foreach ($transcode_list as $key => $value)
                            <label>
                                <input type="checkbox" name="transcode[]" value="{{$key}}" {{ old('transcode', $key) ? 'checked' : '' }}>{{ $value }}
                            </label>
                            @endforeach
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group float-label empty">
                            <label>@lang('general.search_field.field.start_date')</label>
                            <input id="from" name="from" class="form-control datepicker" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group float-label empty">
                            <label>@lang('general.search_field.field.end_date')</label>
                            <input id="to" name="to" class="form-control datepicker" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button id="btn_search" type="submit" class="btn btn-primary btn-search pull-right">@lang('general.button.search')</button>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>

            <div class="table-responsive">
                <table id="wallet-history" class="table table-primary">
                    <thead>
                        <tr>
                            <th>@lang('field.user.created_at')</th>
                            <th>@lang('field.user.name')</th>
                            <th>@lang('field.wallet_record.descr')</th>
                            <th>@lang('general.wallet.awallet')</th>
                            <th>@lang('general.wallet.fwallet')</th>
                            <th>@lang('general.wallet.dowallet')</th>
                            <th>@lang('general.wallet.scwallet')</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
$(function() {
    $('#from').datepicker();
    $('#to').datepicker();
    // datatable
    var columns = [
        {data:'created_at',width:'60px'},
        {data:'name',width:'60px',orderable:false},
        {data:'description',orderable:false}, 
        {data:'awallet',width:'80px',className:"text-right",orderable:false},
        {data:'fwallet',width:'80px',className:"text-right",orderable:false},
        {data:'dowallet',width:'80px',className:"text-right",orderable:false},
        {data:'sc_wallet',width:'80px',className:"text-right",orderable:false},
    ];

    var oTable = $('#wallet-history').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/members/transaction-history/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                            d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                bootbox.alert({
                    title: "@lang('one.error')",
                    message: "@lang('admin.server_error')"
                });
            }
        },
        "columns": columns,
        "language": {
            buttons: {
                colvis: "@lang('general.button.column_visibility')",
                colvisRestore: "@lang('general.button.restore_visibility')"
            },
            url: "@lang('system.lang-datatables')"
        },
        "buttons": [{
            extend: 'colvis',
            postfixButtons: [ 'colvisRestore' ]
        }],
        "ordering": true,
        "order": [ [0,'desc'] ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
        "responsive": true,
    });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });

    $('#btn_search').click();
});
//  $(document).ready(function() {

//     $('.footable').footable();
//     $('.datepicker[type=text]').datepicker({
//         dateFormat: 'yy-mm-dd',
//     });

//     $('#wallet-history').DataTable();


// });
</script>
@endsection