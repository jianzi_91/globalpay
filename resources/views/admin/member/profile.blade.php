@extends('admin.layouts.app')
@section('title', 'Edit Member')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Edit Member</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members') }}">Member List</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/profile', $member->id) }}">Edit Member</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    General Info
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/profile', $member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $member->username }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.user.name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.name') }}" class="form-control" name='name' value='{{empty(old('name'))?$member->name:old('name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.user.nid') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.nid') }}" class="form-control" name='nid' value='{{empty(old('nid'))?$member->nid:old('nid')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.email') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.email') }}" class="form-control" name='email' value='{{empty(old('email'))?$member->email:old('email')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.mobile') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='mobile' value='{{empty(old('mobile'))?$member->mobile:old('mobile')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address1') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address1') }}" class="form-control" name='address1' value='{{empty(old('address1'))?$member->address1:old('address1')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address2') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address2') }}" class="form-control" name='address2' value='{{empty(old('address2'))?$member->address2:old('address2')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.city') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.city') }}" class="form-control" name='city' value='{{empty(old('city'))?$member->city:old('city')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.zip') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.zip') }}" class="form-control" name='zip' value='{{empty(old('zip'))?$member->zip:old('zip')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.state') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.state') }}" class="form-control" name='state' value='{{empty(old('state'))?$member->state:old('state')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.country') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="country">
                                @foreach($__fields['country']['options'] AS $_code=>$_country)
                                    <option value='{{ $_code }}' {{((empty(old('country'))?$member->country:old('country')) == $_code ) ?'selected':''}}>{{ $_country }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.member_status.alliance_status') }}</label>
							<div class="col-md-6">
								<div class="i-checks">
									<label> 
										<input type="checkbox" value="10" name="alliance_status" {{(old('alliance_status', $member->memberStatus->alliance_status) == '10') ? 'checked':''}} > <i></i> {{ trans('general.member_status.alliance_status.10') }} 
									</label>
								</div>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.user.status') }}</label>
							<div class="col-md-6">
								<div class="i-checks">
									<label> 
										<input type="radio" value="10" name="status" {{(empty(old('status')) && $member->status == '10') || old('status')=='10'?'checked':''}} > <i></i> {{ trans('general.user.status.10') }} 
									</label>
								</div>
								<div class="i-checks">
									<label> 
										<input type="radio" value="30" name="status" {{(empty(old('status')) && $member->status == '30') || old('status')=='30'?'checked':''}} > <i></i> {{ trans('general.user.status.30') }} 
									</label>
								</div>
								<div class="i-checks">
									<label> <input type="radio" value="90" name="status" {{(empty(old('status')) && $member->status == '90') || old('status')=='90'?'checked':''}}> <i></i> {{ trans('general.user.status.90') }} 
									</label>
								</div>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.user.user_type') }}</label>
                            <div class="col-md-6">
                                <div class="i-checks">
                                    <label> 
                                        <input type="radio" value="1" name="user_type" {{(empty(old('user_type')) && $member->user_type == '1') || old('user_type')=='1'?'checked':''}} > <i></i> {{ trans('general.user.user_type.1') }} 
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label> 
                                        <input type="radio" value="2" name="user_type" {{(empty(old('user_type')) && $member->user_type == '2') || old('user_type')=='2'?'checked':''}} > <i></i> {{ trans('general.user.user_type.2') }} 
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="radio" value="3" name="user_type" {{(empty(old('user_type')) && $member->user_type == '3') || old('user_type')=='3'?'checked':''}}> <i></i> {{ trans('general.user.user_type.3') }} 
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
        
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Beneficiary Information
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/profile', $member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='5'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.beneficiary_name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.beneficiary_name') }}" class="form-control" name='beneficiary_name' value='{{empty(old('beneficiary_name'))?$member->beneficiary_name:old('beneficiary_name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.beneficiary_nid') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.beneficiary_nid') }}" class="form-control" name='beneficiary_nid' value='{{empty(old('beneficiary_nid'))?$member->beneficiary_nid:old('beneficiary_nid')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.beneficiary_user_relationship') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.beneficiary_user_relationship') }}" class="form-control" name='beneficiary_user_relationship' value='{{empty(old('beneficiary_user_relationship'))?$member->beneficiary_user_relationship:old('beneficiary_user_relationship')}}'>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Bank Information
                </div>
                
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.bank_country') }}</td>
                            <td>{{ isset($__fields['country']['options'][$member->country])?$__fields['country']['options'][$member->country]:'' }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.bank_payee_name') }}</td>
                            <td>{{ $member->name }}</td>
                        </tr>
                    </table>

                    <hr/>
                    
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/profile', $member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='4'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.bank_name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.bank_name') }}" class="form-control" name='bank_name' value='{{empty(old('bank_name'))?$member->bank_name:old('bank_name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.bank_branch_name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.bank_branch_name') }}" class="form-control" name='bank_branch_name' value='{{empty(old('bank_branch_name'))?$member->bank_branch_name:old('bank_branch_name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.bank_acc_no') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.bank_acc_no') }}" class="form-control" name='bank_acc_no' value='{{empty(old('bank_acc_no'))?$member->bank_acc_no:old('bank_acc_no')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.bank_sorting_code') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.bank_sorting_code') }}" class="form-control" name='bank_sorting_code' value='{{empty(old('bank_sorting_code'))?$member->bank_sorting_code:old('bank_sorting_code')}}'>
                                <span class="help-block m-b-none">{{ trans('general.page.user.profile.content.bank_sorting_code-helper') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.bank_iban') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.bank_iban') }}" class="form-control" name='bank_iban' value='{{empty(old('bank_iban'))?$member->bank_iban:old('bank_iban')}}'>
                                <span class="help-block m-b-none">{{ trans('general.page.user.profile.content.bank_iban-helper') }}</span>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection