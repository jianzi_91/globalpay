@extends('admin.layouts.app')
@section('title', trans('general.page.admin.member-list.bar-title.member-list'))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.member-list.bar-title.member-list')</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')
                    
                    @if (Auth::guard('admins')->user()->can("admin-privilege", "members/create"))
                    <a href="{{ url(config('app.admin_slug').'/members/create') }}" class="btn btn-primary btn-xs">{{ trans('general.button.add-new') }}</a>
                    @endif
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.itype') }}</th>
                                <th>{{ trans('field.user.iid') }}</th>
                                <th>{{ trans('field.user.nid') }}</th>
                                <th>{{ trans('field.user.mobile') }}</th>
                                <th>{{ trans('field.user.email') }}</th>
                                <th>{{ trans('field.user.up') }}</th>
                                <th>{{ trans('field.user.address-info') }}</th>
                                <th>{{ trans('field.user.country') }}</th>
                                <th>{{ trans('field.user.bank_country') }}</th>
                                <th>{{ trans('field.user.status') }}</th>
                                <th>{{ trans('field.user.created_at') }}</th>
                                <th>{{ trans('general.table.header.action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'id'},
        {data: 'username' }, 
        {data: 'name' }, 
        {data: 'itype', orderable: false }, 
        {data: 'iid-text', orderable: false },
        {data: 'nid' }, 
        {data: 'mobile' },  
        {data: 'email' }, 
        {data: 'network-info', orderable: false }, 
        {data: 'address-info', orderable: false }, 
        {data: 'country', orderable: false }, 
        {data: 'country', orderable: false }, 
        {data: 'status' },
        {data: 'created_at' },
        {data: 'action', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/members/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
        {
            extend: 'excel',
            exportOptions: {
                columns: ":visible thead th:not(.noExport)",
            },
        },     
        {
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [{
            visible: false
        }],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 