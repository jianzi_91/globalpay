@extends('admin.layouts.app')
@section('title', 'Edit Sub Member')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Edit Sub Member</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members') }}">Member List</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/profile-sub-account', $member->id) }}">Edit Sub Member</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    General Info
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/profile-sub-account', $member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
							<div class="col-md-6">
                                <p class="form-control-static">{{ $member->username }}</p>
							</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.rank') }}</label>
                            <div class="col-md-6">
                                {{ trans('general.user.rank.title.'.$member->rank) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.crank') }}</label>
							<div class="col-md-6">
                                <select class="form-control" name="crank">
                                @foreach($__fields['rank']['options'] AS $_code=>$_rank)
                                    <option value='{{ $_rank }}' {{((empty(old('crank'))?$member->crank:old('crank')) == $_rank ) ?'selected':''}}>{{ trans('general.user.rank.title.'.$_rank) }}</option>
                                @endforeach
                                </select>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //phone code helper
        $("select[name=phone_code]").on("change", function(){
            var val = $($(this).find('option[value='+$(this).val()+']')).attr('phone-code');
            $("input[name=mobile][type=text]").val(val);
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection