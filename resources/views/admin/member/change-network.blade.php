@extends('admin.layouts.app')
@section('title', 'Change Network')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Change Network</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/network-list') }}">Network Listing</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/change-network', $__member->id) }}">Change Network</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Change Network
                </div>

                <div class="panel panel-body">
                
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__member->username }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.name') }}</td>
                            <td>{{ $__member->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.nid') }}</td>
                            <td>{{ $__member->nid }}</td>
                        </tr>
                    </table>

                    <hr/>

                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/change-network', $__member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                      
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.ref') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.ref') }}" class="form-control" name='ref' value='{{ empty(old('ref'))?$__member["ref-username"]:old('ref') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='ref'>{{ trans('general.button.check') }}</button>
                                <span id='ref-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.up') }}" class="form-control" name='up' value='{{ empty(old('up'))?$__member["up-username"]:old('up') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='up'>{{ trans('general.button.check') }}</button>
                                <span id='up-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up_pos') }}</label>
							<div class="col-md-6">
								<div class="i-checks">
									<label> 
										<input type="radio" value="1" name="up_pos" {{ (empty(old('up_pos'))?$__member->up_pos:old('up_pos'))=='1'?'checked':''}} > <i></i> {{ trans('general.user.up_pos.1') }} 
									</label>
								</div>
								<div class="i-checks">
									<label>
										<input type="radio" value="2" name="up_pos" {{ (empty(old('up_pos'))?$__member->up_pos:old('up_pos'))=='2'?'checked':''}}> 	<i></i> {{ trans('general.user.up_pos.2') }}
									</label>
								</div>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=ref]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        // check placement
        $( "button[for=up]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('placement/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection