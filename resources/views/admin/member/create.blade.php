@extends('admin.layouts.app')
@section('title', 'Add Member')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Add Member</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li>
					<a href="{{ url(config('app.admin_slug').'/members') }}">Member List</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/create') }}">Add Member</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Add Member
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/create') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.user.name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.name') }}" class="form-control" name='name' value='{{old('name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nid" class="col-md-4 control-label">{{ trans('field.user.nid') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.nid') }}" class="form-control" name='nid' value='{{old('nid')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.email') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.email') }}" class="form-control" name='email' value='{{old('email')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nid" class="col-md-4 control-label">{{ trans('field.user.mobile') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='mobile' value='{{old('mobile')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address1') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address1') }}" class="form-control" name='address1' value='{{ old('address1') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address2') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address2') }}" class="form-control" name='address2' value='{{ old('address2') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.city') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.city') }}" class="form-control" name='city' value='{{ old('city') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.zip') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.zip') }}" class="form-control" name='zip' value='{{ old('zip') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.state') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.state') }}" class="form-control" name='state' value='{{ old('state') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.country') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="country">
                                @foreach($__fields['country']['options'] AS $_code=>$_country)
                                    <option value='{{ $_code }}' {{(old('country') == $_code ) ?'selected':''}}>{{ $_country }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.up') }}" class="form-control" name='up' value='{{old('up')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='up'>{{ trans('general.button.check') }}</button>
                                <span id='up-text'></span>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up_pos') }}</label>
							<div class="col-md-6">
								<div class="i-checks">
									<label> 
										<input type="radio" value="1" name="up_pos" {{ empty(old('up_pos')) || old('up_pos')=='1'?'checked  ':''}} > <i></i> {{ trans('general.user.up_pos.1') }} 
									</label>
								</div>
								<div class="i-checks">
									<label>
										<input type="radio" value="2" name="up_pos" {{ old('up_pos')=='2'?'checked':''}}> 	<i></i> {{ trans('general.user.up_pos.2') }}  
									</label>
								</div>
							</div>
                        </div> --}}
                        <input type="hidden" value="1" name="up_pos">
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password') }}" class="form-control" name='password' value='{{ old("password") }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password_confirmation') }}" class="form-control" name='password_confirmation' value='{{ old("password_confirmation") }}'>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2') }}" class="form-control" name='password2' value='{{ old("password2") }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2_confirmation') }}" class="form-control" name='password2_confirmation' value='{{ old("password2_confirmation") }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=up]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection