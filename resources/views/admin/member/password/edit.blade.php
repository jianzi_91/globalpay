@extends('admin.layouts.app')
@section('title', 'EEdit Member Passwords')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Edit Member Passwords</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/passwords') }}">Member Passwords Listing</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/passwords/edit', $member->id) }}">Edit Member Passwords</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    User Info
                </div>
                
                <div class="panel panel-body">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>{{ trans('field.user.id') }}</td>
                            <td>{{ $member->id }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $member->username }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.name') }}</td>
                            <td>{{ $member->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.nid') }}</td>
                            <td>{{ $member->nid }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td>
                            <td>{{ $member->email }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.mobile') }}</td>
                            <td>{{ $member->mobile }}</td>
                        </tr>
                    </table>
				</div>
			</div>
		</div>
    </div>
		
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Password
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/passwords/edit', $member->id) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field("PUT") }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password') }}" class="form-control" name='password' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password_confirmation') }}" class="form-control" name='password_confirmation' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
		
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Security Password
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/passwords/edit', $member->id) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field("PUT") }}
						<input type='hidden' name='__req' value='2'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2') }}" class="form-control" name='password2' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2_confirmation') }}" class="form-control" name='password2_confirmation' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection