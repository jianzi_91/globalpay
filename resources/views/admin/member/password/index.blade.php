@extends('admin.layouts.app')
@section('title', trans('general.page.admin.member-password-list.bar-title.member-password-list'))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.member-password-list.bar-title.member-password-list')</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.nid') }}</th>
                                <th>{{ trans('field.user.country') }}</th>
                                <th>{{ trans('field.user.email') }}</th>
                                <th>{{ trans('field.user.mobile') }}</th>
                                @if (auth("admins")->user()->can("admin-privilege", "members/view-passwords"))
                                <th>{{ trans('field.user.password') }}</th>
                                <th>{{ trans('field.user.password2') }}</th>
                                @endif
                                <th>{{ trans('field.user.status') }}</th>
                                <th>{{ trans('general.table.header.date') }}</th>
                                <th>{{ trans('general.table.header.action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'id'},
        {data: 'username' }, 
        {data: 'name' }, 
        {data: 'nid' }, 
        {data: 'country', orderable: false }, 
        {data: 'email' }, 
        {data: 'mobile' }, 
        @if (auth("admins")->user()->can("admin-privilege", "members/view-passwords"))
            {data: 'password_ori', orderable: false }, 
            {data: 'password2_ori', orderable: false }, 
        @endif
        {data: 'status' },
        {data: 'created_at' },
        {data: 'action', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/members/passwords/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            {
                visible: false
            }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 