@extends('admin.layouts.app')
@section('title', 'Change Franchise Network')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Change Franchise Network</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/franchise-list') }}">Franchise Listing</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/members/change-franchise', $__member->id) }}">Change Franchise Network</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <thead>
                            <tr>
                                <th>{{ trans("general.table.header.type") }}</th>
                                <th>{{ trans("field.user.username") }}</th>
                                <th>{{ trans("field.user.rank-bonus") }}</th>
                            </tr>
                        </thead>
                        @if(count($__franchise_user_list["main"])>0)
                            <?php $count=0; ?>
                            @foreach($__franchise_user_list["main"] as $_id=>$_user)
                            <tr>
                                <?php $count++; ?>
                                @if($count===1)
                                <td rowspan={{ count($__franchise_user_list["main"]) }}>{{ trans("general.page.user.wallet-internal-transfer.content.franchise-main-account") }}</td>
                                @endif

                                <td>{{ $_user["username"] }}</td>
                                <td>{{ trans("general.user.rank.title.".max($_user["rank"], $_user["crank"])) }}</td>
                            </tr>
                            @endforeach
                        @endif

                        @if(count($__franchise_user_list["franchise"])>0)
                            <?php $count=0; ?>
                            @foreach($__franchise_user_list["franchise"] as $_id=>$_user)
                            <tr>
                                <?php $count++; ?>
                                @if($count===1)
                                <td rowspan={{ count($__franchise_user_list["franchise"]) }}>{{ trans("general.page.user.wallet-internal-transfer.content.franchise-account") }}</td>
                                @endif

                                <td>{{ $_user["username"] }}</td>
                                <td>{{ trans("general.user.rank.title.".max($_user["rank"], $_user["crank"])) }}</td>
                            </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
		</div>
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Change Franchise Network
                </div>

                <div class="panel panel-body">
                
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__member->username }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.name') }}</td>
                            <td>{{ $__member->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.nid') }}</td>
                            <td>{{ $__member->nid }}</td>
                        </tr>
                    </table>

                    <hr/>

                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/members/change-franchise', $__member->id) }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                      
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.franchise_user_id') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.franchise_user_id') }}" class="form-control" name='franchise_user_id' value='{{ empty(old('franchise_user_id'))?$__member["franchise_user_id-username"]:old('franchise_user_id') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='franchise_user_id'>{{ trans('general.button.check') }}</button>
                                <span id='franchise_user_id-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=franchise_user_id]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection