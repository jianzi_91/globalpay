@extends('admin.layouts.app')
@section('title', trans("general.page.admin.member-country.bar-title.member-country"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.member-country.bar-title.member-country") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    {{ trans("general.page.admin.member-country.content.current_date_time") }}: <span id="current-datatime">-</span>

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans("general.table.header.number") }}</th>
                                <th>{{ trans("general.page.admin.member-country.content.continent") }}</th>
                                <th>{{ trans("general.page.admin.member-country.content.country_code") }}</th>
                                <th>{{ trans("general.table.header.country") }}</th>
                                <th>{{ trans("general.page.admin.member-country.content.total_members") }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-right">{{ trans("general.table.footer.total") }}:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: null, orderable: false },
        {data: 'continent_name'},
        {data: 'country'},
        {data: 'country_name'},
        {data: 'total_member' }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bl<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/member-country/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                $("#current-datatime").html(json.current_datetime);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();
            var numberWithCommas = function (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,%]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
            
            var footer_columns = [4]; 

            for (var j=0; j<footer_columns.length; j++ ) {
                if (api.column(footer_columns[j]).data().length){
                    var total = api
                    .column( footer_columns[j] )
                    .data()
                    .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                    } ) }
                else{ total = 0};
                
                total = numberWithCommas(total > 0? parseFloat(total).toFixed():0);

                // Update footer
                $( api.column(footer_columns[j]).footer() ).html(
                    total
                );
            }
        },
    });
 
    oTable.on( 'order.dt search.dt', function () {
        oTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'total_member:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        location.href = "#main-table";
        e.preventDefault();
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 