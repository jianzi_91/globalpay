@extends('admin.layouts.app')
@section('title', trans("general.page.admin.trade-overview.bar-title.trade-overview", ["trade_name" => trans("system.trade_name")]))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.trade-overview.bar-title.trade-overview", ["trade_name" => trans("system.trade_name")]) }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    <h4>{{ trans("general.page.admin.trade-overview.bar-title.trade-allocation", ["trade_name" => trans("system.trade_name")]) }} <small>({{ trans("general.page.admin.trade-overview.content.current_price") }}: <span id="trade-price">-</span>)</small></h4>
                    
                    <table id="trade-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans("general.page.admin.trade-overview.content.owner") }}:</th>
                                <th>{{ trans("general.page.admin.trade-overview.content.total_trade_unit", ["trade_name" => trans("system.trade_name")]) }}</th>
                                <th>{{ trans("general.page.admin.trade-overview.content.total_trade_value", ["trade_name" => trans("system.trade_name")]) }}</th>
                                <th>{{ trans("general.page.admin.trade-overview.content.ratio") }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="text-align:right">{{ trans("general.table.footer.total") }}:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>

                    <hr/>
                    <br/>
                    <h4>{{ trans("general.page.admin.trade-overview.bar-title.company_account_list") }}</h4>
                    
                    <table id="company-account-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans("field.user.id") }}</th>
                                <th>{{ trans("field.user.username") }}</th>
                                <th>{{ trans("field.user.name") }}</th>
                                <th>{{ trans("general.wallet.trade-unit") }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'user_type', orderable: false },
        {data: 'sum_unit', orderable: false },
        {data: 'sum_value', orderable: false },
        {data: 'ratio', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#trade-table').DataTable({
        "dom": 'l<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/trade-allocation/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                $("#trade-price").html(json.trade_price);
                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();
            var numberWithCommas = function (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,%]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
            
            var footer_columns = [1, 2, 3]; 
            var total = [];
            for (var j=0; j<footer_columns.length; j++ ) {
                if (api.column(footer_columns[j]).data().length){
                    total[j] = api
                    .column( footer_columns[j] )
                    .data()
                    .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                    } ) }
                else{ total[j] = 0};
            }
            
            total[0] = numberWithCommas(total[0].toFixed());
            total[1] = numberWithCommas(total[1].toFixed(3));
            total[2] = numberWithCommas(total[2].toFixed(2)) + " %";
            for (var j=0; j<footer_columns.length; j++ ) {
                // Update footer
                $( api.column(footer_columns[j]).footer() ).html(
                    total[j]
                );
            }
        },
        "columnDefs": [
            { className: "text-right", "targets": [1, 2, 3] }
        ]
    });
    
    // wallet table
    var columns = [
        {data: 'id' },
        {data: 'username' },
        {data: 'name'},
        {data: 'unit_held'}
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });
    var sTable = $('#company-account-table').DataTable({
        "dom": 'l<"clear-both">'+'<"table-responsive"'+'t'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/trade-allocation/company-account/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "columnDefs": [
            { className: "text-right", "targets": [3] }
        ]
    });
 
    $('#trade-table tbody, #company-account-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    });
});
</script>
@endsection 