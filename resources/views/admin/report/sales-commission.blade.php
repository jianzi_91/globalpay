@extends('admin.layouts.app')
@section('title', trans("general.page.admin.sales-commission.bar-title.sales-commission"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.sales-commission.bar-title.sales-commission") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans("general.page.admin.sales-commission.content.month") }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.wallet_sales", ["wallet" => trans("general.wallet.awallet")]) }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.wallet_sales", ["wallet" => trans("general.wallet.rwallet")]) }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.wallet_sales", ["wallet" => trans("general.wallet.fwallet")]) }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.total_sales") }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.total_bv") }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.total_bonus_payout") }}</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.total_bonus_payout") }} / {{ trans("general.page.admin.sales-commission.content.total_sales") }} %</th>
                                <th>{{ trans("general.page.admin.sales-commission.content.total_roi_payout") }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="text-align:right">{{ trans("general.table.footer.total") }}:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'bonus_month', orderable: false },
        {data: 'awallet_amount', orderable: false },
        {data: 'rwallet_amount', orderable: false },
        {data: 'fwallet_amount', orderable: false },
        {data: 'total_price' , orderable: false },
        {data: 'total_amount', orderable: false },
        {data: 'total_commission', orderable: false },
        {data: 'total_payout_ratio', orderable: false },
        {data: 'total_roi', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bl<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/sales-commission/datatable') }}",
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            { className: "text-right", "targets": [1, 2, 3, 4, 5, 6, 7, 8] },
            { visible: false }
        ],
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();
            var numberWithCommas = function (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,%]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
            
            var footer_columns = [1, 2, 3, 4, 5, 6, 8]; 
            
            var total = [];
            for (var j=0; j<footer_columns.length; j++ ) {
                if (api.column(footer_columns[j]).data().length){
                    total[j] = api
                    .column( footer_columns[j] )
                    .data()
                    .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                    } ) }
                else{ total[j] = 0};
            }

            for (var i = 0; i< total.length; i++) {
                total[i] = intVal(total[i]);
            }

            var par_total = total[5]/total[4] *100;
            
            total[0] = numberWithCommas(parseFloat(total[0]).toFixed(2));
            total[1] = numberWithCommas(parseFloat(total[1]).toFixed(2));
            total[2] = numberWithCommas(parseFloat(total[2]).toFixed(2));
            total[3] = numberWithCommas(parseFloat(total[3]).toFixed(2));
            total[4] = numberWithCommas(parseFloat(total[4]).toFixed(2));
            total[5] = numberWithCommas(parseFloat(total[5]).toFixed(2));
            total[6] = numberWithCommas(parseFloat(total[6]).toFixed(2));

            for (var j=0; j<footer_columns.length; j++ ) {
                // Update footer
                $( api.column(footer_columns[j]).footer() ).html(
                    total[j]
                );
            }

            par_total = numberWithCommas(par_total.toFixed(2));
            $( api.column(7).footer() ).html(
                par_total
            );
        },
        "order": []
    });
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
});
</script>
@endsection 