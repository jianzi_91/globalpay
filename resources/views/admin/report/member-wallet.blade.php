@extends('admin.layouts.app')
@section('title', trans("general.page.admin.wallet-summary.bar-title.wallet-summary"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.wallet-summary.bar-title.wallet-summary") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="main-table-div">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans("general.table.header.wallet") }}</th>
                                    <th>{{ trans("general.page.admin.wallet-summary.content.no_of_user") }}</th>
                                    <th>{{ trans("general.page.admin.wallet-summary.content.total_amount") }}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right">{{ trans("general.table.footer.total") }}:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="sub-table-div" style="display:none;">
                        <table id="sub-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans("field.user.id") }}</th>
                                    <th>{{ trans("field.user.username") }}</th>
                                    <th>{{ trans("general.table.header.amount") }}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'wallet', orderable: false },
        {data: 'user_count'},
        {data: 'sum_amount'}
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bl<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/member-wallet/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [1, 2] },
            { visible: false }
        ],
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "order": [],
        "deferLoading": 0, // prevent initial loading
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();
            var numberWithCommas = function (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,%]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
            
            var footer_columns = [2]; 
            var total = [];
            for (var j=0; j<footer_columns.length; j++ ) {
                if (api.column(footer_columns[j]).data().length){
                    total[j] = api
                    .column( footer_columns[j] )
                    .data()
                    .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                    } ) }
                else{ total[j] = 0};
            }
            
            total[0] = numberWithCommas(parseFloat(total[0]).toFixed(4));

            for (var j=0; j<footer_columns.length; j++ ) {
                // Update footer
                $( api.column(footer_columns[j]).footer() ).html(
                    total[j]
                );
            }
        },
    });
    
    // wallet table
    var columns = [
        {data: 'id' },
        {data: 'username' },
        {data: 'amount'}
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });
    var sTable = $('#sub-table').DataTable({
        "dom": 'Bli<"clear-both">'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/report/member-wallet/sub/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [2] },
            { visible: false }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
 
    $('#main-table tbody, #sub-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {        
        if ($("select[name=wallet]").val() == "summary") {
            $('#main-table-div').css("display", "block")
            $('#sub-table-div').css("display", "none")
            oTable.draw();
            location.href = "#main-table";
        }
        else {
            $('#main-table-div').css("display", "none")
            $('#sub-table-div').css("display", "block")
            sTable.draw();
            location.href = "#sub-table";
        }
        e.preventDefault();
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 