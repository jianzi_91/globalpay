@extends('admin.layouts.app')
@section('title', trans("general.page.admin.withdrawal.bar-title.withdrawal"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.withdrawal.bar-title.withdrawal") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th class="noExport">
                                    <input id="withdrawals-toggle-all" type="checkbox" /><br/>
                                    <form id="withdraw-paid-form" method="POST" action="{{ url(config('app.admin_slug').'/withdrawals/multi-update/paid') }}">
                                        {{ csrf_field() }}
                                        <select id="withdraw-paid-form-select" name="withdrawal_ids[]" style="display:none" multiple="multiple">
                                        </select>
                                        <button id="withdraw-paid-form-submit" class="btn btn-sm btn-primary">{{ trans("general.button.pay") }}</button>
                                    </form>
                                </th>
                                <th>{{ trans('field.withdrawal.id') }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.withdrawal.editor_id') }}</th>
                                <th>{{ trans('field.withdrawal.amount') }}</th>
                                <th>{{ trans('field.withdrawal.receivable_amount') }}</th>
                                <th>{{ trans('field.withdrawal.fee') }}</th>
                                <th>{{ trans('field.withdrawal.currency') }}</th>
                                <th>{{ trans('field.withdrawal.currency_amount') }}</th>
                                <th>{{ trans('field.withdrawal.currency_receivable_amount') }}</th>
                                <th>{{ trans('field.withdrawal.currency_fee') }}</th>
                                <th>{{ trans('field.withdrawal.bank_country') }}</th>
                                <th>{{ trans('field.withdrawal.bank_name') }}</th>
                                <th>{{ trans('field.withdrawal.bank_branch_name') }}</th>
                                <th>{{ trans('field.withdrawal.bank_payee_name') }}</th>
                                <th>{{ trans('field.withdrawal.bank_acc_no') }}</th>
                                <th>{{ trans('field.withdrawal.bank_sorting_code') }}</th>
                                <th>{{ trans('field.withdrawal.bank_iban') }}</th>
                                <th>{{ trans('field.withdrawal.status') }}</th>
                                <th>{{ trans('field.withdrawal.remarks') }}</th>
                                <th>{{ trans('field.withdrawal.aremarks') }}</th>
                                <th>{{ trans('general.table.header.date') }}</th>
                                <th class="noExport">{{ trans('general.table.header.action') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><span id="amount-span">-<span></th>
                                <th><span id="receivable_amount-span">-<span></th>
                                <th><span id="fee-span">-<span></th>
                                <th></th>
                                <th><span id="currency_amount-span">-<span></th>
                                <th><span id="currency_receivable_amount-span">-<span></th>
                                <th><span id="currency_fee-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready( function () {

    var selected = [];
    var columns = [
        {data: 'multi-select-box', orderable: false},
        {data: 'id'},
        {data: 'uid' }, 
        {data: 'user_username', orderable: false }, 
        {data: 'user_name', orderable: false }, 
        {data: 'editor_id-text', orderable: false }, 
        {data: 'amount' }, 
        {data: 'receivable_amount' }, 
        {data: 'fee' }, 
        {data: 'currency' }, 
        {data: 'currency_amount' }, 
        {data: 'currency_receivable_amount' }, 
        {data: 'currency_fee' }, 
        {data: 'bank_country' }, 
        {data: 'bank_name' }, 
        {data: 'bank_branch_name' }, 
        {data: 'bank_payee_name' }, 
        {data: 'bank_acc_no' }, 
        {data: 'bank_sorting_code' }, 
        {data: 'bank_iban' }, 
        {data: 'status' }, 
        {data: 'remarks' }, 
        {data: 'aremarks' }, 
        {data: 'created_at' },
        {data: 'action', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/withdrawals/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                // var dataSerialize = $("#search-form").serialize();
                // window.history.pushState("object or string", "Title", window.location.origin + window.location.pathname + "?" + dataSerialize);
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                try {
                    $("#amount-span").html(json.grand_total.amount);
                    $("#receivable_amount-span").html(json.grand_total.receivable_amount);
                    $("#fee-span").html(json.grand_total.fee);
                    $("#currency_amount-span").html(json.grand_total.currency_amount);
                    $("#currency_receivable_amount-span").html(json.grand_total.currency_receivable_amount);
                    $("#currency_fee-span").html(json.grand_total.currency_fee);
                }
                catch(err) {
                    console.log(err.message);
                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            },
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            {
                visible: false
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [6, 7, 8, 10, 11, 12] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ],
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    // toggle check all
    $("input#withdrawals-toggle-all").click(function (event) {
        var checked = this.checked;
        $("input[name=withdrawal_ids]:enabled").prop("checked", checked);
    });

    // toggle paid
    $("#withdraw-paid-form-submit").click( function (event) {
        event.preventDefault();
        $("select#withdraw-paid-form-select").html("");
        $("input[name=withdrawal_ids]:checked").each(function() {
            $("select#withdraw-paid-form-select").append($('<option>', {
                value: this.value,
                selected: true
            }));
        });
        var checked_value = $("select#withdraw-paid-form-select").val();
        if (checked_value && checked_value.length > 0) {
            bootbox.confirm({
                title: "Withdrawal Paid",
                message: '<div class="text-center">Your have selected ' + checked_value.length + ' withdrawal for update to paid. Do you want to proceed?</div>',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> {{ trans("general.button.no") }}'
                    },
                    confirm: {
                        className: "btn-primary btn-confirm-sell",
                        label: '<i class="fa fa-check"></i> {{ trans("general.button.yes") }}'
                    }
                },
                callback: function (result) {
                    $('.bootbox-confirm .btn-confirm-sell').prop('disabled', true);
                    var executed = false;
                    if (result === true && executed === false) {
                        executed = true;
                        $("#withdraw-paid-form").submit();
                    }
                }
            });
        }
        else {
            bootbox.alert({
                title: "Withdrawal Paid",
                message: '<div class="text-center">Please select at least 1 record.</div>',
            });
        }
    });
});
</script>
@endsection 