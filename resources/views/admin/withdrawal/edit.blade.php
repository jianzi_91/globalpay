@extends('admin.layouts.app')
@section('title', "Edit Withdrawal")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Edit Withdrawal</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/withdrawals') }}">Withdrawal</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/withdrawals/'.$__withdrawal->id.'/edit') }}">Edit Withdrawal</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Edit Withdrawal
                </div>
                
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.id') }}</td>
                            <td>{{ $__withdrawal->id }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.created_at') }}</td>
                            <td>{{ $__withdrawal["created_at_op"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__user->username }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.mobile') }}</td>
                            <td>{{ $__user->mobile }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td>
                            <td>{{ $__user->email }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_country') }}</td>
                            <td>{{ $__withdrawal["bank_country-text"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_name') }}</td>
                            <td>{{ $__withdrawal->bank_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_payee_name') }}</td>
                            <td>{{ $__withdrawal->bank_payee_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_acc_no') }}</td>
                            <td>{{ $__withdrawal->bank_acc_no }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_branch_name') }}</td>
                            <td>{{ $__withdrawal->bank_branch_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_sorting_code') }}</td>
                            <td>{{ $__withdrawal->bank_sorting_code }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_iban') }}</td>
                            <td>{{ $__withdrawal->bank_iban }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.amount') }}</td>
                            <td>{{ number_format($__withdrawal->amount, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.receivable_amount') }}</td>
                            <td>{{ number_format($__withdrawal->receivable_amount, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.fee') }}</td>
                            <td>{{ number_format($__withdrawal->fee, 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency') }}</td>
                            <td>{{ $__withdrawal->currency?$__withdrawal->currency:"-" }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_amount') }}</td>
                            <td>{{ $__withdrawal->currency?number_format($__withdrawal->currency_amount, 2):"-" }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_receivable_amount') }}</td>
                            <td>{{ $__withdrawal->currency?number_format($__withdrawal->currency_receivable_amount, 2):"-"  }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_fee') }}</td>
                            <td>{{ $__withdrawal->currency?number_format($__withdrawal->currency_fee, 2):"-"  }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.remarks') }}</td>
                            <td>{{ $__withdrawal->remarks }}</td>
                        </tr>
                    @if($__withdrawal->status == $__fields['status']['to_code']["paid"])
                        <tr>
                            <td>{{ trans('field.withdrawal.aremarks') }}</td>
                            <td>{{ $__withdrawal->aremarks }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.status') }}</td>
                            <td>{{ trans("general.withdrawal.status.".$__withdrawal["status"]) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.editor_id') }}</td>
                            <td>{{ $__withdrawal["editor_id-text"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.paid_at') }}</td>
                            <td>{{ $__withdrawal["paid_at_op"] }}</td>
                        </tr>
                    </table>
                    @elseif($__withdrawal->status == $__fields['status']['to_code']["cancelled"])
                        <tr>
                            <td>{{ trans('field.withdrawal.aremarks') }}</td>
                            <td>{{ $__withdrawal->aremarks }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.status') }}</td>
                            <td>{{ trans("general.withdrawal.status.".$__withdrawal["status"]) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.editor_id') }}</td>
                            <td>{{ $__withdrawal["editor_id-text"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.updated_at') }}</td>
                            <td>{{ $__withdrawal["updated_at_op"] }}</td>
                        </tr>
                    </table>
                    @else
                    </table>

                    <hr/>
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/withdrawals/'.$__withdrawal->id.'/edit') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.withdrawal.aremarks') }}</label>
							<div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.withdrawal.aremarks') }}" class="form-control" name='aremarks' value='{{empty(old('aremarks'))?$__withdrawal->aremarks:old('aremarks')}}'>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.withdrawal.status') }}</label>
							<div class="col-md-6">
                                @foreach($__fields["status"]["options"] as $_field=>$_value)
								<div class="i-checks">
									<label> 
										<input type="radio" value="{{ $_value }}" name="status" {{ (empty(old('status')) && $__withdrawal->status == $_value) || old("status")==$_value?'checked':''}} > <i></i> {{ trans('general.withdrawal.status.'.$_value) }} 
									</label>
								</div>
                                @endforeach
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
                    @endif
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
$( document ).ready(function () {
    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
});
</script>
@endsection