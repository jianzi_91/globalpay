@extends('admin.layouts.app')
@section('title', 'Add Admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Add Admin</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li>
					<a href="{{ url(config('app.admin_slug').'/adminusers') }}">Admin List</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/adminusers/create') }}">Add Admin</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Add Admin
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal"role="form" action="{{ url(config('app.admin_slug').'/adminusers/create') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.admin.username') }}" class="form-control" name='username' value='{{old('username')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.admin.name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.admin.name') }}" class="form-control" name='name' value='{{old('name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.email') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.admin.email') }}" class="form-control" name='email' value='{{old('email')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.country') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" required="required" name="country">
                                @foreach($__fields['country']['options'] AS $_code=>$_country)
                                    <option value='{{ $_code }}' {{(old('country') == $_code ) ?'selected':''}}>{{ $_country }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.admin.privileges') }}</label>
							<div class="col-md-6">
                                @foreach ($__fields['privileges']['options'] as $_code => $_privilege)
								<div class="i-checks">
									<label> 
										<input type="checkbox" value="{{ $_privilege }}" name="privileges[]" {{ (in_array($_privilege, (empty(old('privileges')) ? [] :old('privileges'))) ? 'checked':'') }} > {{ trans('general.admin.privileges.'.$_code) }} 
									</label>
							    </div>
                                @endforeach
							</div>
                        </div>
                        <hr/>
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.password') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.admin.password') }}" class="form-control" name='password' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.password_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.admin.password_confirmation') }}" class="form-control" name='password_confirmation' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
$( document ).ready(function () {
    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
});
</script>
@endsection