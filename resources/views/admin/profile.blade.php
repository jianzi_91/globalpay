@extends('admin.layouts.app')
@section('title', 'Profile')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Profile</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/profile') }}">Profile</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    General Info
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/profile') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.username') }}</label>
                            <div class="col-md-6">
                                {{$admin->username}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.admin.name') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.admin.name') }}" class="form-control" name='name' value='{{empty(old('name'))?$admin->name:old('name')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.email') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.admin.email') }}" class="form-control" name='email' value='{{empty(old('email'))?$admin->email:old('email')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.country') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="country">
                                @foreach($__fields['country']['options'] AS $_code=>$_country)
                                    <option value='{{ $_code }}' {{((empty(old('country'))?$admin->country:old('country')) == $_code ) ?'selected':''}}>{{ $_country }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
		
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Password
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/profile') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='2'>
                       
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.password-old') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.admin.password-old') }}" class="form-control" name='password' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.password') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.admin.password') }}" class="form-control" name='new_password' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.admin.password_confirmation') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.admin.password_confirmation') }}" class="form-control" name='new_password_confirmation' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
$( document ).ready(function () {
    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
});
</script>
@endsection