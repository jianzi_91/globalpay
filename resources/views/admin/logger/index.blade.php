@extends('admin.layouts.app')
@section('title', trans('general.page.admin.logger.bar-title.logger'))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.logger.bar-title.logger')</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    @lang('general.page.admin.logger.content.last_archive_date'): <span id="archive-datatime">-</span>

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('general.table.header.date') }}</th>
                                <th>@lang('field.logger.itype')</th>
                                <th>@lang('field.logger.iid')</th>
                                <th>@lang('field.logger.id1')</th>
                                <th>@lang('field.logger.id2')</th>
                                <th>@lang('field.logger.log_data')</th>
                                <th>@lang('field.logger.path')</th>
                                <th>@lang('field.logger.action')</th>
                                <th>@lang('field.logger.ip')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'created_at' },
        {data: 'itype'}, 
        {data: 'iid-text', orderable: false },
        {data: 'id1' },
        {data: 'id2' },
        {data: 'log_data' },
        {data: 'path' },
        {data: 'action' },
        {data: 'ip' },
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/loggers/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                $("#archive-datatime").html(json.info.last_archive);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            },
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { visible: false }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

});
</script>
@endsection 