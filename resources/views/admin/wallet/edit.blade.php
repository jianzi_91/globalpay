@extends('admin.layouts.app')
@section('title', "Edit Member Wallet")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>@lang('general.menu.admin.wallet.edit_member_wallet')</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">@lang('general.menu.admin.home')</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/wallets/edit') }}">@lang('general.menu.admin.wallet.edit_member_wallet')</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('general.menu.admin.wallet.edit_member_wallet')
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/wallets/edit') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.mobile') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='uid' value='{{old('uid')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                                <span id='uid-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.wallet') }}</label>
                            <div class="col-md-6">
                                <select class="form-control"  name="wallet">
                                    <option value=''>@lang('general.selection.please-select')</option>
                                    @foreach ($__fields["wallet"]['options'] as $_value)
                                        <option value='{{ $_value }}' {{ $_value == old('wallet') ? 'selected' : '' }}>{{ trans("general.wallet.".$_value) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.amount') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.amount') }}" class="form-control" name='amount' value='{{old('amount')}}'>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.type') }}</label>
							<div class="col-md-6">
                                @foreach ($__fields["type"]['options'] as $_value)
                                <div class="i-checks">
                                    <label> 
                                        <input type="radio" value="{{ $_value }}" name="type" {{empty(old('type')) || old('type')==$_value?'checked':''}} > <i></i> {{ trans('general.wallet-record.type.'.$_value) }} 
                                    </label>
                                </div>
                                @endforeach
							</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.price') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.price') }}" class="form-control" name='price' value='{{old('price')}}'>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.remarks') }}</label>
							<div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.remarks') }}" class="form-control" name='remarks' value='{{old('remarks')}}'>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.aremarks') }}</label>
							<div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.aremarks') }}" class="form-control" name='aremarks' value='{{old('aremarks')}}'>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //check username
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();
            console.log(username);
            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('wallet/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection