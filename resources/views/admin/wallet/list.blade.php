@extends('admin.layouts.app')
@section('title', trans('general.page.admin.wallet-records.bar-title.wallet-records'))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.admin.wallet-records.bar-title.wallet-records') }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.wallet_record.id') }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.mobile') }}</th>
                                <th>{{ trans('field.wallet_record.wallet') }}</th>
                                <th>{{ trans('field.wallet_record.type') }}</th>
                                <th>{{ trans('field.wallet_record.price') }}</th>
                                <th>{{ trans('general.table.header.amount') }}</th>
                                <th>{{ trans('general.table.header.balance') }}</th>
                                <th>{{ trans('field.wallet_record.trans_code') }}</th>
                                <th>{{ trans('field.wallet_record.descr') }}</th>
                                <th>{{ trans('field.wallet_record.itype') }}</th>
                                <th>{{ trans('field.wallet_record.iid') }}</th>
                                <th>{{ trans('general.table.header.date') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><span id="amount-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'id'},
        {data: 'uid' }, 
        {data: 'user_username' }, 
        {data: 'user_name' }, 
        {data: 'user_mobile' }, 
        {data: 'wallet' }, 
        {data: 'type' }, 
        {data: 'price', className: "text-right" }, 
        {data: 'amount', className: "text-right" }, 
        {data: 'balance', className: "text-right" }, 
        {data: 'trans_code' }, 
        {data: 'descr-text', orderable: false }, 
        {data: 'itype', orderable: false }, 
        {data: 'iid-text', orderable: false },
        {data: 'created_at' },
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/wallet-records/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                try {
                    $("#amount-span").html(json.grand_total.amount);
                }
                catch(err) {
                    console.log(err.message);
                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { visible: false }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ],
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

});
</script>
@endsection 