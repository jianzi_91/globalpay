@extends('admin.layouts.app')
@section('title', trans("general.page.admin.non-trade-history.bar-title.non-trade-history", ["trade_name" => trans('general.wallet.trade-float-unit')]))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.non-trade-history.bar-title.non-trade-history", ["trade_name" => trans('general.wallet.trade-float-unit')]) }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="table-wrap" class="hide">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('field.wallet_record.id') }}</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('general.table.header.date') }}</th>
                                    <th>{{ trans('field.wallet_record.type') }}</th>
                                    <th>{{ trans('trade.qty') }}</th>
                                    <th>{{ trans('field.wallet_record.itype') }}</th>
                                    <th>{{ trans('field.wallet_record.iid') }}</th>
                                    <th>{{ trans('field.wallet_record.descr') }}</th>
                                </tr>
                            </thead>
                            <tfoot id="tbl_footer">
                                <tr>
                                    <th></th>
                                    <th>{{ trans("general.page.admin.non-trade-history.content.total_credit") }}</th>
                                    <th id="credit_sum" class="text-right"></th>
                                    <th></th>
                                    <th>{{ trans("general.page.admin.non-trade-history.content.total_debit") }}</th>
                                    <th id="debit_sum" class="text-right"></th>
                                    <th></th>
                                    <th>{{ trans("general.page.admin.non-trade-history.content.balance") }}</th>
                                    <th id="balance" class="text-right"></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'DT_Row_Index', orderable: false},
        {data: 'id'},
        {data: 'member_id'}, 
        {data: 'member_username'}, 
        {data: 'created_at'},
        {data: 'tx_type'}, 
        {data: 'qty'},
        {data: 'itype', orderable: false}, 
        {data: 'iid', orderable: false},
        {data: 'descr', orderable: false},
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/trade/history/non-trade/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                try {
                    $("#credit_sum").html(json.credit_sum.toLocaleString());
                    $("#debit_sum").html(json.debit_sum.toLocaleString());
                    $("#balance").html(json.balance.toLocaleString());
                }
                catch (err) {

                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [6] },
            { visible: false }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ]
    ]);
    //oTable.draw();
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
        $("#table-wrap").removeClass('hide');
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('[name="radio_date"]').first().click();
});
</script>
@endsection 