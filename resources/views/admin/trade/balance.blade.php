@extends('admin.layouts.app')
@section('title', trans('general.page.admin.trade-balance.bar-title.trade-balance', ["trade_name" => trans("system.trade_name")]))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.trade-balance.bar-title.trade-balance', ["trade_name" => trans("system.trade_name")])</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="table-wrap" class="hide">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.user.name') }}</th>
                                    <th>{{ trans('field.user.country') }}</th>
                                    <th>{{ trans('general.wallet.trade-unit') }}</th>
                                </tr>
                            </thead>
                            <tfoot id="tbl_footer">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('general.page.admin.trade-balance.content.balance')</th>
                                    <th id="unit_held_sum" class="text-right"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'DT_Row_Index', orderable: false},
        {data: 'id'},
        {data: 'username'}, 
        {data: 'name'}, 
        {data: 'country', orderable: false}, 
        {data: 'unit_held'}
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/trade/balance/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                try {
                    $("#unit_held_sum").html(json.unit_held_sum.toLocaleString());
                }
                catch (err) {

                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [5] },
            { visible: false }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
        {{-- "drawCallback": function( settings ) {
            $('#balance').html( $('.calc').sumHTML().toLocaleString() );
            $('#total_credit').html( $('.calc').sumHTML('c').toLocaleString() );
            $('#total_debit').html( $('.calc').sumHTML('d', true, true).toLocaleString() );
        } --}}
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
    //oTable.draw();
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
        $("#table-wrap").removeClass('hide');
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 