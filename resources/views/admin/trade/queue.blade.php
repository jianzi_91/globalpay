@extends('admin.layouts.app')
@section('title', trans('general.page.admin.trade-queue-history.bar-title.trade-queue-history'))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.trade-queue-history.bar-title.trade-queue-history')</h2>  

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="table-wrap" class="hide">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('trade.buy_id')</th>
                                    <th>{{ trans('field.user.id') }} (@lang('trade.buy'))</th>
                                    <th>{{ trans('field.user.username') }} (@lang('trade.buy'))</th>
                                    <th>{{ trans('field.user.name') }} (@lang('trade.buy'))</th>
                                    <th>@lang('trade.sell_id')</th>
                                    <th>{{ trans('field.user.id') }} (@lang('trade.sell'))</th>
                                    <th>{{ trans('field.user.username') }} (@lang('trade.sell'))</th>
                                    <th>{{ trans('field.user.name') }} (@lang('trade.sell'))</th>
                                    <th>@lang('trade.buy_date')</th>
                                    <th>@lang('trade.qty')</th>
                                    <th>@lang('trade.price')</th>
                                    <th>@lang('general.table.header.status')</th>
                                    <th>@lang('general.table.header.action')</th>
                                </tr>
                            </thead>
                            <tfoot id="tbl_footer">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th id="total-matched"></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'DT_Row_Index', orderable: false},
        {data: 'buy_id'}, 
        {data: 'buyer_uid'}, 
        {data: 'buyer_username'}, 
        {data: 'buyer_name'},
        {data: 'sell_id'}, 
        {data: 'seller_uid'}, 
        {data: 'seller_username'}, 
        {data: 'seller_name'},
        {data: 'queue_datetime'},
        {data: 'quantity', className: 'text-right' },  
        {data: 'price', className: 'text-right' },
        {data: 'status' },
        {data: 'action', width: 'auto'},
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/trade/history/queue/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                try {
                    $("#total-matched").html(json.total_matched.toLocaleString());
                }
                catch (err) {

                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'queue_datetime:name' ).index()), "desc" ]
    ]);
    //oTable.draw();
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
        $("#table-wrap").removeClass('hide');
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('[name="radio_buy_date"]').first().click();
});
</script>
@endsection 