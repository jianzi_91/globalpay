@extends('admin.layouts.app')
@section('title', trans('general.page.admin.trade-pending-sell-history.bar-title.trade-pending-sell-history', ["trade_name" => trans("system.trade_name")]))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.trade-pending-sell-history.bar-title.trade-pending-sell-history', ["trade_name" => trans("system.trade_name")])</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="table-wrap" class="hide">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('general.table.header.sell_id') }}</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.user.name') }}</th>
                                    <th>@lang('general.table.header.country')</th>
                                    <th>@lang('general.table.header.placed_qty')</th>
                                    <th>@lang('general.table.header.pending_qty')</th>
                                    <th>{{ trans('trade.price') }}</th>
                                    <th>{{ trans('general.table.header.status') }}</th>
                                    <th>@lang('general.table.header.sell_date')</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">@lang('general.page.admin.trade-pending-sell-history.content.total_pending')</th>
                                    <th id="total-pending"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'DT_Row_Index', orderable: false},
        {data: 'sell_id'},
        {data: 'uid'}, 
        {data: 'username'},
        {data: 'name'},
        {data: 'country_en'},
        {data: 'initial_qty', className: 'text-right' },
        {data: 'open_qty', className: 'text-right' },
        {data: 'price'},
        {data: 'status'},
        {data: 'sell_datetime'},
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/trade/history/sell/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                try {
                    $("#total-pending").html(json.total_pending.toLocaleString());
                }
                catch (err) {

                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
        {{-- "drawCallback": function( settings ) {
            $('#placed_qty').html( $('.add-placed').sumHTML().toLocaleString() );
            $('#sold_qty').html( $('.add-sold').sumHTML().toLocaleString() );
            $('#pending_qty').html( $('.add-pending').sumHTML().toLocaleString() );
        } --}}
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'sell_datetime:name' ).index()), "desc" ],
    ]);
    //oTable.draw();
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
        $("#table-wrap").removeClass('hide');
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('[name="radio_sell_date"]').first().click();
});
</script>
@endsection 