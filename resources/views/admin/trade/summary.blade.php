@extends('admin.layouts.app')
@section('title', trans('general.page.admin.trade-summary.bar-title.trade-summary', ["trade_name" => trans("system.trade_name")]))

@section('content')
<div class="container">
    <h2>@lang('general.page.admin.trade-summary.bar-title.trade-summary', ["trade_name" => trans("system.trade_name")])</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="table-wrap" class="hide">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('general.table.header.last_transaction_date')</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.user.name') }}</th>
                                    <th>@lang('general.table.header.country')</th>
                                    <th>{{ trans('field.user.ref') }}</th>
                                    <th>@lang('general.table.header.total_investment_amt')</th>
                                    <th>{{ trans('general.table.header.total_trade_income', ['trade_name'=> trans("system.trade_name")]) }}</th>
                                    <th>@lang('general.table.header.total_pending_withdrawal')</th>
                                    <th>@lang('general.table.header.total_paid_withdrawal')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    var selected = [];
    var columns = [
        {data: 'DT_Row_Index', orderable: false},
        {data: 'tx_datetime'},
        {data: 'uid'}, 
        {data: 'username'}, 
        {data: 'name'}, 
        {data: 'country'},
        {data: 'sponsor'}, 
        {data: 'total_investment_amt', orderable: false},
        {data: 'total_trade_income'},
        {data: 'total_pending_withdrawal', orderable: false}, 
        {data: 'total_paid_withdrawal', orderable: false},
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/trade/summary/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
            }, 
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [7, 8, 9, 10] },
            { visible: false }
        ],
        "paging": true,
        "pageLength": 25,
        "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'tx_datetime:name' ).index()), "desc" ]
    ]);
    //oTable.draw();
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        $("#search-form-toggle").click();
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
        $("#table-wrap").removeClass('hide');
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('[name="radio_date"]').first().click();
});
</script>
@endsection 