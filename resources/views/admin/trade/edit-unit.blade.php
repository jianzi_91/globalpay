@extends('admin.layouts.app')
@section('title', 'Edit '.trans('system.trade_name'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Edit @lang('system.trade_name')</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li>
                    <a href="{{ url(config('app.admin_slug').'/trade/history') }}">@lang('system.trade_name') History</a>
                </li>
				<li class="active">
					Edit @lang('system.trade_name')
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Edit @lang('system.trade_name')
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/trade/edit-unit') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label" for="username">{{ trans('field.user.username') }}</label>
                            <div class="col-md-5">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name="username" value="{{ old('username') }}">
                                <span id="username_text"></span>
                                <div class="help-block with-errors">
                                    @if ($errors->has('username'))
                                        <strong>{!! $errors->first('username') !!}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <button id="btn_username_check" type="button" class="btn btn-sm btn-primary">{{ trans('general.button.check') }}</button>

                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('trade.price') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('trade.price') }}" class="form-control" name="price" value="{{ old('price') }}">
                                <div class="help-block with-errors">
                                    @if ($errors->has('price'))
                                        <strong>{!! $errors->first('price') !!}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('trade.qty') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('trade.qty') }}" class="form-control" name="qty" value="{{ old('qty') }}">
                                <div class="help-block with-errors">
                                    @if ($errors->has('qty'))
                                        <strong>{!! $errors->first('qty') !!}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('common.type')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="type">
                                    <option value="c"{{ old('type') == 'c' ? ' selected' : '' }}>@lang('common.credit') (+)</option>
                                    <option value="d"{{ old('type') == 'd' ? ' selected' : '' }}>@lang('common.debit') (-)</option>
                                </select>
                                <div class="help-block with-errors">
                                    @if ($errors->has('type'))
                                        <strong>{!! $errors->first('type') !!}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('field.trade.remark') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade.remark') }}" class="form-control" name="remark" value="{{ old('remark') }}" maxlength="200">
                                <div class="help-block with-errors">
                                    @if ($errors->has('remark'))
                                        <strong>{!! $errors->first('remark') !!}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('admin_remark') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('field.trade.admin_remark') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.trade.admin_remark') }}" class="form-control" name="admin_remark" value="{{ old('admin_remark') }}" maxlength="200">
                                <div class="help-block with-errors">
                                    @if ($errors->has('admin_remark'))
                                        <strong>{!! $errors->first('admin_remark') !!}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit" onclick="return confirm('Are you sure?');">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "#btn_username_check" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name="username"][type=text]').val();
            console.log(username);
            var output_field = $('#username_text');
            output_field.removeClass('text-danger').html();

            if (username) {
                username = username.trim();
                $('input[name="username"][type=text]').val(username);

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username },
                    url: '{{ url("trade/user-info") }}',
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + '{{ trans("ajax.errmsg.fail") }}').addClass('text-danger');
                    }
                });
            } else {
                output_field.html('<br>' + '{{ trans("ajax.errmsg.username-required") }}').addClass('text-danger');
            }
        });
    });
</script>
@endsection