@extends('admin.layouts.app')
@section('title', trans('general.page.admin.member-list.bar-title.member-list'))

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <h4>@lang('general.menu.admin.withdrawal.user_details')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="form-filled">
                        <div class="form-group">
                            <label>ID</label>
                        <p>{{$manualwithdrawal->id}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.id')</label>
                        <p>{{$manualwithdrawal->uid}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.name')</label>
                        <p>{{$user_info->name}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.mobile')</label>
                        <p>{{$user_info->mobile}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h4>@lang('general.menu.admin.withdrawal.withdrawal_details')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="form-filled">
                        <div class="form-group">
                            <label>@lang('field.user.status')</label>
                            @if ($manualwithdrawal->status == 0)
                            <p class="label label-warning">@lang('field.withdrawal.pending')</p>
                            @elseif ($manualwithdrawal->status == 1)
                            <p class="label label-success">@lang('field.withdrawal.completed')</p>
                            @elseif ($manualwithdrawal->status == 2)
                            <p class="label label-danger">@lang('field.withdrawal.cancelled')</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.receivable_amount')t</label>
                        <p>USD {{number_format($manualwithdrawal->receivable_amount, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.commission_amount')</label>
                        <p>USD {{number_format($manualwithdrawal->commission_amount, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.currency_amount')</label>
                        <p>RMB {{number_format($manualwithdrawal->receivable_amount * 7, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.currency_rate')</label>
                            <p>7</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <h4>@lang('general.menu.admin.withdrawal.receipt')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    {{-- <img src="https://www.beginner-bookkeeping.com/images/Word_Receipt_Example.jpg" class="receipt-img"> --}}
                    <div class="img-container">
                        @if(is_null($manualwithdrawal->receipt_url))
                            <p>—<p>
                        @else
                            <img src="{{ $manualwithdrawal->receipt_url }}" alt="">
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection 