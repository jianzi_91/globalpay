@extends('admin.layouts.app')
@section('title', trans('general.page.admin.member-list.bar-title.member-list'))

@section('content')
<div class="container">
<form id="form" action="{{ url(config('app.admin_slug').'/wallet-manualwithdrawal', $manualwithdrawal->id).'/edit' }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            @include('admin.common.success')
            @include('admin.common.errors')
        </div>
        <div class="col-md-4">
            <h4>@lang('general.menu.admin.withdrawal.user_details')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="form-filled">
                        <div class="form-group">
                            <label>ID</label>
                        <p>{{$manualwithdrawal->id}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.id')</label>
                        <p>{{$manualwithdrawal->uid}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.name')</label>
                        <p>{{$user_info->name}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.mobile')</label>
                        <p>{{$user_info->mobile}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h4>@lang('general.menu.admin.withdrawal.user_bank_details')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="form-filled">
                        <div class="form-group">
                            <label>@lang('field.user.bank_name')</label>
                        <p>{{$user_info->bank_name ? $user_info->bank_name : '-'}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.bank_branch_name')</label>
                        <p>{{$user_info->bank_branch_name ? $user_info->bank_branch_name : '-'}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.user.bank_acc_no')</label>
                        <p>{{$user_info->bank_acc_no ? $user_info->bank_acc_no : '-'}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <h4>@lang('general.menu.admin.withdrawal.withdrawal_details')</h4>
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="form-filled">
                        <div class="form-group">
                            <label>@lang('field.user.status')</label>
                            @if ($manualwithdrawal->status == 0)
                            <p class="label label-warning">@lang('field.withdrawal.pending')</p>
                            @elseif ($manualwithdrawal->status == 1)
                            <p class="label label-success">@lang('field.withdrawal.completed')</p>
                            @elseif ($manualwithdrawal->status == 2)
                            <p class="label label-danger">@lang('field.withdrawal.cancelled')</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.receivable_amount')t</label>
                        <p>USD {{number_format($manualwithdrawal->receivable_amount, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.commission_amount')</label>
                        <p>USD {{number_format($manualwithdrawal->commission_amount, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.currency_amount')</label>
                        <p>RMB {{number_format($manualwithdrawal->receivable_amount * 7, 2)}}</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('field.withdrawal.currency_rate')</label>
                            <p>7</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <h4>@lang('general.menu.admin.withdrawal.receipt')</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    {{-- <img src="https://www.beginner-bookkeeping.com/images/Word_Receipt_Example.jpg" class="receipt-img"> --}}
                    <div class="img-container">
                        {{-- <img src="" alt="" id="preview" class="img img-rounded" > --}}
                        <a id="single_image" data-fancybox-trigger="gallery" href="">
                            <img src="" alt="" id="preview" class="img img-rounded"/>
                        </a>
                    </div>
                    <div class="upload-action">
                        <label class="btn btn-primary"><span id="photo-text">@lang('general.field.upload_receipt')</span>
                            <input type="file" name="image" accept="image/*;capture=camera">
                        </label>
                    </div>
                    {{-- <div class="upload-action"><button type="file" class="btn btn-primary">Upload</button></div> --}}

                </div>
            </div>
        </div>

        <div class="col-md-12 text-center">
            <input type="hidden" name="status">
            <button id="btn_cancel" class="btn btn-danger">@lang('general.button.cancel')</button>
            <button id="btn_confirm" class="btn btn-success">@lang('general.button.confirm')</button>
        </div>
    </div>
</form>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
    $('a#single_image').fancybox({
    });

    $('input:file').change(function () {

        var file = this.files[0];
        var fileType = file["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];

        if ($.inArray(fileType, ValidImageTypes) < 0) {
            bootbox.alert({
                size: 'small',
                message: "{{ trans('validation.image', ['attribute' => trans('one.file')]) }}"
            });
        } else {
            $('#photo-text').text("{{ trans('general.field.new_image') }}");

            var reader = new FileReader();

            reader.onload = function()
            {
                var output = document.getElementById('preview');
                output.src = reader.result;
                var output2 = document.getElementById('single_image');
                output2.href = reader.result;
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    })
    $('#btn_confirm').click(function() {
        $('input[name=status]').val('confirm');
        $('#form').submit();
    });

    $('#btn_cancel').click(function() {
        $('input[name=status]').val('cancel');
        $('#form').submit();
    });
});
</script>
@endsection 