@extends('admin.layouts.app')
@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang("general.page.admin.home.bar-title.dashboard")</div>

                <div class="panel-body">
                @lang("general.page.admin.home.content.welcome")
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
