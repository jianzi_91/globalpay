@extends('admin.layouts.app')
@section('title', trans("general.page.admin.sales-cancellation.bar-title.sales-cancellation"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.sales-cancellation.bar-title.sales-cancellation") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')
                    
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.activation.created_at') }}</th>
                                <th>{{ trans('field.activation.id') }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.nid') }}</th>
                                <th>{{ trans('field.user.country') }}</th>
                                <th>{{ trans('field.activation.code-name') }}</th>
                                <th>{{ trans('field.activation.amount') }}</th>
                                <th>{{ trans('field.activation.price') }}</th>
                                <th>{{ trans('field.activation.aid') }}</th>
                                <th>{{ trans('field.activation.aid-username') }}</th>
                                <th>{{ trans('field.activation.aid-name') }}</th>
                                <th>{{ trans('general.wallet.cwallet') }} Balance</th>
                                <th>{{ trans('general.wallet.dwallet') }} Balance</th>
                                <th>{{ trans('general.wallet.swallet') }} Balance</th>
                                <th>{{ trans('field.user.ref') }}</th>
                                <th>{{ trans('field.activation.act_type') }}</th>
                                <th>{{ trans('field.activation.status') }}</th>
                                <th>{{ trans('field.activation.itype') }}</th>
                                <th>{{ trans('field.activation.iid') }}</th>
                                <th>{{ trans('field.activation.bonus_at') }}</th>
                                <th>{{ trans('general.table.header.action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <!-- Cancel Activation model -->
        <div class="modal fade bs-cancel-activation" tabindex="-1" role="dialog" aria-labelledby="CancelActivationModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cancel Activation</h4>
                    </div>
                    <form class="form-horizontal" id="cancel-form" action="" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body text-center">
                            <p>Are you confirm to cancel this activation?</p>
                            <div class="alert alert-warning">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.user.id") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="cancel-user-id"></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.activation.code-name") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="cancel-package-name"></span></p>
                                    </div>
                                </div>
                                <div id="cancel-loan-portion">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Loan Deducted</label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"><span id="cancel-loan-deduct"></span></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Credit Back to User</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="credit_back">
                                                <option value=1>Yes</option>
                                                <option value=0 selected>No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end Cancel Activation model -->

        <!-- Cancel Confirmed Activation model -->
        <div class="modal fade bs-cancel-confirmed-activation" tabindex="-1" role="dialog" aria-labelledby="CancelConfirmedActivationModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cancel Confirmed Activation</h4>
                    </div>
                    <form class="form-horizontal" id="cancel-confirmed-form" action="" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body text-center">
                            <p>Are you confirm want to cancel this already bonus calculated activation?</p>
                            <div class="alert alert-warning">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.user.id") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="cancel-confirmed-user-id"></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans("field.activation.code-name") }}</label>
                                    <div class="col-md-6">
                                        <p class="form-control-static"><span id="cancel-confirmed-package-name"></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end Cancel Activation model -->
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'created_at' },
        {data: 'id'},
        {data: 'uid', orderable: false }, 
        {data: 'uid-username', orderable: false }, 
        {data: 'uid-name', orderable: false }, 
        {data: 'uid-nid', orderable: false }, 
        {data: 'uid-country', orderable: false }, 
        {data: 'code' }, 
        {data: 'amount' }, 
        {data: 'price' }, 
        {data: 'aid', orderable: false }, 
        {data: 'aid-username', orderable: false }, 
        {data: 'aid-name', orderable: false }, 
        {data: 'cwallet' , orderable: false }, 
        {data: 'dwallet' , orderable: false }, 
        {data: 'swallet' , orderable: false }, 
        {data: 'ref-username', orderable: false }, 
        {data: 'act_type' }, 
        {data: 'status' }, 
        {data: 'itype', orderable: false }, 
        {data: 'iid-text', orderable: false }, 
        {data: 'bonus_at' },
        {data: 'action' , orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'tf'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/activations/cancel-list/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        // "columnDefs": [
        //     { className: "text-right", "targets": [8, 9, 13, 14, 15, 16, 17, 18, 19] }
        // ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ],
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

    $('.bs-cancel-activation').on('shown.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var action = "{{ url(config('app.admin_slug').'/activations/cancel') }}" + "/" + id ;

        $('#cancel-form').attr('action', action);
        $('#cancel-package-name').html($(e.relatedTarget).data('package-name'));
        $('#cancel-user-id').html($(e.relatedTarget).data('user-id'));
        $('[name=credit_back]').val(0);

        if ($(e.relatedTarget).data('act-type') == "30") {
            $('#cancel-loan-portion').show();
        }
        else {
            $('#cancel-loan-portion').hide();
        }
        $('#cancel-loan-deduct').html($(e.relatedTarget).data('loan-deduct'));
    });

    $('.bs-cancel-confirmed-activation').on('shown.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var action = "{{ url(config('app.admin_slug').'/activations/cancel-confirmed') }}" + "/" + id ;

        $('#cancel-confirmed-form').attr('action', action);
        $('#cancel-confirmed-package-name').html($(e.relatedTarget).data('package-name'));
        $('#cancel-confirmed-user-id').html($(e.relatedTarget).data('user-id'));
    });

    var postCounter = 0;
    $("form[method=post]").submit(function (e) {
        if (postCounter++ == 0) {
            $("form[method=post]").find("[type=submit]").attr("disabled", true);
        }
        else {
            e.preventDefault();
        }
    });
});
</script>
@endsection 