@extends('admin.layouts.app')
@section('title', 'Add Loan Activation')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Add Loan Activation</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li>
					<a href="{{ url(config('app.admin_slug').'/activations/list') }}">Activation List</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/loan/activations/add') }}">Add Loan Activation</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Add Loan Activation
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url(config('app.admin_slug').'/loan/activations/add') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='uid' value='{{old('uid')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                                <span id='uid-text'></span>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.code-name') }}</label>
							<div class="col-md-6">
                                @foreach($__fields['code']['options']  as $_code=>$_details)
								<div class="i-checks">
									<label> 
										<input type="radio" value="{{ $_code }}" name="code" {{old('code')==$_code?'checked':''}} > <i></i> {{ trans('general.activation.title.'.$_code) }}, {{ trans('field.activation.price') }} : {{ number_format($_details['price'], 2) }}, {{ trans('field.activation.amount') }} : {{ number_format($_details['amount'], 2) }}

                                        @if(isset($_details['get_wallet']))
                                            @foreach($_details['get_wallet'] as $_wallet=>$_amount)
                                                , {{ trans('general.wallet.'.$_wallet) }} : {{ number_format($_amount, 2) }}
                                            @endforeach
                                        @endif
									</label>
								</div>
                                @endforeach
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.distribute_after_loan') }}</label>
							<div class="col-md-6">
                                <select class="form-control" name="distribute_after_loan">
                                @foreach($__fields['distribute_after_loan']['options'] AS $_code)
                                    <option value='{{ $_code }}' {{ (old('distribute_after_loan')== $_code ) ?'selected':'' }}>{{ trans('general.activation.distribute_after_loan.'.$_code) }}</option>
                                @endforeach
                                </select>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.distribute_after_loan2') }}</label>
							<div class="col-md-6">
                                <select class="form-control" name="distribute_after_loan2">
                                @foreach($__fields['distribute_after_loan2']['options'] AS $_code)
                                    <option value='{{ $_code }}' {{ (old('distribute_after_loan2')== $_code ) ?'selected':'' }}>{{ trans('general.activation.distribute_after_loan2.'.$_code) }}</option>
                                @endforeach
                                </select>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.share_convert') }}</label>
							<div class="col-md-6">
                                <select class="form-control" name="share_convert">
                                @foreach($__fields['share_convert']['options'] AS $_code)
                                    <option value='{{ $_code }}' {{ (old('share_convert')== $_code ) ?'selected':'' }}>{{ trans('general.activation.share_convert.'.$_code) }}</option>
                                @endforeach
                                </select>
							</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.activation.share_price') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.activation.share_price') }}" class="form-control" name='share_price' value='{{old('share_price')}}'>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.user.dpercent') }}</label>
							<div class="col-md-6">
                                <select class="form-control" name="dpercent">
                                @foreach($__fields['dpercent']['options'] AS $_code=>$_percent)
                                    <option value='{{ $_code }}' {{ (old('dpercent')== $_code ) ?'selected':'' }}>{{ $_percent }}</option>
                                @endforeach
                                </select>
							</div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.aremarks') }}</label>
							<div class="col-md-6">
                               <input type="text" placeholder="{{ trans('field.activation.aremarks') }}" class="form-control" name='aremarks' value='{{old('aremarks')}}'>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('highest-loan-package/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection