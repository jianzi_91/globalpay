@extends('admin.layouts.app')
@section('title', trans("general.page.admin.sales-list.bar-title.sales-list"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.sales-list.bar-title.sales-list") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')
                    
                    @include('admin.partials.search-form')
                    
                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.activation.created_at') }}</th>
                                <th>{{ trans('field.activation.id') }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.nid') }}</th>
                                <th>{{ trans('field.user.country') }}</th>
                                <th>{{ trans('field.activation.code-name') }}</th>
                                <th>{{ trans('field.activation.amount') }}</th>
                                <th>{{ trans('field.activation.price') }}</th>
                                <th>{{ trans('field.activation.aid') }}</th>
                                <th>{{ trans('field.activation.aid-username') }}</th>
                                <th>{{ trans('field.activation.aid-name') }}</th>
                                <th>{{ trans('general.wallet.cwallet') }}</th>
                                <th>{{ trans('general.wallet.dwallet') }}</th>
                                <th>{{ trans('general.wallet.swallet') }}</th>
                                <th>{{ trans('field.user.ref') }}</th>
                                <th>{{ trans('field.activation.act_type') }}</th>
                                <th>{{ trans('field.activation.status') }}</th>
                                <th>{{ trans('field.activation.itype') }}</th>
                                <th>{{ trans('field.activation.iid') }}</th> 
                                <th>{{ trans('field.activation.bonus_at') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><span id="amount_sum-span">-<span></th>
                                <th><span id="price_sum-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><span id="use_cwallet_sum-span">-<span></th>
                                <th><span id="get_dwallet_sum-span">-<span></th>
                                <th><span id="get_swallet_sum-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

$(document).ready( function () {

    var selected = [];
    var columns = [
        {data: 'created_at' },
        {data: 'id'},
        {data: 'uid', orderable: false }, 
        {data: 'uid-username', orderable: false }, 
        {data: 'uid-name', orderable: false }, 
        {data: 'uid-nid', orderable: false }, 
        {data: 'uid-country', orderable: false }, 
        {data: 'code' }, 
        {data: 'amount' }, 
        {data: 'price' }, 
        {data: 'aid', orderable: false }, 
        {data: 'aid-username', orderable: false }, 
        {data: 'aid-name', orderable: false }, 
        {data: 'use_cwallet' }, 
        {data: 'get_dwallet' }, 
        {data: 'get_swallet' }, 
        {data: 'ref-username', orderable: false }, 
        {data: 'act_type' }, 
        {data: 'status' }, 
        {data: 'itype', orderable: false }, 
        {data: 'iid-text', orderable: false }, 
        {data: 'bonus_at' }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'tf'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/activations/list/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                $("#amount_sum-span").html(json.grand_total.amount);
                $("#price_sum-span").html(json.grand_total.price);
                $("#use_cwallet_sum-span").html(json.grand_total.use_cwallet);
                $("#get_dwallet_sum-span").html(json.grand_total.get_dwallet);
                $("#get_swallet_sum-span").html(json.grand_total.get_swallet);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            },  
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [8, 9, 13, 14, 15] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });

    //set order before draw
    oTable.order([
        [(oTable.column( 'created_at:name' ).index()), "desc" ],
        [(oTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

});
</script>
@endsection 