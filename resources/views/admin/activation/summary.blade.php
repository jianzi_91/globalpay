@extends('admin.layouts.app')
@section('title', trans("general.page.admin.sales-summary.bar-title.sales-summary"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.sales-summary.bar-title.sales-summary") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="main-table-div">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans("general.table.header.package") }}</th>
                                    <th>{{ trans("general.page.admin.sales-summary.content.last_sales_date") }}</th>
                                    <th>{{ trans("general.page.admin.sales-summary.content.no_of_sales") }}</th>
                                    <th>{{ trans("general.wallet.awallet") }}</th>
                                    <th>{{ trans("general.wallet.rwallet") }}</th>
                                    <th>{{ trans("general.wallet.fwallet") }}</th>
                                    <th>{{ trans("general.wallet.dowallet") }}</th>
                                    <th>{{ trans("general.wallet.wallet1") }}</th>
                                    <th>{{ trans("general.wallet.wallet2") }}</th>
                                    <th>{{ trans("general.page.admin.sales-summary.content.total_amount") }}</th>
                                    <th>{{ trans("general.page.admin.sales-summary.content.total_price") }}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right">{{ trans("general.table.footer.total") }}:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="sub-table-div" style="display:none;">
                        <table id="sub-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans('field.activation.created_at') }}</th>
                                    <th>{{ trans('field.activation.id') }}</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.user.name') }}</th>
                                    <th>{{ trans('field.user.nid') }}</th>
                                    <th>{{ trans('field.user.country') }}</th>
                                    <th>{{ trans('field.activation.code-name') }}</th>
                                    <th>{{ trans('field.activation.amount') }}</th>
                                    <th>{{ trans('field.activation.price') }}</th>
                                    <th>{{ trans('field.activation.aid') }}</th>
                                    <th>{{ trans('field.activation.aid-username') }}</th>
                                    <th>{{ trans('field.activation.aid-name') }}</th>
                                    <th>{{ trans('general.wallet.awallet') }}</th>
                                    <th>{{ trans('general.wallet.rwallet') }}</th>
                                    <th>{{ trans('general.wallet.fwallet') }}</th>
                                    <th>{{ trans('general.wallet.dowallet') }}</th>
                                    <th>{{ trans('general.wallet.dwallet') }}</th>
                                    <th>{{ trans('general.wallet.wallet1') }}</th>
                                    <th>{{ trans('general.wallet.wallet2') }}</th>
                                    <th>{{ trans('field.activation.act_type') }}</th>
                                    <th>{{ trans('field.activation.status') }}</th>
                                    <th>{{ trans('field.activation.itype') }}</th>
                                    <th>{{ trans('field.activation.iid') }}</th>
                                    <th>{{ trans('field.activation.bonus_at') }}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><span id="amount_sum-span">-<span></th>
                                    <th><span id="price_sum-span">-<span></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><span id="use_awallet_sum-span">-<span></th>
                                    <th><span id="use_rwallet_sum-span">-<span></th>
                                    <th><span id="use_fwallet_sum-span">-<span></th>
                                    <th><span id="use_dowallet_sum-span">-<span></th>
                                    <th><span id="get_dwallet_sum-span">-<span></th>
                                    <th><span id="get_wallet1_sum-span">-<span></th>
                                    <th><span id="get_wallet2_sum-span">-<span></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'code', orderable: false },
        {data: 'bonus_at_last', orderable: false },
        {data: 'act_count', orderable: false },
        {data: 'use_awallet_sum', orderable: false },
        {data: 'use_rwallet_sum', orderable: false },
        {data: 'use_fwallet_sum', orderable: false },
        {data: 'use_dowallet_sum', orderable: false },
        {data: 'get_wallet1_sum', orderable: false },
        {data: 'get_wallet2_sum', orderable: false },
        {data: 'amount_sum', orderable: false },
        {data: 'price_sum', orderable: false }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bl<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/activations-summary/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            { className: "text-right", "targets": [2, 3, 4, 5, 6, 7, 8, 9, 10] },
        ],
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "order": [],
        "deferLoading": 0, // prevent initial loading
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();
            var numberWithCommas = function (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,%]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
            
            var footer_columns = [2, 3, 4, 5, 6, 7, 8, 9, 10]; 
            var total = [];
            for (var j=0; j<footer_columns.length; j++ ) {
                if (api.column(footer_columns[j]).data().length) {
                    total[j] = api
                    .column( footer_columns[j] )
                    .data()
                    .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                    } ) }
                else { total[j] = 0}

                if (footer_columns[j] == 2) {
                    total[j] = numberWithCommas(parseFloat(total[j]).toFixed(0));
                }
                else {
                    total[j] = numberWithCommas(parseFloat(total[j]).toFixed(2));
                }
            }

            for (var j=0; j<footer_columns.length; j++ ) {
                // Update footer
                $( api.column(footer_columns[j]).footer() ).html(
                    total[j]
                );
            }
        },
    });
    
    // wallet table
    var columns = [
        {data: 'created_at' },
        {data: 'id'},
        {data: 'uid', orderable: false }, 
        {data: 'uid-username', orderable: false }, 
        {data: 'uid-name', orderable: false }, 
        {data: 'uid-nid', orderable: false }, 
        {data: 'uid-country', orderable: false }, 
        {data: 'code' }, 
        {data: 'amount' }, 
        {data: 'price' }, 
        {data: 'aid', orderable: false }, 
        {data: 'aid-username', orderable: false }, 
        {data: 'aid-name', orderable: false }, 
        {data: 'use_awallet' }, 
        {data: 'use_rwallet' }, 
        {data: 'use_fwallet' }, 
        {data: 'use_dowallet' }, 
        {data: 'get_dwallet' }, 
        {data: 'get_wallet1' }, 
        {data: 'get_wallet2' }, 
        {data: 'act_type' }, 
        {data: 'status' }, 
        {data: 'itype', orderable: false }, 
        {data: 'iid-text', orderable: false }, 
        {data: 'bonus_at' }
    ];
    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });
    var sTable = $('#sub-table').DataTable({
        "dom": 'Bli<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/activations-summary/sub-datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                $("#amount_sum-span").html(json.grand_total.amount);
                $("#price_sum-span").html(json.grand_total.price);
                $("#use_rwallet_sum-span").html(json.grand_total.use_rwallet);
                $("#use_awallet_sum-span").html(json.grand_total.use_awallet);
                $("#use_fwallet_sum-span").html(json.grand_total.use_fwallet);
                $("#use_dowallet_sum-span").html(json.grand_total.use_dowallet);
                $("#get_dwallet_sum-span").html(json.grand_total.get_dwallet);
                $("#get_wallet1_sum-span").html(json.grand_total.get_wallet1);
                $("#get_wallet2_sum-span").html(json.grand_total.get_wallet2);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "columnDefs": [
            { className: "text-right", "targets": [8, 9, 10, 13, 14, 15, 16, 17, 18, 19] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
 
    $('#main-table tbody, #sub-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        
        if ($("select[name=code]").val() == "") {
            $('#main-table-div').css("display", "block")
            $('#sub-table-div').css("display", "none")
            oTable.draw();
            location.href = "#main-table";
        }
        else {
            $('#main-table-div').css("display", "none")
            $('#sub-table-div').css("display", "block")
            sTable.draw();
            location.href = "#sub-table";
        }
        e.preventDefault();
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 