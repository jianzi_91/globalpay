@extends('admin.layouts.app')
@section('title', 'Check Group')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>Check Group</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url(config('app.admin_slug').'/home') }}">Home</a>
				</li>
				<li class="active">
					<a href="{{ url(config('app.admin_slug').'/networks/check-group') }}">Check Group</a>
				</li>
			</ol>
		</div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('admin.common.success')
			@include('admin.common.errors')
		</div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Check Group
                </div>

                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
                      
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username 1</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="Username 1" class="form-control" name='username1' value='{{ old('username1') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='username1'>{{ trans('general.button.check') }}</button>
                                <span id='username1-text'></span>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username 2</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="Username 2" class="form-control" name='username2' value='{{ old('username2') }}'>
                                <button class="btn btn-sm btn-primary" type="button" for='username2'>{{ trans('general.button.check') }}</button>
                                <span id='username2-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=username2],button[for=username1]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection