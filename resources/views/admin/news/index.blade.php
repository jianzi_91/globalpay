@extends('admin.layouts.app')
@section('title', trans("general.page.admin.news.bar-title.news"))

@section('content')
    <div class="container">
        @include('admin.common.success')
        @include('admin.common.errors')
        <h1>{{ trans("general.page.admin.news.bar-title.news") }}</h1>
        <a role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="search_form">@lang("general.button.toggle_search_form")</a>

        <form class="form-horizontal collapse" role="form" id='search_form' method="GET">
            <div class="form-group">
                <div class="form-group">
                    <label for="title_en" class="col-md-2 control-label">{{ trans("field.news.title") }}</label>
                    <div class="col-md-6">
                        <input type="text" name="title_en" class="form-control" value="{{ $search->has('title_en') ? $search->title_en : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title_zh_cn" class="col-md-2 control-label">{{ trans("field.news.title_zh_cn") }}</label>
                    <div class="col-md-6">
                        <input type="text" name="title_zh_cn" class="form-control" value="{{ $search->has('title_zh_cn') ? $search->title_zh_cn : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="content_en" class="col-md-2 control-label">{{ trans("field.news.content") }}</label>
                    <div class="col-md-6">
                        <input type="text" name="content_en" class="form-control" value="{{ $search->has('content_en') ? $search->content_en : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="content_zh_cn" class="col-md-2 control-label">{{ trans("field.news.content_zh_cn") }}</label>
                    <div class="col-md-6">
                        <input type="text" name="content_zh_cn" class="form-control" value="{{ $search->has('content_zh_cn') ? $search->content_zh_cn : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-md-2 control-label">{{ trans("field.news.status") }}</label>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="published"><input type="checkbox" name="status[published]" value="1" @if($search->has('status.published')) checked="checked" @endif>{{ trans("general.news.status.1") }}</label>
                        </div>
                        <div class="checkbox">
                            <label for="not_published"><input type="checkbox" name="status[not_published]" value="0" @if($search->has('status.not_published')) checked="checked" @endif>{{ trans("general.news.status.0") }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-6">
                        <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                    </div>
                </div>
            </div>
        </form>

        <hr>
        <div>
            {{ $news->total() }} {{ trans('general.table.listing.record') }}
            @if(!empty($search->all()))
                <a href='{{ $search->url() }}'>{{ trans('general.table.listing.refresh') }}</a>
            @endif
            <a href="{{ url(config('app.admin_slug').'/news/create') }}" class="btn btn-primary btn-xs">{{ trans('general.button.add-new') }}</a>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{ trans("field.news.title") }} /<br>{{ trans("field.news.title_zh_cn") }}</th>
                    <th>{{ trans("field.news.created_at") }}</th>
                    <th>{{ trans("field.news.updated_at") }}</th>
                    <th>{{ trans("field.news.status") }}</th>
                    <th>{{ trans("general.table.header.action") }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($news as $item)
                <tr>
                    <td>
                        {{ $item->title_en }}<br>
                        {{ $item->title_zh_cn }}
                    </td>
                    <td>{{ $item->created_at }}</td>
                    <td>{{ $item->updated_at }}</td>
                    <td>
                        @if ($item->status)
                            <span class="label label-primary">{{ trans("general.news.status.1") }}</span>
                        @else
                            <span class="label label-default">{{ trans("general.news.status.0") }}</span>
                        @endif
                    </td>
                    <td><a href="{{ url(config('app.admin_slug').'/news/'.$item->id.'/edit') }}"><i class="fa fa-pencil"></i> {{ trans("general.button.edit") }}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $news->links() }}
    </div>
@endsection

@section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>

    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.datepicker[type=text]').datepicker({
                dateFormat: 'yy-mm-dd',
            });

        });
    </script>
@endsection