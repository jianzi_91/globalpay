@extends('admin.layouts.app')
@section('title', 'News')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit News: {{ $item->title_en }}</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('admin.common.errors')
                <div class="panel panel-default">
                    <div class="panel-heading">Edit News</div>
                    <div class="panel-body">

                        <form class="form-horizontal" action="{{ url(config('app.admin_slug').'/news', $item->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="pop_up" value="1" />

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="status">Start Date</label>
                                <div class="col-md-7">
                                    <input type="text" id="start-date" name="start_date" class="form-control datepicker" value="{{ $item->start_date }}" placeholder="Start Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="status">End Date</label>
                                <div class="col-md-7">
                                    <input type="text" id="end-date" name="end_date" class="form-control datepicker" value="{{ is_null($item->end_date) ? '' : $item->end_date }}" placeholder="End Date">
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <ul class="nav nav-tabs">
                                        <li role="presentation" class="active"><a data-toggle="tab" href="#en">English</a>
                                        </li>
                                        <li role="presentation"><a data-toggle="tab" href="#cn">中文</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div style="padding-top: 25px;">

                                <div class="tab-content">

                                    <!-- en tab -->
                                    <div id="en" class="tab-pane fade in active">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="title_en">Title</label>
                                            <div class="col-md-7">
                                                <input type="text" name="title_en" class="form-control" value="{{ $item->title_en }}" placeholder="Title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="description_en">Description</label>
                                            <div class="col-md-7">
                                                <input type="text" name="description_en" class="form-control" value="{{ $item->descr_en }}" placeholder="Description">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="content_en">Content</label>
                                            <div class="col-md-7">
                                                <textarea name="content_en" id="content_en" cols="30" rows="10" class="form-control">{{ $item->content_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end en tab

                                    <!-- cn tab -->
                                    <div id="cn" class="tab-pane fade">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="title_cn">Title</label>
                                            <div class="col-md-7">
                                                <input type="text" name="title_cn" class="form-control" value="{{ $item->title_zh_cn }}" placeholder="Title (cn)">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="description_cn">Description</label>
                                            <div class="col-md-7">
                                                <input type="text" name="description_cn" class="form-control" value="{{ $item->descr_zh_cn }}" placeholder="Description (cn)">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="content_cn">Content</label>
                                            <div class="col-md-7">
                                                <textarea name="content_cn" id="content_cn" cols="30" rows="10" class="form-control">{{ $item->content_zh_cn }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end cn tab -->
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="status">Pop Up</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="pop_up">
                                                <option value="1" @if ($item->pop_up) selected @endif>Yes</option>
                                                <option value="0" @if (!$item->pop_up) selected @endif>No</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="status">Status</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="status">
                                                <option value="1" @if($item->status) selected @endif>Publish</option>
                                                <option value="0" @if(!$item->status) selected @endif>Not Publish</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">{{ trans('general.button.edit') }}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('content_en');
        CKEDITOR.replace('content_cn');
    </script>
    
    <script>
        $(document).ready(function () {
            $('.footable').footable();

            $('#start-date').datepicker({
                dateFormat: 'yy-mm-dd',
            }).on('change', function (e) {
                $('#end-date').datepicker( "option", "minDate", $(e.currentTarget).datepicker("getDate"));
            });

            $('#end-date').datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: $('#start-date').datepicker("getDate")
            });
        });
    </script>
@endsection