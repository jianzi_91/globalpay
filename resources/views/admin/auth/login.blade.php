<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ trans('system.company') }} {{ trans('system.admin') }}: @yield('title')</title>
    <link rel="shortcut icon" href="/assets/images/favicon.png">
    <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/default/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/default/font-loto.css">
    <link rel="stylesheet" href="/plugin/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.min.css">
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/layout.css?v5">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <div class="page-wrap">
        <nav class="navbar navbar-default navbar-admin navbar-main navbar-static-top" style="margin-bottom:5px;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <a href="javascript:void(0)" class="sidebar-toggle">
                        <i class="fa fa-bars"></i>
                        <span>{{ trans('general.menu.user.menu') }}</span>
                    </a>

                    <!-- Branding Image -->
                    <a class="navbar-brand auth" href="{{ url(config('app.admin_slug').'/') }}">
                        <img class="logo" src="/assets/images/gpay.png" alt="@lang('system.company')" />
                        <label>@lang('system.admin')</label>
                    </a>
                </div>
                <div class="sidebar-overlay"></div>
                <div id="sidebar">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right public">
                        <li class="dropdown pull-right">
                            <a class="current-open dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">{{trans('system.language')}}</a>
                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
                                <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="admin-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">{{ trans('general.page.admin.login.bar-title.login') }}</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url(config('app.admin_slug').'/login') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label for="username" class="col-md-4 control-label">{{ trans('field.admin.username') }}</label>

                                        <div class="col-md-6">
                                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus=true>

                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">{{ trans('field.admin.password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('_captcha') ? ' has-error' : '' }}">
                                        <label for="_captcha" class="col-md-4 control-label">{{ trans('general.field.captcha') }}</label>

                                        <div class="col-md-6">
                                            <input id="_captcha" type="text" class="form-control" name="_captcha" autocomplete="off">
                                            <div class="captcha">
                                                <img src="{{ url('captcha', rand()) }}" id='captchaimg'>
                                                <span>
                                                    {!! trans('general.page.public.login.content.captcha-helper', array('link'=>"<a href='javascript: refreshCaptcha();'>", 'end_link'=>"</a>")) !!}
                                                </span>
                                            </div>
                                            
                                            @if ($errors->has('_captcha'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('_captcha') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-sign-in"></i> {{ trans('general.button.login') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer site-footer">
        <div class="container">
            © {{ config('app.start_year') }} @if (config('app.start_year') != date('Y')) - {{ date('Y') }} @endif {{ trans('system.company') }}. {{ trans('system.allrights') }}.
        </div>
    </footer>

    <!-- JavaScripts -->
    <script src="/default/jquery.min.js"></script>
    <script src="/plugin/moment/moment.js"></script>
    <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="/plugin/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="/plugin/datatables/js/buttons.html5.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/admin.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
	<script>
        autoLogout('login', {{ config('session.lifetime') }});
        $(function() {
            setLocale('{{ url("setlocale") }}');
        });
	</script>
</body>
</html>