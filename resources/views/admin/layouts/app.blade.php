<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ trans('system.company') }} {{ trans('system.admin') }}: @yield('title')</title>
    <link rel="shortcut icon" href="/assets/images/favicon.png">
    <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/default/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/default/font-loto.css">
    <link rel="stylesheet" href="/plugin/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.min.css">
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/plugin/pace/pace_minimal.css">
    <link rel="stylesheet" type="text/css" media="all" href="/assets/css/jquery-ui.css" />
    <link rel="stylesheet" href="/assets/css/layout.css?v5">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    @yield('style')
</head>
<body id="app-layout">
    <div class="page-wrap">
         <nav class="navbar navbar-inverse navbar-static-top navbar-top hidden-xs">
            <div class="container">
                <div class="collapse navbar-collapse top-bar">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                       @if (Auth::guard('admins')->user())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::guard('admins')->user()->username }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url(config('app.admin_slug').'/profile') }}"><i class="fa fa-btn fa-user"></i>@lang("general.menu.admin.profile")</a></li>
                                <li>
                                    <a href="{{ url(config('app.admin_slug').'/logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        <i class="fa fa-btn fa-sign-out"></i>@lang("general.menu.admin.logout")</a>
                                    </a>

                                    <form id="logout-form" action="{{ url(config('app.admin_slug').'/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">{{trans('system.language')}}<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#" class="locale" data-lang="en">English</a></li>
                                <li><a href="#" class="locale" data-lang="zh-cn">中文</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-default navbar-admin navbar-main navbar-static-top" style="margin-bottom:5px;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <a href="javascript:void(0)" class="sidebar-toggle">
                        <i class="fa fa-bars"></i>
                        <span>{{ trans('general.menu.user.menu') }}</span>
                    </a>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url(config('app.admin_slug').'/') }}">
                        <img class="logo" src="/assets/images/gpay.png" alt="@lang('system.company')" />
                        <label>@lang('system.admin')</label>
                    </a>
                </div>
                <div class="sidebar-overlay"></div>

                <!-- Sidebar -->
                <div id="sidebar">
                    @if (Auth::guard('admins')->user())
                    <ul class="nav navbar-nav" style="width:100%">
                        <li><a href="{{ url(config('app.admin_slug').'/home') }}">@lang("general.menu.admin.home")</a></li>

                        @if (Auth::guard('admins')->user()->can("admin-privilege", "members/list|members/edit-passwords|genealogy/placement|uplines|networks/check-group".
                        "|members/edit-network"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.member.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "members/list"))
                                <li><a href="{{ url(config('app.admin_slug').'/members') }}">@lang("general.menu.admin.member.listing")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "members/edit-passwords"))
                                <li><a href="{{ url(config('app.admin_slug').'/members/passwords') }}">@lang("general.menu.admin.member.passwords_listing")</a></li>
                                @endif
                                <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "members/sa1-list"))
                                <li><a href="{{ url(config('app.admin_slug').'/members/sa1-list') }}">@lang("general.menu.admin.member.sa1_listing")</a></li>
                                @endif -->
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "genealogy/sponsor|genealogy/placement|uplines|networks/check-group".
                                "|members/edit-network|members/edit-franchise"))
                                <li>
                                    <a href="#" class="secondary dropdown-toggle" data-toggle="dropdown">@lang("general.menu.admin.member.genealogy.title")</a>
                                    <ul class="secondary dropdown-menu">
                                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "genealogy/sponsor"))
                                        <li><a href="{{ url(config('app.admin_slug').'/sponsor-genealogy') }}">@lang("general.menu.admin.member.genealogy.sponsor_tree")</a></li>
                                        @endif -->
                                        @if (Auth::guard('admins')->user()->can("admin-privilege", "genealogy/placement"))
                                        <li><a href="{{ url(config('app.admin_slug').'/placement-genealogy') }}">@lang("general.menu.admin.member.genealogy.placement_tree")</a></li>
                                        @endif
                                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "uplines"))
                                        <li><a href="{{ url(config('app.admin_slug').'/uplines') }}">@lang("general.menu.admin.member.genealogy.upline_listing")</a></li>
                                        @endif -->
                                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "networks/check-group"))
                                        <li><a href="{{ url(config('app.admin_slug').'/networks/check-group') }}">@lang("general.menu.admin.member.genealogy.network_check")</a></li>
                                        @endif -->
                                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "members/edit-network"))
                                        <li><a href="{{ url(config('app.admin_slug').'/members/network-list') }}">@lang("general.menu.admin.member.genealogy.network_listing")</a></li>
                                        @endif -->
                                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "members/edit-franchise"))
                                        <li><a href="{{ url(config('app.admin_slug').'/members/franchise-list') }}">@lang("general.menu.admin.member.genealogy.franchise_listing")</a></li>
                                        @endif -->
                                    </ul>
                                </li>
                                <li><a href="{{ url(config('app.admin_slug').'/members/transaction-history') }}">@lang('field.user.member_transaction_history')</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif

                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "activations/list|activations/cancel-list".
                        "|personal-activations|group-activations|activations-summary|members/create|members/send-welcome-email".
                        "|loan-activations|loan-activations/settlement|loan/activations/add|bvfree/activations/add|special/activations/add".
                        "|small-group-activations-summary"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.sales.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "activations/list"))
                                <li><a href="{{ url(config('app.admin_slug').'/activations/list') }}">@lang("general.menu.admin.sales.listing")</a></li>
                                @endif
                                
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "activations/cancel-list"))
                                <li><a href="{{ url(config('app.admin_slug').'/activations/cancel-list') }}">@lang("general.menu.admin.sales.cancellation")</a></li>
                                @endif
                                
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "personal-activations"))
                                <li><a href="{{ url(config('app.admin_slug').'/personal-activations') }}">@lang("general.menu.admin.sales.personal_listing")</a></li>
                                @endif

                                @if (Auth::guard('admins')->user()->can("admin-privilege", "group-activations"))
                                <li><a href="{{ url(config('app.admin_slug').'/group-activations') }}">@lang("general.menu.admin.sales.group_listing")</a></li>
                                @endif

                                @if (Auth::guard('admins')->user()->can("admin-privilege", "activations-summary"))
                                <li><a href="{{ url(config('app.admin_slug').'/activations-summary') }}">@lang("general.menu.admin.sales.summary")</a></li>
                                @endif

                                @if (Auth::guard('admins')->user()->can("admin-privilege", "small-group-activations-summary"))
                                <li><a href="{{ url(config('app.admin_slug').'/small-group-activations-summary') }}">{{ trans("general.menu.admin.sales.small_group_activations_summary") }}</a></li>
                                @endif
                                <li class='divider'></li>
                                
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "members/create"))
                                <li><a href="{{ url(config('app.admin_slug').'/members/create') }}">@lang("general.menu.admin.sales.add_free_member")</a></li>
                                @endif

                                @if (Auth::guard('admins')->user()->can("admin-privilege", "members/send-welcome-email"))
                                <li><a href="{{ url(config('app.admin_slug').'/members/send-welcome-email') }}">@lang("general.menu.admin.sales.send_welcome_email")</a></li>
                                @endif

                                <li class='divider'></li>
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "loan-activations"))
                                <li><a href="{{ url(config('app.admin_slug').'/loan-activations') }}">@lang("general.menu.admin.sales.loan_accounts_listing")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "loan-activations/settlement"))
                                <li><a href="{{ url(config('app.admin_slug').'/loan-activations/settlement') }}">@lang("general.menu.admin.sales.early_settlement")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "loan/activations/add"))
                                <li><a href="{{ url(config('app.admin_slug').'/loan/activations/add') }}">@lang("general.menu.admin.sales.add_loan_activation")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "bvfree/activations/add"))
                                <li><a href="{{ url(config('app.admin_slug').'/bvfree/activations/add') }}">@lang("general.menu.admin.sales.add_zero_bv_activation")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "special/activations/add"))
                                <li><a href="{{ url(config('app.admin_slug').'/special/activations/add') }}">@lang("general.menu.admin.sales.add_special_package")</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif -->

                        @if (Auth::guard("admins")->user()->can("admin-privilege", "wallets/edit".
                        "|wallet-records|report/member-wallet"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.wallet.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard("admins")->user()->can("admin-privilege", "wallets/edit".
                                "|wallets/edit/type/debit"))
                                <li><a href="{{ url(config('app.admin_slug').'/wallets/edit') }}">@lang("general.menu.admin.wallet.edit_member_wallet")</a></li>
                                @endif
                                <li class='divider'></li>
                                @if (Auth::guard("admins")->user()->can("admin-privilege", "wallet-records"))
                                <li><a href="{{ url(config('app.admin_slug').'/wallet-records') }}">@lang("general.menu.admin.wallet.history")</a></li>
                                <li><a href="{{ url(config('app.admin_slug').'/wallet-topups') }}">@lang("general.menu.admin.wallet.topup", ["wallet" => trans("general.wallet.rwallet")])</a></li>
                                @endif
                                @if (Auth::guard("admins")->user()->can("admin-privilege", "report/member-wallet"))
                                <li><a href="{{ url(config('app.admin_slug').'/report/member-wallet') }}">@lang("general.menu.admin.wallet.summary")</a></li>
                                @endif
                                <li class="divider"></li>
                                <li><a href="{{ url(config('app.admin_slug').'/wallet-manualwithdrawal') }}">@lang("general.menu.admin.wallet.manualwithdrawal")</a></li>
                            </ul>
                        </li>
                        @endif

                        @if (Auth::guard("admins")->user()->can("admin-privilege", "commissions|commissions-summary"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.bonus.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard("admins")->user()->can("admin-privilege", "commissions"))
                                <li><a href="{{ url(config('app.admin_slug').'/commissions') }}">@lang("general.menu.admin.bonus.listing")</a></li>
                                @endif
                                @if (Auth::guard("admins")->user()->can("admin-privilege", "commissions-summary"))
                                <li><a href="{{ url(config('app.admin_slug').'/commissions-summary') }}">@lang("general.menu.admin.bonus.summary")</a></li>
                                @endif
                                <!-- @if (Auth::guard("admins")->user()->can("admin-privilege", "commissions-analysis"))
                                <li><a href="{{ url(config('app.admin_slug').'/commissions-analysis') }}">@lang("general.menu.admin.bonus.analysis")</a></li>
                                @endif -->
                            </ul>
                        </li>
                        @endif

                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "withdrawals"))
                        <li><a href="{{ url(config('app.admin_slug').'/withdrawals') }}">@lang("general.menu.admin.withdrawal.title")</a></li>
                        @endif -->
                        
                        @if (Auth::guard('admins')->user()->can("admin-privilege", "news"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.news.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="{{ url(config('app.admin_slug').'/news') }}">@lang("general.menu.admin.news.listing")</a></li>
                                <li class='divider'></li>
                                <li><a href="{{ url(config('app.admin_slug').'/news/create') }}">@lang("general.menu.admin.news.add_news")</a></li>
                            </ul>
                        </li>
                        @endif
                        
                        <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "exchange"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.exchange.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="{{ url(config('app.admin_slug').'/exchange') }}">@lang("general.menu.admin.exchange.listing")</a></li>
                                <li><a href="{{ url(config('app.admin_slug').'/exchange/history') }}">@lang("general.menu.admin.exchange.history")</a></li>
                                <li class='divider'></li>
                                <li><a href="{{ url(config('app.admin_slug').'/exchange/create') }}">@lang("general.menu.admin.exchange.add_exchange")</a></li>
                            </ul>
                        </li>
                        @endif -->
                        
                        @if (Auth::guard('admins')->user()->can("admin-privilege", "trade/history/buy|trade/history/queue".
                        "|trade/history/sell"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.trade.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "trade/history/sell"))
                                <li><a href="{{ url(config('app.admin_slug').'/trade/history/sell') }}">@lang('general.menu.admin.trade.pending_sell_listing')</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "trade/history/queue"))
                                <li><a href="{{ url(config('app.admin_slug').'/trade/history/queue') }}">@lang('general.menu.admin.trade.queue_listing')</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "trade/history/buy"))
                                <li><a href="{{ url(config('app.admin_slug').'/trade/history/buy') }}">@lang('general.menu.admin.trade.buy_listing')</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif

                        @if (Auth::guard('admins')->user()->can("admin-privilege", "report/member-country"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.report.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "report/member-country"))
                                <li><a href="{{ url(config('app.admin_slug').'/report/member-country') }}">@lang("general.menu.admin.report.member_overview")</a></li>
                                @endif
                                <!-- @if (Auth::guard('admins')->user()->can("admin-privilege", "report/trade-allocation"))
                                <li><a href="{{ url(config('app.admin_slug').'/report/trade-allocation') }}">@lang("general.menu.admin.report.trade_overview")</a></li>
                                @endif
                                @if (Auth::guard('admins')->user()->can("admin-privilege", "report/sales-commission"))
                                <li><a href="{{ url(config('app.admin_slug').'/report/sales-commission') }}">@lang("general.menu.admin.report.sales_and_bonus_overview")</a></li>
                                @endif -->
                            </ul>
                        </li>
                        @endif
                        
                        @if (Auth::guard('admins')->user()->can("admin-privilege", "loggers"))
                        <li><a href="{{ url(config('app.admin_slug').'/loggers') }}">@lang("general.menu.admin.log_history")</a></li>
                        @endif
                        
                        @if (Auth::guard('admins')->user()->can("admin-privilege", "is-master"))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">@lang("general.menu.admin.settings.title")</a>
                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="{{ url(config('app.admin_slug').'/settings') }}">@lang("general.menu.admin.settings.general")</a></li>
                                <li><a href="{{ url(config('app.admin_slug').'/settings/active_bonus') }}">@lang("general.menu.admin.settings.active_bonus")</a></li>
                                <li><a href="{{ url(config('app.admin_slug').'/settings/passive_bonus') }}">@lang("general.menu.admin.settings.passive_bonus")</a></li>
                            </ul>
                        </li>
                        @endif
                        
                        @if (Auth::guard('admins')->user()->can("admin-privilege", "adminusers"))
                        <li><a href="{{ url(config('app.admin_slug').'/adminusers') }}">@lang("general.menu.admin.admin")</a></li>
                        @endif

                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guard('admins')->user())
                        <li class="dropdown visible-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::guard('admins')->user()->username }}
                            </a>

                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="{{ url(config('app.admin_slug').'/profile') }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ url(config('app.admin_slug').'/logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        <i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                    </a>

                                    <form id="logout-form" action="{{ url(config('app.admin_slug').'/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="dropdown visible-xs">
                        @if (Auth::guard('admins')->user()) visible-xs @endif">
                            <a class="current-open dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">{{trans('system.language')}}</a>
                            <ul class="first dropdown-menu" role="menu">
                                <li><a href="#" class="locale" data-lang="en">English</a></li>
                                <li><a href="#" class="locale" data-lang="zh-cn">中文</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="admin-container">
        @yield('content')
        </div>
    </div>

    <footer class="footer site-footer">
        <div class="container">
            © {{ config('app.start_year') }} @if (config('app.start_year') != date('Y')) - {{ date('Y') }} @endif {{ trans('system.company') }}. {{ trans('system.allrights') }}.
        </div>
    </footer>

    <!-- JavaScripts -->
    <script src="/default/jquery.min.js"></script>
    <script src="/plugin/moment/moment.js"></script>
    <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="/plugin/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="/plugin/datatables/js/buttons.html5.js"></script>
    <script src="/plugin/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="/plugin/pace/pace.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    @if (app()->getLocale() && app()->getLocale() != 'en')
        <script src="/assets/js/i18n/datepicker-{{app()->getLocale()}}.js"></script>
    @endif
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/admin.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
	<script>
        $(function() {
            setLocale('{{ url("setlocale") }}');
        });
	</script>
    @yield('script')
    @include('partials.general-script') 
</body>
</html>
