@extends('admin.layouts.app')
@section('title', trans("general.page.admin.bonus-summary.bar-title.bonus-summary"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.bonus-summary.bar-title.bonus-summary") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <div id="main-table-div">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans("general.table.header.bonus-type") }}</th>
                                    <th>{{ trans("general.page.admin.bonus-summary.content.last_bonus_date") }}</th>
                                    <th>{{ trans("general.page.admin.bonus-summary.content.no_of_bonus") }}</th>
                                    <th>{{ trans("general.page.admin.bonus-summary.content.total_amount") }}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right">{{ trans("general.table.footer.total") }}:</th>
                                    <th></th>
                                    <th><span id="comm_count-span">-<span></th>
                                    <th><span id="comm_amount_sum-span">-<span></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div id="sub-table-div" style="display:none;">
                        <table id="sub-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                                <tr>
                                    <th>{{ trans('field.commission.id') }}</th>
                                    <th>{{ trans('field.user.id') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.user.name') }}</th>
                                    <th>{{ trans('field.user.nid') }}</th>
                                    <th>{{ trans('field.user.country') }}</th>
                                    <th>{{ trans('general.table.header.amount') }}</th>
                                    <th>{{ trans('field.commission.percent') }}</th>
                                    <th>{{ trans('field.commission.type') }}</th>
                                    <th>{{ trans('field.commission.bdate') }}</th>
                                    <th>{{ trans('field.commission.status') }}</th>
                                    <th>{{ trans('field.commission.created_at') }}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><span id="amount_sum-span">-<span></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {

    var selected = [];
    var columns = [
        {data: 'type', orderable: false },
        {data: 'comm_bdate_last', orderable: false },
        {data: 'comm_count', className:"text-right", orderable: false },
        {data: 'comm_amount_sum', className:"text-right", orderable: false },
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bl<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'pr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/commissions-summary/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                $("#comm_count-span").html(json.grand_total.comm_count);
                $("#comm_amount_sum-span").html(json.grand_total.comm_amount_sum);
                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "paging": false,
        "pageLength": -1,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "order": [],
        "deferLoading": 0, // prevent initial loading
    });
    
    // sub table
    var columns = [
        {data: 'id'},
        {data: 'uid' }, 
        {data: 'uid-username', orderable: false }, 
        {data: 'uid-name', orderable: false }, 
        {data: 'uid-nid', orderable: false }, 
        {data: 'uid-country', orderable: false }, 
        {data: 'amount', className:"text-right" }, 
        {data: 'percent', className:"text-right", orderable: false }, 
        {data: 'type' }, 
        {data: 'bdate' }, 
        {data: 'status' }, 
        {data: 'created_at' },
    ];
    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });
    var sTable = $('#sub-table').DataTable({
        "dom": 'Bli<"clear-both">'+'<"table-responsive"'+'tf'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/commissions-summary/sub-datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                $("#amount_sum-span").html(json.grand_total.amount);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
    
    sTable.order([
        [(sTable.column( 'id:name' ).index()), "desc" ]
    ]);
 
    $('#main-table tbody, #sub-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {    
            
        if ($("select[name=type]").val() == "") {
            $('#main-table-div').css("display", "block");
            $('#sub-table-div').css("display", "none");
            oTable.draw();
            location.href = "#main-table";
        }
        else {
            $('#main-table-div').css("display", "none");
            $('#sub-table-div').css("display", "block");
            sTable.draw();
            location.href = "#sub-table";
        }
        e.preventDefault();
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
@endsection 