@extends('admin.layouts.app')
@section('title', trans("general.page.admin.bonus-analysis.bar-title.bonus-analysis"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.bonus-analysis.bar-title.bonus-analysis") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.last_bonus_date") }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.user.name') }}</th>
                                <th>{{ trans('field.user.nid') }}</th>
                                <th>{{ trans('field.user.country') }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_bonus") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_bonus_payout_total_sales") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_roi") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_investment_amount") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_pending_withdrawal") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.total_paid_withdrawal") }}</th>
                                <th>{{ trans("general.page.admin.bonus-analysis.content.account_creation_date") }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><span id="comm_amount_sum-span">-<span></th>
                                <th><span id="comm_amount_sum_percent-span">-<span></th>
                                <th><span id="comm_roi_amount_sum-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready( function () {

    var selected = [];
    var columns = [
        {data: 'comm_bdate_last' }, 
        {data: 'user_id' }, 
        {data: 'user_username', orderable: false }, 
        {data: 'user_name', orderable: false }, 
        {data: 'user_nid', orderable: false }, 
        {data: 'user_country', orderable: false },
        {data: 'comm_amount_sum' },
        {data: 'comm_amount_sum-percent', orderable: false },
        {data: 'comm_roi_amount_sum' },
        {data: 'act_amount-sum', orderable: false },
        {data: 'withdrawal_pending_amount-sum', orderable: false },
        {data: 'withdrawal_paid_amount-sum', orderable: false },
        {data: 'user_created_at' }
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/commissions-analysis/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                $("#comm_amount_sum-span").html(json.grand_total.comm_amount_sum);
                $("#comm_amount_sum_percent-span").html(json.grand_total.comm_amount_sum_percent);
                $("#comm_roi_amount_sum-span").html(json.grand_total.comm_roi_amount_sum);

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [
            {
                extend: 'excel',
                exportOptions: {
                    columns: ":visible thead th:not(.noExport)",
                },
                "footer": true,
            },
            {
                extend: 'colvis',
                text: '@lang("general.button.column_visibility")',
                postfixButtons: [{
                    extend: 'colvisRestore',
                    text: '@lang("general.button.restore_visibility")',
                }]
            }
        ],
        "columnDefs": [
            { className: "text-right", "targets": [6, 7, 8, 9, 10, 11] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

});
</script>
@endsection 