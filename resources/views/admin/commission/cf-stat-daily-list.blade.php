@extends('admin.layouts.app')
@section('title', trans("general.page.admin.pairing-list.bar-title.pairing-list"))

@section('content')
<div class="container">
    <h2>{{ trans("general.page.admin.pairing-list.bar-title.pairing-list") }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('admin.common.errors')
                    @include('admin.common.success')

                    @include('admin.partials.search-form')

                    <table id="main-table" class="table table-striped table-bordered datatable-admin">
                        <thead>
                            <tr>
                                <th>{{ trans('field.member_cf_stat_daily.id') }}</th>
                                <th>{{ trans('field.user.id') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.paired_amt') }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.leg1_cf') }} : {{ trans("general.table.helper.before") }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.leg2_cf') }} : {{ trans("general.table.helper.before") }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.leg1_cf') }} : {{ trans("general.table.helper.after") }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.leg2_cf') }} : {{ trans("general.table.helper.after") }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.paired_max') }}</th>
                                <th>{{ trans('field.member_cf_stat_daily.tdate') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-right">@lang("general.table.footer.grand_total"):</th>
                                <th></th>
                                <th></th>
                                <th><span id="paired_amt-span">-<span></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready( function () {

    var selected = [];
    var columns = [
        {data: 'id' }, 
        {data: 'uid', orderable: false }, 
        {data: 'uid-username', orderable: false }, 
        {data: 'paired_amt' }, 
        {data: 'leg1_cf-before', orderable: false }, 
        {data: 'leg2_cf-before', orderable: false }, 
        {data: 'leg1_cf-after', orderable: false }, 
        {data: 'leg2_cf-after', orderable: false }, 
        {data: 'paired_max' }, 
        {data: 'tdate' },
    ];

    //append data as name to each column
    $.each(columns, function(key, value) {
        columns[key]["name"] = value["data"];
    });

    var oTable = $('#main-table').DataTable({
        "dom": 'Bli'+'<"table-responsive"'+'ft'+'>'+'ipr',
        "processing": true,
        "serverSide": true,
        "ajax":  {
            "type": "GET",
            "url": "{{ url(config('app.admin_slug').'/pairing-history/datatable') }}",
            "data": function (d) {
                var dataArray = $("#search-form").serializeArray();
                for (var $i = 0; $i<dataArray.length; $i++) {
                    if (dataArray[$i]["name"].match(/\[\]$/)) {
                        if (!d.hasOwnProperty(dataArray[$i]["name"])) {
                           d[dataArray[$i]["name"]] = [];
                        }
                        d[dataArray[$i]["name"]].push(dataArray[$i]["value"]);
                    }
                    else {
                        d[dataArray[$i]["name"]] = dataArray[$i]["value"];
                    }
                }
            },
            "dataSrc" : function (json) {
                // manipulate your data (json)
                
                try {
                    $("#paired_amt-span").html(json.grand_total.paired_amt);
                }
                catch(err) {
                    console.log(err.message);
                }

                // return the data that DataTables is to use to draw the table
                return json.data;
            },
            "error": function(response) {
                var ok = confirm("{{ trans('ajax.errmsg.return_error') }}");
                if (ok) {
                    location.reload();
                }
            }
        },
        "columns": columns,
        "language": {
            url: "{{ trans('system.lang-datatables') }}"
        },
        "buttons": [{
            extend: 'colvis',
            text: '@lang("general.button.column_visibility")',
            postfixButtons: [{
                extend: 'colvisRestore',
                text: '@lang("general.button.restore_visibility")',
            }]
        }],
        columnDefs: [
            { className: "text-right", "targets": [3, 4, 5, 6, 7, 8] }
        ],
        "paging": true,
        "pageLength": 10,
        "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
        "searching": false,
        "info": true,
        "autoWidth": false,
        "stateSave": true, //to save columns visibility
        "scrollX": false, // this will set header column width
        "deferLoading": 0, // prevent initial loading
    });
 
    $('#main-table tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );

    //set order before draw
    oTable.order([
        [(oTable.column( 'tdate:name' ).index()), "desc" ]
    ]);
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
        location.href = "#main-table";
    });

    // date picker
    $('.datepicker[type=text]').datepicker({
        dateFormat: 'yy-mm-dd',
    });

});
</script>
@endsection 