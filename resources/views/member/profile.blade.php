@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.profile')))

@section('content')
<div class="page-title">{{ trans('general.page.user.profile.bar-title.profile') }}</div>

    <div class="my-profile">
        <div class="avatar">
            <img src="/assets/images/default_avatar.jpg">
        </div>
        <div class="profile-content">
            @if(Auth::user()->sub_id == 0)
            <div class="username">{{ Auth::user()->mobile.'('. Auth::user()->username .')' }}</div>
            @else
            <div class="username">{{ Auth::user()->mobile.'-'.Auth::user()->sub_id.'('. Auth::user()->username .')' }}</div>
            @endif
            <p>{{ Auth::user()->email }}</p>
        </div>
        <a href="#profile-function" class="page-scroll edit">
            <i class="icon-edit"></i>
            {{ trans('general.page.user.profile.content.edit_profile') }}
        </a>
    </div>

    <div class="line-seperate"></div>

    <ul class="profile-function" id="profile-function">
        <li class="list active">
            <a data-toggle="pill" href="#general_info">
                <i class="icon-general-info"></i>
                {{ trans('general.page.user.profile.bar-title.general-info') }}
            </a>
        </li>
        <li class="list">
            <a data-toggle="pill" href="#downline_info">
                <i class="icon-beneficiary"></i>
                {{ trans('general.page.user.profile.bar-title.downline-info') }}
            </a>
        </li>
        {{--<li class="list">
            <a data-toggle="pill" href="#beneficiary_info">
                <i class="icon-beneficiary"></i>
                {{ trans('general.page.user.profile.bar-title.beneficiary-info') }}
            </a>
        </li>--}}
        <li class="list">
            <a data-toggle="pill" href="#bank_info">
                <i class="icon-bank"></i>
                {{ trans('general.page.user.profile.bar-title.bank-info') }}
            </a>
        </li>
        <li class="list">
            <a data-toggle="pill" href="#login_password">
                <i class="icon-login-password"></i>
                {{ trans('general.page.user.profile.bar-title.login-password') }}
            </a>
        </li>
        <li class="list">
            <a data-toggle="pill" href="#secure_password">
                <i class="icon-secure-password"></i>
                {{ trans('general.page.user.profile.bar-title.password2') }}
            </a>
        </li>
        <!-- <li class="list">
            <a data-toggle="pill" href="#account_setting">
                <i class="icon-setting"></i>
                {{ trans('general.page.user.profile.bar-title.account-setting') }}
            </a>
        </li> -->
    </ul>

    <div class="general-container">

        @include('member.common.success')
        @include('member.common.errors')
        
        <div class="tab-content">

            <!-- ####### GENERAL INFO ###### -->
            <div id="general_info" class="tab-pane fade in active">
                <div class="form-filled">
                    <div class="form-group">
                        <label>{{ trans('field.user.username') }}</label>
                        @if($user->sub_id == 0)
                        <div class="form-content">{{ $user->mobile }}</div>
                        @else
                        <div class="form-content">{{ $user->mobile.'-'.$user->sub_id }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.mobile') }}</label>
                        <div class="form-content">{{ $user->mobile }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.unique-identifier') }}</label>
                        <div class="form-content">{{ $user->username }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.name') }}</label>
                        <div class="form-content">{{ $user->name }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.nid') }}</label>
                        <div class="form-content">{{ $user->nid }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.country') }}</label>
                        <div class="form-content">{{ isset($__fields['country']['options'][$user->country])?$__fields['country']['options'][$user->country]:'' }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.user_rank') }}</label>
                        <div class="form-content">{{ $user_rank }}</div>
                    </div>
                </div>

                @if($main)
                <div class="form-filled">
                    <div class="form-group">
                        <label>{{ trans('field.user.main-acc') }}</label>
                        <div class="form-content">{{ $main->username }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.sub-acc-id') }}</label>
                        <div class="form-content">{{ str_pad($user->sub_id, 2, 0, STR_PAD_LEFT) }}</div>
                    </div>
                </div>
                @endif

                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='1'>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.address') }}</label>
                                <div class="multiple-row">
                                    <input type="text" placeholder="{{ trans('field.user.address1') }}" class="form-control" name='address1' value='{{empty(old('address1'))?$user->address1:old('address1')}}'>
                                    <input type="text" placeholder="{{ trans('field.user.address2') }}" class="form-control" name='address2' value='{{empty(old('address2'))?$user->address2:old('address2')}}'>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.city') }}</label>
                                <input type="text" class="form-control" name='city' value='{{empty(old('city'))?$user->city:old('city')}}'>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.zip') }}</label>
                                <input type="text" class="form-control" name='zip' value='{{empty(old('zip'))?$user->zip:old('zip')}}'>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.state') }}</label>
                                <input type="text" class="form-control" name='state' value='{{empty(old('state'))?$user->state:old('state')}}'>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ trans('field.user.email') }} <span style="color:red;">*</span></label>
                                <input type="text" placeholder="{{ trans('field.user.email') }}" class="form-control" name='email' value='{{empty(old('email'))?$user->email:old('email')}}'>
                            </div>
                        </div>
                        {{--  <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.mobile') }} <span style="color:red;">*</span></label>
                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='mobile' value='{{empty(old('mobile'))?$user->mobile:old('mobile')}}'>
                            </div>
                        </div>     --}}
                        <div class="col-sm-12">                    
                            <div class="form-group">
                                <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div>
            <!-- ####### GENERAL INFO END ###### -->

            <!-- ####### DOWNLINE INFO ###### -->
            <div id="downline_info" class="tab-pane fade">
                <div class="styled-heading">伞下用户</div>
                <div class="downline-list">
                    <div class="list">
                        <div class="title">第一代家庭成员</div>
                        <ol>
                            @foreach ($user->MemberRelationship as $downline)
                            @if ($downline->level_diff == 1)
                            <li>********{{substr($downline->downline_info->mobile, -4)}}</li>
                            @endif
                            @endforeach
                            {{-- <li>********1234</li>
                            <li>********8856</li>
                            <li>********2179</li> --}}
                        </ol>
                    </div>
            
                    <div class="list">
                    <div class="title">第二代家庭成员</div> <span>{{$downline_2ndGen_Total}}人</span>
                    </div>
            
                    <div class="list">
                        <div class="title">伞下家庭成员</div> <span>{{$downline_3rdGen_Total}}人</span>
                    </div>
                </div>
            </div>
            <!-- ####### DOWNLINE INFO END ###### -->

            {{--<!-- ####### BENEFICIARY INFO ###### -->
            <div id="beneficiary_info" class="tab-pane fade">
                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='5'>

                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.beneficiary_name') }}</label>
                        <input type="text" class="form-control" name='beneficiary_name' value='{{empty(old('beneficiary_name'))?$user->beneficiary_name:old('beneficiary_name')}}'>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.beneficiary_nid') }}</label>
                        <input type="text" class="form-control" name='beneficiary_nid' value='{{empty(old('beneficiary_nid'))?$user->beneficiary_nid:old('beneficiary_nid')}}'>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.beneficiary_user_relationship') }}</label>
                        <input type="text" class="form-control" name='beneficiary_user_relationship' value='{{empty(old('beneficiary_user_relationship'))?$user->beneficiary_user_relationship:old('beneficiary_user_relationship')}}'>
                    </div>                       
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='epassword' value=''>
                    </div>
                    
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div>
            <!-- ####### BENEFICIARY INFO END ###### -->--}}


            <!-- ####### BANK INFO ###### -->
            <div id="bank_info" class="tab-pane fade">
                <div class="form-filled">
                    <div class="form-group">
                        <label>{{ trans('field.user.bank_country') }}</label>
                        <div class="form-content">{{ isset($__fields['country']['options'][$user->country])?$__fields['country']['options'][$user->country]:'' }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('field.user.bank_payee_name') }}</label>
                        <div class="form-content">{{ $user->name }}</div>
                    </div>
                </div>

                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='4'>

                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.bank_name') }} <span style="color:red;">*</span></label>
                        @if (count($__fields["bank_name"]["options"]))
                        <select class="form-control" id="bank_name-select">
                            <option value="">{{ trans("general.selection.please-select") }}</option>
                            @foreach ($__fields["bank_name"]["options"] as $_bank_name)
                                <option value="{{ $_bank_name }}" {{old('bank_name', $user->bank_name) == $_bank_name ? "selected": "" }}>{{ $_bank_name }}</option>
                            @endforeach
                            <option value="-others-" {{ !in_array(old('bank_name', $user->bank_name), $__fields["bank_name"]["options"]) && old('bank_name', $user->bank_name) != "" ? "selected" : "" }}>-- {{ trans("general.selection.others") }}, {{ trans("general.selection.please-specify") }} --</option>
                        </select>
                        @endif
                        <input type="text" class="form-control" name='bank_name' value='{{ old('bank_name', $user->bank_name) }}'>
                    </div>

                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.bank_branch_name') }} <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name='bank_branch_name' value='{{empty(old('bank_branch_name'))?$user->bank_branch_name:old('bank_branch_name')}}'>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.bank_acc_no') }} <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name='bank_acc_no' value='{{empty(old('bank_acc_no'))?$user->bank_acc_no:old('bank_acc_no')}}'>
                    </div>

                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.bank_sorting_code') }}</label>
                        <input type="text" class="form-control" name='bank_sorting_code' value='{{empty(old('bank_sorting_code'))?$user->bank_sorting_code:old('bank_sorting_code')}}'>
                        <span class="help-block m-b-none">{{ trans('general.page.user.profile.content.bank_sorting_code-helper') }}</span>
                    </div>

                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.bank_iban') }}</label>
                        <input type="text" class="form-control" name='bank_iban' value='{{empty(old('bank_iban'))?$user->bank_iban:old('bank_iban')}}'>
                        <span class="help-block m-b-none">{{ trans('general.page.user.profile.content.bank_iban-helper') }}</span>
                    </div>                       
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='epassword' value=''>
                    </div>
                    
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div>
            <!-- ####### BANK INFO END ###### -->

            <!-- ####### LOGIN PASSWORD ###### -->
            <div id="login_password" class="tab-pane fade">
                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='2'>
                    
                    <div class="form-group">
                        <label class="control-label">{{ trans('general.page.user.profile.content.password2', array('field'=> trans('field.user.password'))) }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='password' value=''>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('general.page.user.profile.content.password2', array('field'=> trans('field.user.password_confirmation'))) }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='password_confirmation' value=''>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='epassword' value=''>
                    </div>
                    
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div>
            <!-- ####### LOGIN PASSWORD END ###### -->

            
            <!-- ####### SECURITY PASSWORD ###### -->
            <div id="secure_password" class="tab-pane fade">
                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='3'>
                    
                    <div class="form-group">
                        <label class="control-label">{{ trans('general.page.user.profile.content.password2', array('field'=> trans('field.user.password2'))) }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='new_password2' value=''>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('general.page.user.profile.content.password2', array('field'=> trans('field.user.password2_confirmation'))) }} <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" name='new_password2_confirmation' value=''>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                        <input type="password" name='epassword' class="form-control" value=''>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div>
            <!-- ####### SECURITY PASSWORD END ###### -->
            
            <!-- ####### ACCOUNT SETTING ###### -->
            <!-- <div id="account_setting" class="tab-pane fade">
                <form class="general-form" role="form" action="{{ url('/member/profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type='hidden' name='__req' value='6'>
                    
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.member_status.bonus_wallet_status') }} <span style="color:red;">*</span></label>

                        @foreach ($__fields["bonus_wallet_status"]["options"] as $key=>$value)
                        <label class="radio radio-inline"> 
                            <input type="radio" value="{{ $key }}" name="bonus_wallet_status" {{ old('bonus_wallet_status', $user->memberStatus->bonus_wallet_status) == $key ? 'checked' : ''}} > 
                            <span>{{ trans("general.wallet.".$value) }}</span>
                        </label>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                        <input type="password" name='epassword' class="form-control" value=''>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                    </div>
                </form>
            </div> -->
            <!-- ####### ACCOUNT SETTING END ###### -->

        </div> 
    </div>
        
@endsection

@section('script')
<script type="text/javascript" src="/assets/js/jquery.easing.min.js"></script>
<script>
    $( document ).ready(function () {
        @if (count($__fields["bank_name"]["options"]))
        $("#bank_name-select").on("change load", function(event) {
            var val = $(this).val();
            
            if (val == "-others-") {
                $("input[name=bank_name]").attr("readonly", false);
                if (event.type == "change") {
                    $("input[name=bank_name]").val("").focus();
                }
            }
            else if (val == "") {
                $("input[name=bank_name]").attr("readonly", true);
                $("input[name=bank_name]").val("");
            }
            else {
                $("input[name=bank_name]").attr("readonly", true);
                $("input[name=bank_name]").val(val);
            }
        });
        $("#bank_name-select").load();
        @endif

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection