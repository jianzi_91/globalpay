<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('system.member') }} - {{ trans('system.company') }}: @yield('title')</title>
	<link rel="shortcut icon" href="/assets/images/favicon.png">

    <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="/default/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/plugin/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.min.css">
    <link rel="stylesheet" href="/plugin/datatables/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/plugin/pace/pace_minimal.css">
    <link rel="stylesheet" type="text/css" media="all" href="/assets/css/jquery-ui.css" />
	<link rel="stylesheet" href="/assets/css/globalpay_style.css?v6">
    {{-- <link rel="stylesheet" href="{{ elixir('css/app.css') }}"> --}}
	@yield('style')
</head>
<body>
    <div class="page-wrap">
        <header class="main-header">
            <a href="{{ url('/') }}">
                <img src="/assets/images/gpay.png" class="brand">
            </a>
            @yield('page-title')
            <div class="menu-toggle">
				<span></span>
			</div>
        </header>

        <div class="menu">
            <ul>
                {{--  <li><a href="{{ url('/member') }}">{{ trans('general.menu.user.home') }}</a></li>  --}}
                <li><a href="{{ url('/member/register') }}">{{ trans('general.menu.user.register') }}</a></li>
                <li><a href="{{ url('/member/profile') }}">{{ trans('general.menu.user.profile') }}</a></li>
                @if(auth()->user()->id == config('app.company_main_account_id'))
                <li><a href="{{ url('/member/awallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.awallet')]) }}</a></li>
                @else
                <li><a href="{{ url('/member/dowallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.dowallet')]) }}</a></li>
                <li><a href="{{ url('/member/dowallet/wallet-transfer/repeat') }}">{{ trans("general.menu.user.wallet-transfer-repeat") }}</a></li>
                <li><a href="{{ url('/member/awallet/wallet-transfer/purchase-scwallet') }}">{{ trans("general.menu.user.wallet-transfer-purchase-scwallet") }}</a></li>
                @endif
                <li><a href="{{ url('/member/withdrawal') }}">{{ trans('general.menu.user.wallet-withdraw') }}</a></li>
                <li><a href="{{ url('/member/transaction-history') }}">{{ trans('general.menu.user.transaction-history') }}</a></li>
                {{-- <li class="dropdown"> 
                    <a href="javascript:void" data-toggle="dropdown">{{ trans("general.menu.user.wallet-transfer-title") }} <b class="caret"></b></a> 
                    <ul class="dropdown-menu"> 
                        @if (auth("web")->user()->memberStatus->alliance_status == 10) 
                            <li><a href="{{ url('/member/fwallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.fwallet')]) }}</a></li> 
                        @endif 

                         <li><a href="{{ url('/member/rwallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.rwallet')]) }}</a></li>  
                        <li><a href="{{ url('/member/awallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.awallet')]) }}</a></li> 
                         <li><a href="{{ url('/member/cwallet/wallet-transfer') }}">{{ trans("general.menu.user.wallet-transfer", ["wallet"=>trans('general.wallet.cwallet')]) }}</a></li>  
                    </ul> 
                </li>  --}}
                {{-- <li class="dropdown"> 
                    <a href="javascript:void" data-toggle="dropdown">{{ trans("general.menu.user.wallet-history-title") }} <b class="caret"></b></a> 
                    <ul class="dropdown-menu "> 
                        <li><a href="{{ url('/member/cwallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.cwallet')]) }}</a></li> 

                        @if (auth("web")->user()->memberStatus->alliance_status == 10) 
                            <li><a href="{{ url('/member/fwallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.fwallet')]) }}</a></li> 
                        @endif 
                        <li><a href="{{ url('/member/rwallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.rwallet')]) }}</a></li> 
                        <li><a href="{{ url('/member/awallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.awallet')]) }}</a></li> 
                        <li><a href="{{ url('/member/awallet2/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.awallet2')]) }}</a></li> 
                        <li><a href="{{ url('/member/swallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.swallet')]) }}</a></li> 
                        <li><a href="{{ url('/member/dowallet/wallet-history') }}">{{ trans("general.menu.user.wallet-history", ["wallet"=>trans('general.wallet.dowallet')]) }}</a></li> 
                    </ul> 
                </li> --}}
                {{--  <li><a href="{{ url('/member/commission-summary') }}">{{ trans('general.menu.user.bonus') }}</a></li>  --}}
                {{--  <li><a href="{{ url('/member/news') }}">{{ trans('general.menu.user.news') }}</a></li>  --}}
                {{--  <li class="dropdown">
                    <a href="javascript:void" data-toggle="dropdown">{{ trans('general.menu.user.trading-lobby') }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/member/trade/buy') }}">{{ trans('general.menu.user.buy-lobby') }}</a></li>
                        <li><a href="{{ url('/member/trade/sell') }}">{{ trans('general.menu.user.sell-lobby') }}</a></li>
                        <li><a href="{{ url('/member/trade/buy/list') }}">{{ trans('general.menu.user.my-list') }}</a></li>
                        <li><a href="{{ url('/member/trade/records') }}">{{ trans('general.menu.user.trade-history') }}</a></li>

                    </ul>
                </li>  --}}
                
                <li class="dropdown">
                    <a href="javascript:void" data-toggle="dropdown">{{ trans('general.menu.user.language') }}<br><small>{{ trans('system.language') }}</small> <b class="caret"></b></a>
                    <ul class="dropdown-menu language">
                        <li><a href="javascript:void(0)" class="locale" data-lang="en">English</a></li>
                        <li><a href="javascript:void(0)" class="locale" data-lang="zh-cn">中文</a></li>
                    </ul>
                </li> 
                <li>
                    <a href="{{ url('/member/logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ trans('general.menu.user.logout') }}</a>

                    <form id="logout-form" action="{{ url('/member/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul> 
        </div>

        <div class="content">
            <div class="wallet-list">
                <div class="styled-heading">{{ trans('general.menu.user.wallets') }}</div>
                <div class="inner">
                    {{-- <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.cwallet') }}</label>
                            <p>{{ $wallets["-cwallet-2d"] }}</p>
                        </div>
                    </div>
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.rwallet') }}</label>
                            <p>{{ $wallets["-rwallet-2d"] }}</p>
                        </div>
                    </div> --}}
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.awallet') }}</label>
                            <p>{{ $wallets["-awallet-2d"] }}</p>
                        </div>
                    </div>
                    {{-- <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.awallet2') }}</label>
                            <p>{{ $wallets["-awallet2-2d"] }}</p>
                        </div>
                    </div> --}}
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.fwallet') }}</label>
                            <p>{{ $wallets["-fwallet-2d"] }}</p>
                        </div>
                    </div>
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.dowallet') }}</label>
                            <p>{{ $wallets["-dowallet-2d"] }}</p>
                        </div>
                    </div>
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.scwallet') }}</label>
                            <p>{{ $wallets["-sc_wallet-2d"] }}</p>
                        </div>
                    </div>
                    @if ($user_type == 3)
                    <div class="list">
                        <div class="list-inner">
                            <img src="/assets/images/icon/ic_wallet.svg" class="wallet-icon">
                            <label>{{ trans('general.wallet.swallet') }}</label>
                            <p>{{ $wallets["-swallet-2d"] }}</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            @yield('submenu')

            <div class="line-seperate"></div>
            
            @yield('content')
        </div>

        <footer class="footer">
            <ul>
                <li>
                    <a href="{{ url('/member') }}">
                        <i class="icon-home"></i>
                        <span>{{ trans('general.menu.user.home') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/member/register') }}">
                        <i class="icon-register"></i>
                        <span>{{ trans('general.menu.user.register') }}</span>
                    </a>
                </li>
                {{-- <li>
                    <a href="{{ url('/member/trade/buy') }}">
                        <i class="icon-trading"></i>
                        <span>{{ trans('general.menu.user.trading-lobby') }}</span>
                    </a>
                </li> --}}
                <li>
                    <a href="{{ url('/member/product_listing_mall') }}">
                        <i class="icon-mall"></i>
                        <span>{{ trans('general.menu.user.mall') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/member/profile') }}">
                        <i class="icon-profile"></i>
                        <span>{{ trans('general.menu.user.profile') }}</span>
                    </a>
                </li>
            </ul>
        </footer>

    </div>
    

    <!-- JavaScripts -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
    <script src="/assets/js/owlcarousel/owl.carousel.js"></script>
    <script src="/plugin/moment/moment.js"></script>
    <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="/plugin/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.min.js"></script>
    <script src="/plugin/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="/plugin/jquery-validate/jquery.validate.min.js"></script>
    <script src="/plugin/jquery-validate/additional-methods.min.js"></script>
    @if ($lang && $lang != 'en')
        <script src="/plugin/jquery-validate/localization/messages_{{$lang}}.min.js"></script>
    @endif
    <script src="/plugin/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="/plugin/pace/pace.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    @if (app()->getLocale() && app()->getLocale() != 'en')
        <script src="/assets/js/i18n/datepicker-{{app()->getLocale()}}.js"></script>
    @endif
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/app.global-pay.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.easing.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
        autoLogout('normal', {{ config('session.lifetime') }})
        $(function() {
            setLocale('{{ url("setlocale") }}');
        });
    </script>
    @yield('script')
    @include('partials.general-script') 
</body>
</html>
