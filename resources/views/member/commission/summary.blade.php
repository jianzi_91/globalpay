@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.commission-summary')))

@section('content')
<div class="page-title">{{ trans('general.page.user.commission-summary.bar-title.commission-summary') }}</div>

<div class='general-container'>
    <div class="styled-heading">{{ trans('general.page.user.commission-summary.bar-title.commission-summary') }}</div>
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp table-reponsible">
             @include('member.common.success')
            <div class="ibox">
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id='search_form' method="GET">
						<input type='hidden' name='__src' value='1'>
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ $__search_fields["bdate_from"]["label"] }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ $__search_fields["bdate_from"]["label"] }}" class="form-control datepicker" name='bdate_from' value="{{ $__search_fields["bdate_from"]["value"] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ $__search_fields["bdate_to"]["label"] }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ $__search_fields["bdate_to"]["label"] }}" class="form-control datepicker" name='bdate_to' value="{{ $__search_fields["bdate_to"]["value"] }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                            </div>
                        </div>
                    </form>
                    
                    <br/>
                    
                    <div>
                        {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                        @if($__searching['yes'])
                            <a href='{{ $__searching['refresh_url'] }}'>{{ trans('general.table.listing.refresh') }}</a>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="footable table table-striped table-bordered" data-page-size="20" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>{{ trans('field.commission.type') }}</th>
                                    <th class="text-right">{{ trans('general.table.header.amount') }}</th>
                                    <th>{{ trans('general.table.header.last-date') }}</th>
                                    <th data-sort-ignore="true"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($__records as $_record)
                                <tr>
                                    <td>{{ trans("general.commission.type.".$_record->type) }}</td>
                                    <td class="text-right">{{ $_record['-sum_amount-ddd'] }}</td>
                                    <td>{{ $_record->max_bdate }}</td>
                                    <td><a class="btn btn-sm btn-primary" href="{{ url("member/".$_record->type."/commission-list").(empty($__query_data)?"":"?").http_build_query($__query_data) }}">{{ trans("general.button.more-detail") }}>></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right">{{ trans('general.table.footer.total') }}</th>
                                    <th class="text-right">{{ $__records_extra['-total_bonus-ddd'] }}</th>
                                    <th colspan=2></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-right">{{ $__records->links() }}</div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });

    });
</script>
@endsection 