@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.commission-list', ['commission'=>trans('general.commission.type.'.$__comm_type)])))

@section('content')
<div class="page-title">{{ trans('general.page.user.commission-list.bar-title.commission-list', ['commission'=>trans('general.commission.type.'.$__comm_type)]) }}</div>

<div class='general-container'>
    <div class="styled-heading">{{ trans('general.page.user.commission-list.bar-title.commission-list', ['commission'=>trans('general.commission.type.'.$__comm_type)]) }}</div>
    <div class="wrapper wrapper-content animated fadeInUp">
            @include('member.common.success')
        <div class="ibox">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" id='search_form' method="GET">
                    <input type='hidden' name='__src' value='1'>
                    <div class="form-group">
                        <label class="col-md-2 control-label">{{ $__search_fields["bdate_from"]["label"] }}</label>
                        <div class="col-md-6">
                            <input type="text" placeholder="{{ $__search_fields["bdate_from"]["label"] }}" class="form-control datepicker" name='bdate_from' value="{{ $__search_fields["bdate_from"]["value"] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">{{ $__search_fields["bdate_to"]["label"] }}</label>
                        <div class="col-md-6">
                            <input type="text" placeholder="{{ $__search_fields["bdate_to"]["label"] }}" class="form-control datepicker" name='bdate_to' value="{{ $__search_fields["bdate_to"]["value"] }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-6">
                            <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                        </div>
                    </div>
                </form>
                
                <br/>
                
                <div>
                    {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                    @if($__searching['yes'])
                        <a href='{{ $__searching['refresh_url'] }}'>{{ trans('general.table.listing.refresh') }}</a>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="footable table table-striped" data-page-size="20" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>{{ trans('general.table.header.date') }}</th>
                                <th>{{ trans('field.user.username') }}</th>
                                <th>{{ trans('general.table.header.amount') }}</th>
                                <th>{{ trans('field.commission.percent') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($__records as $_record)
                            <tr>
                                <td>{{ $_record->bdate }}</td>
                                <td>{{ $_record['uid-username'] }}</td>
                                <td>{{ $_record["-amount-ddd"] }}</td>
                                <td>{{ number_format($_record->percent, 2) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="display:none;">
                            <tr>
                                <td colspan="7">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="text-right">{{ $__records->links() }}</div>
            </div>
            
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection 