@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.sponsor')))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.sponsor.bar-title.sponsor') }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/profile/sponsor') }}">{{ trans('general.menu.user.sponsor') }}</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.sponsor.bar-title.sponsor-info') }}
                </div>
                
                <div class="panel panel-body">
                    @if ($user_ref === 1)
                        <form class="form-horizontal" role="form" action="{{ url('/member/profile/sponsor') }}" method="POST">
                            {{ csrf_field() }}
                            <input type='hidden' name='__req' value='1'>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ trans('field.user.ref') }}</label>
                                <div class="col-md-6">
                                    <input type="text" placeholder="{{ trans('field.user.ref') }}" class="form-control" name='ref' value='{{ old("ref") }}'>
                                    <button class="btn btn-sm btn-primary" type="button" for='ref'>{{ trans('general.button.check') }}</button>
                                    <span id='ref-text'></span>
                                </div>
                            </div>
                       
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                                <div class="col-md-6">
                                    <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-6">
                                    <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.update') }}</button>
                                </div>
                            </div>
                        </form>
                    @else
                        <table class='table table-striped'>
                            <tr>
                                <td>{{ trans('field.user.ref') }}</td>
                                <td>{{ $user_ref }}</td>
                            </tr>
                        </table>
                    @endif
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function () {
        // check username
        $( "button[for=ref]" ).on("click", function() {
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else {
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else {
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
    });
</script>
@endsection