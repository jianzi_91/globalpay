@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.placement-genealogy')))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.user.placement-genealogy.bar-title.placement-genealogy') }}</h2>

<div class='row'>
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            @include('member.common.success')
			@include('member.common.errors')
            <div class="ibox">
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id='search_form' method="GET">
						<input type='hidden' name='__src' value='1'>
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ trans('field.user.mobile') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='uid' value="{{ old('uid') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                            </div>
                        </div>
                    </form>
                    
                    <br/>
                    <br/>

                    <div style="overflow:auto;">

                        <div style="width:{{ $__tree['full_width'] }}px; text-align:center;" class='genealogy-tree'>
                            @for ($_level=1; $_level<count($__user_list)+1; $_level++)
                                
                                {{--Node--}}
                                @for ($_col=1; $_col<count($__user_list[$_level])+1; $_col++)
                                    <div class='node' style="margin-left:{{ ($__tree['full_width']/count($__user_list[$_level])-$__tree['node_width'])/2 * ($_col>=2?2:1) }}px">
                                        @php 
                                            $_user = $__user_list[$_level][$_col]; 
                                            $_register = $_level>1 && $__user_list[$_level-1][ceil($_col/2)]?true:false; 
                                            $__register_info = [];
                                            if ($_register) {
                                                $__register_info['up-username'] = $__user_list[$_level-1][ceil($_col/2)]['user']['username'];
                                                $__register_info['up_pos'] = fmod($_col, 2)?1:2;
                                            }
                                        @endphp
                                        @include('genealogy.placement-node', ['__user'=>$_user, '__first'=>($_level==1?true:false), '__last'=>($_level>1?true:false), '__upgrade'=>true , '__register'=>$_register, '__register_info'=>$__register_info])
                                    </div>
                                @endfor

                                {{--Line--}}
                                @if ($_level < count($__user_list))
                                    <div class='node-clear'></div>

                                    @for ($_col=1; $_col<count($__user_list[$_level])+1; $_col++)
                                        <div class='node' style="margin-left:{{ ($__tree['full_width']/count($__user_list[$_level])-$__tree['node_width'])/2 * ($_col>=2?2:1) }}px">
                                            <div class='vertical-line' {!! $__user_list[$_level][$_col]?'': "style='visibility: hidden;'"!!}></div>
                                        </div>
                                    @endfor

                                    <div class='node-clear'></div>
                                    
                                    @for ($_col=1; $_col<count($__user_list[$_level])+1; $_col++)
                                        <div class='node' style="margin-left:{{ ($__tree['full_width']/count($__user_list[$_level+1])-$__tree['node_width'])/2 * ($_col>=2?2:1) + $__tree['node_width']/2 * ($_col>=2?2:1) - $__tree['line_width']/2 * ($_col>=2?2:1)}}px; width:{{ $__tree['full_width']/count($__user_list[$_level]) - (($__tree['full_width']/count($__user_list[$_level+1])-$__tree['node_width'])/2 + $__tree['node_width']/2)*2 + $__tree['line_width'] }}px">
                                            <div class='horizontal-line' {!! $__user_list[$_level][$_col]?'': "style='visibility: hidden;'"!!}></div>
                                        </div>
                                    @endfor

                                    <div class='node-clear'></div>

                                    @for ($_col=1; $_col<count($__user_list[$_level+1])+1; $_col++)
                                        <div class='node' style="margin-left:{{ ($__tree['full_width']/count($__user_list[$_level+1])-$__tree['node_width'])/2 * ($_col>=2?2:1) }}px">
                                            <div class='vertical-line' {!! $__user_list[$_level][ceil($_col/2)]?'': "style='visibility: hidden;'"!!}></div>
                                        </div>
                                    @endfor

                                    <div class='node-clear'></div>
                                @endif
                            @endfor

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection 

@section('style')
<link rel="stylesheet" href="/plugin/tipped/tipped4.css" type="text/css">
<style>
.genealogy-tree .node{
    width : {{ $__tree['node_width'] }}px;
    float : left;
    padding : 0px;
    text-align : center;
}
.genealogy-tree .node-clear{
    clear : both;
}
.genealogy-tree .vertical-line{
    height : 15px; 
    width : {{ $__tree['line_width'] }}px;
    background-color : cyan;
    margin : 0px auto;
}
.genealogy-tree .horizontal-line{
    height : {{ $__tree['line_width'] }}px; 
    width : 100%;
    background-color : cyan;
}

.genealogy-tree .no-margin{
    margin : 0px;
}

.node-tip{
    color : black;
}
</style>
@endsection 

@section('script')
<script type="text/javascript" src="/plugin/tipped/tipped.js"></script>
<script>
$(document).ready(function() {
    Tipped.create('.inline', {
        close: true,
        shadow: true,
        radius: 2
    });
});
</script>
@endsection