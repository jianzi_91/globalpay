@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.sponsor-genealogy')))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.user.sponsor-genealogy.bar-title.sponsor-genealogy') }}</h2>

<div class='row'>
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            @include('member.common.success')
			@include('member.common.errors')
            <div class="ibox">
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id='search_form' method="GET">
						<input type='hidden' name='__src' value='1'>
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='uid' value="{{ old('uid') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                            </div>
                        </div>
                    </form>
                    
                    <br/>

                    <ul id="tree1" class='tree'>
                        <li class="rank-{{ $__suser["rank"] }}" value='{{ $__suser["username"] }}'>{{ $__suser["username"]}}, {{ trans("general.user.rank.title.".$__suser["rank"]) }} ({{$__suser["created_at_op-date-only"] }})
                            <ul></ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('style')
<style>
.tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#369;
    min-width: 200px;
    font-weight:700;
    position:relative;
    cursor:pointer;
}
.tree li.rank-0 {
    color: #777;
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#fff;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#369;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#369;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
</style>
@endsection 

@section('script')
<script>
$(document).ready(function() {
        
    var openedClass = 'glyphicon-minus-sign';
    var closedClass = 'glyphicon-plus-sign';
    //Initialization of treeviews

    $.fn.extend({
        treed: function (o) {
        
            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                closedClass = o.closedClass;
                }
            };
            
            //initialize each of the top levels
            var tree = $(this);
            
            //fire event from the dynamically added icon
            // tree.find('.branch .indicator').each(function(){
            //     $(this).on('click', function () {
            //         $(this).closest('li').click();
            //     });
            // });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });

    function branchAddEvent(e){
        $(e).prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
        $(e).on('click', function (e) {
            var target = e.target;
            if ($(target).hasClass("indicator")) {
                target = $(target).closest("li")[0];
            }

            if (this == target) {
                var object = target;
                
                if ($(object).find('ul').has('li').length) {
                    //loaded
                    var icon = $(object).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(object).children().children().toggle();
                }
                else {
                    var username = $(object).attr('value');
                    //call ajax
                    $.ajax({
                        type: 'GET',
                        dataType: "json",
                        data: { 'username': username},
                        url: '<?php echo url('sponsor-genealogy/user-info'); ?>',
                        success: function (dataJson) {
                            if (dataJson.status) {
                                var html = "";
                                for (var user_id in dataJson.msg ){
                                    html += "<li value='"+dataJson.msg[user_id].username+"' class='rank-"+dataJson.msg[user_id]["rank"]+"'>"
                                        + dataJson.msg[user_id].username + ", " 
                                        + dataJson.msg[user_id]["rank-title"]
                                        + " ("+dataJson.msg[user_id]["created_at_op-date-only"]+")" 
                                        + (dataJson.msg[user_id].__has?"<ul></ul>":"") 
                                        + "</li>";
                                }

                                if (html == "") {
                                    html += "<li>"+'Empty'+"</li>";
                                }
                                $(object).find('ul:first').html(html);
                                var icon = $(object).children('i:first');
                                icon.toggleClass(openedClass + " " + closedClass);
                                
                                $(object).find('li').has("ul").addClass('branch').each(function(){branchAddEvent(this)});
                            }
                            else{
                                $(object).find('ul:first').html("<li><span class='text-danger'><?php echo trans('ajax.errmsg.fail'); ?></span></li>");
                            }
                        },
                        error: function(data) {
                            var errors = data.responseJSON;
                            $(object).find('ul:first').html("<li><span class='text-danger'><?php echo trans('ajax.errmsg.fail'); ?></span></li>");
                        }
                    });
                }
            }
        });
    }
    
    $('#tree1').find('li').has("ul").addClass('branch').each(function(){branchAddEvent(this)});
    $('#tree1').treed();
    $('#tree1').find('.branch:first').click();
});
</script>
@endsection 