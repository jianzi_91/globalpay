@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.personal-group-purchase')))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.user.personal-group-purchase.bar-title.personal-group-purchase') }}</h2>

<div class='row'>
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
             @include('member.common.success')
            <div class="ibox">
                <div class="ibox-content">
                    
                    <div>
                        {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                    </div>

                    <div class="table-responsive">
                        <table class="footable table table-striped" data-page-size="20" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>{{ trans('general.table.header.date') }}</th>
                                    <th>{{ trans('field.user.username') }}</th>
                                    <th>{{ trans('field.activation.price') }}</th>
                                    <th>{{ trans('field.activation.code-name') }}</th>
                                    <th>{{ trans('field.activation.status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($__records as $_record)
                                <tr>
                                    <td>{{ $_record->created_at_op }}</td>
                                    <td>{{ $_record['uid-username'] }}</td>
                                    <td>{{ number_format($_record->price, 2) }}</td>
                                    <td>{{ trans('general.activation.title.'.$_record->code) }}</td>
                                    <td>{{ trans('general.activation.status.'.$_record->status) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot style="display:none;">
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-right">{{ $__records->links() }}</div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();

    });
</script>
@endsection 