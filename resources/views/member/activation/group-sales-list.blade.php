@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.group-sales-history')))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.user.group-sales-history.bar-title.group-sales-history') }}</h2>

    <div class='row'>
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                @include('member.common.success')
                @include('member.common.errors')
                <div class="ibox">
                    <div class="ibox-content">                
                        <form class="form-horizontal" role="form" id='search_form' method="GET">
                            <input type='hidden' name='__src' value='1'>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{ $__search_fields["uid"]["label"] }}</label>
                                <div class="col-md-6">
                                    <input type="text" placeholder="{{ $__search_fields["uid"]["label"] }}" class="form-control" name='uid' value="{{ $__search_fields["uid"]["value"] }}">
                                    <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                                    <span id='uid-text'></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{ $__search_fields["up_pos"]["label"] }}</label>
                                <div class="col-md-6">
                                    <div class="i-checks">
                                        <label> 
                                            <input type="radio" value="1" name="up_pos" {{ $__search_fields["up_pos"]["value"]=="" || $__search_fields["up_pos"]["value"]=='1'?'checked':''}} > <i></i> {{ trans('general.user.up_pos.1') }} 
                                        </label>
                                    </div>
                                    <div class="i-checks">
                                        <label>
                                            <input type="radio" value="2" name="up_pos" {{ $__search_fields["up_pos"]["value"]=='2'?'checked':''}}> 	<i></i> {{ trans('general.user.up_pos.2') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{ $__search_fields["date_from"]["label"] }}</label>
                                <div class="col-md-6">
                                    <input type="text" placeholder="{{ $__search_fields["date_from"]["label"] }}" class="form-control datepicker" name='date_from' value="{{ $__search_fields["date_from"]["value"] }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{ $__search_fields["date_to"]["label"] }}</label>
                                <div class="col-md-6">
                                    <input type="text" placeholder="{{ $__search_fields["date_to"]["label"] }}" class="form-control datepicker" name='date_to' value="{{ $__search_fields["date_to"]["value"] }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-6">
                                    <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.search') }}</button>
                                </div>
                            </div>
                        </form>
                        
                        <br/>
                        
                        @if($__records)
                        <div>
                            {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                        </div>

                        <div class="table-responsive">
                            <table class="footable table table-striped" data-page-size="20" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>{{ trans('general.table.header.date') }}</th>
                                        <th>{{ trans('field.user.username') }}</th>
                                        <th>{{ trans('field.activation.price') }}</th>
                                        <th>{{ trans('field.activation.code-name') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($__records as $_record)
                                    <tr>
                                        <td>{{ $_record->created_at_op }}</td>
                                        <td>{{ $_record['uid-username'] }}</td>
                                        <td>{{ number_format($_record->price, 2) }}</td>
                                        <td>{{ trans('general.activation.title.'.$_record->code) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot style="display:none;">
                                    <tr>
                                        <td colspan="7">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="text-right">{{ $__records->links() }}</div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });

        // check username
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: '<?php echo url('general/user-info'); ?>',
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + '<?php echo trans('ajax.errmsg.fail'); ?>').addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + '<?php echo trans('ajax.errmsg.username-required'); ?>').addClass('text-danger');
            }
        });
    });
</script>
@endsection 