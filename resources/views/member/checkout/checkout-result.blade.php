@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
    <div class="general-container">
    </div>
    <div class="line-seperate"></div>

    <div class="general-container">
        @if($checkout_status == 'success')
        <div class="transaction-msg success">
            <img class="" src="/assets/images/icon/ic_transaction_success.png">
            <h4>交易完成</h4>
        <p>交易号 ： {{$data['transaction_id']}}</p>
        </div>
        @endif
    
        @if($checkout_status == 'fail')
        <div class="transaction-msg fail">
            <img class="" src="/assets/images/icon/ic_transaction_fail.png">
            <h4>交易失败</h4>
            <p>交易号 : {{$data['transaction_id']}}</p>
        </div>
        @endif
    </div>

    <div class="line-seperate"></div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection