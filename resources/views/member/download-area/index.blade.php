@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.download-area')))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ trans('general.page.user.download-area.bar-title.download-area') }}</h2>
                <hr>
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        {{ trans('general.page.user.download-area.bar-title.download-area') }}
                    </div>
                    <div class="panel panel-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
