@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
    <div class="general-container">
    </div>
    <div class="line-seperate"></div>

    <div class="general-container">
        <div class="styled-heading">{{ trans('general.menu.user.transaction-history') }}</div>
        <div class="transaction-history-action">
        <a href="{{url('member/transaction_history')}}" class="btn btn-primary-outline"><img src="/assets/images/icon/ic_transaction.svg"> {{ trans('general.menu.user.view-my-transaction-history') }}</a>
        </div>
        <hr>

        <div class="styled-heading">{{ trans('general.menu.user.products') }}</div>
        <div class="product-list">
            @foreach($products as $product)
            <div class="product">
                <div class="product-inner">
                <a target="_blank" href="{{url('member/product_details_mall', [ 'id' => $product['id'] ]) }}">
                    <div class="thumb">
                    <img src="{{$product['image']}}">
                    </div>
                    <div class="product-detail">
                        <div class="product-name">{{$product['title']}}</div>
                        <div class="product-price">{{ $product['currency_symbol'] . ' ' . $product['original_price']}}</div>
                    </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="line-seperate"></div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection
