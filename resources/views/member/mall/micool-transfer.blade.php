@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.micool-transfer')))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.micool-transfer.bar-title.wallet-transfer') }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/mall/micool-transfer') }}">{{ trans('general.menu.user.micool-transfer') }}</a>
				</li>
			</ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">

            @if ($__fields['__step']['value'] == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.micool-transfer.bar-title.wallet-transfer-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <td>{{ trans('general.page.user.micool-transfer.content.mall') }}</td><td>{{ trans('partner.micool.name') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.wallet-to') }}</td><td>{{ trans('partner.micool.wallet.vtoken') }}</td>
                        </tr>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td><td>{{ old('to_ref') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.amount') }}</td><td>{{ number_format( (float) old('amount')) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.remarks') }}</td><td>{{ old('remarks') }}</td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="{{ url('/member/mall/micool-transfer') }}" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif ($__fields['__step']['value'] == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.micool-transfer.bar-title.wallet-transfer-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <td>{{ trans('general.page.user.micool-transfer.content.mall') }}</td><td>{{ trans('partner.micool.name') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.wallet-to') }}</td><td>{{ trans('partner.micool.wallet.vtoken') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td><td>{{ old("to_ref") }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.amount') }}</td><td>{{ number_format( (float) old('amount')) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.remarks') }}</td><td>{{ old('remarks') }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" action="{{ url('/member/mall/micool-transfer') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__step' value='confirmed'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        @foreach (['to_ref', 'amount', 'remarks', 'epassword'] as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}
            
            @elseif ($__fields['__step']['value'] == 'start')
            {{-- ######## START ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.micool-transfer.bar-title.wallet-transfer') }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('/member/mall/micool-transfer') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__step' value='preview'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('general.page.user.micool-transfer.content.mall') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ trans('partner.micool.name') }}<p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.wallet-to') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ trans('partner.micool.wallet.vtoken') }}<p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.email') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.email') }}" class="form-control" name='to_ref' value='{{ old('to_ref') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.amount') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.amount') }}" class="form-control" name='amount' value='{{ old('amount') }}'>
                                <span class="help-block m-b-none">
                                {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->swallet, 'wallet'=>trans('general.wallet.swallet')]) }}
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.remarks') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.remarks') }}" class="form-control" name='remarks' value='{{ old('remarks') }}'>
                            </div>
                        </div>

                        <hr/>
						
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END START ########### --}}
            @endif
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //back button
        $("#back-button").click(function(e) {
            $("input[name=__step]").val("start");
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection