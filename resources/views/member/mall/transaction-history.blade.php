@extends('member.layouts.app-nowallet')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')

    <div class="general-container">
        <div class="styled-heading">交易历史</div>

        <div class="transaction-list">
            @foreach($transaction_list as $transaction)
            <div class="list">
            <a href="{{url('member/transaction_details_mall', [ 'id' => $transaction['source_id'] ]) }}">
                <div class="inner">
                <label class="transaction-id">交易号 : {{$transaction->source_id}}</label>
                <div class="product-price">GP {{-$transaction->a_amt}}</div>
                <div class="date">{{$transaction->created_at}}</div>
                </div>
                </a>
            </div>
            @endforeach
            <div class="action">
                <a onclick="history.go(-1);" class="btn btn-default">返回</a>
            </div>
        </div>
    </div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection