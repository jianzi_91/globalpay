@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
    <div class="general-container">
        {{-- <div class="styled-heading">{{ trans('general.page.user.home.bar-title.group_summary') }}</div>
        
        <div class="group-summary">
            <div class="left">
                <div class="panel">
                    <div class="panel-heading">{{ trans("general.user.up_pos.1") }}</div>
                    <div class="panel-body">
                        <div class="placement-list">
                            <div class="list">
                                <label>{{ trans("general.table.header.bv") }}</label>
                                <div class="amount">{{ number_format($__downline_info->left_sum, 2) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="right">
                <div class="panel">
                    <div class="panel-heading">{{ trans("general.user.up_pos.2") }}</div>
                    <div class="panel-body">
                        <div class="placement-list">
                            <div class="list">
                                <label>{{ trans("general.table.header.bv") }}</label>
                                <div class="amount">{{ number_format($__downline_info->right_sum, 2) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="line-seperate"></div>

    <div class="general-container">
        <div class="styled-heading">{{ trans('general.menu.user.news') }}</div>
        <div class="news-list">
            
            @foreach($__news_records as $row)
            <div class="list">
                <div class="list-inner">
                    <div class="second-level">
                        <a href="{{ url('member/news', $row->id) }}"> <div class="news-title">{{ App::isLocale('en') ? $row->title_en : $row->title_zh_cn }}</div></a>
                        <p>{{ App::isLocale('en') ? $row->descr_en : $row->descr_zh_cn }}</p>
                        <div class="date">{{ $row->start_date->format('Y-m-d') }}</div>
                    </div>
                </div>
            </div>
            @endforeach

            @if(count($__news_records) ==0)
                <div class="empty">
                    {{ trans('general.field.no_data') }}
                </div>
            @endif
           
        </div>
        
        <a href="{{ url("member/news") }}" class="more">{{ trans("general.button.more-detail") }}</a>
    </div>

    <div class="line-seperate"></div>

    @if ($__popups)
    <div id="model-pupup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">                            
                    @if (isset($__popups["news"]))
                        <h4>{{ trans('general.menu.user.news') }}</h4>
                        @foreach ($__popups["news"] as $_popup)
                            <p>
                                <strong>{{ $_popup["heading"] }}</strong>
                                <br>
                                {!! nl2br(e($_popup["body"])) !!}
                                @if ($_popup['link'])
                                    &nbsp;&nbsp;<a href="{{ $_popup['link'] }}" class="btn btn-xs btn-default"><i class="fa fa-eye"></i> {{ trans("general.button.read") }}</a>
                                @endif
                            </p>
                        @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans("general.button.close") }}</button>
                </div>
            </div>

        </div>
    </div>
    @endif

@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });

        @if ($__popups)
            $('#model-pupup').modal('show');
        @endif

    });
</script>
@endsection
