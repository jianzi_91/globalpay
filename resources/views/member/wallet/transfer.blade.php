@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.wallet-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)])))

@section('content')
<div class="page-title">{{ trans('general.page.user.wallet-transfer.bar-title.wallet-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</div>

@include('member.common.success')
@include('member.common.errors')

<div class="general-container">
    @if ($__step == 'confirmed')
    {{-- ######## SUCCESS ########## --}}
    <div class="general-container">
        <div class="styled-heading">{{ trans('general.page.user.wallet-transfer.bar-title.wallet-transfer-info') }}</div>

        <div class="form-filled">
            <div class="form-group">
                <label>{{ trans('field.user.receiver-acc') }}</label>
                @if(session("__to_user")->sub_id == 0)
                <div class="form-content">{{ session("__to_user")->mobile }}</div>
                @else
                <div class="form-content">{{ session("__to_user")->mobile.'-'.session("__to_user")->sub_id }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>{{ trans('field.wallet_record.amount') }}</label>
                <div class="form-content">{{ number_format(session("post")['amount'], 2) }}</div>
            </div>
            <div class="form-group">
                <label>{{ trans('field.wallet_record.remarks') }}</label>
                <div class="form-content">{{ session("post")['remarks'] }}</div>
            </div>
        </div>

        <div class="form-action">
            <a href="{{ url('/member/'.$__wallet.'/wallet-transfer') }}" class="btn btn-primary">{{ trans('general.button.next') }}</a>
        </div>
    </div>
        
    {{-- ######## END SUCCESS ########## --}}

    @elseif ($__step == 'preview')
    {{-- ######## PREVIEW ########## --}}
    <div class="general-container">
        <div class="styled-heading">{{ trans('general.page.user.wallet-transfer.bar-title.wallet-transfer-review') }}</div>

        <div class="form-filled">
            {{-- <div class="form-group">
                <label>{{ trans('field.wallet_record.wallet-to') }}</label>
                <div class="form-content">{{ $__fields["wallet_to"]["options"][old('wallet_to')] }}</div>
            </div> --}}
            {{-- @if ($__wallet == "dowallet")
                <div class="form-group">
                    <label>{{ trans('field.user.mobile') }}</label>
                    <div class="form-content">{{ old("wallet_to") != "swallet" ? old('uid') : $__user->mobile }}</div>
                </div>
            @endif --}}
            <div class="form-group">
                <label>{{ trans('field.user.receiver-acc') }}</label>
                @if(old('uid'))
                <div class="form-content">{{ old('uid') }}</div>
                @else
                @if($__user->sub_id == 0)
                <div class="form-content">{{ $__user->mobile }}</div>
                @else
                <div class="form-content">{{ $__user->mobile.'-'.$__user->sub_id }}</div>
                @endif
                @endif
            </div>
            <div class="form-group">
                <label>{{ trans('field.wallet_record.amount') }}</label>
                <div class="form-content">{{ number_format(old('amount'), 2) }}</div>
            </div>
            <div class="form-group">
                <label>{{ trans('field.wallet_record.remarks') }}</label>
                <div class="form-content">{{ old('remarks') }}</div>
            </div>
        </div>
                
        {{--##### Hidden Form ####--}}
        <form class="form-horizontal" role="form" action="{{ url('/member/'.$__wallet.'/wallet-transfer') }}" method="POST">
            {{ csrf_field() }}
            <input type='hidden' name='__req' value='1'>
            <input type='hidden' name='__confirm' value='1'>
            <input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
            <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
            @foreach(["wallet_to", 'uid', 'amount', 'remarks'] as $_field)
                <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
            @endforeach

            <div class="form-group">
                <div class="col-md-12">
                    <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                    <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                </div>
            </div>
        </form>
    </div>
        
    {{-- ######## END PREVIEW ########## --}}
            
    @elseif($__step == 'start')
    {{-- ######## START ########## --}}
    <div class="general-container">
        <div class="styled-heading">{{ trans('general.page.user.wallet-transfer.bar-title.wallet-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</div>

        <form role="form" action="{{ url('/member/'.$__wallet.'/wallet-transfer') }}" method="POST">
            {{ csrf_field() }}
            <input type='hidden' name='__req' value='1'>
            <input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>

            @if($__wallet == 'dowallet')
            <div class="form-group">
                <label class="control-label">{{ trans('field.wallet_record.wallet-to') }} <span style="color:red;">*</span></label>
                <select class="form-control"  name="wallet_to">
                    @foreach($__fields['wallet_to']['options'] AS $_code=>$_to_wallet)
                        <option value='{{ $_code }}' {{ $_code==old("wallet_to")?"selected":"" }}>{{ $_to_wallet }}</option>
                    @endforeach
                </select>
            </div>
            @else($__wallet == 'awallet')
            <div class="form-group">
                <div class="input-container">
                    <input type="hidden" class="form-control" name='wallet_to' value='fwallet'>
                </div>
            </div>
            @endif
            @if($__wallet == 'awallet' || $__wallet == 'dowallet')
            <div class="form-group">
                <label class="control-label">{{ trans('field.user.mobile') }} <span style="color:red;">*</span></label>
                <div class="input-container">
                    <input type="text" class="form-control" name='uid' value='{{old('uid')}}'>
                    <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                </div>
                <span id='uid-text'></span>
            </div>
            @elseif($__wallet == 'dowallet')
            <div class="form-group">
                <div class="input-container">
                    <input type="hidden" class="form-control" name='uid' value='{{ \Auth::user()->mobile }}'>
                </div>
            </div>
            @endif
            <div class="form-group">
                <label class="control-label">{{ trans('field.wallet_record.amount') }} <span style="color:red;">*</span></label>
                <input type="number" min="1" class="form-control" name='amount' value='{{old('amount')}}'>
                <span class="help-block m-b-none">
                {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->$__wallet, 'wallet'=>trans('general.wallet.'.$__wallet)]) }}
                </span>
            </div>
            <div class="form-group">
                <label class="control-label">{{ trans('field.wallet_record.remarks') }} <span style="color:red;">*</span></label>
                <input type="text" class="form-control" name='remarks' value='{{old('remarks')}}'>
            </div>

            <hr/>
                    
            <div class="form-group">
                <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
            </div>

            <div class="form-action">
                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
            </div>
        </form>
    </div>
    {{-- ######## END START ########### --}}
    @endif
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        @if ($__wallet == "dowallet")
        // check username 
        $( "select[name=wallet_to]" ).on("change", function() 
        { 
            $wallet_to = $(this).val(); 
            if ($wallet_to == "swallet" || $wallet_to == "awallet2") { 
                var username =  "{{ $__user->mobile }}"; 
                var sub_account = "{{ $__user->sub_id }}";

                if (sub_account != 0) {
                    username = {{ $__user->mobile }} + '-' + {{ $__user->sub_id }};
                }
                
                $('input[name=uid][type=text]').val(username).attr("disabled", "disabled"); 
            } 
            else{ 
                $('input[name=uid][type=text]').removeAttr("disabled"); 
            } 
            
        }); 
        $( "select[name=wallet_to]" ).change();
        @endif

        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection