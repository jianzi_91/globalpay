@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.wallet-history', array('wallet'=>trans('general.wallet.'.$__wallet)))))

@section('content')
<div class="page-title">{{ trans('general.page.user.wallet-history.bar-title.wallet-history', array('wallet'=>trans('general.wallet.'.$__wallet))) }}</div>
    
<div class="general-container">
    <div class="styled-heading">{{ trans('general.page.user.wallet-history.bar-title.wallet-history', array('wallet'=>trans('general.wallet.'.$__wallet))) }}</div>

    <div class="wrapper wrapper-content animated fadeInUp table-reponsible">
        @include('member.common.success')
        <div class="ibox">
            <div class="ibox-content">
                
                <div>
                    {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                </div>

                <div class="table-responsive">
                    <table class="footable table table-striped" data-page-size="20" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>{{ trans('general.table.header.date') }}</th>
                                <th>{{ trans('general.table.header.amount') }}</th>
                                <th>{{ trans('general.table.header.balance') }}</th>
                                <th>{{ trans('general.table.header.descr') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($__records as $_record)
                            <tr>
                                <td>{{ $_record->created_at_op }}</td>
                                <td><span class="fa fa-{{$_record->type=='c'?'plus-square':'minus-square'}}" style="color:{{$_record->type=='c'?'green':'red'}};"></span> {{ number_format($_record->amount, 4) }}</td>
                                <td>{{ number_format($_record->balance, 4) }}</td>
                                <td>{!! nl2br(e($_record["descr-text"])) !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="display:none;">
                            <tr>
                                <td colspan="7">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="text-right">{{ $__records->links() }}</div>
            </div>
            
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();

    });
</script>
@endsection 