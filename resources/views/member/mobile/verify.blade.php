@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.mobile_verify')))

@section('content')
<div class="page-title">{{ trans('general.page.user.mobile_verify.bar-title.mobile_verify') }}</div>

@include('member.common.success')
@include('member.common.errors')

<div class="general-container">
    <div class="styled-heading">
        {{ trans('general.page.user.mobile_verify.bar-title.mobile_verify') }}
    </div>

    @if ($__user->memberStatus->mobile_status == 10)
        <div class="alert alert-info">
            <p>{{ trans('general.page.user.mobile_verify.content.success-noted') }}</p>
        </div>
    @else
        <div class="alert alert-info">
            <p>{{ trans('general.page.user.mobile_verify.content.noted') }}</p>
        </div>
        <div class="form-filled">
            <div class="form-group">
                <label>{{ trans('field.user.mobile') }}</label>
                <div class="form-content">{{ $__user->mobile }}</div>
            </div>
        </div>

        <form role="form" method="POST">
            {{ csrf_field() }}
            <input type='hidden' name='__req' value='1'>
            
            <div class="form-group">
                <label class="control-label">{{ trans('field.code.verification_code') }}</label>
                <div class="input-container">
                    <input type="text" placeholder="{{ trans('field.code.verification_code') }}" class="form-control" name='verification_code' value='' autocomplete="off">

                    <button id="btn-request_tac" type="button">{{ trans("general.button.request_TAC") }}</button>
                </div>

                <div id="tac_msg"></div>
            </div>

            <hr/ >

            <div class="form-group">
                <label class="control-label">{{ trans('field.user.epassword') }}</label>
                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
            </div>
        
            <div class="form-action">
                <button class="btn btn-primary" type="submit">{{ trans('general.button.update') }}</button>
            </div>
        </form>
    @endif
</div>
@endsection

@section('script')
<script>
    $(function () {

        function countdown(expired_date, server_date)
        {
            var nowx = new Date(server_date).getTime();
            var expiredDate = new Date(expired_date).getTime();
            var dateDiffTime = expiredDate - nowx;
            var fiveMinTime = (5 * 60 * 1000);

            var expired = new Date().getTime() + dateDiffTime;

            if (dateDiffTime > fiveMinTime)
                expired = new Date().getTime() + fiveMinTime;

            var x = setInterval(function() {
                var now = new Date().getTime();
                var distance = expired - now;
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                var message = "{{ trans('sms_tac.content.timer') }}";
                message = message.replace(":minute", minutes);
                message = message.replace(":second", seconds);

                $("#btn-request_tac").html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + message).prop('disabled', true);

                if (distance < 0) {
                    clearInterval(x);
                    $("#btn-request_tac").html("{{ trans('general.button.request_TAC') }}").prop('disabled', false);
                }
            }, 1000);
        }

        $('#btn-request_tac').click(function() {
            var mobile = '{{ $__user->mobile }}';
            var country = '{{ $__user->country }}';
            var lang = '{{ app()->getLocale() }}';

            $.get("/send-tac", {mobile: mobile, country: country, lang: lang, action: 'verify_mobile'})
            .done(function( data ) {
                if (data.status == '0') {
                    $('#tac_msg').removeClass('alert alert-success');
                    $('#tac_msg').addClass('alert alert-danger');

                    $('#tac_msg').text(data.errmsg[0]);
                } else {
                    $('#tac_msg').removeClass('alert alert-danger');
                    $('#tac_msg').addClass('alert alert-success');

                    $('#tac_msg').text(data.message);

                    // Call countdown
                    countdown(data.expired_at, data.server_date);
                }
            });
        });
    });
</script>
@endsection