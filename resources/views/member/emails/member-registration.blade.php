<!DOCTYPE html>
<html>
<head></head>
<body>
    
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="580" style="font-family:Arial,Microsoft Yahei,sans-serif;">
        <tbody>
            <tr background="{{url('assets/images/auth/login_bg.jpg')}}">
                <td align="center" height="70" colspan="3">
                    <img src="{{asset('assets/images/gpay_logo.png')}}" style="background: rgba(255,255,255,0.9); padding: 2% 5% 2% 5%; border-radius: 10px; margin: 2% 0 2% 0">
                </td>
            </tr>
            
            <tr style="background:#f6f6f6;" height="25">
                <td colspan="3"></td>
            </tr>
            
            <tr style="background:#f6f6f6;font-size:13px;" height="25">
                <td width="25"></td>
                <td valign="middle" width="530">
                    <p>@lang('email.register.content.welcome')<a href="{{ url('/') }}" style="color:#000;font-weight:bold;text-decoration:none;">{{ url('/') }}</a></p>
                    <table width="530" style="background-color:#eee;border:1px solid #ddd;font-size: 14px;">
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.username')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $username }}</td>
                        </tr>
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.name')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $name }}</td>
                        </tr>
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.nid')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $nid }}</td>
                        </tr>
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.mobile')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $mobile }}</td>
                        </tr>
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.email')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $email }}</td>
                        </tr>
                        <tr>
                            <td style="padding:6px 10px;border-bottom:1px solid #ddd;">@lang('email.register.content.country')</td>
                            <td style="padding-right:10px;color:#000;border-bottom:1px solid #ddd;" align="right">{{ $country }}</td>
                        </tr>
                    </table>
                    
                    <p>@lang('email.register.regards')</p>

                    <p>@lang('email.register.gpay-cs')</p>
                </td>
                <td width="25"></td>
            </tr>
            <tr style="background:#f6f6f6;" height="25">
                <td colspan="3"></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="580" style="font-family:Arial,sans-serif;">		
            <tr>
                <td colspan="3">
                    <p style="font-size:10px;color:#666;margin:5px 0;font-weight:bold;">Notice</p>
                    <p style="font-size:10px;color:#888;margin:5px 0;">
                        This e-mail is confidential. It may also be legally privileged. If you are not the addressee you may not copy, forward, disclose or use any part of it. If you have received this message in error, please delete it and all copies from your system and notify the sender immediately by return e-mail. 
                    </p>

                    <p style="font-size:10px;color:#888;margin-bottom:5px;">Internet communications cannot be guaranteed to be timely, secure, error or virus-free. The sender does not accept any liability for any errors or omissions. </p>
                    
                    <p style="font-size:10px;color:#666;margin-bottom:5px;font-weight:bold;">Please do not reply to this e-mail. Should you wish to contact us, please send your request to <a href="mailto:info@dongli88.com">info@globalpay100.com</a> and we will respond to you.</p>
                    
                    <p style="font-size:10px;color:#888;margin-bottom:5px;">You are receiving this e-mail because you have signed up as a member of GloabalPay and you have consented to the Company sending you promotional materials/information regarding our products and services. You may, at any time, choose not to receive marketing literature/information about our services. To unsubscribe, please email your request to <a href="mailto:info@globalpay100.com">info@globalpay100.com</a> and we will remove your name and email from our mailing lists.</p>
                    
                    <p style="font-size:10px;color:#888;margin-bottom:5px;">©Copyright GlobalPay {{-- Private Limited --}} 2019<br>All rights reserved. This e-mail is intended for selected GlobalPay customers only.</p>
                    
                    
                    
                    <p style="color:#666;text-align:center;font-size:12px;font-weight:bold;">"SAVE PAPER - THINK BEFORE YOU PRINT!"</p>
                </td>
            </tr>
            <tr>
                <td colspan="3" height="25"></td>
            </tr>
            
        </tbody>
    </table>
    
</body>
</html>