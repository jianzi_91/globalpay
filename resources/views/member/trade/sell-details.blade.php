@extends('member.layouts.app')
@section('title', 'Trade')

@inject('trade', 'App\Trades\TradeService')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy/list') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.sell-details')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">{{ $queue->tradeSell->currencyCode->symbol }}</div>{{ number_format($queue->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content full">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">
                            {{ number_format($trade->coinsAfterCharges($queue->quantity),2) }}
                            <div class="total"><label class="total-label">@lang('general.field.total')</label> {{ $queue->tradeSell->currencyCode->symbol }} {{ $queue->quantity * $queue->price }}</div>
                        </div>
                    </div>
                    <!-- <div class="list-row">
                        <label>付款方式</label>
                        <div class="list-detail">
                            银行转账
                            <div class="bank-name">中国银行<br>2134567890<br>23120391<br>其他资料</div>
                        </div>
                    </div> -->
                    <div class="list-row">
                        <label>@lang('general.page.trade.buyer')</label>
                        <div class="list-detail">
                            {{ $queue->buyer->name }}
                        </div>
                    </div>
                    @if($queue->status == 3)
                    <div class="list-row">
                        <label>@lang('general.page.trade.buyer-contact')</label>
                        <div class="list-detail">
                            +{{ $queue->buyer->countryInfo->phone_code }} {{ $queue->buyer->mobile }}
                        </div>
                    </div>
                    @endif
                    <div class="list-row">
                        <label>@lang('general.page.trade.created_at')</label>
                        <div class="list-detail">
                            {{ $ca_display }}
                        </div>
                    </div>
                    @if($queue->status == 4)
                    <div class="list-row">
                        <label>@lang('general.page.trade.sell_at')</label>
                        <div class="list-detail">
                            {{ $ua_display }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

@if($queue->status == 2)
<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar first-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message">
            <h4>@lang('general.page.trade.payment.pending-sell')</h4>
            <p>@lang('general.page.trade.payment.expiry-hint')</p>
        </div>
    </div>
</div>
@elseif($queue->status == 3)
<div class="line-seperate"></div>

<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar second-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message">
            <h4>@lang('general.page.trade.payment.approval-request')</h4>
            <p>@lang('general.page.trade.payment.approval-hint')</p>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container" style="margin-bottom: 200px;">
            
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>

    <div class="text-center">
        <a class="zoomable" href="{{ $receipt }}" target="_blank">
            <img class="img-responsive" src="{{ $receipt }}">
        </a> 
    </div>
    
    <hr>

    <form action="/member/trade/sell/approve" method="POST">
        {{ csrf_field() }}
        <div class="form-action">
            <input type="hidden" name="queue_id" value="{{ $queue->id }}">
            <button type="submit" class="btn btn-primary">@lang('general.button.confirm')</button>
        </div>
    </form>
</div>
@elseif($queue->status == 4)
<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar third-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message success">
            <h4>@lang('general.page.trade.success.trade')</h4>
        </div>
    </div>
</div>
<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container">
            
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>

    <div class="text-center" >
        <a class="zoomable" href="{{ $receipt }}" target="_blank">
            <img class="img-responsive" src="{{ $receipt }}">
        </a>
    </div>

    <br>

    <div class="form-action text-center">
        <a type="button" class="btn btn-primary" href="{{ route('my-trade-buy') }}">@lang('general.button.back')</a>
    </div>
    
</div>

<!-- RATING POPUP ON COMPLETE PAGE -->
<!-- Can use bootbox, the style is same -->
<div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('general.page.trade.rating')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="rating">
                    <form id="rate-form" method="POST" action="{{ url('/member/trade/buy/rate') }}">
                        {{ csrf_field() }}
                        <h5>@lang('general.page.trade.rate.title')</h5>
                        <p>@lang('general.page.trade.rate.message')</p>
                        
                        <!-- the css set it in right to left, so make it descending order from 5 to 1 -->
                        <div class="star-radio">
                        
                            <input type="radio" class="star-input" name="star" id="star-5" value="5"/>
                            <label class="star star-5" for="star-5"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-4" value="4"/>
                            <label class="star star-4" for="star-4"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-3" value="3"/>
                            <label class="star star-3" for="star-3"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-2" value="2"/>
                            <label class="star star-2" for="star-2"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-1" value="1"/>
                            <label class="star star-1" for="star-1"></label>
                        </div>
                        
                        <input type="hidden" name="trade_id" value="{{ $queue->id }}">

                        <div class="form-action">
                            <button class="btn btn-primary">@lang('general.button.confirm')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('script')
@if($queue->status == 4 && !$rated)
<script>
    $(window).on('load',function(){
        $('#rating').modal('show');
    });
</script>
@endif
@endsection