@extends('member.layouts.app')

@section('title', 'Trade')

@section('style')
<style type="text/css">
.btn-del {
    color: red;
}
</style>
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.my-offers')</div>
@endsection

@section('submenu')
<ul class="top-tabs">
    <li class="list">
        <a href="{{ url('/member/trade/buy/list') }}">@lang('general.pagetitle.user.buy-offer')</a>
    </li>
    <li class="list active">
        <a href="{{ url('/member/trade/sell/list') }}">@lang('general.pagetitle.user.sell-offer')</a>
    </li>
</ul>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    @if($requests->count() == 0)
    <div class="trading-list" style="margin-bottom: 50px;">
        <h3 class="text-center" style="color: grey;">@lang('general.page.trade.my-selling-request')</h3>
        <div class="list empty">
            @lang('general.page.trade.temporary-empty')
        </div>
    </div>
    @endif

    @if($requests->count() > 0)
    <div class="trading-list" style="margin-bottom: 50px;">
        <h3 class="text-center" style="color: grey;">@lang('general.page.trade.my-selling-request')</h3>
        @foreach($requests as $request)
        <div class="list">
            <div class="price red">
                <div class="currency">{{ $request->tradeSell->currencyCode->symbol }}</div>{{ number_format($request->tradeSell->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="quantity">
                <label>@lang('general.field.quantity')</label> {{ number_format($request->quantity,2) }}
            </div>
            @if($request->tradeSell->payment_method == 'B')
            <div class="payment-method bank-transfer">@lang('general.page.trade.payment.bank-transfer')</div>
            @elseif($request->tradeSell->payment_method == 'W')
            <div class="payment-method wechatpay">@lang('general.page.trade.payment.wechatpay')</div>
            @elseif($request->tradeSell->payment_method == 'A')
            <div class="payment-method alipay">@lang('general.page.trade.payment.alipay')</div>
            @endif
            <div class="user">
                <label>@lang('general.page.trade.status')</label>
                <div class="order-status">
                    <div class="buy-sell-status">
                        @if($request->status == 2)
                        <ul class="status-bar first-step">
                        @else
                        <ul class="status-bar second-step">
                        @endif
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_1')</label>
                            </li>
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_2')</label>
                            </li>
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_3')</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="other-action">
                <div class="text-center">
                <a href="/member/trade/sell/details/{{ $request->id }}">@lang('general.button.check')</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif

    <div class="line-seperate"></div>

    @if($customs->count() == 0)
    <div class="trading-list" style="margin-top:50px; margin-bottom: 50px;">
        <h3 class="text-center" style="color: grey;">@lang('general.page.trade.custom-selling')</h3>
        <div class="list empty">
            @lang('general.page.trade.temporary-empty')
        </div>
    </div>
    @endif

    @if($customs->count() > 0)
    <div class="trading-list" style="margin-top:50px; margin-bottom: 50px;">
        <h3 class="text-center" style="color: grey;">@lang('general.page.trade.custom-selling')</h3>
        @foreach($customs as $custom)
        <div class="list">
            <div class="price red">
                <div class="currency">{{ $custom->currencyCode->symbol }}</div>{{ number_format($custom->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="quantity">
                <label>@lang('general.field.quantity')</label> {{ number_format($custom->open_qty,2) }}
            </div>
            @if($custom->payment_method == 'B')
            <div class="payment-method bank-transfer">@lang('general.page.trade.payment.bank-transfer')</div>
            @elseif($custom->payment_method == 'W')
            <div class="payment-method wechatpay">@lang('general.page.trade.payment.wechatpay')</div>
            @elseif($custom->payment_method == 'A')
            <div class="payment-method alipay">@lang('general.page.trade.payment.alipay')</div>
            @endif
            <br>
            <div class="request-table">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center warning">@lang('general.page.trade.stat.pending')</th>
                            <th class="text-center success">@lang('general.page.trade.stat.paid')</th>
                            <th class="text-center info">@lang('general.page.trade.stat.completed')</th>
                            <th class="text-center danger">@lang('general.page.trade.stat.cancelled')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">{{ $custom->pendingRequest->count() }}</td>
                            <td class="text-center">{{ $custom->paidRequest->count() }}</td>
                            <td class="text-center">{{ $custom->completedRequest->count() }}</td>
                            <td class="text-center">{{ $custom->cancelledRequest->count() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="other-action">
                <div class="text-center">
                <a href="/member/trade/sell/{{ $custom->id }}/view">@lang('general.button.check')</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.btn-del').click(function(){

            var sell_id = $(this).attr('data-id');

            var modal = bootbox.confirm({
                title: "{{ trans('general.page.trade.cancel-buy') }}",
                message: "{{ trans('general.page.trade.hint.cancel-buy') }}",
                buttons: {
                    confirm: {
                        label: "{{ trans('general.button.confirm') }}",
                        className: "btn-danger",
                    },
                    cancel: {
                        label: "{{ trans('general.button.cancel') }}",
                        className: "btn-default",
                    },
                },
                callback: function(result) {
                    if (result) {
                        window.location.replace('/member/trade/sell/' + sell_id + '/request/delete');      
                    }
                    else {
                        modal.hide();
                    }
                },
            });
        });
    });
</script>
@endsection