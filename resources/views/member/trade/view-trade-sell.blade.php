@extends('member.layouts.app')
@section('title', 'Trade')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy/list') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.sell-details')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">{{ $tradeSell->currencyCode->symbol }}</div>{{ number_format($tradeSell->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content full">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">
                            {{ number_format($tradeSell->open_qty,2) }}
                        </div>
                    </div>
                    <!-- <div class="list-row">
                        <label>付款方式</label>
                        <div class="list-detail">
                            银行转账
                            <div class="bank-name">中国银行<br>2134567890<br>23120391<br>其他资料</div>
                        </div>
                    </div> -->
                    <div class="list-row">
                        <label>@lang('general.page.trade.seller')</label>
                        <div class="list-detail">
                            {{ $tradeSell->seller->name }}
                        </div>
                    </div>
                    <div class="list-row">
                        <label>@lang('general.page.trade.created_at')</label>
                        <div class="list-detail">
                            {{ $ca_display }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    @if($requests->count() == 0)
    <form action="{{ url('member/trade/sell/delete') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="sell_id" value="{{ $tradeSell->id }}">
        <div class="text-center">
            <button type="submit" class="btn btn-danger">@lang('general.button.delete')</button>
        </div>
    </form>
    @endif
</div>

<div class="line-seperate"></div>


<div class="general-container">
    @if($requests->count() == 0)
    <div class="trading-list" style="margin-bottom: 50px;">
        <div class="list empty">
            @lang('general.page.trade.temporary-empty')
        </div>
    </div>
    @endif

    @foreach($requests as $request)
    <div class="trading-list" style="margin-bottom: 50px;">
        <div class="list">
            <div class="price red">
                <div class="currency">{{ $request->tradeSell->currencyCode->symbol }}</div>{{ number_format($request->tradeSell->price) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="quantity">
                <label>@lang('general.field.quantity')</label> {{ number_format($request->quantity,2) }}
            </div>
            <div class="user">
                <label>@lang('general.page.trade.status')</label>
                <div class="order-status">
                    <div class="buy-sell-status">
                        @if($request->status == 2)
                        <ul class="status-bar first-step">
                        @else
                        <ul class="status-bar second-step">
                        @endif
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_1')</label>
                            </li>
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_2')</label>
                            </li>
                            <li>
                                <span></span>
                                <label>@lang('general.page.trade.payment.step_3')</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="other-action">
                <div class="text-center">
                <a href="/member/trade/sell/details/{{ $request->id }}">@lang('general.button.check')</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection