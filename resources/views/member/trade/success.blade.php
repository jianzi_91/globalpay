@extends('member.layouts.app')
@section('title', 'Trade')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">出售</div>

<div class="alert alert-success">
<strong>交易完毕</strong>
<p>成功售出{{ $trans['total'] }}颗币（其中{{ $trans['maintenance'] }}为平台维护费用及{{ $trans['charity'] }}转去慈善钱包为慈善用途）</p>
</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">{{ $queue->tradeSell->currencyCode->symbol }}</div>{{ $queue->price }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content full">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">
                            {{ $queue->quantity }}
                            <div class="total"><label class="total-label">@lang('general.field.total')</label> {{ $queue->tradeSell->currencyCode->symbol }} {{ $queue->quantity * $queue->price }}</div>
                        </div>
                    </div>
                    <!-- <div class="list-row">
                        <label>付款方式</label>
                        <div class="list-detail">
                            银行转账
                            <div class="bank-name">中国银行<br>2134567890<br>23120391<br>其他资料</div>
                        </div>
                    </div> -->
                    <div class="list-row">
                        <label>@lang('general.page.trade.seller')</label>
                        <div class="list-detail">
                            {{ $queue->seller->name }}
                        </div>
                    </div>
                    <div class="list-row">
                        <label>@lang('general.page.trade.sell_at')</label>
                        <div class="list-detail">
                            {{ date_format($queue->created_at, 'd-m-Y H:i:s') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar third-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message success">
            <h4>交易成功</h4>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container">
            
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>

    <div class="text-center" >
        <a class="zoomable" href="{{ $receipt }}" target="_blank">
            <img class="img-responsive" src="{{ $receipt }}">
        </a>
    </div> 
    
</div>

<!-- RATING POPUP ON COMPLETE PAGE -->
<!-- Can use bootbox, the style is same -->
<div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Rate</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="rating">
                    <h5>此次交易已经完毕，请给买家/卖家评价</h5>
                    <p>您的评价将有效的帮助其他会员选择到高效率的卖家及买家。五星为最佳，一星为略差。</p>
                    
                    <!-- the css set it in right to left, so make it descending order from 5 to 1 -->
                    <div class="star-radio">
                    
                        <input type="radio" class="star-input" name="star" id="star-5" value="5"/>
                        <label class="star star-5" for="star-5"></label>
                        
                        <input type="radio" class="star-input" name="star" id="star-4" value="4"/>
                        <label class="star star-4" for="star-4"></label>
                        
                        <input type="radio" class="star-input" name="star" id="star-3" value="3"/>
                        <label class="star star-3" for="star-3"></label>
                        
                        <input type="radio" class="star-input" name="star" id="star-2" value="2"/>
                        <label class="star star-2" for="star-2"></label>
                        
                        <input type="radio" class="star-input" name="star" id="star-1" value="1"/>
                        <label class="star star-1" for="star-1"></label>
                    </div>
                    
                    <div class="form-action">
                        <button class="btn btn-primary">Rate</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(window).on('load',function(){
        $('#rating').modal('show');
    });
</script>
@endsection