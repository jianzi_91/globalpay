@extends('member.layouts.app')

@section('title', 'Trade')

@section('style')
<style type="text/css">
body {
    scroll-behavior: smooth;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number]{
    -moz-appearance:textfield;
}

.form-action {
    margin-top: 50px;
    margin-bottom: 100px;
}

.panel-warning {
    border-color: #faebcc;
    background-color: #fcf8e3;
    padding-top: 10px;
    padding-bottom: 5px;
}

ul.hint {
    list-style: none;
}
ul.hint li::before{
    content: '- ';
}
.hint {
    color: grey;
    font-style: italic;
}
</style>
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.create-buying-offer')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.errors')

    <form action="{{ url('/member/trade/buy/create') }}" method="POST">
    {{ csrf_field() }}

        <div class="alert alert-info">
            <strong>@lang('general.page.trade.reference-price')</strong>
            <p>@lang('general.page.trade.current-market-value') : &yen; {{ $curPrice }} / @lang('general.wallet.coin')</p>
        </div>

        <br>

        <div class="form-group">
            <label class="control-label"><h3>@lang('general.page.trade.transaction-limit')</h3></label>
            <hr>
        </div>
        
        <div class="form-group">
            <div class="panel panel-warning">
                <ul class="hint">
                    <li><small>{{ trans('general.page.trade.hint.min-sell', array('min_qty' => config('trade.min_sell_limit'))) }}</small></li>
                </ul>
            </div>
        </div>

        <div class="form-group">
            <div class="row ">
                <div class="col-xs-6 col-sm-6">
                    <label class="control-label">@lang('general.page.trade.seller-min-sell')</label>
                    <input type="number" class="form-control" placeholder="" name="min_qty" value="{{ old('min_qty') }}">
                </div>
                <div class="col-xs-6 col-sm-6">
                    <label class="control-label">@lang('general.page.trade.seller-max-sell')</label>
                    <input type="number" class="form-control" placeholder="" name="max_qty" value="{{ old('max_qty') }}">
                </div>
            </div>
        </div>
        
        <br>

        <div class="form-group">
            <label class="control-label"><h3>@lang('general.page.trade.payment.method')</h3></label>
            <hr>
        </div>

        <div class="form-group">
            <select class="form-control" name="payment">
                <option value="">@lang('general.page.trade.payment.select')</option>
                <option value="B">@lang('general.page.trade.payment.bank-transfer')</option>
                <option value="W">@lang('general.page.trade.payment.wechatpay')</option>
                <option value="A">@lang('general.page.trade.payment.alipay')</option>
            </select>
        </div>
        
        <div class="form-action">
            <button class="btn btn-primary">@lang('general.button.add-buying-offer')</button>
        </div>
    </form>
    
</div>
@endsection