@extends('member.layouts.app')
@section('title', 'Trade')

@inject('trade', 'App\Trades\TradeService')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number]{
    -moz-appearance:textfield;
}
</style>
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.sell')</div>
@endsection

@section('content')
<div class="general-container">
    @include('member.common.errors')
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">¥‎</div>{{ number_format($tradeBuy->price, 2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">{{ number_format($tradeBuy->min_qty) }} - {{ number_format($tradeBuy->open_qty) }}</div>
                    </div>
                    <!-- <div class="list-row">
                        <label>付款方式</label>
                        <div class="list-detail">
                            银行转账
                            <span class="bank-name">中国银行</span>
                        </div>
                    </div> -->
                    <div class="list-row">
                        <label>@lang('general.page.trade.buyer')</label>
                        <div class="list-detail">
                            {{ $tradeBuy->buyer->name }}
                            <br>
                            @if($tradeBuy->buyer->rating->avg('rate') != 0)
                            <div class="stars-rating {{ trans('general.page.trade.rate.' . ceil($tradeBuy->buyer->rating->avg('rate'))) }}">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            @else
                                <span></span>
                                <span>&emsp; @lang('general.page.trade.rate.0')  </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<div class="general-container">
    
    <div class="alert alert-info">
        <strong>@lang('general.page.trade.hint.attention')</strong>
        <p>@lang('general.page.trade.hint.tax-deduction')</p>
    </div>
    
    <!--- FORM --->
    <form action="{{ url('/member/trade/sell') . '/' . $tradeBuy->id . '/request' }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label">@lang('general.field.quantity')</label>
            <div class="multiple-row">
                <div class="input-container">
                    <input type="number" class="form-control" id="unit" value="{{ old('unit')?:0 }}" name="unit">
                    <div class="input-label">@lang('general.wallet.coin')</div>
                </div>  
                <div class="input-container">
                    <input type="number" class="form-control" id="price" value="{{ $tradeBuy->price }}" readonly="">
                    <div class="input-label">{{ trans('general.page.trade.currency.' . $tradeBuy->currency) }} ({{ $tradeBuy->currencyCode->symbol }})</div>
                </div>
            </div>
        </div>
        
        <div class="amount-summary">
            <div class="list">
                <label>@lang('general.field.total')</label>
                <div class="amount">&yen; <span id="total">0</span></div>
            </div>
            <div class="list">
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.processing-fee') ({{$trade->getPlatformCharge()}}%)</label>
                        <div class="amount"><span class="fee" id="processing">0</span>@lang('general.wallet.coins')</div>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.charity-fund') ({{$trade->getCharityRate()}}%)</label>
                        <div class="amount"><span class="fee" id="charity">0</span>@lang('general.wallet.coins')</div>
                    </div>
                </div>
            </div>
            <div class="list">
                <label>@lang('general.field.total-deduct')</label>
                <div class="amount"><span id="nett">0</span>@lang('general.wallet.coins')</div>
            </div>
        </div>

        @if($tradeBuy->payment_method == 'B')
        {{-- Bank Account Info --}}
        <div class="form-group">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">@lang('general.table.header.bank-info')</div>
                </div>
                <div class="panel-body">
                    <ul class="hint payment" style="list-style: none; padding-top: 10px;">
                        <li>@lang('general.page.trade.payment.bank-owner-name') : {{ auth()->user()->name }}</li>
                        <li>@lang('general.page.trade.payment.bank-name') : {{ isset(auth()->user()->bank_name)?auth()->user()->bank_name:'N/A' }}</li>
                        <li>@lang('general.page.trade.payment.bank-acc-no') : {{ isset(auth()->user()->bank_acc_no)?auth()->user()->bank_acc_no:'N/A' }}</li>
                    </ul>
                </div>
            </div>
        </div>
        @else
        {{-- WeChat Pay && Alipay Info --}}
        <div class="form-group" id="wallet-pay">
            <div class="file-loading">
                <input id="payment-ss" name="payment_ss" type="file" value="{{ old('payment_ss') }}" accept="image/*">
            </div>

            <input type="text" class="form-control" name="payment_id" value="{{ old('payment_id') }}" placeholder="{{ trans('general.page.trade.payment.wallet-id') }}">
        </div>
        @endif
        
        <div class="form-action">
            <button type="submit" class="btn btn-primary">@lang('general.button.confirm')</button>
        </div>
    </form>
        
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    var price = <?php echo($tradeBuy->price) ?>;

    $('#unit').click(function(){
        $(this).select();
    });

    $('#price').click(function(){
        $(this).select();
    });

    $('#unit').blur(function(){
        if ($(this).val() == '') {
            $(this).val(0);
        };
    });

    $('#price').blur(function(){
        if ($(this).val() == '') {
            $(this).val(price);
        };
    });

    $('#unit').on('keyup paste', calculate);

    calculate();

    function calculate()
    {
        var coin = $('#unit').val();
        var processingRate = <?php echo($trade->getPlatformCharge()) / 100 ?>;
        var charityRate = <?php echo($trade->getCharityRate()) / 100 ?>;
        var unitPrice = <?php echo($tradeBuy->price) ?>;
        // selector

        var objTotal = $('#total');
        var objProcessing = $('#processing');
        var objCharity = $('#charity');
        var objNett = $('#nett');

        var total = coin * price;
        var processingFee = Math.round(total * processingRate / unitPrice * 100) / 100;
        var charityFee = Math.round(total * charityRate / unitPrice * 100) / 100;
        var nett = (total * 100 / unitPrice /100) + processingFee + charityFee;

        objTotal.html(total.toFixed(2));
        objProcessing.html(processingFee.toFixed(2));
        objCharity.html(charityFee.toFixed(2));
        objNett.html(nett.toFixed(2));
    }
});
</script>
@if($tradeBuy->payment_method == 'A' || $tradeBuy->payment_method == 'W')
<script src="/assets/plugins/fileinputjs/js/plugins/piexif.js"></script>
<script src="/assets/plugins/fileinputjs/js/plugins/sortable.js"></script>
<script src="/assets/plugins/fileinputjs/js/fileinput.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    $("#payment-ss").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showCaption: false,
        showBrowse: false,
        showRemove: false,
        showUpload: false,
        browseOnZoneClick: true,
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<h6 class="text-muted">@lang("general.page.trade.hint.payment-ss")</h6>',
        layoutTemplates: {
            modal: '<div class="modal-dialog modal-lg{rtl}" role="document">\n' +
            '  <div class="modal-content">\n' +
            '    <div class="modal-body">\n' +
            '      <div class="floating-buttons"></div>\n' +
            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
            '    </div>\n' +
            '  </div>\n' +
            '</div>\n',
        },
        allowedFileExtensions: ["jpg", "png", "gif"]
    });


});

</script>
@endif
@endsection