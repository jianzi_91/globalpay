@extends('member.layouts.app')
@section('title', 'Trade')
@section('page-title')
<div class="page-title">@lang('general.pagetitle.user.trading-lobby')</div>
@endsection
@section('submenu')
<ul class="top-tabs">
    <li class="list">
        <a href="{{ url('/member/trade/buy') }}">@lang('general.pagetitle.user.buy-lobby')</a>
    </li>
    <li class="list active">
        <a href="javascript:void(0)">@lang('general.pagetitle.user.sell-lobby')</a>
    </li>
</ul>
@endsection

@section('content')
<div class="general-container">

    @include('member.common.success')
    @include('member.common.errors')

    @if ($restricted)
        <!-- Form Error List -->
        <div class="alert alert-danger">
            @lang('general.page.trade.error.market-closed')
        </div>
    @endif

    <div class="trading-action">
        <a href="{{ url('member/trade/sell/list') }}" class="order"><i class="icon-existing-trade"></i> @lang('general.button.my-offers')</a>
        <a href="{{ url('member/trade/records') }}" class="history"><i class="icon-trade-history"></i> @lang('general.button.trade-history')</a>
    </div>
    
    <div class="trading-list">
    
        @if($buys->count() == 0)
        <div class="list empty">
            @lang('general.page.trade.temporary-empty')
        </div>
        @else
        @foreach($buys as $buy)
        <div class="list">
            <div class="price green">
                <div class="currency">{{$buy->currencyCode->symbol}}</div>{{ number_format($buy->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="quantity"><label>@lang('general.field.buy_id')</label>{{ date_format($buy->created_at, 'Ymd') . str_pad($buy->id, 4, 0, STR_PAD_LEFT) }}</div>
            <div class="quantity"><label>@lang('general.field.quantity')</label>{{ number_format($buy->open_qty) }} @lang('general.wallet.coin')</div>
            <div class="user">
                <div class="username">{{ $buy->buyer->name }}</div>
                @if($buy->buyer->rating->avg('rate') != 0)
                <div class="stars-rating {{ trans('general.page.trade.rate.' . ceil($buy->buyer->rating->avg('rate'))) }}">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                @else
                    <span></span>
                    <span>&emsp; @lang('general.page.trade.rate.0')  </span>
                @endif
            </div>

            @if($buy->payment_method == 'B')
            <div class="payment-method bank-transfer">@lang('general.page.trade.payment.bank-transfer')</div>
            @elseif($buy->payment_method == 'W')
            <div class="payment-method wechatpay">@lang('general.page.trade.payment.wechatpay')</div>
            @elseif($buy->payment_method == 'A')
            <div class="payment-method alipay">@lang('general.page.trade.payment.alipay')</div>
            @endif

            @if($buy->uid != auth()->user()->id)
            <div class="action">
                <a href="{{ url('/member/trade/sell') . '/' . $buy->id . '/request'}}">
                    <button class="btn btn-primary">@lang('general.button.sell')</button>
                </a>
            </div>
            @endif
        </div>
        @endforeach
        <div class="text-center">{{ $buys->links() }}</div>
        @endif
    </div>
</div>

<a class="add-order" href="{{ url('/member/trade/sell/create') }}"></a>
@endsection