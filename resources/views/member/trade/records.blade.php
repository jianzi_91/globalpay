@extends('member.layouts.app')
@section('title', 'Trade')
@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">@lang('general.menu.user.trade-history')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')

    <div class="trading-list">
        @if($records->count() == 0)
        <div class="list empty">
            @lang('general.page.trade.temporary-empty')
        </div>
        @else
            @foreach($records as $record)
                @if($record->buy_uid == auth()->user()->id)
                <div class="list">
                    <div class="price green">
                        <div class="currency">{{ $record->tradeBuy->currencyCode->symbol }}‎</div>{{ $record->price }} <span>/ @lang('general.wallet.coin')</span>
                    </div>
                    <div class="quantity"><label>@lang('general.field.quantity')</label>{{ $record->quantity }}</div>
                    <div class="quantity"><label>@lang('general.field.type')</label> @lang('general.page.trade.buy')</div>
                    <div class="user">
                        <div class="username">{{ $record->seller->name }}</div>
                    </div>
                    <div class="status">
                        @if($record->status == 4)
                        <label class="label label-success">@lang('general.page.trade.completed_at')</label>
                        @else
                        <label class="label label-danger">@lang('general.page.trade.cancelled_at')</label>
                        @endif
                        <span class="date">{{ $record->updated_at }}</span>
                    </div>
                </div>
                @elseif($record->sell_uid == auth()->user()->id)
                <div class="list">
                    <div class="price green">
                        <div class="currency">{{ $record->tradeSell->currencyCode->symbol }}‎</div>{{ $record->price }} <span>/ @lang('general.wallet.coin')</span>
                    </div>
                    <div class="quantity"><label>@lang('general.field.quantity')</label>{{ $record->quantity }}</div>
                    <div class="quantity"><label>@lang('general.field.type')</label> @lang('general.page.trade.sell')</div>
                    <div class="user">
                        <div class="username">{{ $record->buyer->name }}</div>
                    </div>
                    <div class="status">
                        @if($record->status == 4)
                        <label class="label label-success">@lang('general.page.trade.completed_at')</label>
                        @else
                        <label class="label label-danger">@lang('general.page.trade.cancelled_at')</label>
                        @endif
                        <span class="date">{{ $record->updated_at }}</span>
                    </div>
                </div>
                @endif
            @endforeach
            <div class="paginator text-center">{{ $records->links() }}</div>
        @endif
    </div>
</div>

<a class="add-order" href="{{ url('/member/trade/buy/create') }}"></a>
@endsection