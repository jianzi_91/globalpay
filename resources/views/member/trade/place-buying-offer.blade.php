@extends('member.layouts.app')
@section('title', 'Trade')

@inject('trade', 'App\Trades\TradeService')

@section('style')
<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number]{
    -moz-appearance:textfield;
}
</style>
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.buy')</div>
@endsection

@section('content')
<div class="general-container">
    @include('member.common.errors')
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">¥‎</div>{{ $tradeSell->price }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">{{ number_format($tradeSell->min_qty) }} - {{ number_format($trade->coinsBeforeCharges($tradeSell->open_qty)) }}</div>
                    </div>
                    @if($tradeSell->payment_method == 'B')
                    <div class="list-row">
                        <label>@lang('general.page.trade.payment.method')</label>
                        <div class="list-detail"><span><img src='/assets/images/icon/paymentmethod_ic_bank_transfer.png'></span>&ensp;<span>@lang('general.page.trade.payment.bank-transfer')</span></div>
                    </div>
                    @elseif($tradeSell->payment_method == 'W')
                    <div class="list-row">
                        <label>@lang('general.page.trade.payment.method')</label>
                        <div class="list-detail"><span><img src='/assets/images/icon/paymentmethod_ic_wechatpay.png'></span>&ensp;<span>@lang('general.page.trade.payment.wechatpay')</span></div>
                    </div> 
                    @elseif($tradeSell->payment_method == 'A')
                    <div class="list-row">
                        <label>@lang('general.page.trade.payment.method')</label>
                        <div class="list-detail"><span><img src='/assets/images/icon/paymentmethod_ic_alipay.png'></span>&ensp;<span>@lang('general.page.trade.payment.alipay')</span></div>
                    </div> 
                    @endif
                    <div class="list-row">
                        <label>@lang('general.page.trade.seller')</label>
                        <div class="list-detail">
                            {{ $tradeSell->seller->name }}
                            <br>
                            @if($tradeSell->seller->rating->avg('rate') != 0)
                            <div class="stars-rating {{ trans('general.page.trade.rate.' . ceil($tradeSell->seller->rating->avg('rate'))) }}">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            @else
                                <span></span>
                                <span>&emsp; @lang('general.page.trade.rate.0')  </span>
                            @endif
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<div class="general-container">
    
    <!--- FORM --->
    <form action="{{ url('/member/trade/buy') . '/' . $tradeSell->id . '/request' }}" method="POST" style="margin-bottom: 50px;">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label">@lang('general.field.quantity')</label>
            <div class="multiple-row">
                <div class="input-container">
                    <input type="number" class="form-control" id="unit" value="{{ old('unit')?:0 }}" name="unit">
                    <div class="input-label">@lang('general.wallet.coin')</div>
                </div>  
                <div class="input-container">
                    <input type="number" class="form-control" id="price" value="{{ $tradeSell->price }}" readonly="">
                    <div class="input-label">{{ trans('general.page.trade.currency.' . $tradeSell->currency) }} ({{ $tradeSell->currencyCode->symbol }})</div>
                </div>
            </div>
        </div>
        
        <div class="amount-summary">
            <div class="list">
                <label>@lang('general.field.total')</label>
                <div class="amount">&yen; <span id="total">0</span></div>
            </div>
            <!-- <div class="list">
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.processing-fee') (5%)</label>
                        <div class="amount"><span class="fee" id="processing">0</span>@lang('general.wallet.coins')</div>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.charity-fund') (1%)</label>
                        <div class="amount"><span class="fee" id="charity">0</span>@lang('general.wallet.coins')</div>
                    </div>
                </div>
            </div>
            <div class="list">
                <label>@lang('general.field.receivable')</label>
                <div class="amount"><span id="nett">0</span>@lang('general.wallet.coins')</div>
            </div> -->
        </div>
        
        <div class="form-action">
            <button type="submit" class="btn btn-primary">@lang('general.button.confirm')</button>
        </div>
    </form>
        
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    var price = <?php echo($tradeSell->price) ?>;

    $('#unit').click(function(){
        $(this).select();
    });

    $('#price').click(function(){
        $(this).select();
    });

    $('#unit').blur(function(){
        if ($(this).val() == '') {
            $(this).val(0);
        };
    });

    $('#price').blur(function(){
        if ($(this).val() == '') {
            $(this).val(price);
        };
    });

    $('#unit').on('keyup paste', calculate);

    calculate();

    function calculate()
    {
        var coin = $('#unit').val();
        var processingRate = <?php echo($setting->platform_charge) / 100 ?>;
        var charityRate = <?php echo($setting->charity_percent) / 100 ?>;
        var unitPrice = <?php echo($tradeSell->price) ?>;
        // selector

        var objTotal = $('#total');
        var objProcessing = $('#processing');
        var objCharity = $('#charity');
        var objNett = $('#nett');

        var total = coin * price;
        var processingFee = Math.round(total * processingRate / unitPrice * 100) / 100;
        var charityFee = Math.round(total * charityRate / unitPrice * 100) / 100;
        var nett = (total * 100 / unitPrice /100) - processingFee - charityFee;

        objTotal.html(total.toFixed(2));
        objProcessing.html(processingFee.toFixed(2));
        objCharity.html(charityFee.toFixed(2));
        objNett.html(nett.toFixed(2));
    }
});
</script>
@endsection