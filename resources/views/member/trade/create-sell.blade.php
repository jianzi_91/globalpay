@extends('member.layouts.app')

@section('title', 'Trade')

@inject('trade', 'App\Trades\TradeService')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput-rtl.css">
<style type="text/css">
body {
    scroll-behavior: smooth;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number]{
    -moz-appearance:textfield;
}

.form-action {
    margin-top: 50px;
    margin-bottom: 100px;
}

.panel-warning {
    border-color: #faebcc;
    background-color: #fcf8e3;
    padding-top: 10px;
    padding-bottom: 5px;
}

ul.hint {
    list-style: none;
}
ul.hint.limit li::before{
    content: '- ';
}
.hint.limit {
    color: grey;
    font-style: italic;
}
.hint.payment {
    color: grey;
}
</style>
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/sell') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.create-selling-offer')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.errors')

    <form action="{{ url('/member/trade/sell/create') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}

        <div class="alert alert-info">
            <strong>@lang('general.page.trade.reference-price')</strong>
            <p>@lang('general.page.trade.current-market-value') : &yen; {{ number_format($curPrice,2) }} / @lang('general.wallet.coin')</p>
            <p>@lang('general.page.trade.coin-balance') : {{ number_format($seller->wallet->cwallet,8) }} @lang('general.wallet.coin')</p>
        </div>

        <br>

        <div class="form-group">
            <label class="control-label"><h3>@lang('general.page.trade.transaction-limit')</h3></label>
            <hr>
        </div>
        
        <div class="form-group">
            <div class="panel panel-warning">
                <ul class="hint limit">
                    <li><small>{{ trans('general.page.trade.hint.min-sell', array('min_qty' => config('trade.min_sell_limit'))) }}</small></li>
                    <li><small>{{ trans('general.page.trade.hint.max-buy', array('max_qty' => floor($seller->wallet->cwallet))) }}</small></li>
                </ul>
            </div>
        </div>

        <div class="form-group">
            <div class="row ">
                <div class="col-xs-6 col-sm-6">
                    <label class="control-label">@lang('general.page.trade.buyer-min-buy')</label>
                    <input id="min_qty" type="number" class="form-control" placeholder="" name="min_qty" value="{{ old('min_qty')?:1 }}">
                </div>
                <div class="col-xs-6 col-sm-6">
                    <label class="control-label">@lang('general.page.trade.buyer-max-buy')</label>
                    <input id="max_qty" type="number" class="form-control" placeholder="" name="max_qty" value="{{ old('max_qty')?:1 }}">
                </div>
            </div>
        </div>

        <div class="amount-summary">
            <div class="list">
                <label>@lang('general.field.total')</label>
                <div class="amount">&yen; <span id="total">0</span></div>
            </div>
            <div class="list">
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.processing-fee') ({{$trade->getPlatformCharge()}}%)</label>
                        <div class="amount"><span class="fee" id="processing">0</span>@lang('general.wallet.coins')</div>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <label>@lang('general.field.charity-fund') ({{$trade->getCharityRate()}}%)</label>
                        <div class="amount"><span class="fee" id="charity">0</span>@lang('general.wallet.coins')</div>
                    </div>
                </div>
            </div>
            <div class="list">
                <label>@lang('general.field.total-deduct')</label>
                <div class="amount"><span id="nett">0</span>@lang('general.wallet.coins')</div>
            </div>
        </div>
        
        <br>

        <div class="form-group">
            <label class="control-label"><h3>@lang('general.page.trade.payment.method')</h3></label>
            <hr>
        </div>
        
        <div class="form-group">
            <select class="form-control" name="payment" id="payment">
                <option value="">@lang('general.page.trade.payment.select')</option>
                <option value="B" {{ (old('payment') == 'B')?'selected':'' }}>@lang('general.page.trade.payment.bank-transfer')</option>
                <option value="W" {{ (old('payment') == 'W')?'selected':'' }}>@lang('general.page.trade.payment.wechatpay')</option>
                <option value="A" {{ (old('payment') == 'A')?'selected':'' }}>@lang('general.page.trade.payment.alipay')</option>
            </select>
        </div>

        <br>

        {{-- Bank Account Info --}}
        <div class="form-group" id="bank-transfer" hidden="">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">@lang('general.table.header.bank-info')</div>
                </div>
                <div class="panel-body">
                    <ul class="hint payment" style="list-style: none; padding-top: 10px;">
                        <li>@lang('general.page.trade.payment.bank-owner-name') : {{ $seller->name }}</li>
                        <li>@lang('general.page.trade.payment.bank-name') : {{ isset($seller->bank_name)?$seller->bank_name:'N/A' }}</li>
                        <li>@lang('general.page.trade.payment.bank-acc-no') : {{ isset($seller->bank_acc_no)?$seller->bank_acc_no:'N/A' }}</li>
                    </ul>
                </div>
            </div>
        </div>

        {{-- WeChat Pay && Alipay Info --}}
        <div class="form-group" id="wallet-pay" hidden="">
            <div class="file-loading">
                <input id="payment-ss" name="payment_ss" type="file" value="{{ old('payment_ss') }}" accept="image/*">
            </div>

            <input type="text" class="form-control" name="payment_id" value="{{ old('payment_id') }}" placeholder="{{ trans('general.page.trade.payment.wallet-id') }}">
        </div>

        <div class="form-action">
            <button class="btn btn-primary">@lang('general.button.add-selling-offer')</button>
        </div>
    </form>
</div>
@endsection

@section('script')
<script src="/assets/plugins/fileinputjs/js/plugins/piexif.js"></script>
<script src="/assets/plugins/fileinputjs/js/plugins/sortable.js"></script>
<script src="/assets/plugins/fileinputjs/js/fileinput.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    var selection = "{{ old('payment') }}";

    if (selection == 'B') {
        $('#bank-transfer').show();
    } else if (selection == 'W' || selection == 'A') {
        $('#wallet-pay').show();
    }

    $('#payment').change(function(){
        if ($(this).val() == 'B') {
            $('#bank-transfer').show();
            $('#wallet-pay').hide();
        } else if ($(this).val() == 'W' || $(this).val() == 'A'){
            $('#bank-transfer').hide();
            $('#wallet-pay').show();
        } else {
            $('#bank-transfer').hide();
            $('#wallet-pay').hide();
        }
    });

    $("#payment-ss").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showCaption: false,
        showBrowse: false,
        showRemove: false,
        showUpload: false,
        browseOnZoneClick: true,
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<h6 class="text-muted">@lang("general.page.trade.hint.payment-ss")</h6>',
        layoutTemplates: {
            modal: '<div class="modal-dialog modal-lg{rtl}" role="document">\n' +
            '  <div class="modal-content">\n' +
            '    <div class="modal-body">\n' +
            '      <div class="floating-buttons"></div>\n' +
            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
            '    </div>\n' +
            '  </div>\n' +
            '</div>\n',
        },
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
});
</script>
<script>
$(document).ready(function(){

    var price = <?php echo($trade->getCurrentMarketPrice()) ?>;

    $('#max_qty').click(function(){
        $(this).select();
    });

    $('#max_qty').blur(function(){
        if ($(this).val() == '') {
            $(this).val(0);
        };
    });

    $('#max_qty').on('keyup paste', calculate);

    calculate();

    function calculate()
    {
        var coin = $('#max_qty').val();
        var processingRate = <?php echo($trade->getPlatformCharge()) / 100 ?>;
        var charityRate = <?php echo($trade->getCharityRate()) / 100 ?>;
        var unitPrice = <?php echo($trade->getCurrentMarketPrice()) ?>;
        // selector
        var objTotal = $('#total');
        var objProcessing = $('#processing');
        var objCharity = $('#charity');
        var objNett = $('#nett');

        var total = coin * price;
        var processingFee = Math.round(total * processingRate / unitPrice * 100) / 100;
        var charityFee = Math.round(total * charityRate / unitPrice * 100) / 100;
        var nett = (total * 100 / unitPrice /100) + processingFee + charityFee;

        objTotal.html(total.toFixed(2));
        objProcessing.html(processingFee.toFixed(2));
        objCharity.html(charityFee.toFixed(2));
        objNett.html(nett.toFixed(2));
    }
});
</script>
@endsection