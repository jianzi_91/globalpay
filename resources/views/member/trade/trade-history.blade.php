@extends('member.layouts.app')
@section('title', 'Trade')

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy') }}"></a>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    <div class="trading-list">
        <div class="list">
            <div class="price green">
                <div class="currency">{{ $queue->tradeSell->currencyCode->symbol }}</div>{{ $queue->price }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="quantity"><label>数量</label> 160币 / &yen; 360</div>
            <div class="user">
                <div class="username">买家用户名</div>
            </div>
            <div class="status">
                <label class="label label-success">交易完毕</label>
                <span class="date">03-08-2018  17:30:00</span>
            </div>
        </div>
    </div>
</div>
@endsection