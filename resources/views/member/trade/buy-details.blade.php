@extends('member.layouts.app')
@section('title', 'Trade')

@inject('trade', 'App\Trades\TradeService')

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/plugins/fileinputjs/css/fileinput.css">
@endsection

@section('page-title')
<a class="back-toggle" href="{{ url('/member/trade/buy/list') }}"></a>
<div class="page-title">@lang('general.pagetitle.user.buy-details')</div>
@endsection

@section('content')
<div class="general-container">
    
    @include('member.common.success')
    @include('member.common.errors')
    
    <div class="trading-list">
        <div class="list single">
            <div class="price green">
                <div class="currency">{{ $queue->tradeSell->currencyCode->symbol }}</div>{{ number_format($queue->price,2) }} <span>/ @lang('general.wallet.coin')</span>
            </div>
            <div class="list-content full">
                <div class="inner">
                    <div class="list-row">
                        <label>@lang('general.field.quantity')</label>
                        <div class="list-detail">
                            {{ number_format($queue->quantity,2) }}
                            <div class="total"><label class="total-label">@lang('general.field.total')</label> {{ $queue->tradeSell->currencyCode->symbol }} {{ $queue->quantity * $queue->price }}</div>
                        </div>
                    </div>
                    <div class="list-row">
                        <label>@lang('general.page.trade.payment.method')</label>
                        @if($queue->tradeSell->payment_method == 'B')
                            <div class="list-detail">
                                <span><img src='/assets/images/icon/paymentmethod_ic_bank_transfer.png'></span>&ensp;<a id="btn-check-bank">@lang('general.page.trade.payment.bank-transfer')</a>
                            </div>
                        @elseif($queue->tradeSell->payment_method == 'W')
                            <div class="list-detail">
                                <span><img src='/assets/images/icon/paymentmethod_ic_wechatpay.png'></span>&ensp;<a id="btn-check-qr">@lang('general.page.trade.payment.wechatpay')</a>
                            </div> 
                        @elseif($queue->tradeSell->payment_method == 'A')
                            <div class="list-detail">
                                <span><img src='/assets/images/icon/paymentmethod_ic_alipay.png'></span>&ensp;<a id="btn-check-qr">@lang('general.page.trade.payment.alipay')</a>
                            </div> 
                        @endif
                    </div>
                    <div class="list-row">
                        <label>@lang('general.page.trade.seller')</label>
                        <div class="list-detail">
                            {{ $queue->seller->name }}
                        </div>
                    </div>
                    @if($queue->status == 3)
                    <div class="list-row">
                        <label>@lang('general.page.trade.seller-contact')</label>
                        <div class="list-detail">
                            +{{ $queue->seller->countryInfo->phone_code }} {{ $queue->seller->mobile }}
                        </div>
                    </div>
                    @endif
                    <div class="list-row">
                        <label>@lang('general.page.trade.created_at')</label>
                        <div class="list-detail">
                            {{ $ca_display }}
                        </div>
                    </div>
                    @if($queue->status == 4)
                    <div class="list-row">
                        <label>@lang('general.page.trade.buy_at')</label>
                        <div class="list-detail">
                            {{ $ua_display }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

@if($queue->status == 2)
<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar first-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message">
            <h4>{!! trans('general.page.trade.payment.countdown', ['time' => '<span id="countdown">00:00:00</span>']) !!}</h4>
            <p>@lang('general.page.trade.payment.warning')</p>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container payment" style="margin-bottom: 200px;">
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>
    
    <form method="POST" action="/member/trade/buy/pay" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="queueId" value="{{ $queue->id }}">
        <div class="form-group">
            <div class="file-loading">
                <input id="avatar-2" name="proof" type="file">
            </div>
        </div>
        <div class="form-action">
        <a class="btn btn-danger" href="{{ route('reject-buy-request', ['orderId' => $queue->id]) }}">@lang('general.button.cancel')</a>
        <button type="submit" class="btn btn-primary">@lang('general.button.submit')</button>
        </div>
    </form>
</div>
@elseif($queue->status == 3)
<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar second-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message">
            <h4>@lang('general.page.trade.payment.received')</h4>
        </div>
    </div>
</div>

<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container">
            
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>

    <div class="text-center" >
        <a class="zoomable" href="{{ $receipt }}" target="_blank">
            <img class="img-responsive" src="{{ $receipt }}">
        </a>
    </div> 
    
</div>
@elseif($queue->status == 4)
<div class="general-container">
    <!-- ORDER STATUS -->
    <div class="order-status">
        <div class="buy-sell-status">
            <ul class="status-bar third-step">
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_1')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_2')</label>
                </li>
                <li>
                    <span></span>
                    <label>@lang('general.page.trade.payment.step_3')</label>
                </li>
            </ul>
        </div>
        <div class="message success">
            <h4>@lang('general.page.trade.success.trade')</h4>
        </div>
    </div>
</div>
<div class="line-seperate"></div>

<!--- FORM --->
<div class="general-container">
            
    <div class="styled-heading">@lang('general.page.trade.payment-proof')</div>

    <div class="text-center" >
        <a class="zoomable" href="{{ $receipt }}" target="_blank">
            <img class="img-responsive" src="{{ $receipt }}">
        </a>
    </div>

    <br>

    <div class="form-action text-center">
        <a type="button" class="btn btn-primary" href="{{ route('my-trade-sell') }}">@lang('general.button.back')</a>
    </div>
    
</div>

<!-- RATING POPUP ON COMPLETE PAGE -->
<!-- Can use bootbox, the style is same -->
<div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('general.page.trade.rating')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="rating">
                    <form id="rate-form" method="POST" action="{{ url('/member/trade/sell/rate') }}">
                        {{ csrf_field() }}
                        <h5>@lang('general.page.trade.rate.title')</h5>
                        <p>@lang('general.page.trade.rate.message')</p>
                        
                        <!-- the css set it in right to left, so make it descending order from 5 to 1 -->
                        <div class="star-radio">
                        
                            <input type="radio" class="star-input" name="star" id="star-5" value="5"/>
                            <label class="star star-5" for="star-5"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-4" value="4"/>
                            <label class="star star-4" for="star-4"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-3" value="3"/>
                            <label class="star star-3" for="star-3"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-2" value="2"/>
                            <label class="star star-2" for="star-2"></label>
                            
                            <input type="radio" class="star-input" name="star" id="star-1" value="1"/>
                            <label class="star star-1" for="star-1"></label>
                        </div>

                        <input type="hidden" name="trade_id" value="{{ $queue->id }}">
                        
                        <div class="form-action">
                            <button class="btn btn-primary">@lang('general.button.confirm')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('script')
<script type="text/javascript">
$('#btn-check-qr').click(function(){
    bootbox.alert({
        title: "{{ trans('general.page.trade.payment.method') }}",
        message: "" +
        "<p> {{ trans('general.page.trade.payment.wechatpay') }}: {{ $queue->tradeSell->payment_id }} </p>" +
        "<img class='img-responsive' src='{{ $qr_code }}'>",  
    });
});

$('#btn-check-bank').click(function(){
    bootbox.alert({
        title: "{{ trans('general.page.trade.payment.method') }}",
        message: "" +
        "<label>{{ trans('field.user.bank_country') }}</label>: <p>{{ $queue->seller->country }}</p>" +
        "<label>{{ trans('field.user.bank_payee_name') }}</label>: <p>{{ $queue->seller->name }}</p>" +
        "<label>{{ trans('field.user.bank_name') }}</label>: <p>{{ $queue->seller->bank_name }}</p>" +
        "<label>{{ trans('field.user.bank_branch_name') }}</label>: <p>{{ $queue->seller->bank_branch_name }}</p>" +
        "<label>{{ trans('field.user.bank_acc_no') }}</label>: <p>{{ $queue->seller->bank_acc_no }}</p>" +
        "<label>{{ trans('field.user.bank_sorting_code') }}</label>: <p>{{ $queue->seller->bank_sorting_code }}</p>" +
        "<label>{{ trans('field.user.bank_iban') }}</label>: <p>{{ $queue->seller->bank_iban }}</p>",  
    });
});
</script>
@if($queue->status == 2)
<script src="/assets/plugins/fileinputjs/js/plugins/sortable.js"></script>
<script src="/assets/plugins/fileinputjs/js/fileinput.js"></script>
<script>
$("#avatar-2").fileinput({
    overwriteInitial: true,
    maxFileSize: 1000,
    showCaption: false,
    showBrowse: false,
    showRemove: false,
    showUpload: false,
    browseOnZoneClick: true,
    removeLabel: '',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-2',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt=""><h6 class="text-muted">@lang("general.page.trade.payment.filejs-upload-hint")</h6>',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});
</script>
<script>
    function Timer(duration, display){
        var timer = duration, hours, minutes, seconds;
        setInterval(function () {
            hours = parseInt((timer /3600)%24, 10)
            minutes = parseInt((timer / 60)%60, 10)
            seconds = parseInt(timer % 60, 10);

            if (hours <= 0 && minutes < 0 && seconds < 0) {
                $('.message').text('Session Expired. Please request new one');

                $('.payment').attr('hidden', true);
            }
            else{

                hours = hours < 10 ? "0" + hours : hours;
                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                
                display.text(hours +":"+minutes + ":" + seconds);

                --timer;
            }
        }, 1000);
    }

    jQuery(function ($) {
        var duration = <?php echo(strtotime($queue->expired_at) - strtotime('now')); ?>;
        var display = $('#countdown');
        Timer(duration, display);
    });
</script>
@endif

@if($queue->status == 4 && !$rated)
<script>
    $(window).on('load',function(){
        $('#rating').modal('show');
    });
</script>
@endif
@endsection