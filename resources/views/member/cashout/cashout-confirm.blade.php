@extends('member.layouts.app-nowallet')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
    <div class="general-container">
        <div class="styled-heading">{{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw-review') }}</div>
        <form class="form-horizontal" role="form" action="{{ url('/member/withdrawal/process') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-filled">
                <div class="form-group">
                    <label>{{ trans('field.withdrawal.receivable_amount') }}</label>
                    <div class="form-content">{{ number_format($receivable_amount, 2) }}</div>
                    <input type="hidden" name="receivable_amount" value="{{$receivable_amount}}">
                </div>
                <div class="form-group">
                    <label>{{ trans('field.withdrawal.commission_amount') }}</label>
                    <div class="form-content">{{ number_format($commission_amount, 2) }}</div>
                    <input type="hidden" name="commission_amount" value="{{$commission_amount}}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button onclick="history.go(-1);" class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                    <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection