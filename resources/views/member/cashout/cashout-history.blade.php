@extends('member.layouts.app-nowallet')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
@include('member.common.success')
@include('member.common.errors')
    <div class="general-container">
        <div class="styled-heading">{{ trans('general.menu.user.cashout-request-history') }}</div>
        <div class="cashout-list">
            @foreach($manualwithdrawals as $manualwithdrawal)
            <div class="list">
                <div class="inner">
                    <h4>${{number_format($manualwithdrawal->receivable_amount, 0)}}</h4>
                    <ul>
                    <li>{{ trans('field.user.request_id') }} : {{$manualwithdrawal->id}}</li>
                    <li>{{ trans('field.user.status') }} : {{$manualwithdrawal->status}}</li>
                    </ul>
                    <div class="receipt">
                        <button data-toggle="modal" data-target="#receiptModal" class="btn btn-primary btn-sm" data-image="{{ $manualwithdrawal->receipt_url }}">{{ trans('general.button.view_receipt') }}</button>
                    </div>
                <div class="date">{{$manualwithdrawal->created_at}}</div>
                </div>
            </div>
            @endforeach
            <div class="action">
                <a onclick="history.go(-1);" class="btn btn-default">{{ trans('general.button.back') }}</a>
            </div>
            
        </div>
    </div>

    <!-- Receipt Modal -->
    <div class="modal fade" id="receiptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="" width="100%" class="center">
          </div>
        </div>
      </div>
    </div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.receipt button').click(function () {
            $('#receiptModal img').attr('src', $(this).data('image'));
        });

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection