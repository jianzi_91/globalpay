@extends('member.layouts.app-nowallet')
@section('title', e(trans('general.pagetitle.user.home')))

@section('content')
@include('member.common.success')
@include('member.common.errors')
    <div class="general-container">
        <div class="styled-heading">{{ trans('field.withdrawal.withdrawal_history') }}</div>
        <div class="transaction-history-action">
        <a href="{{url('member/withdrawal/history')}}" class="btn btn-primary-outline">{{ trans('field.withdrawal.my_withdrawal_history') }}</a>
        </div>
        <hr>

        <div class="styled-heading">{{ trans('field.withdrawal.manual_withdrawal') }}</div>

    <form role="form" action="{{url('member/withdrawal/confirm')}}" method="POST">
        {{ csrf_field() }}
        
            <div class="form-group">
                <label class="control-label">{{ trans('field.withdrawal.amount') }} <span style="color:red;">*</span></label>
                <input name="amount" type="number" min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" title="Numbers only" class="form-control">
                <span class="help-block">{{ trans('field.withdrawal.minimum_amount') }}</span>
            </div>

            <hr>

            <div class="form-group">
                <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                <input type="password" name="security_code" placeholder="{{ trans('field.user.epassword') }}" class="form-control">
            </div>
            
            <div class="form-action">
                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.field.continue') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/assets/plugins/owlcarousel/owl.carousel.css" rel="stylesheet">
@endsection

@section('script')
<script>
    $('.page-wrap').addClass('home');
</script>

<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();
        $('.datepicker[type=text]').datepicker({
            dateFormat: 'yy-mm-dd',
        });


    });
</script>
@endsection