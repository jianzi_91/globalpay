@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.forex')))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>
                    {{ trans("general.page.user.forex.bar-title.forex") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
                    </li>
                    <li class="active">
                        <a href="{{ url('/member/exchange') }}">{{ trans('general.menu.user.forex') }}</a>
                    </li>
                </ol>

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans("general.page.user.forex.content.latest-rates") }}</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tr>
                                <th>{{ trans("field.exchange.currency") }}</th>
                                <th>{{ trans("field.exchange.buy_rate") }}</th>
                                <th>{{ trans("field.exchange.sell_rate") }}</th>
                            </tr>
                            @foreach($exchange as $item)
                                <tr>
                                    <td>
                                        {{ $item->currency->code }}<br>
                                        <small>{{ $item->currency->name }}</small>
                                    </td>
                                    <td>{{ $item->buy_rate }}</td>
                                    <td>{{ $item->sell_rate }}</td>
                                </tr>
                            @endforeach
                        </table>
                        @if(empty($exchange))
                            <div class="alert alert-info text-center" role="alert">
                                No Data
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
