@extends('member.layouts.app')
@section('title', 'News')

@section('content')
<div class="page-title">{{ trans("general.page.user.news.bar-title.news") }}</div>

<div class="general-container">
    <div class="styled-heading">{{ trans("general.page.user.news.content.news") }}</div>

    <div class="news-list">
        @foreach ($news as $row)
        <div class="list">
            <div class="list-inner">
                <div class="second-level">
                    <a href="{{ url('member/news', $row->id) }}">
                        <div class="news-title">{{ App::isLocale('en') ? $row->title_en : $row->title_zh_cn }}</div>
                    </a>
                    <p>{{ App::isLocale('en') ? $row->descr_en : $row->descr_zh_cn }}</p>
                    <div class="date">{{ $row->start_date->format('Y-m-d') }}</div>
                </div>
            </div>
        </div>
        @endforeach
        {{ $news->links() }}
        @if(count($news) ==0)
            <div class="empty">
                No Data
            </div>
        @endif         
    </div>
</div>
@endsection
