@extends('member.layouts.app')
@section('title', trans("general.pagetitle.user.news"))

@section('content')
<div class="page-title">{{ trans("general.page.user.news.bar-title.news") }}</div>
<a class="back-toggle" href="{{ url('member/news') }}"></a>

<div class="general-container">
    <div class="single-news">
        <div class="news-title">{{ App::isLocale('en') ? $news->title_en : $news->title_zh_cn }}</div>
        <div class="date">{{ $news->start_date->format('Y-m-d') }}</div>

        <p>{!! App::isLocale('en') ? $news->content_en : $news->content_zh_cn !!}</p>
    </div>
</div>
    

<!--div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ App::isLocale('en') ? $news->title_en : $news->title_zh_cn }}</h3>
    </div>
    <div class="panel-body">
        <p>{!! App::isLocale('en') ? $news->content_en : $news->content_zh_cn !!}</p>
    </div>
</div>
<hr>
<a href="{{ url('member/news') }}">&#171; {{ trans("general.button.back-to-list") }}</a-->
            
                
@endsection

@section('script')
<script>
    $(".page-wrap").addClass("detail");
</script>
@endsection
