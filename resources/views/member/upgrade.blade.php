@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.upgrade')))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.upgrade.bar-title.upgrade') }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/upgrade') }}">{{ trans('general.menu.user.upgrade') }}</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">

            @if ($__step == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.upgrade.bar-title.package-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__fields['uid']['value'] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.code-name') }}</td>
                            <td>{{trans('general.activation.title.'.$__activation->code)}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.price') }}</td>
                            <td>{{ number_format($__fields['code']['options'][$__fields['code']['value']]['price'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.wallet-pay') }}</td>
                            <td>
                                <span style="text-decoration:underline">{{ isset($__fields['wallet_pay']['options'][old("wallet_pay")]) ? $__fields['wallet_pay']['options'][old("wallet_pay")] : "" }}</span>
                                
                                @if (count($__use_wallet))
                                    <ul>
                                    @foreach ($__use_wallet as $_wallet => $_wallet_amount)
                                        <li>{{ trans("general.wallet.".$_wallet) }} : {{ number_format($_wallet_amount, 2) }}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="{{ url('/member/upgrade') }}" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif ($__step == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.upgrade.bar-title.upgrade-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__fields['uid']['value'] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.code-name') }}</td>
                            <td>{{trans('general.activation.title.'.$__fields['code']['value'])}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.price') }}</td>
                            <td>{{ number_format($__fields['code']['options'][$__fields['code']['value']]['price'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.wallet-pay') }}</td>
                            <td>   
                                <span style="text-decoration:underline">{{ isset($__fields['wallet_pay']['options'][$__fields['wallet_pay']['value']]) ? $__fields['wallet_pay']['options'][$__fields['wallet_pay']['value']] : "" }}</span>

                                @if (count($__use_wallet))
                                    <ul>
                                    @foreach ($__use_wallet as $_wallet => $_wallet_amount)
                                        <li>{{ trans("general.wallet.".$_wallet) }} : {{ number_format($_wallet_amount, 2) }}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                        </tr>
                    </table>
                    <span class="help-block m-b-none">
                        {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->cwallet, 'wallet'=>trans('general.wallet.cwallet')]) }}
                    </span> 
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" action="{{ url('/member/upgrade') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__confirm' value='1'>
						<input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                        <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
                        @foreach (array('uid', 'code', "wallet_pay") as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ $__fields[$_field]['value'] }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
                    {{--##### END Hidden Form ####--}}
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}

            @elseif ($__step == 'user')
            {{-- ######## USER ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.upgrade.bar-title.upgrade') }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('/member/upgrade') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='user'>
						<input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='uid' value="{{ $__fields['uid']['value'] }}">
                                <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                                <span id='uid-text'></span>
                            </div>
                        </div>
                                                
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END USER ########### --}}

            @elseif ($__step == 'package')
            {{-- ######## PACKAGE ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.upgrade.bar-title.package') }}
                </div>
                
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td><td>{{ $__fields['uid']['value'] }}</td>
                        </tr>
                    </table>
                    
                    <hr />

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>   
                            <tr>
                                <th>{{ trans('field.activation.code-name') }}</th>
                                <th>{{ trans('field.activation.price') }}</th>

                                <th>{{ trans('field.activation.amount') }}</th>
                                <th>
                                    {{ trans('general.page.user.upgrade.content.package-compliment') }}
                                </th>
                                <th>
                                    {{ trans('general.table.header.action') }}
                                </th>
                            </tr>
                            </thead>   

                            @foreach ($__fields['code']['options'] as $_code=>$_details)
                            <tr>
                                <td><img src="{{ asset('assets/images/rank-'.$_code.'.png') }}" alt="{{ trans('general.activation.title.'.$_code) }}"></td>

                                <td>{{ number_format($_details['price'], 2) }}</td>
                                <td>{{ number_format($_details['amount'], 2) }}</td>

                                @if (isset($_details['get_wallet']))
                                    <td>
                                    @foreach($_details['get_wallet'] as $_wallet=>$_amount)
                                        {{ trans('general.wallet.'.$_wallet) }} : {{ number_format($_amount, 2) }}<br/>
                                    @endforeach
                                    </td>
                                @endif

                                <td>
                                    <form class="form-horizontal" role="form" action="{{ url('/member/upgrade') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="__req" value="1">
                                        <input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                                        <input type="hidden" name="code" value="{{ $_code }}">
                                        <input type="hidden" name="uid" value="{{ $__fields['uid']['value'] }}">
                                        <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
                                        
                                        <select name="wallet_pay">
                                            <option value="">-- {{ trans("field.activation.wallet-pay") }} --</option>
                                        @foreach ($__fields['wallet_pay']['options'] AS $_code=>$_details)
                                            <option value='{{ $_code }}' {{ old('wallet_pay')==$_code?'selected':'' }}>
                                                {{ $_details }}
                                            </option>
                                        @endforeach
                                        </select>

                                        <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.purchase') }}</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <span class="help-block m-b-none">
                        {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->cwallet, 'wallet'=>trans('general.wallet.cwallet')]) }}
                    </span> 
                    <br />
                    <br />
                    <form class="form-horizontal" role="form" action="{{ url('/member/upgrade') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="__req" value="back_user">
                        <input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                        <input type="hidden" name="uid" value="{{ $__fields['uid']['value'] }}">
                        <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.back') }}</button>
                    </form>
                </div>
            </div>
            {{-- ######## END PACKAGE ########## --}}
            @endif
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection