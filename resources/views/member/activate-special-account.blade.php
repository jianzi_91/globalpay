@extends('member.layouts.app')
@section('title', e(trans('general.menu.user.activate-special-account')))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.menu.user.activate-special-account') }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/activate-special-account') }}">{{ trans('general.menu.user.activate-special-account') }}</a>
				</li>
			</ol>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
			@include('partials.notification')
		</div>
        <div class="col-md-12">

            
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.menu.user.activate-special-account') }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('/member/activate-special-account') }}" method="POST">
						{{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='uid' value="{{ old('uid') }}">
                                <button class="btn btn-sm btn-primary" type="button" for='uid'>{{ trans('general.button.check') }}</button>
                                <span id='uid-text'></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('general.wallet.awallet2') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('general.wallet.awallet2') }}" class="form-control" name="amount" value="{{ old('amount') }}">
                            </div>
                        </div>
                                                
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name="epassword">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.submit') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        $( "button[for=uid]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('special-account/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection