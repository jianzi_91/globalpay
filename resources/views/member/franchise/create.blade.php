@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.franchise-account-register')))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.franchise-account-register.bar-title.register') }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/franchise/register') }}">{{ trans('general.menu.user.franchise-account-register') }}</a>
				</li>
			</ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
			<h3 class="text-center text-danger" style="font-weight:800;">{{ "***".trans("general.page.user.franchise-account-register.content.you-are-at-wallet-management-page", ['wallet'=>trans('general.wallet.fwallet')])."***" }}</h3>
		</div>
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">

            @if ($__step == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.franchise-account-register.bar-title.account-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{ $__new_user->username }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.name') }}</td>
                            <td>{{ $__new_user->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.nid') }}</td>
                            <td>{{ $__new_user->nid }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td>
                            <td>{{ $__new_user->email }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.mobile') }}</td>
                            <td>{{ $__new_user->mobile}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.address1') }}</td><td>{{ $__new_user->address1 }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.address2') }}</td><td>{{ $__new_user->address2 }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.city') }}</td><td>{{ $__new_user->city }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.zip') }}</td><td>{{ $__new_user->zip }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.state') }}</td><td>{{ $__new_user->state }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.country') }}</td><td>{{$__fields['country']['options'][$__new_user->country]}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.code-name') }}</td><td>{{trans('general.activation.title.'.$__activation->code)}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.wallet-pay') }}</td>
                            <td>
                                @if (count($__use_wallet))
                                    <ul>
                                    @foreach ($__use_wallet as $_wallet => $_wallet_amount)
                                        <li>{{ trans("general.wallet.".$_wallet) }} : {{ number_format($_wallet_amount, 2) }}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="{{ url('/member/franchise/register') }}" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif ($__step == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.franchise-account-register.bar-title.register-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.user.username') }}</td>
                            <td>{{old('username')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.name') }}</td>
                            <td>{{ $__franchise_main_user->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.nid') }}</td>
                            <td>{{ $__franchise_main_user->nid }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.email') }}</td>
                            <td>{{old('email')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.mobile') }}</td>
                            <td>{{old('mobile')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.address1') }}</td>
                            <td>{{ old('address1') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.address2') }}</td>
                            <td>{{ old('address2') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.city') }}</td>
                            <td>{{ old('city') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.zip') }}</td>
                            <td>{{ old('zip') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.state') }}</td>
                            <td>{{ old('state') }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.country') }}</td>
                            <td>{{$__fields['country']['options'][$__franchise_main_user->country]}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.ref') }}</td>
                            <td>{{old('ref')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.up') }}</td>
                            <td>{{old('up')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.user.up_pos') }}</td>
                            <td> {{trans('general.user.up_pos.'.old('up_pos'))}} </td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.code-name') }}</td>
                            <td>{{trans('general.activation.title.'.old('code'))}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.activation.wallet-pay') }}</td>
                            <td>
                                @if (count($__use_wallet))
                                    <ul>
                                    @foreach ($__use_wallet as $_wallet => $_wallet_amount)
                                        <li>{{ trans("general.wallet.".$_wallet) }} : {{ number_format($_wallet_amount, 2) }}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" action="{{ url('/member/franchise/register') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__confirm' value='1'>
						<input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                        <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
                        @foreach (array('username', 'name', 'nid', 'email', 'mobile', "address1", "address2", "city", "zip", "state", 'country', 'ref', 'up', 'up_pos', 'code', 'password', 'password_confirmation', 'password2', 'password2_confirmation') as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                    {{--##### END Hidden Form ####--}}
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}
            
            @elseif ($__step == 'start')
            {{-- ######## START ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.franchise-account-register.bar-title.register') }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('/member/franchise/register') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.username') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.username') }}" class="form-control" name='username' value='{{old('username')}}'>
                                <span class="help-block m-b-none">{{ trans('general.page.user.register.content.username-helper') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ trans('field.user.name') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $__franchise_main_user->name }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nid" class="col-md-4 control-label">{{ trans('field.user.nid') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $__franchise_main_user->nid }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.email') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.email') }}" class="form-control" name='email' value='{{ empty(old('email'))? $__franchise_main_user->email:old('email') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nid" class="col-md-4 control-label">{{ trans('field.user.mobile') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control"  name="phone_code">
                                    <option value=''>-- {{ trans('general.field.phone_code') }} --</option>
                                @foreach ($__fields['phone_code']['options'] AS $_code=>$_phone_code)
                                    <option value='{{ $_code }}' phone-code='{{ $_phone_code['phone_code'] }}'>{{ $_phone_code['phone_code_concat'] }}</option>
                                @endforeach
                                </select>

                                <input type="text" placeholder="{{ trans('field.user.mobile') }}" class="form-control" name='mobile' value='{{ empty(old('mobile'))? $__franchise_main_user->mobile:old('mobile') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address1') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address1') }}" class="form-control" name='address1' value='{{ empty(old('address1'))? $__franchise_main_user->address1:old('address1') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.address2') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.address2') }}" class="form-control" name='address2' value='{{ empty(old('address2'))? $__franchise_main_user->address2:old('address2') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.city') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.city') }}" class="form-control" name='city' value='{{ empty(old('city'))? $__franchise_main_user->city:old('city') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.zip') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.zip') }}" class="form-control" name='zip' value='{{ empty(old('zip'))? $__franchise_main_user->zip:old('zip') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.state') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.state') }}" class="form-control" name='state' value='{{ empty(old('state'))? $__franchise_main_user->state:old('state') }}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.country') }}</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $__fields["country"]["options"][$__user->country] }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.ref') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.ref') }}" class="form-control" name='ref' value='{{old('ref')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='ref'>{{ trans('general.button.check') }}</button>
                                <span id='ref-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.user.up') }}" class="form-control" name='up' value='{{old('up')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='up'>{{ trans('general.button.check') }}</button>
                                <span id='up-text'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.up_pos') }} <span style="color:red;">*</span></label>
							<div class="col-md-6">
								<div class="i-checks">
									<label> 
										<input type="radio" value="1" name="up_pos" {{ empty(old('up_pos')) || old('up_pos')=='1'?'checked':''}} > <i></i> {{ trans('general.user.up_pos.1') }} 
									</label>
								</div>
								<div class="i-checks">
									<label>
										<input type="radio" value="2" name="up_pos" {{ old('up_pos')=='2'?'checked':''}}> 	<i></i> {{ trans('general.user.up_pos.2') }}  
									</label>
								</div>
							</div>
                        </div>

                        <div class="form-group" >
                            <label class="col-md-4 control-label">{{ trans('field.activation.code-name') }} <span style="color:red;">*</span></label>
							<div class="col-md-6">
                               
                                <select class="form-control" name="code">
                                @foreach ($__fields['code']['options'] AS $_code=>$_details)
                                    <option value='{{ $_code }}' {{ old('code')==$_code?'selected':'' }}>
                                        {{ trans('general.activation.title.'.$_code) }} | {{ trans('field.activation.price') }} : {{ number_format($_details['price'], 2) }} | {{ trans('field.activation.amount') }} : {{ number_format($_details['amount'], 2) }}
                                    </option>
                                @endforeach
                                </select>
                                <span class="help-block m-b-none">
                                    {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->awallet, 'wallet'=>trans('general.wallet.awallet')]) }} <br>
                                    {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->fwallet, 'wallet'=>trans('general.wallet.fwallet')]) }}
                                </span>
							</div>
                        </div>

						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password') }}" class="form-control" name='password' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password_confirmation') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password_confirmation') }}" class="form-control" name='password_confirmation' value=''>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2') }}" class="form-control" name='password2' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.password2_confirmation') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.password2_confirmation') }}" class="form-control" name='password2_confirmation' value=''>
                            </div>
                        </div>

                        <hr/ >
						
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END START ########### --}}

            @elseif ($__step == 'package')
            {{-- ######## PACKAGE ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.franchise-account-register.bar-title.package') }}
                </div>
                
                <div class="panel panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>   
                            <tr>
                                <th>{{ trans('field.activation.code-name') }}</th>
                                <th>{{ trans('field.activation.price') }}</th>
                                <th>{{ trans('general.page.user.register.content.active-account') }} / {{ trans('general.page.user.register.content.reserved-account') }}</th>
                                <th>{{ trans('field.activation.amount') }}</th>
                                <th>
                                    {{ trans('general.page.user.register.content.package-compliment') }}
                                </th>
                                <th>
                                    {{ trans('general.table.header.action') }}
                                </th>
                            </tr>
                            </thead>   

                            @foreach ($__fields['code']['options'] as $_code=>$_details)
                            <tr>
                                <td><img src="{{ asset('assets/images/rank-'.$_code.'.png') }}" alt="{{ trans('general.activation.title.'.$_code) }}"></td>
                                <td>{{ number_format($_details['price'], 2) }}</td>
                                <td>{{ trans('general.page.user.register.content.active-account') }}: 1 <br/> {{ trans('general.page.user.register.content.reserved-account') }}: 6</td>
                                <td>{{ number_format($_details['amount'], 2) }}</td>

                                @if (isset($_details['get_wallet']))
                                    <td>
                                    @foreach($_details['get_wallet'] as $_wallet=>$_amount)
                                        {{ trans('general.wallet.'.$_wallet) }} : {{ number_format($_amount, 2) }}<br/>
                                    @endforeach
                                    </td>
                                @endif

                                <td>
                                    <form class="form-horizontal" role="form" action="{{ url('/member/franchise/register') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="__req" value="package">
                                        <input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                                        <input type="hidden" name="code" value="{{ $_code }}">
                                        <input type="hidden" name="ref" value="{{ $__user->username }}">
                                        <input type="hidden" name="country" value="{{ $__user->country }}">
                                        <input type="hidden" name="up" value="{{ old('up') }}">
                                        <input type="hidden" name="up_pos" value="{{ old('up_pos') }}">
                                        <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.register') }}</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <span class="help-block m-b-none">
                    {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->awallet, 'wallet'=>trans('general.wallet.awallet')]) }} <br>
                    {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->fwallet, 'wallet'=>trans('general.wallet.fwallet')]) }}
                    </span>
                </div>
            </div>
            {{-- ######## END PACKAGE ########## --}}
            @endif
        
            {{-- ######## NOTED ########## --}}
            <div class="panel panel-info">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.register.bar-title.noted') }}
                </div>
                <div class="panel panel-body">
                    {!! nl2br(e(trans('general.page.user.register.content.noted'))) !!}
                </div>
            </div>
            {{-- ######## END NOTED ########## --}}
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=ref]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        // check placement
        $( "button[for=up]" ).on("click", function(){
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if(username!=''){
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('placement/user-info') }}",
                    success: function (dataJson) {
                        if(dataJson.status){
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else{
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else{
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });
        
        //phone code helper
        $("select[name=phone_code]").on("change", function(){
            var val = $($(this).find('option[value='+$(this).val()+']')).attr('phone-code');
            $("input[name=mobile][type=text]").val(val);
        });

        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection