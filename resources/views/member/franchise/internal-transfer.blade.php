@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.wallet-internal-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)])))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.wallet-internal-transfer.bar-title.wallet-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/franchise/'.$__wallet.'/internal-transfer') }}">{{ trans('general.pagetitle.user.wallet-internal-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</a>
				</li>
			</ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
			<h3 class="text-center text-danger" style="font-weight:800;">{{ "***".trans("general.page.user.wallet-internal-transfer.content.you-are-at-wallet-management-page", ['wallet'=>trans('general.wallet.'.$__wallet)])."***" }}</h3>
            
            <div class="panel panel-default">
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <thead>
                            <tr>
                                <th>{{ trans("general.table.header.type") }}</th>
                                <th>{{ trans("field.user.username") }}</th>
                                <th>{{ trans("field.user.rank-bonus") }}</th>
                                <th>{{ trans("general.wallet.".$__wallet) }}</th>
                            </tr>
                        </thead>
                        @if(count($__franchise_user_list["main"])>0)
                            <?php $count=0; ?>
                            @foreach($__franchise_user_list["main"] as $_id=>$_user)
                            <tr>
                                <?php $count++; ?>
                                @if($count===1)
                                <td rowspan={{ count($__franchise_user_list["main"]) }}>{{ trans("general.page.user.wallet-internal-transfer.content.franchise-main-account") }}</td>
                                @endif

                                <td>{{ $_user["username"] }}</td>
                                <td>{{ trans("general.user.rank.title.".max($_user["rank"], $_user["crank"])) }}</td>
                                <td>{{ number_format($_user[$__wallet], 4) }}</td>
                            </tr>
                            @endforeach
                        @endif

                        @if(count($__franchise_user_list["franchise"])>0)
                            <?php $count=0; ?>
                            @foreach($__franchise_user_list["franchise"] as $_id=>$_user)
                            <tr>
                                <?php $count++; ?>
                                @if($count===1)
                                <td rowspan={{ count($__franchise_user_list["franchise"]) }}>{{ trans("general.page.user.wallet-internal-transfer.content.franchise-account") }}</td>
                                @endif

                                <td>{{ $_user["username"] }}</td>
                                <td>{{ trans("general.user.rank.title.".max($_user["rank"], $_user["crank"])) }}</td>
                                <td>{{ number_format($_user[$__wallet], 4) }}</td>
                            </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
		</div>
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">

            @if($__step == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-internal-transfer.bar-title.wallet-transfer-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.wallet_record.from-username') }}</td><td>{{ $__post["from_username"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.to-username') }}</td><td>{{ $__post["to_username"] }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.amount') }}</td><td>{{ number_format($__post['amount'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.remarks') }}</td><td>{{ $__post['remarks'] }}</td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="{{ url('/member/franchise/'.$__wallet.'/internal-transfer') }}" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif($__step == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-internal-transfer.bar-title.wallet-transfer-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.wallet_record.from-username') }}</td><td>{{old('from_username')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.to-username') }}</td><td>{{old('to_username')}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.amount') }}</td><td>{{ number_format(old('amount'), 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.wallet_record.remarks') }}</td><td>{{old('remarks')}}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" action="{{ url('/member/franchise/'.$__wallet.'/internal-transfer') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__confirm' value='1'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
                        @foreach(["from_username", 'to_username', 'amount', 'remarks'] as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
                        @endforeach

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}
            
            @elseif($__step == 'start')
            {{-- ######## START ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-internal-transfer.bar-title.wallet-transfer', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}
                </div>
                
                <div class="panel panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('/member/franchise/'.$__wallet.'/internal-transfer') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.from-username') }}</label>
                            <div class="col-md-6">
                               
                                <select class="form-control" name="from_username">
                                @foreach($__fields['from_username']['options'] AS $_code=>$_from_username)
                                    <option value='{{ $_code }}' {{ ((old('from_username')) == $_code ) ?'selected':'' }}>{{ $_from_username }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.to-username') }}</label>
                            <div class="col-md-6">
                               
                                <select class="form-control" name="to_username">
                                @foreach($__fields['to_username']['options'] AS $_code=>$_to_username)
                                    <option value='{{ $_code }}' {{ ((old('to_username')) == $_code ) ?'selected':'' }}>{{ $_to_username }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.amount') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.amount') }}" class="form-control" name='amount' value='{{old('amount')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.remarks') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.remarks') }}" class="form-control" name='remarks' value='{{old('remarks')}}'>
                            </div>
                        </div>

                        <hr/>
						
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END START ########### --}}
            @endif
		</div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection