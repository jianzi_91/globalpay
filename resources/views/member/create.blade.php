@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.register')))

@section('content')

<div class="page-title">{{ trans('general.page.user.register.bar-title.register') }}</div>
   
@include('member.common.success')
@include('member.common.errors')

    @if ($__step == "error")
        <div class="general-container">
            <div class="styled-heading">{{ trans('general.page.user.register.bar-title.register') }}</div>
            <p>{{ $errmsg }}</p>
        </div>
    @else
        
    @if ($__step == 'confirmed')
        {{-- ######## SUCCESS ########## --}}
        <div class="general-container">
            <div class="styled-heading">{{ trans('general.page.user.register.bar-title.account-info') }}</div>

            <div class="form-filled">
                <div class="form-group">
                    <label>{{ trans('field.user.username') }}</label>
                    <div class="form-content">{{ $__new_user->username }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.name') }}</label>
                    <div class="form-content">{{ $__new_user->name }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.nid') }}</label>
                    <div class="form-content">{{ $__new_user->nid }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.email') }}</label>
                    <div class="form-content">{{ $__new_user->email }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.mobile') }}</label>
                    <div class="form-content">{{ $__new_user->mobile}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.country') }}</label>
                    <div class="form-content">{{ isset($__fields['country']['options'][$__new_user->country]) ? $__fields['country']['options'][$__new_user->country] : "" }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.locale') }}</label>
                    <div class="form-content">{{ isset($__fields['locale']['options'][$__new_user->locale])?$__fields['locale']['options'][$__new_user->locale]:"" }}</div>
                </div>
            </div>

            <div class="form-action">
                <a href="{{ url('/member/register') }}" class="btn btn-primary">{{ trans('general.button.next') }}</a>
            </div>
            
        </div>

        {{-- ######## END SUCCESS ########## --}}

    @elseif ($__step == 'preview')
        {{-- ######## PREVIEW ########## --}}
        <div class="general-container">
            <div class="styled-heading">{{ trans('general.page.user.register.bar-title.register-review') }}</div>

            <div class="form-filled">
                <div class="form-group">
                    <label>{{ trans('field.user.name') }}</label>
                    <div class="form-content">{{old('name')}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.nid') }}</label>
                    <div class="form-content">{{old('nid')}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.email') }}</label>
                    <div class="form-content">{{old('email')}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.mobile') }}</label>
                    <div class="form-content">{{old('mobile')}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.country') }}</label>
                    <div class="form-content">{{ isset($__fields['country']['options'][old('country')]) ? $__fields['country']['options'][old('country')] : "" }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.locale') }}</label>
                    <div class="form-content">{{ isset($__fields['locale']['options'][old('locale')]) ? $__fields['locale']['options'][old('locale')] : "" }}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.up') }}</label>
                    <div class="form-content">{{old('up')}}</div>
                </div>
                <div class="form-group">
                    <label>{{ trans('field.user.up_pos') }}</label>
                    <div class="form-content">{{trans('general.user.up_pos.'.old('up_pos'))}}</div>
                </div>
              
            </div>      
                    
            {{--##### Hidden Form ####--}}
            <form class="form-horizontal" role="form" action="{{ url('/member/register') }}" method="POST">
                {{ csrf_field() }}
                <input type='hidden' name='__req' value='1'>
                <input type='hidden' name='__confirm' value='1'>
                <input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">
                <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>
                @foreach (array('name', 'nid', 'email', 'mobile', 'country', 'up', 'up_pos', 'password', 'password_confirmation', 'password2', 'password2_confirmation', "locale") as $_field)
                    <input type='hidden' name='{{ $_field }}' value='{{ old($_field) }}'>
                @endforeach

                <div class="form-action">
                    <button class="btn btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                    <button class="btn btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                </div>
            </form>
            {{--##### END Hidden Form ####--}}
        </div>
                
    {{-- ######## END PREVIEW ########## --}}

    @elseif($__step == 'start')
        {{-- ######## START ########## --}}

        {{-- ######## NOTED ########## --}}
        <div class="alert alert-info">
            <strong>{{ trans('general.page.user.register.bar-title.noted') }}</strong>
            <p>{!! nl2br(e(trans('general.page.user.register.content.noted'))) !!}</p>
        </div>
        {{-- ######## END NOTED ########## --}}

        <div class="general-container">
            <div class="styled-heading">{{ trans('general.page.user.register.bar-title.register') }}</div>
            
            <form role="form" action="{{ url('/member/register') }}" method="POST">
                {{ csrf_field() }}
                <input type='hidden' name='__req' value='1'>
                <input type='hidden' name='__nonce' value="{{ $__fields['__nonce']['value'] }}">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="control-label">{{ trans('field.user.name') }} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name='name' value='{{old('name')}}'>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nid" class="control-label">{{ trans('field.user.nid') }} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name='nid' value='{{old('nid')}}'>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.email') }} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name='email' value='{{old('email')}}'>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nid" class="control-label">{{ trans('field.user.mobile') }} <span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name='mobile' value='{{old('mobile')}}'>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.country') }} <span style="color:red;">*</span></label>
                            <select class="form-control" name="country">
                            @foreach ($__fields['country']['options'] AS $_code=>$_country)
                                <option value='{{ $_code }}' {{ ((old('country', auth()->user()->country)) == $_code ) ?'selected':'' }}>{{ $_country }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.locale') }}</label>
                            <select class="form-control" name="locale">
                            @foreach ($__fields['locale']['options'] AS $_code=>$_locale)
                                <option value='{{ $_code }}' {{ ((old('locale')) == $_code ) ?'selected':'' }}>{{ $_locale }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.up') }} <span style="color:red;">*</span></label>
                            <div class="input-container">
                                <input type="text" class="form-control" name='up' value='{{old('up')}}'>
                                <button class="btn btn-sm btn-primary" type="button" for='up'>{{ trans('general.button.check') }}</button>
                            </div>
                            <span id='up-text'></span>
                        </div>
                    </div>
                    {{-- <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.up_pos') }} <span style="color:red;">*</span></label>
                            <label class="radio radio-inline"> 
                                <input type="radio" value="1" name="up_pos" {{ empty(old('up_pos')) || old('up_pos')=='1'?'checked':''}} > 
                                <span>{{ trans('general.user.up_pos.1') }}</span>
                            </label>
                            <label class="radio radio-inline">
                                <input type="radio" value="2" name="up_pos" {{ old('up_pos')=='2'?'checked':''}}> 	
                                <span>{{ trans('general.user.up_pos.2') }}</span>
                            </label>
                        </div>
                    </div> --}}
                    <input type="hidden" value="1" name="up_pos">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.password') }} <span style="color:red;">*</span></label>
                            <input type="password" class="form-control" name='password' value=''>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.password_confirmation') }} <span style="color:red;">*</span></label>
                            <input type="password" class="form-control" name='password_confirmation' value=''>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.password2') }} <span style="color:red;">*</span></label>
                            <input type="password" class="form-control" name='password2' value=''>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{ trans('field.user.password2_confirmation') }} <span style="color:red;">*</span></label>
                            <input type="password" class="form-control" name='password2_confirmation' value=''>
                        </div>
                    </div>
                </div>

                <hr/ >
                    
                <div class="form-group">
                    <label class="control-label">{{ trans('field.user.epassword') }} <span style="color:red;">*</span></label>
                    <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                </div>

                <div class="form-action">
                    <button class="btn btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                </div>
            </form>
            
        </div>
        {{-- ######## END START ########### --}}

    @endif
        
   
    
@endif
        
    
@endsection

@section('script')
<script>
    $( document ).ready(function () {
        // check username
        $( "button[for=up]" ).on("click", function() {
            var field = $(this).attr('for');
            var username = $('input[name='+field+'][type=text]').val();
            var output_field = $('#'+field+'-text');
            output_field.removeClass('text-danger').html();

            if (username!='') {
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    data: { 'username': username},
                    url: "{{ url('general/user-info') }}",
                    success: function (dataJson) {
                        if (dataJson.status) {
                            output_field.html('<br>' + dataJson.msg);
                        }
                        else {
                            output_field.html('<br>' + dataJson.msg).addClass('text-danger');
                        }
                    },
                    error: function(data) {
                        var errors = data.responseJSON;
                        output_field.html('<br>' + "{{ trans('ajax.errmsg.fail') }}").addClass('text-danger');
                    }
                });
            }
            else {
                output_field.html('<br>' + "{{ trans('ajax.errmsg.username-required') }}").addClass('text-danger');
            }
        });

        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection