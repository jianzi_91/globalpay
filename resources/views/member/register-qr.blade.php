@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.register')))

@section('content')

<div class="page-title">{{ trans('general.page.user.register.bar-title.register') }}</div>
   
@include('member.common.success')
@include('member.common.errors')

    <div class="general-container">
            
        

        <div class="qr-container">

            <div class="image-container">
                <img class="" src="/assets/images/gpay_logo.png">
            </div>

            {!! QrCode::color(7,144,126)->size(250)->generate(url('/register?referrer-code=' . auth()->user()->username)); !!}

            <div class="qr-heading">@lang('general.page.user.register.content.scan-hint')</div>
        </div>
        
    </div>
    
@endsection