@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)])))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h2>{{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</h2>
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/member/home') }}">{{ trans('general.menu.user.home') }}</a>
				</li>
				<li class="active">
					<a href="{{ url('/member/'.$__wallet.'/wallet-withdraw') }}">{{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}</a>
				</li>
			</ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
			@include('member.common.success')
			@include('member.common.errors')
		</div>
        <div class="col-md-12">

            @if($__step == 'confirmed')
            {{-- ######## SUCCESS ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw-info') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_country') }}</td>
                            <td>{{ isset($__fields['country']['options'][$__user->country])?$__fields['country']['options'][$__user->country]:'' }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_name') }}</td><td>{{ $__user->bank_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_payee_name') }}</td><td>{{ $__user->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_acc_no') }}</td><td>{{ $__user->bank_acc_no }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_branch_name') }}</td><td>{{ $__user->bank_branch_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_sorting_code') }}</td><td>{{ $__user->bank_sorting_code }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_iban') }}</td><td>{{ $__user->bank_iban }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.amount') }}</td><td>{{ number_format($__info['amount'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.receivable_amount') }}</td><td>{{ number_format($__info['receivable_amount'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.fee') }}</td><td>{{ number_format($__info['fee'], 2) }}</td>
                        </tr>
                        @if($__info["withdraw_currency"])
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_amount') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format( $__info['amount_currency'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_receivable_amount') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format($__info['receivable_amount_currency'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_fee') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format($__info['fee_currency'], 2) }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>{{ trans('field.withdrawal.remarks') }}</td><td>{{ $__post['remarks'] }}</td>
                        </tr>
                    </table>

                    <hr />

                    <div class="col-md-12">
                        <a href="{{ url('/member/'.$__wallet.'/wallet-withdraw') }}" class="btn btn-sm btn-primary">{{ trans('general.button.next') }}</a>
                    </div>
                </div>
            </div>
            
            {{-- ######## END SUCCESS ########## --}}

            @elseif($__step == 'preview')
            {{-- ######## PREVIEW ########## --}}
            <div class="panel panel-default">

                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw-review') }}
                </div>

                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_country') }}</td>
                            <td>{{ isset($__fields['country']['options'][$__user->country])?$__fields['country']['options'][$__user->country]:'' }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_name') }}</td><td>{{ $__user->bank_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_payee_name') }}</td><td>{{ $__user->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_acc_no') }}</td><td>{{ $__user->bank_acc_no }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_branch_name') }}</td><td>{{ $__user->bank_branch_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_sorting_code') }}</td><td>{{ $__user->bank_sorting_code }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_iban') }}</td><td>{{ $__user->bank_iban }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.amount') }}</td><td>{{ number_format( $__info['amount'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.receivable_amount') }}</td><td>{{ number_format($__info['receivable_amount'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.fee') }}</td><td>{{ number_format($__info['fee'], 2) }}</td>
                        </tr>
                        @if($__info["withdraw_currency"])
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_amount') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format( $__info["amount_currency"], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_receivable_amount') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format($__info['receivable_amount_currency'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.currency_fee') }}</td><td>{{ $__info["withdraw_currency_display"] }} {{ number_format($__info['fee_currency'], 2) }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>{{ trans('field.withdrawal.remarks') }}</td><td>{{ $__fields['remarks']["value"] }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    {{--##### Hidden Form ####--}}
                    <form class="form-horizontal" role="form" action="{{ url('/member/'.$__wallet.'/wallet-withdraw') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__confirm' value='1'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>
                        @if($__info["withdraw_currency"])
						    <input type='hidden' name='receivable_amount' value='{{ $__fields["receivable_amount"]["value"] }}'>
                        @else
						    <input type='hidden' name='amount' value='{{ $__fields["amount"]["value"] }}'>
                        @endif

                        @foreach(['remarks'] as $_field)
						    <input type='hidden' name='{{ $_field }}' value='{{ $__fields[$_field]["value"] }}'>
                        @endforeach
                        <input type='hidden' name='epassword' value='{{ array_last(session("epassword_nonce", [])) }}'>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-primary" type="button" id="back-button">{{ trans('general.button.back') }}</button>
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            {{-- ######## END PREVIEW ########## --}}
            
            @elseif($__step == 'start')
            {{-- ######## START ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}
                </div>
                
                <div class="panel panel-body">
                    <table class='table table-striped'>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_country') }}</td>
                            <td>{{ isset($__fields['country']['options'][$__user->country])?$__fields['country']['options'][$__user->country]:'' }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_name') }}</td><td>{{ $__user->bank_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_payee_name') }}</td><td>{{ $__user->name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_acc_no') }}</td><td>{{ $__user->bank_acc_no }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_branch_name') }}</td><td>{{ $__user->bank_branch_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_sorting_code') }}</td><td>{{ $__user->bank_sorting_code }}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('field.withdrawal.bank_iban') }}</td><td>{{ $__user->bank_iban }}</td>
                        </tr>
                    </table>
                    
                    <hr />
                    <form class="form-horizontal" role="form" action="{{ url('/member/'.$__wallet.'/wallet-withdraw') }}" method="POST">
						{{ csrf_field() }}
						<input type='hidden' name='__req' value='1'>
						<input type='hidden' name='__nonce' value='{{ $__fields["__nonce"]["value"] }}'>

                        @if($__info["withdraw_currency"])
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.withdrawal.receivable_amount') }}</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $__info["withdraw_currency_display"] }}</span>
                                    <input type="text" placeholder="{{ trans('field.withdrawal.receivable_amount') }}" class="form-control" name='receivable_amount' value='{{ $__fields['receivable_amount']["value"] }}'>
                                    <span class="help-block m-b-none">
                                </div>
                                {{ trans('general.page.user.general.wallet-balance', ['amount'=> $__info["withdraw_currency_display"]." ".number_format($__user[$__wallet."-currency"], 4)." (".$wallets->$__wallet.")", 'wallet'=>trans('general.wallet.'.$__wallet)]) }}
                                </span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel panel-body">
                                        {{ trans('general.page.user.wallet-withdraw.content.your-withdraw-max') }}: {{ $__info["withdraw_currency_display"] }} {{ number_format($__info["withdraw_currency_max"], 2) }}<br />
                                        {{ trans('general.page.user.wallet-withdraw.content.withdraw-min') }}: {{ $__info["withdraw_currency_display"] }} {{ number_format($__info["withdraw_currency_min"], 2) }}<br />
                                        {{ trans('general.page.user.wallet-withdraw.content.withdraw-fee') }}: {{ number_format($__info["withdraw_fee"]) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.withdrawal.amount') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.withdrawal.amount') }}" class="form-control" name='amount' value='{{ $__fields['amount']["value"] }}'>
                                <span class="help-block m-b-none">
                                {{ trans('general.page.user.general.wallet-balance', ['amount'=>$wallets->$__wallet, 'wallet'=>trans('general.wallet.'.$__wallet)]) }}
                                </span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel panel-body">
                                        {{ trans('general.page.user.wallet-withdraw.content.your-withdraw-max') }}: {{ number_format($__info["withdraw_max"], 2) }}<br />
                                        {{ trans('general.page.user.wallet-withdraw.content.withdraw-min') }}: {{ number_format($__info["withdraw_min"], 2) }}<br />
                                        {{ trans('general.page.user.wallet-withdraw.content.withdraw-fee') }}: {{ number_format($__info["withdraw_fee"]) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.wallet_record.remarks') }}</label>
                            <div class="col-md-6">
                                <input type="text" placeholder="{{ trans('field.wallet_record.remarks') }}" class="form-control" name='remarks' value='{{ $__fields['remarks']["value"] }}'>
                            </div>
                        </div>

                        <hr/>
						
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('field.user.epassword') }}</label>
                            <div class="col-md-6">
                                <input type="password" placeholder="{{ trans('field.user.epassword') }}" class="form-control" name='epassword' value=''>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <button class="btn btn-sm btn-primary" type="submit">{{ trans('general.button.proceed') }}</button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
            {{-- ######## END START ########### --}}
            
            @elseif($__step == 'initialize')
            {{-- ######## INITIALIZE ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}
                </div>
                
                <div class="panel panel-body">
                    <p class="text-error">
                        {!! trans('general.page.user.wallet-withdraw.content.initialize-bank-info', ['link'=>"<a href='".url("/member/profile")."'>", 'end_link'=>"</a>"]) !!}
                    </p>
				</div>
			</div>
            {{-- ######## END INITIALIZE ########### --}}

            @elseif ($__step == "upline-block")
            {{-- ######## UPLINE BLOCK ########## --}}
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.wallet-withdraw', ['wallet'=>trans('general.wallet.'.$__wallet)]) }}
                </div>
                
                <div class="panel panel-body">
                    <div class="alert alert-danger">
                        <strong>{{ trans('general.page.user.wallet-withdraw.content.upline-block') }}</strong>
                    </div>
				</div>
			</div>
            {{-- ######## END UPLINE BLOCK ########### --}}
            @endif

            @if ($__step != "upline-block")
            {{-- ######## NOTED ########## --}}
            <div class="panel panel-info">
                <div class="panel panel-heading">
                    {{ trans('general.page.user.wallet-withdraw.bar-title.noted') }}
                </div>
                <div class="panel panel-body">
                    {!! nl2br(e(trans('general.page.user.wallet-withdraw.content.noted'))) !!}
                </div>
            </div>
            {{-- ######## END NOTED ########## --}}
            @endif
		</div>
    </div>
</div>
@endsection

@section("script")
<script>
    $(document).ready(function() {
        //back button
        $("#back-button").click(function(e) {
            $("input[name=__confirm]").val(0);
            var form = $(e.target).closest('form');
            $(form).submit();
        });

        var postCounter = 0;
        $("form[method=post]").submit(function (e) {
            if (postCounter++ == 0) {
                $("form[method=post]").find("[type=submit]").attr("disabled", true);
            }
            else {
                e.preventDefault();
            }
        });
    });
</script>
@endsection