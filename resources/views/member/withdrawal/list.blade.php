@extends('member.layouts.app')
@section('title', e(trans('general.pagetitle.user.wallet-withdraw-history', array('wallet'=>trans('general.wallet.'.$__wallet)))))

@section('content')
<div class="container">
    <h2>{{ trans('general.page.user.wallet-withdraw-history.bar-title.wallet-withdraw-history', array('wallet'=>trans('general.wallet.'.$__wallet))) }}</h2>

<div class='row'>
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
             @include('member.common.success')
            <div class="ibox">
                <div class="ibox-content">
                    
                    <div>
                        {{ $__records->total() }} {{ trans('general.table.listing.record') }}
                    </div>

                    <div class="table-responsive">
                        <table class="footable table table-striped" data-page-size="20" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>{{ trans('general.table.header.date') }}</th>
                                    <th>{{ trans('general.table.header.amount') }}</th>
                                    <th>{{ trans('general.table.header.bank-info') }}</th>
                                    <th>{{ trans('general.table.header.status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($__records as $_record)
                                <tr>
                                    <td>{{ $_record->created_at_op }}</td>
                                    <td>
                                        {{ trans("field.withdrawal.amount") }}: {{ number_format($_record->amount, 2) }} <br />
                                        {{ trans("field.withdrawal.receivable_amount") }}: {{ number_format($_record->receivable_amount, 2) }} <br />
                                        {{ trans("field.withdrawal.fee") }}: {{ number_format($_record->fee, 2) }}

                                        @if($_record->currency)
                                        <br />
                                        <br />
                                        {{ trans("field.withdrawal.currency_amount") }}: {{ $_record->currency }} {{ number_format($_record->currency_amount, 2) }} <br />
                                        {{ trans("field.withdrawal.currency_receivable_amount") }}: {{ $_record->currency }} {{ number_format($_record->currency_receivable_amount, 2) }} <br />
                                        {{ trans("field.withdrawal.currency_fee") }}: {{ $_record->currency }} {{ number_format($_record->currency_fee, 2) }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ trans("field.withdrawal.bank_country") }}: {{ $_record["bank_country-text"] }} <br />
                                        {{ trans("field.withdrawal.bank_name") }}: {{ $_record->bank_name }} <br />
                                        {{ trans("field.withdrawal.bank_payee_name") }}: {{ $_record->bank_payee_name }} <br />
                                        {{ trans("field.withdrawal.bank_acc_no") }}: {{ $_record->bank_acc_no }} <br />
                                        {{ trans("field.withdrawal.bank_branch_name") }}: {{ $_record->bank_branch_name }} <br />
                                        {{ trans("field.withdrawal.bank_sorting_code") }}: {{ $_record->bank_sorting_code }} <br />
                                        {{ trans("field.withdrawal.bank_iban") }}: {{ $_record->bank_iban }}
                                    </td>
                                    <td>
                                        {{ trans("field.withdrawal.status") }}: {{ trans("general.withdrawal.status.".$_record->status) }} <br />
                                        {{ trans("field.withdrawal.remarks") }}: {!! nl2br(e($_record->remarks)) !!}<br />
                                        {{ trans("field.withdrawal.aremarks") }}: {!! nl2br(e($_record->aremarks)) !!}<br />
                                        {{ trans("field.withdrawal.updated_at") }}: {{ nl2br(e($_record->updated_at_op)) }}<br />
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot style="display:none;">
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-right">{{ $__records->links() }}</div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection 

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {

        $('.footable').footable();

    });
</script>
@endsection 