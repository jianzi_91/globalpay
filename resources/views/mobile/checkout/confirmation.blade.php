<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>支付通</title>
        <link rel="shortcut icon" href="/assets/images/favicon.png">
        <link rel="stylesheet" href="/default/bootstrap-3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/globalpay_webview.css">
    </head>
    <body>
        <div class="page-wrap">
            <header class="main-header">
                <div class="container">
                    <div class="brand">
                        <img src="/assets/images/gpay.png" >
                    </div>
                </div>
            </header>

            <div class="content">
                <div class="container">
                    <div class="content-container">
                        <div class="primary full">
                            <form action="{{ route('checkout-confirmation-confirm') }}" method="post">
                                    {{ csrf_field() }}
                                <input type="hidden" name="token" value="{{ $data['token'] }}">
                                <input type="hidden" name="response_url" value="{{ $data['data']['response_url'] }}">

                                <div class="form-filled">
                                    <div class="form-group">
                                        <label class="control-label">{{ trans('general.field.transaction_id') }}</label>
                                    <p class="form-content">{{$data['data']['transaction_id']}}</p>
                                    <input type="hidden" name="transaction_id" value="{{ $data['data']['transaction_id'] }}">

                                    </div>
                                    @if(isset($data['data']['sc_amt']))
                                    <div class="form-group">
                                        <label class="control-label">{{ trans('general.field.sc_amt') }}</label>
                                        <p class="form-content">{{$data['data']['sc_amt']}}</p>
                                        <input type="hidden" name="sc_amt" value="{{ $data['data']['sc_amt'] }}">

                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="control-label">{{ trans('general.field.security_code') }}</label>
                                    <input type="text" name="security_code" class="form-control">
                                </div>

                                <div class="form-action">
                                    <button class="btn btn-primary" type="submit">{{ trans('general.button.confirm') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>

        </div>
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
        <script src="/default/bootstrap-3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/plugin/pace/pace.min.js"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/app.global-pay.js"></script>
        <script>
            $(function() {
                var url = new URL(window.location.href);
                $("#token").val(url.searchParams.get('token'));
            });
        </script>
    </body>
</html>
