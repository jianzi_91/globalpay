{{-- Node --}}

@if($__user)
{{--Up arrow--}}
@if (isset($__first) && $__first && isset($__user['user']['up-username']) && $__user['user']['up-username'])
    <a href='{{ '?uid='.$__user['user']['up-username'] }}' title="{{ trans("general.button.search") }}">
        <div class="fa fa-arrow-circle-up" aria-hidden="true"></div>
    </a>
@endif

<div class='inline'> {{-- data-tipped-options="inline: 'inline-tooltip-{{ $__user['user']['id'] }}'"> --}}

    <table class="table table-striped no-margin">
        <tr>
            <td style="word-break: break-all;">
                {{ $__user['user']['username'] }}
            </td>
        </tr>
        <tr>
            <td style="word-break: break-all;">
                {{ $__user['user']['mobile'] }}
            </td>
        </tr>
    </table>
</div>

@if ($__upgrade)
    {{-- <a href='{{ url('\member\upgrade-topup?uid='.$__user['user']['username']) }}' title="{{ trans("general.button.upgrade") }}">
        <div class="fa fa-btn fa-dollar" aria-hidden="true"></div>
    </a>--}}
@endif

{{--Down arrow--}}
@if (isset($__last) && $__last)
    <a href='{{ '?uid='.$__user['user']['mobile'] }}' title="{{ trans("general.button.search") }}">
        <div class="fa fa-arrow-circle-down" aria-hidden="true"></div>
    </a>
@endif

{{-- Tooltip --}}
{{-- <div style='display:none' class="node-tip" id="inline-tooltip-{{ $__user['user']['id'] }}">
    <table class="table table-bordered" style='background-color:white;'>
        <tr>
            <td>
                {{ $__user['user']['username'] }}
            </td>
            <td>
                {{ trans('general.user.up_pos.1') }}
            </td>
            <td>
                {{ trans('general.user.up_pos.2') }}
            </td>
        </tr>
        <tr>
            <td>
                {{ trans('general.page.genealogy.placement-node.content.accumulated') }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['leg1_acc_amt'], 2) }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['leg2_acc_amt'], 2) }}
            </td>
        </tr>
        <tr>
            <td>
                {{ trans('general.page.genealogy.placement-node.content.carry-forward') }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['leg1_cf'], 2) }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['leg2_cf'], 2) }}
            </td>
        </tr>
        <tr>
            <td>
                {{ trans('general.page.genealogy.placement-node.content.today-sales') }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['today_leg1_amt'], 2) }}
            </td>
            <td>
                {{ number_format($__user['stat_info']['today_leg2_amt'], 2) }}
            </td>
        </tr>
    </table>
</div> --}}

@else
   <div class='inline'>
    @if ($__register && isset($__register_info))
        &nbsp;
        {{-- <form id="placement-node-register-form-{{ $__register_info['up-username']."-".$__register_info['up_pos'] }}" method='post' action='{{ url('member/register') }}'>
			{{ csrf_field() }}
            <input type='hidden' name='up' value='{{$__register_info['up-username']}}'>
            <input type='hidden' name='up_pos' value='{{$__register_info['up_pos']}}'>
            <a href="javascript:{}" onclick="document.getElementById('placement-node-register-form-{{ $__register_info['up-username']."-".$__register_info['up_pos'] }}').submit();" title="{{ trans("general.button.register") }}">
                <div class="fa fa-btn fa-user" aria-hidden="true"></div>
            </a>
        </form> --}}
    @else
        &nbsp;
    @endif
   </div>
@endif