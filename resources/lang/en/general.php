<?php

return [

    'success' => 'Success',
    'attention' => 'Attention',
    'warning' => 'Warning',
    'error' => 'Error',
    'yes' => 'Yes',
    'no' => 'No',

    //General
    'table' => [
        'header' => [
            'id' => 'Reference ID',
            'date' => 'Date',
            'last-date' => 'Last Date',
            'wallet' => 'Wallet Type',
            'type' => 'Type',
            'status' => 'Status',
            'amount' => 'Amount',
            'balance' => 'Balance',
            'bank-info' => 'Bank Information',
            'descr' => 'Description',
            'action' => 'Action',
            'price' => 'Price',
            'qty' => 'Quantity',
            'country' => 'Country',
            'account_rank' => 'Account Rank',
            'account_type' => 'Account Type',
            'total_investment_amt' => 'Total Investment Amt',
            'max_investment_amt' => 'Max Investment Rate',
            'max_investment_rate' => 'Max Investment Rate',
            'max_tradeable_qty' => 'Max Tradeable Qty',
            'placed_qty' => 'Placed Qty',
            'sold_qty' => 'Sold Qty',
            'pending_qty' => 'Pending Qty',
            'sell_id' => "Sell ID",
            'sell_date' => 'Sell Date',
            'account_creation_date' => 'Account Creation Date',
            'last_transaction_date' => 'Last Transaction Date',
            'total_trade_income' => 'Total :trade_name Income',
            'total_pending_withdrawal' => 'Total Pending Withdrawal',
            'total_paid_withdrawal' => 'Total Paid Withdrawal',
            'count' => "Count",
            'member_count' => "Member Count",
            'package' => "Package",
            'account-type' => "Account Type",
            'bonus-type' => "Bonus Type",
            'number' => "No.", 
            'level' => 'Level',
            'leader_info' => 'Leader',
            'bv' => 'linkage value',
            'total_price' => 'Total Price',
            'unit_price' => 'Unit Price',
            'available_sells' => 'Available Sells',
            'available_buys' => 'Available Buys',
        ],
        'helper' => [
            "before" => "Before",
            "after" => "After",
        ],
        'listing' => [
            'record' => 'record(s).',
            'refresh' => 'Clear Search and Refresh.',
        ],
        'footer' => [
            'total' => 'Total',
            'grand_total' => 'Grand Total',
        ],
    ],

    'search_field' => [
        'field' => [
            'field-from' => ':field-from',
            'field-to' => ':field-to',
            'date-from' => ':field-from',
            'date-to' => ':field-to',
            'date' => "Date",
            'today' => "Today",
            'all_times' => "All times",
            'other' => "Other",
            'from' => "From",
            'to' => "To",
            'target_date' => 'Target Date',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'transcode' => 'Transaction code',
        ]
    ],

    'field' => [
        'buy_id' => 'Buy ID',
        'sell_id' => 'Sell ID',
        'phone_code' => 'Country Code',
        'captcha' => 'Captcha',
        'verification-code' => 'Verification Code',
        'quantity' => 'Quantity',
        'price' => 'Price',
        'processing-fee' => 'Processing',
        'charity-fund' => 'Charity',
        'receivable' => 'Receivable',
        'total-deduct' => 'Total Deduction',
        'type' => 'Type',
        'transaction_id' => 'Transaction ID',
        'a_amt' => 'Amount A',
        'a2_amt' => 'Amount A2',
        'sc_amt' => 'Amount SC',
        'security_code' => 'Security Code',
        'upload_receipt' => 'Upload Receipt',
        'new_image' => 'New Image',
        'no_data' => 'No Data',
        'continue' => 'Continue',
    ],

    'wallet'			=>	[
        'rwallet' => 'Register Wallet',
        "awallet" => "GP",
        "awallet2" => "Gold Wallet",
        'cwallet' => 'Cash Wallet',
        'wallet1' => 'P1 Point',
        'wallet2' => 'P2 Point',
        'swallet' => 'SP',
        'dowallet' => 'TP',
        'fwallet' => 'BP',
        'dwallet' => 'Debt Wallet',
        'iwallet' => 'Investment Wallet',
        'commission' => 'Commission Wallet',
        'trade-unit' => 'Tradable eShare',
        'trade-value' => 'eShare Value',
        'trade-float-unit' => 'Floating eShare',
        'coin' => 'Coin',
        'coins' => 'Coin(s)',
        'scwallet' => 'SC',
    ],

    'button'    =>  [
        'yes' => 'Yes',
        'no' => 'No',
        'add' => 'Add',
        'activate' => 'Activate',
        'update' => 'Update',
        'proceed' => 'Proceed',
        'submit' => 'Submit',
        'edit' => 'Edit',
        'back' => 'Back',
        'back-to-list' => 'Back To List',
        'register' => 'Register',
        'read' => 'Read',
        'upgrade' => 'Upgrade',
        'add-new' => 'Add New',
        'next' => 'Next',
        'terminate' => 'Terminate',
        'suspend' => 'Suspend',
        'unsuspend' => 'Unsuspend',
        'search' => 'Search',
        'check' => 'Check',
        'more-detail' => 'More Details',
        'login' => 'Login',
        'cancel' => 'Cancel',
        'confirm' => 'Confirm',
        'view' => 'View',
        'close' => 'Close',
        'click-here' => 'Click here',
        'redirect' => 'Redirect',
        'toggle_search_form' => "Show/Hide Search Form",
        "column_visibility" => "Column visibility",
        "restore_visibility" => "Restore visibility",
        'pay' => "Pay",
        'delete' => "Delete",
        'purchase' => "Purchase",
        'reject' => 'Reject',
        'request_TAC' => 'Request TAC',
        'buy' => 'Buy',
        'sell' => 'Sell',
        'add-selling-offer' => 'Add Selling Offer',
        'add-buying-offer' => 'Add Buying Offer',
        'my-offers' => 'My Offers',
        'trade-history' => 'Trade History',
        'view' => 'View',
        'view_receipt' => 'View Receipt',
    ],

    "selection" => [
        "please-select" => "-- Please Select --",
        "all" => "-- All --",
        'others' => 'Others',
        'please-specify' => 'Please Specify',
        'country' => [
            'china' => 'China',
            'malaysia' => 'Malaysia',
        ],
    ],

    // Option Sentence
    'activation' => [
        'title' => [
            '1100' => 'General',
            '2100' => 'Premium',
            '3100' => 'VIP',
        ],
        'status' => [
            '10' => 'Confirmed',
            '90' => 'Cancelled',
        ],
        'act_type' => [
            '10' => 'Normal',
            '30' => 'Loan',
            '50' => 'SA1',
            '70' => 'Zero BV',
            '90' => 'SA2',
        ],
        'share_convert' => [
            '0' => 'No',
            '10' => 'Yes',
        ],
        'distribute_after_loan' => [
            '0' => 'No',
            '10' => 'Yes',
        ],
        'distribute_after_loan2' => [
            '0' => 'No',
            '10' => 'Yes',
        ],
        'loan_status' => [
            "0" => "N/A", 
            "10" => "Settled",
            "30" => "Pending",
        ]
    ],

    'user' => [
        'up_pos' =>[
            0 => 'N/A',
            1 => 'Left',
            2 => 'Right',
        ],
        'rank' =>[
            'title' => [
                // 0 => 'User',
                // 1100 => 'General',
                // 2100 => 'Premium',
                // 3100 => 'VIP',
                0 => 'normal user',
                1000 => 'silver user',
                5000 => 'gold user',
                10000 => 'perkin user',
                50000 => 'diamond user',
                100000=> 'crown user',
            ]
        ],
        'status' =>[
            10 => 'Active',
            30 => 'Suspended',
            90 => 'Terminated',
        ],
        'dstatus' =>[
            0 => 'No',
            10 => 'In-Debt',
        ],
        'primary_acc' => [
            'n' => 'No',
            'y' => 'Yes',
        ],
        'company_status' => [
            0 => 'Normal Account',
            1 => 'Company Account',
        ],
        
        'ranking' => 'Ranking',
        'account-type' => [
            'normal' => 'Normal',
            'contra' => 'Loan',
            'zbv' => 'ZBV',
            'parking' => 'Parking',
            'free' => 'Free',
        ],
        'user_type' => [
            1 => 'normal user',
            2 => 'pan user',
            3 => 'wine user',
        ]
    ],

    'wallet-record'			=>	[
        'trans_code' => [
            '1001' => 'Wallet Top-Up',
            '1002' => 'Wallet Deduct',
            '1003' => 'Package Purchase',
            '1004' => 'Wallet Tranfer',
            '1005' => 'Finance Wallet Release',
            '1006' => 'Saving Wallet Release',
            '1007' => 'Sponsor Bonus',
            '1008' => 'Wallet Convert',
        ],
        'trans_code_descr' => [
            '1001' => "Admin top up :amount to your :wallet.\n\nRemarks: :remarks",
            '1002' => "Admin deduct :amount from your :wallet.\n\nRemarks: :remarks",
            '1003' => [
                'purchase' => 'Package (:package) purchase to user (:username). Deduct :amount from your :wallet.',
                'product' => 'Package (:package) purchase to user (:username). Add :amount to your :wallet.',
                'debt' => 'Loan package (:package) purchase to user (:username). Add :amount to your :wallet.',
            ],
            '1004' => [
                'from' => "Transfer :amount to user :username from your :wallet.\n\nRemarks: :remarks",
                'to' => "Transfer :amount from user :username to your :wallet.\n\nRemarks: :remarks",
            ],
            '1005' => 'User #:uid finance wallet released :amount points to trading wallet.',
            '1006' => 'User #:uid saving wallet released :amount points to trading wallet.',
            '1007' => 'UserID #:uid received :bonus sponsor bonus from downlineID #duid for spending :amount of premium wallet.',
            '1008' => [
                'from' => "Convert :amount to user :username :wallet2 from your :wallet.\n\nRemarks: :remarks",
                'to' => "Convert :amount from user :username :wallet2 to your :wallet.\n\nRemarks: :remarks",
            ],
        ],
        'type' => [
            'c' => 'Credit', 
            'd'=> 'Debit',
        ],
    ],

    'commission'			=>	[
        'type' => [
            '1001' => 'Active Bonus',
            
            '2001' => 'Passive Bonus',
        ],
        'status' => [
            10 => "Distributed",
            30 => "Pending",
            90 => "Error",
        ],
    ],

    "partner_transfer" => [
        "status" => [
            10 => "Confirmed",
            30 => "Pending",
            50 => "Processing",
            70 => "Cancelled",
            90 => "Error",
        ],
    ],

    'withdrawal'			=>	[
        'status' => [
            10 => "Paid",
            30 => "Pending",
            50 => "Processing",
            70 => "Cancelled",
        ],
    ],

    "member_status" => [
        "alliance_status" => [
            10 => "Yes",
            0 => "No",
        ],
    ],

    "news" => [
        "status" => [
            0 => "Not Publish",
            1 => "Publish",
        ],
    ],

    "exchange" => [
        "status" => [
            1 => "Active",
            0 => "Inactive",
        ],
    ],

    'admin' => [
        "privileges" => [
            "members/list" => "Member List",
            "members/create" => "Member - Add New",
            "members/edit-profile" => "Member - Edit Profile",
            "members/edit-sub-profile" => "Member - Edit Sub Account Profile",
            "members/edit-network" => "Member - Change Network",
            "members/edit-franchise" => "Member - Change Franchise Network",
            "members/send-welcome-email" => "Send Welcome Email",
            "members/sa1-list" => "Member SA1 Listing",
            "members/edit-passwords" => "Member Passwords Listing",
            "members/view-passwords" => "Member Passwords - View Passwords",
            "genealogy/sponsor" => "Referrer Tree",
            "genealogy/placement" => "Placement Tree",
            "uplines" => "Upline Listing",
            "networks/check-group" => "Network Relationship Check",
            "activations/list" => "Sales Listing",
            "bvfree/activations/add" => "Add Zero BV Activation",
            "loan/activations/add" => "Add Loan Activation",
            "special/activations/add" => "Add SA1",
            "group-activations" => "Group Sales Listing",
            "personal-activations" => "Personal Sales Listing",
            "small-group-activations-summary" => "Small Group Sales Summary",
            "activations-summary" => "Sales Summary",
            "activations/cancel-list" => "Sales Cancellation",
            "activations/cancel" => "Sales - Cancel",
            "loan-activations" => "Loan Account Listing",
            "loan-activations/settlement" => "Early Settlement",
            "loan-activations/distribute-loan-activation" => "Loan - Distribute",
            "wallet-records" => "Wallet History",
            "wallets/edit" => "Edit Member Wallet",
            "wallets/edit/wallet/rwallet" => "Edit Member Wallet (Register Wallet) **",
            "wallets/edit/wallet/awallet" => "Edit Member Wallet (Activation Wallet) **",
            "wallets/edit/wallet/awallet2" => "Edit Member Wallet (Activation Wallet 2) **",
            "wallets/edit/type/debit" => "Edit Member Wallet (Debit) **",
            "withdrawals" => "Withdrawal",
            "withdrawals/edit" => "Withdrawal - Edit",
            "commissions" => "Bonus Listing",
            "commissions-analysis" => "Bonus Analysis",
            "commissions-summary" => "Bonus Summary",
            "pairing-history" => "Pairing Listing",
            "paired-max-summary" => "Paired Max Summary",
            "news" => "News & Event",
            "exchange" => "Exchange",
            "trade/history/queue" => "Trade Matched History",
            "trade/history/sell" => "Trade Sell History",
            "trade/history/buy" => "Trade Buy History",
            "report/member-country" => "Member Overview",
            "report/member-wallet" => "Wallet Summary",
            "report/member-wallet/only-swallet" => "Wallet Summary (Shopping Point Only) **",
            "report/trade-allocation" => "eShare Overview",
            "report/sales-commission" => "Sales & Bonus Overview",
            "loggers" => "Log History",
            "adminusers" => "Admin",
        ],
        'status' => [
            10 => 'Active',
            30 => 'Suspended',
            90 => 'Terminated',
        ],
    ],

    // Page Content
    'pagetitle' => [
        'user' => [
            'home' => 'Home',
            'register' => 'Register',
            'upgrade' => 'Package Purchase',
            'upgrade-topup' => 'Package Upgrade',
            'personal-purchase' => 'Personal Purchase',
            'personal-group-purchase' => 'Personal Group Purchase',
            'group-sales-history' => 'Group Sales History',
            'sponsor-genealogy' => 'Referrer Tree',
            'placement-genealogy' => 'Placement Tree',
            'commission-summary' => 'Bonus',
            'commission-list' => 'Bonus History (:commission)',
            'roi-summary' => 'ROI',
            'roi-list' => 'ROI History (:roi)',
            'wallet-history' => ':wallet History',
            'wallet-transfer' => ':wallet Transfer ',
            'wallet-transfer-repeat' => 'Wallet Transfer Repeat',
            'wallet-transfer-purchase-scwallet' => 'Purchase Shopping Coupon',
            'wallet-internal-transfer' => 'Franchise Account Internal Transfer',
            'franchise-account-register' => 'Franchise Account Register',
            'franchise-account-topup' => 'Franchise Account Upgrade',
            'wallet-withdraw-history' => 'Wallet Withdraw History',
            'wallet-withdraw' => 'Wallet Withdraw',
            'download-area' => 'Download Area',
            'news' => 'News & Announcement',
            'forex' => 'Exchange Rate',
            'profile' => 'Profile',
            'sponsor' => 'Referrer',
            'micool-transfer' => 'DM Point to Mi Credit',
            'mobile_verify' => 'Mobile Verify',
            'trading-lobby' => 'Trading Lobby',
            'buy-lobby' => 'Want To Buy',
            'sell-lobby' => 'Want To Sell',
            'my-list' => 'My Trade',
            'trade-history' => 'Trade History',
            'create-buying-offer' => 'Create Buying Offer',
            'create-selling-offer' => 'Create Selling Offer',
            'my-offers' => 'My Offers',
            'buy-offer' => 'Buying Offer',
            'sell-offer' => 'Selling Offer',
            'buy-details' => 'Buying Details',
            'sell-details' => 'Selling Details',
        ],
        'admin' => [

        ],
    ],

    'menu' => [
        'public' => [
            'login' => 'Login',
            'home' => 'Home',
        ],
        'user' => [
            'home' => 'Home',
            'menu' => 'Menu',
            'main-menu' => 'Main Menu',
            'wallet-transfer-title' => 'Wallet Transfer',
            'register' => 'Register New',
            'activate-special-account' => 'Activate Special Account',
            'upgrade' => 'Package Purchase',
            'upgrade-topup' => 'Package Upgrade',
            'wallet-history-title' => 'Wallet History',
            'purchase-record' => 'Purchase Record',
            'personal-purchase' => 'Personal Purchase',
            'personal-group-purchase' => 'Personal Group Purchase',
            'group-sales' => 'Group Sales',
            'genealogy' => 'Genealogy',
            'group-sales-history' => 'Group Sales History',
            'genealogy' => 'Genealogy',
            'sponsor-genealogy' => 'Referrer Tree',
            'placement-genealogy' => 'Placement Tree',
            'bonus' => 'Bonus',
            'roi' => 'ROI',
            'commission-summary' => 'Bonus Summary',
            'account-info' => 'Account Info',
            'points' => 'Points History',
            'wallet-history' => ':wallet History',
            'wallet-transfer' => ':wallet Transfer ',
            'wallet-transfer-repeat' => 'Wallet Transfer [Repeat]',
            'wallet-transfer-purchase-scwallet' => 'Purchase Shopping Coupon',
            'withdrawal' => 'Withdrawal',
            'wallet-withdraw-history' => 'Withdraw History',
            'wallet-withdraw' => 'Withdraw Request',
            'others' => 'Others',
            'info-center' => 'Info Center',
            'news' => 'News & Announcement',
            'news_short' => 'News',
            'download-area' => 'Download Area',
            'forex' => 'Exchange Rate',
            'franchise' => 'Franchise',
            'franchise-account-register' => 'Franchise Account Register',
            'franchise-account-topup' => 'Franchise Account Upgrade',
            'franchise-account-internal-transfer' => 'Franchise Account Internal Transfer',
            'trade' => 'eShare',
            'wallets' => 'My Wallet',
            'mobile_verify' => 'Mobile Verify',
            'profile' => 'Profile',
            'sponsor' => 'Referrer',
            'language' => 'Language',
            'logout' => 'Log Out',
            'trading-lobby' => 'Trading Lobby',
            'buy-lobby' => 'Buy Lobby',
            'sell-lobby' => 'Sell Lobby',
            'my-list' => 'My Trade',
            'trade-history' => 'Trade History',
            'cashout' => 'Cash Out',
            'transaction-history' => 'Transaction History',
            'products' => 'Products',
            'view-my-transaction-history' => 'View my transaction history',
            'mall' => 'Mall',
            'cashout-request-history' => 'Cash-out request history',
        ],
        'admin' => [
            'admin' => 'Admin',
            'home' => 'Home',
            'profile' => 'Profile',
            'logout' => 'Logout',

            'member' => [
                'title' => 'Member',
                'listing' => 'Member Listing',
                'passwords_listing' => 'Member Passwords Listing',
                'sa1_listing' => 'Member SA1 Listing',

                'genealogy' => [
                    'title' => 'Genealogy',
                    'sponsor_tree' => 'Referrer Tree',
                    'placement_tree' => 'Placement Tree',
                    'upline_listing' => 'Upline Listing',
                    'network_check' => 'Network Relationship Check',
                    'network_listing' => 'Network Listing',
                    'franchise_listing' => 'Franchise Listing',
                ],
            ],

            'sales' => [
                'title' => 'Sales',
                'listing' => 'Sales Listing',
                'cancellation' => 'Sales Cancellation',
                'personal_listing' => 'Personal Sales Listing',
                'group_listing' => 'Group Sales Listing',
                'summary' => 'Sales Summary',
                'small_group_activations_summary' => 'Small Group Sales Summary',
                
                'add_free_member' => 'Add Free Member',
                'send_welcome_email' => 'Send Welcome Email',

                'loan_accounts_listing' => 'Loan Accounts Listing',
                'early_settlement' => 'Early Settlement',
                'add_loan_activation' => 'Add Loan Activation',
                'add_zero_bv_activation' => 'Add Zero BV Activation',
                'add_special_package' => 'Add SA1',
            ],

            'wallet' => [
                'title' => 'Wallet',
                'edit_member_wallet' => 'Edit Member Wallet',
                'history' => 'Wallet History',
                'topup' => 'Wallet Top Up (:wallet)',
                'summary' => 'Wallet Summary',
                'manualwithdrawal' => 'Manual Withdrawal',
            ],

            'bonus' => [
                'title' => 'Bonus',
                'listing' => 'Bonus Listing',
                'pairing' => 'Pairing Listing',
                'paired-max-summary' => 'Paired Max Summary',
                'summary' => 'Bonus Summary',
                'analysis' => 'Bonus Analysis',
            ],


            'withdrawal' => [
                'title' => 'Withdrawal',
                'receipt' => 'Receipt',
                'withdrawal_details' => 'Withdrawal Details',
                'user_details' => 'User Details',
                'user_bank_details' => "User's Bank Details" 
            ],

            'news' => [
                'title' => 'News & Events',
                'listing' => 'News & Events Listing',
                'add_news' => 'Add News & Events',
            ],

            'exchange' => [
                'title' => 'Exchange',
                'listing' => 'Exchange Listing',
                'history' => 'Exchange History',
                'add_exchange' => 'Add Exchange',
            ],

            'trade' => [
                'title' => 'Trading',
                'history' => trans('system.trade_name').' History',
                'non_tradeable_history' => "Floating ".trans('system.trade_name')." History",
                'pending_sell_listing' => " Pending Sell History",
                'buy_listing' => "Buy History",
                'queue_listing' => "Match History",
                'balance' => trans('system.trade_name')." Balance",
                'summary' => trans('system.trade_name')." Summary",
                'edit_trade' => 'Edit '. trans('system.trade_name'),
            ],

            'report' => [
                'title' => 'Report',
                'member_overview' => 'Member Overview',
                'trade_overview' => 'eShare Overview',
                'sales_and_bonus_overview' => 'Sales & Bonus Overview',
            ],

            'log_history' => 'Log History',

            'settings' => [
                'title' => 'Settings',
                'general' => 'General',
                'active_bonus' => 'Active Bonus',
                'passive_bonus' => 'Passive Bonus',
            ],
        ],
    ],

    'slide' => [
        'user' => [
            '-en' => '-en',
        ],
    ], 

    'page' => [
        'public' => [
            'login' => [
                'bar-title' => [
                    'login'=>'Member Login',
                ],
                'content' => [
                    'remember me' => 'Remember Me',
                    'forgot password' => 'Forgot Your Password?',
                    'captcha-helper' => 'Click :link here :end_link to refresh.',
                    'terms' => 'I agree with all the <a href=":link">terms & conditions</a>',
                    't&c' => 'Terms & Conditions',
                    'back_login' => 'Back to Login',
                    'privacy-policy' => 'Privacy Policy',
                ],
                'errmsg' => [
                    'terms-check' => "Please agree with all the terms & conditions.",
                    'account-suspended' => 'Account has been suspended.',
                    'account-terminated' => 'Account has been terminated.',
                ],
            ],
            'reset' => [
                'bar-title' => [
                    'reset password'=>'Reset Password',
                ],
            ],
            'email' => [
                'bar-title' => [
                    'reset password'=>'Reset Password',
                ],
                'content' => [
                    'send link' => 'Send Password Reset Link',
                ],
            ],
        ],

        //user page
        'user' => [
            'home' => [
                'bar-title' => [
                    'home'=>'Main Page',
                    'group_summary'=>'Referrer Linkage Group',
                ],
                'content' => [
                    'welcome-msg' => 'Welcome back! your last login :',
                ],
            ],
            'register' => [
                'bar-title' => [
                    'register'=>'Register New',
                    'register-review'=>'Review Account Detail',
                    'account-info'=>'Account Information',
                    'package'=>'Package Selection',
                    'noted' => "Noted",
                ],
                'content' => [
                    'package-compliment' => 'Compliments',
                    'active-account' => 'Active',
                    'reserved-account' => 'Reserved',
                    'username-helper' => 'alphanumeric only, maximum 100 length',
                    'noted' => '* is required field.',
                    'scan-hint' => 'Scan me to register',
                ],
                'msg' => [
                    'register-success' => 'Registration Success',
                ],
                'errmsg' => [
                    'register-error' => 'There are some errors during process, please try again later.',
                    'no-wallet-purchase' => 'Please do wallet puchase.',
                ],
            ],

            'activate-special-account' => [
                'msg' => [
                    'not-special-account' => 'This is not a special account.',
                    'require-point' => 'Require :amount AP2 to activate this special account.',
                    'activate-success' => 'Activation Success. User :username has been activated.',
                    'partial-activate' => 'User :username still requires :balance Activation Point 2.',
                ],
                'errmsg' => [
                    'register-error' => 'There are some errors during process, please try again later.',
                    'non-settled-package' => 'User already had inactive SA1 package.',
                ],
            ],

            'mobile_verify' => [
                'bar-title' => [
                    'mobile_verify' => 'Mobile Verify'
                ],
                'content' => [
                    'noted' => 'Please verify your mobile no before navigate to others page.',
                    'success-noted' => 'Your Mobile is verified, you may now proceed to other page. ',
                ],
                'msg' => [
                    'mobile-verify-success' => 'Mobile Verify Success',
                ],
                'errmsg' => [
                    'mobile-verify-error' => 'Mobile Verify Error',
                ],
            ],

            'upgrade' => [
                'bar-title' => [
                    'upgrade'=>'Package Purchase',
                    'upgrade-review'=>'Review Package Purchase Detail',
                    'package-info'=>'Package Upgrade Information',
                    'package'=>'Package Selection',
                    'noted' => "Noted",
                    'register-sub-account' => 'Register Sub Account',
                ], 
                'content' => [
                    'package-compliment' => 'Compliments',
                    'original-price' => 'Original Price',
                    'upgrade-price' => 'Upgrade Price',
                ],
                'msg' => [
                    'upgrade-success' => 'Package Purchase Success',
                    'create-sub-alert' => 'The mobile number is registered. Are you going to create a sub-account for this mobile number?',
                ],
                'errmsg' => [
                    'non-settled-package' => 'User already had non-settled package.',
                ],
            ],

            'personal-group-purchase' => [
                'bar-title' => [
                    'personal-group-purchase'=>'Personal Group Purchase',
                ],
            ],
            'personal-purchase' => [
                'bar-title' => [
                    'personal-purchase'=>'Personal Purchase',
                ],
            ],
            'group-sales-history' => [
                'bar-title' => [
                    'group-sales-history'=>'Group Sales',
                ],
            ],
            'sponsor-genealogy' => [
                'bar-title' => [
                    'sponsor-genealogy'=>'Referrer Tree',
                ],
            ],
            'placement-genealogy' => [
                'bar-title' => [
                    'placement-genealogy'=>'Placement Tree',
                ],
            ],
            'commission-summary' => [
                'bar-title' => [
                    'commission-summary'=>'Bonus Summary',
                    'roi-summary'=>'ROI Summary',
                ],
            ],
            'commission-list' => [
                'bar-title' => [
                    'commission-list'=>'Bonus History (:commission)',
                    'roi-list'=>'ROI History (:roi)',
                ],
            ],
            'download-area' => [
                'bar-title' => [
                    'download-area'=>'Download Area',
                ],
            ],
            'profile' => [
                'bar-title' => [
                    'profile'=>'Profile',
                    'general-info'=>'General Info',
                    'downline-info' => 'Downline Info',
                    'login-password'=>'Change Login Password',
                    'password2'=>'Change Security Password',
                    'beneficiary-info' => "Beneficiary Information",
                    'bank-info' => "Bank Information",
                    'account-setting' => "Account Setting",
                    'noted' => "Noted",
                ],
                'content' => [
                    'password2' => 'New :field',
                    'password2_confirmation' => 'New :field',
                    'password' => 'New :field',
                    'password_confirmation' => 'New :field',
                    'bank_sorting_code-helper' => "Please key in either your BIC, Swift Code, ABA or routing no.",
                    'bank_iban-helper' => "IBAN is required for Clients with European Bank Accounts.",
                    'edit_profile' => "Edit Profile",
                ],
                'msg' => [
                    'general-info-updated' => 'General Info Updated',
                    'password-updated' => 'Login Password Updated',
                    'password2-updated' => ':field Updated',
                    'beneficiary-info-updated' => 'Beneficiary Information Updated',
                    'bank-info-updated' => 'Bank Information Updated',
                    'setting-updated' => 'Account Setting Updated',
                ],
                "errmsg" => [
                    "address-required" => "Please fill up your address before continue.",
                    "password-change-required" => "Please change your :field before continue.",
                ],
            ],

            'sponsor' => [
                'bar-title' => [
                    'sponsor' => "Referrer",
                    'sponsor-info'=>'Referrer Info',
                ],
                'msg' => [
                    'sponsor-info-updated' => "Referrer Updated"
                ],
            ],

            'wallet-transfer' => [
                'bar-title' => [
                    'wallet-transfer'=>':wallet Transfer ',
                    'wallet-transfer-review'=>'Review Transfer Information',
                    'wallet-transfer-info'=>'Transfer Information',
                    'wallet-transfer-repeat'=>'Wallet Transfer Repeat',
                    'wallet-transfer-purchase-scwallet' => 'Purchase Shopping Coupon',
                ],
                'msg' => [
                    'transfer-success' => 'Transfer Success',
                ],
                "errmsg" => [
                    "same-account-wallet" => "Same account and wallet transfer is not allowed.",
                    "block" => "You are not allowed to make this action.",
                    "daily_max_transfer" => "Daily transfer from :from_wallet to :to_wallet cannot be more than :max. Amount reached :amount."
                ]
            ],
            'micool-transfer' => [
                'bar-title' => [
                    'wallet-transfer' => 'DM Point to Mi Credit',
                    'wallet-transfer-review' => 'Review DM Point to Mi Credit Information',
                    'wallet-transfer-info' => 'DM Point to Mi Credit Information',
                ],
                'content' => [
                    "mall" => "Mall",
                ],
                'msg' => [
                    'transfer-success' => 'DM Point to Mi Credit Success',
                ],
                'errmsg' => [
                    'debt-account' => "You are not allowed to transfer your DM points to mi cool until your loan has been fully settled. Thank you.",
                    'transfer-error' => "There are some errors during process, please try again later."
                ]
            ],
            'wallet-internal-transfer' => [
                'bar-title' => [
                    'wallet-transfer'=>'Franchise Account Internal Transfer',
                    'wallet-transfer-review'=>'Review Transfer Information',
                    'wallet-transfer-info'=>'Transfer Information',
                ],
                'content' => [
                    'franchise-main-account' => 'Main Account',
                    'franchise-account' => 'Franchise Account',
                    'you-are-at-wallet-management-page' => 'You are now at :wallet Management Page',
                ],
                'msg' => [
                    'transfer-success' => 'Transfer Success',
                ],
                "errmsg" => [
                    "same-account-wallet" => "Same account and wallet transfer is not allowed.",
                    "7-accounts-full-package" => "Please purchase PP05 for all main account and franchise accounts before you may internal transfer franchise points from username: :from_username.",
                ]
            ],
            'wallet-history' => [
                'bar-title' => [
                    'wallet-history'=>':wallet History',
                ],
            ],
            'franchise-account-register' => [
                'bar-title' => [
                    'register'=>'Franchise Account Register',
                    'register-review'=>'Review Franchise Account Detail',
                    'account-info'=>'Franchise Account Information',
                    'package'=>'Package Selection',
                    'noted' => "Noted",
                ],
                'content' => [
                    'you-are-at-wallet-management-page' => 'You are now at :wallet Management Page',
                    'noted' => '* is required field.'
                ],
                "errmsg" => [
                    "7-accounts-full-package" => "Please purchase PP05 package for your main account and 6 sub accounts before you can use :wallet to register new account.",
                ],
            ],
            'franchise-account-topup' => [
                'bar-title' => [
                    'upgrade-topup'=>'Franchise Account Upgrade',
                    'upgrade-topup-review'=>'Review Franchise Account Upgrade Detail',
                    'package-info'=>'Franchise Account Upgrade Information',
                    'package'=>'Package Selection',
                ], 
                'content' => [
                    'you-are-at-wallet-management-page' => 'You are now at :wallet Management Page',
                    'original-price' => 'Original Price',
                    'upgrade-price' => 'Upgrade Price',
                ],
                "errmsg" => [
                    "7-accounts-full-package" => "Please purchase PP05 package for your main account and 6 sub accounts before you can use :wallet to upgrade other account.",
                    'non-settled-package' => 'User already had non-settled package.',
                ],
            ],
            'wallet-withdraw' => [
                'bar-title' => [
                    'wallet-withdraw' => 'Wallet Withdrawal',
                    'wallet-withdraw-review' => 'Review Withdrawal Information',
                    'wallet-withdraw-info' => 'Withdrawal Information',
                    'noted' => 'Noted',
                ],
                'content' => [
                    'your-withdraw-max' => 'Your Maximum Withdrawal Amount is',
                    'withdraw-min' => 'Minimum Withdrawal Amount is',
                    'withdraw-fee' => 'Withdrawal Fee (%)',
                    'noted' => "- There are two Withdrawal Periods: 1st of month until 15th of month, and 16th of month until end of month.\n- Only 1 request allowed for each period stated.\n- Processing time: 03 working days.",
                    "initialize-bank-info" => "Please click :link here :end_link to setup your bank information before proceed.",
                    "upline-block" => "Your withdrawal has been blocked, please contact your upline or administrator for further inquiries.",
                ],
                'msg' => [
                    'withdraw-success' => 'Withdrawal request made. Processing time: 03 working days.',
                ],
                'errmsg' => [
                    "max-per-period" => "Only 1 request allowed for each period stated.",
                    "not-complete-withdrawal" => "There is a withdrawal request not completed.",    
                    "not-multiple-of" => "The :attribute is not multiple of :multiple.",    
                ],
            ],
            'wallet-withdraw-history' => [
                'bar-title' => [
                    'wallet-withdraw-history'=>'Wallet Withdraw History',
                ],
            ],
            
            'news' => [
                'bar-title' => [
                    'news'=>'News & Announcement',
                ],
                'content' => [
                    'news' => 'All News',
                ],
            ],
            
            'forex' => [
                'bar-title' => [
                    'forex' => 'Exchange Rate',
                ],
                'content' => [
                    'latest-rates' => 'Latest Rates',
                ],
            ],

            //page general content
            'general' => [
                'wallet-balance' => 'Your :wallet Balance is: :amount',
            ],
            'errmsg' => 'Whoops! Something went wrong!',
        ],

        'trade' => [
            'buy' => 'buy',
            'sell' => 'sell',
            'seller' => 'seller',
            'buyer' => 'buyer',
            'buy-request' => 'Buy Request',
            'sell-request' => 'Sell Request',
            'my-selling-request' => 'Selling Request',
            'custom-selling' => 'Custom Trade Sell',
            'my-buying-request' => 'Buying Request',
            'custom-buying' => 'Custom Trade Buy',
            'reference-price' => 'Reference Price',
            'current-market-value' => 'Current Market Value',
            'coin-balance' => 'Your coin balance',
            'transaction-limit' => 'Transaction Limit',
            'max-buy' => 'Maximum buying quantity',
            'min-buy' => 'Minimum buying quantity',
            'max-sell' => 'Maximum selling quantity',
            'min-sell' => 'Minimum selling quantity',
            'seller-min-sell' => 'Seller minimum sell',
            'seller-max-sell' => 'Seller maximum sell',
            'buyer-min-buy' => 'Buyer minimum buy',
            'buyer-max-buy' => 'Buyer maximum buy',
            'buy_at' => 'Buying at',
            'sell_at' => 'Selling at',
            'cancelled_at' => 'Cancelled At',
            'created_at' => 'Created At',
            'completed_at' => 'Completed At',
            'expired_at' => 'Expired At',
            'status' => 'Status',
            'rating' => 'Rating',
            'success' => 'Trading Success.',
            'cancel-buy' => 'Cancel Buying Request',
            'payment-proof' => 'Payment Proof',
            'temporary-empty' => 'There is none trade at the moment',
            'buyer-contact' => 'Buyer Contact',
            'seller-contact' => 'Seller Contact',
            'payment' => [
                'step_1' => 'Place Offer',
                'step_2' => 'Payment',
                'step_3' => 'Completed',
                'countdown' => 'You have :time left to make payment.',
                'pending-sell' => 'Buyer is getting ready to make payment and upload payment proof.',
                'expiry-hint' => 'If buyer does not make payment within 24 hours. This trade will be cancelled automatically',
                'received' => 'Seller have received your payment and will process soon!',
                'approval-request' => 'Confirm this request to complete it.',
                'approval-hint' => 'Your selling coins will be transfer to buyer wallet right after you confirm with the payment proof shown.',
                'warning' => 'Order will be cancel automatically if you did not make payment within 24 hours.',
                'filejs-upload-hint' => 'Click here to upload payment proof.',
                'method' => 'Payment Method',
                'select' => 'Please choose a payment method',
                'bank-transfer' => 'Bank Transfer',
                'bank-info' => 'Bank Info',
                'bank-name' => 'Bank Name',
                'bank-acc-no' => 'Bank Account Number',
                'wechatpay' => 'Wechat Pay',
                'wechat-id' => 'WeChat ID',
                'alipay' => 'Alipay',
                'alipay-id' => 'Alipay ID',
                'upload-wechat-pay-hint' => 'Click here to upload your WeChat pay QR code screenshot',
                'upload-alipay-hint' => 'Click here to upload your Alipay QR code screenshot',
                'wallet-id' => 'WeChat Pay or Alipay ID',
            ],
            'request' => [
                'initiated' => 'Initiated',
                'pending' => 'Pending',
                'paid' => 'Paid',
                'completed' => 'Completed',
                'cancelled' => 'Cancelled',
            ],
            'hint' => [
                'attention' => 'Attention!',
                'tax-deduction' => '5% of processing fee will be deducted from total coins bought. 1% of total coins bought will be transfer to charity wallet for charity purpose.',
                'min-sell' => 'Minimum selling quantity cannot lower than :min_qty.',
                'max-sell' => 'Maximum selling quantity cannot more than :max_qty.',
                'cancel-buy' => 'Are you confirm to cancel this buying request?',
                'payment-ss' => 'Click here to upload your WeChat Pay or Alipay QR code screenshot',
            ],
            'error' => [
                'min-buy' => 'Minimum buying cannot be lower than :min_qty。',
                'max-buy' => 'Maximum buying cannot be higher than :max_qty。',
                'min-sell' => 'Minimum selling cannot be lower than :min_qty.',
                'max-sell' => 'Maximum selling cannot be higher :max_qty.',
                'insufficient_cwallet' => 'The selling coin is more than what you own.',
                'cancel-buy' => 'There are pending sell requests. Please clear before cancel.',
                'cancel-sell' => 'There are pending buy requests. Please clear before cancel.',
                'coin-required' => 'To sell :amount coins，you need to have :required coin.',
                'selling-coin' => 'There are :locked coins of yours is selling，while your cash wallet have :cwallet left.',
                'bank-info-required' => 'It seems that you have not fill up your bank information yet. Bank information is required for bank transfer payment.',
                'wechatpay-ss-required' => 'WeChat Pay QR code screenshot is required if you want the buyer to pay with WeChat Pay',
                'wechatpay-id-required' => 'WeChat Pay ID is required if you want the buyer to pay with WeChat Pay',
                'alipay-ss-required' => 'Alipay QR code screenshot is required if you want the buyer to pay with Alipay',
                'alipay-id-required' => 'Alipay ID is required if you want the buyer to pay with Alipay',
                'payment-ss-required' => 'QR code screenshot is required if you want the buyer to pay with e-wallet.',
                'payment-id-required' => 'ID of your e-wallet is required for better payment experience.',
                'bank-setup' => 'Bank account information is not set up yet. Please set up your bank info before continue.',
                'selling-capped' => 'The total sellable coins is capped for today. Please try again tomorrow.',
                'left-to-cap' => 'There are only :left sellable coins left to be sold today.',
                'market-closed' => 'Trade market is open from 10a.m. to 5p.m. daily. Market is closed for now.',
                'multiple-cancel' => 'This trade is cancelled.',
                'multiple-approve' => 'This trade is approved.',
                'multiple-trade' => 'Only one open trade at one time allowed. Please cancel your current open trade before create a new one.',
                'tradebuy-queueing' => 'Complete or cancel your buy request before you buy again from this trade sell.',
                'tradesell-queueing' => 'Complete or cancel your sell request before you sell again for this trade buy.',
                'cancel-paid-trade' => 'You cannot cancel a paid trade. Please contact admin if you want to cancel the trade.',
            ],
            'success' => [
                'buy-request' => 'Buying request successfully.',
                'sell-request' => 'Selling request successfully.',
                'buy-offer-created' => 'Buying offer created successfully.',
                'sell-offer-created' => 'Selling offer created successfully.',
                'buy-end' => 'Trade Buy Ended!',
                'sell-end' => 'Trade Sell Ended!',
                'buy-cancelled' => 'Trade Buy Cancelled!',
                'trade' => 'Trading completed successfully.',
                'rate' => 'Your rating is submitted. Thanks for your feedback on the trading experience.',
                'paid' => 'Payment made. Wait for approval.',
            ],
            'currency' => [
                'CNY' => 'Chinese Yuan',
            ],
            'rate' => [
                'title' => 'Trade completed. Leave your rating for buyer/seller',
                'message' => 'Your rating is important for other traders to getting better experience. 5 stars for best, 1 start for bad.',
                '0' => 'No rating yet',
                '1' => 'onestar',
                '2' => 'twostars',
                '3' => 'threestars',
                '4' => 'fourstars',
                '5' => 'fivestars',
            ],
        ],

        //genealogy page
        'genealogy'=>[
            'placement-node' => [
                'content' => [
                    'accumulated' => 'Accumulated',
                    'carry-forward' => 'Carry Forward',
                    'today-sales' => 'Today Sales',
                ],
            ],
        ],

        //admin page
        'admin' => [
            'login' => [
                'bar-title' => [
                    'login' => 'Admin Login',
                ],
                'content' => [
                    'captcha-helper' => 'Can\'t read the image? click :link here :end_link to refresh.',
                ],
                'errmsg' => [
                    'account-suspended' => 'Account has been suspended.',
                    'account-terminated' => 'Account has been terminated.',
                ],
            ],
            'home' => [
                'bar-title' => [
                    'dashboard' => "Dashboard", 
                ],
                'content' => [
                    'welcome' => 'You are logged in!',
                ]
            ],
            'member-list' => [
                'bar-title' => [
                    'member-list' => "Member Listing"
                ],
                'content' => [
                    'initial_package_purchase_date' => 'Initial Package Purchase Date',
                    'last_package_purchase_date' => 'Last Package Purchase Date',
                ],
            ],
            'member-password-list' => [
                'bar-title' => [
                    'member-password-list' => "Member Passwords Listing"
                ],
                'content' => [
                    'edit_password' => 'Edit Passwords',
                ],
            ],
            'member-sa1-list' => [
                'bar-title' => [
                    'member-sa1-list' => "Member SA1 Listing"
                ],
                'content' => [
                    '1st_package_date' => '1st Package Date',
                    'last_activated_date' => 'Latest Activated Date',
                ],
            ],
            'sponsor-genealogy' => [
                'bar-title' => [
                    'sponsor-genealogy' => 'Referrer Tree',
                ],
            ],
            'placement-genealogy' => [
                'bar-title' => [
                    'placement-genealogy' => 'Placement Tree',
                ],
            ],
            'upline-list' => [
                'bar-title' => [
                    'upline-list' => 'Upline Listing',
                ],
                'content' => [
                    'noted' => 'Noted: Please enter User ID or Username in search form for any results.',
                    'network' => "Network",
                    'placement' => "Placement",
                    'sponsor' => "Referrer",
                ],
            ],
            'network-list' => [
                'bar-title' => [
                    'network-list' => 'Network Listing',
                ],
                'content' => [
                    'change_network' => 'Change Network',
                ],
            ],
            'franchise-list' => [
                'bar-title' => [
                    'franchise-list' => 'Franchise Listing',
                ],
                'content' => [
                    'change_franchise' => 'Change Franchise Network',
                ],
            ],
            "change-network" => [
                "errmsg" => [
                    "is-member-sponsor-downline" => "The :attribute is member's Referrer Downline.",
                    "is-member-placement-downline" => "The :attribute is member's Placement Downline.",
                ],
            ],
            'sales-list' => [
                'bar-title' => [
                    'sales-list' => 'Sales Listing',
                ],
            ],
            'sales-cancellation' => [
                'bar-title' => [
                    'sales-cancellation' => 'Sales Cancellation',
                ],
            ],
            'personal-sales-list' => [
                'bar-title' => [
                    'personal-sales-list' => 'Personal Sales Listing',
                ],
                'content' => [
                    'noted' => 'Noted: Please enter User ID or Username in search form for any results.',
                ],
            ],
            'group-sales-list' => [
                'bar-title' => [
                    'group-sales-list' => 'Group Sales Listing',
                ],
                'content' => [
                    'noted' => 'Noted: Please enter User ID or Username in search form for any results.',
                ],
            ],
            "sales-summary" => [
                'bar-title' => [
                    "sales-summary" => "Sales Summary",
                ],
                'content' => [
                    'last_sales_date' => "Last Sales Date",
                    'no_of_sales' => "No. of Sales",
                    'total_amount' => "Total Amount",
                    'total_price' => "Total Price",
                ],
            ],
            "small-group-activations-summary" => [
                'bar-title' => [
                    "small-group-activations-summary" => "Small Group Sales Summary",
                ],
            ],
            "loan-accounts-list" => [
                'bar-title' => [
                    "loan-accounts-list" => "Loan Account Listing",
                ],
                'content' => [
                    "distribute_loan_sales" => "Distribute Loan Sales",
                ],
            ],
            "wallet-records" => [
                'bar-title' => [
                    "wallet-records" => "Wallet History",
                ],
            ],
            "wallet-topups" => [
                'bar-title' => [
                    "wallet-topups" => "Wallet Top Up (:wallet)",
                ],
            ],
            "wallet-summary" => [
                'bar-title' => [
                    "wallet-summary" => "Wallet Summary",
                ],
                'content' => [
                    "user_type" => "User Type",
                    "user_no_debt" => "Member with No Debt",
                    "user_debt" => "Member with Debt",
                    "total_amount" => "Total Amount",
                    "no_of_user" => "No. of Users",
                ],
            ],
            "bonus-list" => [
                'bar-title' => [
                    "bonus-list" => "Bonus Listing",
                ],
            ],
            "pairing-list" => [
                'bar-title' => [
                    "pairing-list" => "Pairing Listing",
                ],
            ],
            "paired-max-summary" => [
                'bar-title' => [
                    "paired-max-summary" => "Paired Max Summary",
                ],
            ],
            "bonus-summary" => [
                'bar-title' => [
                    "bonus-summary" => "Bonus Summary",
                ],
                'content' => [
                    "last_bonus_date" => "Last Bonus Calculation Date",
                    "no_of_bonus" => "No. of Bonus",
                    "total_amount" => "Total Amount",
                    "total_amount_total_sales" => "Total Amount / Total Sales",
                ],
            ],
            "bonus-analysis" => [
                'bar-title' => [
                    "bonus-analysis" => "Bonus Analysis",
                ],
                'content' => [
                    "last_bonus_date" => "Last Bonus Calculation Date",
                    "total_bonus" => "Total Bonus",
                    "total_bonus_payout_total_sales" => "Total Bonus Payout / Total Sales",
                    "total_roi" => "Total ROI",
                    "total_investment_amount" => "Total Investment Amount",
                    "total_pending_withdrawal" => "Total Pending Withdrawal",
                    "total_paid_withdrawal" => "Total Paid Withdrawal",
                    "account_creation_date" => "Account Creation Date",
                ],
            ],
            "withdrawal" => [
                'bar-title' => [
                    "withdrawal" => "Withdrawal History",
                ],
            ],
            "news" => [
                'bar-title' => [
                    "news" => "News & Event",
                ],
            ],
            "exchange" => [
                'bar-title' => [
                    "exchange" => "Currency Exchange",
                    "delete" => "Delete Exchange Rate",
                ],
                "content" => [
                    "no_data" => "No Data",
                    "last_update" => "Last Update",
                    "delete" => "Are sure you want to delete this rate?",
                ],
            ],
            "exchange-history" => [
                'bar-title' => [
                    "exchange-history" => "Currency Exchange History",
                ],
                "content" => [
                    "no_data" => "No Data",
                    "updated_by" => "Updated By",
                    "activity" => "Activity",
                    "last_update" => "Last Update",
                ],
            ],
            "trade-history" => [
                'bar-title' => [
                    "trade-history" => ":trade_name History",
                ],
                'content' => [
                    "balance" => "Balance",
                    "total_debit" => "Total Debit",
                    "total_credit" => "Total Credit",
                ],
            ],
            "non-trade-history" => [
                'bar-title' => [
                    "non-trade-history" => ":trade_name History",
                ],
                'content' => [
                    "balance" => "Balance",
                    "total_debit" => "Total Debit",
                    "total_credit" => "Total Credit",
                ],
            ],
            "trade-pending-sell-history" => [
                'bar-title' => [
                    "trade-pending-sell-history" => "Pending Sell History",
                ],
                'content' => [
                    "total_pending" => "Total Pending",
                    "total_sold" => "Total Sold",
                ],
            ],
            "trade-buy-history" => [
                'bar-title' => [
                    "trade-buy-history" => "Buy History",
                ],
            ],
            "trade-queue-history" => [
                'bar-title' => [
                    "trade-queue-history" => "Matched History",
                ],
            ],
            "trade-balance" => [
                'bar-title' => [
                    "trade-balance" => ":trade_name Balance",
                ],
                'content' => [
                    "balance" => "Balance",
                ],
            ],
            "trade-summary" => [
                'bar-title' => [
                    "trade-summary" => ":trade_name Summary",
                ],
            ],
            "member-country" => [
                'bar-title' => [
                    "member-country" => "Member Overview",
                ],
                "content" => [
                    "country_code" => "Country Code",
                    "continent" => "Continent",
                    "total_members" => "Total Members",
                    "current_date_time" => "Current Date Time",
                ],
            ],
            "trade-overview" => [
                'bar-title' => [
                    "trade-overview" => ":trade_name Overview",
                    "trade-allocation" => ":trade_name Allocation",
                    "company_account_list" => "Company Account List",
                ],
                "content" => [
                    "company" => "Company",
                    "member" => "Member",
                    "owner" => "Owner",
                    "ratio" => "Ratio",
                    "current_price" => "Current Price",
                    "total_trade_value" => "Total :trade_name Value",
                    "total_trade_unit" => "Total :trade_name Unit",
                ],
            ],
            "sales-commission" => [
                'bar-title' => [
                    "sales-commission" => "Sales & Bonus Overview",
                ],
                "content" => [
                    "month" => "Month",
                    "wallet_sales" => ":wallet Sales",
                    "total_sales" => "Total Sales",
                    "total_bv" => "Total BV",
                    "total_bonus_payout" => "Total Bonus Payout",
                    "total_roi_payout" => "Total ROI Payout",
                ],
            ],
            "logger" => [
                'bar-title' => [
                    "logger" => "Log History",
                ],
                'content' => [
                    "last_archive_date" => "Last Archive Date Time",
                    "archive" => "Archive",
                    "yes" => "Yes",
                    "no" => "No",
                ],
            ],
            "admin-list" => [
                'bar-title' => [
                    "admin-list" => "Admin List",
                ],
            ],
            "wallet-edit" => [
                'success' => 'Edit success!',
            ],

            //page general content
            'errmsg' => 'Whoops! Something went wrong!',
        ],
        
        //errors
        "errors" => [
            "custom" => [
                "404" => [
                    "bar-title" => [
                        "404" => "Page Not Found",
                    ],

                    "content" => [
                        "404" => "Sorry, you are landing to a not found page.",
                    ],
                ],
                "general-error" => [
                    "bar-title" => [
                        "general-error" => "Sorry",
                    ],

                    "content" => [
                        "general-error" => "There is an error found during this action, please try again later.",
                    ],
                ],
                "maintenance" => [
                    "bar-title" => [
                        "maintenance" => "Maintenance",
                    ],

                    "content" => [
                        "maintenance" => "Sorry, system is under maintenance.",
                    ],
                ],
            ],
        ],
    ],

    'email' => [
        'reset-password' => [
            'subject' => 'Password Reset',
            'content' => [
                'click-link' => 'Click here to reset your password:',
            ],
        ],

        //member site email
        "user" => [
            'new-user' => [
                'subject' => 'Welcome! You are now ready to experience the whole new level of Dongli88',
                "content" => [
                    "welcome" => "Welcome! Thank you for register with us in Dongli88. You may now logon to our privileged member area at :link with the following information:-",
                    "thanks" => "Thanks & regards,<br/>Customer Service<br/>Dongli88 Limited",
                ],
            ],
        ],
    ],
];
