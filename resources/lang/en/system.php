<?php

return [

    'language'			=>	"English",
    'company'			=>	"Global Pay" . (config("app.site") ? " (" . config("app.site") . ")" : ""),
    'member'			=>	"Member",
    'admin'			    =>	"Admin",
    'system'            => 'System',
    'copyright'         => 'Copyright',
    'allrights'         => 'All rights reserved',
    'coming-soon'       => 'Coming Soon',

    'trade_name'        => 'eShare',

    /**
     * lang for Datatables plugin.
     *
     * will passing url for json file
     */
    'lang-datatables' => '/plugin/datatables/lang/en.json'
];
