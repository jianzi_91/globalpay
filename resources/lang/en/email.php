<?php

return [
    'register' => [
        'subject' => 'ZFT: Member Registered',
        'content' => [
            'welcome' => 'Welcome to join Global Pay! Your membership details is at the below section, please enter the official website to continue.', 
            'username' => 'Username',
            'name' => 'Name',
            'nid' => 'IC/Passport No.',
            'mobile' => 'Mobile No.',
            'email' => 'Email',
            'country' => 'Country',        
        ],
        'regards' => 'Thank you!',
        'gpay-cs' => 'Global Pay - Customer Service',
    ],
];