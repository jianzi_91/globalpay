<?php

return [

    'errmsg'			=>	[
        'username-required' => 'Please insert a Username.',
        'user-not-found' => 'Error: User does not exist.',
        'fail' => 'Fail! Please try again later.',
        'logout' => 'Your have logged out your account.',
        'return_error' => "Error found. Do you want refresh your page before continue?",
    ],

];
