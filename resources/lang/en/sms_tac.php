<?php

return [
    'sms' => [
        'verify_mobile' => 'ZFT :tac. This is your ZFT mobile verification code.',
        'update_mobile' => 'ZFT :tac. This is your ZFT mobile update verification code.',
        'reset_password' => 'ZFT :tac. This is your ZFT password reset verification code.',
        'register' => 'ZFT :tac. This is your ZFT registration code.',
    ],
    'content' => [
        "timer" => ':minutem :seconds left to try again',
    ],
    'msg' => [
        'success' => ":attribute has been send to your :mobile_field(:mobile)."
    ],
    'error' => [
        'not_found' => 'There is no :attribute found. Please request a new :attribute.',
        'not_matched' => 'The :attribute is not matched',
        'resend' => 'Too soon to request another TAC',
        'transmission' => 'Error occurred during SMS transmission',
        'system_error' => 'Error occurred during process',
    ],
];