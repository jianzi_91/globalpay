<?php

return [

    'account' => 'Account',
    'cancel' => 'Cancel',
    'creation' => 'Creation',
    'credit' => 'Credit',
    'company' => 'Company',
    'country' => 'Country',
    'buy' => 'Buy',
    'date' => 'Date',
    'debit' => 'Debit',
    'from' => 'From',
    'have' => 'has|have', 
    'id' => 'ID',
    'member' => 'Member',
    'price' => 'Price',
    'rank' => 'Rank',
    'sell' => 'Sell',
    'status' => 'Status',
    'to' => 'To',
    'total' => 'Total',
    'type' => 'Type',
    'user' => 'User',

];