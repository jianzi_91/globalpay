<?php

return [
    // General
    'trade' => 'Trade',
    'qty' => 'Quantity',
    'price' => 'Price',
    'volume' => 'Volume',
    'unit' => 'unit|units',
    'buy' => 'Buy',
    'sell' => 'Sell',
    // 'marketSuspended' => 'eShare trade system is temporary closed. It will be resumed on 2017/07/02 at 1 am. Please be informed.',
    'marketSuspended' => 'EShare trade system is temporary closed for 1 day for preparation of unit splitting. Please stay tuned.',
    'stock_split' => 'eShare split from 0.300 to 0.100',

    'buy_id' => trans('common.buy').' '.trans('common.id'),
    'buy_date' => trans('common.buy').' '.trans('common.date'),
    'sell_id' => trans('common.sell').' '.trans('common.id'),


    // Status
    'status' => [
        'pending' => 'Pending',
        'fulfilled' => 'Fulfilled',
        'cancelling' => 'Pending Cancel',
        'cancelled' => 'Cancelled',
    ],

    // Modal
    'modal' => [
        'purchaseConfirmation' => 'Purchase Confirmation',
        'saleConfirmation' => 'Sale Confirmation',
        'sure' => 'Are you sure?',
    ],

    // Chart
    'chart' => [
        'title' => 'Chart',
        'from' => 'From',
        'to' => 'To',
        'zoom' => 'Zoom',
        'week' => 'Week',
        'month' => 'Month',
        'year' => 'Year',
        'ytd' => 'YTD',
        'max' => 'MAX',
        'price' => 'Price',
    ],

    // Stats
    'stats' => [
        'title' => 'Statistics',
        'prev' => 'Previous',
        'open' => 'Open',
        'highest' => 'Highest',
        'lowest' => 'Lowest',
        'last' => 'Last',
        'current' => 'Current',
        'change' => 'Change',
        'intradayVol' => 'Intraday Volume',
    ],

    // Buy & Sell Panels
    'panel' => [
        'buyUnit' => 'Buy eShare',
        'sellUnit' => 'Sell eShare',
        'totalBuyingPrice' => 'Total Buying Price',
        'totalSellingPrice' => 'Total Selling Price',
        'securityPassword' => 'Security Password',
        'disclaimer' => 'Quantity & Price subject to market availability.'
                            . '<br>Total Buying Price only for your reference.',
        "maxSellingValue" => "Max Total Selling Value: :max",
        "maxSellUnit" => "Max Quantity: ",
        'tradeCaptcha' => 'Verification Code',
        'tradeCaptchaHelper' => 'Click :link here :end_link to refresh.',

        'msg' => [
            'successPurchase' => 'Your eShare is successfully purchased.',
            'successPlace' => 'You have a successfully put a eShare buy order in queue.',
            'insufficientFund' => 'You do not have enough fund in your '.trans('general.wallet.wallet1').' wallet.',
            'separatePurchases' => 'Because of insufficient '.trans('general.wallet.wallet1').' fund to buy stated quantity as a whole OR eShare price has moved higher,'
                                    . '<br>You may not acquire desired quantity.',

            'successSale' => 'Your eShare has been successfully listed on the market for sale.',
            'isDebtAccount' => 'Debt Account is not permitted to sell eShare.',
            'hasPendingSellTrade' => 'You already have a pending eShare listed.',
            'hasSoldTradeAtPrice' => 'You have already sold eShare at this price before.'
                                        . '<br>Please select a higher selling price.',
            'hasSoldTradeToday' => 'You have already sold eShare today.'
                                        . '<br>Please continue selling again tomorrow.',
            'marketPriceRange' => 'Your selling price is not within permitted price range.'
                                    . '<br>Please select an appropriate selling price.'
                                    . '<br>Current market price: $:curMarketPrice'
                                    . '<br>Your selling price: $:yourSellingPrice',
            'insufficientUnits' => 'You do not have such quantity of eShare for sale.'
                                    . '<br>Please select an appropriate selling quantity.',
            'hasNoRoomToSell' => 'This price is full for sale.'
                                    . '<br>Please select a higher selling price.',
            'investmentLimit' => 'Current selling amount and sold amount cannot be larger than your investment amount'
                                    . '<br>Please select an appropriate selling qty.',
            'partialSale' => 'Selling limit is reached for price $:sellingPrice, original selling quantity reduced. You have only listed :actualQty units.'
                                . '<br>Original Selling Qty: :origQty'
                                . '<br>Actual Selling Qty: :actualQty',
            'hasPendingBuyTrade' => 'You already have a pending eShare buy order.'
                                    . '<br>Please allow the system some time to process it.'
                                    . '<br>Sorry for any inconvenience.',
            'hasReachedDailyMaxPrice' => 'Daily highest selling price has reached.',
            'isDailyTradeClosed' => 'Daily trading time is from 9:00 am to 12:00 am.',
        ],
    ],

    // Current Sell Order
    'currentSellOrder' => [
        'title' => 'Current Sell Order',
        'initialQty' => 'Initial Quantity',
        'currentQty' => 'Current Quantity',

        'msg' => [
            'cancelTimePast' => 'You cannot cancel sell order after 3 mins.',
            'cancellingOrder' => 'Your sell order is cancelled.',
            'cancellingError' => 'Error during cancellation. Please try again.',
            'orderProcessed' => 'Your sell order has already been :status.',
            'portionSold' => 'Your trade is unable to cancel due to portion of '.trans('system.trade_name').' units have been sold.',
            'notBelong' => 'This order does not belong to you.',
        ],
        
        'sellOrderCancellation' => 'Sell Order Cancellation',
    ],

    // Buy Orders List
    'buyOrdersList' => [
        'title' => 'Pending Buy Orders List',
        'no' => 'No.',
        'refNo' => 'Reference No.',
        'initialQty' => 'Initial Quantity',
        'openQty' => 'Open Quantity',
    ],

    // Selling Prices List
    'sellingPricesList' => [
        'title' => 'Selling Prices List',
        'sellingPrice' => 'Selling Price',
        'action' => 'Action',
        'viewList' => 'View List',
    ],

    // Sell Orders List
    'sellOrdersList' => [
        'title' => 'Sell Orders List',
        'no' => 'No.',
        'refNo' => 'Reference No.',
        'openQty' => 'Open Quantity',

        'msg' => [
            'listedQueue' => 'Your sell order is now at queue no. :rownum (reference no: :id).'
        ],
    ],

    // Transaction Reports
    'transactionReports' => [
        'title' => 'Transaction Reports',
        'viewTransactionHistory' => 'View Tradeable eShare Transaction History',
        'viewTradeStatements' => 'View Tradeable eShare Trade Statements',
        'viewNonTradeStatements' => 'View Floating eShare Trade Statements',
    ],

    // Transactions History
    'transactionHistory' => [
        'title' => 'Tradeable eShare Transaction History',
        'amount' => 'Amount',
        'status' => 'Status',
        'pastTransactions' => 'Past Transactions',
    ],

    // Trade Statements
    'tradeStatements' => [
        'title' => 'Tradeable eShare Trade Statements',
        'statements' => 'Statements',
        'unitOnHand' => 'eShare on Hand',
        'tradeableUnit' => 'Tradeable eShare',
        'buyDescription' => 'Buy :qty unit of eShare at price :price from market.',
        'packageBuyDescripton' => ':qty :unit of eShare (:total / :price) have been credited into your account from (:package) Package Purchase on :date',
        'sellDescription' => 'Sell :qty units of eShare at price :price.',
        'splitDescription' => ':newQty units of eShare (:qty x :split_by) have been credited to your account for eShare split from 0.300 to 0.100.',
        'systemTix' => ':qty :unit of eShare (:total / :price) have been credited into your account.',
        "userConvert" => "Transfer :qty eShare units to SRP (equivalent to :rwallet2 SRP)",
        "commAutoBuyDescription" => "Auto Buy :qty unit of eShare at price :price from market. (ROI & Bonus)",
        "nonTradeDistributeDescription" => ":formula from Floating eShare to Tradeable eShare. :day_count", 
    ],

    // Non Trade Statements
    'nonTradeStatements' => [
        'title' => 'Floating eShare Statements',
        'statements' => 'Statements',
        'nonTradeableUnit' => 'Floating eShare',
        'splitDescription' => ':newQty units of eShare (:qty x :split_by) have been credited to your account for eShare split from :from_price to :to_price.',
        "nonTradeDistributeDescription" => ":formula from Floating to tradeable ebond. :day_count", 
    ],

    'admin' => [
        'currentSellOrder' => [
            'msg' => [
                'cancellingOrder' => 'Sell ID :sell_id is cancelled.',
                'cancellingError' => 'Error during cancellation. Please try again.',
                'orderProcessed' => 'Sell ID :sell_id has already been :status.',
                'portionSold' => 'Sell ID :sell_id cannot be cancelled as portion of it has already been sold.',
            ],
        ],
    ]

];