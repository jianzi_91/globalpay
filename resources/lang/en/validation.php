<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
	
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],

        // currency exchange
        'currency' => [
            'unique' => 'Currency already in list.'
        ],
    ],
	
    'unmatch' => [
		'existing' => 'The :attribute field is not match with existing.',
        'incorrect' => 'The :attribute field is invalid.',
        'fwallet_only_allow_transfer_to_own_account' => 'Repeat Transfer [Dowallet -> Fwallet] only can be done by own account',
        'not_enough_value' => 'DoWallet not enough credit',
        'repeat_transfer_value_multiply_10' => 'Repeat Transfer value must be multiple by 10',
        'awallet_not_enough_value' => 'AWallet not enough credit',
    ],
	
    'network' => [
		'non-related' => 'The :attribute1 and :attribute2 are not in same tree.',
		'not-downline' => 'The :attribute1 is not :attribute2 downline.',
		'non-related-with-you' => 'The :attribute and you are not in same tree.',
		'not-your-downline' => 'The :attribute is not your downline.',
		'is-your-downline' => 'The :attribute is your downline.',
		'no-position-downline' => 'The :attribute does not have selected :position_attribute downline.',
		'non-related-with-your-franchise-tree' => 'The :attribute is not your Franchise Account.',
    ],

    'username' => [
        'pattern' => 'The :attribute must contain at least a letter and can only contain letters and numbers.',
    ],

    'password' => [
        'pattern' => 'The :attribute must contains at least a Letter and Digit.',
    ],

    'upgrade-topup' => [
        'full-rank' => 'The :attribute field has no package to upgrade.',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        // news
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'title_en' => 'Title(English)',
        'title_cn' => 'Title(中文)',
        'description_en' => 'Description(English)',
        'description_cn' => 'Description(中文)',
        'content_en' => 'Content(English)',
        'content_cn' => 'Content(中文)',

        // currency exchange
        'currency' => 'Currency',
        'rate' => 'Rate'
    ],
	
    'wallet-insufficient' => ':wallet is insufficient. Required: :required, Balance: :balance.',
    'wallet-insufficient-locked' => ':wallet is insufficient. Required: :required, Balance: :balance, Locked :locked.',
    'trade-unit-insufficent' => 'eShare units insufficient. Required: :required, Balance: :balance.',

    'security_password_notmatch' => 'security password not matching',
    'insufficient_dowallet' => 'insufficient of Do-Wallet',
    'min_withdrawal_100' => 'minimum withdrawal value is 100',
    'min_withdrawal_10' => 'minimum withdrawal value is 10',
    'swallet_transfer_for_wine_user_only' => 'swallet transfer only for wine user',
    'cashout_in_progress' => 'Cash-Out is in progress',
];
