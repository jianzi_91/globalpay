<?php

return [

    'success' => '成功',
    'attention' => '注意',
    'warning' => '警告',
    'error' => '错误',

    //General
    'table' => [
        'header' => [
            'id' => '参考号',
            'date' => '日期',
            'last-date' => '最后日期',
            'wallet' => '钱包类型',
            'type' => '类型',
            'status' => '状态',
            'amount' => '金额',
            'balance' => '余额',
            'bank-info' => '银行账号',
            'descr' => '注解',
            'action' => '行动',
            'price' => '价位',
            'qty' => '数量',
            'country' => '国家',
            'account_rank' => '用户等级',
            'account_type' => '用户类型',
            'total_investment_amt' => '总投资金额',
            'max_investment_rate' => '最高投资率',
            'max_tradeable_qty' => '最高可交易量',
            'placed_qty' => '摆放量',
            'sold_qty' => '已售量',
            'pending_qty' => '待定量',
            'sell_id' => "销售号",
            'sell_date' => '销售日期',
            'account_creation_date' => '用户创建时间',
            'last_transaction_date' => '最后交易日期',
            'total_trade_income' => '总:trade_name收获',
            'total_pending_withdrawal' => '总待定提款',
            'total_paid_withdrawal' => '总已付款提款',
            'count' => "次数",
            'member_count' => "人数",
            'package' => "配套",
            'account-type' => "用户类型",
            'bonus-type' => "奖金类型",
            'number' => "号", 
            'level' => '级别',
            'leader_info' => '领导',
            'bv' => '链接算力',
            'total_price' => '总额',
            'unit_price' => '个别价位',
            'available_sells' => '卖币列表',
            'available_buys' => '买币列表',
        ],
        'helper' => [
            "before" => "前",
            "after" => "后",
        ],
        'listing' => [
            'record' => '记录。',
            'refresh' => '清除搜索及刷新。',
        ],
        'footer' => [
            'total' => '总额',
    
            'grand_total' => '总计',
        ],
    ],

    'search_field' => [
        'field' => [
            'field-from' => '从:field',
            'field-to' => '至:field',
            'date-from' => ':field-从',
            'date-to' => ':field-至',
            'date' => "日期",
            'today' => "今天",
            'all_times' => "全部时间",
            'other' => "其它",
            'from' => "从",
            'to' => "至",
            'target_date' => '计算日期',
            'start_date' => '开始日期',
            'end_date' => '结束日期',
            'transcode' => '交易类型',
        ]
    ],

    'field' => [
        'buy_id' => '挂买编号',
        'sell_id' => '挂卖编号',
        'phone_code' => '电话代码',
        'captcha' => '验证码',
        'verification-code' => '验证码',
        'quantity' => '数量',
        'price' => '价格',
        'total' => '总额',
        'processing-fee' => '平台费用',
        'charity-fund' => '慈善基金',
        'receivable' => '最终收入',
        'total-deduct' => '总扣除',
        'type' => '类型',
        'transaction_id' => '交易编号',
        'a_amt' => '支付通金额',
        'a2_amt' => '二级支付通金额',
        'sc_amt' => '消费卷数额',
        'security_code' => '安全码',
        'upload_receipt' => '凭依上传',
        'new_image' => '新图片',
        'no_data' => '没有资料',
        'continue' => '继续',
    ],

    'wallet'			=>	[
        'rwallet' => '注册钱包',
        "awallet" => "支付通", // 一级商城购物钱包
        "awallet2" => "二级支付通", // 二级商城购物钱包
        // 'cwallet' => '可提钱包',
        'cwallet' => '元宝',
        'wallet1' => '金徽章',
        'wallet2' => '红徽章',
        'swallet' => '储蓄通',
        'dowallet' => '交易通',
        'fwallet' => '理财通',
        'dwallet' => '债务钱包',
        'iwallet' => '投资钱包',
        'commission' => '佣金钱包',
        'trade-unit' => '可交易电子股',
        'trade-value' => '电子股总值',
        'trade-float-unit' => '浮动电子股',
        'coin' => '币',
        'coins' => '币',
        'scwallet' => '消费卷',
    ],

    'button'    =>  [
        'yes' => '是',
        'no' => '否',
        'add' => '添加',
        'activate' => '激活',
        'update' => '更新',
        'proceed' => '继续',
        'submit' => '提交',
        'edit' => '修改',
        'back' => '返回',
        'back-to-list' => '返回列表',
        'register' => '注册',
        'read' => '阅读',
        'upgrade' => '升级',
        'add-new' => '添新',
        'next' => '下一个',
        'terminate' => '终止',
        'suspend' => '暂停',
        'unsuspend' => '取消暂停',
        'search' => '搜索',
        'check' => '检查',
        'more-detail' => '更多详情',
        'login' => '登入',
        'cancel' => '取消',
        'confirm' => '确认',
        'view' => '查看',
        'close' => '关闭',
        'click-here' => '请点击此处',
        'redirect' => '转送',
        'toggle_search_form' => "显示/隐藏搜索表单",
        "column_visibility" => "列表显示",
        "restore_visibility" => "回复显示",
        'pay' => "付款",
        'delete' => "删除",
        'purchase' => "购买",
        'view' => '查看',
        'request_TAC' => '申请验证码',
        'buy' => '买入',
        'sell' => '卖出',
        'add-selling-offer' => '挂卖',
        'add-buying-offer' => '挂买',
        'my-offers' => '我的挂单',
        'trade-history' => '交易记录',
        'view' => '观览',
        'view_receipt' => '观览凭依',
    ],

    "selection" => [
        "please-select" => "-- 请选择 --",
        "all" => "-- 全部 --",
        'others' => '其他',
        'please-specify' => '請注明',
        'country' => [
            'china' => '中国',
            'malaysia' => '马来西亚',
        ],
    ],

    // Option Sentence
    'activation' => [
        'title' => [
            '1100' => '普通',
            '2100' => '独特',
            '3100' => '贵宾',
        ],
        'status' => [
            '10' => '已确认',
            '90' => '已取消',
        ],
        'act_type' => [
            '10' => '正常',
            '30' => '债务',
            '50' => 'SA1',
            '70' => '零 BV',
            '90' => 'SA2',
        ],
        'share_convert' => [
            '0' => '否',
            '10' => '是',
        ],
        'distribute_after_loan' => [
            '0' => '否',
            '10' => '是',
        ],
        'distribute_after_loan2' => [
            '0' => '否',
            '10' => '是',
        ],
        'loan_status' => [
            "0" => "N/A", 
            "10" => "已还清",
            "30" => "待定",
        ]
    ],

    'user' => [
        'up_pos' =>[
            0 => 'N/A',
            1 => '左',
            2 => '右',
        ],
        'rank' =>[
            'title' => [
                // 0 => '用户',
                // 1100 => '普通',
                // 2100 => '独特',
                // 3100 => '贵宾',
                0 => '普通用户',
                1000 => '白银用户',
                5000 => '黄金用户',
                10000 => '铂金用户',
                50000 => '钻石用户',
                100000=> '皇冠用户',
            ] 
        ],
        'status' =>[
            10 => '活跃',
            30 => '暂停',
            90 => '终止',
        ],
        'primary_acc' => [
            'n' => '否',
            'y' => '是',
        ],
        'company_status' => [
            0 => '普通用户',
            1 => '公司用户',
        ],

        'ranking' => '级别',
        'account-type' => [
            'normal' => '普通',
            'contra' => '回填',
            'zbv' => 'ZBV',
            'parking' => '占位',
            'free' => '免费',
        ],
        'user_type' => [
            1 => '普通用户',
            2 => '平移用户',
            3 => '红酒用户',
        ]
    ],

    'wallet-record'			=>	[
        'trans_code' => [
            '1001' => '钱包添加',
            '1002' => '钱包扣除',
            '1003' => '配套购买',
            '1004' => '钱包转帐',
            '1005' => '理财通释放',
            '1006' => '储蓄通释放',
            '1007' => '直推人奖金',
            '1008' => '钱包转换',
        ],
        'trans_code_descr' => [
            '1001' => "管理员添加 :amount :wallet。\n\n备注： :remarks",
            '1002' => "管理员扣除 :amount :wallet。\n\n备注： :remarks",
            '1003' => [
                'purchase' => '配套 (:package) 购买至用户 (:username)。 :wallet 扣除 :amount。',
                'product' => '配套 (:package) 购买至用户 (:username)。 :wallet 添加 :amount。',
                'debt' => '贷款配套 (:package) 购买至用户 (:username)。:wallet 添加 :amount。',
            ],
            '1004' => [
                'from' => "转帐 :amount 从您的 :wallet 至用户 :username。\n\n备注： :remarks",
                'to' => "转帐 :amount  从用户 :username 至您的 :wallet。\n\n备注： :remarks",
            ],
            '1005' => '用户:uid释放amount理财通至交易通。',
            '1006' => "用户:uid释放amount储蓄通至交易通。",
            '1007' => '用户#:uid透过下线#:duid消费:amount的支付通收到:bonus交易通的直推人奖金。',
            '1008' => [
                'from' => "转帐 :amount 从您的 :wallet 至用户 :username :wallet2。\n\n备注： :remarks",
                'to' => "转帐 :amount 从用户 :username :wallet2 至您的 :wallet。\n\n备注： :remarks",
            ],
        ],
        'type' => [
            'c' => '添加', 
            'd'=> '扣除',
        ],
    ],

    'commission'			=>	[
        'type' => [
            '1001' => '动态奖金',

            '2001' => '静态奖金',
        ],
        'status' => [
            10 => "已分配",
            30 => "待定",
            90 => "错误",
        ],
    ],

    "partner_transfer" => [
        "status" => [
            10 => "已确认",
            30 => "待定",
            50 => "处理中",
            70 => "取消",
            90 => "错误",
        ],
    ],

    'withdrawal'			=>	[
        'status' => [
            10 => "已付款",
            30 => "待定",
            50 => "处理中",
            70 => "取消",
        ],
    ],

    "member_status" => [
        "alliance_status" => [
            10 => "是",
            0 => "否",
        ],
    ],

    "news" => [
        "status" => [
            0 => "不发布",
            1 => "发布",
        ],
    ],

    "exchange" => [
        "status" => [
            1 => "活跃",
            0 => "不活跃",
        ],
    ],

    'admin' => [
        'status' =>[
            10 => '活跃',
            30 => '暂停',
            90 => '终止',
        ],
    ],

    // Page Content
    'pagetitle' => [
        'user' => [
            'home' => '主页',
            'register' => '注册',
            'upgrade' => '配套购买',
            'upgrade-topup' => '配套升级',
            'personal-purchase' => '个人购买',
            'personal-group-purchase' => '个人小组购买',
            'group-sales-history' => '小组业绩',
            'sponsor-genealogy' => '推荐图',
            'placement-genealogy' => '安置图',
            'commission-summary' => '奖金总结',
            'commission-list' => '奖金记录 (:commission)',
            'roi-summary' => '每日固定回酬总结',
            'roi-list' => '每日固定回酬记录 (:roi)',
            'wallet-history' => ':wallet明细',
            'wallet-transfer' => ':wallet转帐',
            'wallet-transfer-repeat' => '复投',
            'wallet-transfer-purchase-scwallet' => '购买消费卷',
            'wallet-internal-transfer' => '连锁账户之间转账',
            'franchise-account-register' => '注册新连锁账户',
            'franchise-account-topup' => '连锁账户升级',
            'wallet-withdraw-history' => '提现记录',
            'wallet-withdraw' => '提现申请',
            'download-area' => '下载专区',
            'news' => '新闻与通告',
            'forex' => '兑换率',
            'profile' => '个人档案',
            'sponsor' => '推荐上线',
            'micool-transfer' => '多米点数至米点',
            'mobile_verify' => '手机验证',
            'trading-lobby' => '交易大厅',
            'buy-lobby' => '我要买入',
            'sell-lobby' => '我要售出',
            'my-list' => '我的挂单',
            'trade-history' => '交易记录',
            'create-buying-offer' => '添加买单',
            'create-selling-offer' => '添加卖单',
            'buy' => '购买',
            'sell' => '售出',
            'my-offers' => '我的挂单',
            'buy-offer' => '买单',
            'sell-offer' => '卖单',
            'buy-details' => '购买明细',
            'sell-details' => '出售明细',
        ],
        'admin' => [

        ],
    ],

    'menu' => [
        'public' => [
            'login' => '登入',
            'home' => '主页',
        ],
        'user' => [
            'home' => '主页',
            'menu' => '功能表',
            'main-menu' => '主功能',
            'wallet-transfer-title' => '钱包转账',
            'register' => '注册新账户',
            'activate-special-account' => '激活特殊帐户',
            'upgrade' => '配套购买',
            'upgrade-topup' => '配套升级',
            'wallet-history-title' => '钱包明细',
            'purchase-record' => '购买记录',
            'personal-purchase' => '个人购买',
            'personal-group-purchase' => '个人小组购买',
            'group-sales' => '小组业绩',
            'group-sales-history' => '小组业绩',
            'genealogy' => '组织图',
            'sponsor-genealogy' => '推荐',
            'placement-genealogy' => '安置',
            'bonus' => '奖金',
            'roi' => '每日固定回酬',
            'commission-summary' => '奖金总结',
            'account-info' => '账户资讯',
            'points' => '点数明细',
            'wallet-history' => ':wallet明细',
            'wallet-transfer' => ':wallet转帐',
            'wallet-transfer-repeat' => '复投',
            'wallet-transfer-purchase-scwallet' => '购买消费卷',
            'withdrawal' => '提现',
            'wallet-withdraw-history' => '钱包提现记录',
            'wallet-withdraw' => '钱包提现',
            'others' => '其他',
            'info-center' => '资讯',
            'news' => '新闻与通告',
            'news_short' => '新闻与通告',
            'download-area' => '下载专区',
            'forex' => '兑换率',
            'franchise' => '连锁账号',
            'franchise-account-register' => '注册新连锁账户',
            'franchise-account-topup' => '连锁账户升级',
            'franchise-account-internal-transfer' => '连锁账户之间转账',
            'trade' => '电子股票',
            'wallets' => '我的钱包',
            'mobile_verify' => '手机验证',
            'profile' => '个人档案',
            'sponsor' => '推荐上线',
            'language' => '语言',
            'logout' => '登出',
            'trading-lobby' => '交易大厅',
            'buy-lobby' => '购置厅',
            'sell-lobby' => '售卖厅',
            'my-list' => '我的挂单',
            'trade-history' => '交易记录',
            'cashout' => '集市挂卖',
            'transaction-history' => '交易历史',
            'products' => '产品',
            'view-my-transaction-history' => '查看我的交易历史',
            'mall' => '商城',
            'cashout-request-history' => '提现申请历史',
        ],
        'admin' => [
            'admin' => '管理员',
            'home' => '主页',
            'profile' => '个人档案',
            'logout' => '登出',

            'member' => [
                'title' => '会员',
                'listing' => '会员列表',
                'passwords_listing' => '会员密码列表',
                'sa1_listing' => '会员SA1列表',

                'genealogy' => [
                    'title' => '组织图',
                    'sponsor_tree' => '推荐图',
                    'placement_tree' => '安置图',
                    'upline_listing' => '上线列表',
                    'network_check' => '组织关系查询',
                    'network_listing' => '组织列表',
                    'franchise_listing' => '连锁列表',
                ],
            ],

            'sales' => [
                'title' => '业绩',
                'listing' => '业绩列表',
                'cancellation' => '业绩消除',
                'personal_listing' => '个人业绩列表',
                'group_listing' => '小组业绩列表',
                'summary' => '业绩总结',
                'small_group_activations_summary' => '小区业绩总结',
                
                'add_free_member' => '添加免费会员',
                'send_welcome_email' => '发送欢迎电邮',

                'loan_accounts_listing' => '贷款用户列表',
                'early_settlement' => '提早清还',
                'add_loan_activation' => '添加贷款配套',
                'add_zero_bv_activation' => '添加零BV配套',
                'add_special_package' => '添加SA1特别配套',
            ],

            'wallet' => [
                'title' => '钱包',
                'edit_member_wallet' => '编辑会员钱包',
                'history' => '钱包历史',
                'topup' => '钱包添加 (:wallet)',
                'summary' => '钱包总结',
                'manualwithdrawal' => '手动提现',
            ],

            'bonus' => [
                'title' => '奖金',
                'listing' => '奖金列表',
                'pairing' => '对碰列表',
                'paired-max-summary' => '对碰封顶总结',
                'summary' => '奖金总结',
                'analysis' => '奖金分析',
            ],

            'withdrawal' => [
                'title' => '提款',
                'receipt' => '凭依图片',
                'withdrawal_details' => '提现资料',
                'user_details' => '用户资料',
                'user_bank_details' => "用户银行资料" 
            ],

            'news' => [
                'title' => '新闻与通告',
                'listing' => '新闻与通告列表',
                'add_news' => '添加新闻与通告',
            ],

            'exchange' => [
                'title' => '汇率',
                'listing' => '汇率列表',
                'history' => '汇率历史',
                'add_exchange' => '添加汇率',
            ],

            'trade' => [
                'title' => '交易平台',
                'history' => trans('system.trade_name')."历史",
                'non_tradeable_history' => "浮动".trans('system.trade_name')."历史",
                'pending_sell_listing' => "待售列表",
                'buy_listing' => "购买列表",
                'queue_listing' => "交易列表",
                'balance' => trans('system.trade_name')."余额",
                'summary' => trans('system.trade_name')."总结",
                'edit_trade' => '编辑'. trans('system.trade_name'),
            ],

            'report' => [
                'title' => '报告',
                'member_overview' => '会员概览',
                'trade_overview' => trans('system.trade_name').'概览',
                'sales_and_bonus_overview' => '业绩与奖金概览',
            ],

            'log_history' => '日志历史',

            'settings' => [
                'title' => '设置',
                'general' => '概括',
                'active_bonus' => '动态奖金',
                'passive_bonus' => '静态奖金',
            ],
        ],
    ],

    'slide' => [
        'user' => [
            '-en' => '-cn',
        ],
    ],

    'page' => [
        'public' => [
            'login' => [
                'bar-title' => [
                    'login'=>'会员登入',
                ],
                'content' => [
                    'remember me' => '记住账号',
                    'forgot password' => '忘记密码？',
                    'captcha-helper' => '请点击 :link 此处 :end_link 以便刷新。',
                    'terms' => '我同意所有的<a href=":link">条款和条件</a>',
                    't&c' => '条款和条件',
                    'back_login' => '返回会员登入',
                    'privacy-policy' => '隐私政策',
                ],
                'errmsg' => [
                    'terms-check' => "请同意所有的条款和条件。",
                    'account-suspended' => '用户已被暂停。',
                    'account-terminated' => '用户已被终止。',
                ],
            ],
            'reset' => [
                'bar-title' => [
                    'reset password'=>'更新密码',
                ],
            ],
            'email' => [
                'bar-title' => [
                    'reset password'=>'更新密码',
                ],
                'content' => [
                    'send link' => '发送更新密码链接',
                ],
            ],
        ],
        'user' => [
            'home' => [
                'bar-title' => [
                    'home' => '主页',
                    'group_summary'=>'推广算力池',
                ],
                'content' => [
                    'welcome-msg' => '欢迎回来！您上次登入：',
                ],
            ],
            'register' => [
                'bar-title' => [
                    'register'=>'注册新账户',
                    'register-review'=>'检阅用户信息',
                    'account-info'=>'用户信息',
                    'package'=>'配套选项',
                    'noted' => '备注',
                    'register-sub-account' => '创建子户口',
                ],
                'content' => [
                    'package-compliment' => '配送',
                    'active-account' => '激活',
                    'reserved-account' => '预留',
                    'username-helper' => '仅字母数字，最大长度为100.',
                    'noted' => '*是必须填写的项目。',
                    'scan-hint' => '扫我注册新账户',
                ],
                'msg' => [
                    'register-success' => '注册成功',
                    'create-sub-alert' => '手机号码已被注册。您要用这号码创造子户口吗？',
                ],
                'errmsg' => [
                    'register-error' => '程序出现了些许错误，请待会儿再尝试。',
                    'no-wallet-purchase' => '请行驶购买币。',
                ],
            ],

            'activate-special-account' => [
                'msg' => [
                    'not-special-account' => '这不是特殊帐户SA1。',
                    'require-point' => '需要 :amount 激活分2来激活这个特殊账户SA1。',
                    'activate-success' => '激活成功。用户:username已激活。',
                    'partial-activate' => '用户 :username 还需 :balance 激活分2。',
                ],
                'errmsg' => [
                    'register-error' => '程序出现了些许错误，请待会儿再尝试。',
                    'non-settled-package' => '用户已经有未激活的SA1配套。',
                ],
            ],

            'mobile_verify' => [
                'bar-title' => [
                    'mobile_verify' => '手机验证'
                ],
                'content' => [
                    'noted' => '请先验证您的手机，然后再进入到其他页面.',
                    'success-noted' => '手机验证已成功，您现在可以进入其他页面。',
                ],
                'msg' => [
                    'mobile-verify-success' => '手机验证成功',
                ],
                'errmsg' => [
                    'mobile-verify-error' => '手机验证失败',
                ],
            ],
            
            'upgrade' => [
                'bar-title' => [
                    'upgrade'=>'配套购买',
                    'upgrade-review'=>'检阅配套购买信息',
                    'package-info'=>'配套购买信息',
                    'package'=>'配套选项',
                    'noted' => '注意',
                ], 
                'content' => [
                    'package-compliment' => '配送',
                    'original-price' => '原金额',
                    'upgrade-price' => '升级金额',
                ],
                'msg' => [
                    'upgrade-success' => '配套购买成功',
                ],
                'errmsg' => [
                    'non-settled-package' => '用户已经有未结算的配套。',
                ],
            ],
            'personal-group-purchase' => [
                'bar-title' => [
                    'personal-group-purchase'=>'个人小组购买',
                ],
            ],
            'personal-purchase' => [
                'bar-title' => [
                    'personal-purchase'=>'个人购买',
                ],
            ],
            'group-sales-history' => [
                'bar-title' => [
                    'group-sales-history'=>'小组业绩',
                ],
            ],
            'sponsor-genealogy' => [
                'bar-title' => [
                    'sponsor-genealogy'=>'推荐图',
                ],
            ],
            'placement-genealogy' => [
                'bar-title' => [
                    'placement-genealogy'=>'安置图',
                ],
            ],
            'commission-summary' => [
                'bar-title' => [
                    'commission-summary'=>'奖金总结',
                    'roi-summary'=>'每日固定回酬总结',
                ],
            ],
            'commission-list' => [
                'bar-title' => [
                    'commission-list'=>'奖金记录 (:commission)',
                    'roi-list'=>'每日固定回酬总结 (:roi)',
                ],
            ],
            'download-area' => [
                'bar-title' => [
                    'download-area' => '下载专区',
                ],
            ],
            'profile' => [
                'bar-title' => [
                    'profile'=>'个人档案',
                    'general-info'=>'基本信息',
                    'downline-info' => '伞下用户',
                    'login-password'=>'修改登入密码',
                    'password2' => '修改安全密码',
                    'beneficiary-info' => "受益人信息",
                    'bank-info' => "银行账号",
                    'account-setting' => "设置",
                    'noted' => '注意',
                ],
                'content' => [
                    'password2' => '新 :field',
                    'password2_confirmation' => '新 :field',
                    'password' => '新 :field',
                    'password_confirmation' => '新 :field',
                    'bank_sorting_code-helper' => "请输入您的BIC，SWIFT代码，ABA或转账参号。",
                    'bank_iban-helper' => "如果您用的是欧洲银行帐户，请输入IBAN码。",
                    'edit_profile' => "更新个人资料",
                ],
                'msg' => [
                    'general-info-updated' => '基本信息已成功更新',
                    'password-updated' => '登入密码已成功更新',
                    'password2-updated' => ':field 已成功更新',
                    'beneficiary-info-updated' => '受益人信息已成功更新',
                    'bank-info-updated' => '银行账号已成功更新',
                    'setting-updated' => '设置已成功更新',
                ],
                "errmsg" => [
                    "address-required" => "请您填写完整的联络地址以便继续。",
                    "password-change-required" => "请您更新:field以便继续。",
                ],
            ],

            'sponsor' => [
                'bar-title' => [
                    'sponsor' => "推荐上线",
                    'sponsor-info'=>'推荐上线信息',
                ],
                'msg' => [
                    'sponsor-info-updated' => "推荐上线已成功更新"
                ],
            ],

            'wallet-transfer' => [
                'bar-title' => [
                    'wallet-transfer' => ':wallet转帐',
                    'wallet-transfer-review'=>'检阅转帐信息',
                    'wallet-transfer-info'=>'转帐信息',
                    'wallet-transfer-repeat'=>'复投',
                    'wallet-transfer-purchase-scwallet' => '购买消费卷',
                ],
                'msg' => [
                    'transfer-success' => '转帐成功',
                ],
                "errmsg" => [
                    "same-account-wallet" => "相同账户和钱包是不应许的。",
                    "block" => "您不被应许做该动作。",
                    "daily_max_transfer" => "每日转账从‘:from_wallet’至‘:to_wallet’不能多于‘:max’。已达‘:amount’。"
                ],
            ],
            'micool-transfer' => [
                'bar-title' => [
                    'wallet-transfer' => '多米点数至米点',
                    'wallet-transfer-review' => '检阅多米点数至米点信息',
                    'wallet-transfer-info' => '多米点数至米点信息',
                ],
                'content' => [
                    "mall" => "购物中心",
                ],
                'msg' => [
                    'transfer-success' => '多米点数至米点成功',
                ],
                'errmsg' => [
                    'debt-account' => "您的多米点数暂不能转至米酷直到您的债务钱包已全数偿还。谢谢。",
                    'transfer-error' => "程序出现了些许错误，请待会儿再尝试。"
                ]
            ],
            'wallet-internal-transfer' => [
                'bar-title' => [
                    'wallet-transfer'=>'连锁账户之间转账',
                    'wallet-transfer-review'=>'检阅转帐信息',
                    'wallet-transfer-info'=>'转帐信息',
                ],
                'content' => [
                    'franchise-main-account' => '主用户',
                    'franchise-account' => '连锁账户',
                    'you-are-at-wallet-management-page' => '您现正在 :wallet 的管理页面',
                ],
                'msg' => [
                    'transfer-success' => '转帐成功',
                ],
                "errmsg" => [
                    "same-account-wallet" => "相同账户和钱包是不应许的。",
                    "7-accounts-full-package" => "请您必须在从用户名：:from_username 进行连锁账户之间转账前完成购买PP05于您的主账户及6个子账户。"
                ],
            ],
            'wallet-history' => [
                'bar-title' => [
                    'wallet-history' => ':wallet明细',
                ],
            ],
            'franchise-account-register' => [
                'bar-title' => [
                    'register'=>'注册新连锁账户',
                    'register-review'=>'检阅连锁账户信息',
                    'account-info'=>'连锁账户信息',
                    'package'=>'配套选项',
                    'noted' => '备注',
                ],
                'content' => [
                    'you-are-at-wallet-management-page' => '您现正在 :wallet 的管理页面',
                    'noted' => '*是必须填写的项目。'
                ],
                "errmsg" => [
                    "7-accounts-full-package" => "请务必先购买PP05配套至您的主账户及6个子账户以确保您可使用:wallet注册新的账户。",
                ],
            ],
            'franchise-account-topup' => [
                'bar-title' => [
                    'upgrade-topup'=>'连锁账户升级',
                    'upgrade-topup-review'=>'检阅连锁账户升级信息',
                    'package-info'=>'连锁账户升级信息',
                    'package'=>'配套选项',
                ], 
                'content' => [
                    'you-are-at-wallet-management-page' => '您现正在 :wallet 的管理页面',
                    'original-price' => '原金额',
                    'upgrade-price' => '升级金额',
                ],
                "errmsg" => [
                    "7-accounts-full-package" => "请务必先购买PP05配套至您的主账户及6个子账户以确保您可使用:wallet升级其他的账户。",
                    'non-settled-package' => '用户已经有非结算配套。',
                ],
            ],
            'wallet-withdraw' => [
                'bar-title' => [
                    'wallet-withdraw' => '钱包提现',
                    'wallet-withdraw-review' => '检阅提现信息',
                    'wallet-withdraw-info' => '提现信息',
                    'noted' => '注意',
                ],
                'content' => [
                    'your-withdraw-max' => '您的最高提现金额为',
                    'withdraw-min' => '最低提现金额为',
                    'withdraw-fee' => '提现收费 (%)',
                    'noted' => "- 这一共有两个期限：当月1号至当月15号，和当月16号至当月尾。\n- 每一个所定期限只接受1个申请。\n- 每月只限15日与月尾日处理提现，处理所需时间为03个工作日。",
                    "initialize-bank-info" => "请按 :link 这 :end_link 填写您的银行账号以继续操作。",
                    "upline-block" => "您无法使用提现服务，请您联络您的上线或客服人员以了解详情。",
                ],
                'msg' => [
                    'withdraw-success' => '提现申请已提交。处理所需时间： 03 工作天。',
                ],
                'errmsg' => [
                    "max-per-period" => "每一个所定期限只接受1个提现申请。",
                    "not-complete-withdrawal" => "您还有未完成的提现申请。",    
                    "not-multiple-of" => ":attribute 不是 :multiple 的倍数。",    
                ],
            ],
            'wallet-withdraw-history' => [
                'bar-title' => [
                    'wallet-withdraw-history'=>'钱包提现记录',
                ],
            ],
            
            'news' => [
                'bar-title' => [
                    'news' => '新闻与通告',
                ],
                'content' => [
                    'news' => '新闻与通告列表',
                ],
            ],
            
            'forex' => [
                'bar-title' => [
                    'forex' => '兑换率',
                ],
                'content' => [
                    'latest-rates' => '最新兑换率',
                ],
            ],

            //page general content
            'general' => [
                'wallet-balance' => '您的 :wallet 余额为： :amount',
            ],
            'errmsg' => '哎呦！ 出了些问题！',
        ],

        // trade page
        'trade' => [
            'buy' => '购买',
            'sell' => '出售',
            'seller' => '卖家',
            'buyer' => '买家',
            'buy-request' => '购买请求',
            'sell-request' => '出售请求',
            'my-selling-request' => '我的出售请求',
            'custom-selling' => '自立出售请求',
            'my-buying-request' => '我的购买请求',
            'custom-buying' => '自立购买请求',
            'reference-price' => '参考价格',
            'current-market-value' => '目前市场价格',
            'coin-balance' => '您拥有的币数',
            'transaction-limit' => '交易限制',
            'max-buy' => '最大购买量',
            'min-buy' => '最小购买量',
            'max-sell' => '最大出售量',
            'min-sell' => '最小出售量',
            'seller-min-sell' => '卖家最小出售量',
            'seller-max-sell' => '卖家最大出售量',
            'buyer-min-buy' => '买家最小购买量',
            'buyer-max-buy' => '买家最大购买量',
            'buy_at' => '购买日期',
            'sell_at' => '出售日期',
            'cancelled_at' => '交易取消',
            'completed_at' => '交易完毕',
            'created_at' => '创建日期',
            'expired_at' => '失效日期',
            'status' => '状态',
            'rating' => '星评',
            'cancel-buy' => '取消买币请求',
            'payment-proof' => '付款证明',
            'temporary-empty' => '暂无任何挂单',
            'buyer-contact' => '买家联络',
            'seller-contact' => '卖家联络',
            'payment' => [
                'step_1' => '买入申请',
                'step_2' => '付款',
                'step_3' => '完成',
                'pending-sell' => '买家正准备付款并上载支付凭证。',
                'expiry-hint' => '诺买家在下单24小时未收到支付凭证，此订单将自动作废。',
                'received' => '卖家已收到您的支付，正等待确认。',
                'approval-request' => '确认订单并完成此交易',
                'approval-hint' => '请确认您收到付款后再点击确认键。当你点击确认键后，您所出售的币将会转到买家钱包。',
                'countdown' => '您还有:time为此购买请求付款',
                'warning' => '若在24小时内还未付款，此请求将自动取消。',
                'filejs-upload-hint' => '点击此处上传付款证明',
                'method' => '付款方式',
                'select' => '请选择付款方式',
                'bank-transfer' => '银行转账',
                'bank-info' => '银行资料',
                'bank-owner-name' => '银行账号拥有人',
                'bank-name' => '银行名称',
                'bank-acc-no' => '银行户口账号',
                'wechatpay' => '微信支付',
                'wechat-id' => '微信号',
                'alipay' => '支付宝',
                'alipay-id' => '支付宝号',
                'upload-wechat-pay-hint' => '点击此处上传您的微信支付二维码截图',
                'upload-alipay-hint' => '点击此处上传您的支付宝二维码截图',
                'wallet-id' => '微信支付号或支付宝号',
            ],
            'stat' => [
                'pending' => '待定',
                'paid' => '已付款',
                'completed' => '已完成',
                'cancelled' => '已取消',
            ],
            'hint' => [
                'attention' => '注意',
                'tax-deduction' => '所购买的币将扣除5%为平台维护费用。 总购买的币将扣除1%为慈善用途。',
                'min-buy' => '最小购买量不能低于:min_qty颗。',
                'max-buy' => '最大购买量不能高于:max_qty颗。',
                'min-sell' => '最小出售量不能低于:min_qty颗。',
                'max-sell' => '最大出售量不能高于:max_qty颗。',
                'cancel-buy' => '确定取消此买单吗？', 
                'payment-ss' => '点击此处上传您的微信支付或支付宝二维码截图',
            ],
            'error' => [
                'min-buy' => '最小购买量不能低于:min_qty。',
                'max-buy' => '最大购买量不能高于:max_qty。',
                'min-sell' => '最小出售量不能低于:min_qty.',
                'max-sell' => '最大出售量不能高于:max_qty.',
                'insufficient_cwallet' => '所出售的币量多于所拥有的。',
                'cancel-buy' => '仍有待定的出售挂单。请在取消前先处理。',
                'cancel-sell' => '仍有待定的购买挂单。请在取消前先处理。',
                'coin-required' => '出售:amount颗币，您必须要有:required颗币。',
                'selling-coin' => '您目前有:locked颗币正在出售，而您只有:available能够出售。',
                'bank-info-required' => '看来您还未填写您的银行资料。若欲使用银行转账必须先填写您的银行资料。',
                'wechatpay-ss-required' => '您必须上载微信支付二维码截图若欲买家透过微信支付购买',
                'wechatpay-id-required' => '您必须上载微信号若欲买家透过微信支付购买',
                'alipay-ss-required' => '您必须上载支付宝二维码截图若欲买家透过支付宝购买',
                'alipay-id-required' => '您必须上载支付宝号截图若欲买家透过支付宝购买',
                'payment-ss-required' => '您必须上载微信支付或支付宝二维码若您需要透过电子钱包付账。',
                'payment-id-required' => '为了让您能有更号的交易程序，您必须填上您微信号或支付宝号。',
                'bank-setup' => '您还没完成您的银行账号设立，请完成银行账号明细以继续买卖。',
                'selling-capped' => '今天能出售的币量已经达标。请您明天再尝试。',
                'left-to-cap' => '今天能出售的币量只剩:left。',
                'market-closed' => '交易市场每天从早上十时开市至下午五时。交易市场现在已收市。',
                'multiple-cancel' => '这交易已被取消。',
                'multiple-approve' => '这交易已被通过。',
                'multiple-trade' => '您在同一时间只能有一个挂单。请取消现有的挂单才能创立新的挂单。',
                'tradebuy-queueing' => '在购买之前，请完成或取消您目前对此挂卖的购买请求。',
                'tradesell-queueing' => '在出售之前，请完成或取消您目前对此挂买的出售请求。',
                'cancel-paid-trade' => '您不能取消已付款的交易。若坚持取消，请联络管理员。',
            ],
            'success' => [
                'buy-request' => '购买请求成功。',
                'sell-request' => '出售请求成功。',
                'buy-offer-created' => '挂买成功。',
                'sell-offer-created' => '挂卖成功。',
                'buy-end' => '挂买结束。',
                'sell-end' => '挂卖结束。',
                'buy-cancelled' => '买单成功取消。',
                'trade' => '交易成功。',
                'rate' => '您的星评已被呈交。谢谢您对此交易经验的回馈。',
                'paid' => '付款成功。等待卖家确认交易。',
            ],
            'currency' => [
                'CNY' => '人民币',
            ],
            'rate' => [
                'title' => '此次交易已经完毕，请给买家/卖家评价',
                'message' => '您的评价将有效的帮助其他会员选择到高效率的卖家及买家。五星为最佳，一星为略差。',
                '0' => '暂无星评',
            ],
        ],

        //genealogy page
        'genealogy'=>[
            'placement-node' => [
                'content' => [
                    'accumulated' => '累积',
                    'carry-forward' => '结转',
                    'today-sales' => '今日业绩',
                ],
            ],
        ],

        //admin page
        'admin' => [
            'login' => [
                'bar-title' => [
                    'login'=>'管理员登入',
                ],
                'content' => [
                    'captcha-helper' => '看不清图片里的字？ 请点击 :link 此处 :end_link 以便刷新。',
                ],
                'errmsg' => [
                    'account-suspended' => '管理员已被暂停。',
                    'account-terminated' => '管理员已被终止。',
                ],
            ],
            'home' => [
                'bar-title' => [
                    'dashboard' => "仪表", 
                ],
                'content' => [
                    'welcome' => '您已经登入！',
                ]
            ],
            'member-list' => [
                'bar-title' => [
                    'member-list' => "会员列表"
                ],
                'content' => [
                    'initial_package_purchase_date' => '初始配套购买日期',
                    'last_package_purchase_date' => '最后配套购买日期',
                ],
            ],
            'member-password-list' => [
                'bar-title' => [
                    'member-password-list' => "会员密码列表"
                ],
                'content' => [
                    'edit_password' => '修改密码',
                ],
            ],
            'member-sa1-list' => [
                'bar-title' => [
                    'member-sa1-list' => "会员SA1列表"
                ],
                'content' => [
                    '1st_package_date' => '第一配套日期',
                    'last_activated_date' => '最后激活日期',
                ],
            ],
            'sponsor-genealogy' => [
                'bar-title' => [
                    'sponsor-genealogy' => '推荐图',
                ],
            ],
            'placement-genealogy' => [
                'bar-title' => [
                    'placement-genealogy' => '安置图',
                ],
            ],
            'upline-list' => [
                'bar-title' => [
                    'upline-list' => '上线列表',
                ],
                'content' => [
                    'noted' => '注意: 请在搜索表单内输入会员号或用户名以获得资讯。',
                    'network' => "组织",
                    'placement' => "安置",
                    'sponsor' => "推荐",
                ],
            ],
            'network-list' => [
                'bar-title' => [
                    'network-list' => '组织列表',
                ],
                'content' => [
                    'change_network' => '修改组织',
                ],
            ],
            'franchise-list' => [
                'bar-title' => [
                    'franchise-list' => '连锁列表',
                ],
                'content' => [
                    'change_franchise' => '修改连锁组织',
                ],
            ],
            "change-network" => [
                "errmsg" => [
                    "is-member-sponsor-downline" => ":attribute 是用户的推荐下线。",
                    "is-member-placement-downline" => ":attribute 是用户的安置下线。",
                ],
            ],
            'sales-list' => [
                'bar-title' => [
                    'sales-list' => '业绩列表',
                ],
            ],
            'sales-cancellation' => [
                'bar-title' => [
                    'sales-cancellation' => '业绩消除',
                ],
            ],
            'sales-cancellation' => [
                'bar-title' => [
                    'sales-cancellation' => '业绩消除',
                ],
            ],
            'personal-sales-list' => [
                'bar-title' => [
                    'personal-sales-list' => '个人业绩列表',
                ],
                'content' => [
                    'noted' => '注意: 请在搜索表单内输入会员号或用户名以获得资讯。',
                ],
            ],
            'group-sales-list' => [
                'bar-title' => [
                    'group-sales-list' => '小组业绩列表',
                ],
                'content' => [
                    'noted' => '注意: 请在搜索表单内输入会员号或用户名以获得资讯。',
                ],
            ],
            "sales-summary" => [
                'bar-title' => [
                    "sales-summary" => "业绩总结",
                ],
                'content' => [
                    'last_sales_date' => "最后业绩日期",
                    'no_of_sales' => "业绩数额",
                    'total_amount' => "总积分",
                    'total_price' => "总金额",
                ],
            ],
            "small-group-activations-summary" => [
                'bar-title' => [
                    "small-group-activations-summary" => "小区业绩总结",
                ],
            ],
            "loan-accounts-list" => [
                'bar-title' => [
                    "loan-accounts-list" => "货款用户列表",
                ],
                'content' => [
                    "distribute_loan_sales" => "分配回填业绩",
                ],
            ],
            "wallet-records" => [
                'bar-title' => [
                    "wallet-records" => "钱包历史",
                ],
            ],
            "wallet-topups" => [
                'bar-title' => [
                    "wallet-topups" => "钱包添加 (:wallet)",
                ],
            ],
            "wallet-summary" => [
                'bar-title' => [
                    "wallet-summary" => "钱包总结",
                ],
                'content' => [
                    "user_type" => "会员类型",
                    "user_no_debt" => "没回填会员",
                    "user_debt" => "有回填会员",
                    "total_amount" => "总金额",
                    "no_of_user" => "会员数额",
                ],
            ],
            "bonus-list" => [
                'bar-title' => [
                    "bonus-list" => "奖金列表",
                ],
            ],
            "pairing-list" => [
                'bar-title' => [
                    "pairing-list" => "对碰列表",
                ],
            ],
            "paired-max-summary" => [
                'bar-title' => [
                    "paired-max-summary" => "对碰封顶总结",
                ],
            ],
            "bonus-summary" => [
                'bar-title' => [
                    "bonus-summary" => "奖金总结",
                ],
                'content' => [
                    "last_bonus_date" => "最后奖金计算日期",
                    "no_of_bonus" => "奖金数额",
                    "total_amount" => "总金额",
                    "total_amount_total_sales" => "总金额 / 总业绩",
                ],
            ],
            "bonus-analysis" => [
                'bar-title' => [
                    "bonus-analysis" => "奖金分析",
                ],
                'content' => [
                    "last_bonus_date" => "最后奖金计算日期",
                    "total_bonus" => "总奖金",
                    "total_bonus_payout_total_sales" => "总奖金分配 / 总业绩",
                    "total_roi" => "总ROI",
                    "total_investment_amount" => "总投资额",
                    "total_pending_withdrawal" => "总待定提款",
                    "total_paid_withdrawal" => "总已付款提款",
                    "account_creation_date" => "用户创建日期",
                ],
            ],
            "withdrawal" => [
                'bar-title' => [
                    "withdrawal" => "提款列表",
                ],
            ],
            "news" => [
                'bar-title' => [
                    "news" => "新闻与通告",
                ],
            ],
            "exchange" => [
                'bar-title' => [
                    "exchange" => "汇率列表",
                    "delete" => "删除汇率",
                ],
                "content" => [
                    "no_data" => "没有数据",
                    "last_update" => "最后修改",
                    "delete" => "您确定要删除该汇率？",
                ],
            ],
            "exchange-history" => [
                'bar-title' => [
                    "exchange-history" => "汇率历史",
                ],
                "content" => [
                    "no_data" => "没有数据",
                    "updated_by" => "被修改",
                    "activity" => "活动",
                ],
            ],
            "trade-history" => [
                'bar-title' => [
                    "trade-history" => ":trade_name历史",
                ],
                'content' => [
                    "balance" => "余额",
                    "total_debit" => "总减",
                    "total_credit" => "总加",
                ],
            ],
            "non-trade-history" => [
                'bar-title' => [
                    "non-trade-history" => ":trade_name历史",
                ],
                'content' => [
                    "balance" => "余额",
                    "total_debit" => "总减",
                    "total_credit" => "总加",
                ],
            ],
            "trade-pending-sell-history" => [
                'bar-title' => [
                    "trade-pending-sell-history" => "待售历史",
                ],
                'content' => [
                    "total_pending" => "总待定",
                    "total_sold" => "总已售",
                ],
            ],
            "trade-buy-history" => [
                'bar-title' => [
                    "trade-buy-history" => "购买历史",
                ],
            ],
            "trade-queue-history" => [
                'bar-title' => [
                    "trade-queue-history" => "交易历史",
                ],
            ],
            "trade-balance" => [
                'bar-title' => [
                    "trade-balance" => ":trade_name余额",
                ],
                'content' => [
                    "balance" => "余额",
                ],
            ],
            "trade-summary" => [
                'bar-title' => [
                    "trade-summary" => ":trade_name总结",
                ],
            ],

            "member-country" => [
                'bar-title' => [
                    "member-country" => "会员概览",
                ],
                "content" => [
                    "country_code" => "国家代码",
                    "continent" => "洲",
                    "total_members" => "会员数额",
                    "current_date_time" => "当前时间",
                ],
            ],
            "trade-overview" => [
                'bar-title' => [
                    "trade-overview" => ":trade_name概览",
                    "trade-allocation" => ":trade_name分配",
                    "company_account_list" => "公司用户列表",
                ],
                "content" => [
                    "company" => "公司",
                    "member" => "会员",
                    "owner" => "所有者",
                    "ratio" => "比率",
                    "current_price" => "当前价额",
                    "total_trade_value" => "总:trade_name值",
                    "total_trade_unit" => "总:trade_name单位",
                ],
            ],
            "sales-commission" => [
                'bar-title' => [
                    "sales-commission" => "业绩与奖金概览",
                ],
                "content" => [
                    "month" => "月份",
                    "wallet_sales" => ":wallet业绩",
                    "total_sales" => "总业绩",
                    "total_bv" => "总积分",
                    "total_bonus_payout" => "总奖金分配",
                    "total_roi_payout" => "总ROI分配",
                ],
            ],
            "logger" => [
                'bar-title' => [
                    "logger" => "日志历史",
                ],
                'content' => [
                    "last_archive_date" => "最后档案日期",
                    "archive" => "档案",
                    "yes" => "是",
                    "no" => "否",
                ],
            ],
            "admin-list" => [
                'bar-title' => [
                    "admin-list" => "管理员列表",
                ],
            ],
            "wallet-edit" => [
                'success' => '编辑成功！',
            ],

            //page general content
            'errmsg' => '哎呦！ 出了些问题！',
        ],
        
        //errors
        "errors" => [
            "custom" => [
                "404" => [
                    "bar-title" => [
                        "404" => "页面不存在",
                    ],

                    "content" => [
                        "404" => "对不起，未找到您要登入的页面。",
                    ],
                ],
                "general-error" => [
                    "bar-title" => [
                        "general-error" => "对不起",
                    ],

                    "content" => [
                        "general-error" => "程序出现了些许错误，请待会儿再尝试。",
                    ],
                ],
                "maintenance" => [
                    "bar-title" => [
                        "maintenance" => "维护中",
                    ],

                    "content" => [
                        "maintenance" => "对不起，系统正在维护中。",
                    ],
                ],
            ],
        ],
    ],

    'email' => [
        'reset-password' => [
            'subject' => '密码更新',
            'content' => [
                'click-link' => '请点击以更新密码：',
            ],
        ],

        //member site email
        "user" => [
            'new-user' => [
                'subject' => '欢迎加入动力银家Dongli88！',
                "content" => [
                    "welcome" => "欢迎加入动力银家Dongli88！您的会员注册资讯如下，请登入公司官方系统:link 以了解更多详情。",
                    "thanks" => "谢谢！<br/><br/>动力银家Dongli88- 客户服务部",
                ],
            ],
        ],
    ],
];
