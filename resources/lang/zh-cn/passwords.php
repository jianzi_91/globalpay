<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码必须不少于6个字和与确认相配。',
    'reset' => '您的密码已成功更新。',
    'sent' => '我们已发送电邮给您！',
    'token' => '重新设置密码链接已逾期，请您重新在会员区点击“忘记密码”，再从最新电邮所收到的链接重新点击一遍。',
    'user' => "我们找不到与这电话号码/电邮地址相关的用户。",

];
