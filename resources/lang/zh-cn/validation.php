<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => ':attribute只能包含字母。',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => ':attribute只能包含字母和数字。',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => ':attribute必须介于:min与:max之间。',
        'file'    => ':attribute必须介于:min与:maxKB之间。',
        'string'  => ':attribute必须介于:min与:max个文字之间。',
        'array'   => ':attribute必须介于:min与:max个物品之间。',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => ':attribute和确认的不一样。',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => ':attribute必须是:digits个数字。',
    'digits_between'       => ':attribute必须是由:min至:max个数字。',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ':attribute必须是正确的电邮号。',
    'exists'               => '所选择的:attribute是错误的。',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => '所选择的:attribute是错误的。',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => ':attribute必须是整数。',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => ':attribute不能大于:max。',
        'file'    => ':attribute不能大于:maxKB。',
        'string'  => ':attribute不能大于:max个文字。',
        'array'   => ':attribute不能多于:max个物品。',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => ':attribute不能小于:min.',
        'file'    => ':attribute不能小于:min kilobytes.',
        'string'  => ':attribute不能小于:min个文字。',
        'array'   => ':attribute不能少于:min个物品。',
    ],
    'not_in'               => '所选择的:attribute是无效的。',
    'numeric'              => ':attribute必须是数字。',
    'present'              => ':attribute必须存在。',
    'regex'                => ':attribute格式错误。',
    'required'             => ':attribute是必要的。',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => ':attribute必须是文字。',
    'timezone'             => ':attribute必须是有效区。',
    'unique'               => ':attribute已经存在了。',
    'url'                  => ':attribute格式错误。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
	
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
	
    'unmatch' => [
		'existing' => ':attribute和现有的不合。',
        'incorrect' => ':attribute是错误的。',
        'fwallet_only_allow_transfer_to_own_account' => '只可以对自己的户口进行复投。',
        'not_enough_value' => '交易通的数额不够。',
        'repeat_transfer_value_multiply_10' => '复投的数额一定得是10的倍数',
        'awallet_not_enough_value' => '支付通的数额不够。',
    ],
	
    'network' => [
		'non-related' => ':attribute1和:attribute2不在同一个组织内。',
		'not-downline' => ':attribute1不是:attribute2 的下线。',
		'non-related-with-you' => ':attribute和您不在同一个组织内。',
		'not-your-downline' => ':attribute不是您的下线',
		'is-your-downline' => ':attribute是您的下线',
		'no-position-downline' => ':attribute没有所选择的:position_attribute下线。',
    ],

    'username' => [
        'pattern' => ':attribute必须包含至少一个字母和只能包含字母和数字。',
    ],

    'password' => [
        'pattern' => ':attribute必须包含至少一个字母和数字。',
    ],

    'upgrade-topup' => [
        'full-rank' => ':attribute没有配套可以加额。',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],
	
    'wallet-insufficient' => ':wallet余额不足。所需为：:required，您的余额为：:balance。',
    'wallet-insufficient-locked' => ':wallet余额不足。所需为：:required，您的余额为：:balance，锁定为：:locked。',
    'trade-unit-insufficent' => '电子股票数量不足。所需为：:required，您拥有为：:balance。',

    'security_password_notmatch' => '输入的安全密码不正确。',
    'insufficient_dowallet' => '交易通的数额不足够提现。',
    'min_withdrawal_100' => '提现数额少于100',
    'min_withdrawal_10' => '提现数额少于10',
    'swallet_transfer_for_wine_user_only' => '只可以更改红酒用户的储蓄通',
    'cashout_in_progress' => '目前已有提现在进行中',

];
