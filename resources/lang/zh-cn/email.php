<?php

return [
    'register' => [
        'subject' => '支付通：会员注册',
        'content' => [
            'welcome' => '欢迎加入支付通！您的会员注册资讯如下，请登入公司官方会员系统。',
            'username' => '账号',
            'name' => '姓名',
            'nid' => '身份证/护照编号',
            'mobile' => '电话号码',
            'email' => '电子邮件',
            'country' => '国家',
        ],
        'regards' => '谢谢！',
        'gpay-cs' => '支付通 - 客户服务部',
    ],
];