<?php

return [

    'errmsg'			=>	[
        'username-required' => '请填写用户名。',
        'user-not-found' => '错误： 用户不存在。',
        'fail' => '失败！请等下在尝试。',
        'logout' => '您已经登出。',
        'return_error' => "发现错误。 您想在继续之前刷新您的页面吗？",
    ],

];
