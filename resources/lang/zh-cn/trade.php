<?php

return [
    // General
    'trade' => '贸易',
    'qty' => '数量',
    'price' => '价位',
    'volume' => '量',
    'unit' => '单位|单位',
    'buy' => '购买',
    'sell' => '销售',
    //'marketSuspended' => '电子股票系统暂时关闭。它将会在2017年7月2日凌晨1点开始启动。敬请留意。',
    'marketSuspended' => '电子股票系统暂时关闭一天以作拆分的准备。敬请留意。',
    'stock_split' => '电子股票由0.300拆分为0.100',
    'buy_id' => "购买号",
    'buy_date' => "购买日期",
    'sell_id' => "销售号",

    // Status
    'status' => [
        'pending' => '待定',
        'fulfilled' => '履行',
        'cancelling' => '待定取消',
        'cancelled' => '取消',
    ],

    // Modal
    'modal' => [
        'purchaseConfirmation' => '购买确认',
        'saleConfirmation' => '销售确认',
        'sure' => '您确定吗？'
    ],

    // Chart
    'chart' => [
        'title' => '图表',
        'from' => '从',
        'to' => '至',
        'zoom' => '缩放',
        'week' => '周',
        'month' => '月',
        'year' => '年',
        'ytd' => '本年迄今',
        'max' => '最大',
        'price' => '价位',
    ],

    // Stats
    'stats' => [
        'title' => '统计',
        'prev' => '收盘',
        'open' => '开盘',
        'highest' => '最高',
        'lowest' => '最低',
        'last' => '最后',
        'current' => '当前',
        'change' => '变化',
        'intradayVol' => '日内成交量',
    ],

    // Buy & Sell Panels
    'panel' => [
        'buyUnit' => '购买电子股票',
        'sellUnit' => '销售电子股票',
        'totalBuyingPrice' => '总购买价位',
        'totalSellingPrice' => '总销售价位',
        'securityPassword' => '保安密码',
        'disclaimer' => '数量与价位视市场情况而定。'
            . '<br>总购买价位仅供参考而已。',
        "maxSellingValue" => "最高总购售价值： :max",
        "maxSellUnit" => "最高数量： ",
        'tradeCaptcha' => '验证码',
        'tradeCaptchaHelper' => '请点击 :link 此处 :end_link 以便刷新。',

        'msg' => [
            'successPurchase' => '您的电子股票成功购入。',
            'successPlace' => '您已成功将电子股票购买订单放入队列中。',
            'insufficientFund' => '您的'.trans('general.wallet.wallet1').'钱包金额不足。',
            'separatePurchases' => '因您的'.trans('general.wallet.wallet1').'钱包金额不足 或 电子股票价位已提升，'
                                    . '<br>您可能无法购得所需的数量。',

            'successSale' => '您的电子股票已挂上市场销售。',
            'isDebtAccount' => '债务户口是不允许销售电子股票。',
            'hasPendingSellTrade' => '您经已有电子股票在市场上挂卖。',
            'hasSoldTradeAtPrice' => '您曾经以此价位售出电子股票。'
                                        . '<br>请筛选更高的销售价位。',
            'hasSoldTradeToday' => '您今天已经售卖过电子股票。'
                                        . '<br>请明天继续售卖。',
            'marketPriceRange' => '您的售价不在允许的价格范围内。'
                                    . '<br>请筛选适当的销售价位。'
                                    . '<br>您的售价：$:curMarketPrice'
                                    . '<br>当前市场价位：$:yourSellingPrice',
            'insufficientUnits' => '您不拥有此电子股票数量供于销售。'
                                    . '<br>请筛选适当的销售数量。',
            'hasNoRoomToSell' => '此价格销售限制已满。'
                                    . '<br>请筛选更高的销售价位。',
            'investmentLimit' => '当前销售总额与售出总额不能大于您的投资总额。'
                                    . '<br>请筛选适当的销售数量。',
            'partialSale' => '价位$:sellingPrice销售限制已满，原本销售数量将削减。 您只挂上了 :actualQty 个单位。'
                                . '<br>原本销售数量：:origQty'
                                . '<br>正确销售数量：:actualQty',
            'hasPendingBuyTrade' => '您已经下了电子股票购买订单。'
                                    . '<br>请允许系统一些时间来处理。'
                                    . '<br>任何不便之处，敬请原谅。',
            'hasReachedDailyMaxPrice' => '每日最高销售价格已达到。',
            'isDailyTradeClosed' => '每日交易时间为上午9:00至午夜12:00。',
        ],
    ],

    // Current Sell Order
    'currentSellOrder' => [
        'title' => '当前销售订单',
        'initialQty' => '初量',
        'currentQty' => '现量',

        'msg' => [
            'cancelTimePast' => '你不能在3分钟后取消销售订单。',
            'cancellingOrder' => '您的出售订单已经被取消。',
            'cancellingError' => '取消期间出错，请再尝试取消。',
            'orderProcessed' => '您的出售订单已经被:status。',
            'portionSold' => '由于一部分的'.trans('system.trade_name').'已经成功售出，您的交易是无法被取消的。',
            'notBelong' => '此订单不属于您。',
        ],
        
        'sellOrderCancellation' => '取消销售订单',
    ],

    // Buy Orders List
    'buyOrdersList' => [
        'title' => '购买订单列表',
        'no' => '序号',
        'refNo' => '编号',
        'initialQty' => '初量',
        'openQty' => '现量',
    ],

    // Selling Prices List
    'sellingPricesList' => [
        'title' => '销售价位列表',
        'sellingPrice' => '销售价位',
        'action' => '行动',
        'viewList' => '查阅列表',
    ],

    // Sell Orders List
    'sellOrdersList' => [
        'title' => '销售订单列表',
        'no' => '序号',
        'refNo' => '编号',
        'openQty' => '开放数量',
        
        'msg' => [
            'listedQueue' => '您当前的销售序号是:rownum （编号：:id）。'
        ],
    ],

    // Transaction Reports
    'transactionReports' => [
        'title' => '交易报告',
        'viewTransactionHistory' => '视察可交易电子股票交易历史',//'视察电子股票交易状态',
        'viewTradeStatements' => '视察可交易电子股票交易对帐单',
        'viewNonTradeStatements' => '视察浮动电子股票交易对帐单',
    ],

    // Transactions History
    'transactionHistory' => [
        'title' => '可交易电子股票交易历史',
        'amount' => '数额',
        'status' => '状态',
        'pastTransactions' => '过去交易',
    ],

    // Trade Statements
    'tradeStatements' => [
        'title' => '可交易电子股票交易对帐单',
        'statements' => '对帐单',
        'unitOnHand' => '持有电子股票',
        'tradeableUnit' => '可交易电子股票',
        'buyDescription' => '购入 :qty 电子股票单位于价位:price。',
        'packageBuyDescripton' => '于:date购买:package配套所产生的:qty个电子股票单位 (:total / :price) 已存入您的账户。',
        'sellDescription' => '出售 :qty 电子股票单位于价位:price。',
        'splitDescription' => '电子股票由0.300拆分为0.100。:newQty个配送单位（:qty x :split_by）已存入您的账户。',
        'systemTix' => ':qty个电子股票单位 (:total / :price) 已存入您的账户。',
        "userConvert" => "转:qty单位电子股票至SRP(相等于:rwallet2SRP)",
        "commAutoBuyDescription" => "自动购入 :qty 电子股票单位于价位:price。（配套回酬&奖金）",
        "nonTradeDistributeDescription" => ":formula从浮动电子股票单位至可交易电子股票单位。:day_count", 
    ],

    // Non Trade Statements
    'nonTradeStatements' => [
        'title' => '浮动电子股票对帐单',
        'statements' => '对帐单',
        'nonTradeableUnit' => '浮动电子股票',
        'splitDescription' => '电子股票由:from_price拆分为:to_price。:newQty个配送单位（:qty x :split_by）已存入您的账户。',
        "nonTradeDistributeDescription" => ":formula从浮动电子股票单位至可交易电子股票单位。:day_count", 
    ],

    'admin' => [
        'currentSellOrder' => [
            'msg' => [
                'cancellingOrder' => '出售订单 :sell_id 已经被取消。',
                'cancellingError' => '取消期间出错，请再尝试取消。',
                'orderProcessed' => '出售订单 :sell_id 已经被 :status.',
                'portionSold' => '由于一部分的'.trans('system.trade_name').'已经成功售出，出售订单 sell_id 是无法被取消的。',
            ],
        ],
    ]
];