<?php

return [
    'sms' => [
        'verify_mobile' => '支付通 :tac。这是您的支付通手机验证码。',
        'update_mobile' => '支付通 :tac。这是您的支付通更新手机号验证码。',
        'reset_password' => '支付通 :tac。这是您的支付通更新密码验证码。',
        'register' => '支付通 :tac。这是您的支付通注册码。',
    ],
    'content' => [
        "timer" => ':minute分:second秒至再尝试',
    ],
    'msg' => [
        'success' => ":attribute已经发送至您的:mobile_field(:mobile)。"
    ],
    'error' => [
        'not_found' => ':attribute不存在。请申请新的:attribute。',
        'not_matched' => ':attribute不一样。',
        'resend' => '不能太频密的申请验证码',
        'transmission' => '短信发送程序错误。',
        'system_error' => '程序错误。',
    ],
];