<?php

return [

    'language'			=>	"中文",
    'company'			=>	"支付通" . (config("app.site") ? " (" . config("app.site") . ")" : ""),
    'member'			=>	"会员",
    'admin'			    =>	"管理员",
    'system'    		=> '系统',
    'copyright'         => '版权',
    'allrights'         => '版权所有',
    'coming-soon'       => '即将推出',

    'trade_name' 		=> '电子股票',

    /**
     * lang for Datatables plugin.
     *
     * will passing url for json file
     */
    'lang-datatables'   => '/plugin/datatables/lang/cn_zh.json'
];
