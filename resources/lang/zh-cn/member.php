<?php
return [
    'transaction' => [
        'purchase' => '商城消费', 
        'purchase_checkout_scwallet_api' => '商城消费',
        'purchase_reward_x3_fwallet' => '杠杆 三倍消费去 【理财通】',
        'purchase_scwallet_reward_x3_fwallet' => "购买消费卷 三倍消费去 【理财通】",
        'quicken_release_purchase_reward' => '加速释放【消费】',
        'quicken_release_repeat_reward' => '加速释放【复投】',
        'daily_release_reward' => '每日释放',
        'wallet_topup' => '钱包充值',
        'wallet_transfer_out' => '转账',
        'wallet_transfer_others_awallet_out' => '对冲他人 【交易通】-> 【支付通】',
        'wallet_transfer_others_awallet_in' => '被人对冲 【交易通】-> 【支付通】',
        'wallet_transfer_self_awallet' => '自我转支付通 【交易通】 -> 【支付通】',
        'wallet_transfer_self_fwallet' => '复投, 10%额外奖金转去【消费卷】',
        'purchase_scwallet' => '【支付通】->【消费卷】',
        'quicken_release_fwallet_reward' => '加速释放【理财通】',
        'wallet_edit_admin_add' => '管理员调整钱包【添加】',
        'wallet_edit_admin_subtract' => '管理员调整钱包【扣除】',
        'cashout_member' => '会员手动提现',
        'cashout_admin_approved' => '管理员手动体现【通过】',
        'cashout_admin_rejected' => '管理员手动提现【拒绝】',
    ],
    'transaction_type' => [
        'purchase_checkout_scwallet_api' => '商城消费',
        'purchase_reward_x3_fwallet' => '杠杆',
        'purchase_scwallet_reward_x3_fwallet' => "购买消费卷奖励",
        'quicken_release_purchase_reward' => '加速释放【消费】',
        'quicken_release_repeat_reward' => '加速释放【复投】',
        'daily_release_reward' => '每日释放',
        'wallet_topup' => '钱包充值',
        'wallet_transfer_out' => '转账',
        'wallet_transfer_others_awallet_out' => '对冲他人',
        'wallet_transfer_others_awallet_in' => '被人对冲',
        'wallet_transfer_self_awallet' => '自我转支付通',
        'wallet_transfer_self_fwallet' => '复投',
        'purchase_scwallet' => '购买消费卷',
        'quicken_release_fwallet_reward' => '加速释放【理财通】',
        'wallet_edit_admin_add' => '管理员调整钱包【添加】',
        'wallet_edit_admin_subtract' => '管理员调整钱包【扣除】',
        'cashout_member' => '手动提现',
        'cashout_admin_approved' => '管理员手动体现【通过】',
        'cashout_admin_rejected' => '管理员手动提现【拒绝】',
    ],
];